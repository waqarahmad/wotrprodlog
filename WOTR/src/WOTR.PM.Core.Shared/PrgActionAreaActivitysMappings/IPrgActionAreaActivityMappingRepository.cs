﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.PrgActionAreaActivitysMappings
{
    public interface IPrgActionAreaActivityMappingRepository
    {
        
            List<PrgActionAreaActivityMappingRepositoryDto> GetPrgActionAreaActivityMappingList(int? TenantId1, long? UserID, int programId);
    }
}
