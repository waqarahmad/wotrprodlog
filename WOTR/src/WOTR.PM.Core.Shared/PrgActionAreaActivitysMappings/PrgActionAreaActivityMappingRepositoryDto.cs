﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.PrgActionAreaActivitysMappings
{
    public class PrgActionAreaActivityMappingRepositoryDto : FullAuditedEntity
    {

        public int? ComponentID { get; set; }
        public string ComponentName { get; set; }
        public DateTime ProgrameStartDate { get; set; }
        public DateTime ProgrameEndDate { get; set; }
        public List<ActivityActionandSubactionArea> Activity { get; set; }
        public virtual int TenantId { get; set; }
        public int ProgramID { get; set; }

    }
   

    public class ActivityActionandSubactionArea
    {
        public int? ComponentID { get; set; }
        public int ProgramID { get; set; }
        public int? prgActionAreaID { get; set; }
        public string prgActionAreaAcivityMapId { get; set; }

        public string CostEstimationYear { get; set; }
        public int? ActivityID { get; set; }
        public string ActivityName { get; set; }
        public long? TotalUnits { get; set; }
        public decimal? UnitCost { get; set; }
        public decimal? TotalUnitCost { get; set; }
        public int? ActionAreaID { get; set; }
        public string ActionAreaName { get; set; }
        public string subActionAreaCode { get; set; }
        public int? SubActionAreaID { get; set; }
        public string SubActionAreaName { get; set; }
        public List<ActionSubActionList> ActionSubAction { get; set; }

    }

    public class ActionSubActionList
    {
        public int? ComponentID { get; set; }
        public int? ActionAreaID { get; set; }
        public string ActionAreaName { get; set; }
        public string subActionCode { get; set; }
        public int? SubActionAreaID { get; set; }
        public string SubActionAreaName { get; set; }
        public int? ActivityID { get; set; }
        public string ActivityName { get; set; }

    }
    public class ActionAreawaisedto
    {
        public string ProgramName { get; set; }
        public int ProgramID { get; set; }
        public int ActionAreaId { get; set; }
        public string ActionAreaName { get; set; }
        public int SubactionareaId { get; set; }
        public string SubActionAreaName { get; set; }
        public int ActivityId { get; set; }     
        public string ActivityName { get; set; }
        public string TotalUnits { get; set; }
        public string CommunityContribution { get; set; }
        public string FunderContribution { get; set; }
        public string OtherConstribution { get; set; }
        public string Total { get; set; }
        public decimal? grantamount { get; set; }





    }
    public class overallAnnualActionAreadto
    {
        public string ActionAreaName { get; set; }
        public decimal? grantamount { get; set; }
        public string CommunityContribution { get; set; }
        public string FunderContribution { get; set; }
        public string OtherConstribution { get; set; }
        public string Total { get; set; }

    }
}
