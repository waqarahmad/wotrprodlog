﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.ProgramRegionSpendings.Dto
{
   public class ProgramRegionSpendingListDto : FullAuditedEntity
    {
       
        public int ProgramID { get; set; }
        public int? ComponentID { get; set; }
        public string ComponentName { get; set; }
        public DateTime ProgrameStartDate { get; set; }
        public DateTime ProgrameEndDate { get; set; }

        public List<ProgrameCostEstimationYearRegionSpendingDto> CostEstimationYear { get; set; }


    }

    public class ListActivityforProgamRegionSpendingDto
    {
        public long? regionId { get; set; }
        public int? ActivityID { get; set; }
        public string ActivityName { get; set; }
        public string CostEstimationYear { get; set; }
        public int? UnitOfMeasuresID { get; set; }
        public string UnitOfMeasuresName { get; set; }
        public long? TotalUnits { get; set; }
        public ActivityLevel ActivityLevel { get; set; }
        public decimal? UnitCost { get; set; }
        public decimal? TotalUnitCost { get; set; }
        public decimal? CommunityContribution { get; set; }
        public decimal? Grant { get; set; }
        public decimal? OtherContribution { get; set; }
        public int? ComponentID { get; set; }
        public bool IsProgrameLevel { get; set; }
        public string villageName { get; set; }
        public decimal? FunderContribution { get; set; }


        public List<SubListActivityforProgamRegionSpendingDto> SubUnitActivity { get; set; }
    }

    public class ProgrameCostEstimationYearRegionSpendingDto : FullAuditedEntity
    {
        public string CostEstimationYear { get; set; }

        public List<ListActivityforProgamRegionSpendingDto> Activity { get; set; }
    }

    public class SubListActivityforProgamRegionSpendingDto
    {
        public long? TotalUnits { get; set; }
        public decimal? UnitCost { get; set; }
        public decimal? TotalUnitCost { get; set; }
        public decimal? CommunityContribution { get; set; }
        public decimal? Grant { get; set; }
        public decimal? OtherContribution { get; set; }
        public int? LocationID { get; set; }
        public string LocationName { get; set; }
        public string regionLocationName { get; set; }
        public int regionId { get; set; }
        public int? villageId  { get;set; }
        public string villagename { get; set; }
        public decimal? FunderCosntribution { get; set; }
    }

}
