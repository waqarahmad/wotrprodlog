﻿using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.ProgramRegionSpendings.Dto;

namespace WOTR.PM.ProgramRegionSpendings
{
     public interface IProgramRegionSpendingRespository
    {
        List<ProgramRegionSpendingListDto> GetComponentandActivityForRegionandSpending(int? TenantId, int programId);

    }
}
