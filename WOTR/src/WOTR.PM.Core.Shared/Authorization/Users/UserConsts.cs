﻿namespace WOTR.PM.Authorization.Users
{
    public class UserConsts
    {
        public const int MaxPhoneNumberLength = 24;
    }
    public enum Category
    {
        Social = 1,
        Technical,
        Agriculture,
        Supervision
    }
}
