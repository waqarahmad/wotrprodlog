﻿using Abp.Domain.Entities.Auditing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using WOTR.PM.PrgImplementationPlans;
using WOTR.PM.ProgramFunds;
using WOTR.PM.ProgramRegionSpendings;

namespace WOTR.PM.PrgRequestAAPlas.Dto
{
    public class RequestAAPlasListDto : FullAuditedEntity
    {
        public int ProgramID { get; set; }
        public string ProgramName { get; set; }
        public int? LocationID { get; set; }
        public string LocationName { get; set; }
        public string ManagerName { get; set; }
        public string Year { get; set; }
        public long ManagerID { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public RequestStatus RequestStatus { get; set; }
        public DateTime ProgrameStartDate { get; set; }
        public DateTime ProgrameEndDate { get; set; }
        public ActivityLevel ActivityLevel { get; set; }
        public List<ProgrameCostEstimationYearRequestApplDto> CostEstimationYear { get; set; }
        public List<PrgManagerListDto> Managers { get; set; }
        public List<ReportQuarterwise> QuaterWise { get; set; }
    }
    public class ReportQuarterwise : FullAuditedEntity
    {
        public string Unit { get; set; }
        public string Quantity { get; set; }
        public string UnitCost { get; set; }
        public string CommunityContribution { get; set; }
        public string FunderContribution { get; set; }

    }

    public class ProgrameCostEstimationYearRequestApplDto : FullAuditedEntity
    {
        public string CostEstimationYear { get; set; }
        public int ProgramID { get; set; }
        public int? LocationID { get; set; }
        public long ManagerID { get; set; }
        public Boolean CurrentYear { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public RequestStatus RequestStatus { get; set; }
        public decimal? SubTotalTotalunits { get; set; }
        public decimal? SubTotalUnitCost { get; set; }
        public decimal? SubTotalofTotalCost { get; set; }
        public decimal? SubTotalofCommunityContribution { get; set; }
        public decimal? SubTotalofFunderContribution { get; set; }
        public decimal? SubTotalofOtherContribution { get; set; }
        public List<ListActionSubactionListDto> ActionSubactionCost { get; set; }
    }

    public class ListActionSubactionListDto : FullAuditedEntity
    {

        public string ActionAreaName { get; set; }
        public int ActionId { get; set; }
        public string SubActionAreaName { get; set; }
        public string ActivityName { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ActivityLevel ActivityLevel { get; set; }

        public int? VillageID { get; set; }
        public string VillageName { get; set; }
        public string UnitOfMeasuresName { get; set; }
        public int? UnitOfMeasuresID { get; set; }
        public int? LocationID { get; set; }
        //public int? ChecklistID { get; set; }
        public int? PrgActionAreaActivityMappingID { get; set; }
        public long? TotalUnits { get; set; }
        public long? PreviousUnits { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public RequestStatus RequestStatus { get; set; }

        public string CostEstimationYear { get; set; }
        public decimal? UnitCost { get; set; }
        public decimal? TotalUnitCost { get; set; }
        public decimal? PreviousBalanceRs { get; set; }

        public decimal? CommunityContribution { get; set; }
        public decimal? CommunityContributionReceived { get; set; }


        public decimal? FunderContribution { get; set; }

        public decimal? OtherContribution { get; set; }




        public int? Quarter1PhysicalUnits { get; set; }      //..Quarter1
        public decimal Quarter1FinancialRs { get; set; }

        public int? Quarter2PhysicalUnits { get; set; }         //..Quarter2
        public decimal Quarter2FinancialRs { get; set; }

        public int? Quarter3PhysicalUnits { get; set; }         //..Quarter3
        public decimal Quarter3FinancialRs { get; set; }

        public int? Quarter4PhysicalUnits { get; set; }             //..Quarter4
        public decimal Quarter4FinancialRs { get; set; }
        public List<ListActionSubactionListDto> SubActionSubActionActivity { get; set; }
    }

    public class PrgManagerListDto
    {
        public string Name { get; set; }
        public long ManagerId { get; set; }
        public string ManaggerLocation { get; set; }
    }

    public class ProgramSummary
    {
        public string ActivityName { get; set; }
        public string CostEstimationYear { get; set; }
        public string SubActivtyName { get; set; }
        public decimal? Units { get; set; }
        public decimal? Rs { get; set; }
        public int Quarter { get; set; }
        public DateTime SubActivityStartDate { get; set; }
        public DateTime SubActivityEndDate { get; set; }
        public string UserName { get; set; }
        public SubActivityStatus Status { get; set; }

        public DateTime ExpenseDate { get; set; }
        public string ExpenseYear { get; set; }
        public string ExpenseTitle { get; set; }
        public string ExpenseTypeName { get; set; }
        public Status1 ExpenseStatus { get; set; }
        public int ExpenseUnits { get; set; }
    }

    public class ProjectWiseDashboardData
    {
        public List<ProjectWiseInfoDTO> projectWiseInfoDTOs { get; set; }
        public List<ProjectWiseCostEstimationYear> projectWiseCostEstimationYears { get; set; }
        public List<ProjectWiseInfoPieChart> projectWiseInfoPieCharts { get; set; }
        public List<ProjectWiseInfoLineChart> projectWiseInfoLineCharts { get; set; }
        public DataTable dataTable { get; set; }
    }


    public class ProjectWiseYearlyActivity
    {
        public List<ProjectWiseYearlyActivityInfo> projectWiseInfoPieCharts { get; set; }
        public DataTable dataTable { get; set; }
    }

    public class ProjectWiseYearlyActivityInfo
    {
        public string AssignName { get; set; }
        public string ActivityName { get; set; }
    }

    public class ProjectWiseYearlyClustor
    {
        public List<ProjectWiseYearlyclustorinfo> projectWiseYearlyclustorinfo { get; set; }
        public DataTable dataTable { get; set; }
    }
    public class ProjectWiseYearlyclustorinfo
    {
        public string clustorName { get; set; }
        public string VillageName { get; set; }
        public string AssignName { get; set; }
        public string SubactivityName { get; set; }
        public string TergetUnit { get; set; }
        public string AchievedUnit { get; set; }
    }

    public class ProjectWiseInfoDTO
    {
        public string ProjectName { get; set; }
        public string ProjectManager { get; set; }
        public DateTime ProjectStart { get; set; }
        public DateTime ProjectEnd { get; set; }
        public string ProjectStatus { get; set; }
    }

    public class ProjectWiseCostEstimationYear
    {
        public string CostEstimationYear { get; set; }
    }

    public class ProjectWiseInfoPieChart
    {
        public string AssignName { get; set; }
        public string ActivityName { get; set; }
        public int status { get; set; }
        public string CostEstimationYear { get; set; }
        public string ProjectStatus { get; set; }
    }

    public class ProjectWiseInfoLineChart
    {
        public int ComponentID { get; set; }
        public string CostEstimationYear { get; set; }
        public decimal FunderContribution { get; set; }
        public decimal OtherContribution { get; set; }
        public decimal CommunityContribution { get; set; }
    }

    public class BarChartDataset
    {
        public string label { get; set; }
        public string backgroundColor { get; set; }
        public string borderColor { get; set; }
        public List<decimal> data { get; set; }
    }

    public class BarChartData
    {
        public List<string> labels { get; set; }
        public List<BarChartDataset> datasets { get; set; }
        public string label { get; set; }
    }

    public class AllChartDto
    {
        public List<BarChartData> barChartData { get; set; }
        public List<PieChartData> pieChartData { get; set; }
        public List<ProjectWiseDashboardData> projectWiseDashboardDatas { get; set; }

        public List<ProgramManagerDashboardData> ProgramManagerDashboardDatas { get; set; }
        public List<BarChartData> ProgramManagerBarChart { get; set; }
        public List<BarChartData> ProgramManagerProgramStateWiseBudgetBarChart { get; set; }
        public List<BarChartData> ProgramManagerProgramYearwiseStateWiseBudgetBarChart { get; set; }
        public List<statuswisecount> statuswisecounts { get; set; }
        public List<ProjectWiseYearlyClustor> projectWiseYearlyClustors { get; set; }
        public List<ProjectWiseYearlyclustorinfo> projectWiseYearlyclustorinfo { get; set; }
        public List<notificationdto> notificationdtos { get; set; }
        public List<ProgramManagerImplemenationPlanCheckListWiseBudget> programManagerImplemenationPlanCheckListWiseBudgets { get; set; }
        public List<ActionAReaRRCdto> actionAReaRRCdtos { get; set; }
        public List<SummryAllProjects> summryAllProjects { get; set; }
        public List<LatestActivityAssignReport> latestActivityAssignReports { get; set; }
        public List<budgetachievmentdto> budgetachievmentdtos { get; set; }
        public List<totalbudgettotalexpense> totalbudgettotalexpenses { get; set; }
    }   

    public class SummryAllProjects
    {
        public string ProgramName { get; set; }
        public string ProjectStatus { get; set; }
    }
    public class LatestActivityAssignReport
    {
        public string ProgramName { get; set; }
        public string ActivityName { get; set; }
        public string SubActivityName { get; set; }
        public string AssignTo{ get; set; }
        public string ProjectStatus { get; set; }

    }

    public class BudgetVSActual
    {
        public string ProgramName { get; set; }
        public int PID { get; set; }
        public decimal? Budget { get; set; }
        public decimal? ActualPerformance { get; set; }


    }

    public class PieChartDataset
    {
        public List<string> backgroundColor { get; set; }
        public List<string> borderColor { get; set; }
        public List<decimal> data { get; set; }
        public List<string> labels { get; set; }
        public List<PieChartDataset> datasets { get; set; }
    }

    public class PieChartData
    {
        public List<string> labels { get; set; }
        public List<PieChartDataset> datasets { get; set; }
        public List<decimal> data { get; set; }

    }


    // Program Manager.

    public class ProgramManagerDashboardData
    {
        public List<ProgramManagerStateWiseBudget> ProgramManagerStateWiseBudgets { get; set; }
        public List<ProgramManagerProgramStateWiseBudget> ProgramManagerProgramStateWiseBudgets { get; set; }
        public List<ProgramManagerProgramYearwiseStateWiseBudget> ProgramManagerProgramYearwiseStateWiseBudgets { get; set; }
        public List<ProgramManagerWiseProjectManagerCount> ProgramManagerWiseProjectManagerCounts { get; set; }
        public List<ProgramManagerImplemenationPlanCheckListWiseBudget> ProgramManagerImplemenationPlanCheckListWiseBudgets { get; set; }
        public List<StateWiseDemand> StateWiseDemands { get; set; }
        public List<DemandWiseProject> DemandWiseProjects { get; set; }
        public List<ProgramManagerAmountSanction> ProgramManagerAmountSanctions { get; set; }
        public List<ProgramManagerDashboardInfo> ProgramManagerDashboardInfos { get; set; }
        public List<ProgramManagerWiseTeamMemberInfo> ProgramManagerWiseTeamMemberInfos { get; set; }
        public List<ProgramManagerWiseProjectList> ProgramManagerWiseProjectLists { get; set; }
        public List<ManagerwiseWiseDemand> managerwiseWiseDemands { get; set; }

        public DataTable dataTable { get; set; }
    }

    public class ProgramManagerStateWiseBudget
    {
        public string StateName { get; set; }
        public decimal FunderContribution { get; set; }
        public decimal OtherContribution { get; set; }
        public decimal CommunityContribution { get; set; }
        public decimal Expense { get; set; }
        public int TotalProject { get; set; }
    }
    public class ProgramManagerProgramStateWiseBudget
    {
        public string StateName { get; set; }
        public string ProjectName { get; set; }
        public decimal FunderContribution { get; set; }
        public decimal CommunityContribution { get; set; }
        public decimal OtherContribution { get; set; }
        public decimal Expense { get; set; }
        public string Projectid { get; set; }


    }

    public class ProgramManagerProgramYearwiseStateWiseBudget
    {
        public string StateName { get; set; }
        public string CostEstimationYear { get; set; }
        public string ProjectName { get; set; }
        public decimal FunderContribution { get; set; }
        public decimal CommunityContribution { get; set; }
        public decimal OtherContribution { get; set; }
        public string Component { get; set; }
    }

    public class statuswisecount
    {
        public string ProjectStatus { get; set; }
        public long statuscount { get; set; }
    }
    public class ProgramManagerWiseProjectManagerCount
    {
        public int ProjectManagerCount { get; set; }
    }

    public class ProgramManagerImplemenationPlanCheckListWiseBudget
    {
        public string CostEstimationYear { get; set; }
        public string Activity { get; set; }
        public string SubActivity { get; set; }
        public string AssignedTo { get; set; }
        public DateTime CheckListStartDate { get; set; }
        public DateTime CheklistEndDate { get; set; }
        public string Status { get; set; }
        public List<ActionAReaRRCdto> actionAReaRRCdtos { get; set; }
    }

    public class StateWiseDemand
    {
        public string StateName { get; set; }
        public decimal Demand { get; set; }
        public int StateId { get; set; }
    }
    public class ManagerwiseWiseDemand
    {
        public string ProgramName { get; set; }
        public decimal Demand { get; set; }
        public int ProgramId { get; set; }
    }


    public class DemandWiseProject
    {
        public string StateName { get; set; }
        public string ProjectName { get; set; }
        public decimal Demand { get; set; }
        public decimal DemandRemaining { get; set; }
        public int ProgramId { get; set; }
        public int StateId { get; set; }

    }

    public class ProgramManagerAmountSanction
    {
        public string StateName { get; set; }
        public int ProgramId { get; set; }
        public string ProgramName { get; set; }
        public DateTime Date { get; set; }
        public decimal AmountSanction { get; set; }
    }

    public class ProgramManagerDashboardInfo
    {
        public int TotalProject { get; set; }
        public decimal FunderContribution { get; set; }
        public decimal TotalAchivement { get; set; }
        public int TeamMember { get; set; }
    }

    public class ProgramManagerWiseTeamMemberInfo
    {
        public string Staff_Name { get; set; }
        public string MobileNumber { get; set; }
        public string EmailAddress { get; set; }
        public string Designation { get; set; }
    }

    public class ProgramManagerWiseProjectList
    {
        public string ProjectName { get; set; }
        public string ProjectManagerName { get; set; }
    }
    public class notificationdto
    {
        public string message { get; set; }
        public DateTime timeStamp { get; set; }
        public string name { get; set; }

    }
    public class usernamesdto
    {
        public string usernamess { get; set; }
    }

    
}

public class Rootobject
{
    public string message { get; set; }
    public string timeStamp { get; set; }
    public Usermodel userModel { get; set; }
}

public class Usermodel
{
    public string name { get; set; }
    public string photo_profile { get; set; }
}
public class budgetachievmentdto
{
    public string ProgramName { get; set; }
    public decimal FunderContribution { get; set; }
    public decimal ExpenseAmount { get; set; }


}
public class totalbudgettotalexpense
{
    public string ProgramName { get; set; }
    public int PID { get; set; }

    public decimal FunderContribution { get; set; }
    public decimal CommunityContribution { get; set; }
    public decimal Othercontribution { get; set; }
    public decimal ExpenseAmount { get; set; }
    public decimal Communitycontributionreceived { get; set; }
    public decimal Govcontributionreceived { get; set; }
    public decimal TotalBalanceamount { get; set; }
    public decimal TotalBalanceCommunity { get; set; }


}

public class clustorwisetotalbudgettotalexpense
{
    public string ProgramName { get; set; }
    public string ClustorName { get; set; }
    public int? LocationId { get; set; }
    public int? VillageId { get; set; }
    public string VilageName { get; set; }
    public int PID { get; set; }

    public decimal FunderContribution { get; set; }
    public decimal CommunityContribution { get; set; }
    public decimal Othercontribution { get; set; }
    public decimal ExpenseAmount { get; set; }
    public decimal Communitycontributionreceived { get; set; }
    public decimal Govcontributionreceived { get; set; }
    public decimal TotalBalanceamount { get; set; }
    public decimal TotalBalanceCommunity { get; set; }
}
public class ActionAReaRRCdto
{
    public string ProgramName { get; set; }
    public decimal FunderContribution { get; set; }
    public string ActionAreaName { get; set; }
    public string Subactionareaname { get; set; }
    public decimal Budget { get; set; }

}
public class RRCManagerAmountSanction
{
    public int ProgramId { get; set; }
    public string ProgramName { get; set; }
    public DateTime Date { get; set; }
    public decimal AmountSanction { get; set; }
    public decimal RemainingAmount { get; set; }

}

public class DemandRemaningAmount
{
    public string ProgramName { get; set; }
    public decimal DemandRemainingAmount { get; set; }

}
