﻿using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.PrgRequestAAPlas.Dto;

namespace WOTR.PM.PrgRequestAAPlas
{
   public  interface IRequestApplsRepository
    {
        List<RequestAAPlasListDto> GetRquestApplData(int? TenantId, int programId);


    }
}
