﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Locations
{
    public enum LocationType
    {
        HQ = 1,
        RRC,
        VillageCluster,
        Village
    }
}
