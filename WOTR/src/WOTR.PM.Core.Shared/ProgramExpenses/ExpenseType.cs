﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.ProgramExpenses
{
    public enum ExpenseType
    {
        Work=1,
        Travel,
      
        ADVANCE,
        Other,
        LabourPayment,
        MachinePayment, 
        MaterialPayment, 
        MiscalleneousPayment, 
        SupervisionPayment, 

    }
}
