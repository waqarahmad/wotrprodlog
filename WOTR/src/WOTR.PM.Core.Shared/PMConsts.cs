﻿namespace WOTR.PM
{
    public class PMConsts
    {
        public const string LocalizationSourceName = "PM";

        public const string ConnectionStringName = "Default";

        // public const bool MultiTenancyEnabled = true;
        public const bool MultiTenancyEnabled = false;

        public const int PaymentCacheDurationInMinutes = 30;
    }
}