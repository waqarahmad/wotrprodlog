﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.PrgImplementationPlans;

namespace WOTR.PM.Components
{
  public  class SubActivityForAndri : FullAuditedEntity
    {
        
            public string SubActivityname { get; set; }
            public string Unitofmeasure { get; set; }
            public decimal? unit { get; set; }
            public decimal? AchieveUnits { get; set; }
        public decimal? RemainingBalance { get; set; }
        public decimal? TotalCost { get; set; }
        public string VillageName { get; set; }
        public int? VillageId { get; set; }

        public DateTime startDate { get; set; }
            public DateTime EndDate { get; set; }
            public string Description { get; set; }
            public SubActivityStatus status { get; set; }

        public int ManagerId { get; set; }
        public string ManagerName { get; set; }
        public int? ProgramID { get; set; }
        public string CostEstimationYear { get; set; }
        public int? QuarterID { get; set; }
        public int? PrgActionAreaActivityMappingID { get; set; }
        public int ProgramQuqterUnitMappingID { get; set; }
        public string AssingtoUser { get; set; }
        public int SubActivityId { get; set; }


    }
}
