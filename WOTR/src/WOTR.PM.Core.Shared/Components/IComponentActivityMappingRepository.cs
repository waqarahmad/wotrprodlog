﻿using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.PrgActionAreaActivitysMappings;

namespace WOTR.PM.Components
{
    public interface IComponentActivityMappingRepository
    {
        List<GetComponentActivityMappingListDto> GetComponentActivityMappingDetils(int? TenantId, long? userId);

        List<GetComponentActivityMappingListDto> GetComponentandActivityForProgram(int? TenantId, long? UserID, int programId);
        List<ProgrameComponentListDto> GetComponentandActivityForCostEstimationforallprograms(int? TenantId,long? UserID);

        List<ProgrameComponentListDto> GetComponentandActivityForCostEstimation(int? TenantId, long? UserID, int programId);
        List<ProgrameComponentListDto> GetOverallRegionSpendingdetailsCostEstimation(int? TenantId, long? UserID, int programId,string Year);

        List<loginUserDetailsDto> GetLoginInfo(string mobilenumber, string name, int TenantId);
        List<SubActivityForAndri> GetSubActivityForAndri(int userId, int TenantId, int programId, int PrgActionAreaActivityMappingID, string role);
        List<unitOFmeasuresDtoList> GetUnitofmeasures(int TenantId);
       // List<ProgrameComponentListDto> GetOverallactionplan(int? TenantId, long? UserID, int programId);
        void UpdatesUnit();
    }
}
