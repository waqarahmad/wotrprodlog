﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Components
{
  public   class loginUserDetailsDto :FullAuditedEntity
    {
       
        public string EmailAddress { get; set; }
        public Boolean isActive { get; set; }
        public string FullName { get; set; }
      
        public string PhoneNumber { get; set; }
        public int? TenantId { get; set; }
      
        public string Address { get; set; }
        public string UserRole { get; set; }
        public string LocationName { get; set; }
        public Guid ProfilePictureId { get; set; }
        public string image { get; set; }
    }
}
