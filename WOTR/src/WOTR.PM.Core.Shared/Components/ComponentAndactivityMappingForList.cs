﻿using Abp.Domain.Entities.Auditing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.ProgramRegionSpendings;

namespace WOTR.PM.Components
{
    public class ComponentAndactivityMappingForList : FullAuditedEntity
    {
        public int TenantId { get; set; }
        public int ComponetTableId { get; set; }
        public string ActivityName { get; set; }
        public string ActivityDescri { get; set; }
        public string ComponentActivityMappingCreationTime { get; set; }
        public int ActivityID { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ActivityLevel ActivityLevel { get; set; }
        public String FullName { get; set; }
        public string FirstName { get; set; }
        public string CostEstimationYear { get; set; }
        public int ProgramCostEstimate_ProgramID { get; set; }
        public int? ProgramCostEstimate_ComponentID { get; set; }
        public int? ProgramCostEstimate_ActivityID { get; set; }
        public int? ProgramCostEstimate_UnitOfMeasuresID { get; set; }
        public decimal? ProgramCostEstimate_TotalUnits { get; set; }
        public decimal? ProgramCostEstimate_UnitCost { get; set; }
        public decimal? ProgramCostEstimate_TotalUnitCost { get; set; }
        public decimal? ProgramCostEstimate_CommunityContribution { get; set; }
        public decimal? ProgramCostEstimate_FunderContribution { get; set; }
        public decimal? ProgramCostEstimate_OtherContribution { get; set; }
        public decimal? ProgramCostEstimate_Total { get; set; }
        public string ActionArea { get; set; }
        public string SubAction { get; set; }
        public string Region { get; set; }
        public string ProgramName { get; set; }
        public List<Demo> ProgramNameList { get; set; }
    }

    public class Demo
    {
        public string ProgramName { get; set; }
        public List<string> ProgramNameList { get; set; }
        //public List<ComponentAndactivityMappingForList> ProgramNameList { get; set; }
    }
}
