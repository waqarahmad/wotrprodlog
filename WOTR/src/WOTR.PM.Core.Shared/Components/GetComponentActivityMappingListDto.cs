﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Components
{
   public  class GetComponentActivityMappingListDto : FullAuditedEntity
    {
        public int TenantId { get; set; }
        public int ComponetTableId { get; set; }
        public string ComponentCode { get; set; }
        public string componentName { get; set; }
        public string Description { get; set; }
        public long code { get; set; }
        public int ActivityCount { get; set; }
        public int ActivityID { get; set; }//....Activity ID
        public string ComponentMappingID { get; set; } //...Component ID
        public DateTime ComponentCreatime { get; set; }
        public string FullName { get; set; }
        public string ActivityName { get; set; }
        public string ActivityDescri { get; set; }
        public DateTime ComponentActivityMappingCreationTime { get; set; }

        public List<ComponentAndactivityMappingForList> Activity { get; set; }       
       
    }
}
