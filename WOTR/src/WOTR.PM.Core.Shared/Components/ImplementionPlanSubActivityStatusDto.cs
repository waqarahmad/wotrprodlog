﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Components
{
   public  enum ImplementionPlanSubActivityStatusDto
    {
        NotStarted=1,
        InProgress,
        Completed,
        Overdue
    }
}
