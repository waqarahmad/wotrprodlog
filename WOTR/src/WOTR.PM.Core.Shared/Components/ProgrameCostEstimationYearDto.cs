﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Components
{
    public class ProgrameCostEstimationYearDto : FullAuditedEntity
    {
        public string CostEstimationYear { get; set; }

        public decimal? SubTotalTotalunits { get; set; }
        public decimal? SubTotalUnitCost { get; set; }
        public decimal? SubTotalofTotalCost { get; set; }
        public decimal? SubTotalofCommunityContribution { get; set; }
        public decimal? SubTotalofFunderContribution { get; set; }
        public decimal? SubTotalofOtherContribution { get; set; }
        public int ComponetTableId { get; set; }
        public string ActionArea { get; set; }
        public string SubAction { get; set; }
        public string Region { get; set; }
        public List<ComponentAndactivityMappingForList> Activity { get; set; }
    }
}
