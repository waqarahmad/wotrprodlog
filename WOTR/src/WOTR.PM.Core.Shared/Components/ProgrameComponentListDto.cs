﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Components
{
    public class ProgrameComponentListDto : FullAuditedEntity
    {
        public int TenantId { get; set; }
        public int ComponetTableId { get; set; }
        public int ProgrameId { get; set; }
        public string ComponentCode { get; set; }
        public string componentName { get; set; }
        public DateTime ComponentCreatime { get; set; }
        public string Description { get; set; }
        public DateTime ProgrameStartDate { get; set; }
        public DateTime ProgrameEndDate { get; set; }

        public decimal? TotalUnits { get; set; }
        public decimal? TotalUnitsCost { get; set; }
        public decimal? TotalCost { get; set; }

        public List<ProgrameCostEstimationYearDto> CostEstimationYear { get; set; }
        //public string ActionArea { get; set; }
        //public string ProjectGrant { get; set; }
        //public string ProjectCBOs { get; set; }
        //public string LocalContribution { get; set; }
        //public string GovtContribution { get; set; }
        public List<string> ActionArea { get; set; }
        public List<string> ProjectGrant { get; set; }
        public List<string> ProjectCBOs { get; set; }
        public List<string> LocalContribution { get; set; }
        public List<string> GovtContribution { get; set; }


    }
}
