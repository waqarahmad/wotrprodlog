﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.PrgImplementationPlans
{
   public enum   SubActivityStatus
    {
        NotStarted = 1,
        InProgress,
        Completed,
        Overdue
    }
}
