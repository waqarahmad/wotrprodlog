﻿using Abp.Domain.Entities.Auditing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.PrgQuarterUnits;
using WOTR.PM.PrgRequestAAPlas;
using WOTR.PM.ProgramRegionSpendings;

namespace WOTR.PM.PrgImplementationPlans.Dto
{
    public class ImplementationListDto : FullAuditedEntity
    {
        public int ProgramID { get; set; }
        public string ProgramName { get; set; }
        public int? LocationID { get; set; }
        public string LocationName { get; set; }
        public string ManagerName { get; set; }
        public long ManagerID { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public RequestStatus RequestStatus { get; set; }
        public DateTime ProgrameStartDate { get; set; }
        public DateTime ProgrameEndDate { get; set; }
        public decimal? CommunityContributionRecived { get; set; }
        public decimal? GovernmentContributionRecived { get; set; }
        public List<PrgCostEstimationYearDto> CostEstimationYear { get; set; }
    }
    public class PrgCostEstimationYearDto:FullAuditedEntity
    {
        public string CostEstimationYear { get; set; }

        public decimal? SubTotalTotalunits { get; set; }
        public decimal? SubTotalUnitCost { get; set; }
        public decimal? SubTotalofTotalCost { get; set; }
        public decimal? SubTotalofCommunityContribution { get; set; }       
        public decimal? SubTotalofFunderContribution { get; set; }
        public decimal? SubTotalofOtherContribution { get; set; }       

        public List<ListActionSubactionListImplemenTationDto> ActionSubactionCost { get; set; }
    }

    public class ListActionSubactionListImplemenTationDto : FullAuditedEntity
    {
        public string ActionAreaName { get; set; }
        public string SubActionAreaName { get; set; }
        public string ActivityName { get; set; }
        public int? ActivityId { get; set; }
        public int? RemainingSubActivity { get; set; }
        public int ProgramID { get; set; }
        public int QuarterId { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ActivityLevel ActivityLevel { get; set; }
        public int? VillageID { get; set; }
        public string VillageName { get; set; }
        public string UnitOfMeasuresName { get; set; }
        public int? UnitOfMeasuresID { get; set; }
        public int? LocationID { get; set; }
        public int? ChecklistID { get; set; }
        public int? PrgActionAreaActivityMappingID { get; set; }
        public long? TotalUnits { get; set; }
        public RequestStatus RequestStatus { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ProgramQuarterUnitMappingStatus ProgramQuarterUnitMappingStatus { get; set; }
        public int UnitId { get; set; }
        public string CostEstimationYear { get; set; }
        public decimal? UnitCost { get; set; }

        public decimal? TotalUnitCost { get; set; }

        public decimal? CommunityContribution { get; set; }

        public decimal? CommunityContributionRecived { get; set; }

        public decimal? GovernmentContributionRecived { get; set; }

        public decimal? FunderContribution { get; set; }

        public decimal? OtherContribution { get; set; }

        public int? Quarter1PhysicalUnits { get; set; }      //..Quarter1
        public decimal Quarter1FinancialRs { get; set; }

        public int? Quarter2PhysicalUnits { get; set; }         //..Quarter2
        public decimal Quarter2FinancialRs { get; set; }

        public int? Quarter3PhysicalUnits { get; set; }         //..Quarter3
        public decimal Quarter3FinancialRs { get; set; }

        public int? Quarter4PhysicalUnits { get; set; }             //..Quarter4
        public decimal Quarter4FinancialRs { get; set; }

        public List<subUnitActivityQuarterWise> subUnitActivityQuarterWise { get; set; }
        public List<ListActionSubactionListImplemenTationDto> Quarter1WisePlan { get; set; }
        public List<ListActionSubactionListImplemenTationDto> Quarter2WisePlan { get; set; }
        public List<ListActionSubactionListImplemenTationDto> Quarter3WisePlan { get; set; }
        public List<ListActionSubactionListImplemenTationDto> Quarter4WisePlan { get; set; }
        public List<SubActivityandIteams> SubActivityandIteams { get; set; }
        public List<PrgManagerImplentationListDto> Managers { get; set; }
    }
    public class subUnitActivityQuarterWise : FullAuditedEntity
    {    
       
        public int? PrgActionAreaActivityMappingID { get; set; }   
        public string activityname { get; set; }
        public int? ChecklistID { get; set; }       
        public int? ChecklistItemID { get; set; }
        public string ChecklistItemName { get; set; }
        public int ProgramID { get; set; }
        public string AssignedUserName { get; set; }
        public long? AssingUserId { get; set; }  
        public string UnitOfMeasureName { get; set; }
        public int? UnitOfMeasuresID { get; set; }
        public decimal? AchieveUnits { get; set; }
        public decimal? Units { get; set; }
        public string Description { get; set; }
        public int? IteamLocationId { get; set; }
        public string CostEstimationYear { get; set; }
        public DateTime CheklistStartDate { get; set; }
        public DateTime CheklistEndDate { get; set; }
        public int? QuarterID { get; set; }
        public int ImplementationPlanCheckListID { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public SubActivityStatus SubActivityStatus { get; set; }
        public string ManagerName { get; set; }
        public int? ProgramQuqterUnitMappingID { get; set; }
        public decimal? RemainingBalance { get; set; }
        public string LocatioName { get; set; }
        public string VillageName { get; set; }

    }
    public class subUnitActivityQuarterWisedetails
    {
        public List<subUnitActivityQuarterWise> subUnitActivityQuarterWises1 { get; set; }
        public List<subUnitActivityQuarterWise> subUnitActivityQuarterWises2 { get; set; }
        public List<subUnitActivityQuarterWise> subUnitActivityQuarterWises3 { get; set; }
        public List<subUnitActivityQuarterWise> subUnitActivityQuarterWises4 { get; set; }



    }

    public class SubActivityandIteams : FullAuditedEntity
    {
        public string ChecklistName { get; set; }
        public string Description { get; set; }
        public int? ChecklistID { get; set; }
        public int? Rating { get; set; }
        public virtual int TenantId { get; set; }
        public int ActivityID { get; set; }
        public string ActivityName { get; set; }

        public int ProgramID { get; set; }//for Mapping table
        public int? PrgActionAreaActivityMappingID { get; set; }//for Mapping table

        public List<cheklistandSublistDto1> iteam { get; set; }
        public List<cheklistandSublistDto1> Selectediteams { get; set; }
    }
    public class cheklistandSublistDto1 : FullAuditedEntity
    {
        public string CheklisiteamtName { get; set; }
        public int? checklistID_subiteamTable { get; set; }
    }

    public class PrgManagerImplentationListDto
    {
        public string Name { get; set; }
        public long ManagerId { get; set; }
        public string ManaggerLocation { get; set; }
    }

    public class QuestionaryListDto
    {
        public string Question { get; set; }
      //  public string Answer { get; set; }
        public string InputType { get; set; }
        public int QuestionId { get; set; }
      //  public List<string> QuestionaryUrl { get; set; }
    }

    public class  ImplimentaionChecklistDto : FullAuditedEntity
    {
        public int TenantId { get; set; }
        public int ProgramID { get; set; }
        public int? ChecklistID { get; set; }
        public int? ChecklistItemID { get; set; }
        public int QuaterId { get; set; }

        public int? ProgramLocationId { get; set; }
        public int? PrgActionAreaActivityMappingID { get; set; }
        public int? ProgramQuqterUnitMappingID { get; set; }


        public int? LocationId { get; set; }
        public long? AssingUserId { get; set; }
        public int? UnitOfMeasuresID { get; set; }
        public decimal? Units { get; set; }
        public string Description { get; set; }
        public int? IteamLocationId { get; set; }
        public string CostEstimationYear { get; set; }
        public DateTime CheklistStartDate { get; set; }
        public DateTime CheklistEndDate { get; set; }
        public SubActivityStatus status { get; set; }

        public string ChekListIteamName { get; set; }
        public string FullName { get; set; }
        public string unitName { get; set; }
    }
    public class Year
    {
        public List<costYear> costYears { get; set; }
    }
    public class costYear
    {
        public string CostYears { get; set; }
        public string Years { get; set; }
    }
}
