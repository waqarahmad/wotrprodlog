﻿using Abp.Domain.Entities.Auditing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.PrgRequestAAPlas;
using WOTR.PM.ProgramRegionSpendings;

namespace WOTR.PM.ProgramFunds.Dto
{

    public class ReturnProgramFundDetails : FullAuditedEntity
    {
        public int ProgramID { get; set; }
        public string ProgramName { get; set; }
        public int? LocationID { get; set; }
        public string LocationName { get; set; }
        public string ManagerName { get; set; }
        public long? ProgramManagerID { get; set; }
        public string ProgramManagerName { get; set; }
        public long? ProjectManagerID { get; set; }
        public long ManagerID { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public RequestStatus RequestStatus { get; set; }
        public DateTime ProgrameStartDate { get; set; }
        public DateTime ProgrameEndDate { get; set; }
        public decimal? OpneninBalanceAmmount { get; set; }
        public DateTime FundReleaseDate { get; set; }
        public List<PrgFundsYearDto> CostEstimationYear { get; set; }
        public List<PrgProgramManagerListDto> Managers { get; set; }
        public List<prgFundDetails> prgFundDetails { get; set; }
        public List<prgFundDetails> FundRelease { get; set; }
    }


    public class ProgramFundsListDto : FullAuditedEntity
    {
        public int ProgramID { get; set; }
        public string ProgramName { get; set; }
        public int? LocationID { get; set; }
        public string LocationName { get; set; }
        public string ManagerName { get; set; }  
        public long? ProgramManagerID { get; set; }
        public string ProgramManagerName { get; set; }
        public decimal? DemandAmount { get; set; }
        //public long? ProjectManagerID { get; set; }
        public long ManagerID { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public RequestStatus RequestStatus { get; set; }
        public DateTime ProgrameStartDate { get; set; }
        public DateTime ProgrameEndDate { get; set; }
        public decimal? OpneninBalanceAmmount { get; set; }
        public DateTime FundReleaseDate { get; set; }       
        public List<PrgFundsYearDto> CostEstimationYear { get; set; }
        public List<PrgProgramManagerListDto> Managers { get; set; }
        public List<prgFundDetails> prgFundDetails { get; set; }
        public List<prgFundDetails> FundRelease { get; set; }
        public decimal? ReleasedAmount { get; set; }

    }

    public class prgFundDetails : FullAuditedEntity {
        public string ProgramName { get; set; }
        public string ProgramManagerName { get; set; }
        public string ProjectManagerName { get; set; }
        public long? ProjectManagerID { get; set; }
        public DateTime DemandDate { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public decimal? DemandAmount { get; set; }
        public decimal? ReleaseAmount { get; set; }
        public List<prgFundDetails> ReleaseList { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Status1 Status1 { get; set; }
        public int ProgramID { get; set; }
        public DateTime ProgrameStartDate { get; set; }
        public DateTime ProgrameEndDate { get; set; }
        public decimal? UnitCost { get; set; }
    }

    public class PrgFundsYearDto : FullAuditedEntity
    {
        public string CostEstimationYear { get; set; }

        public decimal? SubTotalTotalunits { get; set; }
        public decimal? SubTotalUnitCost { get; set; }
        public decimal? SubTotalofTotalCost { get; set; }
        public decimal? SubTotalofCommunityContribution { get; set; }
        public decimal? SubTotalofFunderContribution { get; set; }
        public decimal? SubTotalofOtherContribution { get; set; }

        public List<ListActionSubactionListImplemenTationDto1> ActionSubactionCost { get; set; }
        public List<ListViewDeatilsDto> ActionSubactionCost1 { get; set; }
    }

    public class ListActionSubactionListImplemenTationDto1 : FullAuditedEntity
    {
        public string ActionAreaName { get; set; }
        public string SubActionAreaName { get; set; }
        public string ActivityName { get; set; }
        public int? ActivityId { get; set; }
        public int ProgramID { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ActivityLevel ActivityLevel { get; set; }
        public int? VillageID { get; set; }
        public string VillageName { get; set; }
        public string UnitOfMeasuresName { get; set; }
        public int? UnitOfMeasuresID { get; set; }
        public string ManagerName { get; set; }
        public long? ProgramManagerID { get; set; }
        public string ProgramManagerName { get; set; }
        public long? ManagerID { get; set; }

        public int? LocationID { get; set; }
        public int? ChecklistID { get; set; }
        public int? PrgActionAreaActivityMappingID { get; set; }
        public long? TotalUnits { get; set; }
        public RequestStatus RequestStatus { get; set; }
        public int UnitId { get; set; }
        public string CostEstimationYear { get; set; }

        public long? AchivementUnits { get; set; }
        public decimal? Expenditure { get; set; }
        public long? BalanceUnits { get; set; }
        public decimal? BalanceAmmount { get; set; }

        public int DemandUnits { get; set; }
        public decimal? DemandAmmount { get; set; }
        public decimal? UnitCost { get; set; }

        public decimal? TotalUnitCost { get; set; }

        public decimal? CommunityContribution { get; set; }

        public decimal? FunderContribution { get; set; }

        public decimal? OtherContribution { get; set; }

        public int? Quarter1PhysicalUnits { get; set; }      //..Quarter1
        public decimal Quarter1FinancialRs { get; set; }

        public int? Quarter2PhysicalUnits { get; set; }         //..Quarter2
        public decimal Quarter2FinancialRs { get; set; }

        public int? Quarter3PhysicalUnits { get; set; }         //..Quarter3
        public decimal Quarter3FinancialRs { get; set; }

        public int? Quarter4PhysicalUnits { get; set; }             //..Quarter4
        public decimal Quarter4FinancialRs { get; set; }
    }

    public class ListViewDeatilsDto : FullAuditedEntity
    {
        public string ActivityName { get; set; }
        public decimal? TotalUnit { get; set; }

        public int? Quarter1PhysicalUnits { get; set; }      //..Quarter1
        public decimal Quarter1FinancialRs { get; set; }
        public int? Quarter2PhysicalUnits { get; set; }         //..Quarter2
        public decimal Quarter2FinancialRs { get; set; }
        public int? Quarter3PhysicalUnits { get; set; }         //..Quarter3
        public decimal Quarter3FinancialRs { get; set; }
        public int? Quarter4PhysicalUnits { get; set; }             //..Quarter4
        public decimal Quarter4FinancialRs { get; set; }

        public decimal? UnitCost { get; set; }     
        public decimal? TotalUnitCost { get; set; }
        public int DemandUnits { get; set; }
        public decimal? DemandAmmount { get; set; }
        public decimal? subTotalTotalunits { get; set; }
        public decimal? subTotalUnitCost { get; set; }
        public decimal? subTotalTotalUnitCost { get; set; }
        public decimal? subTotalDemandUnits { get; set; }
        public decimal? subTotalDemandAmmount { get; set; }
        public decimal? subTotalQuarter1PhysicalUnits { get; set; }
        public decimal? subTotalQuarter1FinancialRs { get; set; }
        public decimal? subTotalQuarter2PhysicalUnits { get; set; }
        public decimal? subTotalQuarter2FinancialRs { get; set; }
    }

    public class PrgProgramManagerListDto
    {
        public string Name { get; set; }
        public long? ManagerId { get; set; }
        public string ManaggerLocation { get; set; }
        public string Status1 { get; set; }
    }

}
