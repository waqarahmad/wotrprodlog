﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.ProgramFunds
{
   public class ProgramFundDto
    {
        public int ID { get; set; }

        public string ProgramName { get; set; }

        public string ProgramManagerName { get; set; }

        public string ProjectManagerName { get; set; }

        public Decimal? demandAmount { get; set; }

        public Decimal? releasedAmount { get; set; }

        public DateTime? releasedDate { get; set; }
        
        public long? ProjectManagerID { get; set; }

        public Status1 Status { get; set; }
    }
}
