﻿using Abp.AutoMapper;
using WOTR.PM.Organizations.Dto;

namespace WOTR.PM.Models.Users
{
    [AutoMapFrom(typeof(OrganizationUnitDto))]
    public class OrganizationUnitModel : OrganizationUnitDto
    {
        public bool IsAssigned { get; set; }
    }
}