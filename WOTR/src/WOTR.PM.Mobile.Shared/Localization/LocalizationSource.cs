﻿namespace WOTR.PM.Localization
{
    public enum LocalizationSource : byte
    {
        RemoteTranslation = 1,
        LocalTranslation = 2,
        NoTranslation = 3
    }
}