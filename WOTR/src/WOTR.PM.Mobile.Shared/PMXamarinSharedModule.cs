﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace WOTR.PM
{
    [DependsOn(typeof(PMClientModule), typeof(AbpAutoMapperModule))]
    public class PMXamarinSharedModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Localization.IsEnabled = false;
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PMXamarinSharedModule).GetAssembly());
        }
    }
}