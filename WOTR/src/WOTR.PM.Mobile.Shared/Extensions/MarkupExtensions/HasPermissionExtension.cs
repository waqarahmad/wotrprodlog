﻿using System;
using WOTR.PM.Core;
using WOTR.PM.Core.Dependency;
using WOTR.PM.Services.Permission;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WOTR.PM.Extensions.MarkupExtensions
{
    [ContentProperty("Text")]
    public class HasPermissionExtension : IMarkupExtension
    {
        public string Text { get; set; }
        
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (ApplicationBootstrapper.AbpBootstrapper == null || Text == null)
            {
                return false;
            }

            var permissionService = DependencyResolver.Resolve<IPermissionService>();
            return permissionService.HasPermission(Text);
        }
    }
}