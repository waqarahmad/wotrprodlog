﻿namespace WOTR.PM.Services.Permission
{
    public interface IPermissionService
    {
        bool HasPermission(string key);
    }
}