﻿using System.Threading.Tasks;
using WOTR.PM.ApiClient.Models;

namespace WOTR.PM.Services.Account
{
    public interface IAccountService
    {
        AbpAuthenticateModel AbpAuthenticateModel { get; set; }
        AbpAuthenticateResultModel AuthenticateResultModel { get; set; }
        Task LoginUserAsync();
    }
}
