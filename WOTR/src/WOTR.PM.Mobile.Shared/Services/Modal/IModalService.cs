﻿using System.Threading.Tasks;
using WOTR.PM.Views;
using Xamarin.Forms;

namespace WOTR.PM.Services.Modal
{
    public interface IModalService
    {
        Task ShowModalAsync(Page page);

        Task ShowModalAsync<TView>(object navigationParameter) where TView : IXamarinView;

        Task<Page> CloseModalAsync();
    }
}
