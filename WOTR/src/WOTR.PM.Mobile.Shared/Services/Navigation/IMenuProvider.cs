using System.Collections.Generic;
using MvvmHelpers;
using WOTR.PM.Models.NavigationMenu;

namespace WOTR.PM.Services.Navigation
{
    public interface IMenuProvider
    {
        ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions);
    }
}