namespace WOTR.PM.Web.Models.TokenAuth
{
    public class ImpersonateResultModel
    {
        public string ImpersonationToken { get; set; }
    }
}