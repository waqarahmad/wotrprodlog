﻿using System.Threading.Tasks;
using WOTR.PM.Sessions.Dto;

namespace WOTR.PM.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
