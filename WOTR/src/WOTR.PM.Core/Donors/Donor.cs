﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.Donors
{
    public class Donor : FullAuditedEntity, IMustHaveTenant
    {   
        [Required]
        public virtual int TenantId { get; set; }
        [Required]
        public string CompanyName { get; set; }
        [Required]
        public string CompanyAddress { get; set; }
        [Required]
        public string CompanyContactNo { get; set; }
        [Required]
        public string CompanyEmailID { get; set; }
        [Required]
        public string CompanyAddressCountry { get; set; }


        public string FirstName { get; set; }
        public string FirstEmailAddress { get; set; }
        public string FirstAddress { get; set; }
        public string FirstContactNo { get; set; }
        public string FirstOtherEmail{ get; set; }

        
        public string SecondName { get; set; }
        public string SecondEmailAddress { get; set; }
        public string SecondAddress { get; set; }
        public string SecondContactNo { get; set; }
        public string SecondOtherEmail { get; set; }
        

        public string ThridName { get; set; }
        public string ThridEmailAddress { get; set; }
        public string ThridAddress { get; set; }
        public string ThridContactNo { get; set; }
        public string ThridOtherEmail { get; set; }


        public DateTime AmountReceivedDateTime { get; set; }

        [Column(TypeName = "decimal(20, 2)")]
        public decimal AmountReceived { get; set; }

    }
}