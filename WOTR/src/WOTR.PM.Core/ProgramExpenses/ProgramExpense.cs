﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Users;
using WOTR.PM.Checklists;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.PrgQuarterUnits;
using WOTR.PM.ProgramFunds;
using WOTR.PM.Programs;
using WOTR.PM.UnitOfMeasures;

namespace WOTR.PM.ProgramExpenses
{
  public   class ProgramExpense :FullAuditedEntity , IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("Program")]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }

        public string ExpensesYear { get; set; }

        [ForeignKey("PrgActionAreaActivityMapping")]     //...PrgActionAreaActivityMappingID
        public int? PrgActionAreaActivityMappingID { get; set; }
        public virtual PrgActionAreaActivityMapping PrgActionAreaActivityMapping { get; set; }

        public int QuarterId { get; set; }

        public string ExpenseTitle { get; set; }


        [ForeignKey("ExpensesType")]
        public int ExpensesTypeID { get; set; }
        public virtual ExpensesType ExpensesType { get; set; }

        
        public DateTime ExpenseDate { get; set; }
        public string Remark { get; set; }
        public int Unit { get; set; }
        public decimal Amount { get; set; }
        public string Image { get; set; }

        [ForeignKey("ProgramQuqterUnitMapping")]
        public int? ProgramQuqterUnitMappingID { get; set; }
        public virtual ProgramQuqterUnitMapping ProgramQuqterUnitMapping { get; set; }

        [ForeignKey("UnitOfMeasures")]
        public int? UnitOfMeasuresID { get; set; }
        public virtual UnitofMeasure UnitOfMeasures { get; set; }

        //[ForeignKey("MapSubActivityIteamsToImplementionPlan")]
        //public int? MapSubActivityIteamsToImplementionPlanID { get; set; }
        //public virtual MapSubActivityIteamsToImplementionPlan MapSubActivityIteamsToImplementionPlan { get; set; }

        [ForeignKey("ImplementationPlanCheckList")]
        public int? ImplementationPlanCheckListID { get; set; }
        public virtual ImplementationPlanCheckList ImplementationPlanCheckList { get; set; }

        public Status1 Status { get; set; }

        [ForeignKey("User")]                             //...UserID
        public long ManagerID { get; set; }
        public virtual User User { get; set; }


    }





   


}
