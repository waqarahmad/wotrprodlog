﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.ProgramExpenses
{
  public   class ProgramExpenseImage : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("ProgramExpense")]
        public int ProgramExpenseID { get; set; }
        public virtual ProgramExpense ProgramExpense { get; set; }

        public string image { get; set; }
        public Guid ImageId { get; set; }
    }
}
