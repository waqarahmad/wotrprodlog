﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.ProgramExpenses
{
   public  class ExpensesType :FullAuditedEntity , IMustHaveTenant
    {
        public virtual int TenantId { get; set; }
        public string TypeName { get; set; }
        public int ProgramId { get; set; }

    }
}
