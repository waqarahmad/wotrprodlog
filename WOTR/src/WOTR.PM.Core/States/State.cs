﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.States
{
    [Table("States")]
    public class State : Entity<int>
    {
        public string StateName { get; set; }
    }
}
