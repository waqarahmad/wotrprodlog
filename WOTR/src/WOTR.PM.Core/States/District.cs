﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.States
{
    public class District : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }


        public string DistrictName { get; set; }




        public virtual int StateId { get; set; }
        //public State State { get; set; }
    }
}
