﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.NewImpactIndicator;

namespace WOTR.PM.NewImpactIndicator
{
    [Table("IIQuestionary")]
    public class IIQuestionary : FullAuditedEntity
    {
        public string Question { get; set; }

        public string Description { get; set; }

        public string QuestionType { get; set; }

        public string QuestionStatus { get; set; }

        [ForeignKey("FrequencyOfoccurrenceId")]
        public int? FrequencyOfOccurrenceId { get; set; }
        public FrequencyOfoccurrence frequencyOfOccurrence { get; set; }

        [ForeignKey("ImpactIndicatorCategory")]
        public int? ImpactIndicatorCategoryId { get; set; }
        public ImpactIndicatorCategory impactIndicatorCategory { get; set; }


        [ForeignKey("ImpactIndicatorSubCategory")]
        public int? ImpactIndicatorSubCategoryId { get; set; }
        public ImpactIndicatorSubCategory impactIndicatorSubCategory { get; set; }


    }
}
