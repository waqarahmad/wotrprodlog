﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;


namespace WOTR.PM.NewImpactIndicator
{
    public class Source : FullAuditedEntity, IMustHaveTenant
    {
        public string SourceName { get; set; }
        public int TenantId { get; set; }
    }
}
