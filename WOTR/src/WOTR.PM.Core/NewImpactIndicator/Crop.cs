﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.NewImpactIndicator
{
    public class Crop : FullAuditedEntity, IMustHaveTenant
    {
        public string Name { get; set; }
        public int TenantId { get; set; }
    }
}
