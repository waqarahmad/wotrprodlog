﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.NewImpactIndicator
{
    [Table("IISubCategory")]
    public class ImpactIndicatorSubCategory : FullAuditedEntity, IMustHaveTenant
    {
        public string SubCategory { get; set; }
        
        [ForeignKey("ImpactIndicatorCategory")]                           
        public int? ImpactIndicatorCategoryId { get; set; }
        public virtual ImpactIndicatorCategory ImpactIndicatorCategory { get; set; }
        public int TenantId { get; set; }
    }
}
