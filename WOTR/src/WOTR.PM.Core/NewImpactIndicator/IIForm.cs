﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.NewImpactIndicator
{
    [Table("IIForm")]
    public class ImpactIndicatorForm : FullAuditedEntity
    {
        public string FormName { get; set; }

        public string FormStatus { get; set; }

    }
}
