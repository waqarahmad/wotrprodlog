﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Users;
using WOTR.PM.Locations;
using WOTR.PM.Programs;

namespace WOTR.PM.NewImpactIndicator
{
    public class IIFormResponse : FullAuditedEntity<long>, IMustHaveTenant
    {
        public int ProgramId { get; set; }
        public virtual Program program { get; set; }

        public int villageId { get; set; }
        public virtual Village village { get; set; }

        public long DataFillPersonId { get; set; }
        public virtual User DataFillPerson { get; set; }

        public int? impactIndicatorFormId { get; set; }
        public virtual ImpactIndicatorForm ImpactIndicatorForm { get; set; }

        
        public int TenantId { get; set; }

        public ICollection<IIFormAnswer> iIFormAnswers { get; set; }
    }
}
