﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.NewImpactIndicator
{
    public class IIFormAnswer : FullAuditedEntity<long>
    {
        public long IIFormResponseId { get; set; }
        public virtual IIFormResponse IIFormResponse { get; set; }

        public int FrequencyOfoccurrenceId { get; set; }
        public virtual FrequencyOfoccurrence FrequencyOfoccurrence { get; set; }

        public int IIQuestionaryId { get; set; }
        public virtual IIQuestionary IIQuestionary { get; set; }

        public string Answer { get; set; }

        public DateTime DateOfEntry { get; set; }

        public DateTime DateOfCollection { get; set; }

        public int QuestionType { get; set; }

        public int? CropId { get; set; }
        public virtual Crop Crop { get; set; }

        public int SourceId { get; set; }
        public virtual Source Source { get; set; }

    }
}
