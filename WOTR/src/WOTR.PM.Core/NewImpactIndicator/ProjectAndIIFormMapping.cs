﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Programs;

namespace WOTR.PM.NewImpactIndicator
{
    [Table("IIFormProgramMapping")]
    public class ProjectAndIIFormMapping :  FullAuditedEntity
    {
        [ForeignKey("impactIndicatorFormId")]
        public virtual int? impactIndicatorFormId { get; set; }
        public virtual ImpactIndicatorForm impactIndicatorForm { get; set; }

        [ForeignKey("ProgramId")]
        public int? programId { get; set; }
        public virtual Program program { get; set; }

        public string Status { get; set; }
    }
}
