﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.NewImpactIndicator
{
    [Table("IICategory")]
    public class ImpactIndicatorCategory : FullAuditedEntity, IMustHaveTenant
    {
        public string Category { get; set; }

        public int TenantId { get; set; }
    }
}
