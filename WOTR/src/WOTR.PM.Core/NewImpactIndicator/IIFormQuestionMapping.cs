﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace WOTR.PM.NewImpactIndicator
{
    [Table("IIFormQuestionMapping")]
    public class IIFormQuestionMapping : FullAuditedEntity
    {
        [ForeignKey("impactIndicatorFormId")]
        public virtual int? impactIndicatorFormId { get; set; }
        public virtual ImpactIndicatorForm impactIndicatorForm { get; set; }

        [ForeignKey("QuestionId")]
        public int? iIQuestionaryId { get; set; }
        public virtual IIQuestionary iIQuestionary { get; set; }

    }
}
