﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Users;
using WOTR.PM.ChekLists;
using WOTR.PM.Locations;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.Programs;
using WOTR.PM.UnitOfMeasures;

namespace WOTR.PM.PrgRequestAAPlas
{
    [Table("RequestAAplas")]
    public class RequestAAplas : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("Program")]                          //...ProgramID
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }


        [ForeignKey("Location")]                        //...LocationID
        public int? LocationID { get; set; }
        public virtual Location Location { get; set; }

        
        [ForeignKey("PrgActionAreaActivityMapping")]     //...PrgActionAreaActivityMappingID
        public int? PrgActionAreaActivityMappingID { get; set; }
        public virtual PrgActionAreaActivityMapping PrgActionAreaActivityMapping { get; set; }

        [ForeignKey("User")]                             //...UserID
        public long ManagerID { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("UnitOfMeasures")]                  //...UnitOfMeasuresID
        public int? UnitOfMeasuresID { get; set; }
        public virtual UnitofMeasure UnitOfMeasures { get; set; }

        [ForeignKey("Village")]                 
        public int? VillageID { get; set; }
        public virtual Village Village { get; set; }

        public long? TotalUnits { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal? TotalUnitCost { get; set; }

        public decimal? CommunityContribution { get; set; }

        public decimal? FunderContribution { get; set; }

        public decimal? OtherContribution { get; set; }

        public string CostEstimationYear { get; set; }

        public RequestStatus RequestStatus { get; set; }

        public int? Quarter1PhysicalUnits { get; set; }      //..Quarter1
        public decimal Quarter1FinancialRs { get; set; }

        public int? Quarter2PhysicalUnits { get; set; }         //..Quarter2
        public decimal Quarter2FinancialRs { get; set; }

        public int? Quarter3PhysicalUnits { get; set; }         //..Quarter3
        public decimal Quarter3FinancialRs { get; set; }

        public int? Quarter4PhysicalUnits { get; set; }             //..Quarter4
        public decimal Quarter4FinancialRs { get; set; }

        public decimal? CommunityContributionRecived { get; set; }
        public decimal? GovernmentContributionRecived { get; set; }
    }
}
