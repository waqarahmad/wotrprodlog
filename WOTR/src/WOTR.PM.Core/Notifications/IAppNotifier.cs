﻿using System.Threading.Tasks;
using Abp;
using Abp.Notifications;
using WOTR.PM.Authorization.Users;
using WOTR.PM.MultiTenancy;

namespace WOTR.PM.Notifications
{
    public interface IAppNotifier
    {
        Task WelcomeToTheApplicationAsync(User user);

        Task NewUserRegisteredAsync(User user);

        Task NewTenantRegisteredAsync(Tenant tenant);

        Task SendMessageAsync(UserIdentifier user, string message, NotificationSeverity severity = NotificationSeverity.Info);
    }
}
