using Abp;

namespace WOTR.PM
{
    /// <summary>
    /// This class can be used as a base class for services in this application.
    /// It has some useful objects property-injected and has some basic methods most of services may need to.
    /// It's suitable for non domain nor application service classes.
    /// For domain services inherit <see cref="PMDomainServiceBase"/>.
    /// For application services inherit PMAppServiceBase.
    /// </summary>
    public abstract class PMServiceBase : AbpServiceBase
    {
        protected PMServiceBase()
        {
            LocalizationSourceName = PMConsts.LocalizationSourceName;
        }
    }
}