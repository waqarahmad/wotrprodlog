﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.States;

namespace WOTR.PM.Locations
{
    /*Location Class refers to Location Master
      * Stores all the location data.
      * Locations are stored in hierarchy pattern as-
      * HeadQuarter
      *      |
      *      RRC
      *      |
      *      Village Cluster
      *      |
      *      Village
      * Seed the initial headquarter value as AhamadNagar because it is fixed value at this point of time.
      */
    public class Location : FullAuditedEntity, IMustHaveTenant
    {

        public virtual int TenantId { get; set; }

        [Required]
        public string Name { get; set; } //....Location Name

        [Required]
        public int ParentLocation { get; set; }//.....ParentLocation

        [Required]
        public LocationType LocationType { get; set; }//.....ParentLocation

        //[Required]
        //public string State { get; set; } //....Location State


            //modified Yo
        [ForeignKey("State")]
        public int? StateID { get; set; }//Foriegn key is created in database table with the name "StateId"
        public virtual State State { get; set; }

        [ForeignKey("District")]
        public int? DistrictId { get; set; }//Foriegn key is created in database table with the name "DistrictId"
        public virtual District District{ get; set; }

        [ForeignKey("Taluka")]
        public int? TalukaId { get; set; }//Foriegn key is created in database table with the name "TalukaId"
        public virtual Taluka Taluka{ get; set; }

        [ForeignKey("Village")]
        public int? VillageId { get; set; }//Foriegn key is created in database table with the name "VillageId"
        public virtual Village Village { get; set; }




    }
}
