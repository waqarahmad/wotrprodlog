﻿using Abp.Authorization.Users;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Users;
using WOTR.PM.Programs;
using WOTR.PM.States;

namespace WOTR.PM.Locations
{
    public class VillageCluster : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [Required]
        public string Name { get; set; } //....Location Name

        [ForeignKey("Program")]
        public int? ProgramID { get; set; }  //....Foriegn key is created in database table with the name "ProgramID"
        public virtual Program program { get; set; } //....Created program object used for navigation


        [ForeignKey("Location")]
        public int? RRCID { get; set; }  //....Foriegn key is created in database table with the name "LocationId"
        public virtual Location Location { get; set; } //....Created Location object used for navigation

        
        public int? VillageClusterLocationID { get; set; }  //....Foriegn key is created in database table with the name "LocationId"
       

        [ForeignKey("State")]
        public int? StateID { get; set; }  //....Foriegn key is created in database table with the name "ProgramID"
        public virtual State State { get; set; } //....Created program object used for navigation

        [ForeignKey("District")]
        public int? DistrictID { get; set; }  //....Foriegn key is created in database table with the name "ProgramID"
        public virtual District District { get; set; } //....Created program object used for navigation


        [ForeignKey("Taluka")]
        public int? TalukaID { get; set; }  //....Foriegn key is created in database table with the name "ProgramID"
        public virtual Taluka Taluka { get; set; } //....Created program object used for navigation

        [ForeignKey("Village")]
        public int? VillageID { get; set; }  //....Foriegn key is created in database table with the name "ProgramID"
        public virtual Village Village { get; set; } //....Created program object used for navigation

        [Required]
        public virtual long UserId { get; set; }
        public virtual User User { get; set; }

        public virtual long? UserRoleId { get; set; }
        public virtual UserRole UserRole { get; set; }
    }
}
