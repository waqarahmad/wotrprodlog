﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.States;

namespace WOTR.PM.Locations
{
   public class Village : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [Required]
        public string Name { get; set; } //....Location Name

        
        public int VillageClusterParent { get; set; }//.....ParentLocation


        [ForeignKey("State")]
        public int? StateID { get; set; }//Foriegn key is created in database table with the name "StateId"
        public virtual State State { get; set; }

        [ForeignKey("District")]
        public int? DistrictId { get; set; }//Foriegn key is created in database table with the name "DistrictId"
        public virtual District District { get; set; }

        [ForeignKey("Taluka")]
        public int? TalukaId { get; set; }//Foriegn key is created in database table with the name "TalukaId"
        public virtual Taluka Taluka { get; set; }

    }
}
