using System.Threading.Tasks;

namespace WOTR.PM.Identity
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}