﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;

namespace WOTR.PM.ActionSubActionMapping
{
    public class ActionSAActivityMapping : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public virtual int TenantId { get; set; }

        [ForeignKey("ActionArea")]
        public int? ActionAreaID { get; set; }  //....Foriegn key is created in database table with the name "ActionAreaID"
        public virtual ActionArea ActionArea { get; set; } //....Created ActionArea object used for navigation

        [ForeignKey("SubActionArea")]
        public int? SubActionAreaID { get; set; }  //....Foriegn key is created in database table with the name "SubActionAreaID"
        public virtual SubActionArea SubActionArea { get; set; } //....Created SubActionArea object used for navigation

        [ForeignKey("Activity")]
        public int? ActivityID { get; set; }  //....Foriegn key is created in database table with the name "ActivityID"
        public virtual Activity Activity { get; set; } //....Created Activity object used for navigation

    }
}
