﻿using Abp.Domain.Services;

namespace WOTR.PM
{
    public abstract class PMDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected PMDomainServiceBase()
        {
            LocalizationSourceName = PMConsts.LocalizationSourceName;
        }
    }
}
