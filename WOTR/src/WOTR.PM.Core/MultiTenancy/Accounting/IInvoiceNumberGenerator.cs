﻿using System.Threading.Tasks;
using Abp.Dependency;

namespace WOTR.PM.MultiTenancy.Accounting
{
    public interface IInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}