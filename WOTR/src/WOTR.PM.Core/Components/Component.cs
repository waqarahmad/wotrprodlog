﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.Components
{
    public class Component : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public virtual int TenantId { get; set; }
        [Required]
        public string Name { get; set; } //...Name of Component
        [Required]
        public long Code { get; set; }//....Component Code
    }
}
