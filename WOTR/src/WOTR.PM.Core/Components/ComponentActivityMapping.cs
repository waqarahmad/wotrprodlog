﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;

namespace WOTR.PM.Components
{
    public class ComponentActivityMapping : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public virtual int TenantId { get; set; }

        [ForeignKey("Component")]
        public int? ComponentID { get; set; }  //....Foriegn key is created in database table with the name "ComponentID"
        public virtual Component Component { get; set; } //....Created ActionArea object used for navigation


        [ForeignKey("Activity")]
        public int? ActivityID { get; set; }  //....Foriegn key is created in database table with the name "ActivityID"
        public virtual Activity Activity { get; set; } //....Created Activity object used for navigation

    }
}
