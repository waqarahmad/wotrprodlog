﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Checklists;

namespace WOTR.PM.PrgImplementationsPlans
{
  public  class prgImplementationPlanQuestionariesUrlMapping : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        public string url { get;set; }

        [ForeignKey("PrgImplementationsPlans")]
        public int? PrgImplementationsPlansID { get; set; }
        public virtual PrgImplementationsPlans PrgImplementationsPlans { get; set; }


        [ForeignKey("ImplementationPlanCheckList")]
        public int? ImplementationPlanCheckListID { get; set; }
        public virtual ImplementationPlanCheckList ImplementationPlanCheckList { get; set; }

        public int? QuestionaryId { get; set; }
    }
}
