﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Checklists;
using WOTR.PM.PrgImplementationPlans;
using WOTR.PM.Programs;

namespace WOTR.PM.PrgImplementationsPlans
{
    public class prgImplementationPlanQuestionariesMapping : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

     
        [ForeignKey("Program")]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }

     
        
        [ForeignKey("PrgImplementationsPlans")]
        public int? PrgImplementationsPlansID { get; set; }
        public virtual PrgImplementationsPlans PrgImplementationsPlans { get; set; }

        [ForeignKey("ImplementationPlanCheckList")]
        public int? ImplementationPlanCheckListID { get; set; }
        public virtual ImplementationPlanCheckList ImplementationPlanCheckList { get; set; }



        [ForeignKey("Questionarie")]
        public int QuestionarieID { get; set; }
        public virtual Questionarie Questionarie {get;set;}

        public string Answer { get; set; }

        public prgImplementationPlanQuestionariesMappingStatus Status { get; set; }

    }
}
