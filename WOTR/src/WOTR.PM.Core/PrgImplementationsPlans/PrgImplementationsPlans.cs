﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Checklists;
using WOTR.PM.ChekLists;
using WOTR.PM.Locations;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.Programs;
using WOTR.PM.UnitOfMeasures;

namespace WOTR.PM.PrgImplementationsPlans
{
    public class PrgImplementationsPlans:FullAuditedEntity,IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("Program")]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }

        [ForeignKey("Checklist")]
        public int CheckListID { get; set; }
        public virtual Checklist Checklist { get; set; }

        [ForeignKey("PrgActionAreaActivityMapping")]     //...PrgActionAreaActivityMappingID
        public int? PrgActionAreaActivityMappingID { get; set; }
        public virtual PrgActionAreaActivityMapping PrgActionAreaActivityMapping { get; set; }

        [ForeignKey("Location")]
        public int LocationID { get; set; }
        public virtual Location Location { get; set; }

        [ForeignKey("UnitOfMeasures")]
        public int? UnitOfMeasuresID { get; set; }
        public virtual UnitofMeasure UnitOfMeasures { get; set; }

        [ForeignKey("CheckListItem")]
        public int? CheckListItemID { get; set; }
        public virtual ChecklistItem ChecklistItem { get; set; }

    }
}
