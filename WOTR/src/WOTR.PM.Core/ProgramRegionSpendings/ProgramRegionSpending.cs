﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.Components;
using WOTR.PM.Locations;
using WOTR.PM.Programs;
using WOTR.PM.UnitOfMeasures;

namespace WOTR.PM.ProgramRegionSpendings
{
   public  class ProgramRegionSpending : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("Program")]
        public int? ProgramID { get; set; }
        public virtual Program Program { get; set; }


        [ForeignKey("Component")]
        public int? ComponentID { get; set; }
        public virtual Component Component { get; set; }

        

        [ForeignKey("Activity")]
        public int? ActivityID { get; set; }
        public virtual Activity Activity { get; set; }

        [ForeignKey("UnitOfMeasures")]
        public int? UnitOfMeasuresID { get; set; }
        public virtual UnitofMeasure UnitOfMeasures { get; set; }



        public long? TotalUnits { get; set; }

        public decimal? UnitCost { get; set; }

        public decimal? TotalUnitCost { get; set; }

        public decimal? CommunityContribution { get; set; }

        public decimal? FunderContribution { get; set; }

        public decimal? OtherContribution { get; set; }

        public string CostEstimationYear { get; set; }

        [ForeignKey("Location")]
        public int? LocationID { get; set; }
        public virtual Location Location { get; set; }

        public bool isProgramLevel { get; set; }
    }
}
