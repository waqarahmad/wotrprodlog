﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ImpactIndicators;

namespace WOTR.PM.Programs
{
    [Table("ProgramImpactIndicatorActivityMapping")]
    public class ProgramImpactIndicatorActivityMapping : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("Program")]
        public int ProgramID { get; set; } 
        public virtual Program Program { get; set; }

        [ForeignKey("ProgramImpactIndicatorMapping")]
        public int? ProgramImpactIndicatorMappingID { get; set; } 
        public virtual ProgramImpactIndicatorMapping ProgramImpactIndicatorMapping { get; set; }


        [ForeignKey("ImpactIndicatorActivity")]
        public int? ImpactIndicatorctivitiesID { get; set; }  
        public virtual ImpactIndicatorActivity ImpactIndicatorActivity { get; set; } 
    }
}
