﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ImpactIndicators;

namespace WOTR.PM.Programs
{
    [Table("ProgramImpactIndicatorMapping")]

    public  class ProgramImpactIndicatorMapping : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("Program")]
        public int ProgramID { get; set; } 
        public virtual Program Program { get; set; }

        [ForeignKey("ImpactIndicator")]
        public int? ImpactIndicatorID { get; set; }  
        public virtual ImpactIndicator ImpactIndicator { get; set; } 
    }
}
