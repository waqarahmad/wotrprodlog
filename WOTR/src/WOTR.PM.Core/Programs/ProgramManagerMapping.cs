﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.Programs
{
    public class ProgramManagerMapping : FullAuditedEntity, IMustHaveTenant
    {
       
        public virtual int TenantId { get; set; }

        [Required]
        public virtual long? ManagerID { get; set; }

        [Required]
        public virtual int? ProgramID { get; set; }

    }
}
