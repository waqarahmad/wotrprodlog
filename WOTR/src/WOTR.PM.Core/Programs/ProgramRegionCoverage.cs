﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Locations;

namespace WOTR.PM.Programs
{
    public class ProgramRegionCoverage : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set ; }


        [ForeignKey("Program")]
        public int? ProgramID { get; set; }  //....Foriegn key is created in database table with the name "ProgramID"
        public virtual Program program { get; set; } //....Created program object used for navigation


        [ForeignKey("Location")]
        public int? LocationID { get; set; }  //....Foriegn key is created in database table with the name "LocationId"
        public virtual Location Location { get; set; } //....Created Location object used for navigation



    }
}
