﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.Components;
using WOTR.PM.ImpactIndicators;

namespace WOTR.PM.Programs
{
   public  class ProgramComponentsAndImpactIndicatorMapping :FullAuditedEntity,IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        //..Compalsary Added Program and ComponentId
        [ForeignKey("Program")]
        public int ProgramID { get; set; } //..Foriegn Key is created in database table with the name "ProgramID"
        public virtual Program Program { get; set; }//..Created Program object used for navigation



        /*Componet and Activity is Already Mapped in (ComponetActivityMapping)Master Table, but not clear idea so far take ID for Component and Activity table separtly */
        [ForeignKey("Component")]
        public int ComponentID { get; set; }  //....Foriegn key is created in database table with the name "ComponentID"
        public virtual Component Component { get; set; } //....Created ActionArea object used for navigation

        [ForeignKey("Activity")]
        public int? ComponentActivityID { get; set; }  //....Foriegn key is created in database table with the name "ComponentActivityID"
        public virtual Activity Activity { get; set; } //....Created Activity object used for navigation



        [ForeignKey("ImpactIndicartor")]
        public int? ImpactIndicatorID { get; set; }  //....Foriegn key is created in database table with the name "ImpactIndicatorID"
        public virtual ImpactIndicator ImpactIndicator { get; set; } //....Created ImpactIndicator object used for navigation

        [ForeignKey("ImpactIndicatorActivity")]
        public int? ImpactIndicatorctivitiesID { get; set; }  //....Foriegn key is created in database table with the name "ImpactIndicatorctivitiesID"
        public virtual ImpactIndicatorActivity ImpactIndicatorctivities { get; set; } //....Created ImpactIndicatorActivity object used for navigation
    }
}
