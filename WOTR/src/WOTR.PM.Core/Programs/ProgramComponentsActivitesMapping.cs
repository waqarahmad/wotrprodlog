﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.Components;

namespace WOTR.PM.Programs
{
    [Table("ProgramComponentsActivitesMapping")]
    public  class ProgramComponentsActivitesMapping : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("Program")]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }


        [ForeignKey("Component")]
        public int? ComponentID { get; set; }
        public virtual Component Component { get; set; }

        //[ForeignKey("ProgramComponentsMapping")]
        //public int? ProgramComponentsMappingID { get; set; }
        //public virtual ProgramComponentsMapping ProgramComponentsMapping { get; set; }


        [ForeignKey("Activity")]
        public int? ActivityID { get; set; } 
        public virtual Activity Activity { get; set; }
    }
}
