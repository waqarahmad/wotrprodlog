﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Users;
using WOTR.PM.Donors;

namespace WOTR.PM.Programs
{
    [Table("Program")]
    public class Program : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }
        public string Name { get; set; }
       
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }

        [ForeignKey("Donor")]
        public int? DonorID { get; set; }  //....Foriegn key is created in database table with the name "DonorID"
        public virtual Donor Donor { get; set; } //....Created Donor object used for navigation

        public int ManagerID { get; set; } //....Stores userID aof ABPuser table where Role="Manager"

        public long ProgramSanctionedYear { get; set; }
        public DateTime ProgramStartDate { get; set; }
        public DateTime PrgramEndDate { get; set; }
        public string ProgramBadgeImage { get; set; } //....Stores Progrm Badge image
        public string ProgramBadgeColor { get; set; } //...Store Program Badge Color

        //...new requriedment.
        public DateTime ProgramSubmitDate { get; set; } //..program submition date .
        public DateTime ProgramApprovaltDate { get; set; } //..program Approval date .
        public decimal? GantnAmountSanctionRupess { get; set; } //..Donor sanction Amount .

        [ForeignKey("User")]                             //...UserID
        public long? ProgramManagerID { get; set; }
        public virtual User ProgramManager { get; set; }
    }

}
