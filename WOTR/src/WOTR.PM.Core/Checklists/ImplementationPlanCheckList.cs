﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ChekLists;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.PrgImplementationPlans;
using WOTR.PM.PrgQuarterUnits;
using WOTR.PM.Programs;
using WOTR.PM.UnitOfMeasures;

namespace WOTR.PM.Checklists
{
    public class ImplementationPlanCheckList : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public int? ProgramLocationId { get; set; }

        [ForeignKey("PrgActionAreaActivityMapping")]     //...PrgActionAreaActivityMappingID
        public int? PrgActionAreaActivityMappingID { get; set; }
        public virtual PrgActionAreaActivityMapping PrgActionAreaActivityMapping { get; set; }

        [ForeignKey("Checklist")]                       //...ChecklistID
        public int? ChecklistID { get; set; }
        public virtual Checklist Checklist { get; set; }

        [ForeignKey("ChecklistItem")]
        public int? ChecklistItemID { get; set; }
        public virtual ChecklistItem ChecklistItem { get; set; }


        [ForeignKey("Program")]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }

        public long? AssingUserId {get;set;}

        [ForeignKey("UnitOfMeasures")]
        public int? UnitOfMeasuresID { get; set; }
        public virtual UnitofMeasure UnitOfMeasures { get; set; }

        public decimal? Units { get; set; }

        public string Description { get; set; }

        public int? IteamLocationId { get; set; }

        public string CostEstimationYear { get; set; }

        public DateTime CheklistStartDate { get; set; }

        public DateTime CheklistEndDate { get; set; }

        [ForeignKey("ProgramQuqterUnitMapping")]
        public int? ProgramQuqterUnitMappingID { get; set; }
        public virtual ProgramQuqterUnitMapping ProgramQuqterUnitMapping { get; set; }

        public int PrgQuarter { get; set; }

        public SubActivityStatus status { get; set; }

        public decimal? AchieveUnits { get; set; }
    }
}
