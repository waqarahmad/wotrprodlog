using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Entities;
using WOTR.PM.ActionAreas;
using System.Collections.Generic;
using WOTR.PM.Checklists;

namespace WOTR.PM.ChekLists
{
    //Checklist is  Now as SUBACTIVITY-change( checklist Name to subActivity as per Client Request).

	[Table("Checklist")]
    public class Checklist : FullAuditedEntity , IMustHaveTenant
    {
        public int TenantId { get; set; }

        [Required]
        public virtual string ChecklistName { get; set; }

        public string Description { get; set; }

        public int?  Rating { get; set; }

        [ForeignKey("Activity")]
        public int ActivityID { get; set; }
        public virtual Activity Activity { get; set; }

        public ICollection<ChecklistItem> CheckListItems { get; set; }

    }
}