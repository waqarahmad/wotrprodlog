﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ChekLists;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.Programs;

namespace WOTR.PM.Checklists
{
  public   class MapSubActivityIteamsToImplementionPlan: FullAuditedEntity  , IMustHaveTenant
    {
        public int TenantId { get; set; }

        [ForeignKey("Checklist")]                      
        public int? ChecklistID { get; set; }
        public virtual Checklist Checklist { get; set; }

        [ForeignKey("ChecklistItem")]
        public int? ChecklistItemID { get; set; }
        public virtual ChecklistItem ChecklistItem { get; set; }


        [ForeignKey("Program")]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }

        [ForeignKey("PrgActionAreaActivityMapping")]    
        public int? PrgActionAreaActivityMappingID { get; set; }
        public virtual PrgActionAreaActivityMapping PrgActionAreaActivityMapping { get; set; }
    }
}
