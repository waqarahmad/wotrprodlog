﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.ChekLists;

namespace WOTR.PM.Checklists
{
   public  class ActivityCheckList : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [ForeignKey("Checklist")]
        public int? ChecklistID { get; set; }
        public virtual Checklist Checklist { get; set; }

        [ForeignKey("Activity")]
        public int? ActivityID { get; set; } 
        public virtual Activity Activity { get; set; }
    }
}
