﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.PrgImplementationsPlans;

namespace WOTR.PM.Checklists
{
    public class SubActivityQuestionaryMapping : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        [ForeignKey("SubActivityList")]
        public int? SubActivityId { get; set; }
        public virtual SubActivityList SubActivityList { get; set; }

        [ForeignKey("Questionarie")]
        public int? QuestionarieID { get; set; }
        public virtual Questionarie Questionarie { get; set; }

        [ForeignKey("Activity")]
        public int? ActivityID { get; set; }
        public virtual Activity Activity { get; set; }
    }
}
