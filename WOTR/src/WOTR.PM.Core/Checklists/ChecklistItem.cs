﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ChekLists;

namespace WOTR.PM.Checklists
{
  public  class ChecklistItem : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public string Name { get; set; }


        [ForeignKey("Checklist")]
        public int? ChecklistID { get; set; }
        public virtual Checklist Checklist { get; set; }

        [ForeignKey("SubActivityList")]
        public int? SubActivityId { get; set; }
        public virtual SubActivityList SubActivityList { get; set; }

        public bool? IsChecked { get; set; }
    }
}
