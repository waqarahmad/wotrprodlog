﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Programs;

namespace WOTR.PM.ProgramCostEstimations
{
   public  class ProgramCETotalCost : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("Program")]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }

        public DateTime CostEstimationYear { get; set; }

        public decimal? TotalUnits { get; set; }

        public decimal? TotalUnitsCost { get; set; }

        public decimal? TotalCost { get; set; }
    }
}
