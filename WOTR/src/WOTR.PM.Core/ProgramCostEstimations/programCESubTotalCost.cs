﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Components;
using WOTR.PM.Programs;

namespace WOTR.PM.ProgramCostEstimations
{
  public   class programCESubTotalCost : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("Program")]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }


        [ForeignKey("Component")]
        public int? ComponentID { get; set; }
        public virtual Component Component { get; set; }


        public decimal? SubTotalTotalunits { get; set; }
        public decimal? SubTotalUnitCost { get; set; }
        public decimal? SubTotalofTotalCost { get; set; }
        public decimal? SubTotalofCommunityContribution { get; set; }
        public decimal? SubTotalofFunderContribution { get; set; }
        public decimal? SubTotalofOtherContribution { get; set; }
        public string CostEstimationYear { get; set; }

    }
}
