namespace WOTR.PM.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        public const string Pages_ChekLists = "Pages.ChekLists";
        public const string Pages_ChekLists_Create = "Pages.ChekLists.Create";
        public const string Pages_ChekLists_Edit = "Pages.ChekLists.Edit";
        public const string Pages_ChekLists_Delete = "Pages.ChekLists.Delete";

        public const string Pages_UnitofMeasures = "Pages.UnitofMeasures";
        public const string Pages_UnitofMeasures_Create = "Pages.UnitofMeasures.Create";
        public const string Pages_UnitofMeasures_Edit = "Pages.UnitofMeasures.Edit";
        public const string Pages_UnitofMeasures_Delete = "Pages.UnitofMeasures.Delete";

        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents= "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";


        //WOTR PERMISSIONS

        public const string Pages_Tenant_MyWorkSpace = "Pages.Tenant.MyWorkSpace";

        public const string Pages_Donars = "Pages.Donars";
        public const string Pages_Donars_Create = "Pages.Donars.Create";
        public const string Pages_Donars_Edit = "Pages.Donars.Edit";
        public const string Pages_Donars_Delete = "Pages.Donars.Delete";

        public const string Pages_ActionArea = "Pages.ActionArea";
        public const string Pages_ActionArea_Create = "Pages.ActionArea.Create";
        public const string Pages_ActionArea_Edit = "Pages.ActionArea.Edit";
        public const string Pages_ActionArea_Delete = "Pages.ActionArea.Delete";

        public const string Pages_ActionSubAction = "Pages.ActionSubAction";
        public const string Pages_ActionSubAction_Create = "Pages.ActionSubAction.Create";
        public const string Pages_ActionSubAction_Edit = "Pages.ActionSubAction.Edit";
        public const string Pages_ActionSubAction_Delete = "Pages.ActionSubAction.Delete";

        public const string Pages_Activity = "Pages.Activity";
        public const string Pages_Activity_Create = "Pages.Activity.Create";
        public const string Pages_Activity_Edit = "Pages.Activity.Edit";
        public const string Pages_Activity_Delete = "Pages.Activity.Delete";

        public const string Pages_Components = "Pages.Components";
        public const string Pages_Components_Create = "Pages.Components.Create";
        public const string Pages_Components_Edit = "Pages.Components.Edit";
        public const string Pages_Components_Delete = "Pages.Components.Delete";

        public const string Pages_SubActionArea = "Pages.SubActionArea";
        public const string Pages_SubActionArea_Create = "Pages.SubActionArea.Create";
        public const string Pages_SubActionArea_Edit = "Pages.SubActionArea.Edit";
        public const string Pages_SubActionArea_Delete = "Pages.SubActionArea.Delete";

        public const string Pages_Location = "Pages.Location";
        public const string Pages_Location_Create = "Pages.Location.Create";
        public const string Pages_Location_Edit = "Pages.Location.Edit";
        public const string Pages_Location_Delete = "Pages.Location.Delete";

        public const string Pages_CreateProgramInformation = "Pages.CreateProgramInformation";
        //public const string Pages_CreateProgramInformation_Create = "Pages.CreateProgramInformation.Create";
        //public const string Pages_CreateProgramInformation_Edit = "Pages.CreateProgramInformation.Edit";
        //public const string Pages_CreateProgramInformation_Delete = "Pages.CreateProgramInformation.Delete";

        public const string Pages_ProgramComponent = "Pages.ProgramComponent";
        //public const string Pages_Location_Create = "Pages.Location.Create";
        //public const string Pages_Location_Edit = "Pages.Location.Edit";
        //public const string Pages_Location_Delete = "Pages.Location.Delete";

        public const string Pages_ProgramCostEstimation = "Pages.ProgramCostEstimation";
        //public const string Pages_ProgramCostEstimation_Create = "Pages.ProgramCostEstimation.Create";
        //public const string Pages_ProgramCostEstimation_Edit = "Pages.ProgramCostEstimation.Edit";
        //public const string Pages_ProgramCostEstimation_Delete = "Pages.ProgramCostEstimation.Delete";

        public const string Pages_ActivityMapping = "Pages.ActivityMapping";
        //public const string Pages_ActivityMapping_Create = "Pages.ActivityMapping.Create";
        //public const string Pages_ActivityMapping_Edit = "Pages.ActivityMapping.Edit";
        //public const string Pages_ActivityMapping_Delete = "Pages.ActivityMapping.Delete";

        public const string Pages_RegionSpending = "Pages.RegionSpending";
        //public const string Pages_RegionSpending_Create = "Pages.RegionSpending.Create";
        //public const string Pages_RegionSpending_Edit = "Pages.RegionSpending.Edit";
        //public const string Pages_RegionSpending_Delete = "Pages.RegionSpending.Delete";

        public const string Pages_RequestAAPla = "Pages.RequestAAPla";
        //public const string Pages_RequestAAPla_Create = "Pages.RequestAAPla.Create";
        //public const string Pages_RequestAAPla_Edit = "Pages.RequestAAPla.Edit";
        //public const string Pages_RequestAAPla_Delete = "Pages.RequestAAPla.Delete";

        public const string Pages_RequestAAPlaSend = "Pages.RequestAAPlaSend";

        public const string Pages_FundRelease = "Pages.FundRelease";




        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";

    }
}