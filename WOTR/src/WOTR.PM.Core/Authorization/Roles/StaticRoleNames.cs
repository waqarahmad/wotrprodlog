﻿namespace WOTR.PM.Authorization.Roles
{
    public static class StaticRoleNames
    {
        public static class Host
        {
            public const string Admin = "Admin";
        }

        public static class Tenants
        {
            public const string Admin = "Admin";
            public const string User = "User";
            public const string ActionAreaManager = "ActionAreaManager";
            public const string ManagingTrustee = "ManagingTrustee";
            public const string ExecutiveDirective = "ExecutiveDirective";
            public const string ProgramCoordinator = "ProgramCoordinator";
            public const string CommunicationTeam = "CommunicationTeam";
            public const string ProgramManager = "ProgramManager";
            public const string RRcIncharge = "RRcIncharge";
            public const string ProjectManager = "ProjectManager";
            public const string FieldStaffMember = "FieldStaffMember";
            public const string ProgramManagerFinanceOrAdmin = "ProgramManagerFinanceOrAdmin";

        }
    }
}