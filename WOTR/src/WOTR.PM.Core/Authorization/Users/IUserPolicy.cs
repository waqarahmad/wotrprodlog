﻿using System.Threading.Tasks;
using Abp.Domain.Policies;

namespace WOTR.PM.Authorization.Users
{
    public interface IUserPolicy : IPolicy
    {
        Task CheckMaxUserCountAsync(int tenantId);
    }
}
