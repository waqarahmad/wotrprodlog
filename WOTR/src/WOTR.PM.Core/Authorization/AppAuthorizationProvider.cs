using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace WOTR.PM.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));

            var chekLists = pages.CreateChildPermission(AppPermissions.Pages_ChekLists, L("ChekLists"));
            chekLists.CreateChildPermission(AppPermissions.Pages_ChekLists_Create, L("CreateNewChekList"));
            chekLists.CreateChildPermission(AppPermissions.Pages_ChekLists_Edit, L("EditChekList"));
            chekLists.CreateChildPermission(AppPermissions.Pages_ChekLists_Delete, L("DeleteChekList"));



            var unitofMeasures = pages.CreateChildPermission(AppPermissions.Pages_UnitofMeasures, L("UnitOfMeasures"));
            unitofMeasures.CreateChildPermission(AppPermissions.Pages_UnitofMeasures_Create, L("CreateNewUnitofMeasure"));
            unitofMeasures.CreateChildPermission(AppPermissions.Pages_UnitofMeasures_Edit, L("EditUnitofMeasure"));
            unitofMeasures.CreateChildPermission(AppPermissions.Pages_UnitofMeasures_Delete, L("DeleteUnitofMeasure"));


            pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            //TENANT-SPECIFIC PERMISSIONS

            //WOTR PERMISSION
            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_MyWorkSpace, L("MyWorkSpace"), multiTenancySides: MultiTenancySides.Tenant);

            pages.CreateChildPermission(AppPermissions.Pages_CreateProgramInformation, L("CreateProgramInformation"), multiTenancySides: MultiTenancySides.Tenant);

            pages.CreateChildPermission(AppPermissions.Pages_ProgramComponent, L("ProgramComponent"), multiTenancySides: MultiTenancySides.Tenant);

            pages.CreateChildPermission(AppPermissions.Pages_ProgramCostEstimation, L("ProgramCostEstimation"), multiTenancySides: MultiTenancySides.Tenant);

            pages.CreateChildPermission(AppPermissions.Pages_ActivityMapping, L("ActivityMapping"), multiTenancySides: MultiTenancySides.Tenant);

            pages.CreateChildPermission(AppPermissions.Pages_RegionSpending, L("RegionSpending"), multiTenancySides: MultiTenancySides.Tenant);

            pages.CreateChildPermission(AppPermissions.Pages_RequestAAPla, L("RequestAAPla"), multiTenancySides: MultiTenancySides.Tenant);

            pages.CreateChildPermission(AppPermissions.Pages_RequestAAPlaSend, L("RequestAAPlaSend"), multiTenancySides: MultiTenancySides.Tenant);

            pages.CreateChildPermission(AppPermissions.Pages_FundRelease, L("FundRelease"), multiTenancySides: MultiTenancySides.Tenant);

            var donar = pages.CreateChildPermission(AppPermissions.Pages_Donars, L("Donars"), multiTenancySides: MultiTenancySides.Tenant);
            donar.CreateChildPermission(AppPermissions.Pages_Donars_Create, L("CreatingNewDonar"), multiTenancySides: MultiTenancySides.Tenant);
            donar.CreateChildPermission(AppPermissions.Pages_Donars_Edit, L("EditingDonar"), multiTenancySides: MultiTenancySides.Tenant);
            donar.CreateChildPermission(AppPermissions.Pages_Donars_Delete, L("DeletingDonar"), multiTenancySides: MultiTenancySides.Tenant);

            var ActionArea = pages.CreateChildPermission(AppPermissions.Pages_ActionArea, L("ActionArea"), multiTenancySides: MultiTenancySides.Tenant);
            ActionArea.CreateChildPermission(AppPermissions.Pages_ActionArea_Create, L("CreatingNewActionArea"), multiTenancySides: MultiTenancySides.Tenant);
            ActionArea.CreateChildPermission(AppPermissions.Pages_ActionArea_Edit, L("EditingActionArea"), multiTenancySides: MultiTenancySides.Tenant);
            ActionArea.CreateChildPermission(AppPermissions.Pages_ActionArea_Delete, L("DeletingActionArea"), multiTenancySides: MultiTenancySides.Tenant);

            var ActionSubAction = pages.CreateChildPermission(AppPermissions.Pages_ActionSubAction, L("ActionSubAction"), multiTenancySides: MultiTenancySides.Tenant);
            ActionSubAction.CreateChildPermission(AppPermissions.Pages_ActionSubAction_Create, L("CreatingNewActionSubAction"), multiTenancySides: MultiTenancySides.Tenant);
            ActionSubAction.CreateChildPermission(AppPermissions.Pages_ActionSubAction_Edit, L("EditingActionSubAction"), multiTenancySides: MultiTenancySides.Tenant);
            ActionSubAction.CreateChildPermission(AppPermissions.Pages_ActionSubAction_Delete, L("DeletingActionSubAction"), multiTenancySides: MultiTenancySides.Tenant);

            var Activity = pages.CreateChildPermission(AppPermissions.Pages_Activity, L("Activity"), multiTenancySides: MultiTenancySides.Tenant);
            Activity.CreateChildPermission(AppPermissions.Pages_Activity_Create, L("CreatingNewActivity"), multiTenancySides: MultiTenancySides.Tenant);
            Activity.CreateChildPermission(AppPermissions.Pages_Activity_Edit, L("EditingActivity"), multiTenancySides: MultiTenancySides.Tenant);
            Activity.CreateChildPermission(AppPermissions.Pages_Activity_Delete, L("DeletingActivity"), multiTenancySides: MultiTenancySides.Tenant);

            var Components = pages.CreateChildPermission(AppPermissions.Pages_Components, L("Components"), multiTenancySides: MultiTenancySides.Tenant);
            Components.CreateChildPermission(AppPermissions.Pages_Components_Create, L("CreatingNewComponents"), multiTenancySides: MultiTenancySides.Tenant);
            Components.CreateChildPermission(AppPermissions.Pages_Components_Edit, L("EditingComponents"), multiTenancySides: MultiTenancySides.Tenant);
            Components.CreateChildPermission(AppPermissions.Pages_Components_Delete, L("DeletingComponents"), multiTenancySides: MultiTenancySides.Tenant);

            var SubActionArea = pages.CreateChildPermission(AppPermissions.Pages_SubActionArea, L("SubActionArea"), multiTenancySides: MultiTenancySides.Tenant);
            SubActionArea.CreateChildPermission(AppPermissions.Pages_SubActionArea_Create, L("CreatingNewSubActionArea"), multiTenancySides: MultiTenancySides.Tenant);
            SubActionArea.CreateChildPermission(AppPermissions.Pages_SubActionArea_Edit, L("EditingSubActionArea"), multiTenancySides: MultiTenancySides.Tenant);
            SubActionArea.CreateChildPermission(AppPermissions.Pages_SubActionArea_Delete, L("DeletingSubActionArea"), multiTenancySides: MultiTenancySides.Tenant);

            var Location = pages.CreateChildPermission(AppPermissions.Pages_Location, L("Location"), multiTenancySides: MultiTenancySides.Tenant);
            Location.CreateChildPermission(AppPermissions.Pages_Location_Create, L("CreatingNewLocation"), multiTenancySides: MultiTenancySides.Tenant);
            Location.CreateChildPermission(AppPermissions.Pages_Location_Edit, L("EditingLocation"), multiTenancySides: MultiTenancySides.Tenant);
            Location.CreateChildPermission(AppPermissions.Pages_Location_Delete, L("DeletingLocation"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, PMConsts.LocalizationSourceName);
        }
    }
}
