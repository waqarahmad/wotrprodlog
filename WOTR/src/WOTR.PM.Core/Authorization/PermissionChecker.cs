﻿using Abp.Authorization;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Authorization.Users;

namespace WOTR.PM.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
