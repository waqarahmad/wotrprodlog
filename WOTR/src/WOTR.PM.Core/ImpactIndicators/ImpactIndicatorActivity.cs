﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.ImpactIndicators
{
    [Table("ImpactIndicatorActivity")]
   public class ImpactIndicatorActivity : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }
        [Required]
         public string Name { get; set; }

        public string Description { get; set; }
    }
}
