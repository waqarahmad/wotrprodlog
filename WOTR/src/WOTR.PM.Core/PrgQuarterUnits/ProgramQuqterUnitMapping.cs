﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Locations;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.Programs;

namespace WOTR.PM.PrgQuarterUnits
{
    public class ProgramQuqterUnitMapping : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("Program")]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }

        public string ProgramQuaterYear { get; set; }

        [ForeignKey("PrgActionAreaActivityMapping")]
        public int? PrgActionAreaActivityMappingID { get; set; }
        public virtual PrgActionAreaActivityMapping PrgActionAreaActivityMapping { get; set; }

        public int QuarterId { get; set; }
        public int NosOfUnit { get; set; }
        public decimal Rs { get; set; }
        public ProgramQuarterUnitMappingStatus Status { get; set; }

        [ForeignKey("Village")]
        public int? VillageID { get; set; }
        public virtual Village Village { get; set; }

    }
    
}
