﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.Components;
using WOTR.PM.Programs;

namespace WOTR.PM.PrgActionAreaActivitysMappings
{
    
   public class PrgActionAreaActivityMapping :FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [ForeignKey("Program")]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }


        [ForeignKey("Component")]
        public int? ComponentID { get; set; }
        public virtual Component Component { get; set; }


        [ForeignKey("Activity")]
        public int? ActivityID { get; set; }
        public virtual Activity Activity { get; set; }

        [ForeignKey("ActionArea")]
        public int? ActionAreaID { get; set; }
        public virtual ActionArea ActionArea { get; set; }

        [ForeignKey("SubActionArea")]
        public int? SubActionAreaID { get; set; }
        public virtual SubActionArea SubActionArea { get; set; }

    }
}
