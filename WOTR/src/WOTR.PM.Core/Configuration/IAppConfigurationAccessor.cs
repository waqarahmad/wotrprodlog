﻿using Microsoft.Extensions.Configuration;

namespace WOTR.PM.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
