﻿using Abp.Dependency;

namespace WOTR.PM
{
    public class AppFolders : IAppFolders, ISingletonDependency
    {
        public string TempFileDownloadFolder { get; set; }

        public string SampleProfileImagesFolder { get; set; }

        public string WebLogsFolder { get; set; }
        public string LogoImgFolder { get; set; }
    }
}