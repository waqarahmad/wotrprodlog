﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Organizations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.LoginAndroid
{
   public class AppInfo : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ApplicationId { get; set; }
        public string SenderId { get; set; }
        public string PackageName { get; set; }
    }
}
