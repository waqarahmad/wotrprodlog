﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Users;

namespace WOTR.PM.LoginAndroid
{
   public  class AppDeviceLists : FullAuditedEntity ,IMustHaveTenant
    {
        public int TenantId { get; set; }  
        public int AppId { get; set; }

        public string DeviceId { get; set; }
        public bool DeviceStatus { get; set; }
        public string DeviceUniqueId { get; set; }

        public string ApplicationId { get; set; }
        public string SenderId { get; set; }

        [ForeignKey("User")]
        public long? UserId { get; set; }
        public virtual User User { get; set; }
    }
}
