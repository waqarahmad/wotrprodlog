﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Users;
using WOTR.PM.Locations;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.Programs;
using WOTR.PM.UnitOfMeasures;

namespace WOTR.PM.ProgramFunds
{
    public class ProgramFund : FullAuditedEntity, IMustHaveTenant
    {

        public virtual int TenantId { get; set; }

        [ForeignKey("Program")]
        public int ProgramID { get; set; }
        public virtual Program Program { get; set; }


        [ForeignKey("PrgActionAreaActivityMapping")]     //...PrgActionAreaActivityMappingID
        public int? PrgActionAreaActivityMappingID { get; set; }
        public virtual PrgActionAreaActivityMapping PrgActionAreaActivityMapping { get; set; }

        public Status1 Status { get; set; }

        public int Unit { get; set; }
        public decimal Amount { get; set; }

        [ForeignKey("UnitOfMeasures")]
        public int? UnitOfMeasuresID { get; set; }
        public virtual UnitofMeasure UnitOfMeasures { get; set; }

        [ForeignKey("User")]                             //...UserID
        public long? ProjectManagerID { get; set; }
        public virtual User ProjectManager { get; set; }

        [ForeignKey("User")]                             //...UserID
        public long? ProgramManagerID { get; set; }
        public virtual User ProgramManager { get; set; }

        [ForeignKey("Location")]                        //...LocationID
        public int? LocationID { get; set; }
        public virtual Location Location { get; set; }
        public string CostEstimationYear { get; set; }

    }
    
}
