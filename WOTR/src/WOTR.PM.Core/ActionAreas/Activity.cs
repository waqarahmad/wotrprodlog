﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.UnitOfMeasures;

namespace WOTR.PM.ActionAreas
{
    public class Activity : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public virtual int TenantId { get; set; }
        [Required]
        public string Name { get; set; } //...Activity Name 
        public string Description { get; set; }//....Activity Description

        [ForeignKey("UnitOfMeasures")]
        public int? UnitOfMeasuresId { get; set; }  
        public virtual UnitofMeasure UnitOfMeasures { get; set; } 
    }
}
