﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.ActionAreas
{
    [Table("ActionArea")]
    public class ActionArea : FullAuditedEntity, IMustHaveTenant
    {
      
        
            [Required]
            public virtual int TenantId { get; set; }

            [Required]
            public string Name { get; set; } //...Name of Action Area

             [Required]
            public string Code { get; set; }//....Action area code which is user defined
      
    }
}
