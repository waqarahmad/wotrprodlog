﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.ActionAreas
{
    [Table("SubActionArea")]
    public class SubActionArea : FullAuditedEntity, IMustHaveTenant
    {
        [Required]
        public virtual int TenantId { get; set; }
        [Required]
        public string Name { get; set; } //...Name of SubAction Area
        [Required]
        public string Code { get; set; }//....SubAction area code which is user defined

        [ForeignKey("ActionArea")]
        public int? ActionAreaId { get; set; }
        public virtual ActionArea ActionArea { get; set; }
    }
}
