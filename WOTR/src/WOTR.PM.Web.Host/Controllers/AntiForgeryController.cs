﻿using Microsoft.AspNetCore.Antiforgery;

namespace WOTR.PM.Web.Controllers
{
    public class AntiForgeryController : PMControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
