﻿import { PMPage } from './app.po';

describe('abp-zero-template App', function () {
    let page: PMPage;

    beforeEach(() => {
        page = new PMPage();
    });

    it('should display message saying app works', () => {
        page.navigateTo();
        page.getCopyright().then(value => {
            expect(value).toEqual(new Date().getFullYear() + ' © PM.');
        });
    });
});
