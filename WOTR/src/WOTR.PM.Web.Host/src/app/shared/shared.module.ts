﻿import { NgModule } from '@angular/core';
import { EditorModule } from 'primeng/primeng';
import { CommonModule } from '@angular/common';

import { ModalModule, TooltipModule, TabsModule } from 'ngx-bootstrap';
//import { ProgramInformationComponent } from './../main/programinformation/programinformation.component';
//import { ProjectTreeComponent } from '../shared/layout/nav/projecttree.component';

import { FormsModule } from '@angular/forms';
@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        ModalModule.forRoot(),
        TooltipModule.forRoot(),
        TabsModule.forRoot(),
        EditorModule
     ],
    declarations: [
        // ProjectTreeComponent,
//ProgramInformationComponent,

    ],
    exports: [
     //   ProjectTreeComponent,
//ProgramInformationComponent
    ]
})
  
export class SharedModule {}