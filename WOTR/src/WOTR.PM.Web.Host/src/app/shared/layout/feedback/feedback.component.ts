﻿import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FeedbackServiceProxy, FeedbackDto, WorkItemListDto, IdNameDropdownDto } from '@shared/service-proxies/service-proxies';
//import { LazyLoadEvent } from 'primeng/api';
//import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
    selector: 'feedbackModal',
    templateUrl: './feedback.component.html',
    styleUrls: ['./feedback.component.css']
})
export class FeedbackModalComponent extends AppComponentBase implements OnInit {

    @ViewChild('feedbackModal') modal: ModalDirective;
    tabIndex: number;
    pageConvert: boolean = true;
    filter: string;
    addFeedBack: FeedbackDto = new FeedbackDto();
    displayFeedBack: WorkItemListDto[] = [];
    SearchByStatusDto: IdNameDropdownDto[] = [];
    detailData: WorkItemListDto = new WorkItemListDto();
    ascendingSort: boolean = true;
    sortNo: number = 1;
    public PriorityStatus = [
        { 'id': 1, 'itemName': 'New' },
        { 'id': 2, 'itemName': 'Active' },
        { 'id': 3, 'itemName': 'Resolved' },
        { 'id': 4, 'itemName': 'closed' }
    ];

    constructor(injector: Injector,
        private _feedbackServiceProxy: FeedbackServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.sortNo = 1;
        this.tabIndex = 1;
        this.getAllFeedbackListFromDevops();
    }

    onKey(tabIndex): void {
        if (tabIndex == 1) {
            this.tabIndex = 1;

        }
        else {

            this.tabIndex = 2;

        }
    }

    show(): void {
        this.modal.show();
    }
    close() {
        this.modal.hide();
    }
    onChange(name: any, event) {
        const checked = event.target.checked;
        if (checked) {

            this.SearchByStatusDto.push(name);
            this.getAllFeedbackListFromDevops();

        } else {

            const index = this.SearchByStatusDto.indexOf(name);
            this.SearchByStatusDto.splice(index, 1);
            this.getAllFeedbackListFromDevops();
        }



    }

    submit() {

        this._feedbackServiceProxy.createFeedbackInAzureDevops(this.addFeedBack).subscribe(result => {
            this.notify.success("Save Successfully");
            this.addFeedBack.title = "";
            this.addFeedBack.feedbackType = "";
            this.addFeedBack.description = "";
            this.getAllFeedbackListFromDevops();
            this.modal.hide();
        });
    }

    apply() {
        this.getAllFeedbackListFromDevops();
    }
    getAllFeedbackListFromDevops() {
        this._feedbackServiceProxy.getAllFeedbackListFromDevops(this.filter, this.SearchByStatusDto, this.sortNo).subscribe(result => {
            this.displayFeedBack = result.items;
        });
    }

    GetData(id) {
        this._feedbackServiceProxy.getFeedbackId(id).subscribe(result => {
            if (result.length > 0) {
                this.detailData.id = result[0].id;
                this.detailData.title = result[0].title;
                this.detailData.status = result[0].status;
                this.detailData.type = result[0].type;
                this.detailData.description = result[0].description;
                this.detailData.reproDescription = result[0].reproDescription;

                this.pageConvert = false;
            }

        });
    }

    //config: AngularEditorConfig = {
    //    editable: true,
    //    spellcheck: true,
    //    height: 'auto',
    //    minHeight: '5rem',
    //    placeholder: 'Enter text here...',
    //    translate: 'no',
    //    defaultParagraphSeparator: 'p',
    //    defaultFontName: 'Arial',

    //    toolbarHiddenButtons: [
    //        ['bold']
    //    ],
    //    customClasses: [
    //        {
    //            name: "quote",
    //            class: "quote",
    //        },
    //        {
    //            name: 'redText',
    //            class: 'redText'
    //        },
    //        {
    //            name: "titleText",
    //            class: "titleText",
    //            tag: "h1",
    //        },
    //    ]
    //};

    sortData() {
        if (this.ascendingSort == true) {
            this.sortNo = 1;
            this.ascendingSort = false;
            this.getAllFeedbackListFromDevops();
        }
        else {
            this.sortNo = 2;
            this.ascendingSort = true;
            this.getAllFeedbackListFromDevops();
        }
    }
}
