import { Component, OnInit, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { AbpSessionService } from '@abp/session/abp-session.service';
import { AppSessionService } from '@shared/common/session/app-session.service';
import { AbpMultiTenancyService } from '@abp/multi-tenancy/abp-multi-tenancy.service';
import {
    ProfileServiceProxy,
    UserLinkServiceProxy,
    UserServiceProxy,
    LinkedUserDto,
    ChangeUserLanguageDto,
    TenantLoginInfoDto,
    GetCurrentLoginInformationsOutput,
    SessionServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LoginAttemptsModalComponent } from './login-attempts-modal.component';
import { LinkedAccountsModalComponent } from './linked-accounts-modal.component';
import { ChangePasswordModalComponent } from './profile/change-password-modal.component';
import { ChangeProfilePictureModalComponent } from './profile/change-profile-picture-modal.component';
import { MySettingsModalComponent } from './profile/my-settings-modal.component';
import { AppAuthService } from '@app/shared/common/auth/app-auth.service';
import { ImpersonationService } from '@app/admin/users/impersonation.service';
import { LinkedAccountService } from '@app/shared/layout/linked-account.service';
import { NotificationSettingsModalComponent } from '@app/shared/layout/notifications/notification-settings-modal.component';
import { UserNotificationHelper } from '@app/shared/layout/notifications/UserNotificationHelper';
//import { MyLocationModalComponent } from '../../main/locations/locationModal.component';
import { CreateOrEditUserModalComponent } from 'app/admin/users/create-or-edit-user-modal.component'
import { AppConsts } from '@shared/AppConsts';
import { SubscriptionStartType, EditionPaymentType } from '@shared/AppEnums';
import { MyLocationModalComponent } from './profile/my-location-modal.component';
import { FeedbackModalComponent } from './feedback/feedback.component';

import { Router } from '@angular/router';
import * as _ from 'lodash';
import * as moment from 'moment';
@Component({
    templateUrl: './header.component.html',
    selector: 'header-bar',
    encapsulation: ViewEncapsulation.None
})
export class HeaderComponent extends AppComponentBase implements OnInit {

    @ViewChild('notificationSettingsModal') notificationSettingsModal: NotificationSettingsModalComponent;

    @ViewChild('loginAttemptsModal') loginAttemptsModal: LoginAttemptsModalComponent;
    @ViewChild('linkedAccountsModal') linkedAccountsModal: LinkedAccountsModalComponent;
    @ViewChild('changePasswordModal') changePasswordModal: ChangePasswordModalComponent;
    @ViewChild('changeProfilePictureModal') changeProfilePictureModal: ChangeProfilePictureModalComponent;
    @ViewChild('mySettingsModal') mySettingsModal: MySettingsModalComponent;
    @ViewChild('myLocationModal') myLocationModal: MyLocationModalComponent;
    @ViewChild('createOrEditModal') mycreateOrEditModal: CreateOrEditUserModalComponent;
    @ViewChild('feedbackModal') feedbackModal: FeedbackModalComponent;
    languages: abp.localization.ILanguageInfo[];
    currentLanguage: abp.localization.ILanguageInfo;
    isImpersonatedLogin = false;
    isMultiTenancyEnabled: boolean = false;

    isAdmin: boolean = false;

    shownLoginNameTitle = '';
    shownLoginName = '';
    loginId = '';
    tenancyName = '';
    userName = '';
    userRole;
    tenantId = '';
    profilePicture = AppConsts.appBaseUrl + '/assets/common/images/default-profile-picture.png';
    defaultLogo = AppConsts.appBaseUrl + '/assets/common/images/app-logo-on-' + this.ui.getAsideSkin() + '.png';
    recentlyLinkedUsers: LinkedUserDto[];
    unreadChatMessageCount = 0;

    remoteServiceBaseUrl: string = AppConsts.remoteServiceBaseUrl;

    chatConnected = false;
    rolename: string = '';
    tenant: TenantLoginInfoDto = new TenantLoginInfoDto();
    subscriptionStartType = SubscriptionStartType;
    editionPaymentType: typeof EditionPaymentType = EditionPaymentType;

    constructor(
        injector: Injector,
        private _abpSessionService: AbpSessionService,
        private _abpMultiTenancyService: AbpMultiTenancyService,
        private _profileServiceProxy: ProfileServiceProxy,
        private _userLinkServiceProxy: UserLinkServiceProxy,
        private _userServiceProxy: UserServiceProxy,
        private _authService: AppAuthService,
        private _impersonationService: ImpersonationService,
        private _linkedAccountService: LinkedAccountService,
        private _userNotificationHelper: UserNotificationHelper,
        private _sessionService: SessionServiceProxy,
        private _appSessionService: AppSessionService,

        private _router: Router
    ) {
        super(injector);
    }

    get multiTenancySideIsTenant(): boolean {
        return this._abpSessionService.tenantId > 0;
    }

    ngOnInit() {
        this.isMultiTenancyEnabled = this._abpMultiTenancyService.isEnabled;
        this._userNotificationHelper.settingsModal = this.notificationSettingsModal;

        this.languages = _.filter(this.localization.languages, l => (<any>l).isDisabled === false);
        this.currentLanguage = this.localization.currentLanguage;
        this.isImpersonatedLogin = this._abpSessionService.impersonatorUserId > 0;

        this.shownLoginNameTitle = this.isImpersonatedLogin ? this.l('YouCanBackToYourAccount') : '';
        this.getCurrentLoginInformations();
        this.getProfilePicture();
        this.getRecentlyLinkedUsers();

        this.registerToEvents();
        this.getrolename();
    }

    registerToEvents() {
        abp.event.on('profilePictureChanged', () => {
            this.getProfilePicture();
        });

        abp.event.on('app.chat.unreadMessageCountChanged', messageCount => {
            this.unreadChatMessageCount = messageCount;
        });

        abp.event.on('app.chat.connected', () => {
            this.chatConnected = true;
        });
    }

    changeLanguage(languageName: string): void {
        const input = new ChangeUserLanguageDto();
        input.languageName = languageName;

        this._profileServiceProxy.changeLanguage(input).subscribe(() => {
            abp.utils.setCookieValue(
                'Abp.Localization.CultureName',
                languageName,
                new Date(new Date().getTime() + 5 * 365 * 86400000), //5 year
                abp.appPath
            );

            window.location.reload();
        });
    }

    getCurrentLoginInformations(): void {

      
        this.shownLoginName = this.appSession.getShownLoginName();
        this.tenancyName = this.appSession.tenancyName;
        this.userName = this.appSession.user.userName;
        this.userRole = this.appSession.user.userRole;


        this._sessionService.getCurrentLoginInformations()


            .subscribe((result: GetCurrentLoginInformationsOutput) => {
                this.tenant = result.tenant;
            });


        this._userServiceProxy.confirmUserRole(this.appSession.userId).subscribe((result) => {
            this.isAdmin = result;
        });

    }





    getShownUserName(linkedUser: LinkedUserDto): string {
        if (!this._abpMultiTenancyService.isEnabled) {
            return linkedUser.username;
        }

        return (linkedUser.tenantId ? linkedUser.tenancyName : '.') + '\\' + linkedUser.username;
    }
    getrolename() {
        this._userServiceProxy.getUserRoleById(this.appSession.user.userRole).subscribe(result => {
            this.rolename = result;

        });
    }
    getProfilePicture(): void {
        this._profileServiceProxy.getProfilePicture().subscribe(result => {
            if (result && result.profilePicture) {
                this.profilePicture = 'data:image/jpeg;base64,' + result.profilePicture;
            }
        });
    }

    getRecentlyLinkedUsers(): void {
        this._userLinkServiceProxy.getRecentlyUsedLinkedUsers().subscribe(result => {
            this.recentlyLinkedUsers = result.items;
        });
    }

    showLoginAttempts(): void {
        this.loginAttemptsModal.show();
    }

    showLinkedAccounts(): void {
        this.linkedAccountsModal.show();
    }

    changePassword(): void {
        this.changePasswordModal.show();
    }

    changeProfilePicture(): void {
        this.changeProfilePictureModal.show();
    }

    changeMySettings(): void {
        this.mySettingsModal.show();
    }

    logout(): void {
        this._authService.logout();
    }

    onMySettingsModalSaved(): void {
        this.shownLoginName = this.appSession.getShownLoginName();
    }

    backToMyAccount(): void {
        this._impersonationService.backToImpersonator();
    }

    switchToLinkedUser(linkedUser: LinkedUserDto): void {
        this._linkedAccountService.switchToAccount(linkedUser.id, linkedUser.tenantId);
    }

    get chatEnabled(): boolean {
        return this.appSession.application.features['SignalR'] && (!this._abpSessionService.tenantId || this.feature.isEnabled('App.ChatFeature'));
    }

    subscriptionStatusBarVisible(): boolean {
        return this._appSessionService.tenantId > 0 && (this._appSessionService.tenant.isInTrialPeriod || this.subscriptionIsExpiringSoon());
    }

    subscriptionIsExpiringSoon(): boolean {
        if (this._appSessionService.tenant.subscriptionEndDateUtc) {
            return moment().utc().add(AppConsts.subscriptionExpireNootifyDayCount, 'days') >= moment(this._appSessionService.tenant.subscriptionEndDateUtc);
        }

        return false;
    }

    getSubscriptionExpiringDayCount(): number {
        if (!this._appSessionService.tenant.subscriptionEndDateUtc) {
            return 0;
        }

        return Math.round(moment(this._appSessionService.tenant.subscriptionEndDateUtc).diff(moment().utc(), 'days', true));
    }

    getTrialSubscriptionNotification(): string {
        return abp.utils.formatString(this.l('TrialSubscriptionNotification'),
            '<strong>' + this._appSessionService.tenant.edition.displayName + '</strong>',
            '<a href=\'/account/buy?editionId=' + this._appSessionService.tenant.edition.id + '&editionPaymentType=' + this.editionPaymentType.BuyNow + '\'>' + this.l('ClickHere') + '</a>');
    }

    getExpireNotification(localizationKey: string): string {
        return abp.utils.formatString(this.l(localizationKey), this.getSubscriptionExpiringDayCount());
    }
    ShowUser() {
        //this.mycreateOrEditModal.show();
        this._router.navigate(['/app/admin/users']);
    }
    funActionAre() {
        this._router.navigate(['/app/main/ActionArea']);
    }
    i: number = 0;
    funLocation() {
        this.myLocationModal.show();
    }
    funSubActionArea() {
        this._router.navigate(['/app/main/SubAction']);
    }
    funComponents() {
        this._router.navigate(['/app/main/Components']);
    }
    funActionSubActionArea() {
        this._router.navigate(['/app/main/ActionSubAction']);
    }
    funDonorWsie() {
        this._router.navigate(['/app/main/DonorWsie']);
    }
    funregionWsie() {
        this._router.navigate(['/app/main/RegionWsie']);
    }
    funregionWsieRegionDetails() {
        this._router.navigate(['/app/main/RegionWiseDetails']);
    }
    funregionWsieRegionSumarry() {
        this._router.navigate(['/app/main/RegionWiseSumarry']);
    }
    funactionareawise() {
        this._router.navigate(['/app/main/ActionAreawise']);
    }
    funacoverAllAnualAction() {
        this._router.navigate(['/app/main/OverAllAnualAction']);
    }
    funclusterWiseReport() {
        this._router.navigate(['/app/main/ClusterWiseReport']);
    }
    funQuarterWiseReport() {
        this._router.navigate(['/app/main/QuarterWiseReport']);
    }
    funActivites() {
        this._router.navigate(['/app/main/Activity']);
    }
    funDonors() {
        this._router.navigate(['/app/main/Donor']);
    }
    funCreateProgram() {
       
        if (this.rolename == "ProgramManagerFinanceOrAdmin") {
            this._router.navigate(['/app/main/ProjectFund'], {
                queryParams: {
                    ProgrameName: '',
                    ProgrameId: 1,
                }
            });
        }
        else if (this.isGranted('Pages.RequestAAPlaSend')) {
            this._router.navigate(['/app/main/CreateProgramInformation']);
        } else {
           // this._router.navigate(['/app/main/RequestAAPla', '1']);

            this._router.navigate(['/app/main/RequestAAPla'], {
                queryParams: {
                    ProgrameName: '',
                    ProgrameId: 1,
                }
            });
        }
    }
    funManagePeople() {
        this._router.navigate(['/app/main/ManagePeople']);
    }
    funprogramdetails() {
        this._router.navigate(['/app/main/ProgramDetails']);
    }
    funoverallregion() {
        this._router.navigate(['/app/main/OverAllRegion']);
    }
    funyearwiseregion() {
        this._router.navigate(['/app/main/YearWiseRegion']);
    }

    funrequestsenddetails() {
        this._router.navigate(['/app/main/RequestSendDetails']);
    }
    funapproveappladetails() {
        this._router.navigate(['/app/main/ApproveAaaplaDetails']);
    }
    funImplemenationPlanGroupingDetails() {

        this._router.navigate(['/app/main/ImplemenationPlanGroupingDetails']);
    }
    funexpensesDetails() {

        this._router.navigate(['/app/main/ExpensesDetails']);
    }
    funprojectlist() {
        this._router.navigate(['/app/main/ProjectList']);
    }
    funProjectListIII() {
        this._router.navigate(['/app/main/ProjectListIII']);
    }

    funUnitOfMeasurMent() {
        this._router.navigate(['/app/main/UnitOfMeasurMent']);
    }
    funCheckList() {
        this._router.navigate(['/app/main/CheckList']);
    }
    funQue() {
        this._router.navigate(['/app/main/QuestionaryMapping'])
    }

    fundistrict() {
        this._router.navigate(['/app/main/District'])
    }

    funtaluka() {
        this._router.navigate(['/app/main/Taluka'])
    }
    funvillage() {
        this._router.navigate(['/app/main/Village'])
    }

    funIIForm() {
        this._router.navigate(['/app/main/ImpactIndicatorForm'])
    }

    funCrop() {
        this._router.navigate(['/app/main/Crop'])
    }

    //funFreqOfOccurrence() {
    //    this._router.navigate(['/app/main/FrequencyOfOccurrence'])

    //}

    funSource() {
        this._router.navigate(['/app/main/Source'])
    }
    funIIQuestionary() {
        this._router.navigate(['/app/main/ImpactIndicatorQuestionary'])
    }

    funIIFormAndProgramMapping() {
        this._router.navigate(['/app/main/ImpactIndicatorFormAndPogramMap'])
    }

    funIIResponseForm() {
        this._router.navigate(['/app/main/ImpactIndicatorResponseForm'])
    }

    funIIReport() {
        this._router.navigate(['/app/main/ImpactIndicatorReport'])
    }

    funIICategory() {
        this._router.navigate(['/app/main/Category'])
    }

    funIISubCategory() {
        this._router.navigate(['/app/main/SubCategory'])
    }  

    funDeleteProgram() {
        this._router.navigate(['/app/main/Prog'])
    }
    rolesPermissions() {

        this._router.navigate(['/app/admin/roles']);
    }

}
