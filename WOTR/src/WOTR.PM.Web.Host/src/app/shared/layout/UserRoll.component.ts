﻿import { Component, ViewChild, Injector, ElementRef, Output, EventEmitter } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize } from 'rxjs/operators';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    selector: 'UserRollsSelectModal',
    templateUrl: './UserRoll.component.html'
})
export class ShowUserRollcomponent extends AppComponentBase {

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('UserRollsSelectModal') modal: ModalDirective;
    active: boolean = false;
   

    constructor(
        injector: Injector,
        
    ) {
        super(injector);
    }

    show(): void {
        this.active = true;       
        this.modal.show();
    }

    onShown(): void {
      
    }

    save(): void {
       
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
}
