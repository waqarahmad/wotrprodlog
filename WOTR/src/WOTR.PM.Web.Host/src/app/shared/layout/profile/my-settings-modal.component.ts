
import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppConsts } from '@shared/AppConsts';
import { ProfileServiceProxy, CurrentUserProfileEditDto, DefaultTimezoneScope, UpdateGoogleAuthenticatorKeyOutput } from '@shared/service-proxies/service-proxies';
import { AppSessionService } from '@shared/common/session/app-session.service';
import { LocationServiceProxy, State, District, Taluka, Village, LocationListDto } from '@shared/service-proxies/service-proxies';
import { AppTimezoneScope } from '@shared/AppEnums';
import { SmsVerificationModalComponent } from './sms-verification-modal.component';
import { NgForm } from '@angular/forms';

//import { MysortPipe } from '../../../main/mysort.pipe';//yo modified
import { MysortPipe } from '../../common/mysort.pipe';//yo modified
import { CapitalizePipe } from '../../common/Capitalize.pipe';//yo modified


@Component({
    selector: 'mySettingsModal',
    templateUrl: './my-settings-modal.component.html'
})
export class MySettingsModalComponent extends AppComponentBase {
    @ViewChild('nameInput') nameInput: ElementRef;
    @ViewChild('mySettingsModal') modal: ModalDirective;
    @ViewChild('smsVerificationModal') smsVerificationModal: SmsVerificationModalComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    location: LocationListDto;
    locationList: LocationListDto[];
    stateList: State[];
    districtList: District[];
    talukaList: Taluka[];
    villageList: Village[];

    public active = false;
    public saving = false;
    public isGoogleAuthenticatorEnabled = false;
    public isPhoneNumberConfirmed: boolean;
    public isPhoneNumberEmpty = false;
    public smsEnabled: boolean;
    public user: CurrentUserProfileEditDto;
    public showTimezoneSelection: boolean = abp.clock.provider.supportsMultipleTimezone;
    public canChangeUserName: boolean;
    public defaultTimezoneScope: DefaultTimezoneScope = AppTimezoneScope.User;
    private _initialTimezone: string = undefined;

    @ViewChild('Modal') eForm: NgForm;//yo

    constructor(
        injector: Injector,
        private _profileService: ProfileServiceProxy,
        private _appSessionService: AppSessionService,
        private _locationService: LocationServiceProxy,
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.location = new LocationListDto();
        this.getAllState();
        //this.getAllVillage();
        //this.getAllDistrict(null);
        //this.getAllTaluka(null);
       
        //this.show();
    }


    ngAfterViewChecked(): void {
        //Temporary fix for: https://github.com/valor-software/ngx-bootstrap/issues/1508
        $('tabset ul.nav').addClass('m-tabs-line');
        $('tabset ul.nav li a.nav-link').addClass('m-tabs__link');
    }

    show(): void {
        this.active = true;
        this._profileService.getCurrentUserProfileForEdit().subscribe((result) => {
            this.smsEnabled = this.setting.getBoolean('App.UserManagement.SmsVerificationEnabled');
            this.user = result;
            this._initialTimezone = result.timezone;
            this.canChangeUserName = this.user.userName !== AppConsts.userManagement.defaultAdminUserName;
            this.modal.show();
            this.talukaList = [];
            this.districtList = [];
            
           // this.stateList = [];
            this.isGoogleAuthenticatorEnabled = result.isGoogleAuthenticatorEnabled;
            this.isPhoneNumberConfirmed = result.isPhoneNumberConfirmed;
            this.isPhoneNumberEmpty = result.phoneNumber === '';
        });
        this.location = new LocationListDto();
        this.location.locationType = 2;
        this.getAllLocation(this.location.locationType,16);
    }

    updateQrCodeSetupImageUrl(): void {
        this._profileService.updateGoogleAuthenticatorKey().subscribe((result: UpdateGoogleAuthenticatorKeyOutput) => {
            this.user.qrCodeSetupImageUrl = result.qrCodeSetupImageUrl;
            this.isGoogleAuthenticatorEnabled = true;
        });
    }

    smsVerify(): void {
        this._profileService.sendVerificationSms()
            .subscribe(() => {
                 this.smsVerificationModal.show();
        });
    }

    changePhoneNumberToVerified(): void {
        this.isPhoneNumberConfirmed = true;
    }

    onShown(): void {
        $(this.nameInput.nativeElement).focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
        this.eForm.reset();
    }

    save(): void {
        this.saving = true;
        this._profileService.updateCurrentUserProfile(this.user)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this._appSessionService.user.name = this.user.name;
                this._appSessionService.user.surname = this.user.surname;
                this._appSessionService.user.userName = this.user.userName;
                this._appSessionService.user.emailAddress = this.user.emailAddress;

                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);

                if (abp.clock.provider.supportsMultipleTimezone && this._initialTimezone !== this.user.timezone) {
                    this.message.info(this.l('TimeZoneSettingChangedRefreshPageNotification')).done(() => {
                        window.location.reload();
                    });
                }
            });
    }

    saveLocation() {
        this.saving = true;
        this.location.parentLocation = 1;

        //if (this.location.locationType==4) {
        //    this.location.name = this.villageList[0].name;
        //}
        this._locationService.createLocation(this.location)
            .finally(() => this.saving = false)
            .subscribe((result) => {
                this.notify.info(result[0]);
                this.close();
                this.modalSave.emit(null);
            })
    }
    checkLocationType(event) {
        this.location.locationType = event.target.value;
        this.getAllLocation(this.location.locationType,16);
        //yo modified
        if (event.target.value == 4) {
            $('#lblDistrict').show();
            $('#lblTaluka').show();
            $('#lblVillage').show();
            $('#txtLocation').hide();
        }
        if ( event.target.value == 3 ) {
            $('#lblDistrict').hide();
            $('#lblTaluka').hide();
            $('#lblVillage').hide();
        }
        if (event.target.value == 2) {
            $('#lblDistrict').hide();
            $('#lblTaluka').hide();
            $('#lblVillage').hide();
            $('#txtLocation').show();
        }

    }
    getAllLocation(locationType, StateId) {
        this._locationService.getAllParentLocations(locationType, StateId).subscribe(result => {
            this.locationList = result;
        })
    }
    getAllState() {
        this._locationService.getAllState().subscribe(result => {
            this.stateList = result;
        })
    }

    //modified 
    onChangeState(event) {
        this.talukaList = [];
        this.getAllDistrict(event.target.value);
        this.getAllLocation(this.location.locationType, event.target.value);
    }
    getAllDistrict(stateId)
    {
        this._locationService.getAllDistrict(stateId).subscribe(result => {
            this.districtList = result;
        })
    }


    onChangeDistrict(event)
    {
        this.talukaList = [];
        this.getAllTaluka(event.target.value);
    }

    getAllTaluka(districtId) {   
       
            this._locationService.getAllTaluka(districtId).subscribe(result => {
                this.talukaList = result;

            })
        
    }

    onChangeTaluka(event)
    {
        this.villageList = [];
        this.getAllVillage(event.target.value);
    }

    getAllVillage(talukaId)
    {
        this._locationService.getAllVillageByTalukaId(talukaId).subscribe(result => {
            this.villageList = result;
            
        })
    }

}
