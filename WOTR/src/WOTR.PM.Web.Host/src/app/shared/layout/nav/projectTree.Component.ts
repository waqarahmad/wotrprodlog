﻿import { Component, Injector, AfterViewInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { PrgActionAreaActivityMappingServiceProxy, OrganizationUnitDto, MoveOrganizationUnitInput } from '@shared/service-proxies/service-proxies';
import { Observable } from 'rxjs/Observable';
import { IBasicOrganizationUnitInfo } from '../../../admin/organization-units/basic-organization-unit-info';
import { IUserWithOrganizationUnit } from '../../../admin/organization-units/user-with-organization-unit';
import { IUsersWithOrganizationUnit } from '../../../admin/organization-units/users-with-organization-unit';
import { Router, ActivatedRoute, RoutesRecognized } from '@angular/router';
import * as _ from "lodash";
import { ModalDirective } from 'ngx-bootstrap';
import { AppSessionService } from '@shared/common/session/app-session.service';
export interface ILocationsOnTree {
    id: string;
    parent: string | number;
    displayName: string;
    memberCount: number,
    text: string;
    state: any;
    icon: string;
}

@Component({
    selector: 'projectTree',
    templateUrl: "./tree.component.html",
    styleUrls: ["./tree.component.less"]})
export class ProjectTreeComponent extends AppComponentBase implements AfterViewInit {

    @Output() ouSelected = new EventEmitter<IBasicOrganizationUnitInfo>();
    @ViewChild('tree') tree: ElementRef;
    private _$tree: JQuery;
    userId: number;
    private treeData = [];

    constructor(
        injector: Injector,
        private _router: Router,
        private _PrgActionAreaActivityMappingService: PrgActionAreaActivityMappingServiceProxy
    ) {
        super(injector);

    }
   
    totalUnitCount: number = 0;

    ngOnInit() {
        this._router.events.subscribe(val => {
            if (val instanceof RoutesRecognized) {
                var values = val.url.split("?");
                for (let i of values) {
                    if (i == "/app/main/ProgramCluster" || i == "/app/main/ProgramCostEstimation") {
                        this.reload();
                    }
                }
            }
        });
        this.getCurrentLoginInformations();
    }

    getCurrentLoginInformations(): any {
        this.userId = this.appSession.user.id;
    }

  

    ngAfterViewInit(): void {   
        var self = this;
        this._$tree = $(this.tree.nativeElement);
        this.treeData = [
            { "id": "192.168.0.0", "parent": "#", "text": "192.168.0.0" },
            { "id": "192.168.0.1", "parent": "192.168.0.0", "text": "192.168.0.1" },
            { "id": "192.168.2.1", "parent": "192.168.0.1", "text": "192.168.2.1", "icon": "/" },
            { "id": "192.168.10.0", "parent": "#", "text": "192.168.10.0" },
            { "id": "192.168.10.1", "parent": "192.168.10.0", "text": "192.168.10.1" },
            { "id": "192.168.20.0", "parent": "#", "text": "192.168.20.0" },
            { "id": "192.168.20.1", "parent": "192.168.20.0", "text": "192.168.20.1" },
        ];
        this.getTreeDataFromServer(treeData => {
            this._$tree
                .on('changed.jstree', (e, data) => {
                    abp.ui.setBusy();
                    this._PrgActionAreaActivityMappingService.getUserRoleNameForEdit(this.appSession.user.userRole).subscribe(res => {


                        if (data.selected.length !== 1) {

                        }
                        else if (data.node.original.parent == '#' && res.roleName == "ProgramManagerFinanceOrAdmin" && data.event != undefined) {
                            this._router.navigate(['/app/main/ProjectFund'], {
                                queryParams: {
                                    ProgrameName: data.node.original.displayName,
                                    ProgrameId: data.node.original.id,
                                }
                            });
                        }
                        else if (data.node.original.parent == '#' && this.isGranted('Pages.RequestAAPlaSend') && data.event != undefined) {
                            this._router.navigate(['/app/main/ApproveAAPla'], {
                                queryParams: {
                                    ProgrameName: data.node.original.displayName,
                                    ProgrameId: data.node.original.id,
                                }
                            });
                        }
                        else if (data.event != undefined) {
                            this._router.navigate(['/app/main/RequestAAPla'], {
                                queryParams: {
                                    ProgrameName: data.node.original.displayName,
                                    ProgrameId: data.node.original.id,
                                }
                            });
                        }
                        abp.ui.clearBusy();
                    })
                })
                .jstree({
                    'core': {
                        'data': treeData,
                        "themes": { "stripes": false }
                    }
                });

        });
    }

    loc: any;
    reload(): void {
        this.getTreeDataFromServer(treeData => {
            this.totalUnitCount = treeData.length;
            (<any>this._$tree.jstree(true)).settings.core.data = treeData;
            this._$tree.jstree('refresh');
        });
    }
    inputJstree: string;
    inputType: string = "";
    show(input, inputType) {
        this.inputJstree = input;
        this.inputType = inputType;
        this.reload();
    }
    private getTreeDataFromServer(callback: (ous: ILocationsOnTree[]) => void): void {
       this._PrgActionAreaActivityMappingService.getAllProgrameComponentByRoleId(this.inputJstree, this.userId, this.appSession.user.userRole).subscribe(result => {
            this.loc = [];
            for (let r of result) {
                var treeData = {
                    id: r.prgComponentActivityId,
                    parent: r.pranteId ? r.pranteId : '#',
                    displayName: r.name,
                    state: {
                        opened: this.inputType == "search" ? true : false
                    },
                    text: r.name
                }
                this.loc.push(treeData);
            }
          

            for (let i = 0; i < this.loc.length; i++) {
                if (this.loc[i].parent == '1') {
                    this.loc[i].icon = "../../../../assets/common/images/Group41451.png";
                }
                else {
                    this.loc[i].icon = "../../../../assets/common/images/Group41451.png";

                }
            }
            callback(this.loc);
        });
    }
}