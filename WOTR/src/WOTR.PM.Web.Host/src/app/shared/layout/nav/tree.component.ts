﻿import { Component, Injector, AfterViewInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LocationServiceProxy, LocationListDto, OrganizationUnitDto, MoveOrganizationUnitInput } from '@shared/service-proxies/service-proxies';
import { Observable } from 'rxjs/Observable';
import { IBasicOrganizationUnitInfo } from '../../../admin/organization-units/basic-organization-unit-info';
import { IUserWithOrganizationUnit } from '../../../admin/organization-units/user-with-organization-unit';
import { IUsersWithOrganizationUnit } from '../../../admin/organization-units/users-with-organization-unit';

import * as _ from "lodash";

export interface ILocationsOnTree {
    id: string;
    parent: string | number;
    displayName: string;
    memberCount: number,
    text: string;
    state: any;
    icon: string;
}

@Component({
    selector: 'tree',
    templateUrl: "./tree.component.html",
    styleUrls: ["./tree.component.less"]
})
export class TreeComponent extends AppComponentBase implements AfterViewInit {

    @Output() ouSelected = new EventEmitter<IBasicOrganizationUnitInfo>();

    @ViewChild('tree') tree: ElementRef;

    private _$tree: JQuery;
    private _updatingNode: any;
    private treeData = [];

    locationData: LocationListDto[] = [];

    constructor(
        injector: Injector,
        private _locationService: LocationServiceProxy
    ) {
        super(injector);

    }

    totalUnitCount: number = 0;


    ngAfterViewInit(): void {
        var self = this;
        this._$tree = $(this.tree.nativeElement);
        this.treeData = [
            { "id": "192.168.0.0", "parent": "#", "text": "192.168.0.0" },
            { "id": "192.168.0.1", "parent": "192.168.0.0", "text": "192.168.0.1" },
            { "id": "192.168.2.1", "parent": "192.168.0.1", "text": "192.168.2.1", "icon": "/" },
            { "id": "192.168.10.0", "parent": "#", "text": "192.168.10.0" },
            { "id": "192.168.10.1", "parent": "192.168.10.0", "text": "192.168.10.1" },
            { "id": "192.168.20.0", "parent": "#", "text": "192.168.20.0" },
            { "id": "192.168.20.1", "parent": "192.168.20.0", "text": "192.168.20.1" },
        ];
        this.getTreeDataFromServer(treeData => {
            this._$tree.jstree({
                'core': {
                    'data': treeData,
                    "themes": { "stripes": false }
                }
            });
        });

        // 7 bind to events triggered on the tree
        this._$tree.on("changed.jstree", function (e, data) {
        });
        // 8 interact with the tree - either way is OK
        $('#button1').on('click', function () {
            $('#tree').jstree(true).select_node('child_node_1');
            $('#tree').jstree('select_node', 'child_node_1');
            $.jstree.reference('#tree').select_node('child_node_1');
        });
        $('#button2').on('click', function () {
            $('#tree').jstree(true).select_node('child_node_2');
            $('#tree').jstree('select_node', 'child_node_2');
            $.jstree.reference('#tree').select_node('child_node_2');
        });
    }
    loc: any;
    reload(): void {
        this.getTreeDataFromServer(treeData => {
            this.totalUnitCount = treeData.length;
            (<any>this._$tree.jstree(true)).settings.core.data = treeData;
            this._$tree.jstree('refresh');
        });
    }


    inputJstree: string;
    inputType: string = " ";
    show(input, inputType) {
        this.inputJstree = input;
        this.inputType = inputType;
        this.reload();
    }
    private getTreeDataFromServer(callback: (ous: ILocationsOnTree[]) => void): void {
        this._locationService.getAllLocations().subscribe((result) => {

            this.locationData = result;

            this.loc = [];
            for (let r of result) {

                var treeData = {
                    id: r.id,
                    parent: r.parentLocation ? r.parentLocation : '#',
                    displayName: r.name,
                    state: {
                        opened: false
                    },
                    text: r.name
                }
                this.loc.push(treeData);
            }


            for (let i = 0; i < this.loc.length; i++) {
                if (this.loc[i].parent == '1') {
                    this.loc[i].icon = "../../../../assets/common/images/Location_icon_White.png";
                }
                else {
                    this.loc[i].icon = "../../../../assets/common/images/Location_icon_White.png";

                }
            }

            callback(this.loc);

        });
    }

}