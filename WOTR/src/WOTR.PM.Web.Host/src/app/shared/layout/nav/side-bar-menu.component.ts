import { Component, Injector, OnInit, ViewEncapsulation,ViewChild,ElementRef } from '@angular/core';
import { AppMenu } from './app-menu';
import { LocationServiceProxy, LocationListDto, OrganizationUnitDto, MoveOrganizationUnitInput, PrgActionAreaActivityMappingServiceProxy } from '@shared/service-proxies/service-proxies';
import { PermissionCheckerService } from '@abp/auth/permission-checker.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppSessionService } from '@shared/common/session/app-session.service';
import { AppUiCustomizationService } from '@shared/common/ui/app-ui-customization.service';
import { AppNavigationService } from './app-navigation.service';
import { ProjectTreeComponent } from './projectTree.Component';

import { TreeComponent } from './tree.component';
import { PeopleTreeComponent } from './peopleTree.Component'
import { Router } from '@angular/router';

export interface ILocationsOnTree {
    id: string;
    parent: string | number;
    displayName: string;
    memberCount: number,
    text: string;
    state: any;
    icon: string;
}

@Component({
    templateUrl: './side-bar-menu.component.html',
    selector: 'side-bar-menu',
    encapsulation: ViewEncapsulation.None
})
export class SideBarMenuComponent extends AppComponentBase implements OnInit {
    @ViewChild('projectTree') projectTree: ProjectTreeComponent;

    @ViewChild('tree') tree: TreeComponent;
    @ViewChild('peopleTree') peopleTree: PeopleTreeComponent;
    menu: AppMenu = null; 

    constructor(
        injector: Injector,
        public permission: PermissionCheckerService,
        private _appSessionService: AppSessionService,
        private _uiCustomizationService: AppUiCustomizationService,
        private _router: Router,
        private _appNavigationService: AppNavigationService,
        private _locationService: LocationServiceProxy,
        private _PrgActionAreaActivityMappingService: PrgActionAreaActivityMappingServiceProxy) {
        super(injector);
    }

    ngOnInit() {
        $('.showTree').hide();
        $('.peopleTree').hide();
       
        this.menu = this._appNavigationService.getMenu();
    }   
   

    showMenuItem(menuItem): boolean {
        if (menuItem.permissionName === 'Pages.Administration.Tenant.SubscriptionManagement' && this._appSessionService.tenant && !this._appSessionService.tenant.edition) {
            return false;
        }

        if (menuItem.permissionName) {
            return this.permission.isGranted(menuItem.permissionName);
        }

        if (menuItem.items && menuItem.items.length) {
            return this._appNavigationService.checkChildMenuItemPermission(menuItem);
        }

        return true;
    }

    displayLocationTree(): void {       
        $('.projectTree').hide();
        $('.peopleTree').hide();
        $('.showTree').show();

        var locationproperty = document.getElementById('locationbtn');
        var teamsproperty = document.getElementById('teamsbtn');
        var peopleproperty = document.getElementById('Peoplebtn');

        locationproperty.style.backgroundColor = "#01B6E6";
        locationproperty.style.color = "White";

        teamsproperty.style.backgroundColor = "#364150";
        teamsproperty.style.color = "#01B6E6";

        peopleproperty.style.backgroundColor = "#364150";
        peopleproperty.style.color = "#01B6E6";
    }

    GetSearchResult(event) {
        var input = (document.getElementById("FilterTreeText") as HTMLInputElement).value;
        this.projectTree.show(input, "search")
       this.peopleTree.show(input, "search");
    }
    displayTeamsTree(): void {
      //  $('.showTree').hide();
        $('.projectTree').show();
      
        $('.peopleTree').hide();

        //var locationproperty = document.getElementById('locationbtn');
        var teamsproperty = document.getElementById('teamsbtn');
        var peopleproperty = document.getElementById('Peoplebtn');

        teamsproperty.style.backgroundColor = "#01B6E6";
        teamsproperty.style.color = "White";

        //locationproperty.style.backgroundColor = "#364150";
        //locationproperty.style.color = "#01B6E6";

        peopleproperty.style.backgroundColor = "#364150";
        peopleproperty.style.color = "#01B6E6";
    }

    displayPeople(): void {
       // $('.showTree').hide();
        $('.peopleTree').show();
        $('.projectTree').hide();

        //var locationproperty = document.getElementById('locationbtn');
        var teamsproperty = document.getElementById('teamsbtn');
        var peopleproperty = document.getElementById('Peoplebtn');

        peopleproperty.style.backgroundColor = "#01B6E6";
        peopleproperty.style.color = "White";

       // locationproperty.style.backgroundColor = "#364150";
        //locationproperty.style.color = "#01B6E6";

        teamsproperty.style.backgroundColor = "#364150";
        teamsproperty.style.color = "#01B6E6";
    }
}
