import { Component, ViewChild, Injector, Output, EventEmitter, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import {
    UserServiceProxy, ProfileServiceProxy, UserEditDto, CreateOrUpdateUserInput, OrganizationUnitDto,
    UserRoleDto, PasswordComplexitySetting, LocationServiceProxy, LocationListDto, EmailServiceProxy, ActionAreasListDto, ActionAreasServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppConsts } from '@shared/AppConsts';
import { OrganizationUnitsTreeComponent, IOrganizationUnitsTreeComponentData } from '../shared/organization-unit-tree.component';

import * as _ from 'lodash';
import { NgForm } from '@angular/forms';
import { MysortPipe } from '../../shared/common/mysort.pipe';//yo modified
import { FileUploader, FileUploaderOptions, Headers } from 'ng2-file-upload';
import { UpdateProfilePictureInput } from '@shared/service-proxies/service-proxies';



@Component({
    selector: 'createOrEditUserModal',
    templateUrl: './create-or-edit-user-modal.component.html',
    styles: [`.user-edit-dialog-profile-image {
             margin-bottom: 20px;
        }`
    ]
})
export class CreateOrEditUserModalComponent extends AppComponentBase {
    logo = AppConsts.appBaseUrl + '/assets/common/images/default-profile-picture.png';
    us: any;
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: UserRoleDto[] = [];
    setlocation: string = "Location";
    
    filesToUpload: Array<File>;
    @ViewChild('nameInput') nameInput: ElementRef;
    @ViewChild('createOrEditModal') modal: ModalDirective;
    @ViewChild('organizationUnitTree') organizationUnitTree: OrganizationUnitsTreeComponent;
    
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    locationList: LocationListDto[];
    RRClist: LocationListDto[];
    ActionArealist: ActionAreasListDto[];
    active = false;
    saving = false;
    canChangeUserName = true;
    isTwoFactorEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.TwoFactorLogin.IsEnabled');
    isLockoutEnabled: boolean = this.setting.getBoolean('Abp.Zero.UserManagement.UserLockOut.IsEnabled');
    passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting();
    profilePicture = AppConsts.appBaseUrl + '/assets/common/images/default-profile-picture.png';

    user: UserEditDto = new UserEditDto();
    actionarea: ActionAreasListDto = new ActionAreasListDto();
    roles: UserRoleDto[];
    sendActivationEmail = true;
    setRandomPassword = true;
    passwordComplexityInfo = '';
    // profilePicture: string;
    emailContent: string;
    private maxProfilPictureBytesUserFriendlyValue = 5;
    private temporaryPictureFileName: string;
    private _uploaderOptions: FileUploaderOptions = {};
    private _$profilePictureResize: JQuery;
    private _$jcropApi: any;
    //yo modification
    //@ViewChild('Modal') userForm: NgForm;
    //editFlag: boolean = false;
    display: any;

    allOrganizationUnits: OrganizationUnitDto[];
    memberedOrganizationUnits: string[];
    showActionDropDown = false;

    constructor(
        injector: Injector,
        private _userService: UserServiceProxy,
        private _profileService: ProfileServiceProxy,
        private _locationService: LocationServiceProxy,
        private _emailServiceProxy: EmailServiceProxy,
        private _actionareaService: ActionAreasServiceProxy,
    ) {
        super(injector);
    }


    ngAfterViewChecked(): void {
        //Temporary fix for: https://github.com/valor-software/ngx-bootstrap/issues/1508
        $('tabset ul.nav').addClass('m-tabs-line');
        $('tabset ul.nav li a.nav-link').addClass('m-tabs__link');
    }

    show(userId?: number): void {
        if (!userId) {
            this.active = true;
            this.setRandomPassword = true;
            this.sendActivationEmail = true;
        }

        this._userService.getUserForEdit(userId).subscribe(userResult => {
            this.user = userResult.user;
            if (this.user.actionId) {
                this.showActionDropDown = true;
            }
            this.roles = userResult.roles;
            this.canChangeUserName = this.user.userName !== AppConsts.userManagement.defaultAdminUserName;

            this.allOrganizationUnits = userResult.allOrganizationUnits;
            this.memberedOrganizationUnits = userResult.memberedOrganizationUnits;

            this.getProfilePicture(userResult.profilePictureId);

            if (userId) {
                this.active = true;

                setTimeout(() => {
                    this.setRandomPassword = true;
                }, 0);

                this.sendActivationEmail = false;
            }

            this._profileService.getPasswordComplexitySetting().subscribe(passwordComplexityResult => {
                this.passwordComplexitySetting = passwordComplexityResult.setting;
                this.setPasswordComplexityInfo();
                this.modal.show();
            });
        });
    }

    setPasswordComplexityInfo(): void {

        this.passwordComplexityInfo = '<ul>';

        if (this.passwordComplexitySetting.requireDigit) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireDigit_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireLowercase) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireLowercase_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireUppercase) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireUppercase_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requireNonAlphanumeric) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequireNonAlphanumeric_Hint') + '</li>';
        }

        if (this.passwordComplexitySetting.requiredLength) {
            this.passwordComplexityInfo += '<li>' + this.l('PasswordComplexity_RequiredLength_Hint', this.passwordComplexitySetting.requiredLength) + '</li>';
        }

        this.passwordComplexityInfo += '</ul>';
    }

    getProfilePicture(profilePictureId: string): void {
        if (!profilePictureId) {
            this.profilePicture = '/assets/common/images/default-profile-picture.png';
        } else {
            this._profileService.getProfilePictureById(profilePictureId).subscribe(result => {

                if (result && result.profilePicture) {
                    this.profilePicture = 'data:image/jpeg;base64,' + result.profilePicture;
                } else {
                    this.profilePicture = '/assets/common/images/default-profile-picture.png';
                }
            });
        }
    }

    onShown(): void {
        $(this.nameInput.nativeElement).focus();
        this.getAllLoactions();
        this.getAllRRclist();
        this.getActionArealist();
        this.user.locationId = this.user.locationId;
    }
    onchangeLocation(event) {
        this.user.locationId = event.target.value;
    }
   
    UserNameshow(u) {
        if (u.surname == undefined) {
            u.userName = u.name;
        }
        else {
            u.username = u.name + "." + u.surname;
            this.user.userName = u.name + "." + u.surname;
        }   
    }

    save(): void { 
        let input = new CreateOrUpdateUserInput();
        input.user = this.user;
       // input.user = this.actionarea.code;
        input.setRandomPassword = this.setRandomPassword;
        input.sendActivationEmail = this.sendActivationEmail;        
        input.assignedRoleNames =
            _.map(
            _.filter(this.roles, { isAssigned: true }), role => role.roleName//// changed isAssigned to isActive
            );
        this.saving = true;
        try {
            this._userService.createOrUpdateUser(input)
                .finally(() => { this.saving = false; })
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                    this.emailContent = "Dear " + input.user.name + ",\n" + "<h4>Thank you for registration</h4>" +
                        "<ul><li><span class='text-muted'> User Name : </span>" + input.user.userName + "</li>" +
                        "<li><span class='text-muted' > Password : Welcome1 </span></li> " +
                        "</ul> <div>" + "<h1>Please click here for login</h1>\n <div>https://wotrstaging.azurewebsites.net</div>";

                    this._emailServiceProxy.mailSending(this.emailContent, 'Login Details for WOTR Annual Action Plan', input.user.emailAddress, null, null)
                        .subscribe((result) => {
                        });
                });

        }
        catch(exception) {

        }
    }

    close(): void {
        this.active = false;
        this.showActionDropDown = false;
        this.modal.hide();
    }

    getAssignedRoleCount(): number {
        
        return _.filter(this.roles, { isAssigned: true }).length;//changed  isAssigned to isActive
    }
    getAllLoactions() {
        this._locationService.getAllLocations().subscribe(result => {
            this.locationList = result;

        })
    }
    getAllRRclist() {
        this._locationService.getAllRRcList().subscribe(result => {
            this.RRClist = result;
        })
    }
    getActionArealist() {
        this._actionareaService.getAllActionAreaRecord().subscribe(result => {
            this.ActionArealist = result;
        })
    }

    

    checkValue(e, val) {
        //if (e == "ActionAreaManager") {
        //    this.showActionDropDown = val;          
        //}
    }   

    upload() {

        this.us.makeFileRequest("http://localhost:5000/api/SampleData/Upload", this.filesToUpload)
            .then((result) => {
                console.log(result);
            }, (error) => {
                console.error(error);
            });
    }

    onFileChange(fileInput: any) {
        this.profilePicture = fileInput.target.files[0];

        let reader = new FileReader();

        reader.onload = (e: any) => {
            this.profilePicture = e.target.result;
        }

        reader.readAsDataURL(fileInput.target.files[0]);
    }


    importFile(event) {

        if (event.target.files.length == 0) {
            console.log("No file selected!");
            return
        }
        let file: File = event.target.files[0];
        // after here 'file' can be accessed and used for further process
    }



    changePage(p: any) {
        alert(p.rows);
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
}
   