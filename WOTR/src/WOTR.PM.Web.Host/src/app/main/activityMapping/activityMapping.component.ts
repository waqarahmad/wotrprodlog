﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import {
    PrgActionAreaActivityMappingServiceProxy, PrgActionAreaActivityMappingRepositoryDto,
    ActivityActionandSubactionArea, PrgActionAreaJSTreeDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';
//import { activitymappingtreecomponent } from './activitymappingtree.component'

declare var jQuery: any;
export interface ILocationsOnTree {
    id: string;
    parent: string | number;
    displayName: string;
    memberCount: number,
    text: string;
    state: any;
    icon: string;
}

@Component({
    selector: 'activitymapping',
    templateUrl: "./activityMapping.component.html",
    styleUrls: ['./activityMapping.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ActivityMappingComponent extends AppComponentBase {
    @ViewChild('activityMappingTree') tree: ElementRef;

    ActivityMappingRecord: PrgActionAreaActivityMappingRepositoryDto[]=[]   ;
    Activity: ActivityActionandSubactionArea;
    jsTree: PrgActionAreaJSTreeDto;

    private _$tree: JQuery;
    private _updatingNode: any;
    private treeData = [];

    actionActivityJsTree: PrgActionAreaJSTreeDto[] = [];

    saving: boolean = false;
    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _prgActionAreaActivityMappingServiceProxy: PrgActionAreaActivityMappingServiceProxy,

    ) {
        super(injector);

    }
    ngOnInit(): void {
        this.funPanel();
        this.getAllProgramActivityMapping();
    }
    ngAfterViewInit(): void {
        this.funPanel();

        var self = this;
        this._$tree = $(this.tree.nativeElement);
        this.treeData = [
            { "id": "192.168.0.0", "parent": "#", "text": "192.168.0.0" },
            { "id": "192.168.0.1", "parent": "192.168.0.0", "text": "192.168.0.1" },
            { "id": "192.168.2.1", "parent": "192.168.0.1", "text": "192.168.2.1", "icon": "/" },
            { "id": "192.168.10.0", "parent": "#", "text": "192.168.10.0" },
            { "id": "192.168.10.1", "parent": "192.168.10.0", "text": "192.168.10.1" },
            { "id": "192.168.20.0", "parent": "#", "text": "192.168.20.0" },
            { "id": "192.168.20.1", "parent": "192.168.20.0", "text": "192.168.20.1" },
        ];
        this.getTreeDataFromServer(treeData => {
            console.log("LocationTreeData", treeData);
            this._$tree.jstree({
                'core': {
                    'data': treeData,
                    "themes": { "stripes": false }
                }
            });
        });

    }

    funPanel() {
        $('.panel-collapse').on('show.bs.collapse', function () {
            $(this).siblings('.panel-heading').addClass('active');
        });

        $('.panel-collapse').on('hide.bs.collapse', function () {
            $(this).siblings('.panel-heading').removeClass('active');
        });
    }

    funProgramCluster() {
        this._router.navigate(['/app/main/ProgramCluster'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }




    funComponents() {
        this._router.navigate(['/app/main/ProgramComponent'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funProgramCostEstimation() {
        this._router.navigate(['/app/main/ProgramCostEstimation'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funRegionSpending() {
        this._router.navigate(['/app/main/RegionSpending'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }
   
    AddTree(res, event) {
        this.saving = true;
        var tre = event.target.value;

        if (tre.includes(",")) {
            var values = tre.split(",");
            res.actionAreaID = values[0];
            res.subActionAreaID = values[1];
            this.jsTree = new PrgActionAreaJSTreeDto();
            this.jsTree.actionAreaID = values[0];
            this.jsTree.subActionAreaID = values[1];
            this.jsTree.activityID = res.activityID;
            this.jsTree.componentID = res.componentID;
            this.jsTree.programID = res.programID;

            this._prgActionAreaActivityMappingServiceProxy.createOrUpdateActivityActionAreaJSTree(this.jsTree)
                .finally(() => this.saving = false)
                .subscribe(result => {
                    this.reload();             
                })
        }
    }


    getAllProgramActivityMapping() {
        var ProgrammeId = this._activatedRoute.snapshot.queryParams.ProgrameId;
        this._prgActionAreaActivityMappingServiceProxy.getAllprogramActionAreaActivity(ProgrammeId).subscribe(
            (result) => {
                this.ActivityMappingRecord = result;
            });
    }

    loc: any;
    totalUnitCount: number = 0;   
    reload(): void {
        this.getTreeDataFromServer(treeData => {
            this.totalUnitCount = treeData.length;
            (<any>this._$tree.jstree(true)).settings.core.data = treeData;
            this._$tree.jstree('refresh');
        });
    }

    private getTreeDataFromServer(callback: (ous: ILocationsOnTree[]) => void): void {
        var ProgrammeId = this._activatedRoute.snapshot.queryParams.ProgrameId;
        this._prgActionAreaActivityMappingServiceProxy.getAllJsTree(ProgrammeId).subscribe((result) => {
            this.actionActivityJsTree = result;
            this.loc = [];
            for (let r of result) {
                var treeData = {
                    id: r.id,
                    parent: r.pranteId ? r.pranteId : '#',
                    displayName: r.name,
                    state: {
                        opened: false
                    },
                    text: r.name
                }
                this.loc.push(treeData);
            }

            callback(this.loc);

        });
    }



    AddActivity(record, res) {
        var i = 0;
        for (let v of res.activity) {
            if (v.activityID == record.activityID) {
                i = i + 1;
            }
        }
        this.Activity = new ActivityActionandSubactionArea();

        this.Activity.actionAreaID = null;
        this.Activity.subActionAreaID = null;
        this.Activity.activityName = record.activityName;
        this.Activity.actionSubAction = record.actionSubAction;
        this.Activity.activityID = record.activityID;
        this.Activity.componentID = record.componentID;
        this.Activity.programID = record.programID;

        if (record.actionSubAction.length >= 1 && i < record.actionSubAction.length) {
            res.activity.push(this.Activity);
        }
        else {
            alert("not found");
        }
    }

    save() {
        this.saving = true;
        this._prgActionAreaActivityMappingServiceProxy.createOrUpdatePrgActivityMapping(this.ActivityMappingRecord)
            .finally(() => this.saving = false)
            .subscribe(result => {
                this.notify.info(result[0]);
                this.funRegionSpending();

            })
    }


}
