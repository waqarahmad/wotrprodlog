﻿import { Component, Injector, ViewChild, EventEmitter, Output } from '@angular/core';
import { ComponentServiceProxy, GetComponentActivityMappingListDto, ComponentActivityMappingServiceProxy, ActivityServiceProxy, ComponentActivityMapListDto, ActivitiyListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
declare var jQuery: any;


@Component({
    selector: 'ComponentsAddModal',
    templateUrl: "./componentsAddModal.component.html",
    styleUrls: ['./componentsAddModal.component.less'],
})
export class ComponentsAddModalComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('ComponentsAddModal') modal: ModalDirective;
    active: boolean = false;
    saving: boolean = false;
    component: GetComponentActivityMappingListDto;
    code: number;
    selectedItems: any;
    settings: any;
    componentActivityMap: ComponentActivityMapListDto = new ComponentActivityMapListDto;
    ddlshow: boolean = false;
    activityList: ActivitiyListDto[];
    itemList: any;
    ActivityId: number[] = [];

    constructor(
        injector: Injector,
        private _activityService: ActivityServiceProxy,
        private _componentService: ComponentServiceProxy,
        private _componentActivityService: ComponentActivityMappingServiceProxy) {
        super(injector);

    }

    getcode() {
        this._componentService.getComponentCode().subscribe((result) => {
            this.code = result;
            this.code++;
            this.component = new GetComponentActivityMappingListDto();
            this.component.code = this.code;
        });
    }

    show(p): void {
        this.active = true;
        if (p != undefined) {
            this.ddlshow = true;
            $('#lblTitle').text('Edit Components');
            this.component = p;
            this.settings = {
                singleSelection: false,
                text: "Select Activities",
                selectAllText: 'Select All',
                unSelectAllText: 'UnSelect All',
                enableSearchFilter: true,
                classes: "myclass custom-class"
            };
            this.getAllActivity();
        }
        else {
            this.getcode();
            this.ddlshow = false;
            $('#lblTitle').text('Add Components');
        }
        this.modal.show();
    }


    save() {
        if (this.component.componetTableId != undefined) {
            this.componentActivityMap.actID = this.ActivityId;
            this.componentActivityMap.componentID = this.component.componetTableId;
            this.componentActivityMap.componentName = this.component.componentName;
            this.componentActivityMap.componentCode = this.component.code;
            this._componentActivityService.createComponentActivityMapping(this.componentActivityMap)
                .finally(() => this.saving = false).subscribe(result => {
                    this.notify.info(result[0]);
                    this.close();
                    this.modalSave.emit(null);
                });
        }
        else {
            this._componentService.createComponent(this.component)
                .finally(() => this.saving = false)
                .subscribe((result) => {
                    this.notify.info(result[0]);
                    this.close();
                    this.modalSave.emit(null);
                })
        }
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }

    getAllActivity() {
        this.itemList = [];
        this._activityService.getAllActivities("").subscribe(result => {
            this.activityList = result.sort();
            for (let a of result) {
                this.itemList.push({
                    id: a.id,
                    itemName: a.name
                })
            }
        });
    }


    onItemSelect(item: any) {
        this.ActivityId.push(item.id);
    }

    OnItemDeSelect(item: any) {
        var index = this.ActivityId.indexOf(item.id, 0);
        if (index > -1) {
            this.ActivityId.splice(index, 1);
        }
    }

    onSelectAll(items: any) {
        for (let obj of items) {
            this.ActivityId.push(obj.id);
        }
    }

    onDeSelectAll(items: any) {
        this.ActivityId = [];
    }
}