﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { ComponentServiceProxy, ComponentListDto, ComponentActivityMapListDto, GetComponentActivityMappingListDto, ComponentActivityMappingServiceProxy, ActivityServiceProxy, ActivitiyListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ComponentsAddModalComponent } from './componentsAddModal.component';
import { ModalDirective } from 'ngx-bootstrap';

import { MysortPipe } from '../../shared/common/mysort.pipe';//yo modified for Sorting Drop Downs


declare var jQuery: any;


@Component({
    selector: 'componentscomponent',
    templateUrl: "./components.component.html",
    styleUrls: ['./components.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class componentsComponent extends AppComponentBase {
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('ComponentsAddModal') myModalView: ComponentsAddModalComponent;
    componentActivityList: GetComponentActivityMappingListDto[];
    activityList: ActivitiyListDto[];
    componentActivityMap: ComponentActivityMapListDto = new ComponentActivityMapListDto;
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    setFlag: number = 0;
    saving: boolean = false;
    selectedItems: any;
    settings: any;
    constructor(
        injector: Injector,
        private _componentService: ComponentServiceProxy,
        private _activityService: ActivityServiceProxy,
        private _componentActivityService: ComponentActivityMappingServiceProxy

    ) {
        super(injector);
    }

    ngOnInit() {
        this.settings = {
            singleSelection: false,
            text: "Select Activities",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        };          
    }

    ngAfterViewInit(): void {
        this.getAllComponents();
        //this.getAllActivity();
    }

    compActivity: any;
    componentAct: any;
    componentName: string;
    compActList: any;
    componentCode: any;
    totalListOfComponentActivity: any;
    componentId: any;
    getAllComponents() {
        abp.ui.setBusy();
        this._componentActivityService.getAllComponentandActivitymappingforcomponenttable().subscribe(result => {
            this.componentActivityList = result;
            this.totalListOfComponentActivity = [];
            abp.ui.clearBusy();
        })
    }

    getAllComponentssearch() {
        abp.ui.setBusy();
        var input = (document.getElementById("FilterEmployeeText") as HTMLInputElement).value;

        this._componentActivityService.getAllComponentandActivitymappingforcomponenttablesearch(input).subscribe(result => {
            this.componentActivityList = result;
            this.totalListOfComponentActivity = [];
            abp.ui.clearBusy();
        })
    }
   
    itemList: any;
    getAllActivity() {  
        this.itemList = [];
        this._activityService.getAllActivities("").subscribe(result => {
            this.activityList = result.sort();
            for (let a of result) {
                this.itemList.push({
                    id: a.id,
                    itemName: a.name
                })
            }
        })
    }

    ActivityId: number[] = [];
    onItemSelect(item: any) {
        this.ActivityId.push(item.id);
    }
    OnItemDeSelect(item: any) {
        var index = this.ActivityId.indexOf(item.id, 0);
        if (index > -1) {
            this.ActivityId.splice(index, 1);
        }
    }
    onSelectAll(items: any) {
        for (let obj of items) {
            this.ActivityId.push(obj.id);
        }
    }
    onDeSelectAll(items: any) {
        this.ActivityId = [];
    }

    onClickComponent(component) {
        this.componentActivityMap = new ComponentActivityMapListDto();
        this.selectedItems = [];
        this.ActivityId = [];
        this.componentActivityMap.componentID = component.componetTableId;
    }

    save() {
        this.componentActivityMap.actID = this.ActivityId;
        this._componentActivityService.createComponentActivityMapping(this.componentActivityMap)
            .finally(() => this.saving = false).subscribe(result => {
                this.notify.info(result[0]);
                this.getAllComponents();
            });
    }

    ComponentsModal() {
        this.myModalView.show(undefined);
    }
    ActivityModel(c) {
        this.myModalView.show(c);
    }
    editComponent(components) {
        this.myModalView.show(components);
    }
    deleteComponentActivity(del) {        
        this.message.confirm(
            this.l('ComponentActivityDelete', del.activityName),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._componentActivityService.deleteComponentActivityMapping(del.id)
                        .subscribe(() => {
                            this.notify.success(this.l('Successfully Deleted'));
                            this.getAllComponents();
                        });
                }
            })
    }

    deleteComponent(del) {
        this.message.confirm(
            this.l('ComponentDeleteWarningMessage', del.componentName),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._componentService.deleteComponent(del.componetTableId)
                        .subscribe((result) => {

                            if (result == "Record Exist") {
                                this.notify.success(this.l('This record is already in used'));
                                this.getAllComponents();
                            }
                            else {
                                this.notify.success(this.l('Successfully Delete Component'));
                                this.getAllComponents();
                            }
                           
                        });
                }
            })
    }

    changePage(p: any,res) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        res = res.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
}