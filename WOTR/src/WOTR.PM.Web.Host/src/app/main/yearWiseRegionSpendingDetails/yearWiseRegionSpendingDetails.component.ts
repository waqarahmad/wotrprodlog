﻿import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { ProgramRegionSpendingServiceProxy, ProgramServiceProxy, PrgRequestAAPlasServiceProxy, ProgramRegionSpendingListDto, ProgrameComponentListDto, ProgramCompnentsServiceProxy, ProgramInformationListDto, PrgVillageClusterListDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
    selector: 'yearWiseRegionSpendingDetails',
    templateUrl: "./yearWiseRegionSpendingDetails.component.html",
    styleUrls: ['./yearWiseRegionSpendingDetails.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class yearWiseRegionSpendingDetailsComponent extends AppComponentBase {    
    selectedPermission = '';
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: ProgramInformationListDto[] = [];
    programdetails: ProgramInformationListDto[];
    programd: ProgramInformationListDto = new ProgramInformationListDto();
    VillageList: any = [];
    ActivityList: any = [];
    ComponentList: any = [];
    VillageClustorList: any = [];
    program: ProgramInformationListDto[];
    villageclustor: PrgVillageClusterListDto[];
    ImplementationList: ProgrameComponentListDto[] = [];
    count: number = -1;
    finalUnits: number = 0;
    sumOfTotal: number = 0;
    sumOfTotal1: number = 0;
    pro: any[];
    active: boolean = false;
    list: any = [];
    ProgramRegionSpending: ProgramRegionSpendingListDto[] = [];
    Year: any = [];
    costyear: string;

    constructor(
        injector: Injector,
        private _programService: ProgramServiceProxy,
        private _ProgramCompnentsService: ProgramCompnentsServiceProxy,
        private _programRegionSpendingServiceProxy: ProgramRegionSpendingServiceProxy,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy,
        private _fileDownloadService: FileDownloadService)
    {
        super(injector);
    }

    ngOnInit() {
        this.getAllProgram();
        this.getcostestimationyear();
    }    
  

    getAllProgram() {
        this._programService.getAllProgram().subscribe(result => {
            this.programdetails = result;
        });
    }
    getcostestimationyear() {
        this._PrgRequestAAPlasService.overallPrgCostEstimationYears().subscribe(result => {
            this.Year = result;

        });
    }
    onChangeState(event) {
        //this.getAllProgrambyid(event.target.value);
    }

    programName: string;

    
    getAllProgrambyid(programId, year) {
        this.costyear = year;
        this._programService.getProgramDetailsbyID(programId).subscribe(result => {
            this.program = result;
            this.programName = this.program[0].programName;
        });

        this._programRegionSpendingServiceProxy.getProgramRegionSpendingDeatailsforexcelreports(programId,this.costyear).subscribe(result => {
            this.ProgramRegionSpending = result;
        });
    }

    exportToExcel(programId): void {
        abp.ui.setBusy();
        this._programRegionSpendingServiceProxy.createYearWiseRegionExcelDoc("YearWiseRegion", programId,this.costyear).subscribe(
            (result) => {
                this._fileDownloadService.downloadTempFile1(result);
                abp.ui.clearBusy();
            });
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
   
}