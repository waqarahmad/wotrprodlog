﻿import { Component, Injector, ViewEncapsulation } from '@angular/core';
import {
    ProgramCompnentsServiceProxy, ProgrameComponentListDto, ProgramServiceProxy, ProgramInformationListDto,
    ProgramCostEstimationServiceProxy, UnitOfMeasuresServiceProxy, UnitofMeasureDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';
import { round } from 'd3';
declare var jQuery: any;


@Component({
    selector: 'programCostEstimation',
    templateUrl: "./programCostEstimation.component.html",
    styleUrls: ['./programCostEstimation.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ProgramCostEstimationComponent extends AppComponentBase {
    componentWithActivityList: ProgrameComponentListDto[] = [];
    programDetails: ProgramInformationListDto[];
    unitOfMeasureList: UnitofMeasureDto[];
    saving: boolean = false;
    year: any;
    totalUnits: number;
    tabindex: number = 0;
    prevTempComponentWithActivityList: ProgrameComponentListDto[] = [];
    tempComponentWithActivityList: ProgrameComponentListDto[] = [];
    ac = [];
    unitCost: number = 0;
    totalUnitsCost: number = 0;
    totalCost: number = 0;
    tunits: number = 0;
    cContri: number = 0;
    fContri: number = 0;
    oContri: number = 0;
    i: number = 1;
    j: number = 1;

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _ProgramCompnentsService: ProgramCompnentsServiceProxy,
        private _ProgramService: ProgramServiceProxy,
        private _ProgramCostEstimationService: ProgramCostEstimationServiceProxy,
        private _UnitOfMeasureService: UnitOfMeasuresServiceProxy

    ) {
        super(injector);

    }

    ngAfterViewInit(): void {
        this.GetProgramDetailsById();
        this.GetAllUnitOfMeasure();
        this.GetComponentAndActivityForPrograme();
    }

    slectedAllYears(res, years, rec) {
        for (let y of years.costEstimationYear) {
            if (y.costEstimationYear != "OverAll") {
                for (let a of y.activity) {
                    if (rec.activityName == a.activityName) {
                        a.programCostEstimate_UnitOfMeasuresID = res;
                    }
                }
            }
        }
    }

    checkTotalCost(rec) {
        if (rec.programCostEstimate_TotalUnitCost == (+rec.programCostEstimate_CommunityContribution + +rec.programCostEstimate_FunderContribution + +rec.programCostEstimate_OtherContribution)) {

        }
        else {
            alert("Total Cost is Not equal of FUNDER Contribution OTHER Contribution and COMMUNITY Contribution")
        }
    }

    funPanel() {
        $('.panel-collapse').on('show.bs.collapse', function () {
            $(this).siblings('.panel-heading').addClass('active');
        });

        $('.panel-collapse').on('hide.bs.collapse', function () {
            $(this).siblings('.panel-heading').removeClass('active');
        });
    }

    openCity(evt, cityName, btn) {
        $("#" + cityName).siblings().hide();
        $("#" + cityName).show();

        $("#" + btn).siblings().removeClass("active");
        $("#" + btn).addClass("active");
    }

    GetAllUnitOfMeasure() {
        this._UnitOfMeasureService.getAllUnitOfMeasure().subscribe(result => {
            this.unitOfMeasureList = result;
        })
    }

    funComponents() {
        this._router.navigate(['/app/main/ProgramComponent'], {
            queryParams: {
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                Title: 'ProgramComponent',
            }
        });
    }

    funProgramCluster() {
        this._router.navigate(['/app/main/ProgramCluster'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funProgramInformation() {
        this._router.navigate(['/app/main/CreateProgramInformation'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funActivityMaping() {
        this._router.navigate(['/app/main/ActivityMapping'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    GetProgramDetailsById() {
        var ProgrammeId = this._activatedRoute.snapshot.queryParams.ProgrameId;
        this._ProgramService.getProgramDetailsbyID(ProgrammeId).subscribe(result => {
            this.programDetails = result;
        })
    }

    ActiveClass(i) {
        $("#tab_" + i + "_0").addClass("active");
        $("#tab_" + i + "_0").siblings().removeClass("active");
    }

    funClick(i, c, ri, location) {
        $("#mapId_" + i + "_" + c + "_" + ri).siblings().hide();
        $("#mapId_" + i + "_" + c + "_" + ri).show();
    }

    changeProLevel(res, years, rec) {
        for (let y of years.costEstimationYear) {
            if (y.costEstimationYear != "OverAll") {
                for (let a of y.activity) {
                    if (rec.activityName == a.activityName) {
                        a.activityLevel = res;
                    }
                }
            }
        }
    }

    cal1(rec, res, yr, cost, funder, other) {
        if (rec.programCostEstimate_TotalUnits < 0) {
            rec.programCostEstimate_TotalUnits = 0;
        }
        if (rec.programCostEstimate_UnitCost < 0) {
            rec.programCostEstimate_UnitCost = 0;
        }
        if (rec.programCostEstimate_CommunityContribution < 0) {
            rec.programCostEstimate_CommunityContribution = 0;
        }
        if (rec.programCostEstimate_FunderContribution < 0) {
            rec.programCostEstimate_FunderContribution = 0;
        }
        if (rec.programCostEstimate_OtherContribution < 0) {
            rec.programCostEstimate_OtherContribution = 0;
        }

        if (yr.costEstimationYear == "OverAll") {
            this.totalUnits = rec.programCostEstimate_TotalUnits;
            this.unitCost = rec.programCostEstimate_UnitCost;

            this.tunits = rec.programCostEstimate_TotalUnits;
            this.i = 1;
            for (let ce of res.costEstimationYear) {
                ce.subTotalTotalunits = 0;
                ce.subTotalofTotalCost = 0;
                if (ce.costEstimationYear != "OverAll") {
                    for (let c of ce.activity) {
                        if (c.activityName == rec.activityName) {
                            c.programCostEstimate_UnitCost = rec.programCostEstimate_UnitCost;
                            this.tunits = this.tunits - c.programCostEstimate_TotalUnits;
                        }
                        ce.subTotalTotalunits += +c.programCostEstimate_TotalUnits;
                        ce.subTotalofTotalCost += +c.programCostEstimate_TotalUnitCost;
                    }

                    this.i = this.i + 1;
                }
                else {

                    rec.programCostEstimate_TotalUnitCost = this.totalUnits * this.unitCost;

                    for (let act of ce.activity) {
                        ce.subTotalTotalunits += +act.programCostEstimate_TotalUnits;
                        ce.subTotalofTotalCost += +act.programCostEstimate_TotalUnitCost;
                    }
                }
            }
            this.totalUnits = 0;
            this.totalCost = 0;
            for (let c of this.componentWithActivityList) {
                for (let co of c.costEstimationYear) {
                    if (co.costEstimationYear == "OverAll") {
                        this.totalUnits += +co.subTotalTotalunits;
                        this.totalCost += +co.subTotalofTotalCost;
                    }
                }
                c.totalUnits = this.totalUnits;
                c.totalCost = this.totalCost;
            }
        }
        else {
            var a = 0;
            var b = 0;
            var c = 0;
            var d = 0;
            var e = 0;
            rec.programCostEstimate_TotalUnitCost = +rec.programCostEstimate_TotalUnits * +rec.programCostEstimate_UnitCost;
            var query = res.costEstimationYear.filter(t => t.costEstimationYear == rec.costEstimationYear); // "OverAll");
            for (let p of query) {
                var act = p.activity.filter(y => y.activityID == rec.activityID);
                for (let q of act) {
                    a = q.programCostEstimate_UnitCost;
                    b = q.programCostEstimate_CommunityContribution;
                    c = q.programCostEstimate_TotalUnits;
                    d = q.programCostEstimate_FunderContribution;
                    e = q.programCostEstimate_OtherContribution;
                }
                rec.programCostEstimate_CommunityContribution = isNaN((b / c) * rec.programCostEstimate_TotalUnits) ? 0 : round((b / c) * rec.programCostEstimate_TotalUnits);
                rec.programCostEstimate_FunderContribution = isNaN((d / c) * rec.programCostEstimate_TotalUnits) ? 0 : round((d / c) * rec.programCostEstimate_TotalUnits);
                rec.programCostEstimate_OtherContribution = isNaN((e / c) * rec.programCostEstimate_TotalUnits) ? 0 : round((e / c) * rec.programCostEstimate_TotalUnits);
            }

            for (let ce of res.costEstimationYear.filter(t => t.costEstimationYear != "OverAll")) {
                ce.subTotalTotalunits = 0;
                ce.subTotalofTotalCost = 0;
                    for (let c of ce.activity) {
                        if (c.activityName == rec.activityName) {
                           // c.programCostEstimate_UnitCost = rec.programCostEstimate_UnitCost; // Commented thid line on 20 April 20 
                            this.tunits = this.tunits - c.programCostEstimate_TotalUnits;
                        }
                        ce.subTotalTotalunits += +c.programCostEstimate_TotalUnits;
                        ce.subTotalofTotalCost += +c.programCostEstimate_TotalUnitCost;
                    }
            }
        }

    }


    cal(rec, res, yr, cost, funder, other) {

        if (rec.programCostEstimate_TotalUnits < 0) {
            rec.programCostEstimate_TotalUnits = 0;
        }
        if (rec.programCostEstimate_UnitCost < 0) {
            rec.programCostEstimate_UnitCost = 0;
        }
        if (rec.programCostEstimate_CommunityContribution < 0) {
            rec.programCostEstimate_CommunityContribution = 0;
        }
        if (rec.programCostEstimate_FunderContribution < 0) {
            rec.programCostEstimate_FunderContribution = 0;
        }
        if (rec.programCostEstimate_OtherContribution < 0) {
            rec.programCostEstimate_OtherContribution = 0;
        }
        if (yr.costEstimationYear == "OverAll") {
            this.totalUnits = rec.programCostEstimate_TotalUnits;
            this.unitCost = rec.programCostEstimate_UnitCost;

            this.tunits = rec.programCostEstimate_TotalUnits;
            this.i = 1;
            for (let ce of res.costEstimationYear) {
                ce.subTotalTotalunits = 0;
                ce.subTotalofTotalCost = 0;
                if (ce.costEstimationYear != "OverAll") {
                    for (let c of ce.activity) {
                        if (c.activityName == rec.activityName) {
                            c.programCostEstimate_TotalUnits = Math.ceil(this.tunits / (res.costEstimationYear.length - this.i));

                            c.programCostEstimate_CommunityContribution = (rec.programCostEstimate_CommunityContribution / rec.programCostEstimate_TotalUnits) * c.programCostEstimate_TotalUnits;
                            c.programCostEstimate_FunderContribution = (rec.programCostEstimate_FunderContribution / rec.programCostEstimate_TotalUnits) * c.programCostEstimate_TotalUnits;
                            c.programCostEstimate_OtherContribution = (rec.programCostEstimate_OtherContribution / rec.programCostEstimate_TotalUnits) * c.programCostEstimate_TotalUnits;
                            c.programCostEstimate_UnitCost = rec.programCostEstimate_UnitCost;
                            c.programCostEstimate_TotalUnitCost = c.programCostEstimate_TotalUnits * c.programCostEstimate_UnitCost;
                            this.tunits = this.tunits - c.programCostEstimate_TotalUnits;
                        }
                        ce.subTotalTotalunits += c.programCostEstimate_TotalUnits;
                        ce.subTotalofTotalCost += c.programCostEstimate_TotalUnitCost;
                    }

                    this.i = this.i + 1;
                }
                else {

                    rec.programCostEstimate_TotalUnitCost = this.totalUnits * this.unitCost;

                    for (let act of ce.activity) {
                        ce.subTotalTotalunits += +act.programCostEstimate_TotalUnits;
                        ce.subTotalofTotalCost += +act.programCostEstimate_TotalUnitCost;
                    }
                }
            }
            this.totalUnits = 0;
            this.totalCost = 0;
            for (let c of this.componentWithActivityList) {
                for (let co of c.costEstimationYear) {
                    if (co.costEstimationYear == "OverAll") {
                        this.totalUnits += co.subTotalTotalunits;
                        this.totalCost += co.subTotalofTotalCost;
                    }
                }
                c.totalUnits = this.totalUnits;
                c.totalCost = this.totalCost;
            }
        }

        else {
            var a = 0;
            var b = 0;
            var c = 0;
            var d = 0;
            var e = 0;
            var aa = 0;
            var bb = 0;
            var cc = 0;
            var dd = 0;
            var ee = 0;
            for (let p of res.costEstimationYear) {
                if (p.costEstimationYear == "OverAll") {
                    for (let act of p.activity) {
                        if (act.activityID == rec.activityID) {
                            a = act.programCostEstimate_UnitCost;
                            b = act.programCostEstimate_CommunityContribution;
                            c = act.programCostEstimate_TotalUnits;
                            d = act.programCostEstimate_FunderContribution;
                            e = act.programCostEstimate_OtherContribution;
                        }
                        rec.programCostEstimate_UnitCost = a;
                        rec.programCostEstimate_TotalUnitCost = rec.programCostEstimate_TotalUnits * rec.programCostEstimate_UnitCost;
                    }
                }
                if (p.costEstimationYear != "OverAll") {
                    for (let act of p.activity) {
                        if (act.activityID == rec.activityID) {
                            aa = act.programCostEstimate_UnitCost;
                            bb = act.programCostEstimate_CommunityContribution;
                            cc = act.programCostEstimate_TotalUnits;
                            dd = act.programCostEstimate_FunderContribution;
                            ee = act.programCostEstimate_OtherContribution;
                        }
                        rec.programCostEstimate_UnitCost = aa;
                        rec.programCostEstimate_TotalUnitCost = rec.programCostEstimate_TotalUnits * rec.programCostEstimate_UnitCost;
                        if (cost == null && funder == null && other == null) {
                            rec.programCostEstimate_CommunityContribution = (bb / cc) * rec.programCostEstimate_TotalUnits;
                            rec.programCostEstimate_FunderContribution = (dd / cc) * rec.programCostEstimate_TotalUnits;
                            rec.programCostEstimate_OtherContribution = (ee / cc) * rec.programCostEstimate_TotalUnits;
                        }
                        if (cost) {
                            rec.programCostEstimate_CommunityContribution = cost;

                            rec.programCostEstimate_FunderContribution = 0.00;
                            rec.programCostEstimate_OtherContribution = 0.00;


                        }
                        if (funder) {
                            rec.programCostEstimate_FunderContribution = funder;
                            rec.programCostEstimate_CommunityContribution = rec.programCostEstimate_CommunityContribution;
                            rec.programCostEstimate_OtherContribution = rec.programCostEstimate_OtherContribution
                        }
                        if (other) {
                            rec.programCostEstimate_OtherContribution = other;
                            rec.programCostEstimate_FunderContribution = rec.programCostEstimate_FunderContribution;
                            rec.programCostEstimate_CommunityContribution = rec.programCostEstimate_CommunityContribution;

                        }
                    }
                }
            }

        }

    }

    GetComponentAndActivityForPrograme() {
        var ProgrammeId = this._activatedRoute.snapshot.queryParams.ProgrameId;
        abp.ui.setBusy();
        this._ProgramCompnentsService.getComponentCostEstimation(ProgrammeId).subscribe(result => {
            this.componentWithActivityList = result;
            if (result.length != 0) {
                this.totalUnits = result[0].totalUnits;
                this.totalCost = result[0].totalCost;
            }
            abp.ui.clearBusy();
        })
    }

    save() {
        abp.ui.setBusy();
        this.saving = true;
        this._ProgramCostEstimationService.createOrUpdateProgramCostEstimation(this.componentWithActivityList)
            .finally(() => this.saving = false).subscribe(res => {
                this.notify.info(res[0]);
                abp.ui.clearBusy();
                this.funActivityMaping();
            });
    }
}