﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { ActionAreasServiceProxy, ActionAreasListDto, ActionSubActionAreaMappingListDto, ActionSubActionMappingServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';
import { AddModalActionSubActionAreaComponent } from './ModalActionSubActionArea.component';
declare var jQuery: any;

@Component({
    selector: 'actionSubActionAreacomponent',
    templateUrl: "./ActionSubActionArea.component.html",
    styleUrls: ['./ActionSubActionArea.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ActionSubActionAreaComponent extends AppComponentBase {
    @ViewChild('subActionAreaModal') myModalActionSubAction: AddModalActionSubActionAreaComponent;
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    ActivitySubActionMapping: any;
    ActivitySubaction: any;
    actionSubActionAreaActivity: any;
    TotalActionArea: any;
    actionAreaName: string;
    actionSubActionAreaMapList: ActionSubActionAreaMappingListDto[];

    constructor(
        injector: Injector,
        private _actionSubActionMappingService: ActionSubActionMappingServiceProxy) {
        super(injector);
    }

    ngAfterViewInit(): void {
        this.PanelView();
        this.getAllActionSubActionArea();
    }

    ngOnInit(): void {
        this.PanelView();
    }

    getAllActionSubAction() {

    }

    changePage() {

    }

    editActionSubActionArea(ActionSubActionArea) {
        this.myModalActionSubAction.show(ActionSubActionArea);
    }

    getAllActionSubActionAreaa() {
        abp.ui.setBusy();
        var input = (document.getElementById("FilterEmployeeText") as HTMLInputElement).value;

        this._actionSubActionMappingService.getAllActionAreaRecordsearch(input).subscribe(result => {
            this.actionSubActionAreaMapList = result;
            this.actionSubActionAreaActivity = {};
            this.TotalActionArea = [];
            var flags = [], output = [], l = result.length, i;
            for (i = 0; i < l; i++) {
                if (flags[result[i].actionAreaID]) continue;
                flags[result[i].actionAreaID] = true;
                output.push(result[i].actionAreaID);
            }

            for (let o of output) {
                this.ActivitySubActionMapping = {};
                this.ActivitySubaction = [];
                for (let x of result) {
                    if (x.actionAreaID == o) {
                        this.actionAreaName = x.actionAreaName;
                        this.ActivitySubActionMapping = {
                            Activity: x.activityName,
                            subAction: x.subActionAreaName,
                            createdBy: x.firstName,
                            creationTime: x.creationTime,
                            id: x.id
                        }
                        this.ActivitySubaction.push(this.ActivitySubActionMapping);
                    }
                }
                this.actionSubActionAreaActivity = {
                    actionAreaName: this.actionAreaName,
                    activitySubAction: this.ActivitySubaction,
                }
                this.TotalActionArea.push(this.actionSubActionAreaActivity);
            }
            abp.ui.clearBusy();
        })
    }



    getAllActionSubActionArea() {
        abp.ui.setBusy();
        this._actionSubActionMappingService.getAllActionAreaRecord().subscribe(result => {
            this.actionSubActionAreaMapList = result;
            this.actionSubActionAreaActivity = {};
            this.TotalActionArea = [];
            var flags = [], output = [], l = result.length, i;
            for (i = 0; i < l; i++) {
                if (flags[result[i].actionAreaID]) continue;
                flags[result[i].actionAreaID] = true;
                output.push(result[i].actionAreaID);
            }

            for (let o of output) {
                this.ActivitySubActionMapping = {};
                this.ActivitySubaction = [];
                for (let x of result) {
                    if (x.actionAreaID == o) {
                        this.actionAreaName = x.actionAreaName;
                        this.ActivitySubActionMapping = {
                            Activity: x.activityName,
                            subAction: x.subActionAreaName,
                            createdBy: x.firstName,
                            creationTime: x.creationTime,
                            id: x.id
                        }
                        this.ActivitySubaction.push(this.ActivitySubActionMapping);
                    }
                }
                this.actionSubActionAreaActivity = {
                    actionAreaName: this.actionAreaName,
                    activitySubAction: this.ActivitySubaction,
                }
                this.TotalActionArea.push(this.actionSubActionAreaActivity);
            }
            abp.ui.clearBusy();
        })
    }

    PanelView() {
        $(document).on('click', '.panel-heading span.clickable', function (e) {
            var $this = $(this);
            if (!$this.hasClass('panel-collapsed')) {
                $this.parents('.panel').find('.panel-body').slideUp();
                $this.addClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            } else {
                $this.parents('.panel').find('.panel-body').slideDown();
                $this.removeClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            }
        })
    }

    deleteActionSubActionArea(del) {
        this.message.confirm(
            this.l('ActionSubActionDeleteWarning', del.Activity),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._actionSubActionMappingService.deleteActionSubactionMapping(del.id)
                        .subscribe(() => {
                            this.notify.success(this.l('Successfully Deleted'));
                            this.getAllActionSubActionArea();
                        });
                }
            })
    }

}