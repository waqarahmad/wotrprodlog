﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { ActivityServiceProxy, ActivitiyListDto, ActionAreasServiceProxy, ActionAreasListDto, ActionSubActionAreaMappingListDto, ActionSubActionMappingServiceProxy, SubActionAreaServiceProxy, SubActionAreaListDto, } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
import { MysortPipe } from '../../shared/common/mysort.pipe';//yo modified
declare var jQuery: any;



@Component({
    selector: 'ModalActionSubActionArea',
    templateUrl: "./ModalActionSubActionArea.component.html",
    styleUrls: ['./ModalActionSubActionArea.component.less'],
})
export class AddModalActionSubActionAreaComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('ModalActionSubActionArea') modal: ModalDirective;
    active: boolean = false;
    saving: boolean = false;
    actionAreaList: ActionAreasListDto[];
    subActionAreaList: SubActionAreaListDto[];
    activityList: ActivitiyListDto[];
    actionSubActionMap: ActionSubActionAreaMappingListDto;
    selectedItems: any;
    settings: any;
    constructor(
        injector: Injector,
        private _actionAreaService: ActionAreasServiceProxy,
        private _actionSubActionAreaMappingService: ActionSubActionMappingServiceProxy,
        private _subActionAreaService: SubActionAreaServiceProxy,
        private _activityService: ActivityServiceProxy
    ) {
        super(injector);

    }
    ngOnInit(): void {
            this.settings = {
            singleSelection: false,
            text: "Select Activities",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        };      
        this.getAllActivity();
        this.actionSubActionMap = new ActionSubActionAreaMappingListDto();
    }
    ngAfterViewInit(): void {
        this.getAllActionArea();
    }

    getAllActionArea() {
        this._actionAreaService.getAllActionAreaRecord().subscribe(result => {
            this.actionAreaList = result;
        })
    }
    getAllSubActionArea() {
        this._subActionAreaService.getAllSubActionAreaRecord().subscribe(result => {          
            this.subActionAreaList = result;
        })
    }

    itemList: any;
    getAllActivity() {
        this.itemList = [];
        this._activityService.getAllActivities(" ").subscribe(result => {
            this.activityList = result;
            for (let a of result) {
                this.itemList.push({
                    id: a.id,
                    itemName: a.name
                })
            }
        })
    }
    ActivityId: number[] = [];
    onItemSelect(item: any) {
        this.ActivityId.push(item.id);
    }
    OnItemDeSelect(item: any) {
        var index = this.ActivityId.indexOf(item.id, 0);
        if (index > -1) {
            this.ActivityId.splice(index, 1);
        }
    }
    onSelectAll(items: any) {
        for (let obj of items) {
            this.ActivityId.push(obj.id);
        }
    }
    onDeSelectAll(items: any) {
        this.ActivityId = [];
    }

    //No: number;
    getAllSubActionByActionID() {
        this._subActionAreaService.getSubactionaginsActionarea(this.actionSubActionMap.actionAreaID).subscribe((result) => {
            this.subActionAreaList = result;
        })
    }

    save() {        
        this.saving = true;
        this.actionSubActionMap.activityID = this.ActivityId;
        this._actionSubActionAreaMappingService.creatOrUpdateActionSubActionMappingArea(this.actionSubActionMap)
            .finally(() => this.saving = false)
            .subscribe(result => {
                this.notify.info(result[0]);
                this.close();
                this.modalSave.emit(null);
        })
    }

    show(a): void { 
        this.active = true;
        if (a != '') {
            $('#lblTitle').text('Edit Action Sub Action Area');
            //this.actionarea = a;
        }
        else {
            this.getAllActivity();
            this.selectedItems = [];
            this.ActivityId = [];
            this.actionSubActionMap = new ActionSubActionAreaMappingListDto();
           
            $('#lblTitle').text('Add Action Sub Action Area');
            //subActionAreaList: SubActionAreaListDto[];
            //this.subActionAreaList = new SubActionAreaListDto();
        }
        this.modal.show();

    } 
    close(): void {
        this.modal.hide();
        this.active = false;
       /* this.eForm.reset();*///yo Form Reset
    }
}