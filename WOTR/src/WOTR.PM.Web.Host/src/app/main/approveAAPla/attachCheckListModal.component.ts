﻿import { Component, Injector, ViewChild, ElementRef } from '@angular/core';
import { } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
declare var jQuery: any;
@Component({
    selector: 'attachCheckListModal',
    templateUrl: "./attachCheckListModal.component.html",
    styleUrls: ['./attachCheckListModal.component.less'],
})
export class AttachCheckListModal extends AppComponentBase {
    @ViewChild('attachCheckListModal') modal: ModalDirective;
    active: boolean = false;
    constructor(
        injector: Injector,
        private _router: Router,

    ) {
        super(injector);

    }
    ngOnInit(): void {

    }
    ngAfterViewInit(): void {

    } 
    show(): void {       
        this.active = true;      
        this.modal.show();
    }
    close(): void {
        this.modal.hide();
        this.active = false;
    }
}