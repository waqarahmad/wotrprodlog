﻿import { Component, Injector, ViewChild, EventEmitter, Output } from '@angular/core';
import { ActivitiyListDto, PrgRequestAAPlasServiceProxy, UnitofMeasureDto, ProjectWiseYearlyActivity } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
declare var jQuery: any;


@Component({
    selector: 'ActivitesAddModals',
    templateUrl: "./approveAAPlaActivityInfo.component.html"
})
export class ActivitesAddModalsComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('createOrEditModal1') modal: ModalDirective;

    yearWiseActivityInfo: ProjectWiseYearlyActivity = new ProjectWiseYearlyActivity();
    active: boolean = false;
    saving: boolean = false;
    activity: ActivitiyListDto;
    unitofMeasureDto: UnitofMeasureDto[];

    constructor(
        injector: Injector,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy,
    ) {
        super(injector);
    }

    show(pId: number, year: string, status: string): void {
        this.active = true;
        this._PrgRequestAAPlasService.yearWiseActivity(pId, year, status).subscribe(result => {
            this.yearWiseActivityInfo = result;
        });
        this.modal.show();
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
}