﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation, NgZone} from '@angular/core';
import { PrgRequestAAPlasServiceProxy, RequestAAPlasListDto, ProjectWiseYearlyClustor, ProjectWiseYearlyclustorinfo, EmailServiceProxy, ProjectWiseDashboardData, BarChartDataset, AllChartDto, ProjectWiseYearlyActivity, IProjectWiseYearlyClustor, Notificationdto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';
import { AttachCheckListModal } from './attachCheckListModal.component';
import { ActivitesAddModalsComponent } from './approveAAPlaActivityInfo.component';

import { ModalDirective } from 'ngx-bootstrap';
import { debuglog } from 'util';
declare var jQuery: any;
import { ChartModule } from 'primeng/primeng';
import { empty } from 'rxjs/observable/empty';

@Component({
   
    templateUrl: "./approveAApla.component.html",
    styleUrls: ['./approveAApla.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ApproveAAPlaComponent extends AppComponentBase {
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('attachCheckListModal') myModalAttachCheckListModal: AttachCheckListModal;

    @ViewChild('ActivitesAddModals') ActivitesAddModals: ActivitesAddModalsComponent;

    requestList: RequestAAPlasListDto[] = [];

    saving: boolean = false;
    emailContent: any;
    today: number = Date.now();
    projectWiseDashboardData: ProjectWiseDashboardData = new ProjectWiseDashboardData();
    ProjectInfo: any = [];
    ProjectInformation: any = [];
    datas: AllChartDto = new AllChartDto(); 

    projectName: string;
    projectstart: string;
    projectEnd: string;
    projectstatus: string;

    simplebarcharts: any;
    piechartdata: any;
    costestimationyeardto: any = [];
    Year: string;
    LineBarChart: AllChartDto = new AllChartDto();
    yearWiseclustorInfo: ProjectWiseYearlyclustorinfo[];
    notification: Notificationdto[];
    programeId: number = 0;

    constructor(
        injector: Injector, private zone: NgZone,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy,
        private _emailServiceProxy: EmailServiceProxy

    ) {
        super(injector);
    }
    
    ngOnInit(): void {
        this._activatedRoute.queryParams.subscribe(params => {
            this.programeId = +this._activatedRoute.snapshot.queryParams.ProgrameId;
            this.GetAllApproveRequests();
            this.sumarryForProjectWise();

        });
    }

   
    sumarryForProjectWise() {
        abp.ui.setBusy();
        this._PrgRequestAAPlasService.sumarryForProjectWise(this.programeId).subscribe(result => {           
            
            this.ProjectInfo = [];
            result.projectWiseDashboardDatas[0].projectWiseInfoDTOs.forEach(x => {
                this.ProjectInfo.push(x.projectManager)
            })
            this.projectName = result.projectWiseDashboardDatas[0].projectWiseInfoDTOs.length == 0 ? "" : result.projectWiseDashboardDatas[0].projectWiseInfoDTOs[0].projectName;
            this.projectstart = result.projectWiseDashboardDatas[0].projectWiseInfoDTOs.length == 0 ? "" : (result.projectWiseDashboardDatas[0].projectWiseInfoDTOs[0].projectStart).format('MM/DD/YYYY');
            this.projectEnd = result.projectWiseDashboardDatas[0].projectWiseInfoDTOs.length == 0 ? "" : (result.projectWiseDashboardDatas[0].projectWiseInfoDTOs[0].projectEnd).format('MM/DD/YYYY');
            this.projectstatus = result.projectWiseDashboardDatas[0].projectWiseInfoDTOs.length == 0 ? "" : result.projectWiseDashboardDatas[0].projectWiseInfoDTOs[0].projectStatus;

            
            this._PrgRequestAAPlasService.costEstimationYear(this.programeId).subscribe(results => {
                this.costestimationyeardto = results;
            });

            abp.ui.clearBusy();
        });
    }

    clickyear(year) {
        this.Year = year;
        this._PrgRequestAAPlasService.yearWisecomponent(this.programeId, year).subscribe(result => {
            this.simplebarcharts = result.programManagerProgramYearwiseStateWiseBudgetBarChart;
            this.drawCharts();
            this.piechartdata = result.statuswisecounts;
            this.piecharts();
            this.yearWiseclustorInfo= result.projectWiseYearlyclustorinfo;
            this.notification = result.notificationdtos; 
        });
    }

    drawCharts() {        this.zone.runOutsideAngular(() => {            Promise.all([                import("@amcharts/amcharts4/core"),                import("@amcharts/amcharts4/charts"),                import("@amcharts/amcharts4/themes/animated"),            ])                .then(modules => {                    const am4core = modules[0];                    const am4charts = modules[1];                    const am4themes_animated = modules[2].default;                    am4core.options.commercialLicense = true;                    //am4core.useTheme(am4themes_animated);                    am4core.useTheme(am4themes_animated);

                    // Create chart instance
                    let chart = am4core.create("chartdiv", am4charts.XYChart);


                    // Add data
                //    this.simplebarcharts.forEach(
                //        xx => {

                //            chart.data.push({
                //                "year": xx.labels[0],
                //                "funderContribution": xx.datasets[0].data[0],
                //                "communityContribution": xx.datasets[1].data[0],
                //                "otherContribution": xx.datasets[2].data[0],

                //            })
                //        })

                //    // Create axes
                //    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                //    categoryAxis.dataFields.category = "year";
                //    categoryAxis.renderer.grid.template.location = 0;
                //    let label = categoryAxis.renderer.labels.template;
                //    label.wrap = true;
                //    label.maxWidth = 120;

                //    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                //    valueAxis.min = 0;

                //    // Create series
                //    function createSeries(field, name) {
                        
                //        // Set up series
                //        var series = chart.series.push(new am4charts.ColumnSeries());
                //        series.name = name;
                //        series.dataFields.valueY = field;
                //        series.dataFields.categoryX = "year";
                //        series.sequencedInterpolation = true;
                //        if (name == "FunderContribution") {
                //            series.columns.template.fill = am4core.color("#5D78FF");
                //            series.stroke = am4core.color("#5D78FF");
                //        }
                //        if (name == "CommunityContribution") {
                //            series.columns.template.fill = am4core.color("#0ABB87");
                //            series.stroke = am4core.color("#0ABB87");
                //        }
                //        if (name == "OtherContribution") {
                //            series.columns.template.fill = am4core.color("#FD397A");
                //            series.stroke = am4core.color("#FD397A");
                //        }
                //        // Make it stacked
                //        series.stacked = true;

                //        // Configure columns
                //        series.columns.template.width = am4core.percent(60);
                //        series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";
                       
                //        // Add label
                //        var labelBullet = series.bullets.push(new am4charts.LabelBullet());
                //        labelBullet.label.text = "{valueY}";
                //        labelBullet.locationY = 0.5;

                //        return series;
                //    }

                //    createSeries("funderContribution", "FunderContribution");
                //    createSeries("communityContribution", "CommunityContribution");                //    createSeries("otherContribution", "OtherContribution");
                //    // Legend
                //    chart.legend = new am4charts.Legend();                //})                //.catch(e => {                //    console.error("Error when creating chart", e);                //});                    this.simplebarcharts.forEach(
                        xx => {

                            chart.data.push({
                                "year": xx.labels[0],
                                "funderContribution": xx.datasets[0].data[0],
                                "communityContribution": xx.datasets[1].data[0],
                                "otherContribution": xx.datasets[2].data[0],

                            })
                        })


                    // Create axes
                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

                    categoryAxis.dataFields.category = "year";
                    categoryAxis.title.text = "Component";
                    categoryAxis.renderer.grid.template.location = 0;
                    categoryAxis.renderer.minGridDistance = 20;
                    categoryAxis.renderer.cellStartLocation = 0.1;
                    categoryAxis.renderer.cellEndLocation = 0.9;

                    categoryAxis.renderer.labels.template

                        .cursorOverStyle = [
                            {
                                "property": "cursor",
                                "value": "pointer"
                            }
                        ];
                    let label = categoryAxis.renderer.labels.template;
                    label.wrap = true;
                    label.maxWidth = 120;
                    label.tooltipText = "{category}"
                    categoryAxis.tooltip.label.maxWidth = 200;
                    categoryAxis.tooltip.label.wrap = true;

                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                    valueAxis.min = 0;
                    valueAxis.title.text = " ";
                    // Create series
                    function createSeries(field, name) {
                        var series = chart.series.push(new am4charts.ColumnSeries());
                        series.dataFields.valueY = field;
                        series.dataFields.categoryX = "year";
                        series.name = name;
                        series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
                        //series.stacked = stacked;
                        series.columns.template.width = am4core.percent(95);
                        if (name == "FunderContribution") {
                            series.columns.template.fill = am4core.color("#5D78FF");
                            series.stroke = am4core.color("#5D78FF");
                        }
                        if (name == "CommunityContribution") {
                            series.columns.template.fill = am4core.color("#0ABB87");
                            series.stroke = am4core.color("#0ABB87");
                        }
                        if (name == "OtherContribution") {
                            series.columns.template.fill = am4core.color("#FD397A");
                            series.stroke = am4core.color("#FD397A");
                        }
                    }

                    createSeries("funderContribution", "FunderContribution");
                    createSeries("communityContribution", "CommunityContribution");                    createSeries("otherContribution", "OtherContribution");                })                .catch(e => {                    console.error("Error when creating chart", e);                });        });    }
    piecharts() {
        this.zone.runOutsideAngular(() => {            Promise.all([                import("@amcharts/amcharts4/core"),                import("@amcharts/amcharts4/charts"),                import("@amcharts/amcharts4/themes/animated"),            ])                .then(modules => {                    const am4core = modules[0];                    const am4charts = modules[1];                    const am4themes_animated = modules[2].default;                    am4core.options.commercialLicense = true;                    //am4core.useTheme(am4themes_animated);                    am4core.useTheme(am4themes_animated);
                    // Themes end

                    // Create chart instance
                    var piechart = am4core.create("chartdivs", am4charts.PieChart);

                    // Add and configure Series
                    var pieSeries = piechart.series.push(new am4charts.PieSeries());
                                        pieSeries.slices.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;

                    pieSeries.dataFields.value = "statuscount";
                    pieSeries.dataFields.category = "projectStatus";

                    // Let's cut a hole in our Pie chart the size of 30% the radius
                    piechart.innerRadius = am4core.percent(30);

                    // Put a thick white border around each Slice
                    pieSeries.slices.template.stroke = am4core.color("#fff");
                    pieSeries.slices.template.strokeWidth = 2;
                    pieSeries.slices.template.strokeOpacity = 1;
                   

                    pieSeries.alignLabels = true;
                    pieSeries.labels.template.bent = true;
                    pieSeries.labels.template.radius = 3;
                    pieSeries.labels.template.padding(0, 0, 0, 0);
                    pieSeries.labels.template
                        // change the cursor on hover to make it apparent the object can be interacted with
                        .cursorOverStyle = [
                            {
                                "property": "cursor",
                                "value": "pointer"
                            }
                        ];
                    pieSeries.labels.template.events.on("hit", function (ev) {
                        
                        //alert(ev.event.target);
                        this.ActivitesAddModals.show(this.programeId, this.Year, ev.target.currentText);
                    }, this);
                    pieSeries.ticks.template.disabled = false;

                    // Create a base filter effect (as if it's not there) for the hover to return to
                    var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
                    shadow.opacity = 0;

                    // Create hover state
                    var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

                    // Slightly shift the shadow and make it more prominent on hover
                    var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
                    hoverShadow.opacity = 0.7;
                    hoverShadow.blur = 5;

                    // Add a legend
                    piechart.legend = new am4charts.Legend();
                    this.piechartdata.forEach(
                        xx => {
                            piechart.data.push({
                                "projectStatus": xx.projectStatus,
                                "statuscount": xx.statuscount,  
                            })
                        })                })                .catch(e => {                    console.error("Error when creating chart", e);                });        });
    }

    GetAllApproveRequests() {
        abp.ui.setBusy();

        this._PrgRequestAAPlasService.getReQuestApplaForProjectManager(this.programeId).subscribe(result => {
            this.requestList = result;
            for (let r of this.requestList) {
                for (let c of r.costEstimationYear) {
                    for (let s of c.actionSubactionCost) {
                        c.subTotalTotalunits += s.totalUnits;
                        c.subTotalUnitCost += s.unitCost;
                        c.subTotalofTotalCost += s.totalUnitCost;
                        c.subTotalofCommunityContribution += s.communityContribution;
                        c.subTotalofFunderContribution += s.funderContribution;
                        c.subTotalofOtherContribution += s.otherContribution;
                    }
                }
            }
            abp.ui.clearBusy();
        });

    }

    save(res, action, req) {
        abp.ui.setBusy();
        for (let p of res.costEstimationYear) {
            for (let b of p.actionSubactionCost) {
                b.checklistID = 1;
                if (req == "ChangeRequest") {
                    b.requestStatus = "ChangeRequest";
                }
            }
        }
        this._PrgRequestAAPlasService.creatOrUpdateRequestAppl(action)
            .finally(() => this.saving = false)
            .subscribe(result => {
                this.notify.info("Save SuccesFully");
                this.GetAllApproveRequests();
                var email = ((document.getElementById("emailtmp") as HTMLInputElement).value);

                this.emailContent = "<h4>Dear " +
                    result[0].name +
                    " ," +
                    "</h4>" +
                    "<br/>" +
                    email +
                    " <br/><div>Thank You</div>";
                this._emailServiceProxy.mailSending1(this.emailContent,
                    'Approval for WOTR Annual Action Plan ',
                    result[0].emailAddress,
                    null,
                    null,
                    null)
                    .subscribe((result) => {

                    });
                abp.ui.clearBusy();
            });

    }

    funActivityMaping() {
        this._router.navigate(['/app/main/ActivityMapping'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this.programeId,
            }
        });
    }

    funRegionSpending() {
        this._router.navigate(['/app/main/RegionSpending'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this.programeId,
            }
        });
    }

    funImplementationPlan() {
        this._router.navigate(['/app/main/ImplementationPlan'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this.programeId,
            }
        });
    }

    funRequestAAPla() {
        this._router.navigate(['/app/main/RequestAAPla'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this.programeId,
            }
        });
    }

    attachCheckListModal() {
        this.myModalAttachCheckListModal.show();
    }
}