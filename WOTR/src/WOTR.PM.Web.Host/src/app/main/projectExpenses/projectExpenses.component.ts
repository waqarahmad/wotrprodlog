﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { ImplimantionChecklistServiceProxy, ImplementationListDto, ListActionSubactionListImplemenTationDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';
import { ProjectExpensesModalComponent } from './projectExpensesModal.component';
import { ProjectExpensesCommunityContributionModalComponent } from './projectExpensescommunitycontributionModal.component';

@Component({
    selector: 'ProjectExpenses',
    templateUrl: "./projectExpenses.component.html",
    styleUrls: ['./projectExpenses.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ProjectExpensesComponent extends AppComponentBase {
    ImplementationList: ImplementationListDto[];
    List: ListActionSubactionListImplemenTationDto[];
    @ViewChild('ProjectExpensesModal') myModalView: ProjectExpensesModalComponent;
    @ViewChild(' ProjectExpensesCommunityContributionModal') myModalView1: ProjectExpensesCommunityContributionModalComponent;

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _ImplimantionChecklistService: ImplimantionChecklistServiceProxy

    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.getAllImplementaionActionAreaRecord();
        this.QuarterName = "Quarter 1";
    }

    ngAfterViewInit(): void {
        this.dateTime();
    }

    funNextPage() {
        this._router.navigate(['./app/main/ActivityLive']);
    }

    a: Date;

    dateTime() {
        $(function () {
            $("#ProgramStartDate").datetimepicker();
        });
        $("#ProgramStartDate").on("dp.change", () => {
            var dateString = $("#ProgramStartDate").val();
            var dateObj = new Date(dateString.toString());
        });

        $(function () {
            $("#ProgramEndDate").datetimepicker();
        });
        $("#ProgramEndDate").on("dp.change", () => {
            var dateString = $("#ProgramEndDate").val();
            var dateObj = new Date(dateString.toString());
        });
    }

    rowClass(rowData) {
        if (rowData.quarter1PhysicalUnits != 0 || rowData.quarter2PhysicalUnits != 0 || rowData.quarter3PhysicalUnits != 0 || rowData.quarter4PhysicalUnits != 0) {
            return '';
        }
        else {
            return 'noExpander';
        }
    }

    QuarterName: string;

    DivideUnits(r, res, index, result) {
        this.QuarterName = r;
        this.List = result;
    }

    getAllImplementaionActionAreaRecord() {
        abp.ui.setBusy();
        this._ImplimantionChecklistService.getImplementionPlan(this._activatedRoute.snapshot.queryParams.ProgrameId)
            .subscribe(result => {
                this.ImplementationList = result;
                for (let r of this.ImplementationList) {
                    for (let c of r.costEstimationYear) {
                        for (let s of c.actionSubactionCost) {
                            if (s.quarter1PhysicalUnits == 0 &&
                                s.quarter2PhysicalUnits == 0 &&
                                s.quarter3PhysicalUnits == 0 &&
                                s.quarter4PhysicalUnits == 0) {
                                for (let p of c.actionSubactionCost) {
                                    if (s.prgActionAreaActivityMappingID == p.prgActionAreaActivityMappingID &&
                                        (p.quarter1PhysicalUnits != 0 ||
                                            p.quarter2PhysicalUnits != 0 ||
                                            p.quarter3PhysicalUnits != 0 ||
                                            p.quarter4PhysicalUnits != 0)) {
                                        p.actionAreaName = undefined;
                                        p.subActionAreaName = undefined;
                                        p.activityName = undefined;
                                    }
                                }
                            }
                            c.subTotalTotalunits += s.totalUnits;
                            c.subTotalUnitCost += s.unitCost;
                            c.subTotalofTotalCost += s.totalUnitCost;
                            c.subTotalofCommunityContribution += s.communityContribution;
                            c.subTotalofFunderContribution += s.funderContribution;
                            c.subTotalofOtherContribution += s.otherContribution;
                        }
                    }

                }
                abp.ui.clearBusy();
            });
    }

    funApproveAAPla() {
        if (this.isGranted('Pages.RequestAAPlaSend')) {
            this._router.navigate(['/app/main/ApproveAAPla'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
        else {
            this._router.navigate(['/app/main/RequestAAPla'], {
                queryParams: {
                    ProgrameName: "",
                    ProgrameId: 1,
                }
            });
        }
    }

    funImplementationPlan() {
        if (this.isGranted('Pages.RequestAAPlaSend')) {
            this._router.navigate(['/app/main/ImplementationPlan'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
        else {
            this._router.navigate(['/app/main/ImplementationPlan']);
        }
    }

    funProjectFund() {
        if (this.isGranted('Pages.RequestAAPlaSend')) {
            this._router.navigate(['/app/main/ProjectFund'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
        else {
            this._router.navigate(['/app/main/ProjectFund'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
    }

    delete(del) {
        this._ImplimantionChecklistService.deleteImplimentationChecklist(del.implementationPlanCheckListID)
            .subscribe(() => {
                this.notify.success(this.l('Successfully Deleted Record'));
                this.getAllImplementaionActionAreaRecord();
            });
    }

    edit(res,imp) {
        this.myModalView.show(res,imp);
    }

    completeActivity(item) {
        this._ImplimantionChecklistService.updateActivityStatus(item.unitId).subscribe(res => {
            this.notify.success(this.l('Successfully updated status'));
            this.getAllImplementaionActionAreaRecord();
        })
    }
}
