﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import {
    ExpenseDto, UnitofMeasureDto, Expensesimage,
    CheklistandSublistDto1, UnitOfMeasuresServiceProxy, ImplimantionChecklistServiceProxy, ProgramExpenseServiceProxy, ExpensesTypeDto,
    UploadFileDto, UploadExpensesFileAppserviceServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
import * as moment from "moment";
import { Router, ActivatedRoute } from '@angular/router';
import { FileUploadModule } from 'primeng/primeng'
import { AppConsts } from '@shared/AppConsts';
import { FileUploader, FileUploaderOptions, Headers } from 'ng2-file-upload';
import { HttpClient, HttpRequest, HttpEventType } from '@angular/common/http';
import { IAjaxResponse } from "abp-ng2-module/src/abpHttpInterceptor";


declare var jQuery: any;


@Component({
    selector: 'ProjectExpensesCommunityContributionModal',
    templateUrl: "./projectExpensescommunitycontributionModal.component.html",
    styleUrls: ['./projectExpensescommunitycontributionModal.component.less'],
})
export class ProjectExpensesCommunityContributionModalComponent extends AppComponentBase {
    @ViewChild('ProjectExpensesCommunityContributionModal') modal: ModalDirective;
    @ViewChild('ProjectExpensesModalforstatus') modalstatus: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('addNewItemModal') MyModal: ModalDirective;

    public uploader: FileUploader = new FileUploader({ url: "" });
    private _uploaderOptions: FileUploaderOptions = {};
    saving: boolean = true;
    expenses: ExpenseDto = new ExpenseDto();
    getExpenses: ExpenseDto[];

    active: boolean = false;

    ExpenseType: ExpensesTypeDto[];
    unitOfMeasureList: UnitofMeasureDto[];
    subAcivity: CheklistandSublistDto1[];
    selectedCheckListItem: CheklistandSublistDto1[];

    imageList: Expensesimage = new Expensesimage();
    expensesImageList: Expensesimage[] = [];
    uploadedFiles: any[] = [];
    myFiles: any[] = [];
    uploadFilesdto: UploadFileDto[] = [];


    constructor(
        injector: Injector,
        private _UnitOfMeasureService: UnitOfMeasuresServiceProxy,
        private _ImplimantionChecklistServiceProxy: ImplimantionChecklistServiceProxy,
        private _ProgramExpensesServiceProxy: ProgramExpenseServiceProxy,
        private _ImplimantionChecklistService: ImplimantionChecklistServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _uploadImage: UploadExpensesFileAppserviceServiceProxy, private router: Router,
        private http: HttpClient
    ) {
        super(injector);

    }
    managerName: string;
    remaingbalance: number;

    show(res): void {
        this.active = true;
        if (res != undefined) {
            this.expenses = new ExpenseDto();
            $('#lblTitle').text('ADD Expense')
            this._ImplimantionChecklistService.getRemaingBalance(res.prgActionAreaActivityMappingID, res.programID).subscribe(result => {
                this.remaingbalance = result;
            })
            $("#ExpenseDate").val("Expense Date");
            this.expenses.programID = res.programID;
            this.expenses.mapSubActivityIteamsToImplementionPlanID = res.mapSubActivityIteamsToImplementionPlanID;
            this.expenses.expensesYear = res.costEstimationYear;
            this.expenses.prgActionAreaActivityMappingID = res.prgActionAreaActivityMappingID;
            this.expenses.implementationPlanCheckListID = res.implementationPlanCheckListID;
            this.expenses.managerID = res.creatorUserId;
            this.managerName = res.managerName;
            this.expenses.subActivityName = res.checklistItemName;
            this.expenses.subActivityId = res.checklistItemID;
            this.expenses.quarterId = res.quarterId;
            this.expenses.expensesimage = this.expensesImageList;
            this.GetExpenseAsPerProgramID(res.programID);
            this.myFiles = [];
            this._ImplimantionChecklistService.getMapsubActivityForImplentionPlan(res.prgActionAreaActivityMappingID).subscribe(result => {
                this.selectedCheckListItem = result;
                this.modal.show();

            });
            this.initializeModal();
        }
    }
    ngOnInit() {
        $("#ExpenseDate").val("" + moment(this.expenses.expenseDate).format('MM/DD/YYYY'));

    }

    initializeModal(): void {
        this.active = true;
        this.initFileUploader();
    }

    removeFile(file: File, uploader: any) {
        const index = uploader.indexOf(file);
        uploader.splice(index, 1);

    }

    initFileUploader(): void {
        const self = this;

        self.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Upload/UploadExpenseImage' });
        self._uploaderOptions.autoUpload = true;

        self.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        self.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {  
                this.myFiles.push({
                    name: resp.result.fileName,
                    image: AppConsts.remoteServiceBaseUrl + '/Common/Images/SampleProfilePics/' + resp.result.fileName + '?v=' + new Date().valueOf(),
                    file: item.file
                });
            } else {
                this.message.error(resp.error.message);
            }
        };
        self.uploader.setOptions(self._uploaderOptions);
    }


    ngAfterViewInit(): void {
        this.GetAllUnitOfMeasure();
        this.getExpensesType();
        this.dateTime();
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }

    save() {
        this.saving = true;

        const self = this;
        for (let i of this.myFiles) {
            const input = new UploadFileDto();
            input.fileName = i.name;
            this.uploadFilesdto.push(input);
        }
        abp.ui.setBusy();
        this._uploadImage.uploadFiles(this.uploadFilesdto, "wotrexp").subscribe(result => {
            for (let p of result) {
                this.imageList = new Expensesimage();
                this.imageList.image = p;
                this.expenses.expensesimage.push(this.imageList);
            }
            this._ProgramExpensesServiceProxy.createORUpdate(this.expenses)
                .finally(() => { this.saving = false; })
                .subscribe((result) => {
                    this.notify.info(this.l("Saved SuccessFully"));
                    this.close();
                    abp.ui.clearBusy();
                });
        });
    }

    GetAllUnitOfMeasure() {
        this._UnitOfMeasureService.getAllUnitOfMeasure().subscribe(result => {
            this.unitOfMeasureList = result;
        })
    }

    getExpensesType() {
        this._ProgramExpensesServiceProxy.getAllExpensesType().subscribe(result => {
            this.ExpenseType = result;
        })
    }



    dateTime() {
        $(function () {
            $("#ExpenseDate").datetimepicker({
            });
        });

        $("#ExpenseDate").on("dp.change", () => {
            var dateString = $("#ExpenseDate").val();
            var dateObj = new Date(dateString.toString());
            this.expenses.expenseDate = moment(dateObj);
        });
    }

    GetExpenseAsPerProgramID(n) {
        abp.ui.setBusy();
        this._ProgramExpensesServiceProxy.getExpenseAsPerProgramID(n).subscribe(result => {
            this.getExpenses = result;
            console.log(this.getExpenses);
            abp.ui.clearBusy();
        })
    }

    editActionArea(res) {
        this.modal.show();
    }

    StatusClick(res, status) {
        this.saving = true;
        if (status == 2) {
            res.status = "Approved";
        } else {
            res.status = "Reject";
        }

        this._ProgramExpensesServiceProxy.createORUpdate(res)
            .finally(() => { this.saving = false; })
            .subscribe((result) => {
                this.notify.info(this.l("Saved SuccessFully"));
                this.close();
            });
    }


    private inputFocused = false;
    AmountCheck(val, event) {
        if (this.remaingbalance < val) {
            alert("Amount must be less Than TotalCostUnit" + " " + this.remaingbalance)
            this.expenses.amount = null;
            this.inputFocused = true;
            document.getElementById('amountinput').focus();
        } else if (val == 0) {
            alert("Please Entere Valid Amount")
            this.expenses.amount = null;
            this.inputFocused = true;
            document.getElementById('amountinput').focus();

        }
        else if (isNaN(val)) {
            alert("Please Entere Number")
            this.expenses.amount = null;
            this.inputFocused = true;
            document.getElementById('amountinput').focus();
        }
    }



    onBeforeSend(event): void {
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
    }
}

