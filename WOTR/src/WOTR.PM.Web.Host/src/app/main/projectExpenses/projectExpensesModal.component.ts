﻿import { Component, Injector, ViewChild, EventEmitter, Output } from '@angular/core';
import {
    ExpenseDto, UnitofMeasureDto, Expensesimage,
    CheklistandSublistDto1, UnitOfMeasuresServiceProxy, ImplimantionChecklistServiceProxy, ProgramExpenseServiceProxy, ExpensesTypeDto,
    UploadFileDto, UploadExpensesFileAppserviceServiceProxy, PushNotifiactionServiceProxy
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import * as moment from "moment";
import { AppConsts } from '@shared/AppConsts';
import { FileUploader, FileUploaderOptions, Headers } from 'ng2-file-upload';
import { IAjaxResponse } from "abp-ng2-module/src/abpHttpInterceptor";
import { forEach } from '@angular/router/src/utils/collection';
import { AbpSessionService } from 'abp-ng2-module/src/session/abp-session.service';
declare var jQuery: any;
import { from } from 'rxjs/observable/from';
import { Paginator, LazyLoadEvent } from 'primeng/primeng';

@Component({
    selector: 'ProjectExpensesModal',
    templateUrl: "./projectExpensesModal.component.html",
    styleUrls: ['./projectExpensesModal.component.less'],
})
export class ProjectExpensesModalComponent extends AppComponentBase {
    @ViewChild('ProjectExpensesModal') modal: ModalDirective;
    @ViewChild('ProjectExpensesModalforstatus') modalstatus: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('addNewItemModal') MyModal: ModalDirective;
    @ViewChild('paginator') paginator: Paginator;

    public uploader: FileUploader = new FileUploader({ url: "" });
    private _uploaderOptions: FileUploaderOptions = {};
    saving: boolean = true;
    expenses: ExpenseDto = new ExpenseDto();
    getExpenses: ExpenseDto[];
    active: boolean = false;
    userId: number;
    checklistItemName: string;
    ExpenseType: ExpensesTypeDto[];
    unitOfMeasureList: UnitofMeasureDto[];
    subAcivity: CheklistandSublistDto1[];
    selectedCheckListItem: CheklistandSublistDto1[];
    imageList: Expensesimage = new Expensesimage();
    expensesImageList: Expensesimage[] = [];
    uploadedFiles: any[] = [];
    myFiles: any[] = [];
    uploadFilesdto: UploadFileDto[] = [];
    flag: boolean = false;
    filter: string = "";
    programId: number;
    imgsrc: string = 'https://wotrorg.blob.core.windows.net/wotrexp/IMG-20181129-WA0050.jpg';

    constructor(
        injector: Injector,
        private _UnitOfMeasureService: UnitOfMeasuresServiceProxy,
        private _ProgramExpensesServiceProxy: ProgramExpenseServiceProxy,
        private _ImplimantionChecklistService: ImplimantionChecklistServiceProxy,
        private _pushNotificationService: PushNotifiactionServiceProxy,
        private _uploadImage: UploadExpensesFileAppserviceServiceProxy, ) {
        super(injector);
    }
    managerName: string;
    remaingbalance: number;
    managerId: any;

    show(res, record): void {
        this.flag = false;
        this.managerId = record.managerID;
        this.active = true;
        if (res != undefined) {
            this.expenses = new ExpenseDto();
            $('#lblTitle').text('ADD Expense');
            this._ImplimantionChecklistService.getRemaingBalance(res.prgActionAreaActivityMappingID, res.programID)
                .subscribe(result => {
                    this.remaingbalance = result;
                });
            $("#ExpenseDate").val("Expense Date");
            this.userId = res.assingUserId;
            this.checklistItemName = res.checklistItemName;
            this.expenses.programID = res.programID;
            this.expenses.mapSubActivityIteamsToImplementionPlanID = res.mapSubActivityIteamsToImplementionPlanID;
            this.expenses.expensesYear = res.costEstimationYear;
            this.expenses.prgActionAreaActivityMappingID = res.prgActionAreaActivityMappingID;
            this.expenses.implementationPlanCheckListID = res.implementationPlanCheckListID;
            this.expenses.managerID = res.creatorUserId;
            this.managerName = res.managerName;
            this.expenses.subActivityName = res.checklistItemName;
            this.expenses.subActivityId = res.checklistItemID;
            this.expenses.quarterId = res.quarterID;
            this.expenses.expensesimage = this.expensesImageList;
            this.GetExpenseAsPerProgramID();
            this.myFiles = [];
            this.programId = res.programID;
            this._ImplimantionChecklistService.getMapsubActivityForImplentionPlan(res.prgActionAreaActivityMappingID).subscribe(result => {
                this.selectedCheckListItem = result;
                this.modal.show();
            });
            this.initializeModal();
        }
    }
    ngOnInit() {
        $("#ExpenseDate").val("" + moment(this.expenses.expenseDate).format('MM/DD/YYYY'));
    }

    initializeModal(): void {
        this.active = true;
        this.initFileUploader();
    }

    removeFile( id,file: File, uploader: any) {
        const index = uploader.indexOf(file);
        var ImageId = id;
        uploader.splice(index, 1);
        this._ProgramExpensesServiceProxy.deleteExpenseImage(ImageId).subscribe(
            result => {
                this.notify.info(this.l("Image removed successfully"));

            });

    }

    initFileUploader(): void {
        const self = this;
        self.uploader = new FileUploader({ url: AppConsts.remoteServiceBaseUrl + '/Upload/UploadExpenseImage' });
        self._uploaderOptions.autoUpload = true;

        self.uploader.onAfterAddingFile = (file) => {
            file.withCredentials = false;
        };

        self.uploader.onSuccessItem = (item, response, status) => {
            const resp = <IAjaxResponse>JSON.parse(response);
            if (resp.success) {
                this.myFiles.push
                    ({
                        name: resp.result.fileName,
                        image: AppConsts.remoteServiceBaseUrl + '/Common/Images/SampleProfilePics/' + resp.result.fileName + '?v=' + new Date().valueOf(),
                        file: item.file,
                        id: 0  // currenntly added parameter
                    });
            } else {
                this.message.error(resp.error.message);
            }
        };
        self.uploader.setOptions(self._uploaderOptions);
    }

    ngAfterViewInit(): void {
        this.GetAllUnitOfMeasure();
        this.getExpensesType();
        this.dateTime();
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }

    save() {
        this.saving = true;
        this.uploadFilesdto = [];

        for (let i of this.myFiles) {
            const input = new UploadFileDto();
            input.fileName = i.name;
            if (i.id == 0) {
                this.uploadFilesdto.push(input);
            }
        }
        abp.ui.setBusy();

        this._uploadImage.uploadFiles(this.uploadFilesdto, "wotrexp").subscribe(result => {
            this.expenses.expensesimage = [];
            for (let p of result) {
                this.imageList = new Expensesimage();
                this.imageList.image = p;
                this.expenses.expensesimage.push(this.imageList);
            }
            this._ProgramExpensesServiceProxy.createORUpdate(this.expenses)
                .finally(() => { this.saving = false; })
                .subscribe((result) => {
                    this.notify.info(this.l("Saved successfully"));
                    this.close();
                    abp.ui.clearBusy();
                });
        });
        this.initializeModal();
    }

    GetAllUnitOfMeasure() {
        this._UnitOfMeasureService.getAllUnitOfMeasure().subscribe(result => {
            this.unitOfMeasureList = result;
        });
    }

    getExpensesType() {
        this._ProgramExpensesServiceProxy.getAllExpensesType().subscribe(result => {
            this.ExpenseType = result;
        });
    }

    Edit(record) {
        //this.expenses.unit = record.7
        this.myFiles = [];
        this.flag = true;
        this.expenses = record;
        this.expenses.creatorUserId = this.expenses.managerID;
        this.expenses.tenantId = abp.session.tenantId;
        this.expensesImageList = this.expenses.expensesimage;

        var k = this;
        this.expensesImageList.forEach(function (value) {
            k.myFiles.push({
                image: value.image,
                name : value.image,
                id : value.id
            });
        },this);
    }


    dateTime() {
        $(function () {
            $("#ExpenseDate").datetimepicker({
                format: 'MM/DD/YYYY'
            });
        });

        $("#ExpenseDate").on("dp.change", () => {
            var dateString = $("#ExpenseDate").val();
            var dateObj = new Date(dateString.toString());
            this.expenses.expenseDate = moment(dateObj);
        });

        $(".fa-calendar").on("click", function () {
            $(this).siblings("input").datetimepicker("show");

        });
    }

    GetExpenseAsPerProgramID( event?: LazyLoadEvent) {
        abp.ui.setBusy();

        this._ProgramExpensesServiceProxy.getExpenseAsPerProgramIDForExcelReport(
            this.filter,
            this.expenses.programID,
            this.expenses.expensesYear, null,
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event),
            this.primengDatatableHelper.getSkipCount(this.paginator, event)
        ).subscribe(result => {
            this.getExpenses = result;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.records = result;
            abp.ui.clearBusy();
        });
    }

    getPageChangeHandler(event?: LazyLoadEvent) {
        var d = this.primengDatatableHelper.getMaxResultCount(this.paginator, event);
        var b = this.primengDatatableHelper.getSkipCount(this.paginator, event);
        
    }

    editActionArea(res) {
        this.modal.show();
    }

    StatusClick(res, status) {
        this.saving = true;
        var msg = "";
        if (status == 2) {
            res.status = "Approved";
            msg = this.checklistItemName + " Project Expenses Approve ";

        } else {
            res.status = "Reject";
            msg = this.checklistItemName + " Project Expenses Reject ";
        }

        this._ProgramExpensesServiceProxy.createORUpdate(res)
            .finally(() => { this.saving = false; })
            .subscribe((result) => {
                this.notify.info(this.l("Saved Successfully"));
                this.close();
            });
        this._pushNotificationService.sendPushNotification(this.userId.toString(), msg, "Notification").subscribe
            (result => {
                this.close();
            });
    }

    AmountCheck(val, event) {
        //if (this.remaingbalance < val) {
        //    alert("Amount must be less Than TotalCostUnit" + " " + this.remaingbalance)
        //    this.expenses.amount = null;
        //    this.inputFocused = true;
        //    document.getElementById('amountinput').focus();
        //} else if (val == 0) {
        //    alert("Please Entere Valid Amount")
        //    this.expenses.amount = null;
        //    this.inputFocused = true;
        //    document.getElementById('amountinput').focus();

        //}
        //else if (isNaN(val)) {
        //    alert("Please Entere Number")
        //    this.expenses.amount = null;
        //    this.inputFocused = true;
        //    document.getElementById('amountinput').focus();
        //}
    }



    onBeforeSend(event): void {
        event.xhr.setRequestHeader('Authorization', 'Bearer ' + abp.auth.getToken());
    }

    delete(record) {
        this._ProgramExpensesServiceProxy.deleteExpense(record.id).subscribe(
            result => {
                this.notify.info(this.l("Expense deleted successfully"));
                this.GetExpenseAsPerProgramID()
            })
    }
}

