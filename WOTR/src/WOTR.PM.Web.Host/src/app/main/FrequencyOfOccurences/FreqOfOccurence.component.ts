﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { FrequencyOfOccurencesServiceProxy, GetAllFreqOfOccurrence } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { FreqOfOccurrenceModalComponent } from './FreqOfOccurence.modal.component';

//import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'freqOfOccurrenceComponent',
    templateUrl: "./FreqOfOccurence.component.html",
    styleUrls: ['./FreqOfOccurence.component.less']

})

export class FreqOfOccurrenceComponent extends AppComponentBase {
    @ViewChild('FreqOfOccurrenceModal') myModalView: FreqOfOccurrenceModalComponent;
    searchText = "";
    freqOfOccurrence: GetAllFreqOfOccurrence[] = [];
    freqOfOccurrenceList: GetAllFreqOfOccurrence[] = []
    timer: any;
    currentPage: number = 1;


    constructor(
        injector: Injector,
        private _frequencyOfOccurencesAppService: FrequencyOfOccurencesServiceProxy) {
        super(injector);
    }

    ngOnInit() {
        this.getAllFreqOfOccurrence();
    }

   


    getAllFreqOfOccurrence(): void {
        abp.ui.setBusy();
        this._frequencyOfOccurencesAppService.getAllFreqOfOccurrence(this.searchText).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.length;

            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;

            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.freqOfOccurrenceList = result;
            this.freqOfOccurrence = result;
            this.freqOfOccurrenceList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();

        })

    }

    searchPC(): void {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => { this.getAllFreqOfOccurrence() }, 1000);
    }

    editActionArea(p) {

        this.myModalView.show(p);
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.freqOfOccurrence.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.freqOfOccurrenceList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);

    }

    delete(p) {
        this.message.confirm(
            this.l('Frequency Of Occurrence', ''),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._frequencyOfOccurencesAppService.delete(p.id)
                        .subscribe((result) => {
                            this.notify.success(this.l('Successfully Deleted  Record'));
                            this.getAllFreqOfOccurrence();
                        });
                };

            });
    }

}