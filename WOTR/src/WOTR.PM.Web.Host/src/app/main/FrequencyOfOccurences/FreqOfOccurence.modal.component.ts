﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { FrequencyOfOccurencesServiceProxy, FrequencyOfoccurrence } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
declare var jQuery: any;

@Component({
    selector: 'FreqOfOccurrenceModal',
    templateUrl: "./FreqOfOccurence.modal.component.html",
    styleUrls: ['./FreqOfOccurence.modal.component.less']
})

export class FreqOfOccurrenceModalComponent extends AppComponentBase {
    @ViewChild('FreqOfOccurrenceModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    frequencyOfoccurrence: FrequencyOfoccurrence = new FrequencyOfoccurrence();
    saving: boolean = false;


    constructor(injector: Injector,
        private _FrequencyOfOccurenceServiceProxy: FrequencyOfOccurencesServiceProxy
    ) {
        super(injector)
    }

    ngOnInit() {


    }

    show(p) {
        this.active = true;
        if (p != undefined) {
            this.frequencyOfoccurrence = p;
            $('#lblTitle').text('Edit Frequncy Of Occurrence');
        }
        else {
            this.frequencyOfoccurrence = new FrequencyOfoccurrence();
        }
        this.active = true;
        this.modal.show();

    }



    save(): void {
        this.saving = true;
        if (this.frequencyOfoccurrence.freqOfOccurrence.trim() != "") {
            var r = this._FrequencyOfOccurenceServiceProxy.creatOrUpdateFreqOfOccurrence(this.frequencyOfoccurrence)
                .finally(() => this.saving = false)
                .subscribe((result) => {
                    if (result == "Record alread Present!") {
                        this.notify.warn(this.l(result));
                    }
                    if (result == "Frequncy Of Occurrence Update  sucessfully !" || result == "Frequncy Of Occurrence Added  sucessfully !") {
                        this.notify.info(this.l(result));
                    }
                    this.close();
                    this.modalSave.emit(null);
                });
        } else {
            this.notify.info("Please Enter Frequncy Of Occurrence");
            this.modalSave.emit(null);
        }

    }


    close(): void {
        this.modal.hide();
        this.active = false;
    }

}