﻿import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { ImplimantionChecklistServiceProxy, SubUnitActivityQuarterWise, SubUnitActivityQuarterWisedetails, ImplementationListDto, ListActionSubactionListDto, Village, RequestAAPlasListDto, PrgRequestAAPlasServiceProxy, ProgramServiceProxy, ProgramInformationListDto, PrgVillageClusterListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute } from '@angular/router';
import { FileDownloadService } from '@shared/utils/file-download.service';


@Component({
    selector: 'implemenationPlanGroupingDetailscomponent',
    templateUrl: "./implemenationPlanGroupingDetails.component.html",
    styleUrls: ['./implemenationPlanGroupingDetails.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ImplemenationPlanGroupingDetailsComponent extends AppComponentBase {
    selectedPermission = '';
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: ProgramInformationListDto[] = [];
    programdetails: ProgramInformationListDto[];
    programd: ProgramInformationListDto = new ProgramInformationListDto();
    VillageList: any = [];
    ActivityList: any = [];
    ComponentList: any = [];
    ImplementationList: ImplementationListDto[];
    ImplementationListquaerterwise: SubUnitActivityQuarterWisedetails = new SubUnitActivityQuarterWisedetails();

    VillageClustorList: any = [];
    program: ProgramInformationListDto[];
    requestList: RequestAAPlasListDto[] = [];
    villageclustor: PrgVillageClusterListDto[];
    pro: any[];
    active: boolean = false;
    list: any = [];
    requestSingleYearList: RequestAAPlasListDto = new RequestAAPlasListDto();
    AddSubActionList: ListActionSubactionListDto;
    emailContent: any;
    saving: boolean = false;
    prgVilages: Village[] = [];
    programName: string;
    locationName: string;
    managerName: string;
    requestStatus: number;
    Granamout: number;
    ddd: number[];
    Manager: number;
    startdate: string;
    EndDate: string;
    aaaa: boolean;
    activitylist: any = [];
    QuarterName: string;
    QuarterId: number;
    Year: any = [];
    costyear: string;
    class: string;
    quarter2: string;
    class1: string;
    quarter4: string;
    ImplementationListquaerter1: SubUnitActivityQuarterWise[];
    ImplementationListquaerter2: SubUnitActivityQuarterWise[];
    ImplementationListquaerter3: SubUnitActivityQuarterWise[];
    ImplementationListquaerter4: SubUnitActivityQuarterWise[];
    Home: any;
    Menu1: any;
    Menu2: any;
    Menu3: any;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _programService: ProgramServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _ImplimantionChecklistService: ImplimantionChecklistServiceProxy, private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy) {
        super(injector);
    }

    ngOnInit() {
        this.getAllProgram();
        this.programName = this._activatedRoute.snapshot.queryParams.ProgrameName;
        this.class = "tab-pane fade in active";
        this.quarter2 = "tab-pane fade";
        this.class1 = "tab-pane fade";
        this.quarter4 = "tab-pane fade";
        $("#" + 0).addClass("tab-pane fade in active");
        $("#" + 1).addClass("tab-pane fade");
    }

    getAllProgram() {
        this._programService.getAllProgram().subscribe(result => {
            this.programdetails = result;
        });
    }

    onChangeState(event) {
        //this.getAllProgrambyid(event.target.value);
    }

    getcostestimationyear(programId) {
        this._PrgRequestAAPlasService.prgCostEstimationYears(programId).subscribe(result => {
            this.Year = result;

        });
    }
    changetab(id) {
        id = "Home";
        let myDiv = document.getElementById(id) as HTMLInputElement;
        myDiv.className = 'tab-pane fade in active in show';
        this.class = myDiv.className;
        this.quarter2 = 'tab-pane fade';
        this.class1 = 'tab-pane fade';
        this.quarter4 = 'tab-pane fade';
    }

    changetab1(id) {
        id = "Menu1";
        let myDiv1 = document.getElementById(id) as HTMLInputElement;
        this.quarter2 = 'tab-pane fade in active in show';
        this.class = "tab-pane fade";
        this.class1 = 'tab-pane fade';
        this.quarter4 = 'tab-pane fade';
    }
    changetab2(id) {
        id = "Menu2";
        let myDiv2 = document.getElementById(id) as HTMLInputElement;
        this.class1 = 'tab-pane fade in active in show';
        this.class = "tab-pane fade";
        this.quarter2 = "tab-pane fade";
        this.quarter4 = 'tab-pane fade';

    }
    changetab3(id) {
        id = "Menu3";
        let myDiv2 = document.getElementById(id) as HTMLInputElement;
        this.quarter4 = 'tab-pane fade in active in show';
        this.class = "tab-pane fade";
        this.quarter2 = "tab-pane fade";
        this.class1 = 'tab-pane fade';

    }
    getAllProgrambyid(programId, year) {
        abp.ui.setBusy();
        this.costyear = year;
        this._ImplimantionChecklistService.getImplementionPlanForExcelReport(programId, year).subscribe(result => {
            this.ImplementationList = result;
            if (result.length != 0) {
                this.programName = this.ImplementationList[0].programName;
            }
        });
        this._ImplimantionChecklistService.getImplementionPlanForExcelReport1(programId, year).subscribe(result => {
            this.ImplementationListquaerterwise = result;
            this.ImplementationListquaerter1 = result.subUnitActivityQuarterWises1;
            this.ImplementationListquaerter2 = result.subUnitActivityQuarterWises2;
            this.ImplementationListquaerter3 = result.subUnitActivityQuarterWises3;
            this.ImplementationListquaerter4 = result.subUnitActivityQuarterWises4;

            abp.ui.clearBusy();
        });
    }

    DivideUnits(r, res, index) {
        this.QuarterId = index;
        this.QuarterName = r;
    }

    exportToExcel(programId): void {
        abp.ui.setBusy();
        this._ImplimantionChecklistService.createExcelImpilemetationDoc("ImpimentationPlanDetails", programId, this.costyear)
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile1(result);
                abp.ui.clearBusy();
            });
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
}