﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { RoleServiceProxy, State, LocationServiceProxy, District,Village} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalVillageComponent } from './ModalVillage.component';
//import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    selector: 'villagecomponent',
    templateUrl: "./village.component.html",
    styleUrls: ['./village.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class VillageComponent extends AppComponentBase {    
    @ViewChild('ModalVillage') myModalView: ModalVillageComponent;
    selectedPermission = '';
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: State[] = [];
    district: District = new District();
    districtlist: District[] = [];
    village: Village[] = [];
    villagelist: Village[] = [];
    active: boolean = false;
    list: any = [];
    filterText: string = "";
    timer: any;
    searchText;
    constructor(
        injector: Injector,
        private _locationService: LocationServiceProxy,

        private _roleService: RoleServiceProxy)
    {
        super(injector);
    }

    ngOnInit() {
        this.getallvellage(); 
        
    }    
    getAllvillagerecordssearch(): void { 
        // var input = (document.getElementById("txtActionArea") as HTMLInputElement).value;
        //var input = this.filterText;
        var input = this.searchText.trim();
        abp.ui.setBusy();
        this._locationService.getallvillages(input).subscribe((result) => {       
            this.primengDatatableHelper.totalRecordsCount = result.length;
           
            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;
         
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.villagelist = result;
            this.village = result;
            this.villagelist = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        });
    }


    getallvellage(): void {
        var input = (document.getElementById("txtActionArea") as HTMLInputElement).value;
        
        this._locationService.getallvillages(input).subscribe((result) => {
          
            this.primengDatatableHelper.records = result;
            this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.villagelist = result;
            this.village = result;
            this.villagelist = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
           
        });
    }

    search() {
        let current = this;
        this.timer = setTimeout(() => { this.getAllvillagerecordssearch() }, 1000);
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.village.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.villagelist = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
    }

    editActionArea(p) {
        this.myModalView.show(p);
    }
    
        count: number = -1;
        finalUnits: number = 0;
    sumOfTotal: number = 0;
    sumOfTotal1: number = 0;
    DivideUnits(res, rec) {
    }
    
    deletedistrict(del) {
        this.message.confirm(
            this.l('VillageDeleteWarning', del.name),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._locationService.deleteVillage(del.id)
                        .subscribe((result) => {
                            if (result == "Record Exist") {
                                this.notify.success(this.l('This record is already in used'));
                                this.getallvellage();
                            }
                            else {
                                this.notify.success(this.l('Successfully Deleted Action Area Record'));
                                this.getallvellage();
                            }
                        });
                }
            })
    }
}