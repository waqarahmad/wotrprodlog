﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { ActionAreasServiceProxy, ActionAreasListDto, State, LocationServiceProxy, District, Taluka,Village} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
declare var jQuery: any;
@Component({
    selector: 'ModalVillage',
    templateUrl: "./ModalVillage.component.html",
    styleUrls: ['./ModalVillage.component.less'],
})
export class ModalVillageComponent extends AppComponentBase {
    @ViewChild('ModalVillage') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    passwordComplexityInfo = '';
    district: District = new District();
    saving: boolean = false;
    search: ActionAreasListDto[] = [];
    StateList: State[];
    stateList: State[];
    districtList: District[];
    talukaList: Taluka[];
    villageList: Village[];
    @ViewChild('projectForm') eForm: NgForm;//yo
    village: Village = new Village();

    constructor(
        injector: Injector,
        private _actionareaService: ActionAreasServiceProxy,
        private _locationService: LocationServiceProxy,
)
    {
        super(injector);        
    }
    ngAfterViewInit() {
        this.getAllState();
    }
    show(p): void {
        this.active = true;
        if (p != undefined) {
            this.getAllDistrict(p.stateID);
            this.getAllTaluka(p.districtId);

            this.village = p;
            $('#lblTitle').text('Edit Village');
        }
        else {
            this.village = new Village();
        }
        this.modal.show();
    }

    close(): void {
        this.modal.hide();
        this.active = false;       
    }
    //...save the Record in ActionArea table.
    save(): void {
         this.saving = true;
        var r = this._locationService.creatOrUpdateVillage(this.village)
            .finally(() => this.saving = false)
            .subscribe((result) => {
                if (result[0] =="Record is already Present") {
                    this.notify.warn(this.l(result[0]));
                }
                if (result[0] == "ActionArea Update  sucessfully !" || result[0] == "ActionArea Added  sucessfully !") {
                    this.notify.info(this.l(result[0]));
                }
                this.close();
                this.modalSave.emit(null);
            });
    }
    getAllState() {
        this._locationService.getAllState().subscribe(result => {
            this.stateList = result;
        })
    }

    //modified 
    onChangeState(event) {
        this.talukaList = [];
        this.getAllDistrict(event.target.value);
       
    }
    getAllDistrict(stateId) {
        this._locationService.getAllDistrict(stateId).subscribe(result => {
            this.districtList = result;
        })
    }


    onChangeDistrict(event) {
        this.talukaList = [];
        this.getAllTaluka(event.target.value);
    }

    getAllTaluka(districtId) {

        this._locationService.getAllTaluka(districtId).subscribe(result => {
            this.talukaList = result;

        })

    }

    //onChangeTaluka(event) {
    //    this.villageList = [];
    //    this.getAllVillage(event.target.value);
    //}
 
}