﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { PrgVillageClusterServiceProxy, ProgramServiceProxy, ProgramInformationListDto, PrgVillageClusterListDto, PrgRequestAAPlasServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalProgramDetailsComponent } from './ModalProgramDetails.component';
import { FileDownloadService } from '@shared/utils/file-download.service';



@Component({
    selector: 'programDetailscomponent',
    templateUrl: "./programDetails.component.html",
    styleUrls: ['./programDetails.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ProgramDetailsComponent extends AppComponentBase {
    @ViewChild('ModalProgramDetails') myModalView: ModalProgramDetailsComponent;
    selectedPermission = '';
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: ProgramInformationListDto[] = [];
    programdetails: ProgramInformationListDto[];
    programd: ProgramInformationListDto = new ProgramInformationListDto();
    VillageList: any = [];
    ActivityList: any = [];
    ComponentList: any = [];
    VillageClustorList: any = [];
    program: ProgramInformationListDto[];
    villageclustor: PrgVillageClusterListDto[];
    pro: any[];
    active: boolean = false;
    list: any = [];
    villageClusterList: PrgVillageClusterListDto[];

    programName: string;
    DonorName: string;
    SanctionedYear: number;
    Granamout: number;
    ddd: number[];
    Manager: number;
    startdate: string;
    EndDate: string;
    aaaa: boolean;
    Year: any = [];
    costyear: string;

    constructor(
        injector: Injector,
        private _programService: ProgramServiceProxy,
        private _PrgVillageClusterService: PrgVillageClusterServiceProxy,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy,
        private _fileDownloadService: FileDownloadService) {
        super(injector);
    }

    ngOnInit() {

        this.getAllProgram();
   this.getcostestimationyear();
    }
   getcostestimationyear() {
        this._PrgRequestAAPlasService.overallPrgCostEstimationYears().subscribe(result => {
            this.Year = result;

        });
    }

    getAllProgram() {
        abp.ui.setBusy();
        this._programService.getAllProgram().subscribe(result => {
            this.programdetails = result;
            abp.ui.clearBusy();
        });
    }

    editActionArea(p) {
        this.myModalView.show(p);
    }

    count: number = -1;
    finalUnits: number = 0;
    sumOfTotal: number = 0;
    sumOfTotal1: number = 0;

    onChangeState(event) {
        //this.getAllProgramebyId(event.target.value);
    }

    getAllProgramebyId(programId,year) {
        this.costyear = year;
        abp.ui.setBusy();
        this._programService.getProgramDetailsbyID(programId).subscribe(result => {
            this.program = result;
            this.programName = this.program[0].programName;
            this.DonorName = this.program[0].donorName;
            this.SanctionedYear = this.program[0].programSanctionedYear;
            this.Granamout = this.program[0].gantnAmountSanctionRupess;
            this.startdate = (this.program[0].programStartDate).format('MM/DD/YYYY');
            this.EndDate = (this.program[0].prgramEndDate).format('MM/DD/YYYY');
        });

        this._programService.getProgramDetailsbyvillageclustorforExcel(programId,this.costyear).subscribe(result => {
            this.villageclustor = result;
            this.VillageClustorList = [];
            this.VillageList = [];
            this.ComponentList = [];
            this.ActivityList = [];
            for (let i of result[0].villageClusterList) {
                this.VillageClustorList.push(i.name);
            }
            for (let v of result[0].villages) {
                this.VillageList.push(v.name);
            }

            for (let c of result[0].components) {
                this.ComponentList.push(c.componentName);
            }
            for (let A of result[0].activity) {
                var z = {
                    "ActivityName": A.activityName,
                    "TotalUnit": A.totalUnit,
                    "UnitCost": A.unitCost,
                    "TotalUnitCost": A.totalUnitCost,
                    "CoomunityContribution": A.communityConstribution,
                    "FunderContribution": A.funderConstribution,
                    "OtherContribution": A.otherConstribution,
                    "AllTotal": A.allTotal
                };
                this.ActivityList.push(z);
            }
            abp.ui.clearBusy();
        });

        this._PrgVillageClusterService.getAllVillageCluster(programId, this.costyear).subscribe(result => {
            this.villageClusterList = result;
        });

    }



    exportToExcel(programId): void {
        abp.ui.setBusy();
        this._programService.createExcelDoc("ProgramDetails", programId,this.costyear).subscribe(result => {
            this._fileDownloadService.downloadTempFile1(result);
            abp.ui.clearBusy();
        });
    }

    export(e) {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('div[id$=a]').html()));
        e.preventDefault();
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }

}