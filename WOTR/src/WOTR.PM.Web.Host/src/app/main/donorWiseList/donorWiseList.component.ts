﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { ListActionSubactionListDto, Village, RequestAAPlasListDto, ProgramServiceProxy, ProgramInformationListDto, PrgVillageClusterListDto, Donorwisedto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute } from '@angular/router';
import { FileDownloadService } from '@shared/utils/file-download.service';
declare let jsPDF;



@Component({
    selector: 'donorWiseListcomponent',
    templateUrl: "./donorWiseList.component.html",
    styleUrls: ['./donorWiseList.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class DonorWiseListComponent extends AppComponentBase {
    @ViewChild('test') el: ElementRef;

    selectedPermission = '';
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: ProgramInformationListDto[] = [];
    Records: Donorwisedto[] = [];

    programdetails: ProgramInformationListDto[];
    programd: ProgramInformationListDto = new ProgramInformationListDto();
    VillageList: any = [];
    ActivityList: any = [];
    ComponentList: any = [];
    VillageClustorList: any = [];
    program: ProgramInformationListDto[];

    requestList: RequestAAPlasListDto[] = [];
    villageclustor: PrgVillageClusterListDto[];
    pro: any[];
    active: boolean = false;
    list: any = [];
    requestSingleYearList: RequestAAPlasListDto = new RequestAAPlasListDto();
    AddSubActionList: ListActionSubactionListDto;
    emailContent: any;
    saving: boolean = false;
    prgVilages: Village[] = [];
    Year: any = [];
    costyear: string;
    programName: string;
    locationName: string;
    managerName: string;
    requestStatus: number;
    costestimationyear: string;
    ActivityLevel: Number;
    activityname: string;
    Granamout: number;
    ddd: number[];
    Manager: number;
    startdate: string;
    EndDate: string;
    aaaa: boolean;

    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _programService: ProgramServiceProxy,
        private _fileDownloadService: FileDownloadService) {
        super(injector);
    }

    ngOnInit() {
        this.getallyears();
        this.programName = this._activatedRoute.snapshot.queryParams.ProgrameName;
    }

    getallyears() {
        this._programService.prgCostEstimationYears().subscribe(result => {
            this.Year = result;
        });
    }

    getAllProgrambyid(Year) {
        this.costyear = Year;
        this._programService.getAllProgramsDetailsDonorWise(Year).subscribe(result => {
            this.Records = result;
        });
    }

    createPdf(): void {
        let pdf = new jsPDF('l', 'pt', 'a4');
        let options = {
            pagesplit: true
        };
        pdf.addHTML(this.el.nativeElement, 0, 0, options, () => {
            pdf.save("test.pdf");
        });
    }

    exportToExcel(): void {
        this._programService.createExcelDocallDonorWise("Donor Wise", this.costyear).subscribe(result => {
            this._fileDownloadService.downloadTempFile1(result);
        })
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
}