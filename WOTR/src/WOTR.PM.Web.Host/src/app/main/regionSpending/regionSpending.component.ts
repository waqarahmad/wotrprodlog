﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import {
    ProgramRegionSpendingListDto, ProgramRegionSpendingServiceProxy, LocationListDto,
    LocationServiceProxy, SubListActivityforProgamRegionSpendingDto, PrgVillageClusterServiceProxy, PrgVillageClusterListDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';


declare var jQuery: any;
declare var google: any;
var expanded = false;
declare var divId: any;


@Component({
    selector: 'regionSpending',
    templateUrl: "./regionSpending.component.html",
    styleUrls: ['./regionSpending.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class regionSpendingComponent extends AppComponentBase {
    ProgramRegionSpending: ProgramRegionSpendingListDto[] = [];
    locationList: LocationListDto[];
    saving: boolean = false;
    Location: string;
    tabindex: number = 0;
    prgVillageClusterList: PrgVillageClusterListDto[];
    constructor(
        injector: Injector,
        private _router: Router,
        private _programRegionSpendingServiceProxy: ProgramRegionSpendingServiceProxy,
        private _locationService: LocationServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _PrgVillageClusterService: PrgVillageClusterServiceProxy
    ) {
        super(injector);
        this.getAllReginSpending();
    }
    ngOnInit(): void {
        this.funPanel();

    }
    ngAfterViewInit(): void {
        this.getAllLocation();
    }
    sumOfTotal: number = 0;
    sumOfTotal1: number = 0;
    DivideUnits(res, rec) {
        if (rec.totalUnits > res.length) {
            this.sumOfTotal = 0;
            for (let s of rec.subUnitActivity) {
                this.sumOfTotal += +s.totalUnits;
            }
            if (rec.totalUnits > this.sumOfTotal || this.sumOfTotal == 0) {
                var res1 = new SubListActivityforProgamRegionSpendingDto();
                res.push(res1);
            }
        }
    }

    removeSigleChoice(res, rowIndex, record) {
        if (record.regionId) {
            this.message.confirm(
                '',
                (isConfirmed) => {
                    if (isConfirmed) {
                        this._programRegionSpendingServiceProxy.delete(record.regionId)
                            .subscribe(() => {
                                this.notify.success(this.l('SuccessfullyDeleted'));
                            });
                        res.splice(rowIndex, 1);
                    }
                });
        }
        else {
            res.splice(rowIndex, 1);
        }
    }

    cal(res, record) {
        if (record.totalUnits >= res.totalUnits) {
            this.sumOfTotal1 = 0;
            for (let s of record.subUnitActivity) {
                this.sumOfTotal1 += +s.totalUnits;
            }
            if (record.totalUnits >= this.sumOfTotal1 || this.sumOfTotal1 == 0) {
                res.unitCost = record.unitCost;
                res.totalUnitCost = res.totalUnits * res.unitCost;
                res.communityContribution = (record.communityContribution / record.totalUnits) * res.totalUnits;
                res.grant = (record.grant / record.totalUnits) * res.totalUnits;
                res.otherContribution = (record.otherContribution / record.totalUnits) * res.totalUnits;
            }
            else {
                this.notify.warn("Total Units Greater Than Your Units");
                res.unitCost = "";
                res.totalUnitCost = "";
                res.communityContribution = "";
                res.grant = "";
                res.otherContribution = "";
            }
        }
        else {
            this.notify.warn("Total Units Greater Than Your Units");
            res.unitCost = "";
            res.totalUnitCost = "";
            res.communityContribution = "";
            res.grant = "";
            res.otherContribution = "";
        }
    }
    funComponents() {
        this._router.navigate(['/app/main/ProgramComponent'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funProgramCostEstimation() {
        this._router.navigate(['/app/main/ProgramCostEstimation'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funActivityMaping() {
        this._router.navigate(['/app/main/ActivityMapping'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funRegionSpending() {
        this._router.navigate(['/app/main/RegionSpending']);
    }

    funRequestAAPla() {
        this._router.navigate(['/app/main/RequestAAPla'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    getAllReginSpending() {       
        var ProgrammeId = this._activatedRoute.snapshot.queryParams.ProgrameId;
        abp.ui.setBusy();
        this._programRegionSpendingServiceProxy.getProgramRegionSpendingDeatails(ProgrammeId).subscribe((result) => {
            this.ProgramRegionSpending = result;
            abp.ui.clearBusy();
        })
    }
    isPresent: boolean = false;
    changeLocation(p, event, i, c, ri, res) {
        var tre = event.target.value;
        this.isPresent = false;
        if (tre.includes(",")) {
            var values = tre.split(",");
            for (let s of res.subUnitActivity) {
                if (s.locationID == values[1]) {
                    this.isPresent = true;
                }
            }

            if (this.isPresent != true) {
                p.locationName = values[0];
                p.locationID = values[1];
            }
            else {
                this.notify.error("Already Present");
                event.target.value = undefined;
                p.locationName = undefined;
                p.locationID = undefined;
            }
        }
    }

  

    save() {
        abp.ui.setBusy();
        this.saving = true;
        this._programRegionSpendingServiceProxy.createOrUpdateProgramRegionSpending(this.ProgramRegionSpending)
            .finally(() => this.saving = false).subscribe(res => {
                this.notify.info(res[0]);
                abp.ui.clearBusy();
                this.funRequestAAPla();
            });
    }
    RRcList: any;
    villageClusterList: any;
    villageList: any;
    getAllLocation() {
        var ProgrammeId = this._activatedRoute.snapshot.queryParams.ProgrameId;
        this._PrgVillageClusterService.getAllVillageClusterList(ProgrammeId).subscribe(result => {
            this.prgVillageClusterList = result;

        })       
    }
    funPanel() {
        $('.panel-collapse').on('show.bs.collapse', function () {
            $(this).siblings('.panel-heading').addClass('active');
        });

        $('.panel-collapse').on('hide.bs.collapse', function () {
            $(this).siblings('.panel-heading').removeClass('active');
        });
    }
    funActiveClickPanel(i, c) {
        $("#tab_" + i + "_" + c).addClass("active");
    }
    funChange(val) {
        this.Location = "" + $('#' + val).val();
    }
}