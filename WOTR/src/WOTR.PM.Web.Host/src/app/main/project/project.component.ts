﻿import { Component, Injector, ViewChild, ElementRef } from '@angular/core';
import { } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';


declare var jQuery: any;


@Component({
    selector: 'projectComponent',
    templateUrl: "./project.component.html",
    styleUrls: ['./project.component.less'],
})
export class ProjectComponent extends AppComponentBase {
    constructor(
        injector: Injector,

    ) {
        super(injector);

    }
}