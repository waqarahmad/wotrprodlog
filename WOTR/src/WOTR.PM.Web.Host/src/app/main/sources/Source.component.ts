﻿import { Component, Injector, ViewChild } from '@angular/core';
import { SourceServiceProxy, GetAllSource } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { SourceModalComponent } from './SourceModal.component';

@Component({
    selector: 'SourceComponent',
    templateUrl: "./Source.component.html",
    styleUrls: ['./Source.component.less']

})

export class SourceComponent extends AppComponentBase {
    @ViewChild('SourceModal') myModalView: SourceModalComponent;
    searchText = "";
    Source: GetAllSource[] = [];
    SourceList: GetAllSource[] = []
    timer: any;
    currentPage: number = 1;


    constructor(
        injector: Injector,
        private _SourceAppService: SourceServiceProxy) {
        super(injector);
    }

    ngOnInit() {
        this.getAllSource();
    }

    


    getAllSource(): void {
        abp.ui.setBusy();
        this._SourceAppService.getAllSource(this.searchText).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.length;

            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;

            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.SourceList = result;
            this.Source = result;
            this.SourceList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();

        })

    }

    searchPC(): void {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => { this.getAllSource() }, 1000);
    }

    editActionArea(p) {

       this.myModalView.show(p);
    }

    delete(Source) {
        this.message.confirm(
            this.l('Delete Source', Source.sourceName),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._SourceAppService.delete(Source.id)
                        .subscribe((result) => {
                            this.notify.success(this.l('Successfully Deleted  Record'));
                            this.getAllSource();
                        });
                };

            });
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.Source.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.SourceList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);

    }

}