﻿import { Component, Injector, ViewChild,  EventEmitter, Output } from '@angular/core';
import { SourceServiceProxy, Source } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    selector: 'SourceModal',
    templateUrl: "./SourceModal.component.html",
    styleUrls: ['./SourceModal.component.less']
})

export class SourceModalComponent extends AppComponentBase {
    @ViewChild('SourceModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    source: Source = new Source();
    saving: boolean = false;


    constructor(injector: Injector,
        private _SourceServiceProxy: SourceServiceProxy
    ) {
        super(injector)
    }

    ngOnInit() {


    }

    show(p) {
        this.active = true;
        if (p != undefined) {
            this.source = p;
            this.source.id = p.id;
            $('#lblTitle').text('Edit Source');
        }
        else {
            this.source = new Source();
        }
        this.active = true;
        this.modal.show();

    }



    save(): void {
        this.saving = true;
        if (this.source.sourceName.trim() != "") {
            var r = this._SourceServiceProxy.creatOrUpdateSource(this.source)
                .finally(() => this.saving = false)
                .subscribe((result) => {
                    if (result == "Record alread Present!") {
                        this.notify.warn(this.l(result));
                    }
                    if (result == "Source Update  sucessfully !" || result == "Source Added  sucessfully !") {
                        this.notify.info(this.l(result));
                    }
                    this.close();
                    this.modalSave.emit(null);
                });

        } else {
            this.notify.info("Please Enter Source");
           // this.close();
            this.modalSave.emit(null);}
    }


    close(): void {
        this.modal.hide();
        this.active = false;
    }

}