﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { PrgActionAreaActivityMappingServiceProxy, ListActionSubactionListDto, Village, RequestAAPlasListDto, PrgRequestAAPlasServiceProxy, ProgramInformationListDto, PrgVillageClusterListDto, OverallAnnualActionAreadto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
declare let jsPDF;

@Component({
    selector: 'overAllAnualActioncomponent',
    templateUrl: "./overAllAnualAction.component.html",
    styleUrls: ['./overAllAnualAction.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})

export class OverAllAnualActioncomponent extends AppComponentBase {
    @ViewChild('test') el: ElementRef;

    programd: ProgramInformationListDto = new ProgramInformationListDto();
    program: ProgramInformationListDto[];

    requestList: RequestAAPlasListDto[] = [];
    villageclustor: PrgVillageClusterListDto[];
    pro: any[];
    active: boolean = false;
    list: any = [];
    requestSingleYearList: RequestAAPlasListDto = new RequestAAPlasListDto();
    AddSubActionList: ListActionSubactionListDto;
    emailContent: any;
    saving: boolean = false;
    prgVilages: Village[] = [];
    Record: ProgramInformationListDto[] = [];
    Year: any = [];
    overallactionareawise: OverallAnnualActionAreadto[];
    costyear: string;

    constructor(
        injector: Injector,
        private _fileDownloadService: FileDownloadService,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy,
        private _practionareaactivitymapping: PrgActionAreaActivityMappingServiceProxy)
    {
        super(injector);
    }

    ngOnInit() {
        this.getcostestimationyear();
    }

    getcostestimationyear() {
        this._PrgRequestAAPlasService.overallPrgCostEstimationYears().subscribe(result => {
            this.Year = result;
        });
    }

    getAllProgrambyid(year) {
        this.costyear = year;
        abp.ui.setBusy();
        this._practionareaactivitymapping.overallAnnualActionAreawisereport(year).subscribe(result => {
            this.overallactionareawise = result;
            abp.ui.clearBusy();
        });
    }

    exportToExcel(): void {
        this._practionareaactivitymapping.overallAnnualActionCreateExcelDoc("OverAll Annual Action Plan", this.costyear).subscribe(result => {
            this._fileDownloadService.downloadTempFile1(result);
        })
    }
}
