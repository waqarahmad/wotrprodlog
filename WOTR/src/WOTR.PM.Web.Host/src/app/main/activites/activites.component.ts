﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { ActivityServiceProxy, ActivitiyListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ActivitesAddModalComponent } from './activitesAddModal.component';

declare var jQuery: any;


@Component({
    selector: 'activitescomponent',
    templateUrl: "./activites.component.html",
    styleUrls: ['./activites.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ActivitesComponent extends AppComponentBase {
    @ViewChild('ActivitesAddModal') myModalView: ActivitesAddModalComponent;
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    activityList: ActivitiyListDto[];
    activity: ActivitiyListDto[];
    constructor(
        injector: Injector,
        private _activityService: ActivityServiceProxy

    ) {
        super(injector);
    }
    getAllActivity() {
        abp.ui.setBusy();
        var input = (document.getElementById("txtActivity") as HTMLInputElement).value;
        this._activityService.getAllActivities(input).subscribe(result => {
            this.primengDatatableHelper.records = result;
            this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.activityList = result;
            this.activity = result;
            this.activityList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        })

    }

    getAllActivitysearch() {
        abp.ui.setBusy();
        var input = (document.getElementById("txtActivity") as HTMLInputElement).value;
        this._activityService.getAllActivities(input).subscribe(result => {
            this.primengDatatableHelper.records = result;
            //this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.activityList = result;
            this.activity = result;
            this.activityList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        })

    }
    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.activity.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.activityList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);

    }
   
    editActivity(Activity) {
        this.myModalView.show(Activity);
    }
    deleteActivity(del) {
        this.message.confirm(
            this.l('ActivityDeleteWarningMessage', del.name),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._activityService.deleteActivity(del.id)
                        .subscribe((result) => {
                            if (result == "Record Exist") {
                                this.notify.success(this.l('This record is already in used'));
                                this.getAllActivity();
                            }
                            else {
                                this.notify.success(this.l('Successfully Delete Activity'));
                                this.getAllActivity();
                            }
                        });
                }
            })
    }
}