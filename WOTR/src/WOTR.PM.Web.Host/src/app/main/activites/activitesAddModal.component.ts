﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { ActivityServiceProxy, ActivitiyListDto, UnitOfMeasuresServiceProxy, UnitofMeasureDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';

declare var jQuery: any;


@Component({
    selector: 'ActivitesAddModal',
    templateUrl: "./activitesAddModal.component.html",
    styleUrls: ['./activitesAddModal.component.less'],
})
export class ActivitesAddModalComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('ActivitesAddModal') modal: ModalDirective;
    active: boolean = false;
    saving: boolean = false;
    activity: ActivitiyListDto;
    unitofMeasureDto : UnitofMeasureDto[];

    @ViewChild('Modal') activityForm: NgForm;//yo

    constructor(
        injector: Injector,
         private _activityService: ActivityServiceProxy,
         private _unitOfMeasuresServiceProxy: UnitOfMeasuresServiceProxy,
    ) {
        super(injector);

    }
    ngAfterViewInit(): void {
        this.getUnitofMeasures();       
    }
    show(a): void {
        this.active = true;
        if (a != undefined) {
            $('#lblTitle').text('Edit Activity');
            this.activity = a;
        }
        else {
            $('#lblTitle').text('Add Activity');
            this.activity = new ActivitiyListDto();
        }
        this.modal.show();
    }
    save() {
        this.saving = true;
        this._activityService.createActivity(this.activity)
            .finally(() => this.saving = false)
            .subscribe(result => {
                if (result[0] == "Record is already is Present!") {
                    this.notify.warn(this.l(result[0]));
                }
                if (result[0] == "Activity Updated Successfully" || result[0] == "Activity Added Successfully") {
                    this.notify.info(this.l(result[0]));
                }
                this.close();
                this.modalSave.emit(null);
            })
    }
    getUnitofMeasures() {
        this._unitOfMeasuresServiceProxy.getAllUnitOfMeasure().subscribe(result => {
            this.unitofMeasureDto = result;
        })
    }
    close(): void {
        this.modal.hide();
        this.active = false;      
    }
}