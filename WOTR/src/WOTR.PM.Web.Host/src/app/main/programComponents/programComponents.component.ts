﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import {
    ComponentServiceProxy, ComponentListDto, ImpactIndicatorServiceProxy, ActivitiyListDto, ActivityServiceProxy,
    ImpactIndicatorsListDto, ProgramCompnentsServiceProxy, ProgramCompnentsListDto, GetComponentActivityMappingListDto,
    GetAllComponentandActivityMappingDtoList,ProgramComponentMappingListDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';

import { MysortPipe } from '../../shared/common/mysort.pipe';//yo modified

declare var jQuery: any;


@Component({
    selector: 'programComponents',
    templateUrl: "./programComponents.component.html",
    styleUrls: ['./programComponents.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ProgramComponentsComponent extends AppComponentBase {
    @ViewChild('table') table;
    saving: boolean = false;
    impacatindicatorList: ImpactIndicatorsListDto[];
    componentWithActivityList: GetComponentActivityMappingListDto[];
    componentList: ComponentListDto[];
    activityDetails: ActivitiyListDto[];
    addActivityToProgrameComponent: GetAllComponentandActivityMappingDtoList = new GetAllComponentandActivityMappingDtoList();
    programeComponent: ProgramComponentMappingListDto = new ProgramComponentMappingListDto();
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    setting: any;
    selectedItems: any;

    constructor(
        injector: Injector,
        private _router: Router,
        private _componentService: ComponentServiceProxy,
        private _impactIndicatorService: ImpactIndicatorServiceProxy,
        private _ProgramCompnentsService: ProgramCompnentsServiceProxy,
        private _activityService: ActivityServiceProxy,
        private _activatedRoute: ActivatedRoute,)
    {
        super(injector);
        
    }

    ngOnInit() {   
        this.GetComponentAndActivityForPrograme();       
        this.GetAllComponents();
        this.getAllActivity(''); 
        this.setting = {
            singleSelection: false,
            text: "Select Component",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        };      
    }
   
    funClick1() {
        this.saving = true;
        this.programeComponent.programID = this._activatedRoute.snapshot.queryParams.ProgrameId;;
        //this.programeComponent.componentID = this.componentAdd;
        this.programeComponent.componentID = this.ComponentId;

        if (this.programeComponent.componentID.length == 0) {
            this.notify.info("Please Select Component");

            return true;

        }
       
        this._ProgramCompnentsService.addComponetToProgramComponetMapping(this.programeComponent)
            .finally(() => this.saving = false).subscribe(result => {
                this.notify.info("Component Add Successfully");
                this.GetComponentAndActivityForPrograme();
            });
    }
    itemList: any;
    GetAllComponents(): void {
        this.itemList = [];
        this._componentService.getAllComponents().subscribe(result => {
            this.componentList = result.sort();
            for (let a of result) {
                this.itemList.push({
                    id: a.id,
                    itemName: a.name
                })
            }
        })
    }


    ComponentId: number[] = [];
    onItemSelect(item: any) {
        this.ComponentId.push(item.id);
    }
    OnItemDeSelect(item: any) {
        var index = this.ComponentId.indexOf(item.id, 0);
        if (index > -1) {
            this.ComponentId.splice(index, 1);
        }
    }
    onSelectAll(items: any) {
        for (let obj of items) {
            this.ComponentId.push(obj.id);
        }
    }
    onDeSelectAll(items: any) {
        this.ComponentId = [];
    }

    //GetAllComponents(): void {
    //    this._componentService.getAllComponents().subscribe(result => {
    //        this.componentList = result;       
    //    })
    //}
   
    GetComponentAndActivityForPrograme() {
        var ProgrammeId = this._activatedRoute.snapshot.queryParams.ProgrameId;
        this._ProgramCompnentsService.getComponentandActivityForProgram(ProgrammeId).subscribe(result => {
            this.componentWithActivityList = result;
            this.primengDatatableHelper.records = result;
            //this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.componentWithActivityList = result;
            this.componentWithActivityList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);

        })
    }    

    GetAllImpactindicator(): void {
        this._impactIndicatorService.getAll().subscribe((result) => {
            this.impacatindicatorList = result;
        });
    }
   
    componentAdd: any[] = [];
    ischecked(event,com): void {

        if (event.target.checked) {
            this.componentAdd.push(com);
        }
        else {
            var index = this.componentAdd.indexOf(com);
            if (index > -1) {
                this.componentAdd.splice(index, 1);
            }           
        }
    }
    getAllActivity(input) {
        this._activityService.getAllActivities("").subscribe(result => {
            this.activityDetails = result;
        })
    }
    funProgramInformation() {
        this._router.navigate(['/app/main/CreateProgramInformation']);
    }

    funProgramCluster() {
        this._router.navigate(['/app/main/ProgramCluster'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funProgramInformation1() {
        this._router.navigate(['/app/main/CreateProgramInformation'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }
    funProgramCostEstimation() {
        this._router.navigate(['/app/main/ProgramCostEstimation'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }   

    funActivityMaping() {
        this._router.navigate(['/app/main/ActivityMapping']);
    }

    on(ComponentId) {
        $('#id' + ComponentId).click(function () {
            $('#span' + ComponentId).removeClass("fa fa-angle-down");
            $('#span' + ComponentId).addClass("fa fa-angle-right");
        });
        this.addActivityToProgrameComponent.componentID = ComponentId;
        this.addActivityToProgrameComponent.activityID = undefined;
    }

    AddActivity() {
        this.saving = true;
        
        if (this.addActivityToProgrameComponent.activityID == undefined) {
            this.notify.info("Please Select Activity");

            return true;
        }
        this.addActivityToProgrameComponent.programID = this._activatedRoute.snapshot.queryParams.ProgrameId;;
        this._ProgramCompnentsService.addActivityToProgrameComponentActivityMapping(this.addActivityToProgrameComponent)
            .finally(() => this.saving = false).subscribe(res => {
                this.notify.info("Activity Add Successfully");
                this.GetComponentAndActivityForPrograme();
            })

      
    }

    close() {
        this._router.navigate(['/app/main/ProgramCluster'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }
    deleteProgramComponent(ComponentId) {
        var ProgrameId = this._activatedRoute.snapshot.queryParams.ProgrameId;
        this.message.confirm(
            this.l('Component Delete Warning Message', ''),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._ProgramCompnentsService.deleteProgramComponent(ComponentId, ProgrameId)
                        .subscribe(() => {
                            this.notify.success(this.l('Successfully Deleted Coponent Record'));       
                            this.GetComponentAndActivityForPrograme();
                        });
                }
            })
       

    }

    deleteProgramComponentActivity(ComponentId, ActivityID) {
        var ProgrameId = this._activatedRoute.snapshot.queryParams.ProgrameId;
        this.message.confirm(
            this.l('Component Activity Delete Warning Message', ''),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._ProgramCompnentsService.deleteProgramComponentActivity(ComponentId, ProgrameId, ActivityID)
                        .subscribe((result) => {
                            this.notify.success(this.l('Successfully Deleted Coponent Record'));
                            this.GetComponentAndActivityForPrograme();
                        });
                }
            })


    }

    //deleteProgramComponentActivity(del) {
    //    debugger
    //    this.message.confirm(
    //        this.l('ActionAreaDeleteWarningMessage', del.name),
    //        (isConfirmed) => {
    //            if (isConfirmed) {
    //                this._ProgramCompnentsService.deleteProgramActivity(del.id)
    //                    .subscribe((result) => {
    //                        this.notify.success(this.l('Successfully Deleted Coponent Record'));
    //                        this.GetComponentAndActivityForPrograme();
    //                    });
    //            }
    //        })
    //}


    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.componentWithActivityList = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
}
