﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { ActionAreasServiceProxy, ActionAreasListDto, State, LocationServiceProxy, GetAllCategory, CategoryServiceProxy, ImpactIndicatorCategory } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
declare var jQuery: any;

@Component({
    selector: 'CategoryModal',
    templateUrl: "./IICategory.modal.component.html",
    styleUrls: ['./IICategory.modal.component.less']
})

export class CategoryModalComponent extends AppComponentBase {
    @ViewChild('CategoryModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    IICategory: ImpactIndicatorCategory = new ImpactIndicatorCategory();
    saving: boolean = false;


    constructor(injector: Injector,
        private _CategoryServiceProxy: CategoryServiceProxy
    ) {
        super(injector)
    }

    ngOnInit() {


    }

    show(p) {
        this.active = true;
        if (p != undefined) {
            this.IICategory = p;
            this.IICategory.id = p.id;
        }
        else {
            this.IICategory = new ImpactIndicatorCategory();
        }
        this.active = true;
        this.modal.show();

    }

    
    save(): void {
        this.saving = true;
        if (this.IICategory.category.trim() != "") {
            var r = this._CategoryServiceProxy.createEditImapactIndiactorCategory(this.IICategory)
                .finally(() => this.saving = false)
                .subscribe((result) => {
                    if (result == "Record Already Present!") {
                        this.notify.warn(this.l(result));
                    }
                    if (result == "Category Update  sucessfully !" || result == "Category Added  sucessfully !") {
                        this.notify.info(this.l(result));
                    }
                    this.close();
                    this.modalSave.emit(null);
                });
        } else {
            this.notify.warn("Please Enter Category");
        }
    }


    close(): void {
        this.modal.hide();
        this.active = false;
    }

    

}