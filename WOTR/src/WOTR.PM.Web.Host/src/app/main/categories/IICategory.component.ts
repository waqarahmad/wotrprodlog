﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { RoleServiceProxy, CategoryServiceProxy, ImpactIndicatorCategory, GetAllCategory } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { CategoryModalComponent } from './IICategory.modal.component';


@Component({
    selector: 'Category',
    templateUrl: "./IICategory.component.html",
    styleUrls: ['./IICategory.component.less']

})

export class CategoryComponent extends AppComponentBase  {
    @ViewChild('CategoryModal') myModalView: CategoryModalComponent;
    searchText: string = "";
    //category: ImpactIndicatorCategory[] = [];
    //categoryList: ImpactIndicatorCategory[] = [];
      category: GetAllCategory[] = [];
   categoryList: GetAllCategory[] = [];

    timer: any;
    currentPage: number = 1;



    constructor(injector: Injector,
        private _CategoryAppService: CategoryServiceProxy
    ) {
        super(injector)
    }

    ngOnInit() {
        this.getAllCategory();
    }

    getallCategorysearch(): void {
        abp.ui.setBusy();
        this._CategoryAppService.getAllImapactIndiactorCategory(this.searchText).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.categoryList = result;
            this.category = result;
            this.categoryList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        });
    }


    getAllCategory(): void {
        abp.ui.setBusy();
        this._CategoryAppService.getAllImapactIndiactorCategory(this.searchText).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.categoryList = result;
            this.category = result;
            this.categoryList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        })
    }

    searchPC(): void {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => { this.getallCategorysearch() }, 1000);
    }

    editActionArea(p) {
        this.myModalView.show(p);
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.category.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.categoryList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
    }


    delete(p) {
        this.message.confirm(
            this.l('Delete  ' + p.category),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._CategoryAppService.delete(p.id)
                        .subscribe((result) => {
                            this.notify.success(this.l('Successfully Deleted  Record'));
                            this.getAllCategory();
                        });
                };

            });
    }





}


