﻿import { Component, Injector, ViewChild, ElementRef } from '@angular/core';
import { } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';


declare var jQuery: any;


@Component({
    selector: 'peopleComponent',
    templateUrl: "./people.component.html",
    styleUrls: ['./people.component.less'],
})
export class PeopleComponent extends AppComponentBase {

    constructor(
        injector: Injector,
      
    ) {
        super(injector);
        
    }

}