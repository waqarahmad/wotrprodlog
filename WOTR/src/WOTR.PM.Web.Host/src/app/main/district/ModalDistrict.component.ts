﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { ActionAreasServiceProxy, ActionAreasListDto, State, LocationServiceProxy,District } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
declare var jQuery: any;
@Component({
    selector: 'ModalDistrict',
    templateUrl: "./ModalDistrict.component.html",
    styleUrls: ['./ModalDistrict.component.less'],
})
export class ModalDistrictComponent extends AppComponentBase {
    @ViewChild('ModalDistrict') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    passwordComplexityInfo = '';
    district: District = new District();
    saving: boolean = false;
    search: ActionAreasListDto[] = [];
    StateList: State[];
    @ViewChild('projectForm') eForm: NgForm;

    constructor(
        injector: Injector,
        private _actionareaService: ActionAreasServiceProxy,
        private _locationService: LocationServiceProxy,
)
    {
        super(injector);        
    }
    ngAfterViewInit() {
        this.getAllstate();
    }
    show(p): void {
        this.active = true;
        if (p != undefined) {
            this.district = p;
            $('#lblTitle').text('Edit District');


        }
        else {
            this.district = new District();
        }
        this.modal.show();
    }

    close(): void {
        this.modal.hide();
        this.active = false;       
    }
    //...save the Record in ActionArea table.
    save(): void {
         this.saving = true;
        var r = this._locationService.creatOrUpdateDistrict(this.district)
            .finally(() => this.saving = false)
            .subscribe((result) => {
                if (result[0] =="Record is already Present") {
                    this.notify.warn(this.l(result[0]));
                }
                if (result[0] == "ActionArea Update  sucessfully !" || result[0] == "ActionArea Added  sucessfully !") {
                    this.notify.info(this.l(result[0]));
                }
                this.close();
                this.modalSave.emit(null);
            });
    }

    getAllstate() {
        this._locationService.getAllState().subscribe(result => {
            this.StateList = result;
        });
    } 
}