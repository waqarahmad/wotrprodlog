﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { RoleServiceProxy, State, LocationServiceProxy, District} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDistrictComponent } from './ModalDistrict.component';
//import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    selector: 'districtcomponent',
    templateUrl: "./district.component.html",
    styleUrls: ['./district.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class DistrictComponent extends AppComponentBase {    
    @ViewChild('ModalDistrict') myModalView: ModalDistrictComponent;
    selectedPermission = '';
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: State[] = [];
    district: District[] = [];
    districtlist: District[] = [];
    active: boolean = false;
    list: any = [];
    searchText;
    timer: any;
    constructor(
        injector: Injector,
        private _locationService: LocationServiceProxy,

        private _roleService: RoleServiceProxy)
    {
        super(injector);
    }

    ngOnInit() {
        this.getallstate(); 
        this.getAllstaterecordssearch();
    }    


    searchPC(): void {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => { this.getAllstaterecordssearch() }, 1000);
    }

    getAllstaterecordssearch(): void {  
        var input = (document.getElementById("txtActionArea") as HTMLInputElement).value;
        abp.ui.setBusy();
        this._locationService.getAllDistricts(input).subscribe((result) => {       
            this.primengDatatableHelper.totalRecordsCount = result.length;
           
            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;
         
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.districtlist = result;
            this.district = result;
            this.districtlist = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        });
    }


    getallstate(): void {
        var input = (document.getElementById("txtActionArea") as HTMLInputElement).value;
        abp.ui.setBusy();
        this._locationService.getAllDistricts(input).subscribe((result) => {
          
            this.primengDatatableHelper.records = result;
            this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.districtlist = result;
            this.district = result;
            this.districtlist = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        });
    }
    editActionArea(p) {        
        this.myModalView.show(p);
    }
    
        count: number = -1;
        finalUnits: number = 0;
    sumOfTotal: number = 0;
    sumOfTotal1: number = 0;
    DivideUnits(res, rec) {
    }
    
    deletedistrict(del) {
        this.message.confirm(
            this.l('DistrictDeleteWarning', del.districtName),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._locationService.deleteDistrict(del.id)
                        .subscribe((result) => {
                            if (result == "Record Exist") {
                                this.notify.success(this.l('This record is already in used'));
                                this.getallstate();
                            }
                            else {
                                this.notify.success(this.l('Successfully Deleted Record'));
                                this.getallstate();
                            }
                        });
                }
            })
    }
    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.district.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.districtlist = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
   
}