﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation, NgZone } from '@angular/core';
import {
    PrgRequestAAPlasServiceProxy, RequestAAPlasListDto, EmailServiceProxy, Village,
    ListActionSubactionListDto, Notificationdto, ProjectWiseYearlyclustorinfo
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';
import { ActivitesAddModalsComponent } from '../approveAAPla/approveAAPlaActivityInfo.component';


@Component({
    selector: 'requestAAPla',
    templateUrl: "./requestAAPla.component.html",
    styleUrls: ['./requestAAPla.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class requestAAPlaComponent extends AppComponentBase {

    @ViewChild('ActivitesAddModals') ActivitesAddModals: ActivitesAddModalsComponent;

    requestList: RequestAAPlasListDto[] = [];
    requestListt: RequestAAPlasListDto[] = [];

    requestSingleYearList: RequestAAPlasListDto = new RequestAAPlasListDto();
    AddSubActionList: ListActionSubactionListDto;
    emailContent: any;
    saving: boolean = false;
    programName: string;
    prgVilages: Village[] = [];
    Getcurdate = new Date();
    AssignCurDate: any;
    ProjectInfo: any = [];
    projectName: string;
    projectstart: string;
    projectEnd: string;
    projectstatus: string;
    programeId: number = 0;
    costestimationyeardto: any = [];
    Year: string;
    simplebarcharts: any;
    piechartdata: any;
    notification: Notificationdto[];
    yearWiseclustorInfo: ProjectWiseYearlyclustorinfo[];
    constructor(
        injector: Injector,
        private zone: NgZone,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy,
        private _emailServiceProxy: EmailServiceProxy

    ) {
        super(injector);

    }
    ngOnInit(): void {
        this._activatedRoute.queryParams.subscribe(params => {
            this.GetAllRequests();
            this.programName = this._activatedRoute.snapshot.queryParams.ProgrameName;
            this.AssignCurDate = this.Getcurdate;
            this.programeId = +this._activatedRoute.snapshot.queryParams.ProgrameId;
            this.sumarryForProjectWise();
           // abp.session["programId"] = this._activatedRoute.snapshot.queryParams.ProgrameId
            this.appSession.user.programId = this._activatedRoute.snapshot.queryParams.ProgrameId
        });
    }

    sumarryForProjectWise() {
        abp.ui.setBusy();
        this._PrgRequestAAPlasService.sumarryForProjectWise(this.programeId).subscribe(result => {
            this.ProjectInfo = [];
            result.projectWiseDashboardDatas[0].projectWiseInfoDTOs.forEach(x => {
                this.ProjectInfo.push(x.projectManager)
            })
            this.projectName = result.projectWiseDashboardDatas[0].projectWiseInfoDTOs.length == 0 ? "" : result.projectWiseDashboardDatas[0].projectWiseInfoDTOs[0].projectName;
            this.projectstart = result.projectWiseDashboardDatas[0].projectWiseInfoDTOs.length == 0 ? "" : (result.projectWiseDashboardDatas[0].projectWiseInfoDTOs[0].projectStart).format('MM/DD/YYYY');
            this.projectEnd = result.projectWiseDashboardDatas[0].projectWiseInfoDTOs.length == 0 ? "" : (result.projectWiseDashboardDatas[0].projectWiseInfoDTOs[0].projectEnd).format('MM/DD/YYYY');
            this.projectstatus = result.projectWiseDashboardDatas[0].projectWiseInfoDTOs.length == 0 ? "" : result.projectWiseDashboardDatas[0].projectWiseInfoDTOs[0].projectStatus;


            this._PrgRequestAAPlasService.costEstimationYear(this.programeId).subscribe(results => {
                this.costestimationyeardto = results;
            });
           // abp.ui.clearBusy();
        });
    }

    clickyear(year) {
        this.Year = year;
        this._PrgRequestAAPlasService.yearWisecomponent(this.programeId, year).subscribe(result => {
            this.simplebarcharts = result.programManagerProgramYearwiseStateWiseBudgetBarChart;
            this.drawCharts();
            this.piechartdata = result.statuswisecounts;
            this.piecharts()

            this.yearWiseclustorInfo = result.projectWiseYearlyclustorinfo;
            for (var i = 0; i < this.yearWiseclustorInfo.length; i++) {
                this.yearWiseclustorInfo[i].tergetUnit = parseInt(this.yearWiseclustorInfo[i].tergetUnit).toString();
            }
            this.notification = result.notificationdtos;
        });
    }

    drawCharts() {        this.zone.runOutsideAngular(() => {            Promise.all([                import("@amcharts/amcharts4/core"),                import("@amcharts/amcharts4/charts"),                import("@amcharts/amcharts4/themes/animated"),            ])                .then(modules => {                    const am4core = modules[0];                    const am4charts = modules[1];                    const am4themes_animated = modules[2].default;                    am4core.options.commercialLicense = true;                    //am4core.useTheme(am4themes_animated);                    am4core.useTheme(am4themes_animated);

                    // Create chart instance
                    let chart = am4core.create("chartdiv", am4charts.XYChart);


                    // Add data
                    //    this.simplebarcharts.forEach(
                    //        xx => {

                    //            chart.data.push({
                    //                "year": xx.labels[0],
                    //                "funderContribution": xx.datasets[0].data[0],
                    //                "communityContribution": xx.datasets[1].data[0],
                    //                "otherContribution": xx.datasets[2].data[0],

                    //            })
                    //        })

                    //    // Create axes
                    //    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                    //    categoryAxis.dataFields.category = "year";
                    //    categoryAxis.renderer.grid.template.location = 0;
                    //    let label = categoryAxis.renderer.labels.template;
                    //    label.wrap = true;
                    //    label.maxWidth = 120;

                    //    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                    //    valueAxis.min = 0;

                    //    // Create series
                    //    function createSeries(field, name) {

                    //        // Set up series
                    //        var series = chart.series.push(new am4charts.ColumnSeries());
                    //        series.name = name;
                    //        series.dataFields.valueY = field;
                    //        series.dataFields.categoryX = "year";
                    //        series.sequencedInterpolation = true;
                    //        if (name == "FunderContribution") {
                    //            series.columns.template.fill = am4core.color("#5D78FF");
                    //            series.stroke = am4core.color("#5D78FF");
                    //        }
                    //        if (name == "CommunityContribution") {
                    //            series.columns.template.fill = am4core.color("#0ABB87");
                    //            series.stroke = am4core.color("#0ABB87");
                    //        }
                    //        if (name == "OtherContribution") {
                    //            series.columns.template.fill = am4core.color("#FD397A");
                    //            series.stroke = am4core.color("#FD397A");
                    //        }
                    //        // Make it stacked
                    //        series.stacked = true;

                    //        // Configure columns
                    //        series.columns.template.width = am4core.percent(60);
                    //        series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

                    //        // Add label
                    //        var labelBullet = series.bullets.push(new am4charts.LabelBullet());
                    //        labelBullet.label.text = "{valueY}";
                    //        labelBullet.locationY = 0.5;

                    //        return series;
                    //    }

                    //    createSeries("funderContribution", "FunderContribution");
                    //    createSeries("communityContribution", "CommunityContribution");                    //    createSeries("otherContribution", "OtherContribution");
                    //    // Legend
                    //    chart.legend = new am4charts.Legend();                    //})                    //.catch(e => {                    //    console.error("Error when creating chart", e);                    //});                    this.simplebarcharts.forEach(
                        xx => {

                            chart.data.push({
                                "year": xx.labels[0],
                                "funderContribution": xx.datasets[0].data[0],
                                "communityContribution": xx.datasets[1].data[0],
                                "otherContribution": xx.datasets[2].data[0],

                            })
                        })


                    // Create axes
                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

                    categoryAxis.dataFields.category = "year";
                    categoryAxis.title.text = "Component";
                    categoryAxis.renderer.grid.template.location = 0;
                    categoryAxis.renderer.minGridDistance = 20;
                    categoryAxis.renderer.cellStartLocation = 0.1;
                    categoryAxis.renderer.cellEndLocation = 0.9;

                    categoryAxis.renderer.labels.template

                        .cursorOverStyle = [
                            {
                                "property": "cursor",
                                "value": "pointer"
                            }
                        ];
                    let label = categoryAxis.renderer.labels.template;
                    label.wrap = true;
                    label.maxWidth = 120;

                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                    valueAxis.min = 0;
                    valueAxis.title.text = " ";
                    //categoryAxis.renderer.labels.template.events.on("hit", function (ev) {
                    //    debugger
                    //    //alert(ev.event.target);
                    //    this.AssignReportRRCModals.show(this.projetName, ev.target);
                    //}, this);

                    // Create series
                    function createSeries(field, name) {
                        var series = chart.series.push(new am4charts.ColumnSeries());
                        series.dataFields.valueY = field;
                        series.dataFields.categoryX = "year";
                        series.name = name;
                        series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
                        //series.stacked = stacked;
                        series.columns.template.width = am4core.percent(95);
                        if (name == "FunderContribution") {
                            series.columns.template.fill = am4core.color("#5D78FF");
                            series.stroke = am4core.color("#5D78FF");
                        }
                        if (name == "CommunityContribution") {
                            series.columns.template.fill = am4core.color("#0ABB87");
                            series.stroke = am4core.color("#0ABB87");
                        }
                        if (name == "OtherContribution") {
                            series.columns.template.fill = am4core.color("#FD397A");
                            series.stroke = am4core.color("#FD397A");
                        }
                    }

                    createSeries("funderContribution", "FunderContribution");
                    createSeries("communityContribution", "CommunityContribution");                    createSeries("otherContribution", "OtherContribution");                })                .catch(e => {                    console.error("Error when creating chart", e);                });        });    }

    piecharts() {
        this.zone.runOutsideAngular(() => {            Promise.all([                import("@amcharts/amcharts4/core"),                import("@amcharts/amcharts4/charts"),                import("@amcharts/amcharts4/themes/animated"),            ])                .then(modules => {                    const am4core = modules[0];                    const am4charts = modules[1];                    const am4themes_animated = modules[2].default;                    am4core.options.commercialLicense = true;                    //am4core.useTheme(am4themes_animated);                    am4core.useTheme(am4themes_animated);
                    // Themes end

                    // Create chart instance
                    var piechart = am4core.create("chartdivs", am4charts.PieChart);

                    // Add and configure Series
                    var pieSeries = piechart.series.push(new am4charts.PieSeries());
                    pieSeries.slices.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;

                    pieSeries.dataFields.value = "statuscount";
                    pieSeries.dataFields.category = "projectStatus";

                    // Let's cut a hole in our Pie chart the size of 30% the radius
                    piechart.innerRadius = am4core.percent(30);

                    // Put a thick white border around each Slice
                    pieSeries.slices.template.stroke = am4core.color("#fff");
                    pieSeries.slices.template.strokeWidth = 2;
                    pieSeries.slices.template.strokeOpacity = 1;


                    pieSeries.alignLabels = true;
                    pieSeries.labels.template.bent = true;
                    pieSeries.labels.template.radius = 3;
                    pieSeries.labels.template.padding(0, 0, 0, 0);
                    pieSeries.labels.template
                        // change the cursor on hover to make it apparent the object can be interacted with
                        .cursorOverStyle = [
                            {
                                "property": "cursor",
                                "value": "pointer"
                            }
                        ];
                    pieSeries.labels.template.events.on("hit", function (ev) {

                        //alert(ev.event.target);
                        this.ActivitesAddModals.show(this.programeId, this.Year, ev.target.currentText);
                    }, this);
                    pieSeries.ticks.template.disabled = false;

                    // Create a base filter effect (as if it's not there) for the hover to return to
                    var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
                    shadow.opacity = 0;

                    // Create hover state
                    var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

                    // Slightly shift the shadow and make it more prominent on hover
                    var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
                    hoverShadow.opacity = 0.7;
                    hoverShadow.blur = 5;

                    // Add a legend
                    piechart.legend = new am4charts.Legend();
                    this.piechartdata.forEach(
                        xx => {
                            piechart.data.push({
                                "projectStatus": xx.projectStatus,
                                "statuscount": xx.statuscount,
                            })
                        });                })                .catch(e => {                    console.error("Error when creating chart", e);                });        });
    }

    rowClass(rowData) {
        return rowData.activityLevel == 'VillageLevel' ? '' : 'noExpander';
    }

    removeSigleChoice(req, res) {
        req.subActionSubActionActivity.splice(res, 1);
    }

    count: number = -1;
    finalUnits: number = 0;
    funClick(req, res) {
        this.count = -1;
        this.finalUnits = 0;
        for (let su of req.subActionSubActionActivity) {
            if (req.prgActionAreaActivityMappingID == su.prgActionAreaActivityMappingID && su.actionAreaName == undefined) {
                this.count++;
                this.finalUnits += +su.totalUnits;
            }
        }
        if (this.finalUnits  > this.count) {
            var total = +req.totalUnits;
            if (req.previousUnits) {
                total = total + +req.previousUnits
            }
            if (total > this.finalUnits || this.finalUnits == 0) {
                var x = new ListActionSubactionListDto();
                x.prgActionAreaActivityMappingID = req.prgActionAreaActivityMappingID;
                x.requestStatus = req.requestStatus;
                x.unitOfMeasuresID = req.unitOfMeasuresID;
                x.quarter1PhysicalUnits = 0;
                x.quarter1FinancialRs = 0;
                x.quarter2PhysicalUnits = 0;
                x.quarter2FinancialRs = 0;
                x.quarter3PhysicalUnits = 0;
                x.quarter3FinancialRs = 0;
                x.quarter4PhysicalUnits = 0;
                x.quarter4FinancialRs = 0;
                req.subActionSubActionActivity.push(x);
            }
        }
    }

    calQuarterUnits(record, res) {
        var x = +record.quarter1PhysicalUnits + +record.quarter2PhysicalUnits + +record.quarter3PhysicalUnits + +record.quarter4PhysicalUnits;        
        var total = +record.totalUnits;
        if (record.previousUnits) {
            total = total + +record.previousUnits
        }
            if(total >= x) {
            record.quarter1FinancialRs = record.quarter1PhysicalUnits * record.unitCost;
            record.quarter2FinancialRs = record.quarter2PhysicalUnits * record.unitCost;
            record.quarter3FinancialRs = record.quarter3PhysicalUnits * record.unitCost;
            record.quarter4FinancialRs = record.quarter4PhysicalUnits * record.unitCost;
        }
        else {
            alert("Greater Than totalUnits");
        }
    }

    cal(record, res) {
        for (let a of res.actionSubactionCost) {
            if (record.prgActionAreaActivityMappingID == a.prgActionAreaActivityMappingID && a.actionAreaName != undefined) {
                record.unitCost = a.unitCost;
                record.totalUnitCost = record.unitCost * record.totalUnits;
                record.communityContribution = (a.communityContribution / a.totalUnits) * record.totalUnits;
                record.funderContribution = (a.funderContribution / a.totalUnits) * record.totalUnits;
                record.otherContribution = (a.otherContribution / a.totalUnits) * record.totalUnits;
            }
        }
    }

    getAllVillagsPrgClusterWise(res) {
        var ProgrammeId = res.programID;
        this._PrgRequestAAPlasService.getAllVillagesClusterWise(ProgrammeId, res.locationID)
            .subscribe(result => {
                this.prgVilages = result;
            });
    }


    GetAllRequests() {
        abp.ui.setBusy();
        var ProgrammeId = this._activatedRoute.snapshot.queryParams.ProgrameId;
        if (this.isGranted('Pages.RequestAAPlaSend')) {
            this._PrgRequestAAPlasService.getRequestApplaDeatils(ProgrammeId).subscribe(result => {
                this.requestList = result;
               abp.ui.clearBusy();
            });
            this._PrgRequestAAPlasService.getRequestApplaDeatilsForActionArea(ProgrammeId).subscribe(result => {
                this.requestListt = result;
              //  abp.ui.clearBusy();
            });
        }
        else {
            this._PrgRequestAAPlasService.getReQuestApplaForProjectManager(ProgrammeId).subscribe(result => {
                this.requestList = result;
               abp.ui.clearBusy();
            });
        }
    }

    AddManagerId(res, event) {
        var tre = event.target.value;
        if (event.target.checked) {
            if (tre.includes(",")) {
                var values = tre.split(",");
                res.managerID = values[0];
                res.managerName = values[1];
                for (let co of res.costEstimationYear) {
                    co.managerID = values[0];
                }
            }
        }
        else {
            res.managerID = null;
            res.managerName = "Select Manager";
        }

    }

    RequestVillageIdNull: boolean = false;

    save(res, action, req) {
        this.RequestVillageIdNull = true;
        for (let b of action.actionSubactionCost) {
            b.checklistID = 1;
            if (req == "RequestNotSend") {
                for (let sub of b.subActionSubActionActivity) {
                    if (sub.villageID == undefined) {
                        this.RequestVillageIdNull = false;
                        this.notify.warn("village is not selected");
                    }
                }
                b.requestStatus = "RequestSend";
            }
            if (req == "RequestSend") {
                b.requestStatus = "RequestNotSend";
                if (res.managerID == 0) {
                    this.RequestVillageIdNull = false;
                    this.notify.warn("Please Select Project Manager");
                }
            }
        }

        if (this.RequestVillageIdNull) {
            this.message.confirm(
                this.l('RequestSendFor', action.costEstimationYear),
                (isConfirmed) => {
                    if (isConfirmed) {
                        abp.ui.setBusy();
                        this._PrgRequestAAPlasService.creatOrUpdateRequestAppl(action)
                            .finally(() => this.saving = false)
                            .subscribe(result => {
                                this.notify.info("Save SuccesFully");
                                this.GetAllRequests();
                                var email = ((document.getElementById("emailtmp") as HTMLInputElement).value);

                                this.emailContent = "<h4>Dear " +
                                    result.users[0].name +
                                    " ," +
                                    "</h4>" +
                                    "Program Name:" +
                                    result.name +
                                    "<br>" +
                                    result.managerName +
                                    " will be sending to you request for aapla" +
                                    "<br/>" +
                                    email +
                                    " <br/><br/><div>Thank You</div>";
                                this._emailServiceProxy.mailSending1(this.emailContent,
                                    'Request for WOTR Annual Action Plan',
                                    result.users[0].emailAddress,
                                    null,
                                    null,
                                    null)
                                    .subscribe((result) => {

                                    });
                                abp.ui.clearBusy();
                            });
                    }
                });
        }
    }

    funActivityMaping() {
        this._router.navigate(['/app/main/ActivityMapping'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funProgramCostEstimation() {
        this._router.navigate(['/app/main/ProgramCostEstimation'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funRegionSpending() {
        this._router.navigate(['/app/main/RegionSpending'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funApproveAAPla() {
        if (this.isGranted('Pages.RequestAAPlaSend')) {
            this._router.navigate(['/app/main/ApproveAAPla'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
        else {
            this._router.navigate(['/app/main/ImplementationPlan'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
    }
}