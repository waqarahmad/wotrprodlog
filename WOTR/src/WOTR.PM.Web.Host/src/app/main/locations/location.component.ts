﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { LocationServiceProxy, State, LocationListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { AppSessionService } from '@shared/common/session/app-session.service';
//import { MyLocationModalComponent } from '../locations/locationModal.component';

declare var jQuery: any;


@Component({
    selector: 'LocationModal',
    templateUrl: './location.component.html',
    styleUrls: ['./location.component.less'],
})
export class LocationComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('LocationModal1') modal: ModalDirective;
    // @ViewChild('myLocationModal') myLocationModal: MyLocationModalComponent;
    active: boolean = false;
    saving: boolean = false;
    
    stateList: State[];
    location: LocationListDto;
    locationList: LocationListDto[];
    constructor(
        injector: Injector,
        private _locationService: LocationServiceProxy,
    ) {
        super(injector);
       
    }
    ngOnInit(): void {
        //this.location = new LocationListDto();
        //this.getAllState();
    }

    
    ngAfterViewInit() {
        //this.show();        
    }
    checkLocationType(event) {
        this.location.locationType = event.target.value;
        //this.getAllLocation(this.location.locationType);

    }
    show(): void {
        this.active = true;
        this.location = new LocationListDto();
        this.location.locationType = 2;
        this.modal.show();
    }

    save() {
        this.saving = true;
        this._locationService.createLocation(this.location)
            .finally(() => this.saving = false)
            .subscribe((result) => {
                this.notify.info(result[0]);
                this.close();
                this.modalSave.emit(null);
            })
    }

    getAllState() {
        this._locationService.getAllState().subscribe(result => {
            this.stateList = result;
        })
    }
    //getAllLocation(locationType) {
    //    this._locationService.getAllParentLocations(locationType).subscribe(result => {
    //        this.locationList = result;
    //    })
    //}
    close(): void {
        this.modal.hide();
        this.active = false;
    }
    funLocation() {
        this.modal.show();
    }
}