﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { RoleServiceProxy, State, CropServiceProxy, GetAllCrops } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { CropModalComponent } from './crop.modal.component';

//import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'cropComponent',
    templateUrl: "./crop.component.html",
    styleUrls: ['./crop.component.less']

})

export class CropComponent extends AppComponentBase {
    @ViewChild('CropModal') myModalView: CropModalComponent;
    searchText = "";
    crop: GetAllCrops[] = [];
    cropList: GetAllCrops[] = []
    timer: any;
    currentPage: number = 1;

   
    constructor(
        injector: Injector,
        private _cropAppService: CropServiceProxy) {
        super(injector);
    }

    ngOnInit() {
        this.getAllCrop();
        this.getallcropsearch();
    }

    getallcropsearch(): void {
        abp.ui.setBusy();
        this._cropAppService.getAllCrop(this.searchText).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.length;

            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;

            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.cropList = result;
            this.crop = result;
            this.cropList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        });
    }


    getAllCrop(): void {
        abp.ui.setBusy();
        this._cropAppService.getAllCrop(this.searchText).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.length;

            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;

            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.cropList = result;
            this.crop = result;
            this.cropList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();

        })

    }

    searchPC(): void {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => { this.getallcropsearch() }, 1000);
    }

    editActionArea(p) {

        this.myModalView.show(p);
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.crop.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.cropList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);

    }

    delete(p) {
        this.message.confirm(
            this.l('Delete ' + p.name),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._cropAppService.delete(p.id)
                        .subscribe((result) => {
                            this.notify.success(this.l('Successfully Deleted  Record'));
                            this.getAllCrop();
                        });
                };

            });
    }

}