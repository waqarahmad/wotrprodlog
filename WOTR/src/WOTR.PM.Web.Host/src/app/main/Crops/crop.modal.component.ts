﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { ActionAreasServiceProxy, ActionAreasListDto, State, LocationServiceProxy, LocationListDto,  CropServiceProxy, Crop } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
declare var jQuery: any;

@Component({
    selector: 'CropModal',
    templateUrl: "./crop.modal.component.html",
    styleUrls: ['./crop.modal.component.less']
})

export class CropModalComponent extends AppComponentBase
{
    @ViewChild('CropModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    Crop: Crop = new Crop();
    saving: boolean = false;


    constructor(injector: Injector,
        private _CropServiceProxy: CropServiceProxy
    ) {
        super(injector)
    }

    ngOnInit() {


    }

    show(p) {
        this.active = true;
        if (p != undefined) {
            this.Crop.id = p.id;
            this.Crop = p;
        }
        else {
            this.Crop = new Crop();
        }
        this.active = true;
        this.modal.show();

    }

    

    save(): void {
        this.saving = true;
        if (this.Crop.name.trim() != "") {
            var r = this._CropServiceProxy.creatOrUpdateCrop(this.Crop)
                .finally(() => this.saving = false)
                .subscribe((result) => {
                    if (result == "Record alread Present!") {
                        this.notify.warn(this.l(result));
                    }
                    if (result == "Crop Update  sucessfully !" || result == "Crop Added  sucessfully !") {
                        this.notify.info(this.l(result));
                    }
                    this.close();
                    this.modalSave.emit(null);
                });
        } else {
            this.notify.info("Please Enter Crop");
            this.modalSave.emit(null);
        }
        
    }


    close(): void {
        this.modal.hide();
        this.active = false;
    }

}