import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UnitOfMeasuresComponent } from './unitOfMeasures/unitOfMeasures.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ActionAreaComponent } from './actionArea/actionArea.component';
import { DonorsComponent } from './donors/donors.component';
import { ActionSubActionAreaComponent } from './actionSubActionArea/actionSubActionArea.component';
import { ActivitesComponent } from './activites/activites.component';
import { componentsComponent } from './components/components.component';
import { SubActionAreaComponent } from './subActionArea/subActionArea.component';
import { LocationComponent } from './locations/location.component';
import { CreateProgramComponent } from './createProgram/createProgram.component';
import { ProgramInformationComponent } from './programinformation/programinformation.component';
import { ProgramComponentsComponent } from './programcomponents/programcomponents.component';
import { ProgramCostEstimationComponent } from './programCostEstimation/programCostEstimation.component';
import { ActivityMappingComponent } from './activityMapping/activityMapping.component';
import { regionSpendingComponent } from './regionSpending/regionSpending.component';
import { requestAAPlaComponent } from './requestAAPla/requestAAPla.component';
import { ApproveAAPlaComponent } from './approveAAPla/approveAAPla.component';
import { ImplementationPlanComponent } from './implementationPlan/implementationPlan.component';
import { projectFundsComponent } from './projectFunds/projectFunds.component';
import { CheckListComponent } from './checkList/checkList.component';
import { ActivityLiveComponent } from './activityLive/activityLive.component';
import { ProjectExpensesComponent } from './projectExpenses/projectExpenses.component';
import { ProgramClusterComponent } from './program-cluster/program-cluster.component';
import { QuestionaryMappingComponent } from './QuestionaryMapping/QuestionaryMapping.Component';
import { DistrictComponent } from './district/district.component';
import { TalukaComponent } from './taluka/taluka.component';
import { VillageComponent } from './village/village.component';
import { ProgramDetailsComponent } from './programDetails/programDetails.component';
import { RequestSendDetailsComponent } from './requestSendDetails/requestSendDetails.component';
import { ApproveApplaDetailsComponent } from './approveApplaDetails/approveApplaDetails.component';
import { ImplemenationPlanGroupingDetailsComponent } from './implemenationPlanGroupingDetails/implemenationPlanGrouping.component';
import { ExpensesDetailsComponent } from './expensesDetails/expensesDetails.component';
import { OverallRegionSpendingDetailsComponent } from './overallRegionSpendingDetails/overallRegionSpendingDetails.component';
import { yearWiseRegionSpendingDetailsComponent } from './yearWiseRegionSpendingDetails/yearWiseRegionSpendingDetails.component';
import { projectListComponent } from './projectList/projectList.component';
import { projectListIIIComponent } from './projectListIII/projectListIII.component';
import { DonorWiseListComponent } from './donorWiseList/donorWiseList.component';

import { RegionWiseListcomponent } from './regionWiseList/RegionWiseList.component';
import { RegionWiseListDetailscomponent } from './regionWiseListDetails/regionWiseListDetails.component';
import { RegionWiseSumarrycomponent } from './regionWiseSumarry/regionWiseSumarry.component';
import { ActionAreawisecomponent } from './actionAreawise/actionAreawise.component';
import { OverAllAnualActioncomponent } from './overAllAnualAction/overAllAnualAction.component';
import { ClusterWiseReportcomponent } from './clusterWiseReport/clusterWiseReport.component';
import { QuarterWisecomponent } from './quarterWise/quarterWise.component';
import { ProgComponent } from './prog/prog.component';

import { CropComponent } from './Crops/crop.component'
import { CategoryComponent } from './categories/IICategory.component'
import { SubCategoryComponent } from './subCategories/IIsubcategory.component'
import { SourceComponent } from './sources/Source.component';
import { QuestionaryComponent } from './ImpactIndicatorQuestionaries/Questionary.component';
import { ImpactIndicatorFormComponent } from './ImpactIndicatorFormMaster/ImpactIndicatorCreateEditForm.component';
import { IIFormAndProgramMapComponent } from './IIFormAndProgramMappings/IIFormAndProgramMapping.component';
import { IIResponseComponent } from './ImpactIndicatorResponseForm/IIResponseForm.component';
import { FreqOfOccurrenceComponent } from './FrequencyOfOccurences/FreqOfOccurence.component';
import { ImpactIndicatorReportComponent } from './ImpactIndicatorReports/ImpactIndicatorReport.component';



@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [                   
                    { path: 'dashboard', component: DashboardComponent, data: { permission: 'Pages.Tenant.Dashboard', breadcrumb: "DASHBOARD"} },
                    { path: 'ActionArea', component: ActionAreaComponent, data: { permission: 'Pages.ActionArea', breadcrumb: "ACTION AREA" }},
                    { path: 'Donor', component: DonorsComponent, data: { permission: 'Pages.Donars',  breadcrumb: "DONOR"} },
                    { path: 'ActionSubAction', component: ActionSubActionAreaComponent, data: { permission: 'Pages.ActionSubAction',  breadcrumb: "ACTION SUB ACTION AREA"} },
                    { path: 'Activity', component: ActivitesComponent, data: { permission: 'Pages.Activity', breadcrumb: "ACTIVITE"} },
                    { path: 'Components', component: componentsComponent, data: { permission: 'Pages.Components', breadcrumb: "COMPONENTS"} },
                    { path: 'SubAction', component: SubActionAreaComponent, data: { permission: 'Pages.SubActionArea', breadcrumb: "SUB ACTION AREA"} },
                    { path: 'Location', component: LocationComponent, data: { permission: 'Pages.Location', breadcrumb: "LOCATION" } },
                    { path: 'CreateProgram', component: CreateProgramComponent, data: {  breadcrumb: "CREATE PROGRAM" } },
                    { path: 'CreateProgramInformation', component: ProgramInformationComponent, data: { } },
                    { path: 'ProgramComponent', component: ProgramComponentsComponent, data: { permission: 'Pages.ProgramComponent',  breadcrumb: "Program : Integrated Watershed Development in Shevagaon District" } },
                    { path: 'ProgramCostEstimation', component: ProgramCostEstimationComponent, data: { permission: 'Pages.ProgramCostEstimation', breadcrumb: "Program : Integrated Watershed Development in Shevagaon District" } },
                    { path: 'ActivityMapping', component: ActivityMappingComponent, data: { permission: 'Pages.ActivityMapping',  breadcrumb: "Program : Integrated Watershed Development in Shevagaon District" } },
                    { path: 'RegionSpending', component: regionSpendingComponent, data: { permission: 'Pages.RegionSpending', breadcrumb: "Program : Integrated Watershed Development in Shevagaon District" } },
                    { path: 'RequestAAPla', component: requestAAPlaComponent, data: { permission: 'Pages.RequestAAPla',  breadcrumb: "Program : Integrated Watershed Development in Shevagaon District" } },
                    { path: 'ApproveAAPla', component: ApproveAAPlaComponent, data: {  breadcrumb: "Program : Integrated Watershed Development in Shevagaon District" } },
                    { path: 'ImplementationPlan', component: ImplementationPlanComponent, data: { breadcrumb: "Program : Integrated Watershed Development in Shevagaon District" } },
                    { path: 'ProjectFund', component: projectFundsComponent, data: { breadcrumb: "Program : Integrated Watershed Development in Shevagaon District" } },
                    { path: 'UnitOfMeasurMent', component: UnitOfMeasuresComponent, data: { breadcrumb: "UNIT OF MEASURES" } },
                    { path: 'CheckList', component: CheckListComponent, data: { breadcrumb: "CHECK LIST" } },
                    { path: 'ActivityLive', component: ActivityLiveComponent, data: { breadcrumb: "CHECK LIST" } },
                    { path: 'ProjectExpenses', component: ProjectExpensesComponent, data: { breadcrumb: "CHECK LIST" } },
                    { path: 'ProgramCluster', component: ProgramClusterComponent, data: { breadcrumb: "PROGRAM CLUSTER" } },
                    { path: 'QuestionaryMapping', component: QuestionaryMappingComponent, data: { breadcrumb: "Questionary" } },
                    { path: 'District', component: DistrictComponent, data: { breadcrumb: "District" } },
                    { path: 'Taluka', component: TalukaComponent, data: { breadcrumb: "Taluka" } },
                    { path: 'Village', component: VillageComponent, data: { breadcrumb: "Village" } },
                    { path: 'ProgramDetails', component: ProgramDetailsComponent, data: { breadcrumb: "ProgramDetails" } },
                    { path: 'RequestSendDetails', component: RequestSendDetailsComponent, data: { breadcrumb: "RequestSendDetails" } },
                    { path: 'ApproveAaaplaDetails', component: ApproveApplaDetailsComponent, data: { breadcrumb: "ApproveAaaplaDetails" } },
                    { path: 'ImplemenationPlanGroupingDetails', component: ImplemenationPlanGroupingDetailsComponent, data: { breadcrumb: "ImplemenationPlanGroupingDetails" } },
                    { path: 'ExpensesDetails', component: ExpensesDetailsComponent, data: { breadcrumb: "ExpensesDetails" } },
                    { path: 'OverAllRegion', component: OverallRegionSpendingDetailsComponent, data: { breadcrumb: "OverAllRegion" } },
                    { path: 'YearWiseRegion', component: yearWiseRegionSpendingDetailsComponent, data: { breadcrumb: "YearWiseRegion" } },
                    { path: 'ProjectList', component: projectListComponent, data: { breadcrumb: "ProjectList" } },
                    { path: 'ProjectListIII', component: projectListIIIComponent, data: { breadcrumb: "ProjectListIII" } },
                    { path: 'DonorWsie', component: DonorWiseListComponent, data: { breadcrumb: "DonorWsie" } },
                    { path: 'RegionWsie', component: RegionWiseListcomponent, data: { breadcrumb: "RegionWsie" } },
                    { path: 'RegionWiseDetails', component: RegionWiseListDetailscomponent, data: { breadcrumb: "RegionWiseDetails" } },
                    { path: 'RegionWiseSumarry', component: RegionWiseSumarrycomponent, data: { breadcrumb: "RegionWiseSumarry" } },
                    { path: 'ActionAreawise', component: ActionAreawisecomponent, data: { breadcrumb: "ActionAreawise" } },
                    { path: 'OverAllAnualAction', component: OverAllAnualActioncomponent, data: { breadcrumb: "OverAllAnualAction" } },
                    { path: 'ClusterWiseReport', component: ClusterWiseReportcomponent, data: { breadcrumb: "ClusterWiseReport" } },
                    { path: 'QuarterWiseReport', component: QuarterWisecomponent, data: { breadcrumb: "QuarterWiseReport" } },
                    { path: 'Prog', component: ProgComponent },
                    { path: 'Crop', component: CropComponent },
                    { path: 'Category', component: CategoryComponent },
                    { path: 'SubCategory', component: SubCategoryComponent },
                    { path: 'Source', component: SourceComponent },
                    { path: 'ImpactIndicatorQuestionary', component: QuestionaryComponent },
                    { path: 'ImpactIndicatorForm', component: ImpactIndicatorFormComponent },
                    { path: 'ImpactIndicatorFormAndPogramMap', component: IIFormAndProgramMapComponent }, 
                    { path: 'ImpactIndicatorResponseForm', component: IIResponseComponent },
                    { path: 'FrequencyOfOccurrence', component: FreqOfOccurrenceComponent },
                    { path: 'ImpactIndicatorReport', component: ImpactIndicatorReportComponent }
                    
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class MainRoutingModule { }
