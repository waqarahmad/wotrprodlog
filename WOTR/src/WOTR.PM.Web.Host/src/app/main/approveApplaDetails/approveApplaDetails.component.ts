﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { ListActionSubactionListDto, Village, RequestAAPlasListDto,PrgRequestAAPlasServiceProxy, ProgramServiceProxy, ProgramInformationListDto, PrgVillageClusterListDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute } from '@angular/router';
import { FileDownloadService } from '@shared/utils/file-download.service';


@Component({
    selector: 'approveApplaDetailscomponent',
    templateUrl: "./approveApplaDetails.component.html",
    styleUrls: ['./approveApplaDetails.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ApproveApplaDetailsComponent extends AppComponentBase {    
    @ViewChild('test') el: ElementRef;

    count: number = -1;
    finalUnits: number = 0;
    sumOfTotal: number = 0;
    sumOfTotal1: number = 0;
    programName: string;
    locationName: string;
    managerName: string;
    requestStatus: number;
    costestimationyear: string;
    ActivityLevel: Number;
    activityname: string;
    Granamout: number;
    ddd: number[];
    Manager: number;
    startdate: string;
    EndDate: string;
    aaaa: boolean;

    selectedPermission = '';
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: ProgramInformationListDto[] = [];
    programdetails: ProgramInformationListDto[];
    programd: ProgramInformationListDto = new ProgramInformationListDto();
    VillageList: any = [];
    ActivityList: any = [];
    ComponentList: any = [];
    VillageClustorList: any = [];
    program: ProgramInformationListDto[];
    requestList: RequestAAPlasListDto[] = [];
    villageclustor: PrgVillageClusterListDto[];
    pro: any[];
    active: boolean = false;
    list: any = [];
    requestSingleYearList: RequestAAPlasListDto = new RequestAAPlasListDto();
    AddSubActionList: ListActionSubactionListDto;
    emailContent: any;
    saving: boolean = false;
    prgVilages: Village[] = [];
    Year: any = [];
    costYear: string;
    programId : number;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,
        private _programService: ProgramServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy)
    {
        super(injector);
    }

    ngOnInit() {
        this.getAllProgram();
        this.programName = this._activatedRoute.snapshot.queryParams.ProgrameName;
    }  
    getcostestimationyear(programId) {
        this._PrgRequestAAPlasService.prgCostEstimationYears(programId).subscribe(result => {
            this.Year = result;

        });
    }
    getAllProgram() {
        this._PrgRequestAAPlasService.getAllProgramForApproveAppla().subscribe(result => {
            this.programdetails = result;
        });
    }
   
    //onChangeState(event) {
    //    this.getAllProgrambyid(event.target.value);
    //}
    
    getAllProgrambyid(programId, Year) {
        abp.ui.setBusy();
        this.costYear = Year;
        this.programId = programId;

        this._PrgRequestAAPlasService.getApproveApplaForprogramId(programId, this.costYear).subscribe(result => {
            this.requestList = result;
            if (result.length != 0) {
                this.programName = result[0].programName;
            }
            else {
                this.notify.success("Request Not Approve For This Year");
            }
           
            abp.ui.clearBusy();
        });
    }
  
    exportToExcel(programid): void {
        abp.ui.setBusy();
        this._PrgRequestAAPlasService.createApproveapplaExcelDoc("ApproveApplaDetails", this.programId, this.costYear).subscribe(result => {
            this._fileDownloadService.downloadTempFile1(result);
            abp.ui.clearBusy();
        });
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
}