﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { ActionAreasServiceProxy, ActionAreasListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
declare var jQuery: any;
@Component({
    selector: 'ModalActionArea',
    templateUrl: "./ModalActionArea.component.html",
    styleUrls: ['./ModalActionArea.component.less'],
})
export class ModalActionAreaComponent extends AppComponentBase {
    @ViewChild('ModalActionArea') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    passwordComplexityInfo = '';
    actionarea: ActionAreasListDto = new ActionAreasListDto();
    saving: boolean = false;
    search: ActionAreasListDto[] = [];
    @ViewChild('projectForm') eForm: NgForm;//yo

    constructor(
        injector: Injector,
        private _actionareaService: ActionAreasServiceProxy)
    {
        super(injector);        
    }

    show(p): void {
        this.active = true;
        if (p != undefined) {
            this.actionarea = p;
        }
        else {
            this.actionarea = new ActionAreasListDto();
        }
        this.modal.show();
    }

    close(): void {
        this.modal.hide();
        this.active = false;       
    }
    //...save the Record in ActionArea table.
    save(): void {
         this.saving = true;
        var r = this._actionareaService.creatOrUpdateActionArea(this.actionarea)
            .finally(() => this.saving = false)
            .subscribe((result) => {
                if (result[0] =="Record is already Present") {
                    this.notify.warn(this.l(result[0]));
                }
                if (result[0] == "ActionArea Update  sucessfully !" || result[0] == "ActionArea Added  sucessfully !") {
                    this.notify.info(this.l(result[0]));
                }
                this.close();
                this.modalSave.emit(null);
            });
    }
 
}