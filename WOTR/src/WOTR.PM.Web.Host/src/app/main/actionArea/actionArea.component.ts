﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { RoleServiceProxy, ActionAreasServiceProxy, ActionAreasListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalActionAreaComponent } from './ModalActionArea.component';
//import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    selector: 'actionAreacomponent',
    templateUrl: "./actionArea.component.html",
    styleUrls: ['./actionArea.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ActionAreaComponent extends AppComponentBase {    
    @ViewChild('ModalActionArea') myModalView: ModalActionAreaComponent;
    selectedPermission = '';
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: ActionAreasListDto[] = [];
    active: boolean = false;
    list: any = [];
    constructor(
        injector: Injector,
        private _actionareaService: ActionAreasServiceProxy,
        private _roleService: RoleServiceProxy)
    {
        super(injector);
    }

    ngOnInit() {
        this.getAllActionAreaRecord();       
    }    
    getAllActionAreaRecordsearch(): void {  
        var input = (document.getElementById("txtActionArea") as HTMLInputElement).value;
        abp.ui.setBusy();
        this._actionareaService.getsearch(input).subscribe((result) => {       
            this.primengDatatableHelper.totalRecordsCount = result.length;
            //this.primengDatatableHelper.records = result.;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;
            //this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.Record = result;
            this.Record = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        });
    }


    getAllActionAreaRecord(): void {
        var input = (document.getElementById("txtActionArea") as HTMLInputElement).value;
        abp.ui.setBusy();
        this._actionareaService.getAllActionAreaRecord().subscribe((result) => {
          
            this.primengDatatableHelper.records = result;
            this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.Record = result;
            this.Record = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        });
    }
    editActionArea(p) {
        
        this.myModalView.show(p);
    }
    
        count: number = -1;
        finalUnits: number = 0;
    sumOfTotal: number = 0;
    sumOfTotal1: number = 0;
    DivideUnits(res, rec) {
    }
    
    deleteActionArea(del) {
        this.message.confirm(
            this.l('ActionAreaDeleteWarningMessage', del.name),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._actionareaService.deleteActoinArea(del.id)
                        .subscribe((result) => {
                            if (result == "Record Exist") {
                                this.notify.success(this.l('This record is already in used'));
                                this.getAllActionAreaRecord();
                            }
                            else {
                                this.notify.success(this.l('Successfully Deleted Action Area Record'));
                                this.getAllActionAreaRecord();
                            }
                        });
                }
            })
    }
    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
   
}