﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { RoleServiceProxy, ActionAreasServiceProxy, ProgramServiceProxy, Clustorwise,PrgRequestAAPlasServiceProxy, ProgrameComponentListDto, ProgramCompnentsServiceProxy, ActionAreasListDto, ProgramInformationListDto, PrgVillageClusterListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
//import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';
import { debounceTime } from 'rxjs/operator/debounceTime';
import * as moment from "moment";
import { forEach } from '@angular/router/src/utils/collection';
import { FileDownloadService } from '@shared/utils/file-download.service';



@Component({
    selector: 'clusterWiseReportcomponent',
    templateUrl: "./clusterWiseReport.component.html",
    styleUrls: ['./clusterWiseReport.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ClusterWiseReportcomponent extends AppComponentBase {
    selectedPermission = '';
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: ProgramInformationListDto[] = [];
    programdetails: ProgramInformationListDto[];
    programd: ProgramInformationListDto = new ProgramInformationListDto();
    VillageList: any = [];
    ActivityList: any = [];
    ComponentList: any = [];
    VillageClustorList: any = [];
    program: Clustorwise[];
    villageclustor: PrgVillageClusterListDto[];
    componentWithActivityList: ProgrameComponentListDto[] = [];

    pro: any[];
    active: boolean = false;
    list: any = [];
    Year: any = [];
    costyear: string;
    constructor(
        injector: Injector,
        private _actionareaService: ActionAreasServiceProxy,
        private _programService: ProgramServiceProxy,
        private _ProgramCompnentsService: ProgramCompnentsServiceProxy,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy,

        private _fileDownloadService: FileDownloadService,
        private _roleService: RoleServiceProxy) {
        super(injector);
    }

    ngOnInit() {
        this.getAllProgram();
        this.getcostestimationyear();
    }

    getcostestimationyear() {
        this._PrgRequestAAPlasService.overallPrgCostEstimationYears().subscribe(result => {
            this.Year = result;

        });
    }
    getAllProgram() {
        this._programService.getAllProgram().subscribe(result => {
            this.programdetails = result;
        })
    }


    count: number = -1;
    finalUnits: number = 0;
    sumOfTotal: number = 0;
    sumOfTotal1: number = 0;
    DivideUnits(res, rec) {
    }
    onChangeState(event) {
        //this.getAllProgrambyid(event.target.value);
    }

    programName: string;
    ClusterName: string;

    getAllProgrambyid(programId, year) {
        this.costyear = year;
        this._programService.getClusterWiseReport(programId,year).subscribe(result => {
            this.program = result;
            this.programName = this.program[0].programName;
            //this.ClusterName = this.program[0].clusterName;
        })

    }



    exportToExcel(programId): void {
        this._programService.createExcelClusterWise("ClusterWiseReport", programId, this.costyear).subscribe(result => {

            ;
            this._fileDownloadService.downloadTempFile1(result);
        })

    }

    export(e) {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('div[id$=a]').html()));
        e.preventDefault();
        
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }

}