﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { RoleServiceProxy, ActionAreasServiceProxy, ListActionSubactionListDto, Village, RequestAAPlasListDto, PrgRequestAAPlasServiceProxy, ProgramServiceProxy, ActionAreasListDto, ProgramInformationListDto, PrgVillageClusterListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';

//import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';
import { debounceTime } from 'rxjs/operator/debounceTime';
import * as moment from "moment";
import { forEach } from '@angular/router/src/utils/collection';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { DataTable } from "primeng/components/datatable/datatable";
declare let jsPDF;




@Component({
    selector: 'regionWiseListcomponent',
    templateUrl: "./regionWiseList.component.html",
    styleUrls: ['./regionWiseList.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class RegionWiseListcomponent extends AppComponentBase {
    @ViewChild('test') el: ElementRef;   
   
    programd: ProgramInformationListDto = new ProgramInformationListDto();   
    program: ProgramInformationListDto[];

    requestList: RequestAAPlasListDto[] = [];
    villageclustor: PrgVillageClusterListDto[];
    pro: any[];
    active: boolean = false;
    list: any = [];
    requestSingleYearList: RequestAAPlasListDto = new RequestAAPlasListDto();
    AddSubActionList: ListActionSubactionListDto;
    emailContent: any;
    saving: boolean = false;
    prgVilages: Village[] = [];
    Record: ProgramInformationListDto[] = [];
    Year: any[];
    costyear: string;


    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,

        private _actionareaService: ActionAreasServiceProxy,
        private _programService: ProgramServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy,

        private _roleService: RoleServiceProxy) {
        super(injector);
       
    }

    ngOnInit() {


        this.getallyear();

        

    }


    getallyear() {
        this._programService.allyear().subscribe(result => {
            this.Year = result;

        })
    }


    count: number = -1;
    finalUnits: number = 0;
    sumOfTotal: number = 0;
    sumOfTotal1: number = 0;
    DivideUnits(res, rec) {
    }
   

    GetAllRegionWiseWiseList(year) {
        this.costyear = year;
        this._programService.getAllProgramsDetailsRegionWiseWise(this.costyear).subscribe(result => {
            this.Record = result

        })
    }



    exportToExcel(): void {
        this._programService.createExcelRegionWisWise("Region Wise", this.costyear).subscribe(result => {
            this._fileDownloadService.downloadTempFile1(result);
        })
    }
}