﻿import { Component, Pipe, PipeTransform, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { QuestionListDto, ImpactIndicatorFormServiceProxy, IIFormProgramMappingServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
declare var jQuery: any;


@Component({
    selector: 'IIQuestionListModal',
    templateUrl: "./QuestionList.Modal.component.html",
    styleUrls: ['./QuestionList.Modal.component.less']
})

export class IIQuestionListModal extends AppComponentBase {

    @ViewChild('IIQuestionListModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    timer: any;
    searchText = "";
    currentPage: number = 1;
    active: boolean = false;
    saving: boolean = false;
    Question: QuestionListDto[] = [];
    QuestionList: QuestionListDto[] = [];
    formId: any;

    constructor(injector: Injector, private _ImpactIndicatorFormServiceProxy: ImpactIndicatorFormServiceProxy
       ) {
        super(injector)
    }



    show(p,cat) {
        this.active = true;
        if (p != undefined) {
            if (cat == 1) {
                this.formId = p.id;
            } else if(cat == 2){
                this.formId = p.impactIndicatorFormId;
            }
            
            this.getAllQuestions(this.formId)
        }
        else {
            this.QuestionList = [];
            this.Question = [];
        }
        this.modal.show();
    }

    getAllQuestions(id) {
        if (id != undefined) {
            this._ImpactIndicatorFormServiceProxy.fetchAllQuestionsByFormId(this.formId).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.length;

            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;

            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.QuestionList = result;
            this.Question = result;
            this.QuestionList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();

        })
        }
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.Question.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.QuestionList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
}