﻿import { Component, Injector, ViewChild } from '@angular/core';
import { IIFormProgramMappingServiceProxy, ProjectAndIIFormMapping, GetAllProgramAndFormMapping } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ImpactIndicatorFormAndProgramModal } from './IIFormAndProgramMapModal.component'
import { IIQuestionListModal } from './QuestionList.Modal.component'



@Component({
    selector: 'IIFormAndProgramMapComponent',
    templateUrl: "./IIFormAndProgramMapping.component.html",
    styleUrls: ['./IIFormAndProgramMapping.component.less']

})

export class IIFormAndProgramMapComponent extends AppComponentBase {
    @ViewChild('IIFormProgramMapModal') myModalView: ImpactIndicatorFormAndProgramModal;
    @ViewChild('IIQuestionListModal') IIQuestionListModal: IIQuestionListModal;
    Mapping: GetAllProgramAndFormMapping[] = [];
    MappingList: GetAllProgramAndFormMapping[] = []
    timer: any;
    searchText = "";
    currentPage: number = 1;

    constructor(
        injector: Injector,
        private _IIFormProgramMappingAppService: IIFormProgramMappingServiceProxy) {
        super(injector);
    }

    ngOnInit() {
        this.getAllMapping();
    }


    getAllMapping(): void {
        abp.ui.setBusy();
        this._IIFormProgramMappingAppService.getAllProgramFormMapping(this.searchText).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.length;

            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;

            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.MappingList = result;
            this.Mapping = result;
            this.MappingList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();

        })

    }

    searchPC(): void {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => { this.getAllMapping() }, 1000);
    }

    delete(Mapping) {
        this.message.confirm(
            this.l('Delete Mapping', 'Mapping'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._IIFormProgramMappingAppService.delete(Mapping.id)
                        .subscribe((result) => {
                            this.notify.success(this.l('Successfully Deleted  Record'));
                            this.getAllMapping();
                        });
                };

            });
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.Mapping.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.MappingList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
    }

    editActionArea(p) {
        this.myModalView.show(p);

    }


}