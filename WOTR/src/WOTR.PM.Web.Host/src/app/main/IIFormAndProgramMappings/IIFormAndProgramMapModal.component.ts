﻿import { Component, Pipe, PipeTransform, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { IIFormProgramMappingServiceProxy, ImpactIndicatorResponseServiceProxy, ImpactIndicatorFormServiceProxy, ProgramServiceProxy, ProgramInformationListDto, ProjectAndIIFormMapping, ImpactIndicatorForm } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';


declare var jQuery: any;

@Component({
    selector: 'IIFormProgramMapModal',
    templateUrl: "./IIFormAndProgramMapModal.component.html",
    styleUrls: ['./IIFormAndProgramMapModal.component.less']
})

export class ImpactIndicatorFormAndProgramModal extends AppComponentBase {

    @ViewChild('IIFormProgramMapModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    saving: boolean = false;
    projectAndIIFormMapping : ProjectAndIIFormMapping = new ProjectAndIIFormMapping();
    Program: ProgramInformationListDto = new ProgramInformationListDto();
    ProgramList: ProgramInformationListDto[] = [];
    ImpactIndicatorForm: ImpactIndicatorForm = new ImpactIndicatorForm();
    ImpactIndicatorFormList: ImpactIndicatorForm[] = [];



    constructor(injector: Injector,
        private _ProgramServiceProxy: ProgramServiceProxy,
        private _ImpactIndicatorFormServiceProxy: ImpactIndicatorFormServiceProxy,
        private _IIFormProgramMappingServiceProxy: IIFormProgramMappingServiceProxy,
        private _ImpactIndicatorResponseServiceProxy: ImpactIndicatorResponseServiceProxy
    ) {
        super(injector)
    }



    show(p) {
        this.active = true;
        this.bindForms();
        this.bindPrograms();
        if (p != undefined) {
            this.projectAndIIFormMapping.id = p.id;
            this.projectAndIIFormMapping = p;
        }
        else {
            this.projectAndIIFormMapping = new ProjectAndIIFormMapping();
        }
        this.modal.show();
    }

    

    bindForms() {
        this._ImpactIndicatorFormServiceProxy.getAllApprovedForm('').subscribe(result => {
            this.ImpactIndicatorFormList = result;
        })
    }

    bindPrograms() {
        this._ImpactIndicatorResponseServiceProxy.getAllProgramForReport().subscribe(result => {
            this.ProgramList = result;
        });
    }
    

    clearFields() {
        this.projectAndIIFormMapping.impactIndicatorFormId = undefined;
        this.projectAndIIFormMapping.programId = undefined;
    }

    close(): void {
        this.clearFields();
        this.modal.hide();
        this.active = false;
    }

    log(event) { }

    save() {

        this._IIFormProgramMappingServiceProxy.createOrEdit(this.projectAndIIFormMapping).subscribe(result => {
                this.notify.info(this.l(result));
            this.close();
            this.modalSave.emit(null);
        })
    }


}