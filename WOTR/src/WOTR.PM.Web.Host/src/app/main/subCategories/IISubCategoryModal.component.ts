﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { GetAllCategory,GetAllSubCategory, CategoryServiceProxy, SubCategoryServiceProxy, ImpactIndicatorSubCategory } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
declare var jQuery: any;

@Component({
    selector: 'SubCategoryModal',
    templateUrl: "./IISubCategoryModal.component.html",
    styleUrls: ['./IISubCategoryModal.component.less']
})

export class SubCategoryModalComponent extends AppComponentBase {
    @ViewChild('SubCategoryModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    IISubCategory: ImpactIndicatorSubCategory = new ImpactIndicatorSubCategory();
    saving: boolean = false;
    Category: GetAllCategory = new GetAllCategory();
    CategoryList: GetAllCategory[] = [];


    constructor(injector: Injector,
        private _SubCategoryServiceProxy: SubCategoryServiceProxy,
        private _CategoryServiceProxy: CategoryServiceProxy
    ) {
        super(injector)
    }

    ngOnInit() {

        this.getAllCategory();
    }

    show(p) {
        this.active = true;
        if (p != undefined) {
            this.IISubCategory = p;
            this.IISubCategory.id = p.id;
        }
        else {
            this.IISubCategory = new ImpactIndicatorSubCategory();
        }
        this.active = true;
        this.modal.show();

    }


    save(): void {
        if (this.IISubCategory.impactIndicatorCategoryId != undefined) {
            if (this.IISubCategory.subCategory.trim() != "") {
            this.saving = true;
            var r = this._SubCategoryServiceProxy.createEditImapactIndiactorSubCategory(this.IISubCategory)
                .finally(() => this.saving = false)
                .subscribe((result) => {
                    if (result == "Record Already Present!") {
                        this.notify.warn(this.l(result));
                    }
                    if (result == "SubCategory Update  sucessfully !" || result == "SubCategory Added  sucessfully !") {
                        this.notify.info(this.l(result));
                    }
                    this.close();
                    this.modalSave.emit(null);
                });
            } else { this.notify.warn("Please Enter SubCategory"); }

        } else {
            this.notify.warn("Please Enter Category");
        }
    }


    close(): void {
        this.modal.hide();
        this.active = false;
    }

    getAllCategory() {
        this._CategoryServiceProxy.getAllImapactIndiactorCategory('').subscribe(result => {
            this.CategoryList = result;
        })
    }



}