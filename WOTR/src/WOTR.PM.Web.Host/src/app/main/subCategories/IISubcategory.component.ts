﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { RoleServiceProxy, SubCategoryServiceProxy, ImpactIndicatorSubCategory, GetAllSubCategory } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { SubCategoryModalComponent } from './IISubCategoryModal.component';


@Component({
    selector: 'SubCategory',
    templateUrl: "./IISubCategory.component.html",
    styleUrls: ['./IISubCategory.component.less']

})

export class SubCategoryComponent extends AppComponentBase {
    @ViewChild('SubCategoryModal') myModalView: SubCategoryModalComponent;
    searchText: string = "";
    SubCategory: GetAllSubCategory[] = []
    SubCategoryList: GetAllSubCategory[] = []
    timer: any;
    currentPage: number = 1;



    constructor(injector: Injector,
        private _SubCategoryAppService: SubCategoryServiceProxy
    ) {
        super(injector)
    }

    ngOnInit() {
        this.getAllSubCategory();
        this.getallSubCategorysearch();
    }

    getallSubCategorysearch(): void {
        abp.ui.setBusy();
        this._SubCategoryAppService.getAllImapactIndiactorSubCategory(this.searchText).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.length;

            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;

            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.SubCategoryList = result;
            this.SubCategory = result;
            this.SubCategoryList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        });
    }


    getAllSubCategory(): void {
        abp.ui.setBusy();
        this._SubCategoryAppService.getAllImapactIndiactorSubCategory(this.searchText).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.length;

            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;

            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.SubCategoryList = result;
            this.SubCategory = result;
            this.SubCategoryList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();

        })

    }

    searchPC(): void {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => { this.getallSubCategorysearch() }, 1000);
    }

    editActionArea(p) {

        this.myModalView.show(p);
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.SubCategory.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.SubCategoryList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);

    }

    delete(p) {
       
        this.message.confirm(
            this.l('Delete ' + p.subCategory),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._SubCategoryAppService.delete(p.id)
                        .subscribe((result) => {
                            this.notify.success(this.l('Successfully Deleted  Record'));
                            this.getAllSubCategory();
                        });
                };

            });
    }




}


