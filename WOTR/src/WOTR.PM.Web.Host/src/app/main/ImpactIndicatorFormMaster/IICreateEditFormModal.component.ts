﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AddEditImpactIndicatorForm, DualListDto, QuestionaryServiceProxy, GetAllQuestionByIdsInputDto, ImpactIndicatorFormServiceProxy, GetAllImpactIndicatorIForms, SubCategoryServiceProxy, CategoryServiceProxy, GetAllCategory, GetAllSubCategory, AddEditQuestionaryDto, EmailServiceProxy, GetUserDetailsForMailDto, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { DualListBoxComponent } from 'ng2-dual-list-box'

@Component({
    selector: 'IIFormModal',
    templateUrl: "./IICreateEditFormModal.component.html",
    styleUrls: ['./IICreateEditFormModal.component.less']
})

export class ImpactIndicatorFormModal extends AppComponentBase {
    @ViewChild('queList') SelectedItem: DualListBoxComponent;
    @ViewChild('IIFormModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    saving: boolean = false;
    addEditQuestionary: AddEditQuestionaryDto = new AddEditQuestionaryDto();
    Category: GetAllCategory = new GetAllCategory();
    CategoryList: GetAllCategory[] = [];
    SubCategory: GetAllSubCategory = new GetAllSubCategory();
    SubCategoryList: GetAllSubCategory[] = [];
    AddEditIIForm: AddEditImpactIndicatorForm = new AddEditImpactIndicatorForm();
    items = [];
    filteredArray = [];
    selected = [];
    available = [];
    userRole;
    rolename: string = '';
    dualListDto: DualListDto[] = []
    UserDetailsListForMail: GetUserDetailsForMailDto[] = [];
    getAllQuestionByIdsInputDto: GetAllQuestionByIdsInputDto = new GetAllQuestionByIdsInputDto();
    emailContent: string;
    Status: { id: string, name: string }[] = [];
    isEdit: boolean = false;
    formStatus: string = "";

    constructor(injector: Injector,
        private _CategoryServiceProxy: CategoryServiceProxy,
        private _SubCategoryServiceProxy: SubCategoryServiceProxy,
        private _ImpactIndicatorFormServiceProxy: ImpactIndicatorFormServiceProxy,
        private _QuestionaryServiceProxy: QuestionaryServiceProxy,
        private _emailServiceProxy: EmailServiceProxy,
        private _userServiceProxy: UserServiceProxy,
    ) {
        super(injector)
    }

    show(p) {
        this.active = true;
        this.items = [];
        this.selected = [];
        this.userRole = this.appSession.user.userRole;
        this.getrolename();
        this.isEdit = true;
        if (p != undefined) {
            this.bindCategory();
            this.bindEditable(p);
            this.active = true;
            if (p.formStatus == "Approved") {
                this.isEdit = true;
            }
            $('#lblTitle').text('Edit Form');
        }
        else {
            this.bindCategory();
            this.bind();
            this.active = true;
            this.AddEditIIForm = new AddEditImpactIndicatorForm();
            this.addEditQuestionary = new AddEditQuestionaryDto();
            this.clearFields();
        }
        this.modal.show();
    }

    getrolename() {
        this._userServiceProxy.getUserRoleById(this.appSession.user.userRole).subscribe(result => {
            this.rolename = result;
            this.bindStatus();

        });
    }

    bindEditable(p) {
        this._ImpactIndicatorFormServiceProxy.getImapactIndiactorFormForEdit(p.id).subscribe(response1 => {
            this.AddEditIIForm = response1;
            this._QuestionaryServiceProxy.fetchAllQuetionByQuestionId(response1.questionList).subscribe(response2 => {
                this.selected = response2;
                this.selected.forEach((s) => {
                    this.SelectedItem.availableItems = this.SelectedItem.availableItems.filter(t => t.value != s.id.toString());
                    if (this.SelectedItem.selectedItems.filter(t => t.value == s.id.toString()).length == 0) {
                        this.SelectedItem.selectedItems.push({ value: s.id.toString(), text: s.name });
                    }
                });
            });
        });
    }

    bindCategory() {
        this._CategoryServiceProxy.getAllImapactIndiactorCategory('').subscribe(result => {
            this.CategoryList = result;
        })
        this.appSession.user.userRole
        this.bind();
    }

    bindSubCategory() {
        if (this.addEditQuestionary.impactIndicatorCategoryId != undefined) {
            this._SubCategoryServiceProxy.getAllImapactIndiactorSubCategoryByCategoryId(this.addEditQuestionary.impactIndicatorCategoryId).subscribe(result => {
                this.SubCategoryList = result;
            })
        }
        this.bind();
    }

    bind() {
        this.items = [];

        this.getAllQuestionByIdsInputDto.categoryId = this.addEditQuestionary.impactIndicatorCategoryId;
        this.getAllQuestionByIdsInputDto.subCategoryId = this.addEditQuestionary.impactIndicatorSubCategoryId;
        this._QuestionaryServiceProxy.fetchAllQuetionByCateAndSubCat(this.getAllQuestionByIdsInputDto).subscribe(result => {
            this.items = result;
        })
    }

    clearFields() {
        this.isEdit = false;
        this.addEditQuestionary.impactIndicatorCategoryId = 0;
        this.addEditQuestionary.impactIndicatorSubCategoryId = 0;
        this.AddEditIIForm.formName = "";
        this.AddEditIIForm.formStatus = "undefined";
    }

    close(): void {
        this.clearFields();
        this.modal.hide();
        this.active = false;
    }

    log(event) {
    }

    save() {
        if (!this.isEdit) {
        this.AddEditIIForm.questionList = this.selected;
        }
        var thisObj = this;
        if (this.isEdit) {
        this.selected.forEach(function (value) {
            thisObj.AddEditIIForm.questionList.push(value.id);
        })
        }


        this._ImpactIndicatorFormServiceProxy.createEditImapactIndiactorForm(this.AddEditIIForm).subscribe(result => {
            this.SendMailForApprove(this.AddEditIIForm.formName);
            if (result == "Record alread Present!") {
                this.notify.warn(this.l(result));

            }
            if (result == "Form Update  sucessfully !" || result == "Form Added sucessfully !") {
                this.notify.info(this.l(result));
            }
            this.close();
            this.modalSave.emit(null);
        })
    }

    bindStatus() {
        if (this.rolename == "ACTIONAREAMANAGER" || this.rolename == "ADMIN" || this.rolename =="Action Area Manager") {
            this.Status = [
                { "id": "Under_Development", "name": "Under_Development" },
                { "id": "Approved", "name": "Approved" },
                { "id": "Disable", "name": "Disable" },
                { "id": "Review", "name": "Review" }
            ];
        } else {
            this.Status = [
                { "id": "Under_Development", "name": "Under_Development" }
            ];
        }

    }

    SendMailForApprove(FormName:any) {
        var thisObj = this; 
        this._ImpactIndicatorFormServiceProxy.getAllActionAreaManager().subscribe(result => {
            this.UserDetailsListForMail = result;
            this.emailContent = "Dear Action Area Manager,\n" + "<h4>Impact Indicator form Approve request </h4>" +
                "<STRONG> Form Name : </STRONG>" + FormName + "" +
                "<div></div><span class='text-muted'> \n Please go through the form and update status. </span>" +
                "<div>" + "<h3>Kindly visit for more details </h3>\n <div>https://pms.wotr.org.in/</div>";
            var thiscopy = thisObj;
            this.UserDetailsListForMail.forEach(function (values) {
                thiscopy._emailServiceProxy.mailSending(thiscopy.emailContent, 'Impact Indicator form request', values.emailId, null, null)
                    .subscribe((result) => {
                    });
            })
        });
    }
}