﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import {  GetAllImpactIndicatorIForms, ImpactIndicatorFormServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ImpactIndicatorFormModal } from './IICreateEditFormModal.component';

//import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';
import { IIQuestionListModal } from '../IIFormAndProgramMappings/QuestionList.Modal.component';


@Component({
    selector: 'ImpactIndicatorFormComponent',
    templateUrl: "./ImpactIndicatorCreateEditForm.component.html",
    styleUrls: ['./ImpactIndicatorCreateEditForm.component.less']

})


export class ImpactIndicatorFormComponent extends AppComponentBase {
    @ViewChild('IIFormModal') myModalView: ImpactIndicatorFormModal;
    @ViewChild('IIQuestionListModal') IIQuestionListModal: IIQuestionListModal;
    searchText: string = " ";
    IIForm: GetAllImpactIndicatorIForms[] = [];
    IIFormList: GetAllImpactIndicatorIForms[] = []
    timer: any;
    currentPage: number = 1;


    constructor(injector: Injector,
        private _ImpactIndicatorFormServiceProxy: ImpactIndicatorFormServiceProxy
    ) {
        super(injector)
    }

    getAllForm() {
        abp.ui.setBusy();
        this._ImpactIndicatorFormServiceProxy.getAllForm(this.searchText).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.length;

            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;

            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.IIFormList = result;
            this.IIForm = result;
            this.IIFormList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();

        })
    }


    searchPC(): void {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => { this.getAllForm() }, 1000);
    }

    editActionArea(p) {
        
        this.myModalView.show(p);
    }

    delete(p) {
        this.message.confirm(
            this.l('Delete Form', 'Form'),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._ImpactIndicatorFormServiceProxy.delete(p.id)
                        .subscribe((result) => {
                            this.notify.success(this.l('Successfully Deleted  Record'));
                            this.getAllForm();
                        });
                };

            });
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.IIForm.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.IIFormList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);

    }

}
