import { Component, ViewChild, Injector, Output, EventEmitter} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { UnitOfMeasuresServiceProxy, CreateOrEditUnitofMeasureDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'createOrEditUnitofMeasureModal',
    templateUrl: './create-or-edit-unitofMeasure-modal.component.html'
})
export class CreateOrEditUnitofMeasureModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal') modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    unitofMeasure: CreateOrEditUnitofMeasureDto = new CreateOrEditUnitofMeasureDto();

    constructor(
        injector: Injector,
        private _unitOfMeasuresServiceProxy: UnitOfMeasuresServiceProxy
    ) {
        super(injector);
    }

    show(unitofMeasureId?: number): void {
        if (!unitofMeasureId) { 
			this.unitofMeasure = new CreateOrEditUnitofMeasureDto();
			this.unitofMeasure.id = unitofMeasureId;
			this.active = true;
			this.modal.show();
        }
		else{
			this._unitOfMeasuresServiceProxy.getUnitofMeasureForEdit(unitofMeasureId).subscribe(unitofMeasureResult => {
				this.unitofMeasure = unitofMeasureResult;
				this.active = true;
				this.modal.show();
			});
		}  
    }

    save(): void {
			this.saving = true;
			this._unitOfMeasuresServiceProxy.createOrEdit(this.unitofMeasure)
			 .finally(() => { this.saving = false; })
			 .subscribe(() => {
			    this.notify.info(this.l('SavedSuccessfully'));
				this.close();
				this.modalSave.emit(null);
             });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}