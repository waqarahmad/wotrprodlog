﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { SubActionAreaListDto, SubActionAreaServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';
import { SubActionAreaModalComponent } from './subActionAreaModal.component';
import { ActivatedRoute } from '@angular/router';


declare var jQuery: any;



@Component({
    selector: 'subActionAreacomponent',
    templateUrl: "./subActionArea.component.html",
    styleUrls: ['./subActionArea.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class SubActionAreaComponent extends AppComponentBase {
    @ViewChild('modal') modal: ModalDirective;
    @ViewChild('subActionAreaModal') myModalView: SubActionAreaModalComponent;
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    getsubactionarea: SubActionAreaListDto[] = [];
    subActionList: SubActionAreaListDto[] = [];
    constructor(
        injector: Injector,
        private _subactionareaService: SubActionAreaServiceProxy,
    ) {
        super(injector);

    }



    ngOnInit() {
        this.getAllSubactionarea();
    }

    getAllSubAction() {

    }
   

    i: number = 1;
    getAllSubactionareaserach(): void {
        var input = (document.getElementById("txtsubactionArea") as HTMLInputElement).value;
        this.primengDatatableHelper.showLoadingIndicator();
        this._subactionareaService.getsearch(input).subscribe((result) => {
            this.primengDatatableHelper.records = result;
          //  this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.getsubactionarea = result;
            this.subActionList = result;
            this.getsubactionarea = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
        });
    }
    getAllSubactionarea(): void {
        this.primengDatatableHelper.showLoadingIndicator();
        this._subactionareaService.getAllSubActionAreaRecord().subscribe((result) => {
            this.primengDatatableHelper.records = result;
            this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.getsubactionarea = result;
            this.subActionList = result;
            this.getsubactionarea = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
        });
    }

    UpdateSubAction(e) {
        this.myModalView.show(e);
    }

    deleteSubActionArea(del) {
        this.message.confirm(
            this.l('SubActionAreaDeleteWarningMessage', del.name),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._subactionareaService.deleteSubactionArea(del.id)
                        .subscribe((result) => {
                            if (result == "Record Exist") {
                                this.notify.success(this.l('This record is already in used'));
                                this.getAllSubactionarea();
                            }
                            else {
                                this.notify.success(this.l('Successfully Deleted Sub Action Area Record'));
                                this.getAllSubactionarea();
                            }
                        });
                }
            })

       
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.subActionList.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.getsubactionarea = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);

    }
}