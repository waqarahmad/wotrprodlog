﻿import { Component, Injector, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { SubActionAreaServiceProxy, SubActionAreaListDto, ActionSubActionAreaMappingListDto, ActionAreasServiceProxy, ActionAreasListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';

import { MysortPipe } from '../../shared/common/mysort.pipe';//yo modified

declare var jQuery: any;


@Component({
    selector: 'subActionAreaModal',
    templateUrl: "./subActionAreaModal.component.html",
    styleUrls: ['./subActionAreaModal.component.less'],
})
export class SubActionAreaModalComponent extends AppComponentBase {
    @ViewChild('subActionAreaModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Output() ModalDirective

    subactionarea: SubActionAreaListDto;
    saving: boolean = false;
    active: boolean = false;
    actionAreaList: ActionAreasListDto[];

    @ViewChild('projectForm') eForm: NgForm;//yo

    constructor(
        injector: Injector,
        private _subactionareaService: SubActionAreaServiceProxy,
        private _actionAreaService: ActionAreasServiceProxy,

    ) {
        super(injector);
    }
    ngOnInit(): void {
       
    }
    ngAfterViewInit() {
        this.getAllActionArea();
    }
    getAllActionArea() {
        this._actionAreaService.getAllActionAreaRecord().subscribe(result => {
            this.actionAreaList = result;
        })
    }
    show(e): void {
        this.active = true;
        if (e != undefined) {
            $('#lblTitle').text('Edit Sub Action Area');
            this.subactionarea = e;
        }
        else {
            $('#lblTitle').text('Add Sub Action Area');
            this.subactionarea = new SubActionAreaListDto();
        }
        this.modal.show();

    }


    close(): void {
        this.modal.hide();
        this.active = false;      
    }

    save(): void {
        this._subactionareaService.creatOrUpdateSubActionArea(this.subactionarea)
            .finally(() => this.saving = false)
            .subscribe((result) => {
                if (result[0] == "Record alread Present!") {
                    this.notify.warn(this.l(result[0]));
                }
                if (result[0] == "SubActionArea Update  sucessfully !" || result[0] == "SubActionArea Added  sucessfully !") {
                    this.notify.info(this.l(result[0]));
                }
                this.close();
                this.modalSave.emit(null);
            })
    }


}