﻿import { Component, Injector, ViewEncapsulation, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';
import { ChekListsServiceProxy, CreateOrEditChekListDto } from '@shared/service-proxies/service-proxies';
import { NotifyService } from '@abp/notify/notify.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { TokenAuthServiceProxy } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Paginator } from 'primeng/components/paginator/paginator';
import { CheckListModalComponent1 } from './checkListModal.component';

@Component({
    selector: 'checkListComponent',
    templateUrl: "./checkList.component.html",
    styleUrls: ['./checkList.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CheckListComponent extends AppComponentBase {
    @ViewChild('modalCheckList') myModal: CheckListModalComponent1;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;
    filterText = '';
    checkList: CreateOrEditChekListDto[];
    check: CreateOrEditChekListDto[];
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;

    constructor(
        injector: Injector,
        private _chekListsServiceProxy: ChekListsServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.getChekLists();
    }
  
    reloadPage(): void {
        this.paginator.changePage(this.paginator.getPage());
    }

    editcheklist(p) {
        this.myModal.show(p);
    }

    deleteCheckList(del) {
        this.message.confirm(
            this.l('ChecklistWarningMessage', del.name),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._chekListsServiceProxy.delete(del.id)
                        .subscribe(() => {
                            this.notify.success(this.l('Successfully Deleted  Record'));
                            this.getChekLists();
                        });
                }
            });
    }

    getChekLists() {
        var input = (document.getElementById("txtchecklist") as HTMLInputElement).value;
        this._chekListsServiceProxy.getAll(input).subscribe(result => {
            this.primengDatatableHelper.records = result;
            this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.checkList = result;
            this.check = result;
            this.checkList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
        });
    }

    //getChekListsserach() {
    //    var input = (document.getElementById("txtchecklist") as HTMLInputElement).value;
    //    this._chekListsServiceProxy.getAll(input).subscribe(result => {
    //        this.primengDatatableHelper.records = result;
    //        this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
    //        this.primengDatatableHelper.totalRecordsCount = result.length;
    //        this.primengDatatableHelper.hideLoadingIndicator();
    //        this.checkList = result;
    //        this.check = result;
    //        this.checkList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
    //    });
    //}
    
    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.check.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.checkList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
}