﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { ChekListsServiceProxy, CreateOrEditChekListDto, CheklistandSublistDto, ActivitiyListDto, ActivityServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
declare var jQuery: any;
@Component({
    selector: 'modalCheckList',
    templateUrl: "./checkListModal.component.html",
    // styleUrls: ['./checkListModal.component.less'],
})
export class CheckListModalComponent1 extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('modalCheckList') modal: ModalDirective;
    active: boolean = false;
    saving: boolean = false;
    @ViewChild('projectForm') eForm: NgForm;
    chekList: CreateOrEditChekListDto = new CreateOrEditChekListDto();
    checkListItem: CheklistandSublistDto;
    activityDetails: ActivitiyListDto[];

    constructor(
        injector: Injector,
        private _chekListsServiceProxy: ChekListsServiceProxy,
        private _activityService: ActivityServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.getAllActivity("");

    }

    show(p): void {
        this.active = true;
        if (p != undefined) {
            $('#lblTitle').text('Edit Sub Activity');
            this.chekList = p;
        }
        else {
            this.chekList = new CreateOrEditChekListDto();
            this.chekList.iteam = [];
        }
        this.modal.show();
    }

    save(): void {
        this.saving = true;
        this.chekList.rating = 5;
        this._chekListsServiceProxy.createOrEdit(this.chekList)
            .finally(() => { this.saving = false; })
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    addNewChoice() {
        this.checkListItem = new CheklistandSublistDto();
        this.chekList.iteam.push(this.checkListItem);
    }

    removeChoice(index) {
        this.chekList.iteam.splice(index, 1);
    }

    getAllActivity(input) {
        this._activityService.getAllActivities("").subscribe(result => {
            this.activityDetails = result;
        });
    }
}