﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { RoleServiceProxy, ActionAreasServiceProxy, ProgramExpenseServiceProxy, ExpenseDto, ImplimantionChecklistServiceProxy, ImplementationListDto, ListActionSubactionListDto, Village, RequestAAPlasListDto,PrgRequestAAPlasServiceProxy, ProgramServiceProxy, ActionAreasListDto, ProgramInformationListDto, PrgVillageClusterListDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';
import { FileDownloadService } from '@shared/utils/file-download.service';



@Component({
    selector: 'expensesDetailscomponent',
    templateUrl: "./expensesDetails.component.html",
    styleUrls: ['./expensesDetails.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ExpensesDetailsComponent extends AppComponentBase {    
   
    selectedPermission = '';
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: ProgramInformationListDto[] = [];
    programdetails: ProgramInformationListDto[];

    programd: ProgramInformationListDto = new ProgramInformationListDto();
    VillageList: any = [];
    ActivityList: any = [];
    ComponentList: any = [];
    ImplementationList: ImplementationListDto[];
    getExpenses: ExpenseDto[];
    
    
    VillageClustorList: any = [];
    program: ProgramInformationListDto[];
    requestList: RequestAAPlasListDto[] = [];
    villageclustor: PrgVillageClusterListDto[];
    pro: any[];
    active: boolean = false;
    list: any = [];
    requestSingleYearList: RequestAAPlasListDto = new RequestAAPlasListDto();
    AddSubActionList: ListActionSubactionListDto;
    emailContent: any;
    saving: boolean = false;
    prgVilages: Village[] = [];
    Year: any = [];
    costyear : string;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,

        private _actionareaService: ActionAreasServiceProxy,
        private _programService: ProgramServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy,
        private _ImplimantionChecklistService: ImplimantionChecklistServiceProxy,
        private _ProgramExpensesServiceProxy: ProgramExpenseServiceProxy,


        private _roleService: RoleServiceProxy)
    {
        super(injector);
    }

    ngOnInit() {
        this.getAllProgram();
        this.programName = this._activatedRoute.snapshot.queryParams.ProgrameName;
    }    
  

    getAllProgram() {
        this._programService.getAllProgram().subscribe(result => {
            this.programdetails = result;
        });
    }
   
   
    
     
    onChangeState(event) {
        //this.getAllProgrambyid(event.target.value);
    }

    programName: string;
    locationName: string;
    managerName: string;
    requestStatus: number;
    costestimationyear: string;
    ActivityLevel: Number;
    activityname: string;
    Granamout: number;
    ddd: number[];
    Manager: number;
    startdate: string;
    EndDate: string;
    aaaa: boolean;
    activitylist: any = [];
    subactivitylist1: any = [];
    subactivitylist2: any = [];
    clustorname: any = [];
    Locationnames: any = [];

    subactivitylist3: any = [];

    subactivitylist4: any = [];

    subactivityname: string;
    getAllProgrambyid(programId, year) {
        this.subactivitylist1 = [];
        this.subactivitylist2 = [];
        this.subactivitylist4 = [];
        this.subactivitylist4 = [];
        this.clustorname = [];
        this.Locationnames = [];
        this.costyear = year;
        this._ProgramExpensesServiceProxy.getExpenseAsPerProgramIDForExcelReport("", programId, year, null, 1000, 0).subscribe(result => {
            this.getExpenses = result;
            this.costyear = result[0].expensesYear;
            this.programName = result[0].programName;
         
            result.forEach(o => {
                if (this.clustorname.indexOf(o.locationName) === -1) {
                    this.clustorname.push(o.locationName);
                    
                }
                if (this.Locationnames.indexOf(o.managerName) === -1) {
                    this.Locationnames.push(o.managerName);

                }
            });
           
            for (let i of result) {
                if (i.quarterId == 1) {
                    var a = { "ActivityName":i.activityName,  "SubactivityName": i.subActivityName, "ExpenseTitle": i.expenseTitle, "expenseDate": i.exDate, "expensesTypeName": i.expensesTypeName, "amount": i.amount, "remark": i.remark,"status":i.status}
                    this.subactivitylist1.push(a);
                }
            }
            for (let i of result) {
                if (i.quarterId == 2) {
                    var a = { "ActivityName": i.activityName, "SubactivityName": i.subActivityName, "ExpenseTitle": i.expenseTitle, "expenseDate": i.exDate, "expensesTypeName": i.expensesTypeName, "amount": i.amount, "remark": i.remark, "status": i.status }
                    this.subactivitylist2.push(a);
                }
            }
            for (let i of result) {
                if (i.quarterId == 3) {
                    var a = { "ActivityName": i.activityName, "SubactivityName": i.subActivityName, "ExpenseTitle": i.expenseTitle, "expenseDate": i.exDate, "expensesTypeName": i.expensesTypeName, "amount": i.amount, "remark": i.remark, "status": i.status }
                    this.subactivitylist3.push(a);
                }
            }
            for (let i of result) {
                if (i.quarterId == 4) {
                    var a = { "ActivityName": i.activityName, "SubactivityName": i.subActivityName, "ExpenseTitle": i.expenseTitle, "expenseDate": i.exDate, "expensesTypeName": i.expensesTypeName, "amount": i.amount, "remark": i.remark, "status": i.status }
                    this.subactivitylist4.push(a);
                }
            }
           
           
        })
    

    }
    QuarterName: string;
    QuarterId: number;
    DivideUnits(r, res, index) {
        this.QuarterId = index;
        this.QuarterName = r;
    }
    exportToExcel(programid): void {
        this._ProgramExpensesServiceProxy.createExcelDoc("Expenses", programid,this.costyear).subscribe(result => {
            this._fileDownloadService.downloadTempFile1(result);
        })


    }
    getcostestimationyear(programId) {
        this._PrgRequestAAPlasService.prgCostEstimationYears(programId).subscribe(result => {
            this.Year = result;

        });
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
   
}