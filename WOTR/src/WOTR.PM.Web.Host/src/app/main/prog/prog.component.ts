import { Component, OnInit, Injector, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { AppSessionService } from '@shared/common/session/app-session.service';
import { ProgramServiceProxy, ProgDtoList } from '@shared/service-proxies/service-proxies';
import { DataTable, Paginator } from 'primeng/primeng';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-prog',
  templateUrl: './prog.component.html',
  styleUrls: ['./prog.component.css'],
  animations: [appModuleAnimation()]
})
export class ProgComponent extends AppComponentBase implements OnInit {

    prg_id: number;
    @ViewChild('dataTable') dataTable: DataTable;
    @ViewChild('paginator') paginator: Paginator;

    programs: ProgDtoList[] = [];

    userId: number;
    filterText: "";
    primengDatatableHelper: any;


    constructor(
        injector: Injector,
        private _appSessionService: AppSessionService,
        private _ProgramService: ProgramServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.userId = this.appSession.user.id;  
    }

    getAllPrograms(event?: LazyLoadEvent) {
        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengDatatableHelper.showLoadingIndicator();

        
        
        this._ProgramService.getAllProgramsWithFilter(
            this.filterText,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event),
            this.primengDatatableHelper.getSkipCount(this.paginator, event)
        ).pipe(finalize(() => this.primengDatatableHelper.hideLoadingIndicator())).subscribe((result) => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
        });
    }

    deleteProgram(program: ProgDtoList, prg_name): void {
        this.message.confirm(
            this.l('Are you sure to delete the program ')+prg_name,
            isConfirmed => {
                if (isConfirmed) {
                    this._ProgramService.deleteProgram(program.id).subscribe(() => {
                        this.notify.info(this.l('SuccessfullyDeleted'));
                        _.remove(this.programs, program);
                        this.paginator.changePage(this.paginator.getPage());
                    });
                }
            }
        );
    }

}
