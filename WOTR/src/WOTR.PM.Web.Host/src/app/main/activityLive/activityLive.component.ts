﻿import { Component, Injector, ViewChild, ElementRef } from '@angular/core';
import { } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';


declare var jQuery: any;


@Component({
    selector: 'ActivityLive',
    templateUrl: "./activityLive.component.html",
    styleUrls: ['./activityLive.component.less'],
})
export class ActivityLiveComponent extends AppComponentBase {

    constructor(
        injector: Injector,
        private _router: Router,

    ) {
        super(injector);

    }
    funNextPage() {
        this._router.navigate(['/app/main/ProjectExpenses']);
    }

}