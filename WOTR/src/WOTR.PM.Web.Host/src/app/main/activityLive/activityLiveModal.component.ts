﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';

declare var jQuery: any;


@Component({
    selector: 'ActivityLiveModal',
    templateUrl: "./activityLiveModal.component.html",
    styleUrls: ['./activityLiveModal.component.less'],
})
export class ActivityLiveModalComponent extends AppComponentBase {
    @ViewChild('ActivityLiveModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('projectForm') eForm: NgForm;
    active: boolean = false;
    constructor(
        injector: Injector,

    ) {
        super(injector);

    }
    show(): void {
        this.active = true;  
        this.modal.show();
    }
    close(): void {
        this.modal.hide();
        this.active = false;
        this.eForm.reset();
    }
    getAllActionAreaRecord() {

    }
    save() {

    }
}