﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';


declare var jQuery: any;
 var imageArray: any[];
 var tempImageArray: any[];


@Component({
    selector: 'createProgram',
    templateUrl: "./createProgram.component.html",
    styleUrls: ['./createProgram.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CreateProgramComponent extends AppComponentBase {

    constructor(
        injector: Injector,
        private _router: Router,
    ) {
        super(injector);
    }  
    ngOnInit(): void {
        this.funClass();
        this.next();            
    }

    ngAfterViewInit(): void {
        this.funClass();
        this.next();      
        $("#programComponents").hide();
        $("#activitymapping").hide();
        $("#programCostEstimation").hide();
        $("#regionSpending").hide();
        $("#programInformation").show();
        $("#id1").css('color', '#3a9fe5');
    } 
    funClass() {
        $(document).ready(function () {
            var divs = $('.mydivs>div');
            var now = 0; // currently shown div
            divs.hide().first().show(); // hide all divs except first
            $("button[name=next]").click(function () {
                divs.eq(now).hide();
                now = (now + 1 < divs.length) ? now + 1 : 0;
                divs.eq(now).show(); // show next
            });
            $("button[name=prev]").click(function () {
                divs.eq(now).hide();
                now = (now > 0) ? now - 1 : divs.length - 1;
                divs.eq(now).show(); // show previous
            });
        });
    }
  
    prev() {
        // INFORMATION TAB IMAGE/TILE CODE
        /* vivek*/
        imageArray = [];
         tempImageArray = [];

         tempImageArray.push({ I: 'images/networkandsharing/networkandsharing-02.svg', H: 'images/networkandsharing/networkandsharing-02White.svg', Id: 1 });
         tempImageArray.push({ I: 'images/networkandsharing/networkandsharing-03.svg', H: 'images/networkandsharing/networkandsharing-03White.svg', Id: 2 });
         tempImageArray.push({ I: 'images/networkandsharing/networkandsharing-04.svg', H: 'images/networkandsharing/networkandsharing-04White.svg', Id: 3 });
         tempImageArray.push({ I: 'images/networkandsharing/networkandsharing-05.svg', H: 'images/networkandsharing/networkandsharing-05White.svg', Id: 4 });
         tempImageArray.push({ I: 'images/networkandsharing/networkandsharing-06.svg', H: 'images/networkandsharing/networkandsharing-06White.svg', Id: 5 });
       

        /* vivek*/
    }
    next() {
       
    }
    funProgramInformation() {
        //$("#mainContent").show();
        $("#programInformation").show();
        $("#programComponents").hide();
        $("#activitymapping").hide();
        $("#programCostEstimation").hide();
        $("#regionSpending").hide();
        $("#id1").css('color', '#3a9fe5');
    }
    funComponents() {
        $("mainContent").hide();
        $("#programComponents").show();
        $("#programCostEstimation").hide();
        $("#activitymapping").hide();
        $("#regionSpending").hide();
        $("#programInformation").hide();
    }
    funProgramCostEstimation() {
        $("mainContent").hide();
        $("#programComponents").hide();
        $("#programCostEstimation").show();
        $("#activitymapping").hide();
        $("#regionSpending").hide();
        $("#programInformation").hide();
    }
    funActivityMaping() {
        $("mainContent").hide();
        $("#programComponents").hide();
        $("#programCostEstimation").hide();
        $("#activitymapping").show();
        $("#regionSpending").hide();
        $("#programInformation").hide();
    }
    funRegionSpending() {
        $("mainContent").hide();
        $("#programComponents").hide();
        $("#programCostEstimation").hide();
        $("#activitymapping").hide();
        $("#regionSpending").show();
        $("#programInformation").hide();
    }
    funRequestAAPla() {
        $("#programInformation").hide();
    }
   

}