﻿import { Component, Injector, ViewChild, EventEmitter, Output } from '@angular/core';
import { TenantDashboardServiceProxy, DashboardServiceProxy, AllChartDto, DemandWiseProject } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { ProjectWiseDemandAdminModalComponent } from './ProjectWise_Demand.Modal.component';
declare var jQuery: any;


@Component({
    selector: 'StateWiseDemandAdminModal',
    templateUrl: "./StateWise_Demand.modal.component.html"
})
export class StateWiseDemandAdminModalComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('StateWiseDemandAdminModal') modal: ModalDirective;
    @ViewChild('ProjectWiseDemandAdminModal') ProjectWiseDemandAdminModal: ProjectWiseDemandAdminModalComponent;
    data: AllChartDto = new AllChartDto();
    active: boolean = false;
    demandWiseProjectData: DemandWiseProject[] = [];
    constructor(
        injector: Injector,
        private _dashboardService: DashboardServiceProxy) {
        super(injector);
    }

    show(data): void {
        var stateId = data.stateId;
        this.active = true;
        this._dashboardService.getDemandAndDemandRemaingByStateId(stateId).subscribe(result => {
            this.demandWiseProjectData = result;
            abp.ui.clearBusy();
        });
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}