﻿import { Component, Injector, ViewChild, EventEmitter, Output } from '@angular/core';
import { AllChartDto, TenantDashboardServiceProxy, DashboardServiceProxy, ProgramManagerAmountSanction } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
declare var jQuery: any;


@Component({
    selector: 'ProjectWiseDemandAdminModal',
    templateUrl: "./ProjectWise_Demand.Modal.component.html"
})

export class ProjectWiseDemandAdminModalComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('ProjectWiseDemandAdminModal') modal: ModalDirective;
    data: AllChartDto = new AllChartDto();
    active: boolean = false;
    projectWiseAmountSanctionData: ProgramManagerAmountSanction[] = [];
    constructor(
        injector: Injector,
        private _dashboardService: DashboardServiceProxy) {
        super(injector);

    }

    show(data): void {
        var programId = data.programId;
        this.active = true;
        this._dashboardService.getAmountSanctionByProjectId(programId).subscribe(result => {
            this.projectWiseAmountSanctionData = result;
            abp.ui.clearBusy();
        });
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}