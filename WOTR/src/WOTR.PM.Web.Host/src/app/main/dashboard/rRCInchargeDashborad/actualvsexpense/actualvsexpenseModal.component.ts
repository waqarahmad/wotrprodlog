﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output, NgZone } from '@angular/core';
import { ActivityServiceProxy, ActivitiyListDto, PrgRequestAAPlasServiceProxy, TenantDashboardServiceProxy, UnitofMeasureDto, ProjectWiseYearlyActivity, AllChartDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
import { forEach } from '@angular/router/src/utils/collection';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import { YearWiseAchievementComponentModal } from '../yearWiseAchievementModal/yearWiseAchievementComponentModal.component';
declare var jQuery: any;


@Component({
    selector: 'Actualvsexpense',
    templateUrl: "./actualvsexpenseModal.component.html"
})
export class ActualvsexpenseModalsComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('Actualvsexpense') modal: ModalDirective;
    @ViewChild('yearWiseAchievementmodals') yearWiseAchievementmodals: YearWiseAchievementComponentModal;
    active: boolean = false;
    saving: boolean = false;
    data: AllChartDto = new AllChartDto();
    dataZ: any;
    dataZs: any;
    dataZsss: any;
    dataZsssa: any;
    len: number;
    type: number;
    labelss: any = [];
    datasetss: any = [];
    role: any;
    constructor(private zone: NgZone,
        private _dashboardService: TenantDashboardServiceProxy,
        injector: Injector,
    ) {
        super(injector);

    }


    show(roleName): void {
        this.role = roleName;
        if (roleName == 'RRC Incharge') {
            this.active = true;
            this._dashboardService.rRCInchargePhysVsActperformance().subscribe(result => {

                this.data.programManagerProgramStateWiseBudgetBarChart = result.programManagerProgramStateWiseBudgetBarChart;
                this.dataZ = this.data.programManagerProgramStateWiseBudgetBarChart;
                this.len = this.dataZ.length
                var graphLen = <HTMLInputElement>document.getElementById("RRCchartdivModal");
                if (this.len > 3) {
                    graphLen.style.width = "300%";
                } else {
                    graphLen.style.width = "100%";
                }

                this.drawCharts(this.dataZ);
                this.modal.show();

            });
            //if (pId == 3) {
            //    this.dataZ = data.programManagerBarChart;

            //    this.drawCharts(this.dataZ);
            //}
            //this.type = pId;
            //this.data = data;
        } else if (roleName == 'Project Manager') {

            this.active = true;
            this._dashboardService.rRCInchargebudget().subscribe(result => {

                this.data.programManagerProgramStateWiseBudgetBarChart = result.programManagerProgramStateWiseBudgetBarChart;
                this.dataZ = this.data.programManagerProgramStateWiseBudgetBarChart;
                this.len = this.dataZ.length
                var graphLen = <HTMLInputElement>document.getElementById("RRCchartdivModal");
                if (this.len > 3) {
                    graphLen.style.width = "300%";
                } else {
                    graphLen.style.width = "100%";
                }

                this.drawCharts(this.dataZ);
                this.modal.show();

            });
            //if (pId == 3) {
            //    this.dataZ = data.programManagerBarChart;

            //    this.drawCharts(this.dataZ);
            //}
            //this.type = pId;
            //this.data = data;

        }


    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
    abc: string;
    drawCharts(dataZ) {
        
        this.zone.runOutsideAngular(() => {
            Promise.all([
                import("@amcharts/amcharts4/core"),
                import("@amcharts/amcharts4/charts"),
                import("@amcharts/amcharts4/themes/animated")
            ])
                .then(modules => {
                    let t = this;
                    setTimeout(function (this) {
                        const am4core = modules[0];
                        const am4charts = modules[1];
                        const am4themes_animated = modules[2].default;
                        am4core.options.commercialLicense = true;
                        am4core.useTheme(am4themes_animated);

                        let chart = am4core.create("RRCchartdivModal", am4charts.XYChart);
                        dataZ.forEach(
                            xx => {
                                xx.datasets.forEach(e => {
                                    chart.data.push({
                                        "year": xx.labels[0],
                                        "budget": xx.datasets[0].data[0],
                                        "actualPerformance": xx.datasets[1].data[0],
                                    })
                                })
                            })

                        // Create axes
                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

                        categoryAxis.dataFields.category = "year";
                        categoryAxis.title.text = "Program";
                        categoryAxis.renderer.grid.template.location = 0;
                        categoryAxis.renderer.minGridDistance = 20;
                        categoryAxis.renderer.cellStartLocation = 0.1;
                        categoryAxis.renderer.cellEndLocation = 0.9;

                        categoryAxis.renderer.labels.template

                            .cursorOverStyle = [
                                {
                                    "property": "cursor",
                                    "value": "pointer"
                                }
                            ];

                        let label = categoryAxis.renderer.labels.template;
                        label.wrap = true;
                        label.maxWidth = 120;
                        //label.truncate = true;
                        //label.tooltipText = "{category}";
                        //categoryAxis.tooltip.label.maxWidth = 200;
                        //categoryAxis.tooltip.label.wrap = true;
                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        valueAxis.min = 0;
                        valueAxis.title.text = " ";
                        let dd = t.yearWiseAchievementmodals;
                        let roleName = t.role;
                        categoryAxis.renderer.labels.template.events.on("hit", function (ev) {
                            
                            dd.show(ev.target, roleName);
                        });
                        // Create series
                        function createSeries(field, name) {
                            var series = chart.series.push(new am4charts.ColumnSeries());
                            series.dataFields.valueY = field;
                            series.dataFields.categoryX = "year";
                            series.name = name;
                            series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
                            //series.stacked = stacked;
                            series.columns.template.width = am4core.percent(95);

                        }

                        createSeries("budget", "Budget");
                        createSeries("actualPerformance", "ActualPerformance");
                    }, 200)

                })
                .catch(e => {
                    console.error("Error when creating chart", e);
                });
        });
    }

}