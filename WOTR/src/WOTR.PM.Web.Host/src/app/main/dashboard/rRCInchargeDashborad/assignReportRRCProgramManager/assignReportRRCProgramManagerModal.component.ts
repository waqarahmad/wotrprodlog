﻿import { Component, Injector, ViewChild, EventEmitter, Output, NgZone } from '@angular/core';
import { TenantDashboardServiceProxy, ProgramManagerImplemenationPlanCheckListWiseBudget } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { SubActionAreaRRCProgramManagerModalsComponent } from '../subActionAreaRRCProgramManager/subActionAreaRRCProgramManagerModal.component';
import { parse } from 'path';
declare var jQuery: any;


@Component({
    selector: 'AssignReportRRCModals',
    templateUrl: "./assignReportRRCProgramManagerModal.component.html"
})
export class AssignReporRRCtModalsComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('assignReport') modal: ModalDirective;
    @ViewChild('subActionAreaRRCRRCModals') subActionAreaRRCRRCModals: SubActionAreaRRCProgramManagerModalsComponent;

    data: ProgramManagerImplemenationPlanCheckListWiseBudget[] = [];
    Record: ProgramManagerImplemenationPlanCheckListWiseBudget[] = [];
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    active: boolean = false;
    piechartdata: any;
    prgramName: string;
    year: string;
    len: any;

    constructor(
        injector: Injector, private zone: NgZone,
        private _dashboardService: TenantDashboardServiceProxy) {
        super(injector);
    }

    show(projectname: string, data): void {
        this.active = true;
        var projectYear = data.currentText;
        this.prgramName = projectname;
        this.year = data.currentText;
        this.active = true;
        abp.ui.setBusy();
        this._dashboardService.yearWiseRRCAssignedReport(projectname, projectYear).subscribe(result => {
            this.primengDatatableHelper.records = result.programManagerImplemenationPlanCheckListWiseBudgets;
            this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.programManagerImplemenationPlanCheckListWiseBudgets.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.Record = result.programManagerImplemenationPlanCheckListWiseBudgets;
            this.Record = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            this.piechartdata = result.actionAReaRRCdtos;
            this.len = this.piechartdata.length;
            var ProjectNameWidth = <HTMLInputElement>document.getElementById('chartdiv');
            if (this.len > 3) {

                ProjectNameWidth.style.width = "300%";
            }
            else {
                ProjectNameWidth.style.width = "100%";
            }
            this.piecharts();
            abp.ui.clearBusy();

        });

        this.data = data;
        this.modal.show();
    }

    piecharts() {
        this.zone.runOutsideAngular(() => {            Promise.all([                import("@amcharts/amcharts4/core"),                import("@amcharts/amcharts4/charts"),                import("@amcharts/amcharts4/themes/animated"),            ])                .then(modules => {                    const am4core = modules[0];                    const am4charts = modules[1];                    const am4themes_animated = modules[2].default;                    am4core.options.commercialLicense = true;                    am4core.useTheme(am4themes_animated);
                    // Themes end

                    // Create chart instance
                    var chart = am4core.create("chartdiv", am4charts.PieChart);

                    // Add and configure Series
                    var pieSeries = chart.series.push(new am4charts.PieSeries());
                    pieSeries.dataFields.value = "funderContribution";
                    pieSeries.dataFields.category = "actionAreaName";

                    // Let's cut a hole in our Pie chart the size of 30% the radius
                    chart.innerRadius = am4core.percent(30);

                    // Put a thick white border around each Slice
                    pieSeries.slices.template.stroke = am4core.color("#fff");
                    pieSeries.slices.template.strokeWidth = 2;
                    pieSeries.slices.template.strokeOpacity = 1;
                    pieSeries.slices.template
                        // change the cursor on hover to make it apparent the object can be interacted with
                        .cursorOverStyle = [
                            {
                                "property": "cursor",
                                "value": "pointer"
                            }
                        ];
                    let label = pieSeries.labels.template;
                    label.wrap = true;
                    label.maxWidth = 200;
                    label.truncate = true;
                    label.tooltipText = "{category}";

                    pieSeries.tooltip.label.maxWidth = 200;
                    pieSeries.tooltip.label.wrap = true;

                    pieSeries.slices.template.events.on("hit", function (ev) {
                        this.subActionAreaRRCRRCModals.show(this.prgramName, this.year, ev.target.dataItem.properties.category);
                    }, this);
                    //pieSeries.labels.template.events.on("hit", function (ev) {

                    //    //alert(ev.event.target);
                    //    this.subActionAreaRRCRRCModals.show(this.prgramName, this.year, ev.target.currentText);
                    //}, this);
                    pieSeries.alignLabels = true;
                    pieSeries.labels.template.bent = true;
                    pieSeries.labels.template.radius = 2;
                    pieSeries.labels.template.padding(0, 0, 0, 0);
                    //pieSeries.labels.template
                    //    // change the cursor on hover to make it apparent the object can be interacted with
                    //    .cursorOverStyle = [
                    //        {
                    //            "property": "cursor",
                    //            "value": "pointer"
                    //        }
                    //    ];
                    pieSeries.ticks.template.disabled = false;

                    // Create a base filter effect (as if it's not there) for the hover to return to
                    var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
                    shadow.opacity = 0;

                    // Create hover state
                    var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

                    // Slightly shift the shadow and make it more prominent on hover
                    var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
                    hoverShadow.opacity = 0.7;
                    hoverShadow.blur = 5;

                    // Add a legend
                    chart.legend = new am4charts.Legend();
                    chart.legend.labels.template.maxWidth = 150;
                    chart.legend.labels.template.truncate = true;

                    this.piechartdata.forEach(
                        xx => {

                            chart.data.push({
                                "actionAreaName": xx.actionAreaName,
                                "funderContribution": xx.funderContribution,


                            })
                        })                })                .catch(e => {                    console.error("Error when creating chart", e);                });        });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = Number(p.rows);
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
}