﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output, NgZone } from '@angular/core';
import { ActivityServiceProxy, ActivitiyListDto, PrgRequestAAPlasServiceProxy, UnitofMeasureDto, ProjectWiseYearlyActivity, AllChartDto, TenantDashboardServiceProxy, ProgramManagerImplemenationPlanCheckListWiseBudget } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms'; 

declare var jQuery: any;


@Component({
    selector: 'subActionAreaRRCProgramManagerRRCModals',
    templateUrl: "./subActionAreaRRCProgramManagerModal.component.html"
})
export class SubActionAreaRRCProgramManagerModalsComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('SubActionAreaReport') modal: ModalDirective;  
    data: ProgramManagerImplemenationPlanCheckListWiseBudget[] = []; 
    Record: ProgramManagerImplemenationPlanCheckListWiseBudget[] = []; 
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    active: boolean = false;
    piechartdata1: any;
    len: any;
    constructor(
        injector: Injector, private zone: NgZone,
        private _dashboardService: TenantDashboardServiceProxy,
    ) {
        super(injector);

    }

    show(projectname: string, year: string, data): void { 
       
        this.active = true;
        var actionareanm = data;
        this.active = true;
        this._dashboardService.yearWiseRRCActionAReaSubActionbudget(projectname,year, actionareanm).subscribe(result => {
            this.piechartdata1 = result;
            this.len = this.piechartdata1.length;
            var ProjectNameWidth = <HTMLInputElement>document.getElementById('chartdiv1');
            if (this.len > 3) {

                ProjectNameWidth.style.width = "300%";
            }
            else {
                ProjectNameWidth.style.width = "100%";
            }
            this.piechartss();
            
           
        });

       

        this.data = data;
        this.modal.show();
    }
    piechartss() {
       
        this.zone.runOutsideAngular(() => {            Promise.all([                import("@amcharts/amcharts4/core"),                import("@amcharts/amcharts4/charts"),                import("@amcharts/amcharts4/themes/animated"),            ])                .then(modules => {                    const am4core = modules[0];                    const am4charts = modules[1];                    const am4themes_animated = modules[2].default;                    am4core.options.commercialLicense = true;                    //am4core.useTheme(am4themes_animated);                    am4core.useTheme(am4themes_animated);
                    // Themes end

                    // Create chart instance
                    var chart1 = am4core.create("chartdiv1", am4charts.PieChart);

                    // Add and configure Series
                    var pieSeries = chart1.series.push(new am4charts.PieSeries());
                    pieSeries.dataFields.value = "budget";
                    pieSeries.dataFields.category = "subactionareaname";

                    // Let's cut a hole in our Pie chart the size of 30% the radius
                    chart1.innerRadius = am4core.percent(30);

                    // Put a thick white border around each Slice
                    pieSeries.slices.template.stroke = am4core.color("#fff");
                    pieSeries.slices.template.strokeWidth = 2;
                    pieSeries.slices.template.strokeOpacity = 1;
                    //pieSeries.slices.template
                    //    // change the cursor on hover to make it apparent the object can be interacted with
                    //    .cursorOverStyle = [
                    //        {
                    //            "property": "cursor",
                    //            "value": "pointer"
                    //        }
                    //    ];

                    pieSeries.alignLabels = true;
                    pieSeries.labels.template.bent = true;
                    pieSeries.labels.template.radius = 2;
                    pieSeries.labels.template.padding(0, 0, 0, 0);
                    //pieSeries.labels.template
                    //    // change the cursor on hover to make it apparent the object can be interacted with
                    //    .cursorOverStyle = [
                    //        {
                    //            "property": "cursor",
                    //            "value": "pointer"
                    //        }
                    //    ];
                    let label = pieSeries.labels.template;
                    label.wrap = true;
                    label.maxWidth = 300;
                    label.truncate = true;

                    pieSeries.tooltip.label.maxWidth = 200;
                    pieSeries.tooltip.label.wrap = true;
                    pieSeries.ticks.template.disabled = false;

                    // Create a base filter effect (as if it's not there) for the hover to return to
                    var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
                    shadow.opacity = 0;

                    // Create hover state
                    var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

                    // Slightly shift the shadow and make it more prominent on hover
                    var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
                    hoverShadow.opacity = 0.7;
                    hoverShadow.blur = 5;

                    // Add a legend
                    chart1.legend = new am4charts.Legend();
                    chart1.legend.labels.template.maxWidth = 150;
                    chart1.legend.labels.template.truncate = true;

                    this.piechartdata1.forEach(
                        xx => {

                            chart1.data.push({
                                "subactionareaname": xx.subactionareaname,
                                "budget": xx.budget,


                            })
                        })

                })                .catch(e => {                    console.error("Error when creating chart", e);                });        });
    }
    close(): void {
        this.active = false; 
        this.modal.hide(); 
    }
    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }
}