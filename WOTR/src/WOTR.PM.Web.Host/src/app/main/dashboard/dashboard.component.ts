import { Component, AfterViewInit, Injector, ViewEncapsulation, OnDestroy, ViewChild, NgZone } from '@angular/core';
import {
    DashboardServiceProxy, TenantDashboardServiceProxy, ProgramFundsServiceProxy,
    AllChartDto, Clustorwisetotalbudgettotalexpense, Totalbudgettotalexpense, ActionAreasListDto,
    SummryAllProjects, LatestActivityAssignReport, Budgetachievmentdto, BudgetProjectMembersByStateDto, UserDetailsDto, AdminDashboardData
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppSalesSummaryDatePeriod } from '@shared/AppEnums';
declare let d3, Datamap: any;
import * as _ from 'lodash';
import { ProgramManagerSummaryModalsComponent } from './programManagerDashbord/modalInfo/programManagerSummaryModal.component';
import { StateWiseDemandModalComponent } from './programManagerDashbord/demandSummary/state-wise-demand-modal.component';
import { StateWiseDemandAdminModalComponent } from './AdminDashboard/StateWise_Demand.modal.component';
import { ActualvsexpenseModalsComponent } from './rRCInchargeDashborad/actualvsexpense/actualvsexpenseModal.component';
import { ProgramWiseDemandModalComponent } from './programManagerDashbord/demandSummary/program-wise-demand-modal.component';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import * as CanvasJS from 'assets/vendor/canvasjs.min';

@Component({
    templateUrl: "./dashboard.component.html",
    styleUrls: ['./dashboard.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class DashboardComponent extends AppComponentBase implements AfterViewInit, OnDestroy {
    appSalesSummaryDateInterval = AppSalesSummaryDatePeriod;
    selectedSalesSummaryDatePeriod: any = AppSalesSummaryDatePeriod.Daily;
    dashboardHeaderStats: DashboardHeaderStats;
    salesSummaryChart: SalesSummaryChart;
    regionalStatsWorldMap: RegionalStatsWorldMap;
    generalStatsPieChart: GeneralStatsPieChart;
    dailySalesLineChart: DailySalesLineChart;
    profitSharePieChart: ProfitSharePieChart;
    memberActivityTable: MemberActivityTable;
    userId: number;
    roleName: string = "";
    programManagerChartInfo: AllChartDto = new AllChartDto();
    actionarea: ActionAreasListDto[] = [];
    actionareas: ActionAreasListDto = new ActionAreasListDto();
    summrryallprojects: SummryAllProjects[] = [];
    budgetvsactualperm: Budgetachievmentdto[] = [];
    latestActivityAssignReport: LatestActivityAssignReport[] = [];
    Record: LatestActivityAssignReport[] = [];
    totalbudgettotalexpense: Totalbudgettotalexpense[] = [];
    clustorwisetotalbudgettotalexpense: Clustorwisetotalbudgettotalexpense[] = [];
    Year: any = [];
    currentPage: number = 1;
    data1: AllChartDto = new AllChartDto();
    dataZ: any;
    dataRRCIncharge: any;
    dataPM: any;
    dataZs: any;
    dataZsss: any;
    dataZsssa: any;
    data: AllChartDto = new AllChartDto();
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    states = [];
    budgetPerformance: AdminDashboardData = new AdminDashboardData();
    demandSummaryTable1: AdminDashboardData = new AdminDashboardData();
    demandSummaryTable2 = [];
    demandSummaryTable3 = [];
    rrcWiseActionChart = [];
    projectWiseChart = [];
    UserDetailsDto: UserDetailsDto[] = [];
    stateDash1: any;
    BudgetProjectMembersByStateDto: BudgetProjectMembersByStateDto = new BudgetProjectMembersByStateDto()
    TotalBudget: any;
    TotalExpense: any;
    CurrentFinancialYear: string = "";


    len: number;
    @ViewChild('SummaryModals') SummaryModals: ProgramManagerSummaryModalsComponent;
    @ViewChild('ActualvsExpenseModals') ActualvsExpenseModals: ActualvsexpenseModalsComponent;
    @ViewChild('StateWiseDemandModal') StateWiseDemandModal: StateWiseDemandModalComponent;
    @ViewChild('StateWiseDemandAdminModal') StateWiseDemandAdminModal: StateWiseDemandAdminModalComponent;
    @ViewChild('ProgramWiseDemandModal') ProgramWiseDemandModal: ProgramWiseDemandModalComponent;


    constructor(
        private zone: NgZone,
        injector: Injector,
        private _dashboardService: TenantDashboardServiceProxy,
        private _dashboardServiceProxy: DashboardServiceProxy,
        private _projectFundGetUser: ProgramFundsServiceProxy,
        private AmCharts: AmChartsService
    ) {

        super(injector);
        this.dashboardHeaderStats = new DashboardHeaderStats();
        this.regionalStatsWorldMap = new RegionalStatsWorldMap(this._dashboardService, 'worldmap');
        this.generalStatsPieChart = new GeneralStatsPieChart(this._dashboardService);
        this.dailySalesLineChart = new DailySalesLineChart(this._dashboardService, '#m_chart_daily_sales');
        this.profitSharePieChart = new ProfitSharePieChart(this._dashboardService, '#m_chart_profit_share');
        this.memberActivityTable = new MemberActivityTable(this._dashboardService);
        this.userId = this.appSession.userId;
        this.getRoleName(this.userId);
        this.bindStates();
    }

    bindStates() {
        this._dashboardServiceProxy.fetchAllStates().subscribe(result => {
            this.states = result;
            this.stateDash1 = result.map(({ id }) => id);
            this.bindActionSubActionByStates();
            this.bindDetailsByState();
            this.bindUserDeatilsByState();
            this.bindProgramBudgetAndExpensesByStateWise();
            this.bindDemandSummaryByStates();
            this.bindContributionsByStates();
            this.bindBudgetByStates();
            this.bindRRCAndProgramWiseExpense();
        });
        this.CurrentFinancialYear = this.getCurrentFinYear();

    }

    getCurrentFinYear() {
        var fiscalyear = "";
        var today = new Date();
        if ((today.getMonth() + 1) <= 3) {
            fiscalyear = (today.getFullYear() - 1) + "-" + today.getFullYear()
        } else {
            fiscalyear = today.getFullYear() + "-" + (today.getFullYear() + 1)
        }
        return fiscalyear
    }

    ChangeByState() {
        this.bindDetailsByState();
        this.bindUserDeatilsByState();
        this.bindProgramBudgetAndExpensesByStateWise();
        this.bindBudgetByStates();
        this.bindDemandSummaryByStates();
        this.bindActionSubActionByStates();
        this.bindContributionsByStates(); // added on demand
        this.bindRRCAndProgramWiseExpense(); // added on demand
    }

    totalProjectBudget: string = "0";

    bindRRCAndProgramWiseExpense() {
        this._dashboardServiceProxy.getRRCAndProgramWiseExpense(this.stateDash1).subscribe(result => {
            if (!result) {
                result = [];
            }
            var newData = [], dates = [];
            result.map((item) => {
                var index = dates.indexOf(item.locationId);
                if (index > -1) {
                    newData[index].amount = +newData[index].amount + +item.amount;
                } else {
                    newData.push(item);
                    dates.push(item.locationId);
                }
            });

            var newData1 = [], dates1 = [];
            result.map((item) => {
                var index = dates1.indexOf(item.programId);
                if (index > -1) {
                    newData1[index].amount = +newData1[index].amount + +item.amount;
                } else {
                    newData1.push(item);
                    dates1.push(item.programId);
                }
            });

            let arr = [];
            let total = 0;
            if (newData1.length > 0) {
                total = newData1.map(item => item.amount).reduce((a, b) => { return parseFloat(a) + parseFloat(b) });
                newData1.forEach((obj, index) => {
                    if (((100 * obj.amount) / total) < 5) {
                        arr.push(obj.amount);
                        newData1.splice(index, 1);
                    }
                });
            }
            if (arr.length > 0) {
                total = arr.reduce((a, b) => { return parseFloat(a) + parseFloat(b) });
                newData1.push({
                    programName: 'Others',
                    amount: total
                });
            }

            var newData2 = [], dates2 = [];
            result.map((item) => {
                var index = dates2.indexOf(item.actionAreaId);
                if (index > -1) {
                    newData2[index].amount = +newData2[index].amount + +item.amount;
                } else {
                    newData2.push(item);
                    dates2.push(item.actionAreaId);
                }
            });

            this.AmCharts.makeChart("RrcWiseActionChart", {
                type: "pie",
                theme: "light",
                dataProvider: newData,
                valueField: "amount",
                titleField: "locationName",
                balloon: {
                    fixedPosition: true
                }
            });

            this.AmCharts.makeChart("ProjectWiseChart", {
                type: "pie",
                theme: "light",
                dataProvider: newData1,
                valueField: "amount",
                titleField: "programName",
                balloon: {
                    fixedPosition: true
                }
            });

            this.AmCharts.makeChart("ActionAreaWiseChart", {
                type: "pie",
                theme: "light",
                dataProvider: newData2,
                valueField: "amount",
                titleField: "actionAreaName",
                balloon: {
                    fixedPosition: true
                }
            });
        });
    }

    bindActionSubActionByStates() {
        this._dashboardServiceProxy.getActionSubActionByStates(this.stateDash1).subscribe(result => {
            if (!result) {
                result = [];
            }
            var newData = [], dates = [];
            result.map((item) => {
                var index = dates.indexOf(item.actionAreaId);
                if (index > -1) {
                    newData[index].value = ++newData[index].value;
                } else {
                    newData.push(item);
                    dates.push(item.actionAreaId);
                }
            });

            var newData1 = [], dates1 = [];
            result.map((item) => {
                var index = dates1.indexOf(item.subactionAreaId);
                if (index > -1) {
                    newData1[index].value = ++newData1[index].value;
                } else {
                    newData1.push(item);
                    dates1.push(item.subactionAreaId);
                }
            });

            let arr = [];
            let total = 0;
            if (newData1.length > 0) {
                total = newData1.map(item => item.value).reduce((a, b) => { return parseFloat(a) + parseFloat(b) });
                newData1.forEach((obj, index) => {
                    if (((100 * obj.value) / total) < 5) {
                        arr.push(obj.value);
                        newData1.splice(index, 1);
                    }
                });
            }
            if (arr.length > 0) {
                total = arr.reduce((a, b) => { return parseFloat(a) + parseFloat(b) });
                newData1.push({
                    subaction: 'Others',
                    value: total
                });
            }

            this.AmCharts.makeChart("AreaActionChart", {
                type: "pie",
                theme: "light",
                dataProvider: newData,
                valueField: "value",
                titleField: "actionName",
                balloon: {
                    fixedPosition: true
                }
            });

            this.AmCharts.makeChart("SubActionChart", {
                type: "pie",
                theme: "light",
                dataProvider: newData1,
                valueField: "value",
                titleField: "subaction",
                balloon: {
                    fixedPosition: true
                }
            });
        });
    }

    bindBudgetByStates() {
        this._dashboardServiceProxy.getBudgetByStates(this.stateDash1).subscribe(result => {
            if (result) {
                this.totalProjectBudget = result;
            } else {
                this.totalProjectBudget = "0";
            }
        });
    }

    bindContributionsByStates() {
        this._dashboardServiceProxy.getContributionsByStates(this.stateDash1).subscribe(result => {
            if (!result) {
                result = [];
            }
            var a1 = [];
            var a2 = [];
            var a3 = [];
            var a4 = [];

            for (let p1 of this.stateDash1) {
                var s = this.states.filter(t => t.id == p1);

                let contriBution = result.filter(t => t.stateId == p1);
                let communityContributionTotal = 0;
                let funderContribution = 0;
                let otherContribution = 0;

                if (contriBution.length > 0) {
                     communityContributionTotal = contriBution.map(item => +item.communityContribution).reduce((a, b) => { return a + b });
                     funderContribution = contriBution.map(item => +item.funderContribution).reduce((a, b) => { return a + b });
                     otherContribution = contriBution.map(item => +item.otherContribution).reduce((a, b) => { return a + b });
                }

                a1.push({ label: s[0].name, y: communityContributionTotal });
                a2.push({ label: s[0].name, y: funderContribution });
                a3.push({ label: s[0].name, y: otherContribution });

                let u = this.budgetPerformance.budgetAndExpenseByStateDto.filter(t => t.stateId == p1);
                let total = u.map(item => +item.totalBudget).reduce((a, b) => { return a + b });
                a4.push({ label: s[0].name, y: total });
            }

            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                toolTip: {
                    shared: true
                },
                legend: {
                    cursor: "pointer",
                    itemclick: toggleDataSeries
                },
                axisY2: {
                    display: false
                },
                data: [{
                    type: "column",
                    name: "Community Contribution",
                    legendText: "Community Contribution",
                    showInLegend: true,
                    dataPoints: a1
                },
                {
                    type: "column",
                    name: "Funder Contribution",
                    legendText: "Funder Contribution",
                    showInLegend: true,
                    dataPoints: a2
                },
                {
                    type: "column",
                    name: "Other Contribution",
                    legendText: "Other Contribution",
                    showInLegend: true,
                    dataPoints: a3
                }
                    , {
                    type: "column",
                    name: "Budget",
                    legendText: "Budget",
                    showInLegend: true,
                    dataPoints: a4
                }
                ]
            });
            chart.render();

            function toggleDataSeries(e) {
                if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                    e.dataSeries.visible = false;
                }
                else {
                    e.dataSeries.visible = true;
                }
                chart.render();
            }

        });
    }

    bindDemandSummaryByStates() {
        this._dashboardServiceProxy.getDemandSummaryByStates(this.stateDash1).subscribe(result => {
            this.demandSummaryTable1 = result;
        });
    }

    bindProgramBudgetAndExpensesByStateWise() {
        this._dashboardServiceProxy.getBudgetAndExpenseByStates(this.stateDash1).subscribe(result => {
            this.budgetPerformance = result;
        });
    }

    bindDetailsByState() {
        this._dashboardServiceProxy.getBudgetProjectMembersdetailsAllStates(this.stateDash1).subscribe(result => {
            this.BudgetProjectMembersByStateDto = result;
        });
    }


    bindUserDeatilsByState() {
        this._dashboardServiceProxy.getMembersdetailsAllStates(this.stateDash1).subscribe(result => {
            this.UserDetailsDto = result;
        })
    }


    getRoleName(val) {
        abp.ui.setBusy();
        this._projectFundGetUser.getUserRoleById(val).subscribe(result => {
            this.roleName = result;
            if (this.roleName == 'Program Manager') {
                this.programManagerDashboard();
            }
            if (this.roleName == 'RRC Incharge') {
                this.RRCIncharge();
            }
            if (this.roleName == 'Project Manager') {
                this.ProjectMngr();
            }
            abp.ui.clearBusy();
        });
    }

    SummarryAllProjects() {
        abp.ui.setBusy();
        var summrry = document.getElementById("summry") as HTMLInputElement
        summrry.style.display = "block";
        this._dashboardService.summaryAllProjects().subscribe(result => {
            this.summrryallprojects = result.summryAllProjects;
            this.latestActivityAssignReport = result.latestActivityAssignReports;
            this.primengDatatableHelper.records = result.latestActivityAssignReports;
            this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.latestActivityAssignReports.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.Record = result.latestActivityAssignReports;
            this.Record = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            this.budgetvsactualperm = result.budgetachievmentdtos;
            this.totalbudgettotalexpense = result.totalbudgettotalexpenses;
            abp.ui.clearBusy();
        });
    }

    clustorwise(data) {
        abp.ui.setBusy();
        this._dashboardService.clustorwisetotalbudgettotalexpenses(data.pid).subscribe(result => {
            this.clustorwisetotalbudgettotalexpense = result;
            abp.ui.clearBusy();
        });
    }

    changeStyle(head, title, subtitle) {
        var summrry = document.getElementById("" + head) as HTMLInputElement
        summrry.style.backgroundColor = "#01B6E6";
        var summrry = document.getElementById("" + title) as HTMLInputElement
        summrry.style.color = "white";
        var summrry = document.getElementById("" + subtitle) as HTMLInputElement
        summrry.style.color = "white";
    }

    changeStyleReverse(head, title, subtitle) {
        var summrry = document.getElementById("" + head) as HTMLInputElement
        summrry.style.backgroundColor = "white";
        var summrry = document.getElementById("" + title) as HTMLInputElement
        summrry.style.color = "black";
        var summrry = document.getElementById("" + subtitle) as HTMLInputElement
        summrry.style.color = "#716aca";
    }

    programManagerDashboard() {
        abp.ui.setBusy();
        this._dashboardService.programManagerDashboard().subscribe(result => {

            this.programManagerChartInfo = result;
            this._dashboardService.programmeMngrStateWiseBudget_G().subscribe(result => {

                this.dataZ = result.programManagerProgramStateWiseBudgetBarChart;
                this.drawCharts1();
                abp.ui.clearBusy();
            });
        });
    }

    RRCIncharge() {

        abp.ui.setBusy();

        this._dashboardService.rRCInchargeDashboard().subscribe(result => {
            this.programManagerChartInfo = result;
        });

        this._dashboardService.rRCInchargePhysVsActperformance().subscribe(result => {

            this.data.programManagerProgramStateWiseBudgetBarChart = result.programManagerProgramStateWiseBudgetBarChart;
            this.dataRRCIncharge = result.programManagerProgramStateWiseBudgetBarChart;
            this.len = this.dataRRCIncharge.length;
            this.drawChartsRRC();
        });


    }

    ProjectMngr() {
        abp.ui.setBusy();
        this._dashboardService.projectManagerDashboard().subscribe(result => {
            this.programManagerChartInfo = result;
            abp.ui.clearBusy();
        });

        this._dashboardService.rRCInchargebudget().subscribe(results => {
            this.data.programManagerProgramStateWiseBudgetBarChart = results.programManagerProgramStateWiseBudgetBarChart;
            this.dataPM = this.data.programManagerProgramStateWiseBudgetBarChart;
            this.drawChartPM();
        });
    }

    drawCharts1() {
        abp.ui.setBusy();
        var dataZ = this.dataZ;
        this.zone.runOutsideAngular(() => {
            Promise.all([
                import("@amcharts/amcharts4/core"),
                import("@amcharts/amcharts4/charts"),
                import("@amcharts/amcharts4/themes/animated")
            ])
                .then(modules => {

                    setTimeout(function () {
                        const am4core = modules[0];
                        const am4charts = modules[1];
                        const am4themes_animated = modules[2].default;
                        am4core.options.commercialLicense = true;
                        am4core.useTheme(am4themes_animated);
                        let chart = am4core.create("chartdiv21", am4charts.XYChart);
                        dataZ.forEach(
                            xx => {
                                xx.datasets.forEach(e => {
                                    chart.data.push({
                                        "year": xx.labels[0],
                                        "budget": xx.datasets[0].data[0],
                                        "actualPerformance": xx.datasets[1].data[0],
                                    })
                                })
                            })
                        // Create axes
                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                        categoryAxis.dataFields.category = "year";
                        categoryAxis.title.text = "State";
                        categoryAxis.renderer.grid.template.location = 0;
                        categoryAxis.renderer.minGridDistance = 20;
                        categoryAxis.renderer.cellStartLocation = 0.1;
                        categoryAxis.renderer.cellEndLocation = 0.9;
                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        valueAxis.min = 0;
                        valueAxis.title.text = " ";
                        // Create series
                        function createSeries(field, name) {
                            var series = chart.series.push(new am4charts.ColumnSeries());
                            series.dataFields.valueY = field;
                            series.dataFields.categoryX = "year";
                            series.name = name;
                            series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
                            series.columns.template.width = am4core.percent(95);
                        }
                        // Add a legend
                        chart.legend = new am4charts.Legend();
                        chart.legend.labels.template.maxWidth = 150;
                        chart.legend.labels.template.truncate = true;

                        createSeries("budget", "Budget");
                        createSeries("actualPerformance", "ActualPerformance");
                    }, 10)
                    abp.ui.clearBusy();
                })
                .catch(e => {
                    console.error("Error when creating chart", e);
                });
        });
    }

    drawChartsRRC() {



        var dataRRCIncharge = this.dataRRCIncharge;
        this.zone.runOutsideAngular(() => {

            Promise.all([
                import("@amcharts/amcharts4/core"),
                import("@amcharts/amcharts4/charts"),
                import("@amcharts/amcharts4/themes/animated")
            ])
                .then(modules => {
                    setTimeout(function () {
                        const am4core = modules[0];
                        const am4charts = modules[1];
                        const am4themes_animated = modules[2].default;
                        am4core.options.commercialLicense = true;
                        am4core.useTheme(am4themes_animated);
                        let chart = am4core.create("RRCchartdiv", am4charts.XYChart);

                        dataRRCIncharge.forEach(
                            xx => {
                                xx.datasets.forEach(e => {
                                    chart.data.push({
                                        "year": xx.labels[0],
                                        "budget": xx.datasets[0].data[0],
                                        "actualPerformance": xx.datasets[1].data[0],
                                    })
                                })
                            })

                        // Create axes
                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());


                        categoryAxis.dataFields.category = "year";
                        categoryAxis.title.text = "Program";
                        categoryAxis.renderer.grid.template.location = 0;
                        categoryAxis.renderer.minGridDistance = 20;
                        categoryAxis.renderer.cellStartLocation = 0.1;
                        categoryAxis.renderer.cellEndLocation = 0.9;
                        categoryAxis.renderer.labels.template
                            .cursorOverStyle = [
                                {
                                    "property": "cursor",
                                    "value": "pointer"
                                }
                            ];

                        let label = categoryAxis.renderer.labels.template;
                        label.wrap = true;
                        label.maxWidth = 120;

                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        valueAxis.min = 0;
                        valueAxis.title.text = "budget";

                        // Create series
                        function createSeries(field, name) {
                            var series = chart.series.push(new am4charts.ColumnSeries());
                            series.dataFields.valueY = field;
                            series.dataFields.categoryX = "year";
                            series.name = name;
                            series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
                            series.columns.template.width = am4core.percent(95);
                        }
                        createSeries("budget", "Budget");
                        createSeries("actualPerformance", "ActualPerformance");
                    }, 3000)
                })
                .catch(e => {
                    console.error("Error when creating chart", e);
                });
        });
    }

    drawChartPM() {
        var dataPM = this.dataPM;
        this.zone.runOutsideAngular(() => {
            Promise.all([
                import("@amcharts/amcharts4/core"),
                import("@amcharts/amcharts4/charts"),
                import("@amcharts/amcharts4/themes/animated")
            ])
                .then(modules => {


                    setTimeout(function () {
                        const am4core = modules[0];
                        const am4charts = modules[1];
                        const am4themes_animated = modules[2].default;
                        am4core.options.commercialLicense = true;
                        am4core.useTheme(am4themes_animated);
                        let chart = am4core.create("PMchartdiv", am4charts.XYChart);


                        dataPM.forEach(
                            xx => {
                                xx.datasets.forEach(e => {
                                    chart.data.push({
                                        "year": xx.labels[0],
                                        "budget": xx.datasets[0].data[0],
                                        "actualPerformance": xx.datasets[1].data[0],
                                    })
                                })
                            })

                        // Create axes
                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

                        categoryAxis.dataFields.category = "year";
                        categoryAxis.title.text = "Program";
                        categoryAxis.renderer.grid.template.location = 0;
                        categoryAxis.renderer.minGridDistance = 20;
                        categoryAxis.renderer.cellStartLocation = 0.1;
                        categoryAxis.renderer.cellEndLocation = 0.9;

                        categoryAxis.renderer.labels.template

                            .cursorOverStyle = [
                                {
                                    "property": "cursor",
                                    "value": "pointer"
                                }
                            ];

                        let label = categoryAxis.renderer.labels.template;
                        label.wrap = true;
                        label.maxWidth = 120;
                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        valueAxis.min = 0;
                        valueAxis.title.text = " ";

                        function createSeries(field, name) {
                            var series = chart.series.push(new am4charts.ColumnSeries());
                            series.dataFields.valueY = field;
                            series.dataFields.categoryX = "year";
                            series.name = name;
                            series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
                            series.columns.template.width = am4core.percent(95);
                        }
                        createSeries("budget", "Budget");
                        createSeries("actualPerformance", "ActualPerformance");
                    }, 3000)
                })
                .catch(e => {
                    console.error("Error when creating chart", e);
                });
        });
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = Number(p.rows);
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }

    getDashboardStatisticsData(datePeriod): void {
        abp.ui.setBusy();
        this.salesSummaryChart.showLoading();
        this.generalStatsPieChart.showLoading();

        this._dashboardService
            .getDashboardData(datePeriod)
            .subscribe(result => {
                this.dashboardHeaderStats.init(result.totalProfit, result.newFeedbacks, result.newOrders, result.newUsers);
                this.generalStatsPieChart.init(result.transactionPercent, result.newVisitPercent, result.bouncePercent);
                this.dailySalesLineChart.init(result.dailySales);
                this.profitSharePieChart.init(result.profitShares);
                this.salesSummaryChart.init(result.salesSummary, result.totalSales, result.revenue, result.expenses, result.growth);
                abp.ui.clearBusy();
            });
    }

    ngAfterViewInit(): void {
    }

    ngOnDestroy() {
    }
}


abstract class DashboardChartBase {
    loading = true;

    showLoading() {
        abp.ui.setBusy();
        setTimeout(() => { this.loading = true; });
    }

    hideLoading() {
        abp.ui.clearBusy();
        setTimeout(() => { this.loading = false; });
    }
}

class SalesSummaryChart extends DashboardChartBase {
    //Sales summary => MorrisJs: https://github.com/morrisjs/morris.js/

    instance: morris.GridChart;
    totalSales = 0; totalSalesCounter = 0;
    revenue = 0; revenuesCounter = 0;
    expenses = 0; expensesCounter = 0;
    growth = 0; growthCounter = 0;
    roleName: string = "";
    constructor(private _dashboardService: TenantDashboardServiceProxy, private _projectFundGetUser: ProgramFundsServiceProxy, private _containerElement: any) {

        super();
    }

    init(salesSummaryData, totalSales, revenue, expenses, growth) {
        this.instance = Morris.Area({
            element: this._containerElement,
            padding: 0,
            behaveLikeLine: false,
            gridEnabled: false,
            gridLineColor: 'transparent',
            axes: false,
            fillOpacity: 1,
            data: salesSummaryData,
            lineColors: ['#399a8c', '#92e9dc'],
            xkey: 'period',
            ykeys: ['sales', 'profit'],
            labels: ['Sales', 'Profit'],
            pointSize: 0,
            lineWidth: 0,
            hideHover: 'auto',
            resize: true
        });

        this.totalSales = totalSales;
        this.totalSalesCounter = totalSales;

        this.revenue = revenue;
        this.expenses = expenses;
        this.growth = growth;

        this.hideLoading();

    }

    reload(datePeriod) {
        abp.ui.setBusy();
        this.showLoading();
        this._dashboardService
            .getSalesSummary(datePeriod)
            .subscribe(result => {
                this.instance.setData(result.salesSummary, true);
                this.hideLoading();
                abp.ui.clearBusy();
            });
    }
}

class RegionalStatsWorldMap extends DashboardChartBase {
    //World map => DataMaps: https://github.com/markmarkoh/datamaps/

    private _worldMap;
    private colors: any = d3.scale.category10();
    private refreshIntervalId: any;

    worldMap = element => {
        let instance: any;
        let init = data => new Datamap({
            element: document.getElementById(element),
            projection: 'mercator',
            fills: {
                defaultFill: '#ABDDA4',
                key: '#fa0fa0'
            },
            data: data,
            done(datamap) {
                const redraw = () => {
                    datamap.svg.selectAll('g').attr('transform', 'translate(' + d3.event.translate + ')scale(' + d3.event.scale + ')');
                };

                datamap.svg.call(d3.behavior.zoom().on('zoom', redraw));
            }
        });

        let redraw = () => {
            abp.ui.setBusy();
            this._dashboardService
                .getWorldMap({})
                .subscribe(result => {
                    let mapData = {};
                    for (let i = 0; i < result.countries.length; i++) {
                        let country = result.countries[i];
                        mapData[country.countryName] = this.colors(Math.random() * country.color);
                    }

                    instance.updateChoropleth(mapData);
                    abp.ui.clearBusy();
                });

        };

        let draw = data => {
            if (!instance) {
                instance = init(data);
            } else {
                instance.redraw();
            }
        };

        return {
            draw: draw,
            redraw: redraw
        };
    }

    constructor(private _dashboardService: TenantDashboardServiceProxy, private _containerElement) {
        super();
        this._worldMap = this.worldMap(this._containerElement);
    }

    draw(isAutoReload = false, reloadInterval = 3000) {

        this._worldMap.draw({
            USA: { fillKey: 'key' },
            JPN: { fillKey: 'key' },
            ITA: { fillKey: 'key' },
            CRI: { fillKey: 'key' },
            KOR: { fillKey: 'key' },
            DEU: { fillKey: 'key' },
            TUR: { fillKey: 'key' },
            RUS: { fillKey: 'key' }

        });

        if (isAutoReload) {
            this.reloadEvery(reloadInterval);
        }

        this.hideLoading();
    }

    dispose() {
        if (!this.refreshIntervalId) {
            return;
        }

        clearInterval(this.refreshIntervalId);
    }

    reloadEvery(milliseconds) {
        this.refreshIntervalId = setInterval(() => {
            this._worldMap.redraw();
        }, milliseconds);
    }
}

class GeneralStatsPieChart extends DashboardChartBase {
    //General stats =>  EasyPieChart: https://rendro.github.io/easy-pie-chart/

    transactionPercent = {
        value: 0,
        options: {
            barColor: '#F8CB00',
            trackColor: '#f9f9f9',
            scaleColor: '#dfe0e0',
            scaleLength: 5,
            lineCap: 'round',
            lineWidth: 3,
            size: 75,
            rotate: 0,
            animate: {
                duration: 1000,
                enabled: true
            }
        }
    };
    newVisitPercent = {
        value: 0,
        options: {
            barColor: '#1bbc9b',
            trackColor: '#f9f9f9',
            scaleColor: '#dfe0e0',
            scaleLength: 5,
            lineCap: 'round',
            lineWidth: 3,
            size: 75,
            rotate: 0,
            animate: {
                duration: 1000,
                enabled: true
            }
        }
    };
    bouncePercent = {
        value: 0,
        options: {
            barColor: '#F3565D',
            trackColor: '#f9f9f9',
            scaleColor: '#dfe0e0',
            scaleLength: 5,
            lineCap: 'round',
            lineWidth: 3,
            size: 75,
            rotate: 0,
            animate: {
                duration: 1000,
                enabled: true
            }
        }
    };

    constructor(private _dashboardService: TenantDashboardServiceProxy) {
        super();
    }

    init(transactionPercent, newVisitPercent, bouncePercent) {
        this.transactionPercent.value = transactionPercent;
        this.newVisitPercent.value = newVisitPercent;
        this.bouncePercent.value = bouncePercent;
        this.hideLoading();
    }

    reload() {
        abp.ui.setBusy();
        this.showLoading();
        this._dashboardService
            .getGeneralStats({})
            .subscribe(result => {
                this.init(result.transactionPercent, result.newVisitPercent, result.bouncePercent);
                abp.ui.clearBusy();
            });
    }
}

class DailySalesLineChart extends DashboardChartBase {
    //== Daily Sales chart.
    //** Based on Chartjs plugin - http://www.chartjs.org/

    _canvasId: string;

    constructor(private _dashboardService: TenantDashboardServiceProxy, canvasId: string) {
        super();
        this._canvasId = canvasId;
    }

    init(data) {
        var dayLabels = [];
        for (var day = 1; day <= data.length; day++) {
            dayLabels.push("Day " + day);
        }

        var chartData = {
            labels: dayLabels,
            datasets: [{
                //label: 'Dataset 1',
                backgroundColor: mUtil.getColor('success'),
                data: data
            }, {
                //label: 'Dataset 2',
                backgroundColor: '#f3f3fb',
                data: data
            }]
        };

        var chartContainer = $(this._canvasId);

        if (chartContainer.length === 0) {
            return;
        }

        var chart = new Chart(chartContainer, {
            type: 'bar',
            data: chartData,
            options: {
                title: {
                    display: false,
                },
                tooltips: {
                    intersect: false,
                    mode: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10
                },
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: false,
                barRadius: 4,
                scales: {
                    xAxes: [{
                        display: false,
                        gridLines: false,
                        stacked: true
                    }],
                    yAxes: [{
                        display: false,
                        stacked: true,
                        gridLines: false
                    }]
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }
                }
            }
        });

        this.hideLoading();
    }

    reload() {
        abp.ui.setBusy();
        this.showLoading();
        this._dashboardService
            .getSalesSummary(AppSalesSummaryDatePeriod.Monthly)
            .subscribe(result => {
                this.init(result.salesSummary);
                this.hideLoading();
                abp.ui.clearBusy();
            });
    }
}

class ProfitSharePieChart extends DashboardChartBase {
    //== Profit Share Chart.
    //** Based on Chartist plugin - https://gionkunz.github.io/chartist-js/index.html

    _canvasId: string;
    data: number[];

    constructor(private _dashboardService: TenantDashboardServiceProxy, canvasId: string) {
        super();
        this._canvasId = canvasId;
    }

    init(data: number[]) {
        this.data = data;
        if ($(this._canvasId).length === 0) {
            return;
        }

        var chart = new Chartist.Pie(this._canvasId, {
            series: [{
                value: data[0],
                className: 'custom',
                meta: {
                    color: mUtil.getColor('brand')
                }
            },
            {
                value: data[1],
                className: 'custom',
                meta: {
                    color: mUtil.getColor('accent')
                }
            },
            {
                value: data[2],
                className: 'custom',
                meta: {
                    color: mUtil.getColor('warning')
                }
            }
            ],
            labels: [1, 2, 3]
        }, {
            donut: true,
            donutWidth: 17,
            showLabel: false
        });

        chart.on('draw', (data) => {
            if (data.type === 'slice') {
                // Get the total path length in order to use for dash array animation
                var pathLength = data.element._node.getTotalLength();

                // Set a dasharray that matches the path length as prerequisite to animate dashoffset
                data.element.attr({
                    'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
                });

                // Create animation definition while also assigning an ID to the animation for later sync usage
                var animationDefinition = {
                    'stroke-dashoffset': {
                        id: 'anim' + data.index,
                        dur: 1000,
                        from: -pathLength + 'px',
                        to: '0px',
                        easing: Chartist.Svg.Easing.easeOutQuint,
                        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                        fill: 'freeze',
                        'stroke': data.meta.color
                    }
                };

                // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
                if (data.index !== 0) {
                    (animationDefinition['stroke-dashoffset'] as any).begin = 'anim' + (data.index - 1) + '.end';
                }

                // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

                data.element.attr({
                    'stroke-dashoffset': -pathLength + 'px',
                    'stroke': data.meta.color
                });

                // We can't use guided mode as the animations need to rely on setting begin manually
                // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
                data.element.animate(animationDefinition, false);
            }
        });

        this.hideLoading();
    }
}

class DashboardHeaderStats extends DashboardChartBase {

    totalProfit = 0; totalProfitCounter = 0;
    newFeedbacks = 0; newFeedbacksCounter = 0;
    newOrders = 0; newOrdersCounter = 0;
    newUsers = 0; newUsersCounter = 0;

    totalProfitChange = 76; totalProfitChangeCounter = 0;
    newFeedbacksChange = 85; newFeedbacksChangeCounter = 0;
    newOrdersChange = 45; newOrdersChangeCounter = 0;
    newUsersChange = 57; newUsersChangeCounter = 0;

    init(totalProfit, newFeedbacks, newOrders, newUsers) {
        this.totalProfit = totalProfit;
        this.newFeedbacks = newFeedbacks;
        this.newOrders = newOrders;
        this.newUsers = newUsers;
        this.hideLoading();
    }
}

class MemberActivityTable extends DashboardChartBase {

    memberActivities: Array<any>;

    constructor(private _dashboardService: TenantDashboardServiceProxy) {
        super();
    }

    init() {
        this.reload();
    }

    reload() {
        abp.ui.setBusy();
        this.showLoading();
        this._dashboardService
            .getMemberActivity()
            .subscribe(result => {
                this.memberActivities = result.memberActivities;
                this.hideLoading();
                abp.ui.clearBusy();
            });
    }
}
