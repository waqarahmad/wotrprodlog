﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { TenantDashboardServiceProxy, ActivityServiceProxy, ActivitiyListDto, PrgRequestAAPlasServiceProxy, DemandRemaningAmount, RRCManagerAmountSanction, UnitofMeasureDto, ProjectWiseYearlyActivity, AllChartDto, DemandWiseProject} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
import { ProjectWiseDemandModalComponent } from './project-wise-demand-modal.component';

declare var jQuery: any;
 

@Component({
    selector: 'ProgramWiseDemandModal',
    templateUrl: "./program-wise-demand-modal.component.html"
})
export class ProgramWiseDemandModalComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('programWiseDemand') modal: ModalDirective;
    @ViewChild('ProjectWiseDemandModal') ProjectWiseDemandModal: ProjectWiseDemandModalComponent;  
    data: AllChartDto = new AllChartDto();
    active: boolean = false;
    demandWiseProjectData: RRCManagerAmountSanction[] = [];
    demandremainingWiseProjectData: DemandRemaningAmount[] = [];

    constructor(
        injector: Injector,
        private _dashboardService: TenantDashboardServiceProxy,
    ) {
        super(injector);

    }

    show(data): void {
        var programId = data.programId;
        this.active = true;
        this._dashboardService.programWiseDemandInfo(programId).subscribe(result => {
            
            this.demandWiseProjectData = result;
            
        });
        this._dashboardService.programWiseDemandRemaningAmountInfo(programId).subscribe(results => {

            this.demandremainingWiseProjectData = results;
            
            abp.ui.clearBusy();
        });

       
        //this.data = data;
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}