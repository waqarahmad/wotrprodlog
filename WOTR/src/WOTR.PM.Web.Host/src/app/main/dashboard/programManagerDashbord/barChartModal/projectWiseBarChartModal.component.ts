﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output, NgZone } from '@angular/core';
import { ActivityServiceProxy, ActivitiyListDto, PrgRequestAAPlasServiceProxy, UnitofMeasureDto, ProjectWiseYearlyActivity, AllChartDto, TenantDashboardServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
import { SimpleBarChartProgramManagerModalsComponent } from '../simpleBarChartModal/simpleBarChartProgramManagerComponentModal.component';

declare var jQuery: any;

@Component({
    selector: 'ProjectWiseBarChart',
    templateUrl: "./projectWiseBarChartModal.component.html"
})
export class ProjectWiseBarChartModalsComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
     
    @ViewChild('SimpleBarChartModals') SimpleBarChartModals: SimpleBarChartProgramManagerModalsComponent;
    @ViewChild('projectWiseModal') modal: ModalDirective;
    active: boolean = false;
    data: AllChartDto = new AllChartDto(); 
    projectmanagerdata: any;
    pmChart: any;
    len: number;
    Wid: number = 200;

    constructor(
        injector: Injector, private zone: NgZone,
        private _dashboardService: TenantDashboardServiceProxy,
    ) {
        super(injector);

    }

    show(data): void {
        
        //var stateName = data.labels; 
        var stateName = data.currentText;
        this.active = true;
        this._dashboardService.programManagerProgramStateWiseBudget(stateName).subscribe(result => {
            
            this.data = result;
            this.projectmanagerdata = result.programManagerProgramStateWiseBudgetBarChart;
            this.pmChart = this.projectmanagerdata.length;
            var pmChart1 = <HTMLInputElement>document.getElementById('chartdiv1');
            if (this.pmChart <= 4) {
                pmChart1.style.width = '100%';
            }
            else if (this.pmChart > 4 && this.pmChart <= 6) {
                pmChart1.style.width = '300%';
            }
            else if (this.pmChart > 7 && this.pmChart <= 9) {
                pmChart1.style.width = '500%';
            } else if (this.pmChart > 9 && this.pmChart <= 11) {
                pmChart1.style.width = '800%';
            } else {
                pmChart1.style.width = '1000%';
            }

            this.drawCharts(); 

            
        });



        //this.data = data;
        this.modal.show();
    }
    drawCharts() {                this.zone.runOutsideAngular(() => {            Promise.all([                import("@amcharts/amcharts4/core"),                import("@amcharts/amcharts4/charts"),                import("@amcharts/amcharts4/themes/animated")            ])                .then(modules => {                    const am4core = modules[0];                    const am4charts = modules[1];                    const am4themes_animated = modules[2].default;                    am4core.options.commercialLicense = true;                    am4core.useTheme(am4themes_animated);                    let chart = am4core.create("chartdiv1", am4charts.XYChart);
                    
                    this.projectmanagerdata.forEach(
                        xx => {
                           
                                chart.data.push({
                                    "year": xx.labels[0],
                                    "budget": xx.datasets[0].data[0],
                                    "actualPerformance": xx.datasets[1].data[0],

                            })
                        })


                    // Create axes
                    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

                    categoryAxis.dataFields.category = "year";
                    categoryAxis.title.text = "Project Name";
                    categoryAxis.renderer.grid.template.location = 0;
                    categoryAxis.renderer.minGridDistance = 20;
                    categoryAxis.renderer.cellStartLocation = 0.1;
                    categoryAxis.renderer.cellEndLocation = 0.9;
                    categoryAxis.renderer.labels.template.tooltipText = "click here";
                    categoryAxis.renderer.labels.template

                        .cursorOverStyle = [
                            {
                                "property": "cursor",
                                "value": "pointer"
                            }
                        ];
                    let label = categoryAxis.renderer.labels.template;
                    label.wrap = true;
                    label.maxWidth = 120;

                    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                    valueAxis.min = 0;
                    valueAxis.title.text = " ";
                    categoryAxis.renderer.labels.template.events.on("hit", function (ev) {
                        
                        //alert(ev.event.target);
                        this.SimpleBarChartModals.show(ev.target);
                    }, this);

                    // Create series
                    function createSeries(field, name) {
                        var series = chart.series.push(new am4charts.ColumnSeries());
                        series.dataFields.valueY = field;
                        series.dataFields.categoryX = "year";
                        series.name = name;
                        series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
                        //series.stacked = stacked;
                        series.columns.template.width = am4core.percent(95);

                    }

                    // Add a legend
                    chart.legend = new am4charts.Legend();
                    chart.legend.labels.template.maxWidth = 150;
                    chart.legend.labels.template.truncate = true;
                    

                    createSeries("budget", "Budget");
                    createSeries("actualPerformance", "ActualPerformance");                })                .catch(e => {                    console.error("Error when creating chart", e);                });        });    }
    close(): void {
        this.modal.hide(); 
        this.active = false;
    }
}