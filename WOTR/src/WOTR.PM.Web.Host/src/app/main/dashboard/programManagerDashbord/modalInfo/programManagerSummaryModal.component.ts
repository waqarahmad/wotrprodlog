﻿import { Component, Injector, ViewChild, EventEmitter, Output, NgZone } from '@angular/core';
import {  TenantDashboardServiceProxy, AllChartDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { ProjectWiseBarChartModalsComponent } from '../barChartModal/ProjectWiseBarChartModal.component';
declare var jQuery: any;


@Component({
    selector: 'ProgramManagerSummary',
    templateUrl: "./programManagerSummaryModal.component.html"
})
export class ProgramManagerSummaryModalsComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('summaryModal') modal: ModalDirective;
    @ViewChild('ProjectWiseBarChartModals') ProjectWiseBarChartModals: ProjectWiseBarChartModalsComponent;
    active: boolean = false;
    saving: boolean = false;
    data: AllChartDto = new AllChartDto();
    dataZ: any;
    dataZs: any;
    dataZsss: any;
    dataZsssa: any;

    type: number;
    labelss: any = [];
    datasetss: any = [];
    constructor(private zone: NgZone,
        private _dashboardService: TenantDashboardServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }


    show(pId: number, data: AllChartDto): void {
        this._dashboardService.programmeMngrStateWiseBudget_G().subscribe(result => {           
                this.dataZ = result.programManagerProgramStateWiseBudgetBarChart;
                this.drawCharts(this.dataZ);
            document.getElementById('loader').style.display = "none"; 
        });
        this.active = true;
        this.type = pId;
        this.data = data;  
        this.modal.show();
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
    abc: string;
    drawCharts(dataZ) {                this.zone.runOutsideAngular(() => {            Promise.all([                import("@amcharts/amcharts4/core"),                import("@amcharts/amcharts4/charts"),                import("@amcharts/amcharts4/themes/animated")            ])                .then(modules => {                    let t = this;                    setTimeout(function (this) {                        const am4core = modules[0];                        const am4charts = modules[1];                        const am4themes_animated = modules[2].default;                        am4core.options.commercialLicense = true;                        am4core.useTheme(am4themes_animated);                        let chart = am4core.create("chartdiv", am4charts.XYChart);
                        dataZ.forEach(
                            xx => {
                                xx.datasets.forEach(e => {
                                    chart.data.push({
                                        "year": xx.labels[0],
                                        "budget": xx.datasets[0].data[0],
                                        "actualPerformance": xx.datasets[1].data[0],
                                    })
                                })
                            })
                        // Create axes
                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

                        categoryAxis.dataFields.category = "year";
                        categoryAxis.title.text = "State";
                        categoryAxis.renderer.grid.template.location = 0;
                        categoryAxis.renderer.minGridDistance = 20;
                        categoryAxis.renderer.cellStartLocation = 0.1;
                        categoryAxis.renderer.cellEndLocation = 0.9;
                        categoryAxis.renderer.labels.template.tooltipText = "click here";
                        categoryAxis.renderer.labels.template

                            .cursorOverStyle = [
                                {
                                    "property": "cursor",
                                    "value": "pointer"
                                }
                            ];
                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        valueAxis.min = 0;
                        valueAxis.title.text = " ";
                        let dd = t.ProjectWiseBarChartModals;
                        categoryAxis.renderer.labels.template.events.on("hit", function (ev) {

                            //alert(ev.event.target);
                           dd.show(ev.target);
                        }, this);
                        // Create series
                        function createSeries(field, name) {
                            var series = chart.series.push(new am4charts.ColumnSeries());
                            series.dataFields.valueY = field;
                            series.dataFields.categoryX = "year";
                            series.name = name;
                            series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
                            //series.stacked = stacked;
                            series.columns.template.width = am4core.percent(95);

                        }

                        // Add a legend
                        chart.legend = new am4charts.Legend();
                        chart.legend.labels.template.maxWidth = 150;
                        chart.legend.labels.template.truncate = true;

                        createSeries("budget", "Budget");
                        createSeries("actualPerformance", "ActualPerformance");                    },2000);                })                .catch(e => {                    console.error("Error when creating chart", e);                });        });            }
     
}