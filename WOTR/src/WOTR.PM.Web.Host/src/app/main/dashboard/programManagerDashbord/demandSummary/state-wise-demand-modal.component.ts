﻿import { Component, Injector, ViewChild, EventEmitter, Output } from '@angular/core';
import { TenantDashboardServiceProxy, AllChartDto, DemandWiseProject } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { ProjectWiseDemandModalComponent } from './project-wise-demand-modal.component';
declare var jQuery: any;


@Component({
    selector: 'StateWiseDemandModal',
    templateUrl: "./state-wise-demand-modal.component.html"
})
export class StateWiseDemandModalComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('stateWiseDemand') modal: ModalDirective;
    @ViewChild('ProjectWiseDemandModal') ProjectWiseDemandModal: ProjectWiseDemandModalComponent;
    data: AllChartDto = new AllChartDto();
    active: boolean = false;
    demandWiseProjectData: DemandWiseProject[] = [];
    constructor(
        injector: Injector,
        private _dashboardService: TenantDashboardServiceProxy) {
        super(injector);
    }

    show(data): void {
        var stateId = data.stateId;
        this.active = true;
        this._dashboardService.stateWiseDemandInfo(stateId).subscribe(result => {
            this.demandWiseProjectData = result;
            abp.ui.clearBusy();
        });
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}