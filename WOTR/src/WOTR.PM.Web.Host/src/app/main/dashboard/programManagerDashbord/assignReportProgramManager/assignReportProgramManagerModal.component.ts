﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { ActivityServiceProxy, ActivitiyListDto, PrgRequestAAPlasServiceProxy, UnitofMeasureDto, ProjectWiseYearlyActivity, AllChartDto, TenantDashboardServiceProxy, ProgramManagerImplemenationPlanCheckListWiseBudget } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms'; 

declare var jQuery: any;


@Component({
    selector: 'AssignReportModals',
    templateUrl: "./assignReportProgramManagerModal.component.html"
})
export class AssignReportModalsComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('assignReport') modal: ModalDirective;  
    data: ProgramManagerImplemenationPlanCheckListWiseBudget[] = []; 
    active: boolean=false;
    constructor(
        injector: Injector,
        private _dashboardService: TenantDashboardServiceProxy,
    ) {
        super(injector);

    }

    show(projectname:string, data): void { 
        this.active = true;
        var projectYear = data.currentText;
        this.active = true;
        abp.ui.setBusy();
        this._dashboardService.yearWiseAssignedReport(projectname,projectYear).subscribe(result => {
            this.data = result;
            abp.ui.clearBusy();
        });

        this.data = data;
        this.modal.show();
    }

    close(): void {
        this.active = false; 
        this.modal.hide(); 
    }
}