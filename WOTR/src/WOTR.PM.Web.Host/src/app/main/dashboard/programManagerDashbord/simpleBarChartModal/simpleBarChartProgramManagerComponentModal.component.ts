﻿import { Component, Injector, ViewChild, EventEmitter, Output, NgZone } from '@angular/core';
import { AllChartDto, TenantDashboardServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { AssignReportModalsComponent } from '../assignReportProgramManager/assignReportProgramManagerModal.component';
declare var jQuery: any;


@Component({
    selector: 'SimpleBarChartProgramManager',
    templateUrl: "./simpleBarChartProgramManagerComponentModal.component.html"
})
export class SimpleBarChartProgramManagerModalsComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('simpleBarChartModal') modal: ModalDirective;
    @ViewChild('AssignReportModals') AssignReportModals: AssignReportModalsComponent;
    active: boolean = false;
    data: AllChartDto = new AllChartDto();
    simplebarcharts: any;
    projetName: string;

    constructor(
        injector: Injector, private zone: NgZone,
        private _dashboardService: TenantDashboardServiceProxy) {
        super(injector);
    }

    show(data): void {
        this.active = true;
        this.projetName = data.currentText;
        this.active = true;
        abp.ui.setBusy();
        this._dashboardService.projectWiseStateWiseBudget(this.projetName).subscribe(result => {
            this.data = result;
            this.simplebarcharts = result.programManagerProgramYearwiseStateWiseBudgetBarChart
            this.drawCharts();
            abp.ui.clearBusy();
        });
        this.modal.show();
    }

    drawCharts() {        this.zone.runOutsideAngular(() => {            Promise.all([                import("@amcharts/amcharts4/core"),                import("@amcharts/amcharts4/charts"),                import("@amcharts/amcharts4/themes/animated"),            ])                .then(modules => {                    let t = this;                    setTimeout(function (this) {                        const am4core = modules[0];                        const am4charts = modules[1];                        const am4themes_animated = modules[2].default;                        am4core.options.commercialLicense = true;                        //am4core.useTheme(am4themes_animated);                        am4core.useTheme(am4themes_animated);                        let chart = am4core.create("chartdiv2", am4charts.XYChart);

                        t.simplebarcharts.forEach(
                            xx => {

                                chart.data.push({
                                    "year": xx.labels[0],
                                    "funderContribution": xx.datasets[0].data[0],
                                    "communityContribution": xx.datasets[1].data[0],
                                    "otherContribution": xx.datasets[2].data[0],

                                })
                            })


                        // Create axes
                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

                        categoryAxis.dataFields.category = "year";
                        categoryAxis.title.text = "Year";
                        categoryAxis.renderer.grid.template.location = 0;
                        categoryAxis.renderer.minGridDistance = 20;
                        categoryAxis.renderer.cellStartLocation = 0.1;
                        categoryAxis.renderer.cellEndLocation = 0.9;
                        categoryAxis.renderer.labels.template.tooltipText = "click here";

                        categoryAxis.renderer.labels.template

                            .cursorOverStyle = [
                                {
                                    "property": "cursor",
                                    "value": "pointer",

                                }
                            ];
                        let label = categoryAxis.renderer.labels.template;
                        label.wrap = true;
                        label.maxWidth = 120;

                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                        valueAxis.min = 0;
                        valueAxis.title.text = " ";
                        let dd = t.AssignReportModals;
                        categoryAxis.renderer.labels.template.events.on("hit", function (ev) {
                            dd.show(t.projetName, ev.target);
                        }, this);

                        // Create series
                        function createSeries(field, name) {
                            var series = chart.series.push(new am4charts.ColumnSeries());
                            series.dataFields.valueY = field;
                            series.dataFields.categoryX = "year";
                            series.name = name;
                            series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
                            //series.stacked = stacked;
                            series.columns.template.width = am4core.percent(95);

                        }

                        // Add a legend
                        chart.legend = new am4charts.Legend();
                        chart.legend.labels.template.maxWidth = 150;
                        chart.legend.labels.template.truncate = true;

                        createSeries("funderContribution", "FunderContribution");
                        createSeries("communityContribution", "CommunityContribution");                        createSeries("otherContribution", "OtherContribution");                    }, 2000)                })                .catch(e => {                    console.error("Error when creating chart", e);                });        });    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }

    ngOnDestroy() {
        this.data = null;
        this.projetName = '';
    }
}