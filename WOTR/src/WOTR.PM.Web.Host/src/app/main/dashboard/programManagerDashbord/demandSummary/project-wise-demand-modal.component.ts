﻿import { Component, Injector, ViewChild, EventEmitter, Output } from '@angular/core';
import { AllChartDto, TenantDashboardServiceProxy, ProgramManagerAmountSanction } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
declare var jQuery: any;


@Component({
    selector: 'ProjectWiseDemandModal',
    templateUrl: "./project-wise-demand-modal.component.html"
})

export class ProjectWiseDemandModalComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('projectWiseDemand') modal: ModalDirective;
    data: AllChartDto = new AllChartDto();
    active: boolean = false;
    projectWiseAmountSanctionData: ProgramManagerAmountSanction[] = [];
    constructor(
        injector: Injector,
        private _dashboardService: TenantDashboardServiceProxy) {
        super(injector);

    }

    show(data): void {
        var programId = data.programId;
        this.active = true;
        this._dashboardService.projectWiseAmountSanction(programId).subscribe(result => {
            this.projectWiseAmountSanctionData = result;
            abp.ui.clearBusy();
        });
        this.modal.show();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}