import { Component, OnInit, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
  selector: 'app-demo-component',
  templateUrl: './demo-component.component.html',
  styleUrls: ['./demo-component.component.css']
})
export class DemoComponentComponent extends AppComponentBase implements OnInit {

    constructor(injector: Injector) {
        super(injector);
  }

  ngOnInit() {
  }

}
