﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { RoleServiceProxy, ActionAreasServiceProxy, ListActionSubactionListDto, Village, RequestAAPlasListDto, PrgRequestAAPlasServiceProxy, ProgramServiceProxy, ActionAreasListDto, ProgramInformationListDto, PrgVillageClusterListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';

//import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';
import { debounceTime } from 'rxjs/operator/debounceTime';
import * as moment from "moment";
import { forEach } from '@angular/router/src/utils/collection';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { DataTable } from "primeng/components/datatable/datatable";
declare let jsPDF;




@Component({
    selector: 'projectListIIIcomponent',
    templateUrl: "./projectListIII.component.html",
    styleUrls: ['./projectListIII.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class projectListIIIComponent extends AppComponentBase {
    @ViewChild('test') el: ElementRef;

    selectedPermission = '';
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: ProgramInformationListDto[] = [];
    programdetails: ProgramInformationListDto[];
    programd: ProgramInformationListDto = new ProgramInformationListDto();
    VillageList: any = [];
    ActivityList: any = [];
    ComponentList: any = [];
    VillageClustorList: any = [];
    program: ProgramInformationListDto[];

    requestList: RequestAAPlasListDto[] = [];
    villageclustor: PrgVillageClusterListDto[];
    pro: any[];
    active: boolean = false;
    list: any = [];
    requestSingleYearList: RequestAAPlasListDto = new RequestAAPlasListDto();
    AddSubActionList: ListActionSubactionListDto;
    emailContent: any;
    saving: boolean = false;
    prgVilages: Village[] = [];
    Year: any[];
    costyear: string;
    constructor(
        injector: Injector,
        private _activatedRoute: ActivatedRoute,

        private _actionareaService: ActionAreasServiceProxy,
        private _programService: ProgramServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy,

        private _roleService: RoleServiceProxy) {
        super(injector);
    }

    ngOnInit() {

        this.getallyear();
        //this.getAllProgramdetail();

        this.programName = this._activatedRoute.snapshot.queryParams.ProgrameName;

    }


    getallyear() {
        this._programService.allyear().subscribe(result => {
            this.Year = result;

        })
    }


    count: number = -1;
    finalUnits: number = 0;
    sumOfTotal: number = 0;
    sumOfTotal1: number = 0;
    DivideUnits(res, rec) {
    }
   

    programName: string;
    locationName: string;
    managerName: string;
    requestStatus: number;
    costestimationyear: string;
    ActivityLevel: Number;
    activityname: string;
    Granamout: number;
    ddd: number[];
    Manager: number;
    startdate: string;
    EndDate: string;
    aaaa: boolean;
    getAllProgramdetail(year) {
        this.costyear = year;
        this._programService.getAllProgramsDetailsIII(year).subscribe(result => {
            this.Record = result;
           
        })
    }
    createPdf(): void {
        let pdf = new jsPDF('l', 'pt', 'a4');
        let options = {
            pagesplit: true
        };
        pdf.addHTML(this.el.nativeElement, 0, 0, options, () => {
            pdf.save("test.pdf");
        });
    }

    exportToExcel(): void {
        this._programService.createExcelDocallprojectlistIII("Project-III", this.costyear).subscribe(result => {
            this._fileDownloadService.downloadTempFile1(result);
        })


    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.Record = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }

}