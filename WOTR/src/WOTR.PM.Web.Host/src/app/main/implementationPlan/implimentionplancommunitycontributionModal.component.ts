﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import {
    ExpenseDto, UnitofMeasureDto, Expensesimage,
    CheklistandSublistDto1, UnitOfMeasuresServiceProxy, ImplimantionChecklistServiceProxy, ProgramExpenseServiceProxy, ExpensesTypeDto,
    UploadFileDto, UploadExpensesFileAppserviceServiceProxy, RequestAAPlasListDto, ListActionSubactionListDto, ListActionSubactionListImplemenTationDto, ImplementationListDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
import * as moment from "moment";
import { Router, ActivatedRoute } from '@angular/router';
import { FileUploadModule } from 'primeng/primeng'
import { AppConsts } from '@shared/AppConsts';
import { FileUploader, FileUploaderOptions, Headers } from 'ng2-file-upload';
import { HttpClient, HttpRequest, HttpEventType } from '@angular/common/http';
import { IAjaxResponse } from "abp-ng2-module/src/abpHttpInterceptor";
import { finalize } from 'rxjs/operators';
declare var jQuery: any;


@Component({
    selector: 'ImplimentionplanCommunityContributionModal',
    templateUrl: "./implimentionplancommunitycontributionModal.component.html",
    styleUrls: ['./implimentionplancommunitycontributionModal.component.less'],
})
export class ImplimentionplanCommunityContributionModalComponent extends AppComponentBase {
    governmentcontributionreceived: string;
    @ViewChild('ImplimentionplanCommunityContributionModal') modal: ModalDirective;
    @ViewChild('ProjectExpensesModalforstatus') modalstatus: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('addNewItemModal') MyModal: ModalDirective;

    saving: boolean = true;


    active: boolean = false;



    requestList: RequestAAPlasListDto[] = [];
    requestSingleYearList: RequestAAPlasListDto = new RequestAAPlasListDto();
    AddSubActionList: ListActionSubactionListDto;
    List: ListActionSubactionListImplemenTationDto = new ListActionSubactionListImplemenTationDto();
    ImplementationList: ImplementationListDto = new ImplementationListDto() ;
    year: any = [];
    constructor(
        injector: Injector,
       
        private _ImplimantionChecklistServiceProxy: ImplimantionChecklistServiceProxy,
        private _ProgramExpensesServiceProxy: ProgramExpenseServiceProxy,
        private _ImplimantionChecklistService: ImplimantionChecklistServiceProxy,
        private _activatedRoute: ActivatedRoute,
      
        private http: HttpClient
    ) {
        super(injector);

    }
    Year: string;
    show(p): void {
        p.communityContributionRecived;
        p.communityContributionRecived
        this.ImplementationList = p;
       this.year = this.ImplementationList.costEstimationYear;
        this.active = true;
        this.modal.show();
        
    }
    ngOnInit() {
      
       
    }

   
    communitycontributionreceived: string = '';


    


    community: number;

    close(): void {
        this.modal.hide();
        this.active = false;
    }
    

    save() {
        this.saving = true;
        this._ImplimantionChecklistService.updatecommunitycontributionReceived(this.ImplementationList.id, this.ImplementationList.programID, this.year, this.ImplementationList.communityContributionRecived, this.ImplementationList.governmentContributionRecived)
            //.finally(() => this.saving = false).subscribe(result => {
            //    this.communitycontributionreceived = result;
            //    this.governmentcontributionreceived = result;
            //  var abc = result
              
            //    this.ImplementationList.communityContributionRecived = parseInt(abc);
            //    this.ImplementationList.governmentContributionRecived = parseInt(abc);
            //    this.notify.info(result[0]);
            //    this.modalSave.emit(null);
            //    this.close();

           // })
            .finally(() => this.saving = false).subscribe(result => {
                this.notify.info(this.l('Saved Successfully'));
                this.modalSave.emit(null);
                 this.close();
            });
    }
}

