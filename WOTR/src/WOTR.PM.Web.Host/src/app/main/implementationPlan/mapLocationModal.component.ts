﻿import { Component, Injector, ViewChild, ElementRef } from '@angular/core';
import { CreateOrEditChekListDto, ChekListsServiceProxy, SubActivityandIteams, ImplimantionChecklistServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';

declare var jQuery: any;


@Component({
    selector: 'mapLocationModal',
    templateUrl: "./mapLocationModal.component.html",
    styleUrls: ['./mapLocationModal.component.less'],
})
export class MapLocationModalComponent extends AppComponentBase {
    active: boolean = false;
    @ViewChild('mapLocationModal') modal: ModalDirective;
    cheklist: SubActivityandIteams[];
    Selectedcheklist: SubActivityandIteams;
    saving: boolean = false;
    List: any = [];
    selectedlist: any = [];

    private 
    constructor(
        injector: Injector,
        private _router: Router,
        private _chelistServiceProxies: ChekListsServiceProxy,
        private _ImplimantionChecklistService: ImplimantionChecklistServiceProxy
    ) {
        super(injector);

    }
    ngOnInit(): void {
    }
    ngAfterViewInit(): void {

    }

    show(res): void {
        this.cheklist = res;
        this.active = true;

        if (this.cheklist[0].selectediteams=[]) {


            this._ImplimantionChecklistService.mapSubActivityToImplimentionPlangetselected(this.cheklist[0].programID, this.cheklist[0].checklistID)


                .finally(() => { this.saving = false; })
                .subscribe((result) => {
                    this.List = result;
                    this.Selectedcheklist = this.List 
                
                     for (let i of result) {

                         this.selectedlist.push(i.checklistID);
                       
                    }
                    $.each(this.selectedlist, function (index, value) {
                        $("input[type='checkbox'][value=" + value + "]").prop("checked", true);
                    });
                  
                });


        }

        this.modal.show();
    }
    close(): void {        
        this.modal.hide();
        this.active = false;
    }

    AddItem(res, record, event) {
        if (event.target.checked) {
            this.Selectedcheklist = res;
            this.Selectedcheklist.selectediteams.push(record);
        }
        else {
            var index = this.Selectedcheklist.selectediteams.indexOf(record, 0);
            if (index > -1) {
                this.Selectedcheklist.selectediteams.splice(index, 1);
            }
        }
    }
    save() {
        this.saving = true;
        this._ImplimantionChecklistService.mapSubActivityToImplimentionPlan(this.Selectedcheklist)
            .finally(() => this.saving = false).subscribe(result => {
                this.notify.info("save successfully");
                this.close();

        })
    }
}