﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { ImplimantionChecklistServiceProxy, ImplementationListDto, ListActionSubactionListImplemenTationDto, PrgRequestAAPlasServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';
import { AddNewItemModalComponent } from './addNewItemModal.component';
import { MapLocationModalComponent } from './mapLocationModal.component';
import { AddQuestionaryModalComponent } from './addQuestionaryModal.component';
import { ImplimentionplanCommunityContributionModalComponent } from './implimentionplancommunitycontributionModal.component';

declare var jQuery: any;
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'implementationPlan',
    templateUrl: "./implementationPlan.component.html",
    styleUrls: ['./implementationPlan.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class ImplementationPlanComponent extends AppComponentBase {
    @ViewChild('addNewItemModal') myModalView: AddNewItemModalComponent;
    @ViewChild('addQuestionaryModal') myModalVieww: AddQuestionaryModalComponent;
    @ViewChild('UnitModal') UnitModal: ModalDirective;
    @ViewChild('mapLocationModal') myModalMapLocation: MapLocationModalComponent;
    @ViewChild('ImplimentionplanCommunityContributionModal') myModalView1: ImplimentionplanCommunityContributionModalComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    programName: string;
    programId: number;
    ImplementationList: ImplementationListDto[];
    List: ListActionSubactionListImplemenTationDto[];
    ListOfUnits: ListActionSubactionListImplemenTationDto[];
    QuarterUnits: ListActionSubactionListImplemenTationDto = new ListActionSubactionListImplemenTationDto();
    QuarterUnits1: ListActionSubactionListImplemenTationDto = new ListActionSubactionListImplemenTationDto();
    QuarterUnits2: ListActionSubactionListImplemenTationDto = new ListActionSubactionListImplemenTationDto();
    saving: boolean = false;
    actionSubActionList: ListActionSubactionListImplemenTationDto;
    communitycontributionreceived: string = '';

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _ImplimantionChecklistService: ImplimantionChecklistServiceProxy,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy) {
        super(injector);
    }


    ngOnInit(): void {
        this.programName = this._activatedRoute.snapshot.queryParams.ProgrameName,
            this.programId = this._activatedRoute.snapshot.queryParams.ProgrameId
        if (this.programId == undefined) {
            this.programId = this.appSession.user.programId
            }
            this.getAllImplementaionActionAreaRecord();
        this.QuarterName = "Quarter 1";
    }
    getcommunitycontributionreceived() {
    }

    funRegionSpending() {
        this._router.navigate(['/app/main/RegionSpending'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funProjectFunds() {
        if (this.isGranted('Pages.RequestAAPlaSend')) {
            this._router.navigate(['/app/main/ProjectFund'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
        else {
            this._router.navigate(['/app/main/ProjectFund'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }

            });
        }
    }
    funApproveAAPla() {
        if (this.isGranted('Pages.RequestAAPlaSend')) {
            this._router.navigate(['/app/main/ApproveAAPla'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
        else {
            this._router.navigate(['/app/main/RequestAAPla'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
    }

    rowClass(rowData) {
        if (rowData.quarter1PhysicalUnits != 0 || rowData.quarter2PhysicalUnits != 0 || rowData.quarter3PhysicalUnits != 0 || rowData.quarter4PhysicalUnits != 0) {
            return '';
        }
        else {
            return 'noExpander';
        }
    }

    QuarterName: string;
    QuarterId: number;

    DivideUnits(r, res, index) {
        this.QuarterId = index;
        this.QuarterName = r;
    }

    UnitChange(r, res, index) {

        if (r == 'Quarter 1') {
            res.quarter1FinancialRs = res.quarter1PhysicalUnits * res.unitCost;
        } else if (r == 'Quarter 2') {
            res.quarter2FinancialRs = res.quarter2PhysicalUnits * res.unitCost;
        } else if (r == 'Quarter 3') {
            res.quarter3FinancialRs = res.quarter3PhysicalUnits * res.unitCost;
        } else if (r == 'Quarter 4') {
            res.quarter4FinancialRs = res.quarter4PhysicalUnits * res.unitCost;
        }
    }

    saveChangedUnits(res) {
        if (res.totalUnits >= (+res.quarter1PhysicalUnits + +res.quarter2PhysicalUnits + +res.quarter3PhysicalUnits + +res.quarter4PhysicalUnits))
        {
            this.QuarterUnits2.id = res.id;
            this.QuarterUnits2.quarter1PhysicalUnits = res.quarter1PhysicalUnits;
            this.QuarterUnits2.quarter1FinancialRs = res.quarter1FinancialRs;
            this.QuarterUnits2.quarter2PhysicalUnits = res.quarter2PhysicalUnits;
            this.QuarterUnits2.quarter2FinancialRs = res.quarter2FinancialRs;
            this.QuarterUnits2.quarter3PhysicalUnits = res.quarter3PhysicalUnits;
            this.QuarterUnits2.quarter3FinancialRs = res.quarter3FinancialRs;
            this.QuarterUnits2.quarter4PhysicalUnits = res.quarter4PhysicalUnits;
            this.QuarterUnits2.quarter4FinancialRs = res.quarter4FinancialRs;
            this._ImplimantionChecklistService.updateQuarterUnitsById(this.QuarterUnits2).subscribe(res => {
                this.notify.success(this.l('Units successfully updated'));
            })

        }
        else {
            alert("Total unit must be equal or greater than addition of quarters unit");
        }
    }

    deleteUnit(del) {
        this.message.confirm(
            this.l('Delet Unit', del.Activity),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._ImplimantionChecklistService.deleteUnit(del.unitId)
                        .subscribe(() => {
                            this.notify.success(this.l('Successfully Deleted'));
                        });
                }
            });
    }
    totalUnits: number;
    TotalUnitsData: number;
    UnitTotalCostCal(res, quarteUnitslist, data) {
        this.totalUnits = 0;
        this.TotalUnitsData = 0;
        for (let p of quarteUnitslist) {
            this.totalUnits += p.quarter1PhysicalUnits;
        }
        if (res.quarterId == 1) {
            this.TotalUnitsData = data.quarter1PhysicalUnits;
        }
        else if (res.quarterId == 2) {
            this.TotalUnitsData = data.quarter2PhysicalUnits;
        }
        else if (res.quarterId == 3) {
            this.TotalUnitsData = data.quarter3PhysicalUnits;
        }
        else if (res.quarterId == 4) {
            this.TotalUnitsData = data.quarter4PhysicalUnits;
        }
        this.totalUnits += +res.quarter1PhysicalUnits;

        res.quarter1FinancialRs = res.quarter1PhysicalUnits * res.unitCost;
    }

    AddUnits(rec, res, data) {
        this.QuarterUnits = new ListActionSubactionListImplemenTationDto();
        this.QuarterUnits1 = new ListActionSubactionListImplemenTationDto();
        this.QuarterUnits1 = data;
        this.QuarterUnits.programID = data.programID;
        this.QuarterUnits.costEstimationYear = data.costEstimationYear;
        this.QuarterUnits.prgActionAreaActivityMappingID = data.prgActionAreaActivityMappingID;
        this.QuarterUnits.villageID = data.villageID;
        this.QuarterUnits.unitCost = data.unitCost;
        if (this.QuarterId == undefined) {
            this.QuarterUnits.quarterId = 1;
        }
        else {
            this.QuarterUnits.quarterId = this.QuarterId;
        }
        this.ListOfUnits = res;
        this.UnitModal.show();
    }

    SaveUnits() {
        if (this.QuarterUnits.quarter1PhysicalUnits != 0) {
            abp.ui.setBusy();
            this.saving = true;
            this._PrgRequestAAPlasService.createPrgUnitMapping(this.QuarterUnits).finally(() => this.saving = false)
                .subscribe(res => {
                    this.notify.info("Save Successfully");
                    if (this.QuarterUnits.villageID == null) {
                        this.QuarterUnits.villageID = 0;
                    }

                    this._ImplimantionChecklistService.getQuarterWisePlan(this.QuarterUnits.programID,
                        this.QuarterUnits.quarterId,
                        this.QuarterUnits.prgActionAreaActivityMappingID,
                        this.QuarterUnits.villageID).subscribe(result => {
                            for (let l of this.ListOfUnits) {
                                for (var i = 0; i < result.length; i++) {
                                    if (l.unitId == result[i].unitId) {
                                        result.splice(i, 1);
                                    }
                                }
                            }
                            for (let list of result) {
                                this.ListOfUnits.push(list);
                            }
                        });
                    abp.ui.clearBusy();
                    this.close();
                });
        }
        else {
            this.notify.warn("Please Add Units");
        }
    }

    cal(rec, res) {
        rec.quarter1FinancialRs = res.unitCost * rec.quarter1PhysicalUnits;
    }

    GetSubActivity(res, rec) {
        this.actionSubActionList = res;
        this.actionSubActionList.prgActionAreaActivityMappingID = rec.prgActionAreaActivityMappingID;
    }

    GetSubActivityUnitWise() {
        abp.ui.setBusy();
        this._ImplimantionChecklistService.getSubActivityUnitWise(this.actionSubActionList.unitId).subscribe(result => {
            this.actionSubActionList.subUnitActivityQuarterWise = result;
            this._ImplimantionChecklistService.remainingSubActivitiesMap(this.actionSubActionList.prgActionAreaActivityMappingID, this.actionSubActionList.unitId)
                .subscribe(result => {
                    this.actionSubActionList.remainingSubActivity = result;
                    abp.ui.clearBusy();
                });
        });
        this.getAllImplementaionActionAreaRecord();

    }

    getAllImplementaionActionAreaRecord() {
        abp.ui.setBusy();
        this._ImplimantionChecklistService.getImplementionPlan(this.programId)
            .subscribe(result => {
                this.ImplementationList = result;
                this.modalSave.emit(null);
                abp.ui.clearBusy();
            });
    }
    close(): void {
        this.UnitModal.hide();
    }


    editimplementationplan(res, index, UnitId, quaterId) {
        this.myModalView.show(res, index, UnitId, quaterId);
    }
    editimplementationplanquarter2(res, index, UnitId1, quaterId2) {
        this.myModalView.show(res, index, UnitId1, quaterId2);
    }

    editimplementationplanquarter3(res3, index3, UnitId3, quaterId3) {
        this.myModalView.show(res3, index3, UnitId3, quaterId3);
    }

    editimplementationplanquarter4(res, index, UnitId4, quaterId4) {
        this.myModalView.show(res, index, UnitId4, quaterId4);
    }

    deleteImplementationchecklist(del) {
        this._ImplimantionChecklistService.deleteImplimentationChecklist(del.implementationPlanCheckListID)
            .subscribe(() => {
                this.notify.success(this.l('Successfully Deleted Record'));
                this.getAllImplementaionActionAreaRecord();
            });

    }
}