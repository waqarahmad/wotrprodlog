﻿import { Component, Injector, ViewChild } from '@angular/core';
import {
    ImplimantionChecklistServiceProxy, LoginAndroidServiceServiceProxy, ImplimentaionChecklistDto,
    GetQuestionandAnswerBySubActivityDto, UpdateQuestionaryStatus, UpdateQuestionaryStatusStatus,
    PushNotifiactionServiceProxy, AppDeviceListsDto,
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    selector: 'addQuestionaryModal',
    templateUrl: "./addQuestionaryModal.component.html",
    styleUrls: ['./addQuestionaryModal.component.less'],
})

export class AddQuestionaryModalComponent extends AppComponentBase {
    questionList: GetQuestionandAnswerBySubActivityDto[];
    AppDeviceListsDto: AppDeviceListsDto[];
    textquestionaryList: any = [];
    checkboxquestionaryList: any = [];
    vdosList: any = [];
    imglist: any = [];
    radioquestionaryList: any = [];
    active: boolean = false;
    updateImplentationID: number;
    AcivementUnit: number;
    userId: number;
    checklistItemName: string;
    UpdateQuestionaryStatusList: UpdateQuestionaryStatus[] = [];
    UpdateQuestionaryStatus: UpdateQuestionaryStatus;
    flag: number;
    @ViewChild('addQuestionaryModal') modal: ModalDirective;
    @ViewChild('modal') modal123;
    ImplimantationSubActivityList: ImplimentaionChecklistDto = new ImplimentaionChecklistDto();
    pdflist = [];
    ImageList = ["../../../assets/common/images/logo.png"];

    vdosList1: any = [];
    imglist1: any = [];
    pdflist1: any = [];
    vdosList2: any = [];
    imglist2: any = [];
    pdflist2: any = [];
    status: any;
    managerID: any;
    Nodata: string;

    constructor(
        injector: Injector,
        private _ImplimantionChecklistService: ImplimantionChecklistServiceProxy,
        private _LoginAndroidService: LoginAndroidServiceServiceProxy,
        private _pushNotificationService: PushNotifiactionServiceProxy) {
        super(injector);
    }

    addNewChoice() {
        this.ImageList.push();
    }


    show(p, rec): void {
        this.managerID = rec.managerID;
        this.updateImplentationID = p.implementationPlanCheckListID;
        this.AcivementUnit = p.units;
        this.getQuestionary(p.checklistItemID, p.implementationPlanCheckListID);
        this.userId = p.assingUserId;
        this.checklistItemName = p.checklistItemName;
        this.modal.show();
    }

    getQuestionary(subId, implementationplanId) {
        abp.ui.setBusy();
        this._LoginAndroidService.getQuestionaryBySubActivityandAnswer(subId, implementationplanId).subscribe(result => {
            this.questionList = result;
            if (this.questionList.length > 0) {
                this.status = this.questionList[0].status;
            }
            this.textquestionaryList = [];
            this.checkboxquestionaryList = [];
            this.radioquestionaryList = [];
            this.imglist = [];
            this.vdosList = [];
            this.pdflist = [];
            if (result.length != 0) {
                for (let i of result) {
                    if (i.inputType == 'InputBox') {
                        this.textquestionaryList.push(i);
                        for (let img of i.url) {
                            var extension = img.url.split('.').pop();

                            if (extension === 'png') {
                                this.imglist.push({ "url": img.url, "urlTableId": img.urlTableId });
                            }
                            else if (extension === 'mp4') {
                                this.vdosList.push(img);
                            }
                            else {
                                this.pdflist.push(img);
                            }
                        }
                    }

                    if (i.inputType == 'CheckBox') {
                        this.checkboxquestionaryList.push(i);
                        for (let img1 of i.url) {
                            var extension = img1.url.split('.').pop();

                            if (extension === 'png') {
                                this.imglist1.push({ "url": img1.url, "urlTableId": img1.urlTableId });
                            }
                            else if (extension === 'mp4') {
                                this.vdosList1.push(img1);
                            }
                            else {
                                this.pdflist1.push(img1);
                            }
                        }
                    }
                    if (i.inputType == 'RadioBox') {
                        this.radioquestionaryList.push(i);
                        for (let img2 of i.url) {
                            var extension = img2.url.split('.').pop();

                            if (extension === 'png') {
                                this.imglist2.push({ "url": img2.url, "urlTableId": img2.urlTableId });
                            }
                            else if (extension === 'mp4') {
                                this.vdosList2.push(img2);
                            }
                            else {
                                this.pdflist2.push(img2);
                            }
                        }
                    }


                }
            }
            else {
                this.Nodata = "No Questionnaire Assign";
            }
            abp.ui.clearBusy();
        });
    }

    saving: boolean = false;

    save() {
        this.saving = true;
        this._ImplimantionChecklistService.createOrEditImplementionCheckList(this.ImplimantationSubActivityList)
            .finally(() => this.saving = false).subscribe(result => {
                this.notify.info(result[0]);
                this.close();
            });
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }

    approval() {
        for (let i of this.questionList) {

            if (i.questionarymappngId > 0) {

                this.UpdateQuestionaryStatus = new UpdateQuestionaryStatus();

                this.UpdateQuestionaryStatus.questionarymappngId = i.questionarymappngId;
                if (this.flag == 0) {
                    this.UpdateQuestionaryStatus.status = UpdateQuestionaryStatusStatus._3;
                } else {
                    this.UpdateQuestionaryStatus.status = UpdateQuestionaryStatusStatus._2;
                }

                this.UpdateQuestionaryStatus.implementaionID = this.updateImplentationID;
                this.UpdateQuestionaryStatus.unit = this.AcivementUnit;
                this.UpdateQuestionaryStatusList.push(this.UpdateQuestionaryStatus);
            }

        }
        this.saving = true;
        this._ImplimantionChecklistService.updateSubActivityQuestionaryStatus(this.UpdateQuestionaryStatusList)
            .finally(() => this.saving = false).subscribe(result => {
                this.notify.info(result[0]);
                this.close();
            });

        var msg = "";
        if (this.flag == 0) {
            msg = this.checklistItemName + " Approve ";
        }
        else {
            msg = this.checklistItemName + " Reject ";
        }
        this._pushNotificationService.sendPushNotification(this.userId.toString(), msg, "Notification").subscribe(
            result => {
                this.close();
            });
    }
}
