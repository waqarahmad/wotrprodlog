﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import {
    ImplimantionChecklistServiceProxy, CheklistandSublistDto1, PrgManagerImplentationListDto,
    UnitOfMeasuresServiceProxy,ImplimentaionChecklistDto, UnitofMeasureDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import * as moment from "moment";

@Component({
    selector: 'addNewItemModal',
    templateUrl: "./addNewItemModal.component.html",
    styleUrls: ['./addNewItemModal.component.less'],
})
export class AddNewItemModalComponent extends AppComponentBase {
    active: boolean = false;
    @ViewChild('addNewItemModal') myModal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    selectedCheckListItem: CheklistandSublistDto1[];
    managerList: PrgManagerImplentationListDto[];
    unitOfMeasureList: UnitofMeasureDto[];
    ImplimantationSubActivityList: ImplimentaionChecklistDto = new ImplimentaionChecklistDto();

    constructor(
        injector: Injector,
        private _ImplimantionChecklistService: ImplimantionChecklistServiceProxy,
        private _UnitOfMeasureService: UnitOfMeasuresServiceProxy

    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.getAllUnitOfMeasure();
        
        
    }
    ngAfterViewInit(): void {
        this.dateTime();
    }

    getAllUnitOfMeasure() {
        this._UnitOfMeasureService.getAllUnitOfMeasure().subscribe(result => {
            this.unitOfMeasureList = result;
        });
    }

    show(res, index, UnitId, quaterId): void {
        this.active = true;
        if (index == 0) {
            this.managerList = res.managers;
            this.ImplimantationSubActivityList = new ImplimentaionChecklistDto();
            this.ImplimantationSubActivityList.programID = res.programID;
            this.ImplimantationSubActivityList.locationId = res.locationID;
            this.ImplimantationSubActivityList.checklistID = res.checklistID;
            this.ImplimantationSubActivityList.prgActionAreaActivityMappingID = res.prgActionAreaActivityMappingID;
            this.ImplimantationSubActivityList.costEstimationYear = res.costEstimationYear;
            this.ImplimantationSubActivityList.programQuqterUnitMappingID = UnitId;
            this.ImplimantationSubActivityList.quaterId = quaterId;
            $("#ProgramStartDate").val("");
            $("#ProgramEndDate").val("");
            this._ImplimantionChecklistService.getMapsubActivityForImplentionPlan(res.prgActionAreaActivityMappingID)
                .subscribe(result => {
                    this.selectedCheckListItem = result;
                    this.myModal.show();
                });
        }
        else {
            this.managerList = res.managers;
            this.ImplimantationSubActivityList = new ImplimentaionChecklistDto();
            this.ImplimantationSubActivityList.programID = res.programID;
            this.ImplimantationSubActivityList.locationId = res.locationID;
            this.ImplimantationSubActivityList.checklistID = res.checklistID;
            this.ImplimantationSubActivityList.prgActionAreaActivityMappingID = res.prgActionAreaActivityMappingID;
            this.ImplimantationSubActivityList.costEstimationYear = res.costEstimationYear;
            this.ImplimantationSubActivityList.programQuqterUnitMappingID = UnitId;
            this.ImplimantationSubActivityList.quaterId = quaterId;
            this.ImplimantationSubActivityList.id = index.implementationPlanCheckListID;
            this.ImplimantationSubActivityList.description = index.description;
            this.ImplimantationSubActivityList.cheklistStartDate = index.cheklistStartDate;
            this.ImplimantationSubActivityList.cheklistEndDate = index.cheklistEndDate;
            this.ImplimantationSubActivityList.units = index.units;
            this.ImplimantationSubActivityList.chekListIteamName = index.checklistItemName;
            this.ImplimantationSubActivityList.unitOfMeasuresID = index.unitOfMeasuresID;
            this.ImplimantationSubActivityList.checklistItemID = index.checklistItemID;
            this.ImplimantationSubActivityList.assingUserId = index.assingUserId;

            $("#ProgramEndDate").val("" + moment(this.ImplimantationSubActivityList.cheklistEndDate).format('MM/DD/YYYY'));
            $("#ProgramStartDate").val("" + moment(this.ImplimantationSubActivityList.cheklistStartDate).format('MM/DD/YYYY'));


            this._ImplimantionChecklistService.getMapsubActivityForImplentionPlan(res.prgActionAreaActivityMappingID)
                .subscribe(result => {
                    this.selectedCheckListItem = result;
                    this.myModal.show();
                });
        }
    }

    dateTime() {
        $(function () {
            $("#ProgramStartDate").datetimepicker({
                format: 'MM/DD/YYYY'
            });
        });

        $("#ProgramStartDate").on("dp.change", () => {
            var dateString = $("#ProgramStartDate").val();
            var dateObj = new Date(dateString.toString());
            this.ImplimantationSubActivityList.cheklistStartDate = moment(dateObj).add(1,'day');
        });

        $(function () {
            $("#ProgramEndDate").datetimepicker({
                format: 'MM/DD/YYYY'
            });
        });

        $("#ProgramEndDate").on("dp.change", () => {
            var dateString = $("#ProgramEndDate").val();
            var dateObj = new Date(dateString.toString());
            this.ImplimantationSubActivityList.cheklistEndDate = moment(dateObj).add(1,'day');
        });
    }
    saving: boolean = false;

    save() {
        this.saving = true;
        if (this.ImplimantationSubActivityList.cheklistStartDate >= this.ImplimantationSubActivityList.cheklistEndDate) {
            this.notify.info(this.l("Item Start Date should be greater than Item End Date"));
            this.saving = false;
            return false;
        }
        if (this.ImplimantationSubActivityList.cheklistStartDate == undefined || this.ImplimantationSubActivityList.cheklistEndDate == undefined) {
            this.notify.info(this.l("Please Select Date"));
            this.saving = false;
            return false;
        }

        this._ImplimantionChecklistService.createOrEditImplementionCheckList(this.ImplimantationSubActivityList)
            .finally(() => this.saving = false).subscribe(result => {
                this.notify.info(result[0]);
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.myModal.hide();
        this.active = false;
    }
}