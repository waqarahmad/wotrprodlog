﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { RoleServiceProxy, State, LocationServiceProxy, District,Taluka} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalTalukaComponent } from './ModalTaluka.component';

//import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    selector: 'talukacomponent',
    templateUrl: "./taluka.component.html",
    styleUrls: ['./taluka.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class TalukaComponent extends AppComponentBase {    
    @ViewChild('ModalTaluka') myModalView: ModalTalukaComponent;
    selectedPermission = '';
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    Record: State[] = [];
    district: District = new District();
    districtlist: District[] = [];
    taluka: Taluka[] = [];
    talukalist: Taluka[] = [];
    active: boolean = false;
    list: any = [];
    searchText;
    timer: any;
    constructor(
        injector: Injector,
        private _locationService: LocationServiceProxy,

        private _roleService: RoleServiceProxy)
    {
        super(injector);
    }

    ngOnInit() {
        this.getalltaluka(); 
        this.getalltalukasearch();
    }    
    getalltalukasearch(): void {   
       // var input = (document.getElementById("txtActionArea") as HTMLInputElement).value;
        var input = this.searchText.trim();
        abp.ui.setBusy();
        this._locationService.getallTalukas(input).subscribe((result) => {       
            this.primengDatatableHelper.totalRecordsCount = result.length;
           
            this.primengDatatableHelper.hideLoadingIndicator();
            this.primengDatatableHelper.records = result;
         
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.talukalist = result;
            this.taluka = result;
            this.talukalist = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        });
    }

    searchPC(): void {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => { this.getalltalukasearch() }, 1000);
    }

    getalltaluka(): void {
        var input = (document.getElementById("txtActionArea") as HTMLInputElement).value;
        abp.ui.setBusy();
        this._locationService.getallTalukas(input).subscribe((result) => {
          
            this.primengDatatableHelper.records = result;
            this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.talukalist = result;
            this.taluka = result;
            this.talukalist = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
            abp.ui.clearBusy();
        });
    }
    editActionArea(p) {
        
        this.myModalView.show(p);
    }
    
        count: number = -1;
        finalUnits: number = 0;
    sumOfTotal: number = 0;
    sumOfTotal1: number = 0;
    DivideUnits(res, rec) {
    }
    
    deletedistrict(del) {
        this.message.confirm(
            this.l('TalukaDeleteWarning', del.talukaName),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._locationService.deleteTaluka(del.id)
                        .subscribe((result) => {
                            if (result == "Record Exist") {
                                this.notify.success(this.l('This record is already in used'));
                                this.getalltaluka();
                            }
                            else {
                                this.notify.success(this.l('Successfully Deleted Action Area Record'));
                                this.getalltaluka();
                            }
                        });
                }
            })
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.taluka.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.talukalist = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);

    }
   
}