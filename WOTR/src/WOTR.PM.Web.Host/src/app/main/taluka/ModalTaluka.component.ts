﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { ActionAreasServiceProxy, ActionAreasListDto, State, LocationServiceProxy, LocationListDto, District, Taluka, Village } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
declare var jQuery: any;
@Component({
    selector: 'ModalTaluka',
    templateUrl: "./ModalTaluka.component.html",
    styleUrls: ['./ModalTaluka.component.less'],
})
export class ModalTalukaComponent extends AppComponentBase {
    @ViewChild('ModalTaluka') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    passwordComplexityInfo = '';
    district: District = new District();
    taluka: Taluka = new Taluka();

    saving: boolean = false;
    search: ActionAreasListDto[] = [];
    StateList: State[];
    districtlist: District[];
    location: LocationListDto;
    locationList: LocationListDto[];
    stateList: State[];
    districtList: District[];
    talukaList: Taluka[];
    villageList: Village[];
    @ViewChild('projectForm') eForm: NgForm;//yo

    constructor(
        injector: Injector,
        private _actionareaService: ActionAreasServiceProxy,
        private _locationService: LocationServiceProxy,
)
    {
        super(injector);        
    }
    ngAfterViewInit() {
        this.getAllstate();
    }
    show(p): void {
        this.active = true;
        if (p != undefined) {
            this.taluka.districtId = p.districtId;
            this.getAllDistrict(p.stateId)
            this.taluka = p;
            $('#lblTitle').text('Edit Taluka');
        }
        else {
            this.taluka = new Taluka();
        }
        this.modal.show();
    }

    close(): void {
        this.modal.hide();
        this.active = false;       
    }
   
    save(): void {
         this.saving = true;
        var r = this._locationService.creatOrUpdateTaluka(this.taluka)
            .finally(() => this.saving = false)
            .subscribe((result) => {
                if (result[0] == "Record alread Present!") {
                    this.notify.warn(this.l(result[0]));
                }
                if (result[0] == "ActionArea Update  sucessfully !" || result[0] == "ActionArea Added  sucessfully !") {
                    this.notify.info(this.l(result[0]));
                }
                this.close();
                this.modalSave.emit(null);
            });
    }
    getAllstate() {
        this._locationService.getAllState().subscribe(result => {
            this.stateList = result;
        })
    }
   
    StateWiseDistrict(stateId) {
        this._locationService.getAllDistrict(stateId).subscribe(result => {
            this.districtlist = result;
        })
    }
    onChangeState(event) {
        this.talukaList = [];
        this.getAllDistrict(event.target.value);
    }
    getAllDistrict(stateId) {
        this._locationService.getAllDistrict(stateId).subscribe(result => {
            this.districtList = result;
        })
    }

}