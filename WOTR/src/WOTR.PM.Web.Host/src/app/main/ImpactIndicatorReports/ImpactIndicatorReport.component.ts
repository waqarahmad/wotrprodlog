﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import {  Village, PrgRequestAAPlasServiceProxy, ProgramServiceProxy, ProgramInformationListDto,  ImpactIndicatorResponseServiceProxy, AddFormResponseDto, ProjectReportInputDto, ImpactIndicatorForm, GetAllResponseReportDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { ActivatedRoute } from '@angular/router';
import { FileDownloadService } from '@shared/utils/file-download.service';


@Component({
    selector: 'ImpactIndicatorReportComponent',
    templateUrl: "./ImpactIndicatorReport.component.html",
    styleUrls: ['./ImpactIndicatorReport.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})

export class ImpactIndicatorReportComponent extends AppComponentBase {
    frequencyOfOccurrence: any;
    @ViewChild('test') el: ElementRef;

    programdetails: ProgramInformationListDto[];
    projectReportInputDto: ProjectReportInputDto = new ProjectReportInputDto();

    VillageList: Village[] = [];
    GetAllResponseReportDtoList: GetAllResponseReportDto[] = [];
    
    itemList2: any;
    selectedItems2: any;
    settings2: any;

    ProgramTitle: string = "";
    VillageName: string = "";
    personName: string = "";

    VillageIds:number[] = []; 
    ImpactIndicatorFormList: ImpactIndicatorForm[] =[];
    addFormResponseDto: AddFormResponseDto = new AddFormResponseDto();
    programInformationListDto:  ProgramInformationListDto = new ProgramInformationListDto();
    FormResponseId: number;
    name: any;


    constructor(
        injector: Injector,
        private _ProgramServiceProxy: ProgramServiceProxy,
        private _activatedRoute: ActivatedRoute,
        private _programService: ProgramServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy,
    private _ImpactIndicatorResponseServiceProxy: ImpactIndicatorResponseServiceProxy,) {
        super(injector);
    }

    ngOnInit() {
        this.getAllProgram();
        this.itemList2 = [];
        this.settings2 = {
            singleSelection: false,
            text: "Select Villages",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        };
    }

    getAllProgram() {
        this._ImpactIndicatorResponseServiceProxy.getAllProgramForReport().subscribe(result => {
            this.programdetails = result;
        });
    }

    OnProjectSelect(event, respId?: number, progId?: number) {
        this.GetAllResponseReportDtoList = [];
        if (!respId) {
            this.bindAllVillages(event.target.value);
            //this.bindForms(progId);
        } else {
            this.bindAllVillages(progId);
            //this.bindForms(progId);
        }
    }

    bindAllVillages(ProjId) {
        this.itemList2 = [];
        this.selectedItems2 = [];
        this._ImpactIndicatorResponseServiceProxy.getVillagesByProgramIdForReport(ProjId).subscribe(result => {
            this.VillageList = result;
            for (let p of result) {
                this.itemList2.push({
                    id: p.id,
                    itemName: p.name
                })
            }
        });
    }

    bindForms() {
        debugger;
        this._ImpactIndicatorResponseServiceProxy.getFormsForReportByProgramIdAndVillageId(this.projectReportInputDto.programId, this.VillageIds).subscribe(result => {
            this.ImpactIndicatorFormList = result;
        });
    }

    onItemSelect2(item: any) {
        var village = new Village();
        village.id = item.id;
        this.VillageIds.push(village.id);
        this.bindForms();
    }

    OnItemDeSelect2(item: any) {
        var index = this.VillageIds.findIndex(t => t == item.id);
        if (index > -1) {
            this.VillageIds.splice(index, 1);
        }
        this.bindForms();
    }

    onSelectAll2(items: any) {
        for (let obj of items) {
            var village = new Village();
            village.id = obj.id;
            this.VillageIds.push(village.id);
        }
        this.bindForms();
    }

    onDeSelectAll2(items: any) {
        this.selectedItems2 = [];
        this.VillageIds = [];
        this.bindForms();
    }

    getReportDeatils(event) {

        this.projectReportInputDto.villageIds = this.VillageIds;
        this._ImpactIndicatorResponseServiceProxy.getImpactIndicatorDetailsForReport(this.projectReportInputDto.programId, this.projectReportInputDto.villageIds, this.projectReportInputDto.formId).subscribe(result => {
            this.GetAllResponseReportDtoList = result;
        });

        this._ProgramServiceProxy.getProgramDetailsByProgramID(this.projectReportInputDto.programId).subscribe(result => {
            this.ProgramTitle = result.programName;
        });

        this._ImpactIndicatorResponseServiceProxy.getVillageNamesById(this.projectReportInputDto.villageIds).subscribe(result => {
            this.VillageName = result.join(",");
        });
    }

    exportToExcel() {
        abp.ui.setBusy();
      if (this.VillageIds.length > 0) {
          this.projectReportInputDto.villageIds = this.VillageIds;
          this._ImpactIndicatorResponseServiceProxy.createImpactIndicatorReportExcelDocVersion1("ImpactIndicatorReport", this.projectReportInputDto).subscribe(
                (result) => {
                    this._fileDownloadService.downloadTempFile1(result);
                    abp.ui.clearBusy();
                });
        } else {
            this.notify.info("Please select village");
        }
    }
}