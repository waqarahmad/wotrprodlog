﻿
import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { ActionAreasServiceProxy, ActionAreasListDto, DonorServiceProxy, DonorListDto,CountryListDto} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';

import { MysortPipe } from '../../shared/common/mysort.pipe';//yo //modified for Sorting Drop Downs
import * as moment from 'moment';
declare var jQuery: any;
@Component({
    selector: 'ModalDonor',
    templateUrl: "./donorsModal.component.html",
    styleUrls: ['./donorsModal.component.less'],
})
export class ModalDonorComponent extends AppComponentBase {
    @ViewChild('ModalDonor') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;   
    saving: boolean = false;
    donarList: DonorListDto;
    countrylist: CountryListDto[]=[];
    
    constructor(

        injector: Injector,    
        private _donarService: DonorServiceProxy
    ) {
        super(injector);

    }
    ngOnInit() {
        this.donarList = new DonorListDto();
        this.getallcountry();
    }
    show(e): void {  
        this.active = true;
        if (e != undefined) {
            $('#lblTitle').text('Edit Funding Agency')
            this.donarList = e;
        }
        else {            
            $('#lblTitle').text('Add Funding Agency')
            this.donarList = new DonorListDto();
        }
        this.modal.show();
    }
    save() {
        this.saving = true;       
        this.donarList.amountReceivedDateTime = moment(new Date());
        this._donarService.createOrUpdateDoner(this.donarList)
            .finally(() => this.saving = false)
            .subscribe(result => {
                if (result[0] == "Record Add in program Table Sucessfully" || result[0]=="Record UpdateSucessfully in program Table") {
                    this.notify.info(result[0]);
                   
                }
                if (result[0] == "Record is already Present") {
                    this.notify.warn(result[0]);
                }
                this.donarList = new DonorListDto();
                this.close();
                this.modalSave.emit(null);
            })
    }

    getallcountry() {
        this._donarService.getAllCountry().subscribe((result) => {
            this.countrylist = result;
        })
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
}