﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { DonorListDto, DonorServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalDonorComponent } from './donorsModal.component';



declare var jQuery: any;
@Component({
    selector: 'donorscomponent',
    templateUrl: "./donors.component.html",
    styleUrls: ['./donors.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class DonorsComponent extends AppComponentBase {
    @ViewChild('ModalDonor') myModalDoner: ModalDonorComponent;
    doner: DonorListDto[] = [];
    donarList: DonorListDto[]=[];
    saving: boolean = false;
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    filterText = "";
    constructor(
        injector: Injector,
        private _donarService: DonorServiceProxy,
       private _activatedRoute: ActivatedRoute


    ) {
        super(injector);
        this.filterText = this._activatedRoute.snapshot.queryParams['filterText'] || '';


    }
    ngAfterViewInit(): void {
        this.dateTime();
    }
    getAllDonerList() {

    }
    ngOnInit() {
        //this.donarList = new DonorListDto();
        this.getallDoner();
    }

    getallDoner() {
        var input = (document.getElementById("FilterEmployeeText") as HTMLInputElement).value;
        this._donarService.getAllDonorsearch(input).subscribe((result) => {
            this.filterText,
            this.donarList = result;
            this.primengDatatableHelper.records = result;
            this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.doner = result;
            this.donarList = result;
            this.donarList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
        })
    }

    dateTime() {
        $(function () {
            $("#txtBirthDate").datetimepicker();
        });      
    }
    editDoner(editRecord) {
        this.myModalDoner.show(editRecord)
    }

    deleteDoner(del) {
        this.message.confirm(
            this.l('FundingAgencyDeleteWarningMessage', del.companyName),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._donarService.deleteDonor(del.id)
                        .subscribe((result) => {
                            if (result == "Record Exist") {
                                this.notify.success(this.l('This record is already in used'));
                                this.getallDoner();
                            }
                            else {
                                this.notify.success(this.l('Successfully Deleted  Record'));
                                this.getallDoner();
                            }
                        });
                }
            })

    }

    
    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.doner.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.donarList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
    }

}