﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';
import { Calendar } from 'primeng/components/calendar/calendar';
import { CapitalizePipe } from '../../shared/common/capitalize.pipe';
import {
    ProgramInformationListDto, ProgramServiceProxy, UserServiceProxy,
    GetAllManagerDto, DonorServiceProxy, DonorListDto, LocationListDto, LocationServiceProxy
} from '@shared/service-proxies/service-proxies';
import * as moment from "moment";
declare var google: any;
declare var jQuery: any;

@Component({
    selector: 'programInformation',
    templateUrl: "./programInformation.component.html",
    styleUrls: ['./programInformation.component.less'],
    animations: [appModuleAnimation()]
})
export class ProgramInformationComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    locationList: LocationListDto[];
    donarList: DonorListDto[];
    managers: GetAllManagerDto[] = [];
    programInfo: ProgramInformationListDto = new ProgramInformationListDto();
    filteredListManager: number[] = [];
    saving: boolean = true;
    date?: Date;
    selected: string[] = [];
    query: string;
    RRcList: any;
    RRcId: number[];
    VillageClusterId: number[];
    RRcSingleId: any;
    CheckIndex: any;
    vcIds: any;
    VillageId: any;
    villageClusterList: any;
    villageClusterList2: LocationListDto[];
    villagelist2: LocationListDto[];
    villageList: any;
    map: any;
    clickiteam: number[];
    clickname: string[];
    selectedItems: any;
    selectedItemsVC: any;
    selectedItemsV: any;
    @ViewChild('p-calendar') calendar: Calendar;
    id: number[];
    ImageUrl: string = "";
    tileColorPickerValue: string = "#4e42f4";
    colorPickerValue: string = '#ffffff';

    constructor(
        injector: Injector,
        private _router: Router,
        private _programService: ProgramServiceProxy,
        private _userService: UserServiceProxy,
        private _donarService: DonorServiceProxy,
        private _locationService: LocationServiceProxy,
        private _activatedRoute: ActivatedRoute, ) {
        super(injector);
    }

    ngOnInit() {
        var ProgrammeId = this._activatedRoute.snapshot.queryParams.ProgrameId;
        if (ProgrammeId != undefined) {
            this._programService.getProgramDetailsByProgramID(parseInt(ProgrammeId))
                .finally(() => { this.saving = false; })
                .subscribe((result) => {
                    this.programInfo = result;
                    this.programInfo.id = result.id;
                    this.programInfo.programManagerId = result.programManagerId;

                    $("#ProgramEndDate").val("" + moment(this.programInfo.prgramEndDate).format('MM/DD/YYYY'));
                    $("#ProgramStartDate").val("" + moment(this.programInfo.programStartDate).format('MM/DD/YYYY'));


                   // $("#ProgramEndDate").val("" + moment(this.programInfo.prgramEndDate).format('MM/DD/YYYY'));
                    //$("#ProgramStartDate").val("" + moment(this.programInfo.programStartDate).format('MM/DD/YYYY'));
                    $("#ProgramSubmitDate").val("" + moment(this.programInfo.programSubmitDate).format('MM/DD/YYYY'));
                    $("#ProgramApprovaltDate").val("" + moment(this.programInfo.programApprovaltDate).format('MM/DD/YYYY'));
                    if (this.programInfo.programBadgeImage != null)
                        this.onImageClick(this.programInfo.programBadgeImage);
                });
        }
    }

    ngAfterViewInit(): void {
        this.getAllManagers();
        this.getAllPrograms();
        this.getAllDonar();
        this.getAllLocation();
        this.funChecked();
        this.funImageSlider();
        this.funRedioButton();
        this.programInfo.locationID = [];
        this.RRcId = [];
        this.VillageClusterId = [];
        this.VillageId = [];
        this.dateTime();
    }


    onChange(item) {
        for (let index in this.managers) {
            if (this.managers[index].managerId == item) {
                this.selected.push(this.managers[index].name + "-" + this.managers[index].managgerLocation);
                this.filteredListManager.push(this.managers[index].managerId);
                break;
            }
        }
        this.query = '';
    }

    remove(item) {
        this.selected.splice(this.selected.indexOf(item), 1);
    }

    getAllManagers(): void {
        this._userService.getAllManager().subscribe((result) => {
            this.managers = result;
        })
    };



    onImageClick(stringImage) {
        this.programInfo.programBadgeColor = "#ffffff";
        this.programInfo.programBadgeImage = stringImage;
        this.ImageUrl = stringImage;
        var canvas = <HTMLCanvasElement>document.getElementById('cvsTile');

        var ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        var base_image = new Image();
        base_image.src = stringImage;
        base_image.onload = function () {
            var wrh = base_image.width / base_image.height;
            var newWidth = canvas.width / 2;
            var newHeight = newWidth / wrh;
            if (newHeight > canvas.height / 2) {
                newHeight = canvas.height / 2;
                newWidth = newHeight * wrh;
            }
            var xOffset = newWidth < canvas.width ? ((canvas.width - newWidth) / 2) : 0;
            var yOffset = newHeight < canvas.height ? ((canvas.height - newHeight) / 2) : 0;
            ctx.globalCompositeOperation = "source-over";
            ctx.fillStyle = '#ffffff';
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(base_image, xOffset, yOffset, newWidth, newHeight);
        }
    }

    getAllDonar() {
        this._donarService.getAllDonor().subscribe(result => {
            this.donarList = result;
        })
    }
    itemList: any;
    getAllLocation() {
        this.itemList = [];
        this._locationService.getAllLocations().subscribe(result => {
            this.locationList = result;
            this.RRcList = [];
            this.villageClusterList = [];
            this.villageList = [];
       
            for (let rrc of result) {
                this.itemList.push({
                    id: rrc.id,
                    itemName: rrc.name
                })
            }
        })
    }

    getVillageCluster(value) {
        this._locationService.getRespeactedLocation(value, 3).subscribe(result => {
            this.villageClusterList2 = result;
        });
    }

    checkedVillageCluster() {
        for (let v of this.VillageClusterId) {
            (<HTMLInputElement>document.getElementById("vc_" + v)).checked = true;
        }
    }

    checkedVillage() {
        for (let v of this.VillageId) {
            (<HTMLInputElement>document.getElementById("v_" + v)).checked = true;
        }
    }

    getvillage(val) {
        this._locationService.getRespeactedLocation(val, 4).subscribe(result => {
            this.villagelist2 = result;
        })
    }

    ActivityId: number[] = [];
    onItemSelect(item: any) {
        this.RRcId.push(item.id);
    }
    OnItemDeSelect(item: any) {
        var index = this.RRcId.indexOf(item.id, 0);
        if (index > -1) {
            this.RRcId.splice(index, 1);
        }
    }
    onSelectAll(items: any) {
        for (let obj of items) {
            this.RRcId.push(obj.id);
        }
    }
    onDeSelectAll(items: any) {
        this.RRcId = [];
    }

    getAllPrograms(): void {
    };

    programeName: string;
    save() {
        this.saving = true;
        var startDate = new Date($("#ProgramStartDate").val().toString());
        var approveDate = new Date($("#ProgramApprovaltDate").val().toString());
        var endDate = new Date($("#ProgramEndDate").val().toString());
        var submitDate = new Date($("#ProgramSubmitDate").val().toString());

        if (startDate < approveDate) {
            this.notify.info(this.l("Program Start Date should be greater than Proposal Approval Date"));

            return false;
        }
        if (startDate > endDate) {
            this.notify.info(this.l("Program End Date should be greater than Program Start Date"));

            return false;
        }
        if (submitDate > approveDate) {
            this.notify.info(this.l("Proposal Approval Date should be greater than Proposal Submit Date"));

            return false;
        }
       
        this.programInfo.managerID = this.filteredListManager;
        this.programeName = this.programInfo.programName;

        //this.programInfo.programManagerId = this.programInfo.managerID[0];
        this._programService.createOrUpdateProgram(this.programInfo)
            .finally(() => { this.saving = false; })

            .subscribe((result) => {
                var ProgrammeId = this._activatedRoute.snapshot.queryParams.ProgrameId;
                if (ProgrammeId != undefined) {
                    this.notify.info(this.l("Program Updated SuccessFully"));
                }
                else {
                    this.notify.info(this.l("Program Saved SuccessFully"));
                }
                this._router.navigate(['/app/main/ProgramCluster'], {
                    queryParams: {
                        ProgrameName: this.programeName,
                        ProgrameId: result[0],
                    }
                });
            });

    }

    dateTime() {
        $(function () {
            $("#ProgramStartDate").datetimepicker({
                format: 'MM/DD/YYYY'
            });
        });

        $("#ProgramStartDate").on("dp.change", () => {
            var startDate = new Date($("#ProgramStartDate").val().toString());
            var approveDate = new Date($("#ProgramApprovaltDate").val().toString());

                $("#ProgramStartDate").val($("#ProgramStartDate").val().toString().substr(0, 10));
                var dateString = $("#ProgramStartDate").val();
                var dateObj = new Date(dateString.toString());
                this.programInfo.programStartDate = moment(dateObj).add(1, 'day');
        });

        $(function () {
            $("#ProgramEndDate").datetimepicker({
                format: 'MM/DD/YYYY'

            });
        });

        // add commas to numbers while typing without decimals
        $("#AmountSanctionRupess").keyup(function (event) {
            // skip for arrow keys
            if (event.which >= 37 && event.which <= 40) return;
            // format number
            $(this).val(function (index, value) {
                return value
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    ;
            });
        });

        $("#ProgramEndDate").on("dp.change", () => {

            var startDate = new Date($("#ProgramStartDate").val().toString());
            var endDate = new Date($("#ProgramEndDate").val().toString());

            //if (startDate > endDate) {
            //    this.notify.info(this.l("Program End Date should be greater than Program Start Date"));
                
            //    return false;
            //}
            //else {
                $("#ProgramEndDate").val($("#ProgramEndDate").val().toString().substr(0, 10));
                var dateString = $("#ProgramEndDate").val();
                var dateObj = new Date(dateString.toString());
                this.programInfo.prgramEndDate = moment(dateObj).add(1,'day');
            //}
        });



        $(function () {

            $("#ProgramSubmitDate").datetimepicker({
               // minDate: new Date()
                format: 'MM/DD/YYYY'

            });
        });

        $("#ProgramSubmitDate").on("dp.change", () => {
            $("#ProgramSubmitDate").val($("#ProgramSubmitDate").val().toString().substr(0, 10));
            var dateString = $("#ProgramSubmitDate").val();
            var dateObj = new Date(dateString.toString());
            this.programInfo.programSubmitDate = moment(dateObj).add(1, 'day');
        });



        $(".fa-calendar").on("click", function () {
            $(this).siblings("input").datetimepicker("show");
           
        });

        $(function () {
            $("#ProgramApprovaltDate").datetimepicker({
                format: 'MM/DD/YYYY'

            });
        });

        $("#ProgramApprovaltDate").on("dp.change", () => {
            var submitDate = new Date($("#ProgramSubmitDate").val().toString());
            var approveDate = new Date($("#ProgramApprovaltDate").val().toString());

            //if (submitDate > approveDate) {
            //    this.notify.info(this.l("Proposal Approval Date should be greater than Proposal Submit Date"));
               
            //    return false;
            //}
            //else {
                $("#ProgramApprovaltDate").val($("#ProgramApprovaltDate").val().toString().substr(0, 10));
                var dateString = $("#ProgramApprovaltDate").val();
                var dateObj = new Date(dateString.toString());
                this.programInfo.programApprovaltDate = moment(dateObj).add(1, 'day');
            //}
        });
    }

    funDonors() {
        this._router.navigate(['/app/main/Donor']);
    }
    funClusters() {
        this._router.navigate(['/app/main/ProgramCluster']);
    }
    funComponents() {
        this._router.navigate(['/app/main/Components']);
    }
    funCostEstimation() {
        this._router.navigate(['/app/main/ProgramCostEstimation']);
    }
    funChecked() {
        $('#checkBox').click(function () {
            $('#divComponent').hide();
            $('divComponent1').show();
        });
    }

    funProgramCostEstimation() {
        this._router.navigate(['/app/main/ProgramCostEstimation']);
    }
    funActivityMaping() {
        this._router.navigate(['/app/main/ActivityMapping']);
    }

    imageArray: any = [];
    existingModuleCollection: any = [];
    newModuleCollection: any = [];
    topicCollection: any = [];
    tempImageUrl: any = [];
    changeImage11: any = [];

    funImageSlider() {
        var tempImageArray = [
            { I: '../../../assets/common/images/Badges/badge1_blue_line_icon.svg', H: '../../../assets/common/images/Badges/badge1_white_line_icon.svg', Id: 1 },
            { I: '../../../assets/common/images/Badges/badge2_blue_line_icon.svg', H: '../../../assets/common/images/Badges/badge2_white_line_icon.svg', Id: 2 },
            { I: '../../../assets/common/images/Badges/badge3_blue_line_icon.svg', H: '../../../assets/common/images/Badges/badge3_white_line_icon.svg', Id: 3 },
            { I: '../../../assets/common/images/Badges/badge4_blue_line_icon.svg', H: '../../../assets/common/images/Badges/badge4_white_line_icon.svg', Id: 4 },
            { I: '../../../assets/common/images/Badges/badge5_blue_line_icon.svg', H: '../../../assets/common/images/Badges/badge5_white_line_icon.svg', Id: 5 }
        ];
        this.imageArray.push(tempImageArray);
        this.existingModuleCollection = new Array();
        this.newModuleCollection = new Array();
        this.topicCollection = new Array();
        this.tempImageUrl = "";
        this.changeImage11 = changeImage11;
        function changeImage11(old, newImage) {
            $("#" + old).prop("src", "" + newImage);
        }
    }
    funRedioButton() {

    }
}