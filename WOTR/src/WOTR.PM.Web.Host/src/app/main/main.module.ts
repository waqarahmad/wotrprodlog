import { NgModule } from '@angular/core';
import { CommonModule, NgStyle } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UnitOfMeasuresComponent } from './unitOfMeasures/unitOfMeasures.component';
import { CreateOrEditUnitofMeasureModalComponent } from './unitOfMeasures/create-or-edit-unitofMeasure-modal.component';
import { DataTableModule } from 'primeng/primeng';
import { FileUploadModule as PrimeNgFileUploadModule, ProgressBarModule } from 'primeng/primeng';
import { AccordionModule } from 'primeng/primeng';
import { AutoCompleteModule } from 'primeng/primeng';
import { PaginatorModule } from 'primeng/primeng';
import { EditorModule } from 'primeng/primeng';
import { InputMaskModule } from 'primeng/primeng';
import { GalleriaModule } from 'primeng/primeng';
import { ModalModule, TabsModule, TooltipModule, BsDatepickerModule, DatepickerModule } from 'ngx-bootstrap';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { MainRoutingModule } from './main-routing.module';
import { CountoModule } from 'angular2-counto';
import { EasyPieChartModule } from 'ng2modules-easypiechart';
import { ActionAreaComponent } from './actionArea/actionArea.component';
import { ModalActionAreaComponent } from './actionArea/ModalActionArea.component';
import { DonorsComponent } from './donors/donors.component';
import { ActionSubActionAreaComponent } from './actionSubActionArea/actionSubActionArea.component';
import { ActivitesComponent } from './activites/activites.component';
import { ActivitesAddModalComponent } from './activites/activitesAddModal.component';
import { componentsComponent } from './components/components.component';
import { SubActionAreaComponent } from './subActionArea/subActionArea.component';
import { SubActionAreaModalComponent } from './subActionArea/subActionAreaModal.component';
import { ComponentsAddModalComponent } from './components/componentsAddModal.component';
import { LocationComponent } from './locations/location.component';
import { CreateProgramComponent } from './createProgram/createProgram.component';
import { ProgramComponentsComponent } from './programcomponents/programcomponents.component';
import { ProgramCostEstimationComponent } from './programCostEstimation/programCostEstimation.component';
import { ActivityMappingComponent } from './activityMapping/activityMapping.component';
import { regionSpendingComponent } from './regionSpending/regionSpending.component';
import { requestAAPlaComponent } from './requestAAPla/requestAAPla.component';
import { ApproveAAPlaComponent } from './approveAAPla/approveAAPla.component';
import { ImplementationPlanComponent } from './implementationPlan/implementationPlan.component';
import { AddQuestionaryModalComponent } from './implementationPlan/addQuestionaryModal.component';
import { projectFundsComponent } from './projectFunds/projectFunds.component';
import { ProgramInformationComponent } from './programinformation/programinformation.component';
import { CalendarModule } from 'primeng/primeng';
import { AddModalActionSubActionAreaComponent } from './actionSubActionArea/ModalActionSubActionArea.component';
//import { ManagePeopleComponent } from './managePeople/managePeople.component';
//import { activitymappingtreecomponent } from './activityMapping/activityMappingTree.component';
import { AttachCheckListModal } from './approveAAPla/attachCheckListModal.component';
import { AddNewItemModalComponent } from './implementationPlan/addNewItemModal.component';
import { MapLocationModalComponent } from './implementationPlan/mapLocationModal.component';
import { PeopleComponent } from './people/people.component';
import { ProjectComponent } from './project/project.component';
import { MyLocationModalComponent } from './locations/locationModal.component';
import { CheckListComponent } from './checkList/checkList.component';
import { ModalDonorComponent } from './donors/donorsModal.component';
import { CheckListModalComponent1 } from './checkList/checkListModal.component';
import { ActivityLiveComponent } from './activityLive/activityLive.component';
import { ActivityLiveModalComponent } from './activityLive/activityLiveModal.component';
import { ProjectExpensesComponent } from './projectExpenses/projectExpenses.component';
import { ProjectExpensesModalComponent } from './projectExpenses/projectExpensesModal.component';
import { ProjectFundModalComponent } from './projectFunds/projectFundsModal.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { ProgramClusterComponent } from './program-cluster/program-cluster.component';
import { ModalProgramClusterComponent } from './program-cluster/program-cluster-modal.component';
import { QuestionaryMappingComponent } from './QuestionaryMapping/QuestionaryMapping.Component';
import { subActivityQuesMapComponent } from './QuestionaryMapping/subActivityQuesMap.Component';
import { AddNewQuestionComponent } from './QuestionaryMapping/addQuestionModal.Component';
import { FileUploadModule } from 'ng2-file-upload';
import { HttpModule, JsonpModule } from '@angular/http';
import { ProjectExpensesCommunityContributionModalComponent } from './projectExpenses/projectExpensescommunitycontributionModal.component';
import { ImplimentionplanCommunityContributionModalComponent } from './implementationPlan/implimentionplancommunitycontributionModal.component';
import { DistrictComponent } from './district/district.component';
import { ModalDistrictComponent } from './district/ModalDistrict.component';
import { TalukaComponent } from './taluka/taluka.component';
import { ModalTalukaComponent } from './taluka/ModalTaluka.component';
import { VillageComponent } from './village/village.component';
import { ModalVillageComponent } from './village/ModalVillage.component';
import { ProgramDetailsComponent } from './programDetails/programDetails.component';
import { ModalProgramDetailsComponent } from './programDetails/ModalProgramDetails.component';
import { RequestSendDetailsComponent } from './requestSendDetails/requestSendDetails.component';
import { ApproveApplaDetailsComponent } from './approveApplaDetails/approveApplaDetails.component';

import { ImplemenationPlanGroupingDetailsComponent } from './implemenationPlanGroupingDetails/implemenationPlanGrouping.component';
import { ExpensesDetailsComponent } from './expensesDetails/expensesDetails.component';
import { OverallRegionSpendingDetailsComponent } from './overallRegionSpendingDetails/overallRegionSpendingDetails.component';
import { yearWiseRegionSpendingDetailsComponent } from './yearWiseRegionSpendingDetails/yearWiseRegionSpendingDetails.component';
import { projectListComponent } from './projectList/projectList.component';
import { projectListIIIComponent } from './projectListIII/projectListIII.component';
import { DonorWiseListComponent } from './donorWiseList/donorWiseList.component';

import { RegionWiseListcomponent } from './regionWiseList/RegionWiseList.component';
import { RegionWiseListDetailscomponent } from './regionWiseListDetails/regionWiseListDetails.component';
import { RegionWiseSumarrycomponent } from './regionWiseSumarry/regionWiseSumarry.component';
import { ActionAreawisecomponent } from './actionAreawise/actionAreawise.component';
import { OverAllAnualActioncomponent } from './overAllAnualAction/overAllAnualAction.component';
import { ClusterWiseReportcomponent } from './clusterWiseReport/clusterWiseReport.component';
import { QuarterWisecomponent } from './quarterWise/quarterWise.component';
import { ProgComponent } from './prog/prog.component';
import { DemoComponentComponent } from './demo-component/demo-component.component';
import { addProjectFundModalComponent } from 'app/main/projectFunds/ProjectFundPopUp.component';
import { ShowUserRollcomponent } from 'app/shared/layout/UserRoll.component';
import { ChartModule } from 'primeng/primeng';
import { ActivitesAddModalsComponent } from './approveAAPla/approveAAPlaActivityInfo.component';
import { ProgramManagerSummaryModalsComponent } from './dashboard/programManagerDashbord/modalInfo/programManagerSummaryModal.component';
import { ProjectWiseBarChartModalsComponent } from './dashboard/programManagerDashbord/barChartModal/ProjectWiseBarChartModal.component';
import { SimpleBarChartProgramManagerModalsComponent } from './dashboard/programManagerDashbord/simpleBarChartModal/simpleBarChartProgramManagerComponentModal.component';
import { AssignReportModalsComponent } from './dashboard/programManagerDashbord/assignReportProgramManager/assignReportProgramManagerModal.component';
import { StateWiseDemandModalComponent } from './dashboard/programManagerDashbord/demandSummary/state-wise-demand-modal.component';
import { ProjectWiseDemandModalComponent } from './dashboard/programManagerDashbord/demandSummary/project-wise-demand-modal.component';
import { ActualvsexpenseModalsComponent } from './dashboard/rRCInchargeDashborad/actualvsexpense/actualvsexpenseModal.component';
import { YearWiseAchievementComponentModal } from './dashboard/rRCInchargeDashborad/yearWiseAchievementModal/yearWiseAchievementComponentModal.component';
import { AssignReporRRCtModalsComponent } from './dashboard/rRCInchargeDashborad/assignReportRRCProgramManager/assignReportRRCProgramManagerModal.component';
import { SubActionAreaRRCProgramManagerModalsComponent } from './dashboard/rRCInchargeDashborad/subActionAreaRRCProgramManager/subActionAreaRRCProgramManagerModal.component';
import { ProgramWiseDemandModalComponent } from './dashboard/programManagerDashbord/demandSummary/program-wise-demand-modal.component';

import { CropComponent } from './Crops/crop.component';
import { CropModalComponent } from './Crops/crop.modal.component';
import { CategoryComponent } from './categories/IICategory.component';
import { CategoryModalComponent } from './categories/IICategory.modal.component';
import { SubCategoryComponent } from './subCategories/IIsubcategory.component';
import { SubCategoryModalComponent } from './subCategories/IISubCategoryModal.component';
import { SourceComponent } from './sources/Source.component';
import { SourceModalComponent } from './sources/SourceModal.component';
import { QuestionaryComponent } from './ImpactIndicatorQuestionaries/Questionary.component';
import { QuestionaryModalComponent } from './ImpactIndicatorQuestionaries/QuestionaryModal.component';
import { ImpactIndicatorFormModal } from './ImpactIndicatorFormMaster/IICreateEditFormModal.component';
import { ImpactIndicatorFormComponent } from './ImpactIndicatorFormMaster/ImpactIndicatorCreateEditForm.component';
import { DualListBoxModule } from 'ng2-dual-list-box';
import { IIFormAndProgramMapComponent } from './IIFormAndProgramMappings/IIFormAndProgramMapping.component';
import { IIQuestionListModal } from './IIFormAndProgramMappings/QuestionList.Modal.component';
import { ImpactIndicatorFormAndProgramModal } from './IIFormAndProgramMappings/IIFormAndProgramMapModal.component';
import { IIResponseComponent } from './ImpactIndicatorResponseForm/IIResponseForm.component';
import { IIResponseModalComponent } from './ImpactIndicatorResponseForm/IIResponseForm.modal.component';

import { FreqOfOccurrenceComponent } from './FrequencyOfOccurences/FreqOfOccurence.component';
import { FreqOfOccurrenceModalComponent } from './FrequencyOfOccurences/FreqOfOccurence.modal.component';
import { ImpactIndicatorReportComponent } from './ImpactIndicatorReports/ImpactIndicatorReport.component';

import { StateWiseDemandAdminModalComponent } from './dashboard/AdminDashboard/StateWise_Demand.modal.component'
import { ProjectWiseDemandAdminModalComponent } from './dashboard/AdminDashboard/ProjectWise_Demand.Modal.component'

//import { ImageZoomModule } from 'angular2-image-zoom';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
    imports: [
        DataTableModule,
        FileUploadModule,
        AutoCompleteModule,
        PaginatorModule,
        EditorModule,
        InputMaskModule,
        CommonModule,
        AccordionModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        MainRoutingModule,
        CountoModule,
        EasyPieChartModule,
        CalendarModule,
        GalleriaModule,
        AngularMultiSelectModule,
        PrimeNgFileUploadModule,
        HttpModule,
        ChartModule,
        NgxImageZoomModule.forRoot(),
        DualListBoxModule.forRoot(),
        BsDatepickerModule.forRoot(),
        DatepickerModule.forRoot(),
        AmChartsModule,
        NgSelectModule
    ],
    declarations: [
        DashboardComponent,
        ActionAreaComponent,
        ModalActionAreaComponent,
        DonorsComponent,
        ActionSubActionAreaComponent,
        ActivitesComponent,
        ActivitesAddModalComponent,
        componentsComponent,
        SubActionAreaComponent,
        SubActionAreaModalComponent,
        ComponentsAddModalComponent,
        LocationComponent,
        CreateProgramComponent,
        ProgramComponentsComponent,
        ProgramCostEstimationComponent,
        ActivityMappingComponent,
        regionSpendingComponent,
        requestAAPlaComponent,
        ApproveAAPlaComponent,
        ImplementationPlanComponent,
        AddQuestionaryModalComponent,
        projectFundsComponent,
        ProjectFundModalComponent,
        ProgramInformationComponent,
        AddModalActionSubActionAreaComponent,
        //ManagePeopleComponent,
        //activitymappingtreecomponent,
        AttachCheckListModal,
        UnitOfMeasuresComponent,
        CreateOrEditUnitofMeasureModalComponent,
        AddNewItemModalComponent,
        MapLocationModalComponent,
        PeopleComponent,
        ProjectComponent,
        MyLocationModalComponent,
        CheckListComponent,
        ModalDonorComponent,
        CheckListModalComponent1,
        ActivityLiveComponent,
        ActivityLiveModalComponent,
        ProjectExpensesComponent,
        ProjectExpensesModalComponent,
        ProjectExpensesCommunityContributionModalComponent,
        ImplimentionplanCommunityContributionModalComponent,
        ProgramClusterComponent,
        ModalProgramClusterComponent,
        QuestionaryMappingComponent,
        subActivityQuesMapComponent,
        AddNewQuestionComponent,
        DistrictComponent,
        ModalDistrictComponent,
        TalukaComponent,
        ModalTalukaComponent,
        VillageComponent,
        ModalVillageComponent,
        ProgramDetailsComponent,
        ModalProgramDetailsComponent,
        RequestSendDetailsComponent,
        ApproveApplaDetailsComponent,
        ImplemenationPlanGroupingDetailsComponent,
        ExpensesDetailsComponent,
        OverallRegionSpendingDetailsComponent,
        yearWiseRegionSpendingDetailsComponent,
        projectListComponent,
        projectListIIIComponent,
        DonorWiseListComponent,
        RegionWiseListcomponent,
        RegionWiseListDetailscomponent,
        RegionWiseSumarrycomponent,
        ActionAreawisecomponent,
        OverAllAnualActioncomponent,
        ClusterWiseReportcomponent,
        QuarterWisecomponent,
        ProgComponent,
        DemoComponentComponent,
        addProjectFundModalComponent,
        ShowUserRollcomponent,
        ActivitesAddModalsComponent,
        ProgramManagerSummaryModalsComponent,
        ProjectWiseBarChartModalsComponent,
        SimpleBarChartProgramManagerModalsComponent,
        AssignReportModalsComponent,
        StateWiseDemandModalComponent,
        ProjectWiseDemandModalComponent,
        ActualvsexpenseModalsComponent,
        YearWiseAchievementComponentModal,
        AssignReporRRCtModalsComponent,
        SubActionAreaRRCProgramManagerModalsComponent,
        ProgramWiseDemandModalComponent,

        CropComponent,
        CropModalComponent,
        CategoryComponent,
        CategoryModalComponent,
        SubCategoryComponent,
        SubCategoryModalComponent,
        SourceComponent,
        SourceModalComponent,
        QuestionaryComponent,
        QuestionaryModalComponent,
        ImpactIndicatorFormModal,
        ImpactIndicatorFormComponent,
        IIFormAndProgramMapComponent,
        ImpactIndicatorFormAndProgramModal,
        IIResponseComponent,
        IIResponseModalComponent,
        FreqOfOccurrenceComponent,
        FreqOfOccurrenceModalComponent,
        ImpactIndicatorReportComponent,
        IIQuestionListModal,

        StateWiseDemandAdminModalComponent,
        ProjectWiseDemandAdminModalComponent
    ]
})
export class MainModule { }
