﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { ListActionSubactionListDto, Village, RequestAAPlasListDto, PrgRequestAAPlasServiceProxy, ProgramServiceProxy, ProgramInformationListDto, PrgVillageClusterListDto, Precostestimationoverallregionsummuery } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { FileDownloadService } from '@shared/utils/file-download.service';
declare let jsPDF;

@Component({
    selector: 'regionWiseSumarrycomponent',
    templateUrl: "./regionWiseSumarry.component.html",
    styleUrls: ['./regionWiseSumarry.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class RegionWiseSumarrycomponent extends AppComponentBase {
    @ViewChild('test') el: ElementRef;

    programd: ProgramInformationListDto = new ProgramInformationListDto();
    program: ProgramInformationListDto[];

    requestList: RequestAAPlasListDto[] = [];
    villageclustor: PrgVillageClusterListDto[];
    pro: any[];
    active: boolean = false;
    list: any = [];
    requestSingleYearList: RequestAAPlasListDto = new RequestAAPlasListDto();
    AddSubActionList: ListActionSubactionListDto;
    emailContent: any;
    saving: boolean = false;
    prgVilages: Village[] = [];
    Record: Precostestimationoverallregionsummuery[] = [];
    Year: any = [];
    costyear: string;
    constructor(
        injector: Injector,
        private _programService: ProgramServiceProxy,
        private _fileDownloadService: FileDownloadService,
        private _PrgRequestAAPlasService: PrgRequestAAPlasServiceProxy) {
        super(injector);
    }

    ngOnInit() {
        this.getcostestimationyear();
    }

    count: number = -1;
    finalUnits: number = 0;
    sumOfTotal: number = 0;
    sumOfTotal1: number = 0;

    DivideUnits(res, rec) {
    }

    getcostestimationyear() {
        this._PrgRequestAAPlasService.overallPrgCostEstimationYears().subscribe(result => {
            this.Year = result;

        });
    }

    GetAllRegionWiseWiseList(year) {
        this.costyear = year;
        this._programService.getAllRegionSummary(this.costyear).subscribe(result => {
            this.Record = result;
        });
    }


    exportToExcel(): void {
        this._programService.createExcelRegionWiseSummary("Region Wise Summarry", this.costyear).subscribe(result => {
            this._fileDownloadService.downloadTempFile1(result);
        });
    }
}
