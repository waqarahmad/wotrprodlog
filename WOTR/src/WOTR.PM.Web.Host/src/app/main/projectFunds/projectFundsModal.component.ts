﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { ProgramFundsListDto, ProgramFundsServiceProxy, PrgActionAreaActivityMappingServiceProxy, ListActionSubactionListImplemenTationDto1, ListViewDeatilsDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
import * as moment from "moment";
declare var jQuery: any;
import * as _ from 'lodash';


@Component({
    selector: 'ProjectFundModal',
    templateUrl: "./projectFundsModal.component.html",

})
export class ProjectFundModalComponent extends AppComponentBase {
    programFunds: ProgramFundsListDto;
    ProjectFundList: ProgramFundsListDto[];
    @ViewChild('ProjectFundModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('projectForm') eForm: NgForm;
    active: boolean = false;
    programFund: ProgramFundsListDto;
    relasefund: ProgramFundsListDto[];
    demandamount: ProgramFundsListDto[];

    subTotalUnitCost: number = 0;
    subTotalTotalunits: number = 0;
    subTotalDemandUnits: number = 0;
    subTotalDemandAmmount: number = 0;

    subTotalQuarter1PhysicalUnits: number = 0;
    subTotalQuarter1FinancialRs: number = 0;
    subTotalQuarter2PhysicalUnits: number = 0;
    subTotalQuarter2FinancialRs: number = 0;
    startDateFilter: any = new Date(new Date().setFullYear(new Date().getFullYear() - 1));
    endDateFilter: any = new Date();
    startDate: any = new Date();
    endDate: any = new Date();

    finalResult: ListActionSubactionListImplemenTationDto1[] = [];
    singleObj: ListViewDeatilsDto = new ListViewDeatilsDto();

    bindData: ListActionSubactionListImplemenTationDto1[] = [];
    ProgramId: number;
    locationId: number;
    year: string;
    constructor(
        injector: Injector,
        private _ProgramFundsService: ProgramFundsServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {

    }

    show(res, year): void {
        this.active = true;
        this.ProgramId = res.programID;
        this.locationId = res.locationID;
        this.year = year;
        var k = this;
        this.startDateFilter = new Date("04/01/" + year.substring(0, 4));
        this.endDateFilter = new Date("03/31/" + year.substring(6, 9));
        this.startDate = moment(this.startDateFilter).format('L');
        this.endDate = moment(this.endDateFilter).format('L');
        $(() => {
            var l = k;
            $("#startDateFilters").datetimepicker({
                date: new Date(this.startDateFilter),
                format: 'MM/DD/YYYY'
            }).on('dp.change', (e) => {
                this.startDateFilter = e.date;
                this.year = e.date.year().toString() + "-" + moment(this.endDateFilter).year().toString();
                this.getAllActionAreaRecord(0);
            });
        });
        $(() => {
            var b = k;
            $("#endDateFilters").datetimepicker({
                date: new Date(this.endDateFilter),
                format: 'MM/DD/YYYY'
            }).on('dp.change', (e) => {
                this.endDateFilter = e.date;
                this.year =  moment(this.startDateFilter).year().toString() + "-" + e.date.year().toString() ;
                this.getAllActionAreaRecord(0);
            });
        });

        this.getAllActionAreaRecord(this.ProgramId);

        if (res != undefined) {
            this.programFund = res;
        }
        this._ProgramFundsService.getProjectFundDetailForResult(this.year, this.ProgramId).subscribe(x => {
            this.relasefund = x.filter(t => t.releasedAmount != null);
            this.demandamount = x.filter(t => t.releasedAmount == null);
        });
        this.modal.show();
    }
   

    close(): void {
        this.startDate = moment(this.startDateFilter).format('L');
        this.endDate = moment(this.endDateFilter).format('L');
        this.modal.hide();
        this.active = false;
    }

    getAllActionAreaRecord(ProgramID) {
        abp.ui.setBusy();
        if (this.startDateFilter && this.endDateFilter) {
            var start = moment(this.startDateFilter);
            var end = moment(this.endDateFilter);
        }
        this.finalResult = [];
        this._ProgramFundsService.getProjectViewDetails(this.ProgramId, this.year, this.locationId, start, end).subscribe(result => {
            this.finalResult = result;
            this.modalSave.emit(null);
            abp.ui.clearBusy();
        });
    }

    CalUnitCost(req) {
        req.demandAmmount = req.demandUnits * req.unitCost;
    }

    //save(form: NgForm) {
    //    form.reset();
    //}
}