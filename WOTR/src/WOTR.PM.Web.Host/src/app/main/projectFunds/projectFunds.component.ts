﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, ViewEncapsulation, Output } from '@angular/core';
import { ProgramFundsServiceProxy, ProgramFundsListDto, PrgActionAreaActivityMappingServiceProxy, UserRoleDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';
import { addProjectFundModalComponent } from 'app/main/projectFunds/ProjectFundPopUp.component';


@Component({
    selector: 'projectFunds',
    templateUrl: "./projectFunds.component.html",
    styleUrls: ['./projectFunds.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class projectFundsComponent extends AppComponentBase {
    @ViewChild('addProjectFundModal') addProjectFundModal: addProjectFundModalComponent;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    ProjectFundList: ProgramFundsListDto[];
    saving: boolean = false;
    userRole: UserRoleDto = new UserRoleDto();
    selectedYear: string = "";

    constructor(
        injector: Injector,
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _ProgramFundsService: ProgramFundsServiceProxy, private _PrgActionAreaActivityMappingService: PrgActionAreaActivityMappingServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.getUserRole();
        this.getAllImplementaionActionAreaRecord();
    }

    getUserRole() {
        this._PrgActionAreaActivityMappingService.getUserRoleNameForEdit(this.appSession.user.userRole).subscribe(res => {
            this.userRole = res;
        });
    }

    ActiveClass(i) {
        $("#tab_" + i + "_0").addClass("active");
        $("#tab_" + i + "_0").siblings().removeClass("active");
    }

    ProgramManagerIsNull: boolean = false;

    GenerateDemand(res, rec, i, coe) {
        this.ProgramManagerIsNull = true;
        this.saving = true;
        for (let r of res) {
            r.programManagerID = rec.programManagerID;
        }
        if (rec.programManagerID == 0 || rec.programManagerID == null) {
            this.ProgramManagerIsNull = false;
            this.notify.warn("please select ProgramFinanceManager");
        }
        if (this.ProgramManagerIsNull) {
            this._ProgramFundsService.createProjectFund(res).finally(() => this.saving = false).subscribe(res => {
                this.notify.info("save SuccessFully");
                this.getAllImplementaionActionAreaRecord();
                $("#tab_" + i + "_" + coe).addClass("active");
            });
        }
    }

    AddManagerId(res, event) {
        var tre = event.target.value;
        if (tre.includes(",")) {
            var values = tre.split(",");
            res.programManagerID = values[0];
            res.programManagerName = values[1];
        }
    }

    getAllImplementaionActionAreaRecord() {
        abp.ui.setBusy();
        if (this.isGranted('Pages.FundRelease')) {

            var programID = this._activatedRoute.snapshot.queryParams.ProgrameId;
            this._ProgramFundsService.getProjectFundsForAdminNew(programID).subscribe(result => {
                this.ProjectFundList = result;
                this.modalSave.emit(null);
                abp.ui.clearBusy();
            });
        }
        else {
            var ProgrameId = this._activatedRoute.snapshot.queryParams.ProgrameId;
            this._ProgramFundsService.getProjectFunds(ProgrameId).subscribe(result => {
                this.ProjectFundList = result;
                for (let r of this.ProjectFundList) {
                    for (let c of r.costEstimationYear) {
                        for (let s of c.actionSubactionCost) {
                            if (s.quarter1PhysicalUnits == 0 &&
                                s.quarter2PhysicalUnits == 0 &&
                                s.quarter3PhysicalUnits == 0 &&
                                s.quarter4PhysicalUnits == 0) {
                                for (let p of c.actionSubactionCost) {
                                    if (s.prgActionAreaActivityMappingID == p.prgActionAreaActivityMappingID &&
                                        (p.quarter1PhysicalUnits != 0 ||
                                            p.quarter2PhysicalUnits != 0 ||
                                            p.quarter3PhysicalUnits != 0 ||
                                            p.quarter4PhysicalUnits != 0)) {
                                        p.actionAreaName = undefined;
                                        p.subActionAreaName = undefined;
                                        p.activityName = undefined;

                                    }
                                }
                            }
                            c.subTotalTotalunits += s.totalUnits;
                            c.subTotalUnitCost += s.unitCost;
                            c.subTotalofTotalCost += s.totalUnitCost;
                            c.subTotalofCommunityContribution += s.communityContribution;
                            c.subTotalofFunderContribution += s.funderContribution;
                            c.subTotalofOtherContribution += s.otherContribution;
                        }
                    }

                }
                abp.ui.clearBusy();
            });
        }
    }

    CalUnitCost(req) {
        req.demandAmmount = req.demandUnits * req.unitCost;
    }

    callthis() {
        abp.ui.setBusy();
        this.getAllImplementaionActionAreaRecord();
        abp.ui.clearBusy();
    }

    approvedClick(FundId, Status) {
        var answer = confirm('Are you sure you want to approve Fund');
        if (answer) {
            console.log('yes');
            $('#btn1').css('background-color', 'forestgreen').css('color', 'white');
            $('#btn2').css('background-color', 'ghostwhite').css('color', 'gray');
            $('#btn3').css('background-color', 'ghostwhite').css('color', 'gray');

            this._ProgramFundsService.updateProjectFundStatus(FundId, Status).subscribe(result => {
                this.notify.info("Save SuccessFully");
                this.getAllImplementaionActionAreaRecord();
            });
        }
        else {
            console.log('cancel');
        }
    }

    clickReject(FundId, Status) {
        var answer = confirm('Are you sure you want to reject Fund');
        if (answer) {
            console.log('yes');
            $('#btn1').css('background-color', 'forestgreen').css('color', 'white');
            $('#btn2').css('background-color', 'ghostwhite').css('color', 'gray');
            $('#btn3').css('background-color', 'ghostwhite').css('color', 'gray');

            this._ProgramFundsService.updateProjectFundStatus(FundId, Status).subscribe(result => {
                this.notify.info("Save SuccessFully");
                this.getAllImplementaionActionAreaRecord();
            });
        }
        else {
            console.log('cancel');
        }
    }
    funImplementationPlan() {
        if (this.isGranted('Pages.RequestAAPlaSend')) {
            this._router.navigate(['/app/main/ImplementationPlan'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
        else {
            this._router.navigate(['/app/main/ImplementationPlan'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
    }

    funNextPage() {
        if (this.isGranted('Pages.RequestAAPlaSend')) {
            this._router.navigate(['/app/main/ProjectExpenses'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
        else {
            this._router.navigate(['/app/main/ProjectExpenses'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId
                }
            });
        }
    }

    funApproveAAPla() {
        if (this.isGranted('Pages.RequestAAPlaSend')) {
            this._router.navigate(['/app/main/ApproveAAPla'], {
                queryParams: {
                    ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                    ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
                }
            });
        }
        else {
            this._router.navigate(['/app/main/RequestAAPla'], {
                queryParams: {
                    ProgrameName: "",
                    ProgrameId: 1,
                }
            });
        }
    }
}