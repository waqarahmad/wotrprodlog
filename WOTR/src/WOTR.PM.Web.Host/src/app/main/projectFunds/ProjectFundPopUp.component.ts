﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import * as moment from "moment";
import index from 'angular2-counto';
import { ProgramFundsListDto, PrgFundDetails, ProgramFundsServiceProxy, ProgramFundDto } from '@shared/service-proxies/service-proxies';
import { window } from 'rxjs/operators/window';
import { Window } from 'selenium-webdriver';
import { Location } from '@angular/common';
declare var google: any;
declare var jQuery: any;

@Component({
    selector: 'addProjectFundModal',
    templateUrl: "./ProjectFundPopUp.component.html",
    styleUrls: ['./ProjectFundPopUp.component.less'],
})
 
export class addProjectFundModalComponent extends AppComponentBase {

    @ViewChild('modal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
   // public filterDateFrom: Date;
   
    active: boolean = false;
    saving: boolean = false;
    filterText = '';
    role = '';
    selectedPermission = '';
    onlyLockedUsers = false;
    datetextvalue=new Date();
    currentdate: any;
    flag=0;
    programFundsListDto: ProgramFundsListDto = new ProgramFundsListDto();
    prgFundDetails: ProgramFundDto = new ProgramFundDto();

    constructor(
        injector: Injector,
        private _route: Router,
        private _ProgramFundsService: ProgramFundsServiceProxy
     
        // private _personService: PersonServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.currentdate = this.datetextvalue;
        this.prgFundDetails.releasedAmount <= this.prgFundDetails.demandAmount;
    }
     
    show(id): void {        
        this.programFundsListDto = id;
        this.prgFundDetails.demandAmount = id.demandAmount;
        this.prgFundDetails.releasedAmount = 0;
        this.prgFundDetails.id = id.id;
        this.prgFundDetails.programManagerName = id.programManagerName;
        this.prgFundDetails.programName = id.programName;
        this.prgFundDetails.projectManagerName = id.projectManagerName;
        this.prgFundDetails.status = id.status1;
        this.prgFundDetails.projectManagerID = id.projectManagerID;            
        this.active = true;
        this.modal.show();        
    }
    onShown(): void {
        
    }

    close(): void {
           
        this.modal.hide();
        this.active = false;
    }

    check() {
        if (this.prgFundDetails.releasedAmount < 0) {
            this.prgFundDetails.releasedAmount = 0
        }
    }

    save() {
        if (this.prgFundDetails.releasedAmount < 0) {
            this.flag = 1;
            this.prgFundDetails.releasedAmount = 0
 
        }
        if (this.prgFundDetails.releasedAmount > 0 && this.prgFundDetails.releasedAmount <= this.prgFundDetails.demandAmount) {
            abp.ui.setBusy();
            this.flag = 0;
            this._ProgramFundsService.saveProjectFund(this.prgFundDetails).subscribe(res => {
                abp.ui.clearBusy();
                this.notify.success("Fund Released Successfully");
                this.close();
                this.modalSave.emit(null);
            });
        }
        else {
            if (this.flag == 0) {
                this.notify.error("Released Amount should be smaller than Demanad Amount");
            }
            else {
                this.notify.error("Enter Amount is not valid");
            }
        }
       
    }
   
}