﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { GetAllQuestionary, QuestionaryServiceProxy,  } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';
import { Paginator, LazyLoadEvent } from 'primeng/primeng';
import { QuestionaryModalComponent } from './QuestionaryModal.component';

@Component({
    selector: 'Questionary',
    templateUrl: "./Questionary.component.html",
    styleUrls: ['./Questionary.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class QuestionaryComponent extends AppComponentBase {
    @ViewChild('QuestionaryModal') myModalDoner: QuestionaryModalComponent;
    saving: boolean = false;
    currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    filterText = "";
    Questionary: GetAllQuestionary[] = [];
    QuestionaryList: GetAllQuestionary[] = [];
    searchText = "";
    timer: any;

    constructor(
        injector: Injector,
        private _QuestionaryServiceProxy: QuestionaryServiceProxy,
        private _activatedRoute: ActivatedRoute


    ) {
        super(injector);
    }
    

    ngOnInit() {
        //this.getallQuestionary();
    }

    getallQuestionary() {
        if (this.searchText == null) {
            this.searchText = " ";
        }
        this._QuestionaryServiceProxy.getAllQuestionary(this.searchText).subscribe((result) => {
            this.filterText,
                this.QuestionaryList = result;
            this.primengDatatableHelper.records = result;
            this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.Questionary = result;
            this.QuestionaryList = result;
            this.QuestionaryList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
        })
    }

   
    editQuestionary(editRecord) {
        this.myModalDoner.show(editRecord)
    }

    searchPC(): void {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => { this.getallQuestionary() }, 1000);
    }

    deleteQuestionary(IIQue) {
        this.message.confirm(
            this.l('Questionnaire', IIQue.question),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._QuestionaryServiceProxy.delete(IIQue.id)
                        .subscribe((result) => {
                            this.notify.success(this.l('Successfully Deleted  Record'));
                            this.getallQuestionary();
                        });
                };

            });
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.Questionary.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.QuestionaryList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);
    }

}