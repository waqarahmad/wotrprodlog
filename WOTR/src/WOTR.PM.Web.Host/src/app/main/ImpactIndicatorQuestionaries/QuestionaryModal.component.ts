﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import {
    QuestionaryServiceProxy, CategoryServiceProxy, SubCategoryServiceProxy, GetAllCategory, GetAllSubCategory, AddEditQuestionaryDto, FrequencyOfOccurencesServiceProxy, GetAllFreqOfOccurrence
} from '@shared/service-proxies/service-proxies';
//import {  QuestionaryType  } from '@shared/AppEnums';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';

import { MysortPipe } from '../../shared/common/mysort.pipe';//yo //modified for Sorting Drop Downs
import * as moment from 'moment';
declare var jQuery: any;
@Component({
    selector: 'QuestionaryModal',
    templateUrl: "./QuestionaryModal.component.html",
    styleUrls: ['./QuestionaryModal.component.less'],
})
export class QuestionaryModalComponent extends AppComponentBase {
    @ViewChild('QuestionaryModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    saving: boolean = false;
    addEditQuestionary: AddEditQuestionaryDto = new AddEditQuestionaryDto();
    Category: GetAllCategory = new GetAllCategory();
    CategoryList: GetAllCategory[] = [];
    SubCategory: GetAllSubCategory = new GetAllSubCategory();
    SubCategoryList: GetAllSubCategory[] = [];
    FreqOfOccurrenceList : GetAllFreqOfOccurrence[] = [];

    constructor(
        injector: Injector,
        private _QuestionaryServiceProxy: QuestionaryServiceProxy,
        private _SubCategoryServiceProxy: SubCategoryServiceProxy,
        private _CategoryServiceProxy: CategoryServiceProxy,
        private _FrequencyOfOccurencesServiceProxy : FrequencyOfOccurencesServiceProxy
    ) {
        super(injector);

    }
    ngOnInit() {

        this.addEditQuestionary.questionStatus = "Active";
        this.addEditQuestionary.questionType = "Custom"
    }

    show(e): void {
        this.active = true;
        if (e != undefined) {
            this.bindFreqOfOccurence();
            this.bindCategory();
            this.addEditQuestionary = e;
            $('#lblTitle').text('Edit Questionnaire')
        }
        else {
            this.bindFreqOfOccurence();
            this.bindCategory();
            $('#lblTitle').text('Add Questionnaire')
        }
        this.active = true;
        this.modal.show();
    }

    bindFreqOfOccurence() {
        this._FrequencyOfOccurencesServiceProxy.getAllFreqOfOccurrence('').subscribe(result => {
            this.FreqOfOccurrenceList = result;
            this.FreqOfOccurrenceList = this.FreqOfOccurrenceList.filter(t => t.freqOfOccurrence != 'Post-Intervention');
            this.FreqOfOccurrenceList = this.FreqOfOccurrenceList.filter(t => t.freqOfOccurrence != 'Pre-Intervention');

            let Obj = new GetAllFreqOfOccurrence();
            Obj.id = 4;
            Obj.freqOfOccurrence = 'Pre-Post Intervention';
            this.FreqOfOccurrenceList.push(Obj)
        });
    }

    save() {
        this.saving = true;
        this._QuestionaryServiceProxy.createOrEdit(this.addEditQuestionary)
            .finally(() => this.saving = false)
            .subscribe(result => {
                if (result == "Questionnaire Added Sucessfully" || result == "Questionnaire Updated Sucessfully") {
                    this.notify.info(result);
                }
                if (result == "Record is already Present") {
                    this.notify.warn(result);
                }
                this.close();
                this.modalSave.emit(null);
            })
    }

    bindCategory() {
        this._CategoryServiceProxy.getAllImapactIndiactorCategory('').subscribe(result => {
            this.CategoryList = result;
        })
    }

    clearFields() {
        this.addEditQuestionary.impactIndicatorCategoryId = undefined;
        this.addEditQuestionary.impactIndicatorSubCategoryId = undefined;
        this.addEditQuestionary.frequencyOfOccurrenceId = undefined;
        this.addEditQuestionary.question = "";
        this.addEditQuestionary.description = "";
        this.addEditQuestionary.questionStatus = "Active";
        this.addEditQuestionary.questionType = "Custom"
    }

    bindSubCategory() {
        if (this.addEditQuestionary.impactIndicatorCategoryId != undefined) {
        this._SubCategoryServiceProxy.getAllImapactIndiactorSubCategoryByCategoryId(this.addEditQuestionary.impactIndicatorCategoryId).subscribe(result => {
            this.SubCategoryList = result;
        })
        }
    }

    close(): void {
        this.clearFields();
        this.modal.hide();
        this.active = false;
    }
}