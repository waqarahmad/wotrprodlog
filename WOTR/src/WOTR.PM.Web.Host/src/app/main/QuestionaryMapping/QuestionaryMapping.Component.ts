﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ChekListsServiceProxy, SubActivityList, SubActivityQuesMapDto, CheklistandSublistDto } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { DataTable } from "primeng/components/datatable/datatable";
import { ModalDirective } from 'ngx-bootstrap';
declare var jQuery: any;

@Component({
    selector: 'questionarycomponent',
    templateUrl: "./QuestionaryMapping.Component.html",
    styleUrls: ['./QuestionaryMapping.Component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class QuestionaryMappingComponent extends AppComponentBase {
    SubActQuesList: CheklistandSublistDto[];
    constructor(injector: Injector, private _checkListService: ChekListsServiceProxy, private _router: Router) {
        super(injector);

    }
    ngOnInit(): void {
        this.getAllSubActivityWithQuestions();
    }
    getAllSubActivityWithQuestions() {
        abp.ui.setBusy();
        this._checkListService.getAllSubActivityWithQuestions().subscribe(result => {
            this.SubActQuesList = result;
            abp.ui.clearBusy();
        })
    }

 deletequestion(del) {
        this.message.confirm(
            this.l('SubActivityDelete', del.cheklisiteamtName),
            (isConfirmed) => {
                if (isConfirmed) {
        this._checkListService.deletequestion(del.id)
            .subscribe(() => {
                this.notify.success(this.l('Successfully Deleted'));
                this.getAllSubActivityWithQuestions();
            });
                  
              }
           })
        
        
    }
    deletequestion1(del) {
        this.message.confirm(
            this.l('SubActivityQuestionDelete', del.name),
            (isConfirmed) => {
                if (isConfirmed) {
        this._checkListService.deletequestion1(del.questionmappingid)
            .subscribe(() => {
                this.notify.success(this.l('Successfully Deleted'));
                this.getAllSubActivityWithQuestions();
            });

                }
           })
        
    }
}