﻿import { Component, Injector, ViewChild, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ChekListsServiceProxy, SubActivityList, SubActivityQuesMapDto } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
declare var jQuery: any;

@Component({
    selector: 'subActivityQue',
    templateUrl: "./subActivityQuesMap.Component.html",
})

export class subActivityQuesMapComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('ModalQuestion') modal: ModalDirective;
    active: boolean = false;
    subActivityList: SubActivityList[];
    itemList: any;
    settings: any;
    saving: boolean = false;
    selectedItems: any;
    selected: any;
    SubActQue: SubActivityQuesMapDto = new SubActivityQuesMapDto();
    listt: SubActivityQuesMapDto = new SubActivityQuesMapDto();
    abc: any = [];
    aaa: any = [];
    aac: any = [];
    QuestinaryId: number[] = [];

    constructor(
        injector: Injector, private _checkListService: ChekListsServiceProxy
    ) {
        super(injector);

    }
    ngOnInit(): void {
        this.getAllSubActivity();
        this.getAllQuestionary();
        this.settings = {
            singleSelection: false,
            text: "Select Questions",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        };
    }

    show(p): void {
        this.active = true;
        if (p != undefined) {
            this.SubActQue = p;
            this.SubActQue.subActivityId = p.id,
                this.listt.subActivityId = p.id,
                this.selectedItems = [];
            this.selected = [];
            for (let v of p.ques) {
                var x = { "id": v.id, "itemName": v.name }
                var y = { "id": v.id, }
                this.selectedItems.push(x);
            }
        }
        else {
            this.selectedItems = [];
            this.QuestinaryId = [];
            this.SubActQue = new SubActivityQuesMapDto();
            this.getAllQuestionary();
        }
        this.modal.show();
    }

    getAllSubActivity() {
        this._checkListService.getAllSubActivity().subscribe(result => {
            this.subActivityList = result;
        });
    }

    getAllQuestionary() {
        this.itemList = [];
        this._checkListService.getAllQuestionarie().subscribe(result => {
            for (let a of result) {
                this.itemList.push({
                    id: a.id,
                    itemName: a.name
                })
            }
        });
    }

    save() {
        this.saving = true;

        this.SubActQue.questionaryIds = this.QuestinaryId;
        this.listt.questionaryIds = this.QuestinaryId;
        this.SubActQue.subActivityId = this.SubActQue.subActivityId;
        this.abc = this.SubActQue;
        this.aaa = this.listt;
        if (this.abc.cheklisiteamtName != undefined) {
            this._checkListService.addSubactQuesMap(this.listt)
                .finally(() => this.saving = false)
                .subscribe(result => {
                    this.notify.info("Save Successfully");
                    this.close();
                    this.modalSave.emit(null);
                })
        }
        else {
            this._checkListService.addSubactQuesMap(this.SubActQue)
                .finally(() => this.saving = false)
                .subscribe(result => {
                    this.notify.info("Save Successfully");
                    this.close();
                    this.modalSave.emit(null);
                })
        }
    }


    onItemSelect(item: any) {
        this.QuestinaryId.push(item.id);
    }

    OnItemDeSelect(item: any) {
        var index = this.QuestinaryId.indexOf(item.id, 0);
        if (index > -1) {
            this.QuestinaryId.splice(index, 1);
        }
    }

    onSelectAll(items: any) {
        for (let obj of items) {
            this.QuestinaryId.push(obj.id);
        }
    }

    onDeSelectAll(items: any) {
        this.QuestinaryId = [];
    }

    close(): void {
        this.modal.hide();
        this.active = false;
    }
}