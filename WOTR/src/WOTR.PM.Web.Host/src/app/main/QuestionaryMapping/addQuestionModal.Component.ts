﻿import { Component, Injector, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ChekListsServiceProxy, SubActivityList, SubActivityQuesMapDto, QuestListDto } from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';
import { NgForm } from '@angular/forms';
import { MysortPipe } from '../../shared/common/mysort.pipe';
declare var jQuery: any;


@Component({
    selector: 'addNewQues',
    templateUrl: "./addQuestionModal.Component.html",
})
export class AddNewQuestionComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('AddQuestion') modal: ModalDirective;
    active: boolean = false;
    QuestionList: QuestListDto = new QuestListDto();
    saving: boolean = false;
    constructor(
        injector: Injector, private _checkListService: ChekListsServiceProxy, private _router: Router
    ) {
        super(injector);

    }
   

    show(): void {
        this.active = true;
        this.QuestionList = new QuestListDto();
        this.modal.show();
    } 

    save() {
        this.saving = true;
        this._checkListService.createOrUpdateQuestionary(this.QuestionList)
            .finally(() => this.saving = false)
            .subscribe(result => {
            this.notify.info("Save SuccessFully");
            this.close();

        })
    }
  
    close(): void {
        this._router.navigate(['/app/main/QuestionaryMapping']);
        this.modal.hide();
        this.active = false;
    }
}