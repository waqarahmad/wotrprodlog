﻿import { Component, Injector, ViewChild, EventEmitter, Output } from '@angular/core';
import {
    GetQuestionType, SourceServiceProxy, ProgramInformationListDto, ProgramServiceProxy, Village, ImpactIndicatorFormServiceProxy, 
    ImpactIndicatorResponseServiceProxy, UserListDto, GetAllSource, AddFormResponseDto, GetAllQuestionsFromForm, CropServiceProxy, GetAllCrops, EmailServiceProxy, GetUserDetailsForMailDto
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import * as moment from "moment";

export interface TableData {
    id: number;
    Date: Date;
    HHS: string;
    Gender: string;
    Area: number;
    yield: number;
    Data: string;
}

@Component({
    selector: 'IIResponseModal',
    templateUrl: "./IIResponseForm.modal.component.html",
    styleUrls: ['./IIResponseForm.modal.component.less']
})


export class IIResponseModalComponent extends AppComponentBase {
    @ViewChild('IIResponseModal') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    addFormResponseDto: AddFormResponseDto = new AddFormResponseDto();

    ProgramList: ProgramInformationListDto[] = [];
    VillageList: Village[] = [];
    UserListDto: UserListDto[] = [];
    GetAllSourceList: GetAllSource[] = [];
    GetAllCropList: GetAllCrops[] = [];
    UserDetailsListForMail: GetUserDetailsForMailDto[] = [];
    emailContent: string;
    active: boolean = false;
    saving: boolean = false;

    ProgramTitle: string = "";
    VillageName: string = "";
    FormName: string = "";

    constructor(injector: Injector, private _ProgramServiceProxy: ProgramServiceProxy,
        private _ImpactIndicatorResponseServiceProxy: ImpactIndicatorResponseServiceProxy,
        private _SourceServiceProxy: SourceServiceProxy, private _cropService: CropServiceProxy,
        private _emailServiceProxy: EmailServiceProxy, private _ImpactIndicatorFormServiceProxy: ImpactIndicatorFormServiceProxy
    ) {
        super(injector)
    }

    show(id?: number) {
        this.active = true;
        this.bindPrograms();
        this.bindSource();
        this.bindCrop();
        if (id) {
            this._ImpactIndicatorResponseServiceProxy.getResponseById(id).subscribe(result => {
                this.addFormResponseDto = result;
                this.OnProjectSelect(null,result.id, result.programId)
                this.modal.show();
            });
        }
        else {
            this.addFormResponseDto = new AddFormResponseDto();
            this.modal.show();
        }

    }

    OnProjectSelect(event, respId?: number, progId?: number) {
        if (!respId) {
            this.bindAllVillages(event.target.value);
            this.bindUsers(event.target.value);
            this.bindQuestions(event.target.value);
        } else {
            this.bindAllVillages(progId);
            this.bindUsers(progId);
            this.bindQuestions(progId,respId);
        }
    }

    bindPrograms() {
        this._ImpactIndicatorResponseServiceProxy.getAllProgramForReport().subscribe(result => {
            this.ProgramList = result;
        });
    }

    bindUsers(ProjId) {
        this._ImpactIndicatorResponseServiceProxy.getProgramUsers(ProjId).subscribe(result => {
            this.UserListDto = result.items;
        });
    }

    bindAllVillages(ProjId) {
        this._ImpactIndicatorResponseServiceProxy.getVillagesByProgramIdForResponse(ProjId).subscribe(result => {
            this.VillageList = result;
        });
    }

    bindSource() {
        this._SourceServiceProxy.getAllSource('').subscribe(result => {
            this.GetAllSourceList = result;
        });
    }

    bindCrop() {
        this._cropService.getAllCrop('').subscribe(result => {
            this.GetAllCropList = result;
        });
    }

    bindQuestions(ProjId: number, id?: number) {
        abp.ui.setBusy();
        this._ImpactIndicatorResponseServiceProxy.getImpactIndicatorFormByProgramId(ProjId).subscribe(result => {
            this.addFormResponseDto.impactIndicatorFormId = result;
        });
        this._ImpactIndicatorResponseServiceProxy.getAllFormResponseDetailsByProgramId(ProjId, id).subscribe(result => {
            this.addFormResponseDto.frequencyOccurenceList = result;
            abp.ui.clearBusy();
        });
    }

    changeEntryDate(event, res: any) {
        res.entryDate = moment.utc(new Date(event));
    }

    changeCollectedDate(event, res: any) {
        res.collectedDate = moment.utc(new Date(event));
    }

    changeDate(event, res: any) {
        res.dateOfEntry = moment.utc(new Date(event));
    }

    onAddRow(res: any, rec: GetQuestionType) {
        var d = new GetQuestionType();
        d.type = Math.max.apply(Math, res.map(function (o) { return o.type; })) + 1;
        d.dateOfEntry = moment.utc(new Date());
        d.dateOfCollection = moment.utc(new Date());
        d.sourceId = 0;
        d.questionList = [];
        for (let p of rec.questionList) {
            var f = new GetAllQuestionsFromForm();
            f.question = p.question;
            f.questionType = p.questionType;
            f.iiQuestionaryId = p.iiQuestionaryId;
            d.questionList.push(f);
        }
        res.push(d);
    }

    onRemoveRow(res: any, rec: GetQuestionType) {
        if (rec.questionList.filter(t => t.id == null).length != 0) {
            const index: number = res.indexOf(rec);
            if (index !== -1) {
                res.splice(index, 1);
            }
        } else {
            this.message.confirm(
                this.l('Answer', ''),
                (isConfirmed) => {
                    if (isConfirmed) {
                        for (let p of rec.questionList) {
                            this._ImpactIndicatorResponseServiceProxy.deleteFormAnswer(p.id)
                                .subscribe((result) => {
                                    this.notify.success(this.l('Successfully Deleted  Record'));
                                });
                        }
                        this.close();
                    };
                });
        }
    }

    save() {
        this.saving = true;
        this._ImpactIndicatorResponseServiceProxy.saveImpactIndicatorResponse(this.addFormResponseDto)
            .finally(() => this.saving = false)
            .subscribe((result) => {
                if (result == "Response Added  sucessfully !" || result == "Response Added  sucessfully !") {
                this._ImpactIndicatorFormServiceProxy.getImapactIndiactorFormForEdit(this.addFormResponseDto.impactIndicatorFormId).subscribe(result => {
                  this.sendNotification(result.formName);
                });
                    this.notify.info(this.l(result));
                }
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {        
        this.active = false;
        this.modal.hide();
    }

    sendNotification(FormName) {
        this._ProgramServiceProxy.getProgramDetailsByProgramID(this.addFormResponseDto.programId).subscribe(result => {
            this.ProgramTitle = result.programName;
        });

        this._ImpactIndicatorResponseServiceProxy.getVillageDeatilsByVillageId(this.addFormResponseDto.villageId).subscribe(result => {
            this.VillageName = result.name;
        });
     
        

        this._ImpactIndicatorResponseServiceProxy.getProgramAndProjectManagerByProgramId(this.addFormResponseDto.programId).subscribe(result => {
            this.UserDetailsListForMail = result;
            Array.from(new Set(this.UserDetailsListForMail.map((item: any) => item.emailId)));
            var thiscopy = this;
            this.UserDetailsListForMail.forEach(function (values) {

                thiscopy.emailContent = "Dear " + values.name + ",\n" + "<h4>Impact Indicator response form</h4>\n" +
                    "<STRONG> Program Name : </STRONG>" + thiscopy.ProgramTitle +
                    "\n<div></div>" + "<STRONG> Village Name : </STRONG>" + thiscopy.VillageName +
                    "\n<div></div>" + "<STRONG> Form Name : </STRONG>" + FormName + "\n<div></div>" +
                    "<span class='text-muted'><div></div> \n Please go through the form response. </span>" +
                    "<div>" + "<h3>Kindly visit for more details </h3>\n <div>https://pms.wotr.org.in/</div>";
                var thisCopy1 = thiscopy;
                thiscopy._emailServiceProxy.mailSending(thisCopy1.emailContent, 'Impact Indicator response form', values.emailId, null, null)
                    .subscribe((result) => {
                    });

                thiscopy.emailContent = "";
            })
        });

       
   
        
    }
}

