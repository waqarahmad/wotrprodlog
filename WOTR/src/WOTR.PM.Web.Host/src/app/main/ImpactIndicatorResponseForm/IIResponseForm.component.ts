﻿import { Component, Injector, ViewChild } from '@angular/core';
import { SourceServiceProxy, GetAllSource, UserServiceProxy, UserListDto, GetAllFormResponseGridView, ImpactIndicatorResponseServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { IIResponseModalComponent } from './IIResponseForm.modal.component';
import { LazyLoadEvent, Paginator } from 'primeng/primeng';
import { finalize } from 'rxjs/operators';
//import { Table } from 'primeng/components/table/table';
import { DataTable } from 'primeng/primeng';



@Component({
    selector: 'IIResponseComponent',
    templateUrl: "./IIResponseForm.component.html",
    styleUrls: ['./IIResponseForm.component.less']

})

export class IIResponseComponent extends AppComponentBase {
    @ViewChild('IIResponseModal') myModalView: IIResponseModalComponent;
    @ViewChild('paginator') paginator: Paginator;
    @ViewChild('dataTable') dataTable: DataTable;
    searchText: string = " ";
    Source: GetAllSource[] = [];
    SourceList: GetAllSource[] = []
    FormResponse: GetAllFormResponseGridView[] = [];
    FormResponseList: GetAllFormResponseGridView[] = []
    GetAllFormResponseGridView
    timer: any;
    currentPage: number = 1;
    myDateValue: Date;
    role: number = undefined;
    attritubeDate;


    constructor(
        injector: Injector,
        private _SourceAppService: SourceServiceProxy,
        private _ImpactIndicatorResponseAppService: ImpactIndicatorResponseServiceProxy,
        ) {
        super(injector);
    }

    ngOnInit() {
        this.myDateValue = new Date();
        this.attritubeDate = new Date();  
    }

  
    getAllResponseGrid(event?: LazyLoadEvent) {
        if (this.primengDatatableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }

        this.primengDatatableHelper.showLoadingIndicator();

        this._ImpactIndicatorResponseAppService.getResponseForGrid(
            this.searchText,
            this.primengDatatableHelper.getSorting(this.dataTable),
            this.primengDatatableHelper.getMaxResultCount(this.paginator, event),
            this.primengDatatableHelper.getSkipCount(this.paginator, event),
        ).pipe(finalize(() => this.primengDatatableHelper.hideLoadingIndicator())).subscribe(result => {
            this.primengDatatableHelper.totalRecordsCount = result.totalCount;
            this.primengDatatableHelper.records = result.items;
            this.primengDatatableHelper.hideLoadingIndicator();
        });

    }

    editActionArea(id) {

        this.myModalView.show(id);
    }

    delete(Response) {
        this.message.confirm(
            this.l('Response', ''),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._ImpactIndicatorResponseAppService.deleteFormResponse(Response.iiFormResponseId)
                        .subscribe((result) => {
                            this.notify.success(this.l('Successfully Deleted  Record'));
                            this.getAllResponseGrid();
                        });
                };
            });
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.primengDatatableHelper.records = this.Source.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
        this.SourceList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);

    }

}