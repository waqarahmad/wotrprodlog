﻿import { Component, Injector, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { LocationListDto, LocationServiceProxy, PrgVillageClusterServiceProxy, PrgVillageClusterListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { Router, ActivatedRoute } from '@angular/router';

declare var jQuery: any;


@Component({
    selector: 'ProgramCluster',
    templateUrl: './program-cluster.component.html',
    styleUrls: ['./program-cluster.component.less'],    
    animations: [appModuleAnimation()]
})
export class ProgramClusterComponent extends AppComponentBase {   
    locationList: LocationListDto[];
    RRcList: any;
    villageClusterList: PrgVillageClusterListDto[];
    villageList: any;
    timer: any;
  currentPage: number = 1;
    pagearrayP: Array<number> = [];
    currentPageP: number = 1;
    searchText;
    constructor(
        injector: Injector,
        private _router: Router,
        private _PrgVillageClusterService: PrgVillageClusterServiceProxy,
        private _activatedRoute: ActivatedRoute,

    ) {
        super(injector);

    }
    ngOnInit() {
        this.getAllVillageCluster();
    }

    getAllRecord() {

    }
    searchPC(): void {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => { this.getAllVillageCluster() }, 1000);
    }

    funProgramComponents(){
        this._router.navigate(['/app/main/ProgramComponent'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    funProgramInformation1() {
        this._router.navigate(['/app/main/CreateProgramInformation'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    //getAllvillageclustorRecordsearch(): void {
    //    var prID = this._activatedRoute.snapshot.queryParams.ProgrameId;
    //    var input=this.searchText.trim();
    //    abp.ui.setBusy();
    //    this._PrgVillageClusterService.getAllVillageClustersearch(prID,input).subscribe(result => {
    //        this.villageClusterList = result;
    //        this.primengDatatableHelper.records = result;
    //        this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
    //        this.primengDatatableHelper.totalRecordsCount = result.length;
    //        this.primengDatatableHelper.hideLoadingIndicator();
    //        this.villageClusterList = result;
    //        this.villageClusterList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);

    //        abp.ui.clearBusy();
    //    });
    //}

   

    getAllVillageCluster() {
        abp.ui.setBusy();
        var prID = this._activatedRoute.snapshot.queryParams.ProgrameId;
        if (this.searchText) {
            var input = this.searchText.trim();
        }
        this._PrgVillageClusterService.getAllVillageCluster(prID, input).subscribe(result => {
            this.villageClusterList = result;
            this.primengDatatableHelper.records = result;
            this.primengDatatableHelper.defaultRecordsCountPerPage = 5;
            this.primengDatatableHelper.totalRecordsCount = result.length;
            this.primengDatatableHelper.hideLoadingIndicator();
            this.villageClusterList = this.primengDatatableHelper.records.slice(0, this.primengDatatableHelper.defaultRecordsCountPerPage);

            abp.ui.clearBusy();
        })
    }

    close() {
        this._router.navigate(['/app/main/CreateProgramInformation'], {
            queryParams: {
                ProgrameName: this._activatedRoute.snapshot.queryParams.ProgrameName,
                ProgrameId: this._activatedRoute.snapshot.queryParams.ProgrameId,
            }
        });
    }

    Deleteprogramclustor(del) {
        this.message.confirm(
            this.l('Do you want to delete', ''),
            (isConfirmed) => {
                if (isConfirmed) {
                    this._PrgVillageClusterService.deleteprogramclustor(del.locationId)
                        .subscribe(() => {
                            //this.notify.success(this.l('Successfully Deleted'));
                            this.getAllVillageCluster();
                        });
                }
            });
    }

    changePage(p: any) {
        this.primengDatatableHelper.defaultRecordsCountPerPage = p.rows;
        this.currentPage = p.page + 1;
        this.villageClusterList = this.primengDatatableHelper.records.slice(((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage), ((this.currentPage - 1) * this.primengDatatableHelper.defaultRecordsCountPerPage) + this.primengDatatableHelper.defaultRecordsCountPerPage);
    }

}