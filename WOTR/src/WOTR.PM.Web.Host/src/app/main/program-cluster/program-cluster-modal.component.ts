﻿import { Component, Injector, ViewChild, EventEmitter, Output } from '@angular/core';
import { LocationServiceProxy, State, District, Taluka, PrgVillageClusterListDto, PrgVillageClusterServiceProxy, ClusterUser } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ActivatedRoute } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
    selector: 'ModalProgramCluster',
    templateUrl: './program-cluster-modal.component.html',
    styleUrls: ['./program-cluster-modal.component.less'],
})
export class ModalProgramClusterComponent extends AppComponentBase {
    @ViewChild('ModalProgramCluster') modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    active: boolean = false;
    RRcList: any;
    selectedItems: any;
    selectedItems1: any;
    selectedItems2: any;
    selectedItems11: any;
    selectedItems22: any;
    selectedItems5: any;

    itemList: any;
    itemList1: any;
    itemList2: any;
    itemList5: any;
    settings1: any;
    settings2: any;
    settings: any;
    settings5: any;

    saving: boolean = false;
    programName: string;
    proname: any;
    programid: any;
    stateList: State[];
    districtList: District[] = [];
    talukaList: Taluka[] = [];
    VillageId: number[] = [];
    flag = 0;
    VillageCluster: PrgVillageClusterListDto = new PrgVillageClusterListDto();
    VillageClusterbackup: PrgVillageClusterListDto = new PrgVillageClusterListDto();
    stateIDbackup: any;
    districtIDbackup: any;
    ProjectManagerId: ClusterUser[] = [];
    RRcInchargeId: ClusterUser[] = [];
    TalukaId: number[] = [];

    constructor(
        injector: Injector,
        private _locationService: LocationServiceProxy,
        private _PrgVillageClusterService: PrgVillageClusterServiceProxy,
        private _activatedRoute: ActivatedRoute,
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.itemList = [];
        this.itemList1 = [];
        this.itemList2 = [];
        this.itemList5 = [];
        this.settings1 = {
            singleSelection: false,
            text: "Select RRC Incharge",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        };
        this.settings2 = {
            singleSelection: false,
            text: "Select Project Managers",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        };
        this.settings = {
            singleSelection: false,
            text: "Select Villages",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        };
        this.settings5 = {
            singleSelection: false,
            text: "Select Tehsil",
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            enableSearchFilter: true,
            classes: "myclass custom-class"
        };
        this.getAllLocation();
        this.getAllState();
    }
    locattionid: any;
    show(p): void {
        if (p == undefined) {
            this.flag = 1;
            this.VillageCluster = new PrgVillageClusterListDto();
            this.programName = this._activatedRoute.snapshot.queryParams.ProgrameName;
            this.proname = this._activatedRoute.snapshot.queryParams;
            this.programid = this.proname.ProgrameId;
            this.GetProgramName(this.programid);
            this.selectedItems = [];
            this.selectedItems1 = [];
            this.selectedItems2 = [];
            this.selectedItems11 = [];
            this.selectedItems22 = [];
            this.selectedItems5 = [];
            this.VillageId = [];
            this.RRcInchargeId = [];
            this.ProjectManagerId = [];
            this.talukaList = [];
            this.itemList5 = [];
        }
        else {
            this.flag = 2;
            this.programName = this._activatedRoute.snapshot.queryParams.ProgrameName;
            this.proname = this._activatedRoute.snapshot.queryParams;
            this.programid = this.proname.ProgrameId;
            this.locattionid = p.locationId;

            this.GetProgramName(this.programid);
            this.VillageCluster.rrcid = p.rrcid;
            this.VillageClusterbackup.rrcid = p.rrcid;
            this.StateWiseDistrict(p.stateID);
            this.stateIDbackup = p.stateID;
            this.DistrictWiseTaluka(p.districtID);
            this.districtIDbackup = p.districtID;
            this.selectedItems = [];
            this.VillageId = [];
            for (let v of p.villages) {
                var x = { "id": v.id, "itemName": v.name }
                this.selectedItems.push(x);
                this.itemList = [];
                this.VillageId.push(x.id);
            }
            this.VillageCluster = p;
            this.selectedItems5 = []
            for (let t of p.talukas) {
                var xy = { "id": t.id, "itemName": t.talukaName }
                this.selectedItems5.push(xy);
                this.itemList5 = [];
                this.TalukaWiseVillage(t.id);
            }
            this.VillageCluster.prgClusterUser = [];
            this.RRcWiseManagers(p, 2);
        }
        this.modal.show();
    }

    getAllState() {
        this._locationService.getAllState().subscribe(result => {
            this.stateList = result;
        })
    }

    RRcWiseManagers(p, num) {
        this.itemList1 = [];
        this.itemList2 = [];
        this.VillageCluster.rrcid = p.rrcid;
        this.RRcInchargeId = [];
        this.ProjectManagerId = [];
        this.selectedItems1 = [];
        this.selectedItems2 = [];
        this._PrgVillageClusterService.getAllProgrameRRcInchargeAndProjectManager(p.rrcid)
            .subscribe(result => {
                for (let r of result) {
                    for (let p of r.projectManagers) {
                        this.itemList2.push({
                            id: p.id,
                            itemName: p.userName,
                            userRole: p.userRoleId
                        })
                    }
                    for (let ri of r.rRcIncharge) {
                        this.itemList1.push({
                            id: ri.id,
                            itemName: ri.userName,
                            userRole: ri.userRoleId
                        })
                    }
                }
            });

        if (num == 2) {
            for (let rrc of p.rrcc) {
                var y = { "id": rrc.id, "itemName": rrc.userName, userRole: rrc.userRoleId }
                this.selectedItems2.push(y);
                var userCluster = new ClusterUser();
                userCluster.userId = rrc.id;
                userCluster.userRoleId = rrc.usreRoleId;
                this.ProjectManagerId.push(userCluster);
            }
            for (let rrclist of p.rrcInchargelist) {
                var z = { "id": rrclist.id, "itemName": rrclist.userName, userRole: rrclist.userRoleId }
                this.selectedItems1.push(z);
                var userCluster = new ClusterUser();
                userCluster.userId = rrclist.id;
                userCluster.userRoleId = rrclist.usreRoleId;
                this.RRcInchargeId.push(userCluster);
            }
        }
    }

    StateWiseDistrict(stateId) {
        this._locationService.getAllDistrict(stateId).subscribe(result => {
            this.districtList = result;
        })
        this.selectedItems5 = [];
        this.selectedItems = [];
    }

    DistrictWiseTaluka(districtId) {
        this.itemList5 = [];
        this._locationService.getAllTaluka(districtId).subscribe(result => {
            this.talukaList = result;
            for (let a of result) {
                this.itemList5.push({
                    id: a.id,
                    itemName: a.talukaName
                })
            }
        })
        this.selectedItems5 = [];
        this.selectedItems = [];
    }


    TalukaWiseVillage(talukaId) {
        this.itemList = [];
        this._locationService.getAllVillageByTalukaId(talukaId).subscribe(result => {
            for (let a of result) {
                this.itemList.push({
                    id: a.id,
                    itemName: a.name
                })
            }
        })
    }

    getAllLocation() {
        this._locationService.getAllLocations().subscribe(result => {
            this.RRcList = result.filter(t => t.locationType == 2);
        })
    }

    save() {
        this.saving = true;
        if (this.selectedItems == 0) {
            this.notify.info("Please Select Village");
            return true;
        }

        if (this.selectedItems2 == 0) {
            this.notify.info("Please Select Project Manager");
            return true;
        }

        if (this.selectedItems1 == 0) {
            this.notify.info("Please Select RRC Manager");
            return true;
        }

        this.VillageCluster.talukaID = this.TalukaId;
        this.VillageCluster.villageID = this.VillageId;
        this.VillageCluster.prgClusterUser = this.ProjectManagerId.concat(this.RRcInchargeId);
        this.VillageCluster.programID = this._activatedRoute.snapshot.queryParams.ProgrameId;
        this._PrgVillageClusterService.creatOrUpdateVillageCluster1(this.VillageCluster)
            .finally(() => this.saving = false)
            .subscribe((result) => {
                this.notify.info("Save SuccessFully");
                this.close();
                this.modalSave.emit(null);
            });
    }


    onItemSelect(item: any) {
        this.VillageId.push(item.id);
    }

    OnItemDeSelect33(item: any) {
        var index = this.VillageId.indexOf(item.id, 0);
        if (index > -1) {
            this.VillageId.splice(index, 1);
        }
    }

    onSelectAll(items: any) {
        for (let obj of items) {
            this.VillageId.push(obj.id);
        }
    }
    onDeSelectAll(items: any) {
        this.VillageId = [];
    }


    onItemSelect5(item: any) {
        this.TalukaId.push(item.id);
        this.TalukaId.forEach(xx => {
            this.TalukaWiseVillage(xx);

        });
    }
    OnItemDeSelect5(item: any) {
        var index = this.TalukaId.indexOf(item.id, 0);
        if (index > -1) {
            this.TalukaId.splice(index, 1);
        }
    }

    onItemSelect1(item: any) {
        var userCluster = new ClusterUser();
        userCluster.userId = item.id;
        userCluster.userRoleId = item.userRole;
        this.RRcInchargeId.push(userCluster);
    }

    OnItemDeSelect1(item: any) {
        var index = this.RRcInchargeId.findIndex(t => t.userId == item.id);
        if (index > -1) {
            this.RRcInchargeId.splice(index, 1);
        }
    }

    onSelectAll1(items: any) {
        for (let obj of items) {
            var userCluster = new ClusterUser();
            userCluster.userId = obj.id;
            userCluster.userRoleId = obj.userRole;
            this.RRcInchargeId.push(userCluster);
        }
    }
    onDeSelectAll1(items: any) {
        this.selectedItems1 = [];
        this.RRcInchargeId = [];
    }


    onItemSelect2(item: any) {
        var userCluster = new ClusterUser();
        userCluster.userId = item.id;
        userCluster.userRoleId = item.userRole;
        this.ProjectManagerId.push(userCluster);
    }

    OnItemDeSelect2(item: any) {
        var index = this.ProjectManagerId.findIndex(t => t.userId == item.id);
        if (index > -1) {
            this.ProjectManagerId.splice(index, 1);
        }
    }
    onSelectAll2(items: any) {
        for (let obj of items) {
            var userCluster = new ClusterUser();
            userCluster.userId = obj.id;
            userCluster.userRoleId = obj.userRole;
            this.ProjectManagerId.push(userCluster);
        }
    }
    onDeSelectAll2(items: any) {
        this.selectedItems2 = [];
        this.ProjectManagerId = [];
    }


    close(): void {
        if (this.flag == 2) {
            this.selectedItems1 = this.selectedItems11;
            this.selectedItems2 = this.selectedItems22;
            this.VillageCluster.rrcid = this.VillageClusterbackup.rrcid;
            this.VillageCluster.stateID = this.stateIDbackup;
            this.VillageCluster.districtID = this.districtIDbackup;
        }
        else {
            this.selectedItems1 = null;
            this.selectedItems2 = null;
            this.VillageCluster.rrcid = null;
            this.VillageCluster.stateID = null;
            this.VillageCluster.districtID = null;
        }
        this.modal.hide();
        this.active = false;
    }

    GetProgramName(programid) {
        this._PrgVillageClusterService.getprgramname(programid).subscribe(result => {
            this.programName = result;
        });
    }
}