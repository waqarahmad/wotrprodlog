import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SessionServiceProxy, UpdateUserSignInTokenOutput, UserServiceProxy, RoleListDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { LoginService, ExternalLoginProvider } from './login.service';
import { accountModuleAnimation } from '@shared/animations/routerTransition';
import { AbpSessionService } from '@abp/session/abp-session.service';
import { UrlHelper } from 'shared/helpers/UrlHelper';
import { ShowUserRollcomponent } from 'app/shared/layout/UserRoll.component';
import { userInfo } from 'os';

@Component({
    templateUrl: './login.component.html',
    animations: [accountModuleAnimation()]
})
export class LoginComponent extends AppComponentBase implements OnInit {
    @ViewChild('UserRollsSelectModal') UserRollsSelectModal: ShowUserRollcomponent;
    submitting = false;
    isMultiTenancyEnabled: boolean = this.multiTenancy.isEnabled;

    //userRole: any
    //userRole: RoleListDto ();
    userRole: RoleListDto[];
    display: any;
    constructor(
        injector: Injector,
        public loginService: LoginService,
        private _router: Router,
        private _sessionService: AbpSessionService,
        private _sessionAppService: SessionServiceProxy,
        private _userAppService: UserServiceProxy
    ) {
        super(injector);
    }

    get multiTenancySideIsTeanant(): boolean {
        return this._sessionService.tenantId > 0;
    }
    
    get isTenantSelfRegistrationAllowed(): boolean {
        return this.setting.getBoolean('App.TenantManagement.AllowSelfRegistration');
    }

    get isSelfRegistrationAllowed(): boolean {
        if (!this._sessionService.tenantId) {
            return false;
        }

        return this.setting.getBoolean('App.UserManagement.AllowSelfRegistration');
    }

    ngOnInit(): void {
        if (this._sessionService.userId > 0 && UrlHelper.getReturnUrl() && UrlHelper.getSingleSignIn()) {
            this._sessionAppService.updateUserSignInToken()
                .subscribe((result: UpdateUserSignInTokenOutput) => {
                    const initialReturnUrl = UrlHelper.getReturnUrl();
                    const returnUrl = initialReturnUrl + (initialReturnUrl.indexOf('?') >= 0 ? '&' : '?') +
                        'accessToken=' + result.signInToken +
                        '&userId=' + result.encodedUserId +
                        '&tenantId=' + result.encodedTenantId;

                    location.href = returnUrl;
                });
        }
    }

    login(): void {      
        this._userAppService.getUserRoles(this.loginService.authenticateModel.userNameOrEmailAddress).subscribe(res => {
            this.userRole = res;        
            if (res.length > 1) {             
                this.display = "block"; 
            }
            else { 

                this.submitting = true;
                this.loginService.authenticate(
                    () => this.submitting = false
                );
            }
        });
       
    }
    loginUser(e) {      
        this._userAppService.userRoleById(e, this.loginService.authenticateModel.userNameOrEmailAddress).subscribe(res => {
            this.submitting = true;
            this.loginService.authenticate(
                () => this.submitting = false);
        });
    }

    externalLogin(provider: ExternalLoginProvider) {
        this.loginService.externalAuthenticate(provider);
    }
}
