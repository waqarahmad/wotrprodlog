﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.IO;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Users.Profile.Dto;
using WOTR.PM.Checklists;
using WOTR.PM.ProgramExpenses.Dto;
using WOTR.PM.ProgramFunds;
using WOTR.PM.Storage;
using WOTR.PM.UnitOfMeasures;
using WOTR.PM.PrgActionAreaActivitysMappings.Dto;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.ActionAreas;
using WOTR.PM.Programs;
using WOTR.PM.PrgRequestAAPlas;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using WOTR.PM.Dto;
//archita
using A = DocumentFormat.OpenXml.Drawing;
using Xdr = DocumentFormat.OpenXml.Drawing.Spreadsheet;
using A14 = DocumentFormat.OpenXml.Office2010.Drawing;
using System.Drawing;
using ShapeProperties = DocumentFormat.OpenXml.Drawing.Spreadsheet.ShapeProperties;
using Color = DocumentFormat.OpenXml.Spreadsheet.Color;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using Font = DocumentFormat.OpenXml.Spreadsheet.Font;
using Abp.Linq.Extensions;
//archita


namespace WOTR.PM.ProgramExpenses
{
    public class ProgramExpenseAppService : PMAppServiceBase, IProgramExpenseAppService
    {
        private readonly IRepository<ProgramExpense> _ProgramExpenseRepository;
        private readonly IRepository<ChecklistItem> _ChecklistItemRepository;
        private readonly IRepository<ExpensesType> _ExpensesTypeRepository;
        private readonly IAppFolders _appFolders;
        private const int MaxProfilPictureBytes = 1048576; //1MB
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IRepository<ImplementationPlanCheckList> _ImplementationPlanCheckListRepository;
        private readonly IRepository<ProgramExpenseImage> _ProgramExpenseImageRepository;
        private readonly IRepository<PrgActionAreaActivityMapping> _PrgActionAreaActivityMappingRepository;
        private readonly IRepository<Activity> _activityRepository;
        private readonly IRepository<Program> _programRepository;
        private readonly IRepository<Locations.Location> _LocationRepository;

        public ProgramExpenseAppService(IRepository<ProgramExpense> ProgramExpenseRepository, IRepository<Program> programRepository,
              IRepository<Locations.Location> LocationRepository,
             IRepository<ChecklistItem> ChecklistItemRepository, IRepository<ExpensesType> ExpensesTypeRepository, IAppFolders appFolders,
              IBinaryObjectManager binaryObjectManager,
              IRepository<ImplementationPlanCheckList> ImplementationPlanCheckListRepository,
              IRepository<ProgramExpenseImage> ProgramExpenseImageRepository, IRepository<Activity> activityRepository,
              IRepository<PrgActionAreaActivityMapping> PrgActionAreaActivityMappingRepository)
        {
            _programRepository = programRepository;
            _LocationRepository = LocationRepository;
            _ProgramExpenseRepository = ProgramExpenseRepository;
            _ChecklistItemRepository = ChecklistItemRepository;
            _ExpensesTypeRepository = ExpensesTypeRepository;
            _appFolders = appFolders;
            _binaryObjectManager = binaryObjectManager;
            _ImplementationPlanCheckListRepository = ImplementationPlanCheckListRepository;
            _ProgramExpenseImageRepository = ProgramExpenseImageRepository;
            _PrgActionAreaActivityMappingRepository = PrgActionAreaActivityMappingRepository;
            _activityRepository = activityRepository;
        }

        public async Task<List<string>> CreateORUpdate(ExpenseDto Input)
        {
            var mse = new List<string>();
            var prgExp = new ProgramExpense();
            try
            {
                if (Input.Id == 0)
                {
                    if (Input.PrgActionAreaActivityMappingID != null)
                    {
                        prgExp.ProgramID = Input.ProgramID;
                        prgExp.ExpensesYear = Input.ExpensesYear;
                        prgExp.PrgActionAreaActivityMappingID = Input.PrgActionAreaActivityMappingID;
                        prgExp.QuarterId = Input.QuarterId;
                        prgExp.ExpensesTypeID = Input.ExpensesTypeID;
                        prgExp.ExpenseDate = Input.ExpenseDate.AddDays(1);
                        prgExp.Remark = Input.Remark;
                        prgExp.Unit = Input.Unit;
                        prgExp.ExpenseTitle = Input.ExpenseTitle;
                        prgExp.Amount = Input.Amount;
                        prgExp.Image = Input.Image;
                        prgExp.ProgramQuqterUnitMappingID = Input.ProgramQuqterUnitMappingID;
                        prgExp.UnitOfMeasuresID = Input.UnitOfMeasuresID;
                        prgExp.ImplementationPlanCheckListID = Input.ImplementationPlanCheckListID;
                        prgExp.Status = Status1.Pending;
                        prgExp.ManagerID = Input.ManagerID;
                        prgExp.TenantId = Input.TenantId;
                        var proexid = await _ProgramExpenseRepository.InsertAndGetIdAsync(prgExp);
                        if (proexid > 0)
                        {
                            foreach (var item in Input.Expensesimage)
                            {
                                var image = new ProgramExpenseImage();
                                image.ProgramExpenseID = proexid;
                                image.image = item.image;

                                await _ProgramExpenseImageRepository.InsertAsync(image);
                            }

                        }
                        mse.Add("SAVE");
                    }
                    else
                    {
                        await _ProgramExpenseRepository.UpdateAsync(prgExp);
                        mse.Add("uPDATE");
                    }
                }
                else if (Input.status == Status1.Approved)
                {
                    var query = await _ProgramExpenseRepository.FirstOrDefaultAsync(e => e.Id == Input.Id);
                    query.Status = Status1.Approved;
                    _ProgramExpenseRepository.Update(query);
                }
                else if (Input.status == Status1.Reject)
                {
                    var query = await _ProgramExpenseRepository.FirstOrDefaultAsync(e => e.Id == Input.Id);
                    query.Status = Status1.Reject;
                    await _ProgramExpenseRepository.UpdateAsync(query);
                }
                else if (Input.Id > 0)
                {
                    var QUERY = await _ProgramExpenseRepository.FirstOrDefaultAsync(E => E.Id == Input.Id);

                    QUERY.ProgramID = Input.ProgramID;
                    QUERY.ExpensesYear = Input.ExpensesYear;
                    QUERY.PrgActionAreaActivityMappingID = Input.PrgActionAreaActivityMappingID;
                    QUERY.QuarterId = Input.QuarterId;
                    QUERY.ExpensesTypeID = Input.ExpensesTypeID;
                    QUERY.ExpenseDate = Input.ExpenseDate.AddDays(1);
                    QUERY.Remark = Input.Remark;
                    QUERY.Unit = Input.Unit;
                    QUERY.ExpenseTitle = Input.ExpenseTitle;
                    QUERY.Amount = Input.Amount;
                    QUERY.Image = Input.Image;
                    QUERY.ProgramQuqterUnitMappingID = Input.ProgramQuqterUnitMappingID;
                    QUERY.UnitOfMeasuresID = Input.UnitOfMeasuresID;
                    QUERY.CreatorUserId = Input.CreatorUserId;
                    QUERY.ImplementationPlanCheckListID = Input.ImplementationPlanCheckListID;
                    QUERY.Status = Status1.Pending;
                    QUERY.ManagerID = Input.ManagerID;
                    QUERY.TenantId = Input.TenantId;

                    await _ProgramExpenseRepository.UpdateAsync(QUERY);

                    foreach (var item in Input.Expensesimage)
                    {
                        if (item.Id == 0)
                        {
                            var image1 = new ProgramExpenseImage();
                            image1.ProgramExpenseID = Input.Id;
                            image1.image = item.image;
                            image1.TenantId = Input.TenantId;
                            image1.CreatorUserId = Input.CreatorUserId;
                            await _ProgramExpenseImageRepository.InsertAsync(image1);
                        }
                        else
                        {
                            var image = await _ProgramExpenseImageRepository.FirstOrDefaultAsync(i => i.Id == item.Id);

                            image.ProgramExpenseID = Input.Id;
                            image.image = item.image;
                            image.TenantId = Input.TenantId;
                            image.CreatorUserId = Input.CreatorUserId;

                            await _ProgramExpenseImageRepository.UpdateAsync(image);
                            mse.Add("UPDATE");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                mse.Add(e.ToString());
            }
            return mse;
        }

        public List<string> getAllEnumLists()
        {
            var name = new List<string>();
            foreach (var item in Enum.GetValues(typeof(ExpenseType)))
            {
                name.Add(item.ToString());
            }
            return name;
        }

        public List<ExpensesTypeDto> GetAllExpensesType()
        {
            var query = _ExpensesTypeRepository.GetAll().ToList().OrderByDescending(e => e.TypeName);

            return new List<ExpensesTypeDto>(ObjectMapper.Map<List<ExpensesTypeDto>>(query));
        }

        public List<ExpenseDto> GetExpenseAsPerProgramID(int programId)
        {



            var queary = (from e in _ProgramExpenseRepository.GetAll().Where(pe => pe.ProgramID == programId)
                          join s in _ImplementationPlanCheckListRepository.GetAll()
                          on e.ImplementationPlanCheckListID equals s.Id
                          join c in _ChecklistItemRepository.GetAll()
                          on s.ChecklistItemID equals c.Id
                          join prein in _PrgActionAreaActivityMappingRepository.GetAll()
                          on e.PrgActionAreaActivityMappingID equals prein.Id
                          join acti in _activityRepository.GetAll() on prein.ActivityID equals acti.Id

                          join program in _programRepository.GetAll() on e.ProgramID equals program.Id


                          join l in _LocationRepository.GetAll() on s.IteamLocationId equals l.Id


                          select new ExpenseDto
                          {
                              ProgramID = e.ProgramID,
                              ExpensesYear = e.ExpensesYear,
                              PrgActionAreaActivityMappingID = e.PrgActionAreaActivityMappingID,
                              ActivityName = prein.Activity.Name,
                              QuarterId = e.QuarterId,
                              ExpenseTitle = e.ExpenseTitle,
                              ExpensesTypeID = e.ExpensesTypeID,
                              ExpensesTypeName = e.ExpensesType.TypeName,
                              ExpenseDate = e.ExpenseDate,
                              Remark = e.Remark,
                              Unit = e.Unit,
                              Amount = e.Amount,
                              Image = e.Image,
                              UnitOfMeasuresID = e.UnitOfMeasuresID,
                              UnitOfMeasuresIDName = e.UnitOfMeasures.Name,
                              subActivityName = c.Name,
                              ProgramQuqterUnitMappingID = e.ProgramQuqterUnitMappingID,
                              status = e.Status,
                              Id = e.Id,
                              ProgramName = program.Name,
                              LocationName = l.Name,
                              Expensesimage = (from img in _ProgramExpenseImageRepository.GetAll()
                                               where (img.ProgramExpenseID == e.Id)
                                               select new expensesimage()
                                               { image = img.image, Id = img.Id }).ToList(),
                          }).ToList();
            return queary;
        }

        public List<ExpenseDto> GetExpenseAsPerProgramIDForExcelReport(GetExpensesPerProgramFilterDto getExpensesPerProgramFilterDto)
        {
            var queary = (from e in _ProgramExpenseRepository.GetAll().Where(pe => pe.ProgramID == getExpensesPerProgramFilterDto.programId && pe.ExpensesYear == getExpensesPerProgramFilterDto.Year).OrderBy(x => x.Id)
                          join s in _ImplementationPlanCheckListRepository.GetAll().Where(es => es.ProgramID == getExpensesPerProgramFilterDto.programId && es.CostEstimationYear == getExpensesPerProgramFilterDto.Year)
                          on e.ImplementationPlanCheckListID equals s.Id
                          join c in _ChecklistItemRepository.GetAll()
                          on s.ChecklistItemID equals c.Id
                          join prein in _PrgActionAreaActivityMappingRepository.GetAll()
                          on e.PrgActionAreaActivityMappingID equals prein.Id
                          join acti in _activityRepository.GetAll() on prein.ActivityID equals acti.Id
                          join program in _programRepository.GetAll() on e.ProgramID equals program.Id
                          join l in _LocationRepository.GetAll() on s.IteamLocationId equals l.Id
                          select new ExpenseDto
                          {
                              ProgramID = e.ProgramID,
                              ExpensesYear = e.ExpensesYear,
                              PrgActionAreaActivityMappingID = e.PrgActionAreaActivityMappingID,
                              ActivityName = prein.Activity.Name,
                              QuarterId = e.QuarterId,
                              ExpenseTitle = e.ExpenseTitle,
                              ExpensesTypeID = e.ExpensesTypeID,
                              ExpensesTypeName = e.ExpensesType.TypeName,
                              ExpenseDate = e.ExpenseDate,
                              ExDate = e.ExpenseDate.ToString("dd/MM/yyyy"),
                              Remark = e.Remark,
                              Unit = e.Unit,
                              Amount = e.Amount,
                              Image = e.Image,
                              UnitOfMeasuresID = e.UnitOfMeasuresID,
                              UnitOfMeasuresIDName = e.UnitOfMeasures.Name,
                              subActivityName = c.Name,
                              ProgramQuqterUnitMappingID = e.ProgramQuqterUnitMappingID,
                              status = e.Status,
                              Id = e.Id,
                              ProgramName = program.Name,
                              LocationName = l.Name,
                              ManagerID = (int)e.ManagerID,
                              ImplementationPlanCheckListID = (int)e.ImplementationPlanCheckListID,
                              ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == e.ManagerID).Name,
                              Expensesimage = (from img in _ProgramExpenseImageRepository.GetAll()
                                               where (img.ProgramExpenseID == e.Id)
                                               select new expensesimage()
                                               { image = img.image, Id = img.Id }).ToList(),
                          });

              var res    = queary
                          .OrderByDescending(x => x.Id).PageBy(getExpensesPerProgramFilterDto)
                          .ToList();

           return res;
        }

        public async Task<FileDto> CreateExcelDoc(string fileName, int programId, string Year)
        {
            var expense = new GetExpensesPerProgramFilterDto
            {
                Filter = "",
                Year = Year,
                programId = programId,
                Sorting = null,
                MaxResultCount =1000,
                SkipCount = 0
            };

            var result = GetExpenseAsPerProgramIDForExcelReport(expense);

            string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";
            string b = @"E:\Reports\";
            string abc = b + a;
            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))


            {

                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();
                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                BlipFill blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                Bitmap bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                ShapeProperties sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                Position pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                Extent ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                AbsoluteAnchor anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                WorksheetDrawing wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                // sheets.Append(sheet);
                wb.Append(fv);
                //archita
                // Constructing header
                Row row = new Row();
                Row row1 = new Row();
                Row row2 = new Row();
                Row row3 = new Row();
                Row row4 = new Row();
                Row row5 = new Row();
                Row row6 = new Row();
                Row row7 = new Row();
                Row row8 = new Row();
                Row row9 = new Row();


                Cell cellString222 = InsertCellInWorksheet("D", 1, wsp);
                cellString222.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                cellString222.DataType = new EnumValue<CellValues>(CellValues.String);

                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cellString222.Append(inlineString);

                Cell cellString22 = InsertCellInWorksheet("D", 2, wsp);
                cellString22.CellValue = new CellValue("Expenditure details");
                cellString22.DataType = new EnumValue<CellValues>(CellValues.String);
                Run run2 = new Run();
                run2.Append(new Text("Expenditure details"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cellString22.Append(inlineString1);

                Cell cell12 = InsertCellInWorksheet("C", 3, wsp);
                cell12.CellValue = new CellValue("Program Title");
                cell12.DataType = new EnumValue<CellValues>(CellValues.Number);

                Cell cellString = InsertCellInWorksheet("D", 3, wsp);
                cellString.CellValue = new CellValue(result[0].ProgramName.ToString());
                cellString.DataType = new EnumValue<CellValues>(CellValues.String);





                Cell cell2 = InsertCellInWorksheet("C", 4, wsp);
                cell2.CellValue = new CellValue("Year");
                cell2.DataType = new EnumValue<CellValues>(CellValues.Number);

                Cell cellString2 = InsertCellInWorksheet("D", 4, wsp);
                cellString2.CellValue = new CellValue(result[0].ExpensesYear.ToString());
                cellString2.DataType = new EnumValue<CellValues>(CellValues.Number);





                var prevmanager = "";
                row3.Append(ConstructCell(" ", CellValues.String),
                    ConstructCell(" ", CellValues.String),
                    ConstructCell("Clustor Name ", CellValues.String),
                    ConstructCell("Manager Name ", CellValues.String));
                sd.AppendChild(row3);
                foreach (var item in result.OrderBy(zz => zz.ManagerName))
                {

                    row3 = new Row();
                    if (prevmanager != item.ManagerName)
                    {
                        prevmanager = item.ManagerName;
                        row3.Append(ConstructCell(" ", CellValues.String),
                            ConstructCell(" ", CellValues.String),
                            ConstructCell(item.LocationName, CellValues.String),
                    ConstructCell(item.ManagerName, CellValues.String));
                        sd.AppendChild(row3);
                    }
                    else
                    {
                        if (prevmanager == item.ManagerName)
                        {

                        }

                    }

                }



                row5 = new Row();
                row5.Append(
                ConstructCell("Quarter", CellValues.String, 2),
                ConstructCell("Activity", CellValues.String, 2),
                ConstructCell("Sub Activity", CellValues.String, 2),
                ConstructCell("Expense Name ", CellValues.String, 2),
                ConstructCell("Expense date", CellValues.String, 2),
                ConstructCell("Expense Type", CellValues.Number, 2),
                ConstructCell("Amount", CellValues.Number, 2),
                ConstructCell("Remark", CellValues.Number, 2),
                ConstructCell("Upload Image", CellValues.String, 2),
                ConstructCell("Status", CellValues.Number, 2));



                sd.AppendChild(row5);
                var total = 0.00;
                var total1 = 0.00;
                var total2 = 0.00;
                var total3 = 0.00;
                var qurarter1 = "Quarter 1 ";

                var results = result.OrderBy(ss => ss.QuarterId);
                foreach (var quartess in results)
                {

                    if (quartess.QuarterId == 1)
                    {

                        foreach (var item in quartess.Expensesimage)
                        {

                            total += Convert.ToDouble(quartess.Amount);

                            row4 = new Row();
                            row4.Append(
                               ConstructCell(qurarter1, CellValues.String, 1),
                               ConstructCell(quartess.ActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartess.subActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartess.ExpenseTitle.ToString(), CellValues.Number, 1),
                               ConstructCell(quartess.ExDate.ToString(), CellValues.String, 1),
                               ConstructCell(quartess.ExpensesTypeName.ToString(), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(quartess.Amount).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(quartess.Remark == null ? "" : quartess.Remark.ToString(), CellValues.Number, 1),
                               ConstructCell(item.image, CellValues.String, 1),
                               ConstructCell(quartess.status.ToString(), CellValues.Number, 1));

                            sd.AppendChild(row4);
                            qurarter1 = "";
                        }

                        if (quartess.Expensesimage.Count() <= 0)
                        {

                            total += Convert.ToDouble(quartess.Amount);

                            row4 = new Row();
                            row4.Append(
                               ConstructCell(qurarter1, CellValues.String, 1),
                               ConstructCell(quartess.ActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartess.subActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartess.ExpenseTitle == null ? "": quartess.ExpenseTitle.ToString(), CellValues.Number, 1),
                               ConstructCell(quartess.ExDate.ToString(), CellValues.String, 1),
                               ConstructCell(quartess.ExpensesTypeName.ToString(), CellValues.Number, 1),
                              ConstructCell(Convert.ToDecimal(quartess.Amount).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(quartess.Remark == null ? "" : quartess.Remark.ToString(), CellValues.Number, 1),
                               ConstructCell("", CellValues.String, 1),
                               ConstructCell(quartess.status.ToString(), CellValues.Number, 1));

                            sd.AppendChild(row4);
                            qurarter1 = "";



                        }

                    }


                }
                int q1 = 0;
                foreach (var quartess in results)
                {


                    if (q1 != quartess.QuarterId)
                    {
                        q1 = quartess.QuarterId;

                        if (quartess.QuarterId == 1)
                        {
                            Row row18 = new Row();

                            row18.Append(
                                           ConstructCell("Total", CellValues.String, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.Number, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.Number, 2),
                                           ConstructCell(Convert.ToDecimal(total).ToString("#,##0.00"), CellValues.Number, 2),
                                           ConstructCell("", CellValues.Number, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.Number, 2));
                            sd.AppendChild(row18);
                        }
                    }
                    else
                    {
                        if (q1 == quartess.QuarterId)
                        {

                        }
                    }
                }

                var qurarter2 = "Quarter2 ";
                foreach (var quartesss in results)
                {



                    if (quartesss.QuarterId == 2)
                    {

                        foreach (var item in quartesss.Expensesimage)
                        {
                            total1 += Convert.ToDouble(quartesss.Amount);


                            row4 = new Row();
                            row4.Append(
                               ConstructCell(qurarter2, CellValues.String, 1),
                               ConstructCell(quartesss.ActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartesss.subActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartesss.ExpenseTitle.ToString(), CellValues.Number, 1),
                               ConstructCell(quartesss.ExDate.ToString(), CellValues.String, 1),
                               ConstructCell(quartesss.ExpensesTypeName.ToString(), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(quartesss.Amount).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(quartesss.Remark == null ? "." : quartesss.Remark.ToString(), CellValues.Number, 1),
                               ConstructCell(item.image, CellValues.Number, 1),
                               ConstructCell(quartesss.status.ToString(), CellValues.Number, 1));

                            sd.AppendChild(row4);
                            qurarter2 = "";

                        }

                        if (quartesss.Expensesimage.Count() <= 0)
                        {
                            total1 += Convert.ToDouble(quartesss.Amount);

                            row4 = new Row();
                            row4.Append(
                               ConstructCell(qurarter2, CellValues.String, 1),
                               ConstructCell(quartesss.ActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartesss.subActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartesss.ExpenseTitle.ToString(), CellValues.Number, 1),
                               ConstructCell(quartesss.ExDate.ToString(), CellValues.String, 1),
                               ConstructCell(quartesss.ExpensesTypeName.ToString(), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(quartesss.Amount).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(quartesss.Remark == null ? "." : quartesss.Remark.ToString(), CellValues.Number, 1),
                               ConstructCell("", CellValues.Number, 1),
                               ConstructCell(quartesss.status.ToString(), CellValues.Number, 1));

                            sd.AppendChild(row4);
                            qurarter2 = "";
                        }
                    }

                }

                int q2 = 0;
                foreach (var quartess in results)
                {


                    if (q2 != quartess.QuarterId)
                    {
                        q2 = quartess.QuarterId;

                        if (quartess.QuarterId == 2)
                        {
                            Row row19 = new Row();

                            row19.Append(
                                           ConstructCell("Total", CellValues.String, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.Number, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.Number, 2),
                                           ConstructCell(Convert.ToDecimal(total1).ToString("#,##0.00"), CellValues.Number, 2),
                                           ConstructCell("", CellValues.Number, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.Number, 2));
                            sd.AppendChild(row19);
                        }
                    }
                    else
                    {
                        if (q2 == quartess.QuarterId)
                        {

                        }
                    }
                }


                var qurarter3 = "Quarter3 ";
                foreach (var quartessss in results)
                {



                    if (quartessss.QuarterId == 3)
                    {

                        foreach (var item in quartessss.Expensesimage)
                        {
                            total2 += Convert.ToDouble(quartessss.Amount);

                            row4 = new Row();
                            row4.Append(
                               ConstructCell(qurarter3, CellValues.String, 1),
                               ConstructCell(quartessss.ActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartessss.subActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartessss.ExpenseTitle.ToString(), CellValues.Number, 1),
                               ConstructCell(quartessss.ExDate.ToString(), CellValues.String, 1),
                               ConstructCell(quartessss.ExpensesTypeName.ToString(), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(quartessss.Amount).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(quartessss.Remark == null ? "" : quartessss.Remark.ToString(), CellValues.Number, 1),
                                ConstructCell(item.image, CellValues.String, 1),
                               ConstructCell(quartessss.status.ToString(), CellValues.Number, 1));

                            sd.AppendChild(row4);
                            qurarter3 = "";

                        }

                        if (quartessss.Expensesimage.Count() <= 0)
                        {
                            total2 += Convert.ToDouble(quartessss.Amount);

                            row4 = new Row();
                            row4.Append(
                               ConstructCell(qurarter3, CellValues.String, 1),
                               ConstructCell(quartessss.ActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartessss.subActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartessss.ExpenseTitle.ToString(), CellValues.Number, 1),
                               ConstructCell(quartessss.ExDate.ToString(), CellValues.String, 1),
                               ConstructCell(quartessss.ExpensesTypeName.ToString(), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(quartessss.Amount).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(quartessss.Remark == null ? "" : quartessss.Remark.ToString(), CellValues.Number, 1),
                                ConstructCell("", CellValues.Number, 1),
                               ConstructCell(quartessss.status.ToString(), CellValues.Number, 1));

                            sd.AppendChild(row4);
                            qurarter3 = "";
                        }
                    }

                }
                int q3 = 0;
                foreach (var quartess in results)
                {


                    if (q3 != quartess.QuarterId)
                    {
                        q3 = quartess.QuarterId;

                        if (quartess.QuarterId == 3)
                        {
                            Row row20 = new Row();

                            row20.Append(
                                           ConstructCell("Total", CellValues.String, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.Number, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.Number, 2),
                                           ConstructCell(Convert.ToDecimal(total2).ToString("#,##0.00"), CellValues.Number, 2),
                                           ConstructCell("", CellValues.Number, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.Number, 2));
                            sd.AppendChild(row20);
                        }
                    }
                    else
                    {
                        if (q3 == quartess.QuarterId)
                        {

                        }
                    }
                }

                var qurarter4 = "Quarter4 ";
                foreach (var quartesssss in results)
                {



                    if (quartesssss.QuarterId == 4)
                    {

                        foreach (var item in quartesssss.Expensesimage)
                        {


                            row4 = new Row();
                            row4.Append(
                               ConstructCell(qurarter3, CellValues.String, 1),
                               ConstructCell(quartesssss.ActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartesssss.subActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartesssss.ExpenseTitle.ToString(), CellValues.Number, 1),
                               ConstructCell(quartesssss.ExDate.ToString(), CellValues.String, 1),
                               ConstructCell(quartesssss.ExpensesTypeName.ToString(), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(quartesssss.Amount).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(quartesssss.Remark == null ? "" : quartesssss.Remark.ToString(), CellValues.Number, 1),
                                ConstructCell(item.image, CellValues.String, 1),
                               ConstructCell(quartesssss.status.ToString(), CellValues.Number, 1));

                            sd.AppendChild(row4);
                            qurarter4 = "";


                        }
                        if (quartesssss.Expensesimage.Count() <= 0)
                        {
                            row4 = new Row();
                            row4.Append(
                               ConstructCell(qurarter3, CellValues.String, 1),
                               ConstructCell(quartesssss.ActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartesssss.subActivityName.ToString(), CellValues.String, 1),
                               ConstructCell(quartesssss.ExpenseTitle.ToString(), CellValues.Number, 1),
                               ConstructCell(quartesssss.ExDate.ToString(), CellValues.String, 1),
                               ConstructCell(quartesssss.ExpensesTypeName.ToString(), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(quartesssss.Amount).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(quartesssss.Remark == null ? "" : quartesssss.Remark.ToString(), CellValues.Number, 1),
                                ConstructCell("", CellValues.String, 1),
                               ConstructCell(quartesssss.status.ToString(), CellValues.Number, 1));

                            sd.AppendChild(row4);
                            qurarter4 = "";
                        }

                    }

                }

                int q4 = 0;
                foreach (var quartess in results)
                {


                    if (q4 != quartess.QuarterId)
                    {
                        q4 = quartess.QuarterId;

                        if (quartess.QuarterId == 4)
                        {
                            Row row21 = new Row();

                            row21.Append(
                                           ConstructCell("Total", CellValues.String, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.Number, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.Number, 2),
                                           ConstructCell(Convert.ToDecimal(total3).ToString("#,##0.00"), CellValues.Number, 2),
                                           ConstructCell("", CellValues.Number, 2),
                                           ConstructCell("", CellValues.String, 2),
                                           ConstructCell("", CellValues.Number, 2));
                            sd.AppendChild(row21);
                        }
                    }
                    else
                    {
                        if (q4 == quartess.QuarterId)
                        {

                        }
                    }
                }




                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();

                //worksheetPart.Worksheet.Save();
            }


            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;



        }
        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
        private Cell ConstructCell(string value, CellValues dataType, uint styleIndex = 0)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType),
                StyleIndex = styleIndex
            };
        }

        private Stylesheet GenerateStylesheet()
        {
            Stylesheet styleSheet = null;

            Fonts fonts = new Fonts(
                new Font( // Index 0 - default
                    new FontSize() { Val = 11 }

                ),
                new Font( // Index 1 - header
                    new FontSize() { Val = 11 },
                    new Bold(),
                    new Color() { Rgb = "000000" }

                ));

            Fills fills = new Fills(
                    new Fill(new PatternFill() { PatternType = PatternValues.None }), // Index 0 - default
                    new Fill(new PatternFill() { PatternType = PatternValues.Gray125 }), // Index 1 - default
                    new Fill(new PatternFill(new ForegroundColor { Rgb = new HexBinaryValue() { Value = "D0CECE" } })
                    { PatternType = PatternValues.Solid }) // Index 2 - header
                );

            Borders borders = new Borders(
                    new Border(), // index 0 default
                    new Border( // index 1 black border
                        new LeftBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new RightBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new TopBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new BottomBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                );

            CellFormats cellFormats = new CellFormats(
                    new CellFormat(), // default
                    new CellFormat { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }, // body
                    new CellFormat { FontId = 1, FillId = 2, BorderId = 1, ApplyFill = true } // header
                );

            styleSheet = new Stylesheet(fonts, fills, borders, cellFormats);

            return styleSheet;
        }


        public Guid UplodImage(UpdateProfilePictureInput input)
        {
            var tempProfilePicturePath = Path.Combine(_appFolders.TempFileDownloadFolder, input.FileName);

            byte[] byteArray;

            using (var fsTempProfilePicture = new FileStream(tempProfilePicturePath, FileMode.Open))
            {
                using (var bmpImage = new System.Drawing.Bitmap(fsTempProfilePicture))
                {
                    var width = input.Width == 0 ? bmpImage.Width : input.Width;
                    var height = input.Height == 0 ? bmpImage.Height : input.Height;
                    var bmCrop = bmpImage.Clone(new System.Drawing.Rectangle(input.X, input.Y, width, height), bmpImage.PixelFormat);

                    using (var stream = new MemoryStream())
                    {
                        bmCrop.Save(stream, bmpImage.RawFormat);
                        byteArray = stream.ToArray();
                    }
                }
            }

            if (byteArray.Length > MaxProfilPictureBytes)
            {
                throw new UserFriendlyException(L("ResizedProfilePicture_Warn_SizeLimit", AppConsts.ResizedMaxProfilPictureBytesUserFriendlyValue));
            }



            var storedFile = new BinaryObject(AbpSession.TenantId, byteArray);
            _binaryObjectManager.SaveAsync(storedFile);

            return storedFile.Id;



        }


        public async Task UploadPicture(UpdateProfilePictureInput input)
        {
            var tempProfilePicturePath = Path.Combine(_appFolders.TempFileDownloadFolder, input.FileName);

            byte[] byteArray;

            using (var fsTempProfilePicture = new FileStream(tempProfilePicturePath, FileMode.Open))
            {
                using (var bmpImage = new System.Drawing.Bitmap(fsTempProfilePicture))
                {
                    var width = input.Width == 0 ? bmpImage.Width : input.Width;
                    var height = input.Height == 0 ? bmpImage.Height : input.Height;
                    var bmCrop = bmpImage.Clone(new System.Drawing.Rectangle(input.X, input.Y, width, height), bmpImage.PixelFormat);

                    using (var stream = new MemoryStream())
                    {
                        bmCrop.Save(stream, bmpImage.RawFormat);
                        byteArray = stream.ToArray();
                    }
                }
            }

            if (byteArray.Length > MaxProfilPictureBytes)
            {
                throw new UserFriendlyException(L("ResizedProfilePicture_Warn_SizeLimit", AppConsts.ResizedMaxProfilPictureBytesUserFriendlyValue));
            }



            var storedFile = new BinaryObject(AbpSession.TenantId, byteArray);
            await _binaryObjectManager.SaveAsync(storedFile);
            ProgramExpense p = new ProgramExpense();
            p.Image = storedFile.Id.ToString();

            FileHelper.DeleteIfExists(tempProfilePicturePath);
        }


        public void DeleteExpense(EntityDto<int> Input)
        {
            var query = _ProgramExpenseRepository.GetAll().Where(vc => vc.Id == Input.Id).ToList();
            foreach (var item in query)
            {
                _ProgramExpenseRepository.Delete(item.Id);
            }
        }

        public void DeleteExpenseImage(int ExpenseImageId) {
            _ProgramExpenseImageRepository.Delete(ExpenseImageId);
        }
    }
}
