﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using WOTR.PM.ProgramExpenses.Dto;
using System.IO;
using System.Drawing;
using Abp.UI;

namespace WOTR.PM.ProgramExpenses
{
    public class UploadExpensesFileAppservice : PMAppServiceBase, IUploadExpensesFileAppservice
    {
        private readonly IAppFolders _appFolders;

        public UploadExpensesFileAppservice(
           IAppFolders appFolders
           )
        {
            _appFolders = appFolders;
        }
        static string account = "wotrorg";
        static string key = "GPx2wPffyy8zDkC1uI1spvALxD0yvgfF9r6iCNmmQJFv78UzM6B7kTAiTahPE3EFSDKyR7NenAv0qhjh4lscXQ==";

        public static CloudStorageAccount GetConnectionString()
        {
            string connectionString = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}", account, key);
            return CloudStorageAccount.Parse(connectionString);
        }

        public CloudBlobContainer GetCloudBlobContainer(string containerName)
        {
            CloudBlobContainer container = null;
            try
            {
                // Parse the connection string and return a reference to the storage account.
                var storageAccount = GetConnectionString();
                // Create the blob client.
                var blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container.
                container = blobClient.GetContainerReference(containerName);

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();
                {
                    container.SetPermissions(new BlobContainerPermissions
                    {
                        PublicAccess = BlobContainerPublicAccessType.Blob
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return container;
        }

        public void UploadImage(UploadFileDto input, string containerName)
        {
            // Get reference to blob
            var tempProfilePicturePath = Path.Combine(_appFolders.SampleProfileImagesFolder, input.FileName);

            byte[] byteArray;

            using (var fsTempProfilePicture = new FileStream(tempProfilePicturePath, FileMode.Open))
            {
                using (var bmpImage = new Bitmap(fsTempProfilePicture))
                {
                    var width = input.Width == 0 ? bmpImage.Width : input.Width;
                    var height = input.Height == 0 ? bmpImage.Height : input.Height;
                    var bmCrop = bmpImage.Clone(new Rectangle(input.X, input.Y, width, height), bmpImage.PixelFormat);

                    using (var stream = new MemoryStream())
                    {
                        bmCrop.Save(stream, bmpImage.RawFormat);
                        byteArray = stream.ToArray();
                        var blobContainer = GetCloudBlobContainer(containerName);
                        var blob = blobContainer.GetBlockBlobReference(input.FileName);
                        // blob.Properties.ContentType = input;

                        blob.Properties.ContentType = "attachment; filename=" + Path.GetFileName(input.FileName);
                        blob.UploadFromStream(stream);
                    }
                }
            }

        }

        public List<string> UploadFiles(List<UploadFileDto> input, string containerName)
        {
            List<string> result = new List<string>();
            foreach (var item in input)
            {
                var blobContainer = GetCloudBlobContainer(containerName);

                var blockBlob = blobContainer.GetBlockBlobReference(item.FileName);
                blockBlob.Properties.ContentType = "attachment; filename=" + Path.GetFileName(item.FileName);

                var tempProfilePicturePath = Path.Combine(_appFolders.SampleProfileImagesFolder, item.FileName);
               
                Bitmap bitmap = new Bitmap(180, 300);
                Graphics graphics = Graphics.FromImage(bitmap as Image);
                if (item.FileName.ToString().Contains("https://wotrorg")) {

                    result.Add(item.FileName);
                } else { 
                using (var fileStream = File.OpenRead(tempProfilePicturePath))
                {
                    blockBlob.UploadFromStream(fileStream);
                }
                    result.Add(blockBlob.StorageUri.PrimaryUri.AbsoluteUri);
                }
                
            }

            return result;
        }


        //Renaming blob in azure
        /// <summary>
        ///     1. Copy the file and name it to a new name
        ///     2. Delete the old file
        /// </summary>
        public async Task RenameImage(string fileName, string newFileName, string containerName, string credentia)
        {
            var blobContainer = GetCloudBlobContainer(containerName);
            var blob = blobContainer.GetBlockBlobReference(newFileName);

            if (!await blob.ExistsAsync())
            {
                var oldblob = blobContainer.GetBlockBlobReference(fileName);

                if (await oldblob.ExistsAsync())
                {
                    await blob.StartCopyAsync(oldblob);
                    await oldblob.DeleteIfExistsAsync();
                }
            }
        }

        public Tuple<MemoryStream, string> DownloadFileFromBlob(string imageName, string containerName,
            string credentia)
        {
            // Get Blob Container
            var blobContainer = GetCloudBlobContainer(containerName);
            // Get reference to blob (binary content)
            var blockBlob = blobContainer.GetBlockBlobReference(imageName);
            // get content type
            blockBlob.FetchAttributes();
            var contentType = blockBlob.Properties.ContentType;
            // Read content            
            var stream = new MemoryStream();
            blockBlob.DownloadToStream(stream);
            return new Tuple<MemoryStream, string>(stream, contentType);
        }

        public string DeleteFile(string fileName, string containerName, string credentia)
        {
            try
            {
                //var blobs = container.ListBlobs();
                var blobContainer = GetCloudBlobContainer(containerName);
                var blockBlob = blobContainer.GetBlockBlobReference(fileName);
                blockBlob.Delete();
                return "File is Deleted";
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }



        #region Functions

        public string DeleteImage(string image, string containerName, string credentia)
        {

            var blobContainer = GetCloudBlobContainer(containerName);
            // get temp directory path
            var blockBlob = blobContainer.GetBlockBlobReference(image);


            blockBlob.Delete();
            return "deleted";
        }

        public string DeleteAllBlobs(string containerName, string credentia)
        {

            CloudBlobContainer blobContainer = GetCloudBlobContainer(containerName);
            try
            {
                //Fetches attributes of container
                blobContainer.FetchAttributes();
                Console.WriteLine("Container exists..");
                blobContainer.Delete();
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
            return "deleted";
        }

        #endregion
    }
}
