﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Authorization.Users.Dto;
using WOTR.PM.NewImpactIndicator;
using WOTR.PM.NewImpactIndicator.IIFormProgramMappings.Dto;
using WOTR.PM.NewImpactIndicator.IIForms;
using WOTR.PM.NewImpactIndicator.IIForms.Dto;

namespace WOTR.PM.ImpactIndicatorForms
{
    public class ImpactIndicatorFormAppService : PMAppServiceBase, IImpactIndicatorFormAppService
    {
        private readonly IRepository<ImpactIndicatorForm> _IImpactIndicatorFormRepository;
        private readonly IRepository<IIFormQuestionMapping> _IIFormQuestionMappingRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _abproleRepository;
        private readonly IRepository<IIQuestionary> _questionaryRepository;
        private readonly IRepository<ImpactIndicatorCategory> _impactIndicatorCategoryRepository;
        private readonly IRepository<ImpactIndicatorSubCategory> _impactIndicatorSubCategoryRepository;

        public ImpactIndicatorFormAppService(
            IRepository<ImpactIndicatorForm> ImpactIndicatorForm, IRepository<UserRole, long> userRoleRepository,
            IRepository<IIFormQuestionMapping> IIFormQuestionMapping, IRepository<Role> abproleRepository,
            IRepository<IIQuestionary> questionaryRepository,
            IRepository<ImpactIndicatorCategory> impactIndicatorCategoryRepository,
            IRepository<ImpactIndicatorSubCategory> impactIndicatorSubCategoryRepository


            )
        {
            _IImpactIndicatorFormRepository = ImpactIndicatorForm;
            _IIFormQuestionMappingRepository = IIFormQuestionMapping;
            _userRoleRepository = userRoleRepository;
            _abproleRepository = abproleRepository;
            _questionaryRepository = questionaryRepository;
            _impactIndicatorCategoryRepository = impactIndicatorCategoryRepository;
            _impactIndicatorSubCategoryRepository = impactIndicatorSubCategoryRepository;


        }

        public async Task<string> createEditImapactIndiactorForm(AddEditImpactIndicatorForm Input)
        {
            string message = "";

            ImpactIndicatorForm IIForms = new ImpactIndicatorForm();
            IIForms.FormName = char.ToUpper(Input.FormName[0]) + Input.FormName.Substring(1);

            if (Input.Id == 0)
            {
                try
                {
                    var QueryCheck = _IImpactIndicatorFormRepository.GetAll().Where(aa => aa.FormName == Input.FormName).ToList();
                    if (QueryCheck.Count == 0)
                    {
                        IIForms.FormName = Input.FormName;
                        IIForms.FormStatus = Input.FormStatus;


                        var FormId = await _IImpactIndicatorFormRepository.InsertAndGetIdAsync(IIForms);

                        

                        if (Input.QuestionList.Count > 0)
                        {
                            foreach (var i in Input.QuestionList)
                            {


                                var IIFormsQuestionMapping = new IIFormQuestionMapping();
                                IIFormsQuestionMapping.impactIndicatorFormId = FormId;
                                IIFormsQuestionMapping.iIQuestionaryId = Convert.ToInt32(i);
                                await _IIFormQuestionMappingRepository.InsertAsync(IIFormsQuestionMapping);
                            }
                        }
                        message = "Form Added sucessfully !";

                    }
                    else { message = "Form Already Present!"; }
                }
                catch (Exception ex)
                {

                    //throw new UserFriendlyException(("Record Not Added"));
                }
            }
            else
            {
                try
                {
                    IIForms.Id = Input.Id;
                    IIForms.FormName = Input.FormName;
                    IIForms.FormStatus = Input.FormStatus;
                    _IImpactIndicatorFormRepository.Update(IIForms);

                    string[] questionList = Input.QuestionList.Distinct().ToArray();

                    if (Input.QuestionList.Count > 0)
                    {
                        _IIFormQuestionMappingRepository.Delete(x => x.impactIndicatorFormId == Input.Id);
                        foreach (var i in questionList)
                        {
                            IIFormQuestionMapping IIFormsQuestionMapping = new IIFormQuestionMapping();
                            IIFormsQuestionMapping.impactIndicatorFormId = Input.Id;
                            IIFormsQuestionMapping.iIQuestionaryId = Convert.ToInt32(i);
                            _IIFormQuestionMappingRepository.Insert(IIFormsQuestionMapping);
                        }
                    }
                    message = "Form Update  sucessfully !";

                }
                catch (Exception)
                {

                    throw new UserFriendlyException("Record Not Added");
                }
            }
            return message;
        }

        public List<GetAllImpactIndicatorIForms> GetAllForm(string input)
        {
            List<GetAllImpactIndicatorIForms> getAllImpactIndicatorIForms = new List<GetAllImpactIndicatorIForms>();

            if (input == null)
            {
                var SubCategoryList = _IImpactIndicatorFormRepository.GetAll().ToList();
                foreach (var p in SubCategoryList)
                {
                    GetAllImpactIndicatorIForms impactIndicatorForm = new GetAllImpactIndicatorIForms();
                    impactIndicatorForm.Id = p.Id;
                    impactIndicatorForm.FormName = p.FormName;
                    impactIndicatorForm.FormStatus = p.FormStatus;
                    getAllImpactIndicatorIForms.Add(impactIndicatorForm);
                }


                return getAllImpactIndicatorIForms;

            }
            else
            {
                var SubCategoryList = _IImpactIndicatorFormRepository.GetAll().ToList().Where(x => x.FormName.ToLower().Contains(input.Trim().ToLower()));
                foreach (var p in SubCategoryList)
                {
                    GetAllImpactIndicatorIForms impactIndicatorForm = new GetAllImpactIndicatorIForms();
                    impactIndicatorForm.Id = p.Id;
                    impactIndicatorForm.FormName = p.FormName;
                    impactIndicatorForm.FormStatus = p.FormStatus;
                    getAllImpactIndicatorIForms.Add(impactIndicatorForm);
                }

                return getAllImpactIndicatorIForms;
            }
        }

        public List<GetAllImpactIndicatorIForms> GetAllApprovedForm(string input)
        {
            List<GetAllImpactIndicatorIForms> getAllImpactIndicatorIForms = new List<GetAllImpactIndicatorIForms>();

            if (input == null)
            {
                var SubCategoryList = _IImpactIndicatorFormRepository.GetAll().Where(t=>t.FormStatus == "Approved").ToList();
                foreach (var p in SubCategoryList)
                {
                    GetAllImpactIndicatorIForms impactIndicatorForm = new GetAllImpactIndicatorIForms();
                    impactIndicatorForm.Id = p.Id;
                    impactIndicatorForm.FormName = p.FormName;
                    impactIndicatorForm.FormStatus = p.FormStatus;
                    getAllImpactIndicatorIForms.Add(impactIndicatorForm);
                }


                return getAllImpactIndicatorIForms;

            }
            else
            {
                var SubCategoryList = _IImpactIndicatorFormRepository.GetAll().ToList().Where(x =>x.FormStatus == "Approved" && x.FormName.ToLower().Contains(input.Trim().ToLower()));
                foreach (var p in SubCategoryList)
                {
                    GetAllImpactIndicatorIForms impactIndicatorForm = new GetAllImpactIndicatorIForms();
                    impactIndicatorForm.Id = p.Id;
                    impactIndicatorForm.FormName = p.FormName;
                    impactIndicatorForm.FormStatus = p.FormStatus;
                    getAllImpactIndicatorIForms.Add(impactIndicatorForm);
                }

                return getAllImpactIndicatorIForms;
            }
        }

        public  AddEditImpactIndicatorForm GetImapactIndiactorFormForEdit(int Input) {

            try {
                var returnform = new AddEditImpactIndicatorForm();

                var form = _IImpactIndicatorFormRepository.FirstOrDefault(Input);

                returnform.Id = form.Id;
                returnform.FormName = form.FormName;
                returnform.FormStatus = form.FormStatus;
                var strList = new List<string>();
                var QList = _IIFormQuestionMappingRepository.GetAll().Where(x => x.impactIndicatorFormId == Input);

                foreach (var p in QList)
                {
                    string QId = p.iIQuestionaryId.ToString();
                    strList.Add(QId);
                }
                returnform.QuestionList = strList;
                return returnform;

            }
            catch (Exception ex) {
                throw ex;
            }

        }

        public async Task Delete(EntityDto input)
        {
            try
            {
                await _IImpactIndicatorFormRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public async Task<List<GetUserDetailsForMailDto>> GetAllActionAreaManager()
        {
            var query = (from u in UserManager.Users
                         join ur in _userRoleRepository.GetAll() on u.Id equals ur.UserId
                         join r in _abproleRepository.GetAll() on ur.RoleId equals r.Id
                         where (r.Name.Trim().ToUpper() == "ActionAreaManager")
                         select new GetUserDetailsForMailDto()
                         {
                             Name = u.FullName,
                             EmailId = u.EmailAddress
                         }).OrderBy(r => r.Name).ToList();
            return  query;
            //var result = new List<TeamEmployeeRosterListDto>(ObjectMapper.Map<List<TeamEmployeeRosterListDto>>(query));
            //   return result;
        }

        public async Task<List<QuestionListDto>> FetchAllQuestionsByFormId(int input)
        {

            var QuestionIds = _IIFormQuestionMappingRepository.GetAll().Where(x => x.impactIndicatorFormId == input).Select(z => z.iIQuestionaryId);

            var Questionary = (from q in _questionaryRepository.GetAll().Where(x=> QuestionIds.Any(z=> z == x.Id))
                               join c in _impactIndicatorCategoryRepository.GetAll() on q.ImpactIndicatorCategoryId equals c.Id
                               join s in _impactIndicatorSubCategoryRepository.GetAll() on q.ImpactIndicatorSubCategoryId equals s.Id
                               select new QuestionListDto
                               {
                                   Question = q.Question,
                                   Category = c.Category,
                                   SubCategory = s.SubCategory
                               }).OrderBy(x=> x.Question).ThenBy(z=> z.Category).ThenBy(y=> y.SubCategory).ToList();
            return Questionary;
        }
    }
}
