﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Collections.Extensions;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using OfficeOpenXml.Drawing;
using WOTR.PM.Authorization.Users.Dto;
using WOTR.PM.DataExporting.Excel.EpPlus;
using WOTR.PM.Dto;
using WOTR.PM.PrgRequestAAPlas.Dto;
using WOTR.PM.PrgVillageCluster.Dto;
using WOTR.PM.Programs.Dto;

namespace WOTR.PM.Authorization.Users.Exporting
{
    public class UserListExcelExporter : EpPlusExcelExporterBase, IUserListExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public UserListExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<UserListDto> userListDtos)
        {
            return CreateExcelPackage(
                "UserList.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Users"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Name"),
                        L("Surname"),
                        L("UserName"),
                        L("PhoneNumber"),
                        L("EmailAddress"),
                        L("EmailConfirm"),
                        L("Roles"),
                        L("LastLoginTime"),
                        L("Active"),
                        L("CreationTime")
                        );

                    AddObjects(
                        sheet, 2, userListDtos,
                        _ => _.Name,
                        _ => _.Surname,
                        _ => _.UserName,
                        _ => _.PhoneNumber,
                        _ => _.EmailAddress,
                        _ => _.IsEmailConfirmed,
                        _ => _.Roles.Select(r => r.RoleName).JoinAsString(", "),
                        _ => _timeZoneConverter.Convert(_.LastLoginTime, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.IsActive,
                        _ => _timeZoneConverter.Convert(_.CreationTime, _abpSession.TenantId, _abpSession.GetUserId())
                        );

                    //Formatting cells

                    var lastLoginTimeColumn = sheet.Column(8);
                    lastLoginTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd";

                    var creationTimeColumn = sheet.Column(10);
                    creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd";

                    for (var i = 1; i <= 10; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto Exportprogramdetails(List<PreprogramdetailsDto> userListDtos)
        {


            var village = new List<string>();
            var component = new List<string>();
            var Activity = new List<string>();
            var totalunit = new List<string>();
            var totalunitcost = new List<string>();
            var communitycontribution = new List<string>();
            var FunderContributioncontribution = new List<string>();
            var OtherContributioncontribution = new List<string>();


            foreach (var item in userListDtos.FirstOrDefault().Villages)
            {
                village.Add(item.villageName);
            }
            foreach (var item in userListDtos.FirstOrDefault().components)
            {
                component.Add(item.ComponentName);
            }

            foreach (var item in userListDtos.FirstOrDefault().Activity)
            {
                Activity.Add(item.ActivityName);
                totalunit.Add(item.TotalUnit.ToString());
                totalunitcost.Add(item.TotalUnitCost.ToString());
                communitycontribution.Add(item.CommunityConstribution.ToString());
                FunderContributioncontribution.Add(item.FunderConstribution.ToString());
                OtherContributioncontribution.Add(item.OtherConstribution.ToString());
            }

            return CreateExcelPackage(
                "ProgramDetails.xlsx",
                excelPackage =>
                {
                var sheet = excelPackage.Workbook.Worksheets.Add(L("ProgramDetails"));
                sheet.OutLineApplyStyle = true;


                    var abc = DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";
                    using (SpreadsheetDocument document = SpreadsheetDocument.Create(abc, SpreadsheetDocumentType.Workbook))


                    {
                       

                        WorkbookPart workbookPart = document.AddWorkbookPart();
                        workbookPart.Workbook = new Workbook();

                        WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                        worksheetPart.Worksheet = new Worksheet();

                        // Adding style
                        WorkbookStylesPart stylePart = workbookPart.AddNewPart<WorkbookStylesPart>();
                        stylePart.Stylesheet = GenerateStylesheet();
                        stylePart.Stylesheet.Save();

                        // Setting up columns
                        Columns columns = new Columns(
                                new Column // Id column
                        {
                                    Min = 46,
                                    Max = 46,
                                    Width = 50,
                                    CustomWidth = true
                                },
                                new Column // Name and Birthday columns
                        {
                                    Min = 46,
                                    Max = 46,
                                    Width = 15,
                                    CustomWidth = true
                                },
                                new Column // Salary column
                        {
                                    Min = 46,
                                    Max = 46,
                                    Width = 15,
                                    CustomWidth = true
                                });

                        worksheetPart.Worksheet.AppendChild(columns);

                        Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                        Sheet sheet1 = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "ProgramDetails" };

                        sheets.Append(sheet1);

                        workbookPart.Workbook.Save();

                        

                        SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());

                        // Constructing header
                        Row row = new Row();
                        Row row1 = new Row();
                        Row row2 = new Row();
                        Row row3 = new Row();
                        Row row4 = new Row();
                        Row row5 = new Row();
                        Row row6 = new Row();
                        Row row7 = new Row();
                        Row row8 = new Row();
                        Row row9 = new Row();
                        foreach (var employee in userListDtos.FirstOrDefault().programdetails)
                        {
                            Cell cell = InsertCellInWorksheet("A", 1, worksheetPart);
                            cell.CellValue = new CellValue("Program Title");
                            cell.DataType = new EnumValue<CellValues>(CellValues.Number);

                            Cell cellString = InsertCellInWorksheet("B", 1, worksheetPart);
                            cellString.CellValue = new CellValue(employee.ProgramName.ToString());
                            cellString.DataType = new EnumValue<CellValues>(CellValues.String);


                            Cell cell6 = InsertCellInWorksheet("A", 2, worksheetPart);
                            cell6.CellValue = new CellValue(" ");
                            cell6.DataType = new EnumValue<CellValues>(CellValues.Number);

                            Cell cellString6 = InsertCellInWorksheet("B", 2, worksheetPart);
                            cellString6.CellValue = new CellValue();
                            cellString6.DataType = new EnumValue<CellValues>(CellValues.String);

                            Cell cell1 = InsertCellInWorksheet("A", 3, worksheetPart);
                            cell1.CellValue = new CellValue("Funding Agency");
                            cell1.DataType = new EnumValue<CellValues>(CellValues.Number);

                            Cell cellString1 = InsertCellInWorksheet("B", 3, worksheetPart);
                            cellString1.CellValue = new CellValue(employee.DonorID.ToString());
                            cellString1.DataType = new EnumValue<CellValues>(CellValues.Number);

                            Cell cell2 = InsertCellInWorksheet("A", 4, worksheetPart);
                            cell2.CellValue = new CellValue("Sanction Year");
                            cell2.DataType = new EnumValue<CellValues>(CellValues.Number);

                            Cell cellString2 = InsertCellInWorksheet("B", 4, worksheetPart);
                            cellString2.CellValue = new CellValue(employee.ProgramSanctionedYear.ToString());
                            cellString2.DataType = new EnumValue<CellValues>(CellValues.Number);

                            Cell cell3 = InsertCellInWorksheet("A", 5, worksheetPart);
                            cell3.CellValue = new CellValue("Program Start Date");
                            cell3.DataType = new EnumValue<CellValues>(CellValues.Number);

                            Cell cellString3 = InsertCellInWorksheet("B", 5, worksheetPart);
                            cellString3.CellValue = new CellValue(employee.ProgramStartDate.ToString("yyyy/MM/dd"));
                            cellString3.DataType = new EnumValue<CellValues>(CellValues.String);

                            Cell cell4 = InsertCellInWorksheet("A", 6, worksheetPart);
                            cell4.CellValue = new CellValue("Program End Date");
                            cell4.DataType = new EnumValue<CellValues>(CellValues.Number);

                            Cell cellString4 = InsertCellInWorksheet("B", 6, worksheetPart);
                            cellString4.CellValue = new CellValue(employee.ProgramSubmitDate.ToString("yyyy/MM/dd"));
                            cellString4.DataType = new EnumValue<CellValues>(CellValues.String);

                            Cell cell5 = InsertCellInWorksheet("A", 7, worksheetPart);
                            cell5.CellValue = new CellValue("Grant Amount");
                            cell5.DataType = new EnumValue<CellValues>(CellValues.Number);

                            Cell cellString5 = InsertCellInWorksheet("B", 7, worksheetPart);
                            cellString5.CellValue = new CellValue(employee.GantnAmountSanctionRupess.ToString());
                            cellString5.DataType = new EnumValue<CellValues>(CellValues.Number);
                        }




                        row5.Append(

                         ConstructCell("", CellValues.String));
                        sheetData.AppendChild(row5);
                        row3.Append(

                            ConstructCell("List Of Villages For Project Clustor Wise ", CellValues.String));

                        sheetData.AppendChild(row3);
                        row1.Append(

                            ConstructCell("Clust Name ", CellValues.String, 2));

                        sheetData.AppendChild(row1);

                        // Inserting each employee
                        foreach (var vl in userListDtos.FirstOrDefault().Villages)
                        {
                            row1 = new Row();

                            row1.Append(
                                ConstructCell(vl.villageName.ToString(), CellValues.String, 1));



                            sheetData.AppendChild(row1);
                        }

                        row4.Append(

                           ConstructCell("", CellValues.String));
                        sheetData.AppendChild(row4);
                        row2.Append(

                            ConstructCell("Villages", CellValues.String, 2));
                        sheetData.AppendChild(row2);

                        foreach (var vil in userListDtos.FirstOrDefault().Villages)
                        {
                            row2 = new Row();

                            row2.Append(

                                ConstructCell(vil.villageName.ToString(), CellValues.String, 1));



                            sheetData.AppendChild(row2);
                        }



                        row6.Append(

                         ConstructCell("", CellValues.String));
                        sheetData.AppendChild(row6);
                        row7.Append(

                            ConstructCell("Component", CellValues.String, 2));
                        sheetData.AppendChild(row7);

                        foreach (var compo in userListDtos.FirstOrDefault().components)
                        {
                            row7 = new Row();

                            row7.Append(

                                ConstructCell(compo.ComponentName.ToString(), CellValues.String, 1));



                            sheetData.AppendChild(row7);
                        }
                        row9.Append(

                         ConstructCell("", CellValues.String));
                        sheetData.AppendChild(row9);
                        row8.Append(

                           ConstructCell("Activity", CellValues.String, 2),
                         ConstructCell("TotalUnit ", CellValues.String, 2),
                            ConstructCell("Unit Cost", CellValues.String, 2),
                            ConstructCell("Total Unit Cost", CellValues.Number, 2),
                            ConstructCell("Community Contribution", CellValues.String, 2),
                            ConstructCell("Funder Contribution", CellValues.String, 2),
                            ConstructCell("Other Contribution", CellValues.Number, 2));
                        sheetData.AppendChild(row8);

                        foreach (var acti in userListDtos.FirstOrDefault().Activity)
                        {
                            row8 = new Row();

                            row8.Append(

                                ConstructCell(acti.ActivityName.ToString(), CellValues.String, 1),
                                ConstructCell(acti.TotalUnit.ToString(), CellValues.Number, 1),
                                ConstructCell(acti.UnitCost.ToString(), CellValues.Number, 1),
                                ConstructCell(acti.TotalUnitCost.ToString(), CellValues.Number, 1),
                                ConstructCell(acti.CommunityConstribution.ToString(), CellValues.Number, 1),
                                ConstructCell(acti.FunderConstribution.ToString(), CellValues.Number, 1),
                                ConstructCell(acti.OtherConstribution.ToString(), CellValues.Number, 1));







                            sheetData.AppendChild(row8);
                        }

                        worksheetPart.Worksheet.Save();

                        

                        // HttpContext.Current.Response.Clear();
                        //HttpContext.Current.Response.ClearHeaders();
                        //HttpContext.Current.Response.ClearContent();
                        //HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
                        //HttpContext.Current.Response.AddHeader("Content-Type", "application/Excel");
                        //HttpContext.Current.Response.ContentType = "application/vnd.xls";
                        //HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                        //HttpContext.Current.Response.WriteFile(file.FullName);
                        //HttpContext.Current.Response.End();

                        //HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + abc);
                        //HttpContext.Current.Response.AddHeader("Content-Type", "application/Excel");
                        //HttpContext.Current.Response.ContentType = "application/vnd.xls";
                        // HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                        // HttpContext.Current.Response.WriteFile(file.FullName);

                    }



                    //AddHeader(
                    //    sheet,
                    //    L("Program_Title"),
                    //    L("Sanctioned Year"),
                    //    L("Program Start Date"),
                    //    L("Project End to"),
                    //    L("Grant Amount"),
                    //    L("Villages"),
                    //    L("Component"),
                    //    L("Activity"),
                    //    L("TotalUnit"),
                    //    L("TotalUnitCost"),
                    //    L("Community Contribution"),
                    //    L("Funder Contribution"),
                    //    L("Other Contribution")


                    //    );
                    //string v = "";
                    //foreach (var item in village)
                    //{
                    //    v = v +','+ item;
                    //}

                    //string c = "";
                    //foreach (var item in component)
                    //{
                    //    c = c + ',' + item;
                    //}
                    //string a = "";
                    //foreach (var item in Activity)
                    //{
                    //    a = a + ',' + item;
                    //}
                    //string totalunits = "";
                    //foreach (var item in totalunit)
                    //{
                    //    totalunits = totalunits + ',' + item;
                    //}
                    //string totalunitscost = "";
                    //foreach (var item in totalunitscost)
                    //{
                    //    totalunitscost = totalunitscost + ',' + item;
                    //}
                    //string communitycontributions = "";
                    //foreach (var item in communitycontribution)
                    //{
                    //    communitycontributions = communitycontributions + ',' + item;
                    //}
                    //string FunderContributioncontributions = "";
                    //foreach (var item in FunderContributioncontribution)
                    //{
                    //    FunderContributioncontributions = FunderContributioncontributions + ',' + item;
                    //}
                    //string OtherContributioncontributions = "";
                    //foreach (var item in OtherContributioncontribution)
                    //{
                    //    OtherContributioncontributions = OtherContributioncontributions + ',' + item;
                    //}
                    //AddObjects(
                    //                sheet, 2, userListDtos,
                    //                    _ => _.programdetails.FirstOrDefault().ProgramName,
                    //                    _ => _.programdetails.FirstOrDefault().ProgramSanctionedYear,
                    //                    _ => _.programdetails.FirstOrDefault().ProgramStartDate.ToShortDateString(),
                    //                    _ => _.programdetails.FirstOrDefault().ProgramSubmitDate.ToShortDateString(),
                    //                    _ => _.programdetails.FirstOrDefault().GantnAmountSanctionRupess,
                    //                    _=> v,
                    //                     _ => c,
                    //                      _ => a,
                    //                      _=> totalunits,
                    //                       _ => totalunitscost,
                    //                        _ => communitycontributions,
                    //                           _ => FunderContributioncontributions,
                    //                            _ => OtherContributioncontributions
                    //                           );




            //Formatting cells

            //var lastLoginTimeColumn = sheet.Column(8);
            //lastLoginTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd";

            //var creationTimeColumn = sheet.Column(8);
            //creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd";

            //for (var i = 1; i <= 8; i++)
            //{
            //    sheet.Column(i).AutoFit();
            //}
        });
        }
        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
        private Cell ConstructCell(string value, CellValues dataType, uint styleIndex = 0)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType),
                StyleIndex = styleIndex
            };
        }

        private Stylesheet GenerateStylesheet()
        {
            Stylesheet styleSheet = null;

            Fonts fonts = new Fonts(
                new Font( // Index 0 - default
                    new FontSize() { Val = 11 }

                ),
                new Font( // Index 1 - header
                    new FontSize() { Val = 11 },
                    new Bold(),
                    new Color() { Rgb = "000000" }

                ));

            Fills fills = new Fills(
                    new Fill(new PatternFill() { PatternType = PatternValues.None }), // Index 0 - default
                    new Fill(new PatternFill() { PatternType = PatternValues.Gray125 }), // Index 1 - default
                    new Fill(new PatternFill(new ForegroundColor { Rgb = new HexBinaryValue() { Value = "D0CECE" } })
                    { PatternType = PatternValues.Solid }) // Index 2 - header
                );

            Borders borders = new Borders(
                    new Border(), // index 0 default
                    new Border( // index 1 black border
                        new LeftBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new RightBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new TopBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new BottomBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                );

            CellFormats cellFormats = new CellFormats(
                    new CellFormat(), // default
                    new CellFormat { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }, // body
                    new CellFormat { FontId = 1, FillId = 2, BorderId = 1, ApplyFill = true } // header
                );

            styleSheet = new Stylesheet(fonts, fills, borders, cellFormats);

            return styleSheet;
        }
        public FileDto Exportrequestetails(List<RequestAAPlasListDto> userListDtos)
        {


            
            var Activity = new List<string>();
            var totalunit = new List<string>();
            var totalunitcost = new List<string>();
            var communitycontribution = new List<string>();
            var FunderContributioncontribution = new List<string>();
            var OtherContributioncontribution = new List<string>();


            

            foreach (var item in userListDtos.FirstOrDefault().CostEstimationYear[0].ActionSubactionCost)
            {
                Activity.Add(item.ActivityName);
                totalunit.Add(item.TotalUnits.ToString());
                totalunitcost.Add(item.TotalUnitCost.ToString());
                communitycontribution.Add(item.CommunityContribution.ToString());
                FunderContributioncontribution.Add(item.FunderContribution.ToString());
                OtherContributioncontribution.Add(item.OtherContribution.ToString());
            }

            return CreateExcelPackage(
                "RequestDetails.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RequestDetails"));
                    sheet.OutLineApplyStyle = true;



                    AddHeader(
                        sheet,
                        L("Program_Title"),
                        L("Location"),
                        L("Program Manager"),
                        
                        L("Activity"),
                        L("TotalUnit"),
                        L("TotalUnitCost"),
                        L("Community Contribution"),
                        L("Funder Contribution"),
                        L("Other Contribution")


                        );
                    
                    string a = "";
                    foreach (var item in Activity)
                    {
                        a = a + ',' + item;
                    }
                    string totalunits = "";
                    foreach (var item in totalunit)
                    {
                        totalunits = totalunits + ',' + item;
                    }
                    string totalunitscost = "";
                    foreach (var item in totalunitcost)
                    {
                        totalunitscost = totalunitscost + ',' + item;
                    }
                    string communitycontributions = "";
                    foreach (var item in communitycontribution)
                    {
                        communitycontributions = communitycontributions + ',' + item;
                    }
                    string FunderContributioncontributions = "";
                    foreach (var item in FunderContributioncontribution)
                    {
                        FunderContributioncontributions = FunderContributioncontributions + ',' + item;
                    }
                    string OtherContributioncontributions = "";
                    foreach (var item in OtherContributioncontribution)
                    {
                        OtherContributioncontributions = OtherContributioncontributions + ',' + item;
                    }
                    AddObjects(
                                    sheet, 2, userListDtos,
                                        _ => _.ProgramName,
                                        _ => _.LocationName,
                                        _ => _.ManagerName,
                                         
                                          _ => a,
                                          _ => totalunits,
                                           _ => totalunitscost,
                                            _ => communitycontributions,
                                               _ => FunderContributioncontributions,
                                                _ => OtherContributioncontributions
                                               );




                    //Formatting cells

                    var lastLoginTimeColumn = sheet.Column(8);
                    lastLoginTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd";

                    var creationTimeColumn = sheet.Column(8);
                    creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd";

                    for (var i = 1; i <= 8; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

        public FileDto Exportapprovedetails(List<RequestAAPlasListDto> userListDtos)
        {



            var Activity = new List<string>();
            var totalunit = new List<string>();
            var totalunitcost = new List<string>();
            var communitycontribution = new List<string>();
            var FunderContributioncontribution = new List<string>();
            var OtherContributioncontribution = new List<string>();




            foreach (var item in userListDtos.FirstOrDefault().CostEstimationYear[0].ActionSubactionCost)
            {
                Activity.Add(item.ActivityName);
                totalunit.Add(item.TotalUnits.ToString());
                totalunitcost.Add(item.TotalUnitCost.ToString());
                communitycontribution.Add(item.CommunityContribution.ToString());
                FunderContributioncontribution.Add(item.FunderContribution.ToString());
                OtherContributioncontribution.Add(item.OtherContribution.ToString());
            }

            return CreateExcelPackage(
                "RequestDetails.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("RequestDetails"));
                    sheet.OutLineApplyStyle = true;



                    AddHeader(
                        sheet,
                        L("Program_Title"),
                        L("Location"),
                        L("Program Manager"),

                        L("Activity"),
                        L("TotalUnit"),
                        L("TotalUnitCost"),
                        L("Community Contribution"),
                        L("Funder Contribution"),
                        L("Other Contribution")


                        );

                    string a = "";
                    foreach (var item in Activity)
                    {
                        a = a + ',' + item;
                    }
                    string totalunits = "";
                    foreach (var item in totalunit)
                    {
                        totalunits = totalunits + ',' + item;
                    }
                    string totalunitscost = "";
                    foreach (var item in totalunitcost)
                    {
                        totalunitscost = totalunitscost + ',' + item;
                    }
                    string communitycontributions = "";
                    foreach (var item in communitycontribution)
                    {
                        communitycontributions = communitycontributions + ',' + item;
                    }
                    string FunderContributioncontributions = "";
                    foreach (var item in FunderContributioncontribution)
                    {
                        FunderContributioncontributions = FunderContributioncontributions + ',' + item;
                    }
                    string OtherContributioncontributions = "";
                    foreach (var item in OtherContributioncontribution)
                    {
                        OtherContributioncontributions = OtherContributioncontributions + ',' + item;
                    }
                    AddObjects(
                                    sheet, 2, userListDtos,
                                        _ => _.ProgramName,
                                        _ => _.LocationName,
                                        _ => _.ManagerName,

                                          _ => a,
                                          _ => totalunits,
                                           _ => totalunitscost,
                                            _ => communitycontributions,
                                               _ => FunderContributioncontributions,
                                                _ => OtherContributioncontributions
                                               );




                    //Formatting cells

                    var lastLoginTimeColumn = sheet.Column(8);
                    lastLoginTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd";

                    var creationTimeColumn = sheet.Column(8);
                    creationTimeColumn.Style.Numberformat.Format = "yyyy-mm-dd";

                    for (var i = 1; i <= 8; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }

    }
}
