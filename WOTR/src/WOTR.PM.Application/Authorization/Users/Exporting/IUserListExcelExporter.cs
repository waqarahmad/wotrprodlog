using System.Collections.Generic;
using WOTR.PM.Authorization.Users.Dto;
using WOTR.PM.Dto;
using WOTR.PM.PrgRequestAAPlas.Dto;
using WOTR.PM.PrgVillageCluster.Dto;
using WOTR.PM.Programs.Dto;

namespace WOTR.PM.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
       // FileDto Exportprogramdetails(List<ProgramInformationListDto> userListDtos);
        FileDto Exportprogramdetails(List<PreprogramdetailsDto> result);
        FileDto Exportrequestetails(List<RequestAAPlasListDto> result);
        FileDto Exportapprovedetails(List<RequestAAPlasListDto> result);
       
    }
}