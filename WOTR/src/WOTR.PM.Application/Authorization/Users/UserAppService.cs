﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Configuration;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Notifications;
using Abp.Organizations;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Zero.Configuration;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WOTR.PM.Authorization.Users.Dto;
using WOTR.PM.Url;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Authorization.Users.Exporting;
using WOTR.PM.Notifications;
using WOTR.PM.Dto;
using WOTR.PM.Organizations.Dto;
using WOTR.PM.Authorization.Permissions.Dto;
using WOTR.PM.Authorization.Permissions;
using WOTR.PM.Locations;
using WOTR.PM.Authorization.Roles.Dto;
using WOTR.PM.ActionAreas;

namespace WOTR.PM.Authorization.Users
{

    public class UserAppService : PMAppServiceBase, IUserAppService
    {
        public IAppUrlService AppUrlService { get; set; }
        private readonly IRepository<VillageCluster> _villageClusterRepository;
        private readonly RoleManager _roleManager;
        private readonly IUserEmailer _userEmailer;
        private readonly IUserListExcelExporter _userListExcelExporter;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<RolePermissionSetting, long> _rolePermissionRepository;
        private readonly IRepository<UserPermissionSetting, long> _userPermissionRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IUserPolicy _userPolicy;
        private readonly IEnumerable<IPasswordValidator<User>> _passwordValidators;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<Role> _abproleRepository;
        private readonly IRepository<Location> _locationRepository;
        private readonly IPermissionManager _permissionManager;
        private readonly UserManager _userManager;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<ActionArea> _actionAreaRepository;
        //    _locationAppServiceRepository

        public UserAppService(
            RoleManager roleManager,
            IUserEmailer userEmailer,
            IUserListExcelExporter userListExcelExporter,
            INotificationSubscriptionManager notificationSubscriptionManager,
            IAppNotifier appNotifier,
            IRepository<RolePermissionSetting, long> rolePermissionRepository,
            IRepository<UserPermissionSetting, long> userPermissionRepository,
            IRepository<UserRole, long> userRoleRepository,
            IUserPolicy userPolicy,
            IEnumerable<IPasswordValidator<User>> passwordValidators,
            IPasswordHasher<User> passwordHasher,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
               IRepository<Role> abproleRepository,
               IRepository<Location> locationRepository,
                IPermissionManager permissionManager,
                  UserManager userManager,
                  IRepository<User, long> userRepository,
                  IRepository<ActionArea> actionAreaRepository, IRepository<VillageCluster> villageClusterRepository
            )
        {
            _roleManager = roleManager;
            _userEmailer = userEmailer;
            _userListExcelExporter = userListExcelExporter;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;
            _rolePermissionRepository = rolePermissionRepository;
            _userPermissionRepository = userPermissionRepository;
            _userRoleRepository = userRoleRepository;
            _userPolicy = userPolicy;
            _passwordValidators = passwordValidators;
            _passwordHasher = passwordHasher;
            _organizationUnitRepository = organizationUnitRepository;
            _abproleRepository = abproleRepository;
            _locationRepository = locationRepository;
            _permissionManager = permissionManager;
            _userManager = userManager;
            AppUrlService = NullAppUrlService.Instance;
            _userRepository = userRepository;
            _actionAreaRepository = actionAreaRepository;
            _villageClusterRepository = villageClusterRepository;
        }

        public async Task<PagedResultDto<UserListDto>> GetUsers(GetUsersInput input)
        {
            var query = UserManager.Users
                .WhereIf(input.Role.HasValue, u => u.Roles.Any(r => r.RoleId == input.Role.Value))
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u =>
                        u.Name.Contains(input.Filter) ||
                        u.Surname.Contains(input.Filter) ||
                        u.UserName.Contains(input.Filter) ||
                        u.EmailAddress.Contains(input.Filter)
                );

            if (!input.Permission.IsNullOrWhiteSpace())
            {
                query = (from user in query
                         join ur in _userRoleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                         from ur in urJoined.DefaultIfEmpty()
                         join up in _userPermissionRepository.GetAll() on new { UserId = user.Id, Name = input.Permission } equals new { up.UserId, up.Name } into upJoined
                         from up in upJoined.DefaultIfEmpty()
                         join rp in _rolePermissionRepository.GetAll() on new { RoleId = ur.RoleId, Name = input.Permission } equals new { rp.RoleId, rp.Name } into rpJoined
                         from rp in rpJoined.DefaultIfEmpty() 
                         where (up != null && up.IsGranted) || (up == null && rp != null)
                         group user by user into userGrouped
                         select userGrouped.Key);
            } 
            

            var userCount = await query.CountAsync();

            var users = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var userListDtos = ObjectMapper.Map<List<UserListDto>>(users);

            foreach (var item in userListDtos)
            {
                var actionArea = await _actionAreaRepository.GetAll().Where(a => a.Id == item.ActionId).SingleOrDefaultAsync();
                if (actionArea != null)
                {
                    item.ActionName = actionArea.Name;
                }
            } 

            await FillRoleNames(userListDtos);

            return new PagedResultDto<UserListDto>(
                userCount,
                userListDtos
                );
        }

        public async Task<FileDto> GetUsersToExcel()
        {
            var users = await UserManager.Users.ToListAsync();
            var userListDtos = ObjectMapper.Map<List<UserListDto>>(users);
            await FillRoleNames(userListDtos);

            return _userListExcelExporter.ExportToFile(userListDtos);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Create, AppPermissions.Pages_Administration_Users_Edit)]
        public async Task<GetUserForEditOutput> GetUserForEdit(NullableIdDto<long> input)
        {
            //Getting all available roles
            var userRoleDtos = await _roleManager.Roles
                .OrderBy(r => r.DisplayName)
                .Select(r => new UserRoleDto
                {
                    RoleId = r.Id,
                    RoleName = r.Name,
                    RoleDisplayName = r.DisplayName
                })
                .ToArrayAsync();

            var allOrganizationUnits = await _organizationUnitRepository.GetAllListAsync();

            var output = new GetUserForEditOutput
            {
                Roles = userRoleDtos,
                AllOrganizationUnits = ObjectMapper.Map<List<OrganizationUnitDto>>(allOrganizationUnits),
                MemberedOrganizationUnits = new List<string>()
            };

            if (!input.Id.HasValue)
            {
                //Creating a new user
                output.User = new UserEditDto
                {
                    IsActive = true,
                    ShouldChangePasswordOnNextLogin = true,
                    IsTwoFactorEnabled = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.TwoFactorLogin.IsEnabled),
                    IsLockoutEnabled = await SettingManager.GetSettingValueAsync<bool>(AbpZeroSettingNames.UserManagement.UserLockOut.IsEnabled)
                };

                foreach (var defaultRole in await _roleManager.Roles.Where(r => r.IsDefault).ToListAsync())
                {
                    var defaultUserRole = userRoleDtos.FirstOrDefault(ur => ur.RoleName == defaultRole.Name);
                    if (defaultUserRole != null)
                    {
                        defaultUserRole.IsAssigned = true;
                    }
                }
            }
            else
            {
                //Editing an existing user
                var user = await UserManager.GetUserByIdAsync(input.Id.Value);

                output.User = ObjectMapper.Map<UserEditDto>(user);
                output.ProfilePictureId = user.ProfilePictureId;

                foreach (var userRoleDto in userRoleDtos)
                {
                    userRoleDto.IsAssigned = await UserManager.IsInRoleAsync(user, userRoleDto.RoleName);
                }

                var organizationUnits = await UserManager.GetOrganizationUnitsAsync(user);
                output.MemberedOrganizationUnits = organizationUnits.Select(ou => ou.Code).ToList();
            }

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_ChangePermissions)]
        public async Task<GetUserPermissionsForEditOutput> GetUserPermissionsForEdit(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            var permissions = PermissionManager.GetAllPermissions();
            var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

            return new GetUserPermissionsForEditOutput
            {
                Permissions = ObjectMapper.Map<List<FlatPermissionDto>>(permissions).OrderBy(p => p.DisplayName).ToList(),
                GrantedPermissionNames = grantedPermissions.Select(p => p.Name).ToList()
            };
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_ChangePermissions)]
        public async Task ResetUserSpecificPermissions(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            await UserManager.ResetAllPermissionsAsync(user);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_ChangePermissions)]
        public async Task UpdateUserPermissions(UpdateUserPermissionsInput input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            var grantedPermissions = PermissionManager.GetPermissionsFromNamesByValidating(input.GrantedPermissionNames);
            await UserManager.SetGrantedPermissionsAsync(user, grantedPermissions);
        }

        public async Task CreateOrUpdateUser(CreateOrUpdateUserInput input)
        {
            if (input.User.Id.HasValue)
            {
                await UpdateUserAsync(input);
            }
            else
            {
                input.User.Password = "Welcome1";
                await CreateUserAsync(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Delete)]
        public async Task DeleteUser(EntityDto<long> input)
        {
            if (input.Id == AbpSession.GetUserId())
            {
                throw new UserFriendlyException(L("YouCanNotDeleteOwnAccount"));
            }

            var user = await UserManager.GetUserByIdAsync(input.Id);
            CheckErrors(await UserManager.DeleteAsync(user));
        }

        public async Task UnlockUser(EntityDto<long> input)
        {
            var user = await UserManager.GetUserByIdAsync(input.Id);
            user.Unlock();
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Edit)]
        protected virtual async Task UpdateUserAsync(CreateOrUpdateUserInput input)
        {
            Debug.Assert(input.User.Id != null, "input.User.Id should be set.");

            var user = await UserManager.FindByIdAsync(input.User.Id.Value.ToString());

            //Update user properties
            ObjectMapper.Map(input.User, user); //Passwords is not mapped (see mapping configuration)

            if (input.SetRandomPassword)
            {
                // input.User.Password = User.CreateRandomPassword();
            }
            if (!input.User.Password.IsNullOrEmpty())
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                CheckErrors(await UserManager.ChangePasswordAsync(user, input.User.Password));
            }
            user.Roles = new Collection<UserRole>();
            var delPrevRole = _userRoleRepository.GetAll().Where(r => r.UserId == user.Id);
            foreach (var item in delPrevRole)
            {
                var cluster = _villageClusterRepository.GetAll().Any(t => t.UserRoleId == item.Id);

                if (!cluster)
                {
                    await _userRoleRepository.DeleteAsync(item.Id);
                }
                else
                {
                    var role = await _roleManager.GetRoleByIdAsync(item.RoleId);
                    var f =  input.AssignedRoleNames.Contains(role.Name);
                    if (f)
                    {
                        input.AssignedRoleNames = input.AssignedRoleNames.Where(t => t != role.Name).ToArray();
                    }
                    else
                    {
                        throw new UserFriendlyException(L("YouCanNotRemoveRole"),role.DisplayName);
                    }
                }
            }
           

            foreach (var roleName in input.AssignedRoleNames)
            {
                var role = await _roleManager.GetRoleByNameAsync(roleName);
                
                user.Roles.Add(new UserRole(AbpSession.TenantId, user.Id, role.Id)); // assign new fresh roles to user
                user.UserRole = role.Id;
            }


            //CheckErrors(await UserManager.UpdateAsync(user));
            await UserManager.UpdateAsync(user);
            

            //Update roles 

            //    CheckErrors(await UserManager.SetRoles(user, input.AssignedRoleNames));



            //update organization units
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());

            if (input.SendActivationEmail)
            {
                user.SetNewEmailConfirmationCode();
                await _userEmailer.SendEmailActivationLinkAsync(
                    user,
                    AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
                    input.User.Password
                );
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Users_Create)]
        protected virtual async Task CreateUserAsync(CreateOrUpdateUserInput input)
        {
            if (AbpSession.TenantId.HasValue)
            {
                await _userPolicy.CheckMaxUserCountAsync(AbpSession.GetTenantId());
            }

            var user = ObjectMapper.Map<User>(input.User); //Passwords is not mapped (see mapping configuration)
            user.TenantId = AbpSession.TenantId;

            //Set password
            if (!input.User.Password.IsNullOrEmpty())
            {
                await UserManager.InitializeOptionsAsync(AbpSession.TenantId);
                foreach (var validator in _passwordValidators)
                {
                    CheckErrors(await validator.ValidateAsync(UserManager, user, input.User.Password));
                }
            }
            else
            {
                input.User.Password = User.CreateRandomPassword();
            }

            user.Password = _passwordHasher.HashPassword(user, input.User.Password);

            user.ShouldChangePasswordOnNextLogin = input.User.ShouldChangePasswordOnNextLogin;

            //Assign roles
            user.Roles = new Collection<UserRole>();
            foreach (var roleName in input.AssignedRoleNames)
            {
                var role = await _roleManager.GetRoleByNameAsync(roleName);
                user.Roles.Add(new UserRole(AbpSession.TenantId, user.Id, role.Id));
                user.UserRole = role.Id;
            }

            CheckErrors(await UserManager.CreateAsync(user));
            await CurrentUnitOfWork.SaveChangesAsync(); //To get new user's Id.


            //if (input.AssignedRoleNames[0] == "User")
            //{
            //    var userRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.User);
            //    await _roleManager.GrantAllPermissionsAsync(userRole);
            //    var permission = _permissionManager.GetAllPermissions();

            //    List<ProhibitPermissionInput> pinput = new List<ProhibitPermissionInput>();
            //    pinput.Add(new ProhibitPermissionInput { UserId = user.Id, Permission = AppPermissions.Pages_Tenant_Dashboard });
            //    foreach (var permit in pinput)
            //    {
            //        await ProhibitPermission(permit);
            //    }
            //}
            //else if (input.AssignedRoleNames[0] == "ProjectManager")
            //{
            //    var userRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.ProjectManager);
            //    await _roleManager.GrantAllPermissionsAsync(userRole);
            //    var permission = _permissionManager.GetAllPermissions();

            //    List<ProhibitPermissionInput> pinput = new List<ProhibitPermissionInput>();
            //    pinput.Add(new ProhibitPermissionInput { UserId = user.Id, Permission = AppPermissions.Pages_RequestAAPlaSend});
            //    foreach (var permit in pinput)
            //    {
            //        await ProhibitPermission(permit);
            //    }
            //}

            //Notifications
            await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(user.ToUserIdentifier());
            await _appNotifier.WelcomeToTheApplicationAsync(user);

            //Organization Units
            await UserManager.SetOrganizationUnitsAsync(user, input.OrganizationUnits.ToArray());

            //Send activation email
            //if (input.SendActivationEmail)
            //{
            //    user.SetNewEmailConfirmationCode();
            //    await _userEmailer.SendEmailActivationLinkAsync(
            //        user,
            //        AppUrlService.CreateEmailActivationUrlFormat(AbpSession.TenantId),
            //        input.User.Password
            //    );
            //}
        }

        public async Task ProhibitPermission(ProhibitPermissionInput input)
        {
            var user = await _userManager.GetUserByIdAsync(input.UserId);
            var permission = _permissionManager.GetPermission(input.Permission);

            await _userManager.ProhibitPermissionAsync(user, permission);
        }

        public List<GetAllManagerDto> GetAllManager()
        {
            var query = (from u in UserManager.Users
                         join ur in _userRoleRepository.GetAll() on u.Id equals ur.UserId
                         join r in _abproleRepository.GetAll() on ur.RoleId equals r.Id
                         join l in _locationRepository.GetAll() on u.LocationId equals l.Id
                         where (r.Name.Trim().ToUpper() == "ProgramManager")
                         select new GetAllManagerDto()
                         {
                             Name = u.FullName,
                             ManagerId = u.Id,
                             ManaggerLocation = l.Name
                         }).OrderBy(r => r.Name).ToList();
            return query;
            //var result = new List<TeamEmployeeRosterListDto>(ObjectMapper.Map<List<TeamEmployeeRosterListDto>>(query));
            //   return result;
        }






        public List<GetAllManagerDto> GetAllManagerByLoaction(int locatinID)
        {
            var query = (from u in UserManager.Users
                         join ur in _userRoleRepository.GetAll() on u.Id equals ur.UserId
                         join r in _abproleRepository.GetAll() on ur.RoleId equals r.Id
                         join l in _locationRepository.GetAll() on u.LocationId equals l.Id
                         where (r.Name.Trim().ToUpper() == "ProjectManager" && u.LocationId == locatinID)
                         select new GetAllManagerDto()
                         {
                             Name = u.FullName,
                             ManagerId = u.Id,
                             ManaggerLocation = l.Name
                         }).OrderBy(r => r.Name).ToList();
            return query;
            //var result = new List<TeamEmployeeRosterListDto>(ObjectMapper.Map<List<TeamEmployeeRosterListDto>>(query));
            //   return result;
        }

        public async Task<string> GetUserRoleById(int roleid)
        {
            if (roleid != 0)
            {
                var apbrole =await _abproleRepository.FirstOrDefaultAsync(x => x.Id == roleid);
                return apbrole.DisplayName;
            }
            else
            {
                return "ADMIN";
            }
        }

        private async Task FillRoleNames(List<UserListDto> userListDtos)
        {
            /* This method is optimized to fill role names to given list. */

            var userRoles = await _userRoleRepository.GetAll()
                .Where(userRole => userListDtos.Any(user => user.Id == userRole.UserId))
                .Select(userRole => userRole).ToListAsync();

            var distinctRoleIds = userRoles.Select(userRole => userRole.RoleId).Distinct();

            foreach (var user in userListDtos)
            {
                var rolesOfUser = userRoles.Where(userRole => userRole.UserId == user.Id).ToList();
                user.Roles = ObjectMapper.Map<List<UserListRoleDto>>(rolesOfUser);
            }

            var roleNames = new Dictionary<int, string>();
            foreach (var roleId in distinctRoleIds)
            {
                roleNames[roleId] = (await _roleManager.GetRoleByIdAsync(roleId)).DisplayName;
            }

            foreach (var userListDto in userListDtos)
            {
                foreach (var userListRoleDto in userListDto.Roles)
                {
                    userListRoleDto.RoleName = roleNames[userListRoleDto.RoleId];
                }

                userListDto.Roles = userListDto.Roles.OrderBy(r => r.RoleName).ToList();
            }
        }

        public bool ConfirmUserRole(int user_id)
        {
            var usr = _userRoleRepository.FirstOrDefault(u => u.UserId == user_id);
            var role = _abproleRepository.FirstOrDefault(u => u.Id == usr.RoleId);

            if (role.Name.Equals("Admin"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<RoleListDto> GetUserRoles(string userName)
        {
            var query = (from u in _userManager.Users.Where(u => u.UserName == userName)
                         join ur in _userRoleRepository.GetAll() on u.Id equals ur.UserId
                         join r in _abproleRepository.GetAll() on ur.RoleId equals r.Id
                         select r
                       ).ToList();


            return ObjectMapper.Map<List<RoleListDto>>(query);

        }
        public async Task userRoleById(int id, string userNameOrEmailAddress)
        {
            var user = _userRepository.GetAll().Where(u => u.EmailAddress == userNameOrEmailAddress || u.UserName == userNameOrEmailAddress).FirstOrDefault();
            user.UserRole = id;
            await _userManager.UpdateAsync(user);
        }
    }
}
