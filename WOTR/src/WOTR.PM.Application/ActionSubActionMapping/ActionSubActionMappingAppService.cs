﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.ActionSubActionMapping.Dto;
using WOTR.PM.PrgActionAreaActivitysMappings;

namespace WOTR.PM.ActionSubActionMapping
{
    public class ActionSubActionMappingAppService : PMAppServiceBase, IActionSubActionMappingAppService
    {
        private readonly IRepository<ActionSAActivityMapping> _actionSunActionAreaMapAppRepository;
        private readonly IRepository<ActionArea> _actionAreaAppRepository;
        private readonly IRepository<SubActionArea> _subactionareaRepository;
        private readonly IRepository<Activity> _activityRepository;
        private readonly IRepository<PrgActionAreaActivityMapping> _PrgActionAreaActivityMappingRepository;


        public ActionSubActionMappingAppService(IRepository<ActionSAActivityMapping> actionSunActionAreaMapAppRepository, 
            IRepository<ActionArea> actionAreaAppRepository,
            IRepository<SubActionArea> subactionareaRepository,
            IRepository<Activity> activityRepository, IRepository<PrgActionAreaActivityMapping> PrgActionAreaActivityMappingRepository)
        {
            _actionSunActionAreaMapAppRepository = actionSunActionAreaMapAppRepository;
            _actionAreaAppRepository = actionAreaAppRepository;
            _subactionareaRepository = subactionareaRepository;
            _activityRepository = activityRepository;
            _PrgActionAreaActivityMappingRepository = PrgActionAreaActivityMappingRepository;
        }

        public List<string> CreatOrUpdateActionSubActionMappingArea(ActionSubActionAreaMappingListDto Input)
        {
            List<string> message = new List<string>();
            foreach (var item in Input.ActivityID)
            {
                ActionSAActivityMapping actionSubActionareas = new ActionSAActivityMapping();
                actionSubActionareas.ActionAreaID = Input.ActionAreaID;
                actionSubActionareas.SubActionAreaID = Input.SubActionAreaID;
                actionSubActionareas.TenantId = Convert.ToInt32(AbpSession.TenantId);
                actionSubActionareas.ActivityID = item; 

                if (Input.Id == 0)
                {
                    var queryCount = _actionSunActionAreaMapAppRepository.GetAll().Where(asa => asa.ActionAreaID == Input.ActionAreaID && asa.SubActionAreaID == Input.SubActionAreaID && asa.ActivityID == item).ToList();
                    if (queryCount.Count() == 0)
                    {
                        try
                        {
                            _actionSunActionAreaMapAppRepository.Insert(actionSubActionareas);
                            message.Add("ActionArea Added  sucessfully !");
                        }
                        catch (Exception)
                        {
                            throw new UserFriendlyException(L("Record NOt Add"));
                        }
                    }
                    else
                    {
                        message.Add("Already Mapped");
                    }
                }
                else
                {
                    try
                    {
                        _actionSunActionAreaMapAppRepository.Update(actionSubActionareas);
                        message.Add("ActionArea Update  sucessfully !");

                    }
                    catch (Exception)
                    {
                        throw new Abp.UI.UserFriendlyException(L("Record NOt Update"));
                    }

                }
            }
            return message;
        }

        public void DeleteActionSubactionMapping(EntityDto<int> Input)
        {

            //var QueryCheck = (from a in _activityRepository.GetAll() where (a.Id == Input.Id) select a.Id).ToList();
           // if (QueryCheck.Count == 0)
           // {
                _actionSunActionAreaMapAppRepository.Delete(Input.Id);

              //  return "Record Deleted ";
         //   }
         //   else
         //   {
              //  return "Record Exist";
          //  }
           
        }

        public List<ActionSubActionAreaMappingListDto> GetAllActionAreaRecord()
        {
            var query = (from aa in _actionSunActionAreaMapAppRepository.GetAll()
                         join ur in UserManager.Users.ToList()
                         on aa.CreatorUserId equals ur.Id                         
                         select new ActionSubActionAreaMappingListDto()
                         {
                             ActionAreaID = aa.ActionAreaID,
                             SubActionAreaID = aa.SubActionAreaID,
                             CreationTime = aa.CreationTime,
                             FirstName = ur.FullName,
                             ActionAreaName = aa.ActionArea.Name,
                             SubActionAreaName =aa.SubActionArea.Name,
                             ActivityName =aa.Activity.Name,
                             Id = aa.Id
                         }).ToList();
            return query;

        }

        public List<ActionSubActionAreaMappingListDto> GetAllActionAreaRecordsearch(string input)
        {
            var query = (from aa in _actionSunActionAreaMapAppRepository.GetAll()
                        
                         join ur in UserManager.Users.ToList()
                         on aa.CreatorUserId equals ur.Id
                         select new ActionSubActionAreaMappingListDto()
                         {
                             ActionAreaID = aa.ActionAreaID,
                             SubActionAreaID = aa.SubActionAreaID,
                             CreationTime = aa.CreationTime,
                             FirstName = ur.FullName,
                             ActionAreaName = aa.ActionArea.Name,
                             SubActionAreaName = aa.SubActionArea.Name,
                             ActivityName = aa.Activity.Name,
                             Id = aa.Id
                         }).ToList().WhereIf(
                 !input.IsNullOrEmpty(),
                 aa => aa.ActionAreaName.ToUpper().Contains(input.ToUpper()) ||
                aa.ActionAreaName.ToLower().Contains(input.ToLower()));
            return new List<ActionSubActionAreaMappingListDto>(ObjectMapper.Map<List<ActionSubActionAreaMappingListDto>>(query));

        }


    }
}
