﻿using System.Collections.Generic;
using WOTR.PM.Auditing.Dto;
using WOTR.PM.Dto;

namespace WOTR.PM.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);
    }
}
