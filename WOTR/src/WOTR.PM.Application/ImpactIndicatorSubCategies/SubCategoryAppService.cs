﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.UI;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.NewImpactIndicator;
using WOTR.PM.NewImpactIndicator.SubCategory;
using WOTR.PM.NewImpactIndicator.SubCategory.Dto;

namespace WOTR.PM.ImpactIndicatorSubCategies
{
    public class SubCategoryAppService : PMAppServiceBase, ISubCategoryAppService
    {
        private readonly IRepository<ImpactIndicatorSubCategory> _IISubCategoryRepository;
        private readonly IRepository<ImpactIndicatorCategory> _IICategoryRepository;

        public SubCategoryAppService(
            IRepository<ImpactIndicatorSubCategory> IISubCategory, IRepository<ImpactIndicatorCategory> IICategory)
        {
            _IISubCategoryRepository = IISubCategory;
            _IICategoryRepository = IICategory;
        }

        public string createEditImapactIndiactorSubCategory(ImpactIndicatorSubCategory Input)
        {
            string message;

            ImpactIndicatorSubCategory subcategory = new ImpactIndicatorSubCategory();
            subcategory.SubCategory = char.ToUpper(Input.SubCategory[0]) + Input.SubCategory.Substring(1);

            if (Input.Id == 0)
            {
                try
                {
                    var QueryCheck = _IISubCategoryRepository.GetAll().Where(aa => aa.SubCategory == Input.SubCategory).ToList();
                    if (QueryCheck.Count == 0)
                    {
                        _IISubCategoryRepository.Insert(Input);
                        message = "SubCategory Added  sucessfully !";
                    }
                    else { message = "Record Already Present!"; }
                }
                catch (Exception ex)
                {

                    throw new UserFriendlyException(("Record Not Added"));
                }
            }
            else
            {
                try
                {
                    subcategory.TenantId = Input.TenantId;
                    subcategory.Id = Input.Id;
                    subcategory.SubCategory = Input.SubCategory;
                    subcategory.ImpactIndicatorCategoryId = Input.ImpactIndicatorCategoryId;
                    _IISubCategoryRepository.Update(subcategory);
                    message = "SubCategory Update  sucessfully !";

                }
                catch (Exception)
                {

                    throw new UserFriendlyException("Record Not Added");
                }
            }
            return message;
        }


        public List<GetAllSubCategory> GetAllImapactIndiactorSubCategory(string filter)
        {
            var SubCategoryList = (from sc in _IISubCategoryRepository.GetAll().Where(x => string.IsNullOrEmpty(filter) || x.SubCategory.Contains(filter))
                                   join c in _IICategoryRepository.GetAll() on sc.ImpactIndicatorCategoryId equals c.Id
                                   select new GetAllSubCategory
                                   {
                                       Id = sc.Id,
                                       SubCategory = sc.SubCategory,
                                       ImpactIndicatorCategoryId = (int)sc.ImpactIndicatorCategoryId,
                                       Category = c.Category
                                   }
                                    ).OrderBy(x=> x.SubCategory).ThenBy(x=> x.Category).ToList();

            return SubCategoryList;
        }

        public List<GetAllSubCategory> GetAllImapactIndiactorSubCategoryByCategoryId(int CategoryId)
        {
            List<GetAllSubCategory> SubCategoryListreturn = new List<GetAllSubCategory>();

            var SubCategoryList = _IISubCategoryRepository.GetAll().Where(x => x.ImpactIndicatorCategoryId == CategoryId).ToList();
            foreach (var p in SubCategoryList)
            {
                GetAllSubCategory getAllSubCategory = new GetAllSubCategory();
                getAllSubCategory.Id = p.Id;
                getAllSubCategory.SubCategory = p.SubCategory;
                getAllSubCategory.ImpactIndicatorCategoryId = (int)p.ImpactIndicatorCategoryId;
                SubCategoryListreturn.Add(getAllSubCategory);
            }
            return SubCategoryListreturn;
        }

        public async Task Delete(EntityDto input)
        {
            try
            {
                await _IISubCategoryRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
    }
}
