﻿using Microsoft.TeamFoundation.WorkItemTracking.WebApi;
using Microsoft.TeamFoundation.WorkItemTracking.WebApi.Models;
using Microsoft.VisualStudio.Services.Common;
using Microsoft.VisualStudio.Services.WebApi.Patch.Json;
using Microsoft.VisualStudio.Services.WebApi.Patch;
using Microsoft.VisualStudio.Services.WebApi;
using Microsoft.TeamFoundation.Build.WebApi;
using System.Threading.Tasks;
using WOTR.PM.Feedback.Dto;
using System;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Linq;
using Abp.UI;
using Abp.Collections.Extensions;
using Abp.Extensions;
using WOTR.PM.Common.Dto;

namespace WOTR.PM.Feedback
{
    public class FeedbackAppService : PMAppServiceBase
    {

        private string PersonalAccessToken = "";
        private string ProjectName = "";
        private string DevopsUrl = "";
        public FeedbackAppService()
        {
            DevopsUrl = "https://dev.azure.com/WOTR";
            ProjectName = "WOTR.pm";
            PersonalAccessToken = "dwfm6euplefx3v3hr3dm6oou3eq4sqpu2gwzzr7ihy4fsm2xumua";
        }

        public async Task<int> CreateFeedbackInAzureDevops(FeedbackDto feedBackInput)
        {
            Uri uri = new Uri(DevopsUrl);


            // var output = await QueryOpenBugs(project);

            VssBasicCredential credentials = new VssBasicCredential("", PersonalAccessToken);
            JsonPatchDocument patchDocument = new JsonPatchDocument();

            //add fields and their values to your patch document
            patchDocument.Add(
                new JsonPatchOperation()
                {
                    Operation = Operation.Add,
                    Path = "/fields/System.Title",
                    Value = feedBackInput.Title
                }
            );



            if (feedBackInput.FeedbackType.ToLower() == "bug")
            {
                patchDocument.Add(
                    new JsonPatchOperation()
                    {
                        Operation = Operation.Add,
                        Path = "/fields/Microsoft.VSTS.TCM.ReproSteps",
                        Value = feedBackInput.Description
                    }
                );
            }
            else if (feedBackInput.FeedbackType.ToLower() == "issue")
            {
                patchDocument.Add(
                    new JsonPatchOperation()
                    {
                        Operation = Operation.Add,
                        // Path = "/fields/Microsoft.VSTS.TCM.ReproSteps",
                        Path = "/fields/System.Description",
                        Value = feedBackInput.Description
                    }
                );
            }
            else
            {
                patchDocument.Add(
                  new JsonPatchOperation()
                  {
                      Operation = Operation.Add,
                      Path = "/fields/System.Description",
                      Value = feedBackInput.Description
                  }
              );
            }

            patchDocument.Add(
                  new JsonPatchOperation()
                  {
                      Operation = Operation.Add,
                      Path = "/fields/System.Tags",
                      Value = "ClientFeedback"
                  }
              );

            VssConnection connection = new VssConnection(uri, credentials);
            WorkItemTrackingHttpClient workItemTrackingHttpClient = connection.GetClient<WorkItemTrackingHttpClient>();




            try
            {

                WorkItem result = workItemTrackingHttpClient.CreateWorkItemAsync(patchDocument, ProjectName, feedBackInput.FeedbackType).Result;

                Console.WriteLine("Bug Successfully Created: Bug #{0}", result.Id);

                return 1;
            }
            catch (AggregateException ex)
            {
                Console.WriteLine("Error creating bug: {0}", ex.InnerException.Message);
                return 0;
            }
        }


        public async Task<PagedResultDto<WorkItemListDto>> GetAllFeedbackListFromDevops(InputFeedbackForm input)
        {
            try
            {
                Uri uri = new Uri(DevopsUrl);
                var credentials = new VssBasicCredential(string.Empty, PersonalAccessToken);

                List<WorkItemListDto> feedbackList = new List<WorkItemListDto>();
                // create a wiql object and build our query
                var wiql = new Wiql()
                {
                    // NOTE: Even if other columns are specified, only the ID & URL will be available in the WorkItemReference
                    Query = "Select [Id] " +
                            "From WorkItems " +
                            "Where ([Work Item Type] = 'Bug' Or [Work Item Type] = 'Feature' or [Work Item Type] = 'Issue')   " +
                            "And [System.Tags] Contains  'ClientFeedback' " +
                            "And [System.TeamProject] = '" + ProjectName + "' " +
                            "And [System.State] <> 'Closed' " +
                            "Order By [State] Asc, [Changed Date] Desc",
                };

                // create instance of work item tracking http client
                using (var httpClient = new WorkItemTrackingHttpClient(uri, credentials))
                {
                    // execute the query to get the list of work items in the results
                    var result = await httpClient.QueryByWiqlAsync(wiql).ConfigureAwait(false);
                    var ids = result.WorkItems.Select(item => item.Id).ToArray();

                    // some error handling
                    if (ids.Length == 0)
                    {
                        return null;
                    }

                    // build a list of the fields we want to see
                    var fields = new[] { "System.Id", "System.Title", "System.State", "System.WorkItemType", "System.Description", "Microsoft.VSTS.TCM.ReproSteps", "System.ChangedDate" };

                    // get work items for the ids found in query
                    List<WorkItem> data = await httpClient.GetWorkItemsAsync(ids, fields).ConfigureAwait(false);
                    foreach (var item in data)
                    {
                        WorkItemListDto feedbackDto = new WorkItemListDto();
                        foreach (var field in item.Fields)
                        {

                            switch (field.Key.ToLower())
                            {
                                case "system.id":
                                    feedbackDto.Id = field.Value.ToString();
                                    break;
                                case "system.title":
                                    feedbackDto.Title = field.Value.ToString();
                                    break;
                                case "system.workitemtype":
                                    feedbackDto.Type = field.Value.ToString();
                                    break;
                                case "system.state":
                                    feedbackDto.Status = field.Value.ToString();
                                    break;
                                case "system.description":
                                    feedbackDto.Description = field.Value.ToString();
                                    break;
                                case "microsoft.vsts.tcm.reprosteps":
                                    feedbackDto.ReproDescription = field.Value.ToString();
                                    break;
                                case "system.changeddate":
                                    feedbackDto.CreatedDate = field.Value.ToString();
                                    break;

                            }
                        }
                        feedbackList.Add(feedbackDto);
                    }

                    var query = feedbackList.WhereIf(!input.filter.IsNullOrWhiteSpace(), c => false || c.Id.Contains(input.filter) || c.Title.ToLower().Contains(input.filter.ToLower()))
                        .WhereIf(input.PriorityIds != null, t => input.PriorityIds.Select(x => x.ItemName).Contains(t.Status)).ToList();

                    var demo = new List<WorkItemListDto>();


                    if (input.SortNo == 1)
                    {
                        demo = query.OrderBy(x => x.Title).ToList();
                    }
                    else
                    {
                        demo = query.OrderByDescending(x => x.Title).ToList();
                    }

                    return new PagedResultDto<WorkItemListDto>
                    {
                        Items = demo,
                        TotalCount = demo.Count
                    };
                }

            }
            catch (Exception ex)
            {

                throw new UserFriendlyException("FeedbackAppService-GetAllFeedbackListFromDevops" + ex.Message.ToString());
            }
        }

        public async Task<List<WorkItemListDto>> GetFeedbackId(string id)
        {
            try
            {
                Uri uri = new Uri(DevopsUrl);
                var credentials = new VssBasicCredential(string.Empty, PersonalAccessToken);
                List<WorkItemListDto> feedbackList = new List<WorkItemListDto>();


                // create a wiql object and build our query
                var wiql = new Wiql()
                {
                    // NOTE: Even if other columns are specified, only the ID & URL will be available in the WorkItemReference
                    Query = "Select [Id] " +
                            "From WorkItems " +
                            "Where ([Work Item Type] = 'Bug' Or [Work Item Type] = 'Feature' or [Work Item Type] = 'Issue')   " +
                            "And [System.Tags] Contains  'ClientFeedback' " +
                            "And [System.TeamProject] = '" + ProjectName + "' " +
                            "And [System.State] <> 'Closed' " +
                            "Order By [State] Asc, [Changed Date] Desc",
                };

                // create instance of work item tracking http client
                using (var httpClient = new WorkItemTrackingHttpClient(uri, credentials))
                {
                    // execute the query to get the list of work items in the results
                    var result = await httpClient.QueryByWiqlAsync(wiql).ConfigureAwait(false);
                    var ids = result.WorkItems.Select(item => item.Id).ToArray();

                    // some error handling
                    if (ids.Length == 0)
                    {
                        return null;
                    }

                    // build a list of the fields we want to see
                    var fields = new[] { "System.Id", "System.Title", "System.State", "System.WorkItemType", "System.Description", "Microsoft.VSTS.TCM.ReproSteps" };

                    // get work items for the ids found in query
                    List<WorkItem> data = await httpClient.GetWorkItemsAsync(ids, fields).ConfigureAwait(false);
                    foreach (var item in data)
                    {
                        WorkItemListDto feedbackDto = new WorkItemListDto();
                        foreach (var field in item.Fields)
                        {

                            switch (field.Key.ToLower())
                            {
                                case "system.id":
                                    feedbackDto.Id = field.Value.ToString();
                                    break;
                                case "system.title":
                                    feedbackDto.Title = field.Value.ToString();
                                    break;
                                case "system.workitemtype":
                                    feedbackDto.Type = field.Value.ToString();
                                    break;
                                case "system.state":
                                    feedbackDto.Status = field.Value.ToString();
                                    break;
                                case "system.description":
                                    feedbackDto.Description = field.Value.ToString();
                                    break;
                                case "microsoft.vsts.tcm.reprosteps":
                                    feedbackDto.ReproDescription = field.Value.ToString();
                                    break;
                            }
                        }
                        feedbackList.Add(feedbackDto);
                        //var abc = from fe in feedbackList.Where(c => c.Id == id).Select(k => new WorkItemListDto() { Id = k.Id, Title = k.Title, Status = k.Status, Type = k.Type }).ToList();


                    }

                    var singledata = feedbackList.Where(c => c.Id == id).Select(k => new WorkItemListDto() { Id = k.Id, Title = k.Title, Status = k.Status, Type = k.Type, Description = k.Description, ReproDescription = k.ReproDescription }).ToList();
                    //var abc = from fe in feedbackList.AsEnumerable().Where(c => c.Id == id)
                    //          select new WorkItemListDto { Id = fe.Id, Title = fe.Title, Status = fe.Status, Type = fe.Type };

                    return singledata;
                }


            }
            catch (Exception ex)
            {

                throw new UserFriendlyException("FeedbackAppService-GetFeedbackId" + ex.Message.ToString());
            }
        }

    }
}
