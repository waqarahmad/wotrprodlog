﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using WOTR.PM.NewImpactIndicator;
using WOTR.PM.NewImpactIndicator.IIForms.Dto;
using WOTR.PM.NewImpactIndicator.Questionaries;
using WOTR.PM.NewImpactIndicator.Questionaries.Dto;

namespace WOTR.PM.Questionaries
{
    public class QuestionaryAppService : PMAppServiceBase, IQuestionaryAppService
    {

        private readonly IRepository<IIQuestionary> _questionaryRepository;
        private readonly IRepository<ImpactIndicatorCategory> _impactIndicatorCategory;
        private readonly IRepository<ImpactIndicatorSubCategory> _impactIndicatorSubCategory;
        private readonly IRepository<FrequencyOfoccurrence> _frequencyOfoccurrence;

        public QuestionaryAppService(
            IRepository<IIQuestionary> questionaryRepository,
            IRepository<ImpactIndicatorCategory> impactIndicatorCategory,
            IRepository<ImpactIndicatorSubCategory> impactIndicatorSubCategory,
            IRepository<FrequencyOfoccurrence> frequencyOfoccurrence
            )
        {
            _questionaryRepository = questionaryRepository;
            _impactIndicatorCategory = impactIndicatorCategory;
            _impactIndicatorSubCategory = impactIndicatorSubCategory;
            _frequencyOfoccurrence = frequencyOfoccurrence;
        }


        public List<GetAllQuestionary> GetAllQuestionary(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                var Questionary = (from o in _questionaryRepository.GetAll()
                                   join c in _impactIndicatorCategory.GetAll() on o.ImpactIndicatorCategoryId equals c.Id
                                   join sc in _impactIndicatorSubCategory.GetAll() on o.ImpactIndicatorSubCategoryId equals sc.Id
                                   join fo in _frequencyOfoccurrence.GetAll() on o.FrequencyOfOccurrenceId equals fo.Id
                                   select new GetAllQuestionary
                                   {
                                       Question = o.Question,
                                       Description = o.Description,
                                       QuestionStatus = o.QuestionStatus,
                                       QuestionType = o.QuestionType,
                                       frequencyOfOccurrenceId = (int)o.FrequencyOfOccurrenceId,
                                       impactIndicatorCategoryId = (int)o.ImpactIndicatorCategoryId,
                                       impactIndicatorSubCategoryId = (int)o.ImpactIndicatorSubCategoryId,
                                       impactIndicatorCategoryName = c.Category,
                                       impactIndicatorSubCategoryName = sc.SubCategory,
                                       frequencyOfOccurrence = fo.FreqOfOccurrence,
                                       CreationTime = o.CreationTime,
                                       Id = o.Id
                                   }).OrderByDescending(x=> x.CreationTime).ToList();
                return Questionary;
            }
            else
            {
                var Questionary = (from o in _questionaryRepository.GetAll().Where(x => x.Question.ToLower().Contains(input.Trim().ToLower()))
                                   join c in _impactIndicatorCategory.GetAll() on o.ImpactIndicatorCategoryId equals c.Id
                                   join sc in _impactIndicatorSubCategory.GetAll() on o.ImpactIndicatorSubCategoryId equals sc.Id
                                   join fo in _frequencyOfoccurrence.GetAll() on o.FrequencyOfOccurrenceId equals fo.Id
                                   select new GetAllQuestionary
                                   {
                                       Question = o.Question,
                                       Description = o.Description,
                                       QuestionStatus = o.QuestionStatus,
                                       QuestionType = o.QuestionType,
                                       frequencyOfOccurrenceId = (int)o.FrequencyOfOccurrenceId,
                                       impactIndicatorCategoryId = (int)o.ImpactIndicatorCategoryId,
                                       impactIndicatorSubCategoryId = (int)o.ImpactIndicatorSubCategoryId,
                                       impactIndicatorCategoryName = c.Category,
                                       impactIndicatorSubCategoryName = sc.SubCategory,
                                       frequencyOfOccurrence = fo.FreqOfOccurrence,
                                       CreationTime = o.CreationTime,
                                       Id = o.Id
                                   }).OrderByDescending(x => x.CreationTime).ToList();
                return Questionary;
            }
        }

        public string CreateOrEdit(AddEditQuestionaryDto input)
        {
            if (input.Id == null || input.Id == 0)
            {
                Create(input);
                return "Questionnaire Added Sucessfully";
            }
            else
            {
                Update(input);

                return "Questionnaire Updated Sucessfully";
            }
        }

        private void Create(AddEditQuestionaryDto input)
        {
            try
            {
                var IIQuestion = new IIQuestionary();
                IIQuestion.Question = input.Question;
                IIQuestion.Description = input.Description;
                IIQuestion.QuestionStatus = input.QuestionStatus;
                IIQuestion.QuestionType = input.QuestionType;
                IIQuestion.FrequencyOfOccurrenceId = input.frequencyOfOccurrenceId;
                IIQuestion.ImpactIndicatorCategoryId = input.impactIndicatorCategoryId;
                IIQuestion.ImpactIndicatorSubCategoryId = input.impactIndicatorSubCategoryId;
                _questionaryRepository.InsertAsync(IIQuestion);
                if (input.frequencyOfOccurrenceId == 4)
                {
                    input.frequencyOfOccurrenceId = 5;
                    Create(input);
                }
            }
            catch (Exception ex) { }
        }

        private void Update(AddEditQuestionaryDto input)
        {
            try
            {
                var IIQuestion = new IIQuestionary();
                IIQuestion.Id = input.Id;
                IIQuestion.Question = input.Question;
                IIQuestion.Description = input.Description;
                IIQuestion.QuestionStatus = input.QuestionStatus;
                IIQuestion.QuestionType = input.QuestionType;
                IIQuestion.FrequencyOfOccurrenceId = input.frequencyOfOccurrenceId;
                IIQuestion.ImpactIndicatorCategoryId = input.impactIndicatorCategoryId;
                IIQuestion.ImpactIndicatorSubCategoryId = input.impactIndicatorSubCategoryId;
                _questionaryRepository.Update(IIQuestion);
                if (input.frequencyOfOccurrenceId == 4)
                {
                    input.frequencyOfOccurrenceId = 5;
                    Update(input);
                }
            }
            catch (Exception ex)
            {
            }

        }

        public async Task Delete(EntityDto input)
        {
            await _questionaryRepository.DeleteAsync(input.Id);
            //var FreOfOcc = _questionaryRepository.FirstOrDefault(x => x.Id == input.Id);
            //if (FreOfOcc.FrequencyOfOccurrenceId == 4)
            //{

            //    await _questionaryRepository.DeleteAsync(x=> x.Id == input.Id );
            //}
        }

        public List<dualListDto> FetchAllQuetionByCateAndSubCat(GetAllQuestionByIdsInputDto input)
        {
            var dualList = new List<dualListDto>();
            var query = new List<IIQuestionary>();

            if (input.CategoryId == 0 && input.SubCategoryId == 0)
            {
                query = _questionaryRepository.GetAll()
                   .Where(x => x.QuestionStatus == "Active").ToList();
            }
            else if (input.CategoryId > 0 && input.SubCategoryId == 0)
            {
                query = _questionaryRepository.GetAll()
          .Where(x => x.QuestionStatus == "Active" && x.ImpactIndicatorCategoryId == input.CategoryId).ToList();
            }
            else
            {
                query = _questionaryRepository.GetAll()
          .Where(x => x.QuestionStatus == "Active" && x.ImpactIndicatorCategoryId == input.CategoryId && x.ImpactIndicatorSubCategoryId == input.SubCategoryId)
          .ToList();
            }


            foreach (var n in query)
            {
                dualListDto dualListObj = new dualListDto();
                dualListObj.id = n.Id;
                dualListObj.name = n.Question;
                dualList.Add(dualListObj);
            }




            return dualList;
        }

        public List<dualListDto> FetchAllQuetionByQuestionId(List<string> input)
        {
            var dualList = new List<dualListDto>();
            var query = new List<IIQuestionary>();

            if (input.Count > 0)
            {
                query = _questionaryRepository.GetAll()
          .Where(x => x.QuestionStatus == "Active" && input.Any(y => y == x.Id.ToString()))

          .ToList();
            }


            foreach (var n in query)
            {
                dualListDto dualListObj = new dualListDto();
                dualListObj.id = n.Id;
                dualListObj.name = n.Question;
                dualList.Add(dualListObj);
            }

            return dualList;

        }
    }
}
