﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.NewImpactIndicator;
using WOTR.PM.NewImpactIndicator.FreqOfOccurences;
using WOTR.PM.NewImpactIndicator.FreqOfOccurences.Dto;

namespace WOTR.PM.FreqOfOccurences
{
    public class FrequencyOfOccurencesAppService : PMAppServiceBase, IFrequencyOfOccurencesAppService
    {
        private readonly IRepository<FrequencyOfoccurrence> _FrequencyOfoccurrenceRepository;

        public FrequencyOfOccurencesAppService(IRepository<FrequencyOfoccurrence> frequencyOfoccurrenceRepository)
        {
            _FrequencyOfoccurrenceRepository = frequencyOfoccurrenceRepository;
        }

        public List<GetAllFreqOfOccurrence> GetAllFreqOfOccurrence(string filter)
        {
            var FreqOfOccurrenceListreturn = new List<GetAllFreqOfOccurrence>();
            if (filter == null)
            {
                var FreqOfOccurrenceList = _FrequencyOfoccurrenceRepository.GetAll().ToList();
                FreqOfOccurrenceListreturn = ObjectMapper.Map<List<GetAllFreqOfOccurrence>>(FreqOfOccurrenceList);

            }
            else
            {
                var FreqOfOccurrenceList = _FrequencyOfoccurrenceRepository.GetAll().ToList().Where(x => x.FreqOfOccurrence.ToLower().Contains(filter.Trim().ToLower())); ;
                FreqOfOccurrenceListreturn = ObjectMapper.Map<List<GetAllFreqOfOccurrence>>(FreqOfOccurrenceList);
            }
            return FreqOfOccurrenceListreturn;
        }

        public string CreatOrUpdateFreqOfOccurrence(FrequencyOfoccurrence Input)
        {

            string message;

            FrequencyOfoccurrence frequencyOfoccurrence = new FrequencyOfoccurrence();
            frequencyOfoccurrence.FreqOfOccurrence = char.ToUpper(Input.FreqOfOccurrence[0]) + Input.FreqOfOccurrence.Substring(1);

            if (Input.Id == 0)
            {
                try
                {
                    var QueryCheck = _FrequencyOfoccurrenceRepository.GetAll().Where(aa => aa.FreqOfOccurrence == Input.FreqOfOccurrence).ToList();
                    if (QueryCheck.Count == 0)
                    {
                        _FrequencyOfoccurrenceRepository.Insert(frequencyOfoccurrence);
                        message = "Frequncy Of Occurrence Added  sucessfully !";
                    }

                    else { message = "Record alread Present!"; }
                }
                catch (Exception ex)
                {

                    throw new UserFriendlyException(L("Record NOt Add"));
                }

            }
            else
            {
                try
                {
                    frequencyOfoccurrence.TenantId = Input.TenantId;
                    frequencyOfoccurrence.CreatorUserId = Input.CreatorUserId;
                    frequencyOfoccurrence.Id = Input.Id;
                    frequencyOfoccurrence.FreqOfOccurrence = Input.FreqOfOccurrence;
                    _FrequencyOfoccurrenceRepository.Update(frequencyOfoccurrence);
                    message = "Frequncy Of Occurrence Update  sucessfully !";

                }
                catch (Exception)
                {

                    throw new UserFriendlyException(L("Record NOt Update"));
                }
            }

            return message;
        }

        public async Task Delete(EntityDto input)
        {
            try
            {
                await _FrequencyOfoccurrenceRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
    }
}
