using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using WOTR.PM.ChekLists.Dtos;
using Abp.Application.Services.Dto;
using WOTR.PM.Authorization;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using WOTR.PM.Checklists;
using WOTR.PM.ActionAreas;
using WOTR.PM.PrgImplementationsPlans;
using Abp.UI;

namespace WOTR.PM.ChekLists
{
    //[AbpAuthorize(AppPermissions.Pages_ChekLists)]
    public class ChekListsAppService : PMAppServiceBase, IChekListsAppService
    {
        private readonly IRepository<Checklist> _chekListRepository;
        private readonly IRepository<ChecklistItem> _ChecklistItemRepository;
        private readonly IRepository<Activity> _ActivityRepository;
        private readonly IRepository<SubActivityList> _SubActivityListRepository;
        private readonly IRepository<Questionarie> _QuestionarieRepository;
        private readonly IRepository<SubActivityQuestionaryMapping> _SubActivityQuestionaryMappingRepository;
        private readonly IRepository<ImplementationPlanCheckList> _ImplementationPlanCheckListRepository;

        public ChekListsAppService(IRepository<Checklist> chekListRepository, IRepository<SubActivityList> SubActivityListRepository, IRepository<SubActivityQuestionaryMapping> SubActivityQuestionaryMappingRepository
            , IRepository<Questionarie> QuestionarieRepository, IRepository<ChecklistItem> ChecklistItemRepository, IRepository<Activity> ActivityRepository, IRepository<ImplementationPlanCheckList> ImplementationPlanCheckListRepository)
        {
            _chekListRepository = chekListRepository;
            _ChecklistItemRepository = ChecklistItemRepository;
            _ActivityRepository = ActivityRepository;
            _SubActivityListRepository = SubActivityListRepository;
            _QuestionarieRepository = QuestionarieRepository;
            _SubActivityQuestionaryMappingRepository = SubActivityQuestionaryMappingRepository;
            _ImplementationPlanCheckListRepository = ImplementationPlanCheckListRepository;

        }
        public async Task<List<CreateOrEditChekListDto>> GetAll(string input)
        {
            //var result = new List<CreateOrEditChekListDto>();


            var query = (from c in _chekListRepository.GetAll().Include(c => c.Activity).Include(c=>c.CheckListItems)
                         select new CreateOrEditChekListDto()
                         {
                             ChecklistName = c.ChecklistName,
                             Description = c.Description,
                             Id = c.Id,
                             ActivityName = c.Activity.Name,
                             ActivityID = c.ActivityID,
                             iteam = c.CheckListItems == null ? new List<cheklistandSublistDto>() : c.CheckListItems.Select(t=>new cheklistandSublistDto() {
                                 CheklisiteamtName = t.Name,
                                 checklistID_subiteamTable = t.ChecklistID,
                                 Id = t.Id
                             }).ToList(),
                         }).WhereIf(!string.IsNullOrWhiteSpace(input), e => e.ChecklistName.ToLower().Contains(input.ToLower()));

            var result = query.ToList();

            return result;

            //var checkList = await _chekListRepository.GetAll().Include(c => c.Activity).ToListAsync();
            //var checkListItem = await _ChecklistItemRepository.GetAll().ToListAsync();

            //var query = (from y in checkList
            //             select new CreateOrEditChekListDto
            //             {
            //                 ChecklistName = y.ChecklistName,
            //                 Description = y.Description,
            //                 Id = y.Id,
            //                 ActivityID = y.ActivityID,
            //                 ActivityName = y.Activity == null ? string.Empty : y.Activity.Name,
            //                 iteam = (from m in checkList
            //                          join i in checkListItem on m.Id equals i.ChecklistID
            //                          select new cheklistandSublistDto()
            //                          {
            //                              CheklisiteamtName = i.Name,
            //                              checklistID_subiteamTable = i.ChecklistID,
            //                              Id = i.Id
            //                          }).ToList(),
            //             }).ToList().OrderByDescending(c => c.ChecklistName);


            //if (query.Any())
            //{
            //    var q = query.ToList().GroupBy(x => x.Id).Select(g => g.First()).ToList();

            //    foreach (var item in q)
            //    {
            //        var sublist = new List<cheklistandSublistDto>();
            //        if (item.Id != 0)
            //        {
            //            foreach (var sub in item.iteam)
            //            {
            //                if (item.Id == sub.checklistID_subiteamTable)
            //                {
            //                    sublist.Add(new cheklistandSublistDto
            //                    {
            //                        checklistID_subiteamTable = sub.checklistID_subiteamTable,
            //                        CheklisiteamtName = sub.CheklisiteamtName,
            //                        Id = sub.Id,
            //                    });
            //                }
            //            }
            //        }

            //        result.Add(new CreateOrEditChekListDto
            //        {
            //            ChecklistName = item.ChecklistName,
            //            Description = item.Description,
            //            Id = item.Id,
            //            iteam = sublist,
            //            ActivityName = item.ActivityName,
            //            ActivityID = item.ActivityID
            //        });
            //    }
            //}
        }

        // [AbpAuthorize(AppPermissions.Pages_ChekLists_Edit)]
        public async Task<CreateOrEditChekListDto> GetChekListForEdit(EntityDto<int> input)
        {
            var checkList = await _chekListRepository.FirstOrDefaultAsync(input.Id);
            return ObjectMapper.Map<CreateOrEditChekListDto>(checkList);
        }

        public async Task CreateOrEdit(CreateOrEditChekListDto input)
        {
            if (input.Id == null || input.Id == 0)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        // [AbpAuthorize(AppPermissions.Pages_ChekLists_Create)]

        private async Task Create(CreateOrEditChekListDto input)
        {
            var abc = await _chekListRepository.GetAll().AnyAsync(x => x.ChecklistName == input.ChecklistName);

            if (abc)
            {
                throw new UserFriendlyException("Checklist Name  Allready Exits");
            }

            int n = 0;
            var checkList = ObjectMapper.Map<Checklist>(input);

            if (AbpSession.TenantId != null)
            {
                checkList.TenantId = (int)AbpSession.TenantId;
            }

            n = await _chekListRepository.InsertAndGetIdAsync(checkList);

            if (n > 0)
            {
                CreateOreditCheklistandsubiteam(input.iteam, n);
            }

        }

        // [AbpAuthorize(AppPermissions.Pages_ChekLists_Edit)]
        private async Task Update(CreateOrEditChekListDto input)
        {
            input.TenantId = Convert.ToInt16(AbpSession.TenantId);
            var checkList = await _chekListRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, checkList);
            CreateOreditCheklistandsubiteam(input.iteam, checkList.Id);
        }

        // [AbpAuthorize(AppPermissions.Pages_ChekLists_Delete)]
        public string Delete(EntityDto<int> input)
        {
            var QueryCheck = (from a in _ImplementationPlanCheckListRepository.GetAll() where (a.ChecklistItemID == input.Id) select a.Id).ToList();

            if (QueryCheck.Count == 0)
            {
                _chekListRepository.DeleteAsync(input.Id);

                return "Record Deleted ";
            }
            else
            {
                return "Record Exist";
            }
        }

        public async Task CreateOrEditCheckListIteam(ChecklistIteamDto input)
        {
            ChecklistItem ChecklistItem = new ChecklistItem();

            ChecklistItem.Name = input.Name;
            ChecklistItem.IsChecked = input.IsChecked;
            ChecklistItem.ChecklistID = input.ChecklistID;
            if (input.ChecklistID != 0)
            {
                if (input.Id == 0)
                {
                    try
                    {
                        if (AbpSession.TenantId != null)
                        {
                            input.TenantId = (int)AbpSession.TenantId;
                        }
                        await _ChecklistItemRepository.InsertAsync(ChecklistItem);
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }

                }
                else
                {
                    var result = _ChecklistItemRepository.FirstOrDefault(a => a.Id == input.Id);
                    result.Name = input.Name;
                    _ChecklistItemRepository.Update(result);
                }
            }
        }

        public async Task Deleteiteam(EntityDto<int> input)
        {
            await _ChecklistItemRepository.DeleteAsync(input.Id);
        }

        public void CreateOreditCheklistandsubiteam(List<cheklistandSublistDto> input, int n)
        {
            foreach (var item in input)
            {
                if (item.Id == 0)
                {
                    int? SubActivityId = 0;
                    var query = _SubActivityListRepository.GetAll().Where(sub => sub.Name == item.CheklisiteamtName)
                        .ToList();
                    if (!query.Any())
                    {
                        var SubActivity = new SubActivityList();
                        SubActivity.Name = item.CheklisiteamtName;
                        SubActivityId = _SubActivityListRepository.InsertAndGetId(SubActivity);
                    }
                    else
                    {
                        foreach (var item1 in query)
                        {
                            SubActivityId = item1.Id;
                        }

                    }

                    var iteam = new ChecklistItem();
                    iteam.Name = item.CheklisiteamtName;
                    iteam.ChecklistID = n;
                    iteam.SubActivityId = SubActivityId;
                    _ChecklistItemRepository.Insert(iteam);
                }
            }

        }


        public void AddQueforSubActivity(int activityId)
        {
            var query = _ActivityRepository.GetAll().Where(a => a.Id == activityId).Select(a => a.Name);
            foreach (var item in query)
            {
                item.Contains("a");
                item.Distinct();
                item.ToLowerInvariant();
            }
        }

        public List<SubActivityList> GetAllSubActivity()
        {
            var query = _SubActivityListRepository.GetAll();
            return query.OrderByDescending(xx => xx.Name).ToList();
        }
        public List<Questionarie> GetAllQuestionarie()
        {
            var query = _QuestionarieRepository.GetAll().ToList();
            return query;
        }

        public void CreateOrUpdateQuestionary(QuestListDto input)
        {
            var query = _QuestionarieRepository.GetAll().Where(s => s.Name == input.Name && s.InputType == input.InputType);
            if (query.Count() == 0)
            {
                Questionarie Que = new Questionarie();
                Que.Name = input.Name;
                Que.InputType = input.InputType;
                _QuestionarieRepository.Insert(Que);
            }

        }

        public void AddSubactQuesMap(SubActivityQuesMapDto input)
        {
            foreach (var item in input.QuestionaryIds)
            {
                var query = _SubActivityQuestionaryMappingRepository.GetAll().Where(s => s.SubActivityId == input.SubActivityId && s.QuestionarieID == item);
                if (query.Count() == 0)
                {
                    SubActivityQuestionaryMapping sub = new SubActivityQuestionaryMapping();
                    sub.SubActivityId = input.SubActivityId;
                    sub.QuestionarieID = item;
                    _SubActivityQuestionaryMappingRepository.Insert(sub);
                }
            }


        }

        public List<cheklistandSublistDto> GetAllSubActivityWithQuestions()
        {
            var query = (from subq in _SubActivityQuestionaryMappingRepository.GetAll()
                         group subq by new
                         {
                             subq.SubActivityList

                         } into gcs
                         select new cheklistandSublistDto()
                         {
                             CheklisiteamtName = gcs.Key.SubActivityList.Name,
                             Id = gcs.Key.SubActivityList.Id,
                             Ques = (from subq1 in _SubActivityQuestionaryMappingRepository.GetAll().Where(q => q.SubActivityId == gcs.Key.SubActivityList.Id)
                                     join q in _QuestionarieRepository.GetAll()
                                     on subq1.QuestionarieID equals q.Id
                                     select new QuestListDto()
                                     {
                                         Name = q.Name,
                                         InputType = q.InputType,
                                         Id = q.Id,
                                         questionmappingid = subq1.Id
                                     }).ToList(),

                         }).ToList();
            return query;

        }

        public async Task deletequestion(EntityDto<int> input)
        {
            var query = _SubActivityQuestionaryMappingRepository.GetAll().Where(i => i.SubActivityId == input.Id).ToList();
            foreach (var item in query)
            {
                _SubActivityQuestionaryMappingRepository.Delete(item.Id);
            }

        }

        public async Task deletequestion1(EntityDto<int> input)
        {
            await _SubActivityQuestionaryMappingRepository.DeleteAsync(input.Id);
        }
    }
}