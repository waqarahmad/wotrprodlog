﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Checklists;
using WOTR.PM.ChekLists.Dtos;
using WOTR.PM.Dto;
using WOTR.PM.LoginAndroid;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.PrgImplementationPlans;
using WOTR.PM.PrgImplementationPlans.Dto;
using WOTR.PM.PrgImplementationsPlans;
using WOTR.PM.PrgQuarterUnits;
using WOTR.PM.PrgRequestAAPlas;
using WOTR.PM.PrgRequestAAPlas.Dto;
using WOTR.PM.ProgramCostEstimations;
using WOTR.PM.ProgramExpenses;
using WOTR.PM.Programs;
using WOTR.PM.UnitOfMeasures;

using System.Drawing;
using ShapeProperties = DocumentFormat.OpenXml.Drawing.Spreadsheet.ShapeProperties;
using Color = DocumentFormat.OpenXml.Spreadsheet.Color;
using System.IO;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using Font = DocumentFormat.OpenXml.Spreadsheet.Font;
using Microsoft.EntityFrameworkCore;



namespace WOTR.PM.ChekLists
{
    public class ImplimantionChecklistAppService : PMAppServiceBase, IImplimantionChecklistAppService
    {
        private readonly IRepository<ImplementationPlanCheckList> _ImplementationPlanCheckListRepository;
        private readonly IRepository<Role> _abproleRepository;
        private readonly IRepository<RequestAAplas> _RequestAAplasRepository;
        private readonly IRepository<Program> _ProgrameRepository;
        private readonly IRepository<Locations.Location> _LocationRepository;
        private readonly IRepository<SubActivityQuestionaryMapping> _SubActivityQuestionaryMappingRepository;
        private readonly IRepository<Questionarie> _QuestionarieRepository;
        private readonly IRepository<SubActivityList> _SubActivityListRepository;
        private readonly IRepository<PrgActionAreaActivityMapping> _PrgActionAreaActivityMappingRepository;
        private readonly IRepository<ChecklistItem> _CheklistItemRepository;
        private readonly IRepository<UnitofMeasure> _UnitofMeasureRepository;
        private readonly IRepository<Checklist> _checkListRepository;
        private readonly IRepository<ProgramCostEstimation> _PrgCostEstRepository;
        private IRepository<ProgramQuqterUnitMapping> _ProgramQuqterUnitMappingRepository;
        private readonly IRepository<MapSubActivityIteamsToImplementionPlan> _MapSubActivityRepository;
        private readonly IRepository<ProgramExpense> _ProgramExpenseRepository;
        private readonly IRepository<prgImplementationPlanQuestionariesMapping> _prgImplementationPlanQuestionariesMappingRepository;
        private readonly ILoginAndroidService _LoginAndroidService;
        private readonly IAppFolders _appFolders;

        public ImplimantionChecklistAppService(IRepository<ImplementationPlanCheckList> ImplementationPlanCheckListRepository,
            IRepository<Role> abproleRepository, IRepository<RequestAAplas> RequestAAplasRepository,
             IRepository<Program> ProgrameRepository, IRepository<Locations.Location> LocationRepository,
              IRepository<PrgActionAreaActivityMapping> PrgActionAreaActivityMappingRepository,
               IRepository<SubActivityQuestionaryMapping> SubActivityQuestionaryMappingRepository,
              IRepository<ProgramCostEstimation> PrgCostEstRepository, IRepository<Questionarie> QuestionarieRepository,
             IRepository<SubActivityList> SubActivityListRepository,
            IRepository<ProgramQuqterUnitMapping> ProgramQuqterUnitMappingRepository,
            IRepository<ChecklistItem> CheklistItemRepository, IRepository<Checklist> checkListRepository, IRepository<UnitofMeasure> UnitofMeasureRepository, 
            IRepository<MapSubActivityIteamsToImplementionPlan> MapSubActivityRepository,
            IRepository<ProgramExpense> ProgramExpenseRepository, IRepository<prgImplementationPlanQuestionariesMapping> prgImplementationPlanQuestionariesMappingRepository,
            ILoginAndroidService LoginAndroidService, IAppFolders appFolders)
        {
            _ImplementationPlanCheckListRepository = ImplementationPlanCheckListRepository;
            _abproleRepository = abproleRepository;
            _RequestAAplasRepository = RequestAAplasRepository;
            _ProgrameRepository = ProgrameRepository;
            _LocationRepository = LocationRepository;
            _PrgActionAreaActivityMappingRepository = PrgActionAreaActivityMappingRepository;
            _CheklistItemRepository = CheklistItemRepository;
            _checkListRepository = checkListRepository;
            _UnitofMeasureRepository = UnitofMeasureRepository;
            _PrgCostEstRepository = PrgCostEstRepository;
            _MapSubActivityRepository = MapSubActivityRepository;
            _ProgramQuqterUnitMappingRepository = ProgramQuqterUnitMappingRepository;
            _ProgramExpenseRepository = ProgramExpenseRepository;
            _SubActivityQuestionaryMappingRepository = SubActivityQuestionaryMappingRepository;
            _QuestionarieRepository = QuestionarieRepository;
            _SubActivityListRepository = SubActivityListRepository;
            _prgImplementationPlanQuestionariesMappingRepository = prgImplementationPlanQuestionariesMappingRepository;
            _LoginAndroidService = LoginAndroidService;
            _appFolders = appFolders;

        }



        public async Task<List<string>> CreateOrEditImplementionCheckList(ImplimentaionChecklistDto input)
        {
            var Mess = new List<string>();
            var ImpCheckIteam = new ImplementationPlanCheckList();
            if (input.QuaterId != null || input.QuaterId != 0)
            {
                ImpCheckIteam.AssingUserId = input.AssingUserId;
                ImpCheckIteam.ChecklistItemID = input.ChecklistItemID;
                ImpCheckIteam.UnitOfMeasuresID = input.UnitOfMeasuresID;
                ImpCheckIteam.ChecklistID = input.ChecklistID;
                ImpCheckIteam.Units = input.Units;
                ImpCheckIteam.Description = input.Description;
                ImpCheckIteam.IteamLocationId = input.LocationId;
                ImpCheckIteam.CostEstimationYear = input.CostEstimationYear;
                ImpCheckIteam.ProgramID = input.ProgramID;
                ImpCheckIteam.CheklistStartDate = input.CheklistStartDate;
                ImpCheckIteam.CheklistEndDate = input.CheklistEndDate;
                ImpCheckIteam.PrgActionAreaActivityMappingID = input.PrgActionAreaActivityMappingID;
                ImpCheckIteam.status = SubActivityStatus.NotStarted;

                ImpCheckIteam.ProgramQuqterUnitMappingID = input.ProgramQuqterUnitMappingID;
                ImpCheckIteam.PrgQuarter = input.QuaterId;

                if (input.Id == 0)
                {
                       await _ImplementationPlanCheckListRepository.InsertAsync(ImpCheckIteam);
                        Mess.Add("Save Successfully");
                }
                else
                {
                    var upadteQuery = _ImplementationPlanCheckListRepository.FirstOrDefault(id => id.Id == input.Id);

                    upadteQuery.AssingUserId = input.AssingUserId;
                    upadteQuery.ChecklistItemID = input.ChecklistItemID;
                    upadteQuery.UnitOfMeasuresID = input.UnitOfMeasuresID;
                    upadteQuery.Units = input.Units;
                    upadteQuery.Description = input.Description;
                    upadteQuery.CheklistStartDate = input.CheklistStartDate;
                    upadteQuery.CheklistEndDate = input.CheklistEndDate;

                   await _ImplementationPlanCheckListRepository.UpdateAsync(upadteQuery);
                    Mess.Add("Updated Successfully");
                }

                var item = _ProgramQuqterUnitMappingRepository.FirstOrDefault(x => x.Id == input.ProgramQuqterUnitMappingID);
                item.Status = ProgramQuarterUnitMappingStatus.Inprogress;
                _ProgramQuqterUnitMappingRepository.Update(item);
            }
            else Mess.Add("Quater NotSelected! ");
            return Mess;
        }

        public int RemainingSubActivitiesMap(int? prgActionMapId, int? unitId)
        {
            var query = _MapSubActivityRepository.GetAll().Where(ma => ma.PrgActionAreaActivityMappingID == prgActionMapId).Count() - _ImplementationPlanCheckListRepository.GetAll().Where(im => im.ProgramQuqterUnitMappingID == unitId).Count();
            return query;
        }

        public List<QuestionaryListDto> GetQuestionaryBySubActivity(int? subActivityId)
        {
            var query = (from che in _CheklistItemRepository.GetAll().Where(ch => ch.Id == subActivityId)
                         join subAct in _SubActivityListRepository.GetAll()
                         on che.SubActivityId equals subAct.Id
                         join sub in _SubActivityQuestionaryMappingRepository.GetAll()
                         on subAct.Id equals sub.SubActivityId
                         join q in _QuestionarieRepository.GetAll()
                         on sub.QuestionarieID equals q.Id
                         select new QuestionaryListDto()
                         {
                             Question = q.Name,
                             InputType = q.InputType
                         }).ToList();
            return query;
        }

        public async Task<List<ImplementationListDto>> GetImplementionPlan(int programId)
        {
            var managerId = AbpSession.UserId; //2;
            var Query = new List<ImplementationListDto>();
            var mapSubActivity = _MapSubActivityRepository.GetAll().Where(p => p.ProgramID == programId);
            var prgQuarterUnitMap =await _ProgramQuqterUnitMappingRepository.GetAll().Where(p =>  p.ProgramID == programId).ToListAsync();

            var impPlanCheckList = await _ImplementationPlanCheckListRepository.GetAll().Include(t => t.UnitOfMeasures)
                .Include(t => t.ChecklistItem).Include(t => t.PrgActionAreaActivityMapping).Where(p => p.ProgramID == programId).ToListAsync();

            var imp = await _RequestAAplasRepository.GetAll().Include(a => a.Village).Include(a => a.UnitOfMeasures).Where(p =>  p.ProgramID == programId).ToListAsync();
            
            

            var useRoleId = await UserManager.Users.FirstOrDefaultAsync(u => u.Id == managerId);//roleId
            var role = await _abproleRepository.FirstOrDefaultAsync(r => r.Id == useRoleId.UserRole);

            if (role == null || role.DisplayName == "Program Manager" || role.DisplayName == "RRC Incharge" || role.DisplayName == null)

            {
                var o = imp.Where(p => p.RequestStatus == RequestStatus.Approved).ToList();
                try
                {
                    Query = (from r in o
                             group r by new
                             {
                                 r.LocationID,
                                 r.ManagerID,
                                 r.ProgramID,
                                 r.RequestStatus,
                             } into gcs
                             join l in _LocationRepository.GetAll()
                            on gcs.Key.LocationID equals l.Id
                             join p in _ProgrameRepository.GetAll()
                             on gcs.Key.ProgramID equals p.Id


                             select new ImplementationListDto()
                             {
                                 ProgramName = p.Name,
                                 ProgramID = p.Id,
                                 ProgrameStartDate = p.ProgramStartDate,
                                 ProgrameEndDate = p.PrgramEndDate,
                                 LocationID = gcs.Key.LocationID,
                                 ManagerID = gcs.Key.ManagerID,
                                 ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == gcs.Key.ManagerID)?.Name,
                                 LocationName = l.Name,
                                 Id = l.Id,
                                 RequestStatus = gcs.Key.RequestStatus,
                                 CostEstimationYear = (from q in imp.Where(ma => ma.LocationID == gcs.Key.LocationID)
                                                       group q by new
                                                       {
                                                           q.CostEstimationYear
                                                       } into gcs1

                                                       select new PrgCostEstimationYearDto()
                                                       {
                                                           Id = l.Id,
                                                           CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                           ActionSubactionCost = (from a in imp.Where(mn => mn.LocationID == gcs.Key.LocationID
                                                                                  && mn.CostEstimationYear == gcs1.Key.CostEstimationYear && mn.ManagerID == gcs.Key.ManagerID)
                                                                                  join pa in _PrgActionAreaActivityMappingRepository.GetAll().Include(t => t.ActionArea).Include(t => t.Activity).Include(t => t.SubActionArea)
                                                                                  on a.PrgActionAreaActivityMappingID equals pa.Id
                                                                                  join pc in _PrgCostEstRepository.GetAll()
                                                                                  on pa.ActivityID equals pc.ActivityID
                                                                                  where (pc.ComponentID == pa.ComponentID && pc.CostEstimationYear == gcs1.Key.CostEstimationYear && pc.ProgramID == gcs.Key.ProgramID)

                                                                                  select new ListActionSubactionListImplemenTationDto()
                                                                                  {
                                                                                      Id = a.Id,
                                                                                      ActionAreaName = a.VillageID != null ? string.Empty : pa.ActionArea.Name,
                                                                                      SubActionAreaName = a.VillageID != null ? string.Empty : pa.SubActionArea.Name,
                                                                                      ActivityName = a.VillageID != null ? string.Empty : pa.Activity.Name,
                                                                                      ActivityId = pa.Activity.Id,
                                                                                      LocationID = a.LocationID,
                                                                                      ProgramID = a.ProgramID,
                                                                                      CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                                                      ActivityLevel = pc.ActivityLevel,
                                                                                      VillageID = a.VillageID,
                                                                                      VillageName = a.VillageID == null ? string.Empty : a.Village.Name,
                                                                                      UnitOfMeasuresName = a.UnitOfMeasuresID == null ? string.Empty : a.UnitOfMeasures.Name,
                                                                                      UnitOfMeasuresID = a.UnitOfMeasuresID,
                                                                                      PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                      TotalUnits = a.TotalUnits,
                                                                                      UnitCost = a.UnitCost,
                                                                                      TotalUnitCost = a.TotalUnitCost,
                                                                                      CommunityContribution = a.CommunityContribution,
                                                                                      CommunityContributionRecived = a.CommunityContributionRecived,
                                                                                      GovernmentContributionRecived = a.GovernmentContributionRecived,
                                                                                      FunderContribution = a.FunderContribution,
                                                                                      OtherContribution = a.OtherContribution,
                                                                                      RequestStatus = a.RequestStatus,
                                                                                      Quarter1PhysicalUnits = a.Quarter1PhysicalUnits,
                                                                                      Quarter1FinancialRs = a.Quarter1FinancialRs,

                                                                                      Quarter2PhysicalUnits = a.Quarter2PhysicalUnits,
                                                                                      Quarter2FinancialRs = a.Quarter2FinancialRs,

                                                                                      Quarter3PhysicalUnits = a.Quarter3PhysicalUnits,
                                                                                      Quarter3FinancialRs = a.Quarter3FinancialRs,

                                                                                      Quarter4PhysicalUnits = a.Quarter4PhysicalUnits,
                                                                                      Quarter4FinancialRs = a.Quarter4FinancialRs,

                                                                                      Quarter1WisePlan = (from qa in prgQuarterUnitMap.Where(qa => qa.ProgramID == a.ProgramID && qa.QuarterId == 1
                                                                                                          && qa.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa.VillageID == a.VillageID
                                                                                                          && qa.ProgramQuaterYear == gcs1.Key.CostEstimationYear)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id)
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == null || s.UnitOfMeasuresID == 0 ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == 0 ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                activityname = s.PrgActionAreaActivityMapping.Activity.Name,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                                    _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                            }).ToList(),
                                                                                                          }).ToList(),
                                                                                      Quarter2WisePlan = (from qa in prgQuarterUnitMap.Where(qa1 => qa1.ProgramID == a.ProgramID
                                                                                                          && qa1.QuarterId == 2 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID && qa1.ProgramQuaterYear == gcs1.Key.CostEstimationYear)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id)
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == 0 ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == 0 ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                   _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                            }).ToList(),
                                                                                                          }).ToList(),
                                                                                      Quarter3WisePlan = (from qa in prgQuarterUnitMap.Where(qa1 => qa1.ProgramID == a.ProgramID
                                                                                                          && qa1.QuarterId == 3 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID && qa1.ProgramQuaterYear == gcs1.Key.CostEstimationYear)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id)
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == 0 ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == 0 ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                   _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                            }).ToList(),
                                                                                                          }).ToList(),
                                                                                      Quarter4WisePlan = (from qa in prgQuarterUnitMap.Where(qa1 => qa1.ProgramID == a.ProgramID
                                                                                                          && qa1.QuarterId == 4 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID && qa1.ProgramQuaterYear == gcs1.Key.CostEstimationYear)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id)
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == 0 ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == 0 ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                   _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                            }).ToList(),
                                                                                                          }).ToList(),

                                                                                      Managers = (from u in UserManager.Users
                                                                                                  join ld in _LocationRepository.GetAll().Where(lc => lc.Id == gcs.Key.LocationID.Value)
                                                                                                  on true equals true
                                                                                                  where (ld.Id == u.LocationId.Value || ld.ParentLocation == u.LocationId.Value)
                                                                                                  select new PrgManagerImplentationListDto()
                                                                                                  {
                                                                                                      Name = u.FullName,
                                                                                                      ManagerId = u.Id,
                                                                                                  }).ToList(),


                                                                                      SubActivityandIteams = (from ch in _checkListRepository.GetAll().Include(t => t.Activity).Where(c => c.ActivityID == pa.Activity.Id)
                                                                                                              select new SubActivityandIteams()
                                                                                                              {
                                                                                                                  ChecklistID = ch.Id,
                                                                                                                  ChecklistName = ch.ChecklistName,
                                                                                                                  Description = ch.Description,
                                                                                                                  ActivityID = ch.ActivityID,
                                                                                                                  ActivityName = ch.ActivityID == 0 ? string.Empty : ch.Activity.Name,
                                                                                                                  ProgramID = p.Id,
                                                                                                                  PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                                                  Selectediteams = new List<cheklistandSublistDto1>(),
                                                                                                                  iteam = (from chI in _CheklistItemRepository.GetAll().Include(t => t.Checklist).Where(ci => ci.ChecklistID == ch.Id)
                                                                                                                           select new cheklistandSublistDto1()
                                                                                                                           {
                                                                                                                               CheklisiteamtName = chI.Name,
                                                                                                                               checklistID_subiteamTable = chI.Id,
                                                                                                                           }).ToList(),
                                                                                                              }).ToList(),

                                                                                  }).OrderBy(pe => pe.PrgActionAreaActivityMappingID).ThenByDescending(pe=>pe.ActivityName).ToList(),
                                                       }).ToList(),

                             }).ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            else
            {
                var o = imp.Where(p => p.RequestStatus == RequestStatus.Approved && p.ManagerID == managerId).ToList();
                try
                 {
                    Query = (from r in o
                             group r by new
                             {
                                 r.LocationID,
                                 r.ManagerID,
                                 r.ProgramID,
                                 r.RequestStatus,
                             } into gcs
                             join l in _LocationRepository.GetAll()
                            on gcs.Key.LocationID equals l.Id
                             join p in _ProgrameRepository.GetAll()
                             on gcs.Key.ProgramID equals p.Id


                             select new ImplementationListDto()
                             {
                                 ProgramName = p.Name,
                                 ProgramID = p.Id,
                                 ProgrameStartDate = p.ProgramStartDate,
                                 ProgrameEndDate = p.PrgramEndDate,
                                 LocationID = gcs.Key.LocationID,
                                 ManagerID = gcs.Key.ManagerID,
                                 ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == gcs.Key.ManagerID)?.Name,
                                 LocationName = l.Name,
                                 Id = l.Id,
                                 RequestStatus = gcs.Key.RequestStatus,
                                 CostEstimationYear = (from q in imp.Where(ma => ma.LocationID == gcs.Key.LocationID)
                                                       group q by new
                                                       {
                                                           q.CostEstimationYear
                                                       } into gcs1

                                                       select new PrgCostEstimationYearDto()
                                                       {
                                                           Id = l.Id,
                                                           CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                           ActionSubactionCost = (from a in imp.Where(mn => mn.LocationID == gcs.Key.LocationID
                                                                                  && mn.CostEstimationYear == gcs1.Key.CostEstimationYear && mn.ManagerID == gcs.Key.ManagerID)

                                                                                  join pa in _PrgActionAreaActivityMappingRepository.GetAll().Include(t => t.ActionArea).Include(t => t.Activity).Include(t => t.SubActionArea)
                                                                                  on a.PrgActionAreaActivityMappingID equals pa.Id
                                                                                  join pc in _PrgCostEstRepository.GetAll()
                                                                                  on pa.ActivityID equals pc.ActivityID
                                                                                  where (pc.ComponentID == pa.ComponentID && pc.CostEstimationYear == gcs1.Key.CostEstimationYear && pc.ProgramID == gcs.Key.ProgramID)

                                                                                  select new ListActionSubactionListImplemenTationDto()
                                                                                  {
                                                                                      Id = a.Id,
                                                                                      ActionAreaName = a.VillageID != null ? string.Empty : pa.ActionArea.Name,
                                                                                      SubActionAreaName = a.VillageID != null ? string.Empty : pa.SubActionArea.Name,
                                                                                      ActivityName = a.VillageID != null ? string.Empty : pa.Activity.Name,
                                                                                      ActivityId = pa.ActivityID,
                                                                                      LocationID = a.LocationID,
                                                                                      ProgramID = a.ProgramID,
                                                                                      CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                                                      ActivityLevel = pc.ActivityLevel,
                                                                                      VillageID = a.VillageID,
                                                                                      VillageName = a.VillageID == null ? string.Empty : a.Village.Name,
                                                                                      UnitOfMeasuresName = a.UnitOfMeasuresID == 0 ? string.Empty : a.UnitOfMeasures.Name,
                                                                                      UnitOfMeasuresID = a.UnitOfMeasuresID,
                                                                                      PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                      TotalUnits = a.TotalUnits,
                                                                                      UnitCost = a.UnitCost,
                                                                                      TotalUnitCost = a.TotalUnitCost,
                                                                                      CommunityContribution = a.CommunityContribution,
                                                                                      CommunityContributionRecived = a.CommunityContributionRecived,
                                                                                      GovernmentContributionRecived = a.GovernmentContributionRecived,
                                                                                      FunderContribution = a.FunderContribution,
                                                                                      OtherContribution = a.OtherContribution,
                                                                                      RequestStatus = a.RequestStatus,
                                                                                      Quarter1PhysicalUnits = a.Quarter1PhysicalUnits,
                                                                                      Quarter1FinancialRs = a.Quarter1FinancialRs,

                                                                                      Quarter2PhysicalUnits = a.Quarter2PhysicalUnits,
                                                                                      Quarter2FinancialRs = a.Quarter2FinancialRs,

                                                                                      Quarter3PhysicalUnits = a.Quarter3PhysicalUnits,
                                                                                      Quarter3FinancialRs = a.Quarter3FinancialRs,

                                                                                      Quarter4PhysicalUnits = a.Quarter4PhysicalUnits,
                                                                                      Quarter4FinancialRs = a.Quarter4FinancialRs,
                                                                                      Quarter1WisePlan = (from qa in prgQuarterUnitMap.Where(qa => qa.ProgramID == a.ProgramID && qa.CreatorUserId == gcs.Key.ManagerID
                                                                                                          && qa.QuarterId == 1 && qa.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa.VillageID == a.VillageID && qa.ProgramQuaterYear == gcs1.Key.CostEstimationYear)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id) // "&& su.AssingUserId == AbpSession.UserId.Value"  for nillanth.rane and  arun dahale record
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == null ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == null ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,

                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                    _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                            }).ToList(),
                                                                                                          }).ToList(),
                                                                                      Quarter2WisePlan = (from qa in prgQuarterUnitMap.Where(qa1 => qa1.ProgramID == a.ProgramID && qa1.CreatorUserId == gcs.Key.ManagerID
                                                                                                          && qa1.QuarterId == 2 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID && qa1.ProgramQuaterYear == gcs1.Key.CostEstimationYear)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id ) // "&& su.AssingUserId == AbpSession.UserId.Value"  for nillanth.rane and  arun dahale record
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == null ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == null ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                   _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                            }).ToList(),
                                                                                                          }).ToList(),
                                                                                      Quarter3WisePlan = (from qa in prgQuarterUnitMap.Where(qa1 => qa1.ProgramID == a.ProgramID && qa1.CreatorUserId == gcs.Key.ManagerID
                                                                                                          && qa1.QuarterId == 3 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID && qa1.ProgramQuaterYear == gcs1.Key.CostEstimationYear)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id ) // "&& su.AssingUserId == AbpSession.UserId.Value"  for nillanth.rane and  arun dahale record
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == null ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == null ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                   _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                            }).ToList(),
                                                                                                          }).ToList(),
                                                                                      Quarter4WisePlan = (from qa in prgQuarterUnitMap.Where(qa1 => qa1.ProgramID == a.ProgramID && qa1.CreatorUserId == gcs.Key.ManagerID
                                                                                                          && qa1.QuarterId == 4
                                                                                                          && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID
                                                                                                          && qa1.VillageID == a.VillageID
                                                                                                          && qa1.ProgramQuaterYear == gcs1.Key.CostEstimationYear
                                                                                                          )
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id ) // "&& su.AssingUserId == AbpSession.UserId.Value"  for nillanth.rane and  arun dahale record
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == null ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItem == null ? string.Empty : s.ChecklistItem.Name, // change for anshuman
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem == null ? 0 : s.ChecklistItem.Id,// change for anshuman 
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                   _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                            }).ToList(),
                                                                                                          }).ToList(),

                                                                                      Managers = (from u in UserManager.Users
                                                                                                  join ld in _LocationRepository.GetAll().Where(lc => lc.Id == gcs.Key.LocationID.Value)
                                                                                                  on true equals true
                                                                                                  where (ld.Id == u.LocationId.Value || ld.ParentLocation == u.LocationId.Value)
                                                                                                  select new PrgManagerImplentationListDto()
                                                                                                  {
                                                                                                      Name = u.FullName,
                                                                                                      ManagerId = u.Id,
                                                                                                  }).ToList(),


                                                                                      SubActivityandIteams = (from ch in _checkListRepository.GetAll().Include(t => t.Activity).Where(c => c.ActivityID == pa.ActivityID)
                                                                                                              select new SubActivityandIteams()
                                                                                                              {
                                                                                                                  ChecklistID = ch.Id,
                                                                                                                  ChecklistName = ch.ChecklistName,
                                                                                                                  Description = ch.Description,
                                                                                                                  ActivityID = ch.ActivityID,
                                                                                                                  ActivityName = ch.Activity.Name,
                                                                                                                  ProgramID = p.Id,
                                                                                                                  PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                                                  Selectediteams = new List<cheklistandSublistDto1>(),
                                                                                                                  iteam = (from chI in _CheklistItemRepository.GetAll().Include(t => t.Checklist).Where(ci => ci.ChecklistID == ch.Id)
                                                                                                                           select new cheklistandSublistDto1()
                                                                                                                           {
                                                                                                                               CheklisiteamtName = chI.Name,
                                                                                                                               checklistID_subiteamTable = chI.Id,
                                                                                                                           }).ToList(),
                                                                                                              }).ToList(),

                                                                                  }).OrderBy(pe => pe.PrgActionAreaActivityMappingID).ThenByDescending(pe => pe.ActivityName).ToList(),
                                                       }).ToList(),

                             }).ToList();
                }
                catch (Exception ex) {
                    throw ex;
                }
            }



            return new List<ImplementationListDto>(ObjectMapper.Map<List<ImplementationListDto>>(Query));

        }

        public async Task<FileDto> CreateExcelImpilemetationDoc(string fileName, int programid,string Year)
        {
            var result = await GetImplementionPlanForExcelReport(programid,Year);

            var ac = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";

            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(ac, SpreadsheetDocumentType.Workbook))
            {
                var wbp = xl.AddWorkbookPart();
                var wsp = wbp.AddNewPart<WorksheetPart>();
                var wb = new Workbook();
                var fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();

                string sImagePath = _appFolders.LogoImgFolder + "Reportlogo.png"; ;
                var dp = wsp.AddNewPart<DrawingsPart>();
                var imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                var nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                var nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                var nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                var blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                var bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                var sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                var pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                var ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                var anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                var wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                var drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                var sheets = new Sheets();
                var sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                wb.Append(fv);

                // Constructing header
                var row2 = new Row();
                var row3 = new Row();
                var row4 = new Row();
                var row5 = new Row();
                var row8 = new Row();
                var row9 = new Row();
                var row10 = new Row();
                var row11 = new Row();
                var row12 = new Row();
                var row13 = new Row();
                var row14 = new Row();
                var row15 = new Row();
                var row16 = new Row();
                var row17 = new Row();
                foreach (var program in result)
                {
                    var cell8 = InsertCellInWorksheet("D", 1, wsp);
                    cell8.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                    cell8.DataType = new EnumValue<CellValues>(CellValues.Number);
                    

                    Run run1 = new Run();
                    run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                    RunProperties run1Properties = new RunProperties();
                    run1Properties.Append(new Bold());
                    run1.RunProperties = run1Properties;
                    InlineString inlineString = new InlineString();
                    inlineString.Append(run1);
                    cell8.Append(inlineString);



                    var cell82 = InsertCellInWorksheet("D", 2, wsp);
                    cell82.CellValue = new CellValue("Implemention Plan Grouping");
                    cell82.DataType = new EnumValue<CellValues>(CellValues.Number);

                    Run run2 = new Run();
                    run2.Append(new Text("Implimetaion Plan Grouping"));
                    RunProperties run2Properties = new RunProperties();
                    Color color3 = new Color() { Rgb = "2F75B5" };
                    FontSize fontSize3 = new FontSize() { Val = 16D };
                    run2Properties.Append(new Bold());
                    run2Properties.Append(color3);
                    run2Properties.Append(fontSize3);
                    run2.RunProperties = run2Properties;
                    InlineString inlineString1 = new InlineString();
                    inlineString1.Append(run2);
                    cell82.Append(inlineString1);


                    var cella = InsertCellInWorksheet("C", 3, wsp);
                    cella.CellValue = new CellValue("Program Title");
                    cella.DataType = new EnumValue<CellValues>(CellValues.Number);

                    var cellString = InsertCellInWorksheet("D", 3, wsp);
                    cellString.CellValue = new CellValue(program.ProgramName);
                    cellString.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cell13 = InsertCellInWorksheet("D", 4, wsp);
                    cell13.CellValue = new CellValue("Year: " + Year);
                    cell13.DataType = new EnumValue<CellValues>(CellValues.Number);

                    //Cell cell1 = InsertCellInWorksheet("A", 4, wsp);
                    //cell1.CellValue = new CellValue("Clustor Name");
                    //cell1.DataType = new EnumValue<CellValues>(CellValues.Number);

                    //Cell cellString1 = InsertCellInWorksheet("B", 4, wsp);
                    //cellString1.CellValue = new CellValue(program.LocationName.ToString());
                    //cellString1.DataType = new EnumValue<CellValues>(CellValues.Number);

                    //Cell cell2 = InsertCellInWorksheet("A", 5, wsp);
                    //cell2.CellValue = new CellValue("Program Manager");
                    //cell2.DataType = new EnumValue<CellValues>(CellValues.Number);

                    //Cell cellString2 = InsertCellInWorksheet("B", 5, wsp);
                    //cellString2.CellValue = new CellValue(program.ManagerName);
                    //cellString2.DataType = new EnumValue<CellValues>(CellValues.Number);

                }
                Cell cell3 = InsertCellInWorksheet("C", 4, wsp);
                cell3.CellValue = new CellValue("Year");
                cell3.DataType = new EnumValue<CellValues>(CellValues.Number);

                Cell cellString3 = InsertCellInWorksheet("D", 4, wsp);
                cellString3.CellValue = new CellValue(Year.ToString());
                cellString3.DataType = new EnumValue<CellValues>(CellValues.Number);

                row3.Append(
                    ConstructCell(" ", CellValues.String),
                    ConstructCell(" ", CellValues.String),
                    ConstructCell("Clustor Name ", CellValues.String),
                    ConstructCell("Manager Name ", CellValues.String));
                sd.AppendChild(row3);
                foreach (var item in result)
                {
                    row3 = new Row();
                    row3.Append(
                        ConstructCell("", CellValues.String),
                        ConstructCell(" ", CellValues.String),
                        ConstructCell(item.LocationName, CellValues.String),
                    ConstructCell(item.ManagerName, CellValues.String));
                    sd.AppendChild(row3);
                }
                
                    

                var row33 = new Row();

                row33.Append(ConstructCell(" ", CellValues.String));
                sd.AppendChild(row33);

                row5 = new Row();
                row5.Append(ConstructCell("Quarter", CellValues.String, 2),
               ConstructCell("Activity", CellValues.String, 2),
                ConstructCell("Sub Activity", CellValues.String, 2),
                ConstructCell("Assinged Field staff Name ", CellValues.String, 2),
                ConstructCell("Unit", CellValues.String, 2),
                ConstructCell("Description", CellValues.Number, 2),
                ConstructCell("Start Date", CellValues.String, 2),
                ConstructCell("End Date", CellValues.Number, 2),
                ConstructCell("status", CellValues.Number, 2));

                sd.AppendChild(row5);

                var quarter1 = "Quarter1";
                var quarter2 = "Quarter2";

                var quarter3 = "Quarter3";
                var quarter4 = "Quarter4";
                foreach (var imp in result)
                {
                    foreach (var c in imp.CostEstimationYear)
                    {
                        row13 = new Row();
                        row13.Append(
                        ConstructCell(quarter1 + ' ' + imp.LocationName, CellValues.String));
                        sd.AppendChild(row13);
                        foreach (var a in c.ActionSubactionCost)
                        {
                            foreach (var q in a.Quarter1WisePlan)
                            {
                                
                                if (a.Quarter1WisePlan.Count != 0)
                                {
                                    foreach (var sub in q.subUnitActivityQuarterWise)
                                    {
                                        var VillageName = a.VillageName == "" ? "" : a.VillageName;
                                        row4 = new Row();
                                        row4.Append(ConstructCell("    " + ' ' + VillageName, CellValues.String, 1),
                                            ConstructCell(sub.activityname.ToString(), CellValues.String, 1),
                                            ConstructCell(sub.ChecklistItemName.ToString(), CellValues.String, 1),
                                            ConstructCell(sub.AssignedUserName.ToString(), CellValues.String, 1),
                                            ConstructCell(sub.Units.ToString(), CellValues.Number, 1),
                                            ConstructCell(sub.Description == "" ? "" : sub.Description, CellValues.String, 1),
                                            ConstructCell(sub.CheklistStartDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                                            ConstructCell(sub.CheklistEndDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                                            ConstructCell(sub.SubActivityStatus.ToString(), CellValues.String, 1)
                                            );

                                        sd.AppendChild(row4);
                                        
                                    }
                                }
                                
                            }
                        }

                    }

                    row14 = new Row();
                    row14.Append(
               ConstructCell("", CellValues.String));
                    sd.AppendChild(row14);
                }
                

                foreach (var imp1 in result)
                {
                    foreach (var c in imp1.CostEstimationYear)
                    {
                        row15 = new Row();
                        row15.Append(
                   ConstructCell(quarter2 + ' ' + imp1.LocationName, CellValues.String));
                        sd.AppendChild(row15);

                        foreach (var a in c.ActionSubactionCost)
                        {
                            foreach (var q in a.Quarter2WisePlan)
                            {
                                if (a.Quarter2WisePlan.Count != 0)
                                {
                                   
                                    foreach (var sub in q.subUnitActivityQuarterWise)
                                    {
                                        var VillageName = a.VillageName == "" ? "" : a.VillageName;
                                        row9 = new Row();
                                        row9.Append(ConstructCell("   " +' '+ VillageName, CellValues.String, 1),
                                            ConstructCell(sub.activityname.ToString(), CellValues.String, 1),
                                            ConstructCell(sub.ChecklistItemName.ToString(), CellValues.String, 1),
                                            ConstructCell(sub.AssignedUserName.ToString(), CellValues.String, 1),
                                            ConstructCell(sub.Units.ToString(), CellValues.Number, 1),
                                            ConstructCell(sub.Description == "" ? "" : sub.Description, CellValues.String, 1),
                                            ConstructCell(sub.CheklistStartDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                                            ConstructCell(sub.CheklistEndDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                                            ConstructCell(sub.SubActivityStatus.ToString(), CellValues.String, 1)
                                            );

                                        sd.AppendChild(row9);
                                        
                                    }
                                }
                            }



                        }
                    }
                    row14 = new Row();
                    row14.Append(
               ConstructCell("", CellValues.String));
                    sd.AppendChild(row14);
                }

                foreach (var imp2 in result)
                {
                    foreach (var c in imp2.CostEstimationYear)
                    {
                        row17 = new Row();
                        row17.Append(
                   ConstructCell(quarter3 + ' ' + imp2.LocationName, CellValues.String));
                        sd.AppendChild(row17);

                        foreach (var a in c.ActionSubactionCost)
                        {
                            foreach (var q in a.Quarter3WisePlan)
                            {
                                if (a.Quarter3WisePlan.Count != 0)
                                {

                                    foreach (var sub in q.subUnitActivityQuarterWise)
                                    {
                                        var VillageName = a.VillageName == "" ? "" : a.VillageName;
                                        row10 = new Row();
                                        row10.Append(ConstructCell("   " + ' ' + VillageName, CellValues.String, 1),
                                            ConstructCell(sub.activityname.ToString(), CellValues.String, 1),
                                            ConstructCell(sub.ChecklistItemName.ToString(), CellValues.String, 1),
                                            ConstructCell(sub.AssignedUserName.ToString(), CellValues.String, 1),
                                            ConstructCell(sub.Units.ToString(), CellValues.Number, 1),
                                            ConstructCell(sub.Description == "" ? "" : sub.Description, CellValues.String, 1),
                                            ConstructCell(sub.CheklistStartDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                                            ConstructCell(sub.CheklistEndDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                                            ConstructCell(sub.SubActivityStatus.ToString(), CellValues.String, 1)
                                            );

                                        sd.AppendChild(row10);

                                    }
                                }
                            }



                        }
                    }
                    row14 = new Row();
                    row14.Append(
               ConstructCell("", CellValues.String));
                    sd.AppendChild(row14);
                }

                foreach (var imp3 in result)
                {
                    foreach (var c in imp3.CostEstimationYear)
                    {
                        row16 = new Row();
                        row16.Append(
                   ConstructCell(quarter4 + ' ' + imp3.LocationName, CellValues.String));
                        sd.AppendChild(row16);

                        foreach (var a in c.ActionSubactionCost)
                        {
                            foreach (var q in a.Quarter4WisePlan)
                            {
                                if (a.Quarter4WisePlan.Count != 0)
                                {

                                    foreach (var sub in q.subUnitActivityQuarterWise)
                                    {
                                        var VillageName = a.VillageName == "" ? "" : a.VillageName;
                                        row11 = new Row();
                                        row11.Append(ConstructCell("   " + ' ' + VillageName, CellValues.String, 1),
                                            ConstructCell(sub.activityname.ToString(), CellValues.String, 1),
                                            ConstructCell(sub.ChecklistItemName.ToString(), CellValues.String, 1),
                                            ConstructCell(sub.AssignedUserName.ToString(), CellValues.String, 1),
                                            ConstructCell(sub.Units.ToString(), CellValues.Number, 1),
                                            ConstructCell(sub.Description == "" ? "" : sub.Description, CellValues.String, 1),
                                            ConstructCell(sub.CheklistStartDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                                            ConstructCell(sub.CheklistEndDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                                            ConstructCell(sub.SubActivityStatus.ToString(), CellValues.String, 1)
                                            );

                                        sd.AppendChild(row11);

                                    }
                                }
                            }



                        }
                    }
                    row14 = new Row();
                    row14.Append(
               ConstructCell("", CellValues.String));
                    sd.AppendChild(row14);
                }
                //foreach (var imp in result)
                //{
                //    foreach (var c in imp.CostEstimationYear)
                //    {
                //        foreach (var a in c.ActionSubactionCost)
                //        {
                //            foreach (var q in a.Quarter1WisePlan)
                //            {
                //                if (a.Quarter1WisePlan.Count != 0)
                //                {
                //                    foreach (var sub in q.subUnitActivityQuarterWise)
                //                    {
                //                        var VillageName = a.VillageName == "" ? "" : a.VillageName;
                //                        row4 = new Row();
                //                        row4.Append(ConstructCell(quarter1 + ' '+ VillageName, CellValues.String, 1),
                //                            ConstructCell(sub.activityname.ToString(), CellValues.String, 1),
                //                            ConstructCell(sub.ChecklistItemName.ToString(), CellValues.String, 1),
                //                            ConstructCell(sub.AssignedUserName.ToString(), CellValues.String, 1),
                //                            ConstructCell(sub.Units.ToString(), CellValues.Number, 1),
                //                            ConstructCell(sub.Description == "" ? "" : sub.Description, CellValues.String, 1),
                //                            ConstructCell(sub.CheklistStartDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                //                            ConstructCell(sub.CheklistEndDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                //                            ConstructCell(sub.SubActivityStatus.ToString(), CellValues.String, 1)
                //                            );

                //                        sd.AppendChild(row4);
                //                        //quarter1 = "";
                //                    }
                //                }
                //            }
                //            foreach (var q2 in a.Quarter2WisePlan)
                //            {
                //                if (a.Quarter2WisePlan.Count != 0)
                //                {
                //                    foreach (var sub in q2.subUnitActivityQuarterWise)
                //                    {
                //                        var VillageName = a.VillageName == "" ? "" : a.VillageName;
                //                        row10 = new Row();
                //                        row10.Append(ConstructCell(quarter2 +' '+ VillageName, CellValues.String, 1),
                //                            ConstructCell(sub.activityname.ToString(), CellValues.String, 1),
                //                            ConstructCell(sub.ChecklistItemName.ToString(), CellValues.String, 1),
                //                            ConstructCell(sub.AssignedUserName.ToString(), CellValues.String, 1),
                //                            ConstructCell(sub.Units.ToString(), CellValues.Number, 1),
                //                            ConstructCell(sub.Description==""?"":sub.Description, CellValues.String, 1),
                //                            ConstructCell(sub.CheklistStartDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                //                            ConstructCell(sub.CheklistEndDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                //                            ConstructCell(sub.SubActivityStatus.ToString(), CellValues.String, 1));

                //                        sd.AppendChild(row10);
                //                        //quarter2 = "";

                //                    }
                //                }
                //            }
                //            foreach (var q3 in a.Quarter3WisePlan)
                //            {
                //                if (a.Quarter3WisePlan.Count != 0)
                //                {
                //                    foreach (var sub in q3.subUnitActivityQuarterWise)
                //                    {
                //                        var VillageName = a.VillageName == "" ? "" : a.VillageName;
                //                        row11 = new Row();
                //                        row11.Append(ConstructCell(quarter3 +' ' + VillageName, CellValues.String, 1),
                //                            ConstructCell(sub.activityname.ToString(), CellValues.String, 1),
                //                            ConstructCell(sub.ChecklistItemName.ToString(), CellValues.String, 1),
                //                            ConstructCell(sub.AssignedUserName.ToString(), CellValues.String, 1),
                //                            ConstructCell(sub.Units.ToString(), CellValues.Number, 1),
                //                             ConstructCell(sub.Description == "" ? "" : sub.Description, CellValues.String, 1),
                //                            ConstructCell(sub.CheklistStartDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                //                            ConstructCell(sub.CheklistEndDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                //                            ConstructCell(sub.SubActivityStatus.ToString(), CellValues.String, 1));

                //                        sd.AppendChild(row11);
                //                        //quarter3 = "";

                //                    }
                //                }
                //            }
                //            foreach (var q4 in a.Quarter4WisePlan)
                //            {
                //                if (a.Quarter4WisePlan.Count != 0)
                //                {
                //                    foreach (var sub in q4.subUnitActivityQuarterWise)
                //                    {
                //                        var VillageName = a.VillageName == "" ? "" : a.VillageName;

                //                        row12 = new Row();
                //                        row12.Append(ConstructCell(quarter4 + ' ' + VillageName, CellValues.String, 1),
                //                            ConstructCell(sub.activityname.ToString(), CellValues.String, 1),
                //                            ConstructCell(sub.ChecklistItemName.ToString(), CellValues.String, 1),
                //                            ConstructCell(sub.AssignedUserName.ToString(), CellValues.String, 1),
                //                            ConstructCell(sub.Units.ToString(), CellValues.Number, 1),
                //                             ConstructCell(sub.Description == "" ? "" : sub.Description, CellValues.String, 1),
                //                            ConstructCell(sub.CheklistStartDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                //                            ConstructCell(sub.CheklistEndDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                //                            ConstructCell(sub.SubActivityStatus.ToString(), CellValues.String, 1));

                //                        sd.AppendChild(row12);
                //                        //quarter4 = "";

                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();
            }

            var file = new FileDto(ac, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;

        }
        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
        private Cell ConstructCell(string value, CellValues dataType, uint styleIndex = 0)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType),
                StyleIndex = styleIndex
            };
        }

        private Stylesheet GenerateStylesheet()
        {
            Stylesheet styleSheet = null;

            Fonts fonts = new Fonts(
                new Font( // Index 0 - default
                    new FontSize() { Val = 11 }

                ),
                new Font( // Index 1 - header
                    new FontSize() { Val = 11 },
                    new Bold(),
                    new Color() { Rgb = "000000" }

                ));

            Fills fills = new Fills(
                    new Fill(new PatternFill() { PatternType = PatternValues.None }), // Index 0 - default
                    new Fill(new PatternFill() { PatternType = PatternValues.Gray125 }), // Index 1 - default
                    new Fill(new PatternFill(new ForegroundColor { Rgb = new HexBinaryValue() { Value = "D0CECE" } })
                    { PatternType = PatternValues.Solid }) // Index 2 - header
                );

            Borders borders = new Borders(
                    new Border(), // index 0 default
                    new Border( // index 1 black border
                        new LeftBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new RightBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new TopBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new BottomBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                );

            CellFormats cellFormats = new CellFormats(
                    new CellFormat(), // default
                    new CellFormat { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }, // body
                    new CellFormat { FontId = 1, FillId = 2, BorderId = 1, ApplyFill = true } // header
                );

            styleSheet = new Stylesheet(fonts, fills, borders, cellFormats);

            return styleSheet;
        }

        public async Task<List<ImplementationListDto>> GetImplementionPlanForExcelReport(int programId,string Year)
        {
            var managerId = AbpSession.UserId;
            var Query = new List<ImplementationListDto>();
            var useRoleId = await UserManager.Users.Where(u => u.Id == managerId).Select(u => u.UserRole).SingleOrDefaultAsync();//roleId
            var roleName = await _abproleRepository.GetAll().Where(r => r.Id == useRoleId).Select(u => u.DisplayName).SingleOrDefaultAsync();
            var imp = await _RequestAAplasRepository.GetAll().Include(a=>a.Village).Include(a=>a.UnitOfMeasures).ToListAsync();
            var mapSubActivity = await _MapSubActivityRepository.GetAll().ToListAsync();
            var impPlanCheckList = await _ImplementationPlanCheckListRepository.GetAll().Include(t=>t.UnitOfMeasures).Include(t=>t.ChecklistItem).Include(t=>t.PrgActionAreaActivityMapping).ToListAsync();
            var prgQuarterUnitMap = await _ProgramQuqterUnitMappingRepository.GetAll().ToListAsync();

            if (roleName == "Program Manager" || roleName == "RRC Incharge" || roleName == null)

            {
               var o = imp.Where(p => p.RequestStatus == RequestStatus.Approved && p.CostEstimationYear==Year && p.ProgramID == programId).ToList();
                try
                {
                    Query = (from r in o
                             group r by new
                             {
                                 r.LocationID,
                                 r.ManagerID,
                                 r.ProgramID,
                                 r.RequestStatus,
                             } into gcs
                             join l in _LocationRepository.GetAll()
                            on gcs.Key.LocationID equals l.Id
                             join p in _ProgrameRepository.GetAll()
                             on gcs.Key.ProgramID equals p.Id


                             select new ImplementationListDto()
                             {
                                 ProgramName = p.Name,
                                 ProgramID = p.Id,
                                 ProgrameStartDate = p.ProgramStartDate,
                                 ProgrameEndDate = p.PrgramEndDate,
                                 LocationID = gcs.Key.LocationID,
                                 ManagerID = gcs.Key.ManagerID,
                                 ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == gcs.Key.ManagerID)?.Name,
                                 LocationName = l.Name,
                                 Id = l.Id,
                                 RequestStatus = gcs.Key.RequestStatus,
                                 CostEstimationYear = (from q in imp.Where(ma => ma.CostEstimationYear == Year && ma.LocationID == gcs.Key.LocationID && ma.ProgramID == gcs.Key.ProgramID)
                                                       group q by new
                                                       {
                                                           q.CostEstimationYear
                                                       } into gcs1

                                                       select new PrgCostEstimationYearDto()
                                                       {
                                                           Id = l.Id,
                                                           CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                           ActionSubactionCost = (from a in imp.Where(mn => mn.LocationID == gcs.Key.LocationID
                                                                                  && mn.CostEstimationYear == Year && mn.ProgramID == gcs.Key.ProgramID && mn.ManagerID == gcs.Key.ManagerID)
                                                                                  join pa in _PrgActionAreaActivityMappingRepository.GetAll().Include(t => t.ActionArea).Include(t => t.Activity).Include(t => t.SubActionArea)
                                                                                  on a.PrgActionAreaActivityMappingID equals pa.Id
                                                                                  join pc in _PrgCostEstRepository.GetAll()
                                                                                  on pa.ActivityID equals pc.ActivityID
                                                                                  where (pc.ComponentID == pa.ComponentID && pc.CostEstimationYear == Year && pc.ProgramID == gcs.Key.ProgramID)

                                                                                  select new ListActionSubactionListImplemenTationDto()
                                                                                  {
                                                                                      Id = a.Id,
                                                                                      ActionAreaName = a.VillageID != null ? string.Empty : pa.ActionArea.Name,
                                                                                      SubActionAreaName = a.VillageID != null ? string.Empty : pa.SubActionArea.Name,
                                                                                      ActivityName = pa.Activity.Name,
                                                                                      ActivityId = pa.Activity.Id,
                                                                                      LocationID = a.LocationID,
                                                                                      ProgramID = a.ProgramID,
                                                                                      CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                                                      ActivityLevel = pc.ActivityLevel,
                                                                                      VillageID = a.VillageID,
                                                                                      VillageName = a.VillageID == null ? string.Empty : a.Village.Name,
                                                                                      UnitOfMeasuresName = a.UnitOfMeasuresID == null ? string.Empty : a.UnitOfMeasures.Name,
                                                                                      UnitOfMeasuresID = a.UnitOfMeasuresID,
                                                                                      PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                      TotalUnits = a.TotalUnits,
                                                                                      UnitCost = a.UnitCost,
                                                                                      TotalUnitCost = a.TotalUnitCost,
                                                                                      CommunityContribution = a.CommunityContribution,
                                                                                      CommunityContributionRecived = a.CommunityContributionRecived,
                                                                                      GovernmentContributionRecived = a.GovernmentContributionRecived,
                                                                                      FunderContribution = a.FunderContribution,
                                                                                      OtherContribution = a.OtherContribution,
                                                                                      RequestStatus = a.RequestStatus,
                                                                                      Quarter1PhysicalUnits = a.Quarter1PhysicalUnits,
                                                                                      Quarter1FinancialRs = a.Quarter1FinancialRs,

                                                                                      Quarter2PhysicalUnits = a.Quarter2PhysicalUnits,
                                                                                      Quarter2FinancialRs = a.Quarter2FinancialRs,

                                                                                      Quarter3PhysicalUnits = a.Quarter3PhysicalUnits,
                                                                                      Quarter3FinancialRs = a.Quarter3FinancialRs,

                                                                                      Quarter4PhysicalUnits = a.Quarter4PhysicalUnits,
                                                                                      Quarter4FinancialRs = a.Quarter4FinancialRs,

                                                                                      Quarter1WisePlan = (from qa in prgQuarterUnitMap.Where(qa => qa.ProgramID == a.ProgramID && qa.QuarterId == 1 && qa.CreatorUserId == gcs.Key.ManagerID
                                                                                                          && qa.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa.VillageID == a.VillageID
                                                                                                          && qa.ProgramQuaterYear == Year)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id && su.CostEstimationYear == Year)
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == 0 ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == 0 ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                activityname = s.PrgActionAreaActivityMapping.Activity.Name,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                                    _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                                LocatioName = _LocationRepository.GetAll().Where(ss => ss.Id == s.IteamLocationId).FirstOrDefault().Name,
                                                                                                                                                VillageName = a.VillageID == null ? string.Empty : a.Village.Name,

                                                                                                                                            }).ToList(),
                                                                                                          }).OrderBy(pe => pe.ActivityName).ThenBy(xx => xx.ActivityId).ToList(),
                                                                                      Quarter2WisePlan = (from qa in prgQuarterUnitMap.Where(qa1 => qa1.ProgramID == a.ProgramID && qa1.CreatorUserId == gcs.Key.ManagerID
                                                                                                          && qa1.QuarterId == 2 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID && qa1.ProgramQuaterYear == Year)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id && su.CostEstimationYear == Year)
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == 0 ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == 0 ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                   _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                                activityname = s.PrgActionAreaActivityMapping.Activity.Name,
                                                                                                                                            }).ToList(),
                                                                                                          }).OrderBy(pe => pe.ActivityName).ThenBy(xx => xx.ActivityId).ToList(),
                                                                                      Quarter3WisePlan = (from qa in prgQuarterUnitMap.Where(qa1 => qa1.ProgramID == a.ProgramID && qa1.CreatorUserId == gcs.Key.ManagerID
                                                                                                          && qa1.QuarterId == 3 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID && qa1.ProgramQuaterYear == Year)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id && su.CostEstimationYear == Year)
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == 0 ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == 0 ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                   _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                                activityname = s.PrgActionAreaActivityMapping.Activity.Name,
                                                                                                                                            }).ToList(),
                                                                                                          }).OrderBy(pe => pe.ActivityName).ThenBy(xx => xx.ActivityId).ToList(),
                                                                                      Quarter4WisePlan = (from qa in prgQuarterUnitMap.Where(qa1 => qa1.ProgramID == a.ProgramID && qa1.CreatorUserId == gcs.Key.ManagerID
                                                                                                          && qa1.QuarterId == 4 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID && qa1.ProgramQuaterYear == Year)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id && su.CostEstimationYear == Year)
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == 0 ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == 0 ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                   _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                                activityname = s.PrgActionAreaActivityMapping.Activity.Name,
                                                                                                                                            }).ToList(),
                                                                                                          }).OrderBy(pe => pe.ActivityName).ThenBy(xx=>xx.ActivityId).ToList(),

                                                                                      Managers = (from u in UserManager.Users
                                                                                                  join ld in _LocationRepository.GetAll().Where(lc => lc.Id == gcs.Key.LocationID.Value)
                                                                                                  on true equals true
                                                                                                  where (ld.Id == u.LocationId.Value || ld.ParentLocation == u.LocationId.Value)
                                                                                                  select new PrgManagerImplentationListDto()
                                                                                                  {
                                                                                                      Name = u.FullName,
                                                                                                      ManagerId = u.Id,
                                                                                                  }).ToList(),


                                                                                      SubActivityandIteams = (from ch in _checkListRepository.GetAll().Include(t => t.Activity).Where(c => c.ActivityID == pa.Activity.Id)
                                                                                                              select new SubActivityandIteams()
                                                                                                              {
                                                                                                                  ChecklistID = ch.Id,
                                                                                                                  ChecklistName = ch.ChecklistName,
                                                                                                                  Description = ch.Description,
                                                                                                                  ActivityID = ch.ActivityID,
                                                                                                                  ActivityName = ch.ActivityID == 0 ? string.Empty : ch.Activity.Name,
                                                                                                                  ProgramID = p.Id,
                                                                                                                  PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                                                  Selectediteams = new List<cheklistandSublistDto1>(),
                                                                                                                  iteam = (from chI in _CheklistItemRepository.GetAll().Include(t => t.Checklist).Where(ci => ci.ChecklistID == ch.Id)
                                                                                                                           select new cheklistandSublistDto1()
                                                                                                                           {
                                                                                                                               CheklisiteamtName = chI.Name,
                                                                                                                               checklistID_subiteamTable = chI.Id,
                                                                                                                           }).ToList(),
                                                                                                              }).ToList(),

                                                                                  }).OrderBy(pe => pe.ActivityName).ThenBy(pee=>pee.ActivityId).ToList(),
                                                       }).OrderBy(pe=>pe.ActionSubactionCost.OrderBy(zz=>zz.ActivityName)).ToList(),

                             }).ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return new List<ImplementationListDto>(ObjectMapper.Map<List<ImplementationListDto>>(Query));

        }

        public async Task<subUnitActivityQuarterWisedetails> GetImplementionPlanForExcelReport1(int programId, string Year)
        {
            var managerId = AbpSession.UserId;
            var Query = new List<ImplementationListDto>();
            var useRoleId = await UserManager.Users.Where(u => u.Id == managerId).Select(u => u.UserRole).SingleOrDefaultAsync();//roleId
            var roleName = await _abproleRepository.GetAll().Where(r => r.Id == useRoleId).Select(u => u.DisplayName).SingleOrDefaultAsync();
            var imp = await _RequestAAplasRepository.GetAll().Include(a => a.Village).Include(a => a.UnitOfMeasures).ToListAsync();
            var mapSubActivity = await _MapSubActivityRepository.GetAll().ToListAsync();
            var impPlanCheckList = await _ImplementationPlanCheckListRepository.GetAll().Include(t => t.UnitOfMeasures).Include(t => t.ChecklistItem).Include(t => t.PrgActionAreaActivityMapping).ToListAsync();
            var prgQuarterUnitMap = await _ProgramQuqterUnitMappingRepository.GetAll().ToListAsync();

            if (roleName == "Program Manager" || roleName == "RRC Incharge" || roleName == null)

            {
                var o = imp.Where(p => p.RequestStatus == RequestStatus.Approved && p.CostEstimationYear == Year && p.ProgramID == programId).ToList();
                try
                {
                    Query = (from r in o
                             group r by new
                             {
                                 r.LocationID,
                                 r.ManagerID,
                                 r.ProgramID,
                                 r.RequestStatus,
                             } into gcs
                             join l in _LocationRepository.GetAll()
                            on gcs.Key.LocationID equals l.Id
                             join p in _ProgrameRepository.GetAll()
                             on gcs.Key.ProgramID equals p.Id


                             select new ImplementationListDto()
                             {
                                 ProgramName = p.Name,
                                 ProgramID = p.Id,
                                 ProgrameStartDate = p.ProgramStartDate,
                                 ProgrameEndDate = p.PrgramEndDate,
                                 LocationID = gcs.Key.LocationID,
                                 ManagerID = gcs.Key.ManagerID,
                                 ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == gcs.Key.ManagerID)?.Name,
                                 LocationName = l.Name,
                                 Id = l.Id,
                                 RequestStatus = gcs.Key.RequestStatus,
                                 CostEstimationYear = (from q in imp.Where(ma => ma.CostEstimationYear == Year && ma.LocationID == gcs.Key.LocationID && ma.ProgramID == gcs.Key.ProgramID)
                                                       group q by new
                                                       {
                                                           q.CostEstimationYear
                                                       } into gcs1

                                                       select new PrgCostEstimationYearDto()
                                                       {
                                                           Id = l.Id,
                                                           CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                           ActionSubactionCost = (from a in imp.Where(mn => mn.LocationID == gcs.Key.LocationID
                                                                                  && mn.CostEstimationYear == Year && mn.ProgramID == gcs.Key.ProgramID && mn.ManagerID == gcs.Key.ManagerID)
                                                                                  join pa in _PrgActionAreaActivityMappingRepository.GetAll().Include(t => t.ActionArea).Include(t => t.Activity).Include(t => t.SubActionArea)
                                                                                  on a.PrgActionAreaActivityMappingID equals pa.Id
                                                                                  join pc in _PrgCostEstRepository.GetAll()
                                                                                  on pa.ActivityID equals pc.ActivityID
                                                                                  where (pc.ComponentID == pa.ComponentID && pc.CostEstimationYear == Year && pc.ProgramID == gcs.Key.ProgramID)

                                                                                  select new ListActionSubactionListImplemenTationDto()
                                                                                  {
                                                                                      Id = a.Id,
                                                                                      ActionAreaName = a.VillageID != null ? string.Empty : pa.ActionArea.Name,
                                                                                      SubActionAreaName = a.VillageID != null ? string.Empty : pa.SubActionArea.Name,
                                                                                      ActivityName = pa.Activity.Name,
                                                                                      ActivityId = pa.Activity.Id,
                                                                                      LocationID = a.LocationID,
                                                                                      ProgramID = a.ProgramID,
                                                                                      CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                                                      ActivityLevel = pc.ActivityLevel,
                                                                                      VillageID = a.VillageID,
                                                                                      VillageName = a.VillageID == null ? string.Empty : a.Village.Name,
                                                                                      UnitOfMeasuresName = a.UnitOfMeasuresID == null ? string.Empty : a.UnitOfMeasures.Name,
                                                                                      UnitOfMeasuresID = a.UnitOfMeasuresID,
                                                                                      PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                      TotalUnits = a.TotalUnits,
                                                                                      UnitCost = a.UnitCost,
                                                                                      TotalUnitCost = a.TotalUnitCost,
                                                                                  
                                                                                     

                                                                                      Quarter1WisePlan = (from qa in prgQuarterUnitMap.Where(qa => qa.ProgramID == a.ProgramID && qa.QuarterId == 1 && qa.CreatorUserId == gcs.Key.ManagerID
                                                                                                          && qa.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa.VillageID == a.VillageID
                                                                                                          && qa.ProgramQuaterYear == Year)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id && su.CostEstimationYear == Year)
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == 0 ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == 0 ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                activityname = s.PrgActionAreaActivityMapping.Activity.Name,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                                    _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                                LocatioName = _LocationRepository.GetAll().Where(ss => ss.Id == s.IteamLocationId).FirstOrDefault().Name,
                                                                                                                                                VillageName = a.VillageID == null ? string.Empty : a.Village.Name,

                                                                                                                                            }).ToList(),
                                                                                                          }).OrderBy(pe => pe.ActivityName).ThenBy(xx => xx.ActivityId).ToList(),
                                                                                      Quarter2WisePlan = (from qa in prgQuarterUnitMap.Where(qa1 => qa1.ProgramID == a.ProgramID && qa1.CreatorUserId == gcs.Key.ManagerID
                                                                                                          && qa1.QuarterId == 2 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID && qa1.ProgramQuaterYear == Year)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id && su.CostEstimationYear == Year)
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == 0 ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == 0 ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                   _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                                activityname = s.PrgActionAreaActivityMapping.Activity.Name,
                                                                                                                                                LocatioName = _LocationRepository.GetAll().Where(ss => ss.Id == s.IteamLocationId).FirstOrDefault().Name,
                                                                                                                                                VillageName = a.VillageID == null ? string.Empty : a.Village.Name,
                                                                                                                                            }).ToList(),
                                                                                                          }).OrderBy(pe => pe.ActivityName).ThenBy(xx => xx.ActivityId).ToList(),
                                                                                      Quarter3WisePlan = (from qa in prgQuarterUnitMap.Where(qa1 => qa1.ProgramID == a.ProgramID && qa1.CreatorUserId == gcs.Key.ManagerID
                                                                                                          && qa1.QuarterId == 3 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID && qa1.ProgramQuaterYear == Year)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id && su.CostEstimationYear == Year)
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == 0 ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == 0 ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                   _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                                activityname = s.PrgActionAreaActivityMapping.Activity.Name,
                                                                                                                                                LocatioName = _LocationRepository.GetAll().Where(ss => ss.Id == s.IteamLocationId).FirstOrDefault().Name,
                                                                                                                                                VillageName = a.VillageID == null ? string.Empty : a.Village.Name,
                                                                                                                                            }).ToList(),
                                                                                                          }).OrderBy(pe => pe.ActivityName).ThenBy(xx => xx.ActivityId).ToList(),
                                                                                      Quarter4WisePlan = (from qa in prgQuarterUnitMap.Where(qa1 => qa1.ProgramID == a.ProgramID && qa1.CreatorUserId == gcs.Key.ManagerID
                                                                                                          && qa1.QuarterId == 4 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID && qa1.ProgramQuaterYear == Year)
                                                                                                          select new ListActionSubactionListImplemenTationDto
                                                                                                          {
                                                                                                              UnitId = qa.Id,
                                                                                                              Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                              Quarter1FinancialRs = qa.Rs,
                                                                                                              ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                              RemainingSubActivity = mapSubActivity.Count(ma => ma.PrgActionAreaActivityMappingID == qa.PrgActionAreaActivityMappingID) - impPlanCheckList.Count(im => im.ProgramQuqterUnitMappingID == qa.Id),
                                                                                                              subUnitActivityQuarterWise = (from s in impPlanCheckList.Where(su => su.ProgramQuqterUnitMappingID == qa.Id && su.CostEstimationYear == Year)
                                                                                                                                            select new subUnitActivityQuarterWise
                                                                                                                                            {
                                                                                                                                                UnitOfMeasureName = s.UnitOfMeasuresID == 0 ? string.Empty : s.UnitOfMeasures.Name,
                                                                                                                                                AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId)?.FullName,
                                                                                                                                                ChecklistItemName = s.ChecklistItemID == 0 ? string.Empty : s.ChecklistItem.Name,
                                                                                                                                                ChecklistItemID = s.ChecklistItemID,
                                                                                                                                                Description = s.Description,
                                                                                                                                                Units = s.Units,
                                                                                                                                                AchieveUnits = s.AchieveUnits,
                                                                                                                                                CheklistStartDate = s.CheklistStartDate,
                                                                                                                                                CheklistEndDate = s.CheklistEndDate,
                                                                                                                                                ImplementationPlanCheckListID = s.Id,
                                                                                                                                                SubActivityStatus = s.status,
                                                                                                                                                CreatorUserId = s.CreatorUserId,
                                                                                                                                                Id = s.ChecklistItem.Id,
                                                                                                                                                ProgramID = s.Program.Id,
                                                                                                                                                CostEstimationYear = s.CostEstimationYear,
                                                                                                                                                QuarterID = s.PrgQuarter,
                                                                                                                                                ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                                PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMappingID,
                                                                                                                                                ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId)?.FullName,
                                                                                                                                                RemainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).TotalUnitCost -
                                                                                                                                                   _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                                UnitOfMeasuresID = s.UnitOfMeasuresID,
                                                                                                                                                AssingUserId = s.AssingUserId,
                                                                                                                                                activityname = s.PrgActionAreaActivityMapping.Activity.Name,
                                                                                                                                                LocatioName = _LocationRepository.GetAll().Where(ss => ss.Id == s.IteamLocationId).FirstOrDefault().Name,
                                                                                                                                                VillageName = a.VillageID == null ? string.Empty : a.Village.Name,
                                                                                                                                            }).ToList(),
                                                                                                          }).OrderBy(pe => pe.ActivityName).ThenBy(xx => xx.ActivityId).ToList(),

                                                                                      Managers = (from u in UserManager.Users
                                                                                                  join ld in _LocationRepository.GetAll().Where(lc => lc.Id == gcs.Key.LocationID.Value)
                                                                                                  on true equals true
                                                                                                  where (ld.Id == u.LocationId.Value || ld.ParentLocation == u.LocationId.Value)
                                                                                                  select new PrgManagerImplentationListDto()
                                                                                                  {
                                                                                                      Name = u.FullName,
                                                                                                      ManagerId = u.Id,
                                                                                                  }).ToList(),


                                                                                      SubActivityandIteams = (from ch in _checkListRepository.GetAll().Include(t => t.Activity).Where(c => c.ActivityID == pa.Activity.Id)
                                                                                                              select new SubActivityandIteams()
                                                                                                              {
                                                                                                                  ChecklistID = ch.Id,
                                                                                                                  ChecklistName = ch.ChecklistName,
                                                                                                                  Description = ch.Description,
                                                                                                                  ActivityID = ch.ActivityID,
                                                                                                                  ActivityName = ch.ActivityID == 0 ? string.Empty : ch.Activity.Name,
                                                                                                                  ProgramID = p.Id,
                                                                                                                  PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                                                  Selectediteams = new List<cheklistandSublistDto1>(),
                                                                                                                  iteam = (from chI in _CheklistItemRepository.GetAll().Include(t => t.Checklist).Where(ci => ci.ChecklistID == ch.Id)
                                                                                                                           select new cheklistandSublistDto1()
                                                                                                                           {
                                                                                                                               CheklisiteamtName = chI.Name,
                                                                                                                               checklistID_subiteamTable = chI.Id,
                                                                                                                           }).ToList(),
                                                                                                              }).ToList(),

                                                                                  }).OrderBy(pe => pe.ActivityName).ThenBy(pee => pee.ActivityId).ToList(),
                                                       }).OrderBy(pe => pe.ActionSubactionCost.OrderBy(zz => zz.ActivityName)).ToList(),

                             }).ToList();

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }


            var d = new List<ImplementationListDto>(ObjectMapper.Map<List<ImplementationListDto>>(Query));
            var rr = new List<subUnitActivityQuarterWise>();
            var subUnitActivityQuarterWisedetails = new List<subUnitActivityQuarterWisedetails>();
            var subUnitActivityQuarterWises1 = new List<subUnitActivityQuarterWise>();
            var subUnitActivityQuarterWises2 = new List<subUnitActivityQuarterWise>();
            var subUnitActivityQuarterWises3 = new List<subUnitActivityQuarterWise>();

            var subUnitActivityQuarterWises4 = new List<subUnitActivityQuarterWise>();


            foreach (var item in d)
            {
                foreach (var item1 in item.CostEstimationYear)
                {
                    foreach (var item2 in item1.ActionSubactionCost)
                    {
                        foreach (var item3 in item2.Quarter1WisePlan)
                        {
                            foreach (var item4 in item3.subUnitActivityQuarterWise)
                            {
                                subUnitActivityQuarterWises1.Add(item4);
                            }
                        }
                        foreach (var item3 in item2.Quarter2WisePlan)
                        {
                            foreach (var item4 in item3.subUnitActivityQuarterWise)
                            {
                                subUnitActivityQuarterWises2.Add(item4);
                            }
                        }
                        foreach (var item3 in item2.Quarter3WisePlan)
                        {
                            foreach (var item4 in item3.subUnitActivityQuarterWise)
                            {
                                subUnitActivityQuarterWises3.Add(item4);
                            }
                        }
                        foreach (var item3 in item2.Quarter4WisePlan)
                        {
                            foreach (var item4 in item3.subUnitActivityQuarterWise)
                            {
                                subUnitActivityQuarterWises4.Add(item4);
                            }
                        }


                    }
                }
            }
            subUnitActivityQuarterWisedetails subUnitActivityQuarterWisedetailss = new subUnitActivityQuarterWisedetails();

            subUnitActivityQuarterWisedetailss.subUnitActivityQuarterWises1 = subUnitActivityQuarterWises1;
            subUnitActivityQuarterWisedetailss.subUnitActivityQuarterWises2 = subUnitActivityQuarterWises2;
            subUnitActivityQuarterWisedetailss.subUnitActivityQuarterWises3 = subUnitActivityQuarterWises3;
            subUnitActivityQuarterWisedetailss.subUnitActivityQuarterWises4 = subUnitActivityQuarterWises4;
            //var subUnitActivityQuarters= new List<subUnitActivityQuarterWisedetails>(ObjectMapper.Map<List<subUnitActivityQuarterWisedetails>>(subUnitActivityQuarterWisedetailss));
            return subUnitActivityQuarterWisedetailss;

        }

        public List<ImplimentaionChecklistDto> GetimplimentaionPlanQuaterWise(GetImplimantionPlanQuaterWiseDTo input)
        {
            var query = (from plan in _ImplementationPlanCheckListRepository.GetAll()
                         .Where(a => a.ProgramID == input.programId && a.ProgramQuqterUnitMappingID == input.QuqterID && a.ProgramLocationId == input.locationID
                          && a.PrgActionAreaActivityMappingID == input.prgActionAreaActivityMappingID)
                         join u in UserManager.Users on plan.AssingUserId equals u.Id
                         join chi in _CheklistItemRepository.GetAll() on plan.ChecklistItemID equals chi.Id
                         join un in _UnitofMeasureRepository.GetAll() on plan.UnitOfMeasuresID equals un.Id

                         select new ImplimentaionChecklistDto()
                         {
                             ProgramID = plan.ProgramID,
                             ChecklistID = plan.ChecklistID,
                             ChecklistItemID = plan.ChecklistItemID,
                             ChekListIteamName = chi.Name,
                             FullName = u.FullName,
                             unitName = un.Name,
                             ProgramQuqterUnitMappingID = plan.ProgramQuqterUnitMappingID,
                             ProgramLocationId = plan.ProgramLocationId,
                             PrgActionAreaActivityMappingID = plan.PrgActionAreaActivityMappingID,

                             LocationId = plan.IteamLocationId,
                             AssingUserId = plan.AssingUserId,
                             UnitOfMeasuresID = plan.UnitOfMeasuresID,
                             Units = plan.Units,
                             Description = plan.Description,
                             IteamLocationId = plan.IteamLocationId,
                             CostEstimationYear = plan.CostEstimationYear,
                             CheklistStartDate = plan.CheklistStartDate,
                             CheklistEndDate = plan.CheklistEndDate
                         }).ToList();
            return new List<ImplimentaionChecklistDto>(ObjectMapper.Map<List<ImplimentaionChecklistDto>>(query));
        }

        public List<cheklistandSublistDto1> GetMapsubActivityForImplentionPlan(int PrgActionAreaActivityMappingID)
        {
            var query = (from sub in _MapSubActivityRepository.GetAll()
                         where (sub.PrgActionAreaActivityMappingID == PrgActionAreaActivityMappingID)
                         join ite in _CheklistItemRepository.GetAll()
              on sub.ChecklistItemID equals ite.Id
                         select new cheklistandSublistDto1()
                         {
                             Id = sub.Id,
                             checklistID_subiteamTable = ite.Id,
                             CheklisiteamtName = ite.Name
                         }).ToList();
            return query;
        }

        public async Task MapSubActivityToImplimentionPlan(SubActivityandIteams input)
        {

            if (input.Id == 0 || input.Id == null)
            {
                foreach (var item in input.Selectediteams)
                {
                    var query = _MapSubActivityRepository.GetAll().Where(maps => maps.ProgramID == input.ProgramID && maps.PrgActionAreaActivityMappingID == input.PrgActionAreaActivityMappingID && maps.ChecklistItemID == item.checklistID_subiteamTable).ToList();
                    if (query.Count() == 0)
                    {
                        var map = new MapSubActivityIteamsToImplementionPlan();
                        map.ChecklistID = input.ChecklistID;
                        map.PrgActionAreaActivityMappingID = input.PrgActionAreaActivityMappingID;
                        map.ProgramID = input.ProgramID;
                        map.ChecklistItemID = item.checklistID_subiteamTable;

                       await _MapSubActivityRepository.InsertAsync(map);
                    }
                }
            }
            else
            {

                var Maping =await _MapSubActivityRepository.FirstOrDefaultAsync((int)input.Id);
                ObjectMapper.Map(input, Maping);
            }
        }


        public List<ListActionSubactionListImplemenTationDto> MapSubActivityToImplimentionPlangetselected(int ProgramID, int ChecklistID)
        {

            var query = (from qa in _MapSubActivityRepository.GetAll().Where(maps => maps.ProgramID == ProgramID && maps.ChecklistID == ChecklistID)

                         select new ListActionSubactionListImplemenTationDto
                         {
                             ChecklistID = qa.ChecklistItemID

                         }).ToList();
            return query;
        }

        public List<ListActionSubactionListImplemenTationDto> GetQuarterWisePlan(int ProgramId, int QuarterId, int PrgActionMapId, int? villageId)
        {
            if (villageId == 0)
            {
                villageId = null;
            }
            var query = (from qa in _ProgramQuqterUnitMappingRepository.GetAll().Where(qa1 => qa1.ProgramID == ProgramId && qa1.QuarterId == QuarterId && qa1.PrgActionAreaActivityMappingID == PrgActionMapId && qa1.VillageID == villageId)
                         select new ListActionSubactionListImplemenTationDto
                         {
                             UnitId = qa.Id,
                             Quarter1PhysicalUnits = qa.NosOfUnit,
                             Quarter1FinancialRs = qa.Rs,
                             ProgramQuarterUnitMappingStatus = qa.Status,
                             subUnitActivityQuarterWise = (from s in _ImplementationPlanCheckListRepository.GetAll().Where(su => su.ProgramQuqterUnitMappingID == qa.Id)
                                                           select new subUnitActivityQuarterWise
                                                           {
                                                               UnitOfMeasureName = s.UnitOfMeasures.Name,
                                                               AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId).FullName,
                                                               ChecklistItemName = s.ChecklistItem.Name,
                                                               ChecklistItemID = s.ChecklistItemID,
                                                               Description = s.Description,
                                                               Units = s.Units,
                                                               CheklistStartDate = s.CheklistStartDate,
                                                               CheklistEndDate = s.CheklistEndDate,
                                                               ImplementationPlanCheckListID = s.Id,
                                                               SubActivityStatus = s.status,
                                                               CreatorUserId = s.CreatorUserId,
                                                               Id = s.ChecklistItem.Id,
                                                               ProgramID = s.Program.Id,
                                                               CostEstimationYear = s.CostEstimationYear,
                                                               QuarterID = s.PrgQuarter,
                                                               ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                               PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMapping.Id,
                                                               ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId).FullName
                                                           }).ToList(),
                         }).ToList();


            return query;
        }

        public List<subUnitActivityQuarterWise> GetSubActivityUnitWise(int unitID)
        {
            var query = (from s in _ImplementationPlanCheckListRepository.GetAll().Where(su => su.ProgramQuqterUnitMappingID == unitID)
                         select new subUnitActivityQuarterWise
                         {
                             UnitOfMeasureName = s.UnitOfMeasures.Name,
                             AssignedUserName = UserManager.Users.FirstOrDefault(u => u.Id == s.AssingUserId).FullName,
                             ChecklistItemName = s.ChecklistItem.Name,
                             ChecklistItemID = s.ChecklistItemID,
                             Description = s.Description,
                             Units = s.Units,
                             CheklistStartDate = s.CheklistStartDate,
                             CheklistEndDate = s.CheklistEndDate,
                             ImplementationPlanCheckListID = s.Id,
                             SubActivityStatus = s.status,
                             CreatorUserId = s.CreatorUserId,
                             Id = s.ChecklistItem.Id,
                             ProgramID = s.Program.Id,
                             CostEstimationYear = s.CostEstimationYear,
                             QuarterID = s.PrgQuarter,
                             ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                             PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMapping.Id,
                             ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == s.CreatorUserId).FullName,
                             UnitOfMeasuresID = s.UnitOfMeasuresID,
                             AssingUserId = s.AssingUserId,
                         }).ToList();
            return query;
        }

        public void DeleteUnit(EntityDto<int> Input)
        {
            _ProgramQuqterUnitMappingRepository.Delete(Input.Id);
            var query = _ImplementationPlanCheckListRepository.GetAll().Where(i => i.ProgramQuqterUnitMappingID == Input.Id).ToList();
            foreach (var item in query)
            {
                _ImplementationPlanCheckListRepository.Delete(item.Id);
            }
            var query1 = _ProgramExpenseRepository.GetAll().Where(pe => pe.ProgramQuqterUnitMappingID == Input.Id).ToList();
            foreach (var item1 in query1)
            {
                _ProgramExpenseRepository.Delete(item1.Id);
            }
        }

        public decimal? GetRemaingBalance(int PrgActionAreaActivityMapping, int Program)
        {
            var remainingBalance = _RequestAAplasRepository.FirstOrDefault(re => re.PrgActionAreaActivityMappingID == PrgActionAreaActivityMapping && re.ProgramID == Program)
                  .TotalUnitCost - _ProgramExpenseRepository.GetAll()
                  .Where(pq => pq.PrgActionAreaActivityMappingID == PrgActionAreaActivityMapping && pq.ProgramID == Program)
                  .Select(t => t.Amount).DefaultIfEmpty(0).Sum();
            return remainingBalance;
        }

        public string UpdateSubActivityQuestionaryStatus(List<UpdateQuestionaryStatus> Input)
        {
            string msg = "";
            foreach (var item in Input)
            {
                prgImplementationPlanQuestionariesMapping prgQ = new prgImplementationPlanQuestionariesMapping();

                var query = _prgImplementationPlanQuestionariesMappingRepository.FirstOrDefault(id => id.Id == item.QuestionarymappngId);
                if (prgImplementationPlanQuestionariesMappingStatus.Approved == item.Status)
                {
                    query.Status = prgImplementationPlanQuestionariesMappingStatus.Approved;
                    msg = "Approved";
                }
                else if (prgImplementationPlanQuestionariesMappingStatus.Reject == item.Status)
                {
                    query.Status = prgImplementationPlanQuestionariesMappingStatus.Reject;
                    msg = "Reject";
                }
                _prgImplementationPlanQuestionariesMappingRepository.Update(query);

                if (msg == "Approved")
                {
                    string value = _LoginAndroidService.UpdateSubActivityUnits(Input[0].implementaionID, Input[0].unit);
                }
                else
                {
                    string value = _LoginAndroidService.UpdateSubActivityUnits(Input[0].implementaionID, 0);
                }
            }
            
            return msg;
        }

        public void DeleteImplimentationChecklist(EntityDto<int> Input)
        {
            _ImplementationPlanCheckListRepository.Delete(Input.Id);
            //throw new NotImplementedException();
        }

        

        public void UpdatecommunitycontributionReceived(int Id, int ProgramId, string costEstimationYear, decimal CommunitycontributionRecevied, decimal GovernmentcontributionRecevied)
        {
            string msg = "";
            var gov = new RequestAAplas();
            var item = _RequestAAplasRepository.FirstOrDefault(i => i.Id == Id && i.ProgramID == ProgramId);

            if (item.CommunityContributionRecived == null)
            {
                item.CommunityContributionRecived = 0;
            }
            if (item.GovernmentContributionRecived == null)
            {
                item.GovernmentContributionRecived = 0;
            }
            item.CommunityContributionRecived = CommunitycontributionRecevied + item.CommunityContributionRecived;
            item.GovernmentContributionRecived = GovernmentcontributionRecevied + item.GovernmentContributionRecived;
            _RequestAAplasRepository.Update(item);
        }

        

        public List<RequestAAPlasListDto> GetDetailsforQuarterWise(int quarterId)
        {
            var query = (from p in _ProgramQuqterUnitMappingRepository.GetAll()
                         where (p.QuarterId == quarterId)

                         select new RequestAAPlasListDto()
                         {
                             ProgramID = p.ProgramID
                         }).ToList();
            return query;
        }

        public void UpdateActivityStatus(int id) {
           var item =  _ProgramQuqterUnitMappingRepository.FirstOrDefault(x => x.Id == id);
            if (item.Status == ProgramQuarterUnitMappingStatus.Inprogress) {
                item.Status = ProgramQuarterUnitMappingStatus.Completed;
            }
            else if (item.Status == ProgramQuarterUnitMappingStatus.Completed) {
                item.Status = ProgramQuarterUnitMappingStatus.Inprogress;
            }
            



            _ProgramQuqterUnitMappingRepository.Update(item);
            var item2 = _ImplementationPlanCheckListRepository.GetAll().Where(x => x.ProgramQuqterUnitMappingID == id);
            foreach(var p in item2) {
                p.status = SubActivityStatus.Completed;
                _ImplementationPlanCheckListRepository.Update(p);
            }
           

        }


        public void UpdateQuarterUnitsById(ListActionSubactionListImplemenTationDto item)
        {

            var obj = _RequestAAplasRepository.FirstOrDefault(x => x.Id == item.Id);
            {
                obj.Quarter1PhysicalUnits = item.Quarter1PhysicalUnits;
                obj.Quarter1FinancialRs = item.Quarter1FinancialRs;
                obj.Quarter2PhysicalUnits = item.Quarter2PhysicalUnits;
                obj.Quarter2FinancialRs = item.Quarter2FinancialRs;
                obj.Quarter3PhysicalUnits = item.Quarter3PhysicalUnits;
                obj.Quarter3FinancialRs = item.Quarter3FinancialRs;
                obj.Quarter4PhysicalUnits = item.Quarter4PhysicalUnits;
                obj.Quarter4FinancialRs = item.Quarter4FinancialRs;
            }
            _RequestAAplasRepository.Update(obj);
        }
    }
}

