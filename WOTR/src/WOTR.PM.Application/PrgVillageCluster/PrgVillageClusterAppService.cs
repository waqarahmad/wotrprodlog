﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Authorization.Users.Dto;
using WOTR.PM.Locations;
using WOTR.PM.Locations.Dto;
using WOTR.PM.PrgVillageCluster.Dto;
using WOTR.PM.Programs;
using WOTR.PM.States;
using Abp.Collections.Extensions;
using Abp.Extensions;
using Abp.Authorization.Users;

namespace WOTR.PM.PrgVillageCluster
{
    public class PrgVillageClusterAppService : PMAppServiceBase, IPrgVillageClusterAppService
    {
        private readonly IRepository<Program> _programRepository;

        private readonly IRepository<VillageCluster> _villageClusterRepository;
        private readonly IRepository<Location> _LocationRepository;
        private readonly IRepository<Village> _VillageRepository;
        private readonly IRepository<Role> _abproleRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;

        public PrgVillageClusterAppService(IRepository<Program> programRepository, IRepository<VillageCluster> villageClusterRepository,
            IRepository<Location> LocationRepository,
            IRepository<Village> VillageRepository,            
            IRepository<Role> abproleRepository, IRepository<UserRole, long> userRoleRepository)
        {
            _programRepository = programRepository;
            _villageClusterRepository = villageClusterRepository;
            _abproleRepository = abproleRepository;
            _LocationRepository = LocationRepository;
            _VillageRepository = VillageRepository;
            _userRoleRepository = userRoleRepository;
        }



        public List<PrgManagersListDto> GetAllProgrameRRcInchargeAndProjectManager(int RRcId)
        {
            var result = new List<PrgManagersListDto>();
            var projectManagerId = _abproleRepository.FirstOrDefault(r => r.Name == "ProjectManager").Id;
            var rrcInchargeId = _abproleRepository.FirstOrDefault(r => r.Name == "RRcIncharge").Id;
            var projectManagerList = ObjectMapper.Map<List<UserListDto>>(from u in UserManager.Users.Where(u => u.LocationId == RRcId)
                                                                         join ur in _userRoleRepository.GetAll()
                                                                         on u.Id equals ur.UserId
                                                                         where ur.RoleId == projectManagerId
                                                                         select u);
            var rrcInchargeList = ObjectMapper.Map<List<UserListDto>>(from u in UserManager.Users.Where(u => u.LocationId == RRcId)
                                                                      join ur in _userRoleRepository.GetAll()
                                                                      on u.Id equals ur.UserId
                                                                      where ur.RoleId == rrcInchargeId
                                                                      select u);

            foreach (var item in projectManagerList)
            {
                item.UserRoleId = _userRoleRepository.FirstOrDefault(t => t.UserId == item.Id && t.RoleId == projectManagerId)?.Id;
            }

            foreach (var item1 in rrcInchargeList)
            {
                item1.UserRoleId = _userRoleRepository.FirstOrDefault(t => t.UserId == item1.Id && t.RoleId == rrcInchargeId)?.Id;
            }

            result.Add(new PrgManagersListDto
            {
                RRcIncharge = rrcInchargeList,
                ProjectManagers = projectManagerList

            });

            return result;
        }

        public async Task<List<PrgVillageClusterListDto>> GetAllVillageCluster(int programeId, string input)
        {
            var projectManagerId = _abproleRepository.FirstOrDefault(r => r.Name == "ProjectManager").Id;
            var rrcInchargeId = _abproleRepository.FirstOrDefault(r => r.Name == "RRcIncharge").Id;

            var query = (from v in _villageClusterRepository.GetAll().Include(t => t.Location).Include(t => t.State)
                         .Include(t => t.District).Include(t => t.Taluka).Include(t => t.User).Include(t => t.UserRole)
                         where (v.ProgramID == programeId)
                         group v by new { v.Location, v.VillageClusterLocationID } into grp
                         select new PrgVillageClusterListDto
                         {
                             Name = grp.FirstOrDefault().Name,
                             Id = grp.FirstOrDefault().Id,
                             RRCID = grp.Key.Location.Id,
                             RRcName = grp.Key.Location.Name,
                             StateName = grp.FirstOrDefault().State.StateName,
                             StateID = grp.FirstOrDefault().StateID,
                             DistrictID = grp.FirstOrDefault().DistrictID,
                             DistrictName = grp.FirstOrDefault().District.DistrictName,
                             TalukaName = grp.FirstOrDefault().Taluka.TalukaName,
                             LocationId = grp.FirstOrDefault().VillageClusterLocationID,
                             Villages = ObjectMapper.Map<List<PrgVillageClusterVillages>>(grp.Where(p => p.RRCID == grp.Key.Location.Id && p.VillageClusterLocationID == grp.FirstOrDefault().VillageClusterLocationID).GroupBy(y => new { y.Village, y.VillageClusterLocationID }).Select(t => t.Key.Village)),
                             Talukas = ObjectMapper.Map<List<PrgVillageClusterTaluka>>(grp.Where(p => p.RRCID == grp.Key.Location.Id && p.VillageClusterLocationID == grp.FirstOrDefault().VillageClusterLocationID).GroupBy(y => new { y.Taluka, y.VillageClusterLocationID }).Select(t => t.Key.Taluka)),
                             RRCC = ObjectMapper.Map<List<PrgManagersListDtos>>(grp.Where(p => p.UserRole.RoleId == projectManagerId && p.RRCID == grp.Key.Location.Id && p.VillageClusterLocationID == grp.FirstOrDefault().VillageClusterLocationID).GroupBy(y => new { y.User }).Select(t => t.Key.User)),
                             RRCInchargelist = ObjectMapper.Map<List<PrgManagersListDtosrrc>>(grp.Where(p => p.UserRole.RoleId == rrcInchargeId && p.RRCID == grp.Key.Location.Id && p.VillageClusterLocationID == grp.FirstOrDefault().VillageClusterLocationID).GroupBy(y => new { y.User }).Select(t => t.Key.User)),
                         });

            var villageCluster = await query.ToListAsync();

            foreach (var item in villageCluster)
            {
                foreach (var item1 in item.RRCC)
                {
                    item1.UsreRoleId = _userRoleRepository.FirstOrDefault(t => t.UserId == item1.Id && t.RoleId == projectManagerId)?.Id;
                }

                foreach (var item2 in item.RRCInchargelist)
                {
                    item2.UsreRoleId = _userRoleRepository.FirstOrDefault(t => t.UserId == item2.Id && t.RoleId == rrcInchargeId)?.Id;
                }
            }
            //if (input != null)
            //{
            //    villageCluster = villageCluster.WhereIf(
            //         !input.IsNullOrEmpty(),
            //         p => p.Name.ToUpper().Contains(input.ToUpper()) || p.Name.ToLower().Contains(input.ToLower()) ||
            //               p.RRcName.ToUpper().Contains(input.ToUpper()) || p.RRcName.ToLower().Contains(input.ToLower())).ToList();
            //}

            return villageCluster;
        }

        public List<PrgVillageClusterListDto> GetAllVillageClusterList(int ProgrameId)
        {
            var result = new List<PrgVillageClusterListDto>();
            var rrcList = new List<PrgVillageClusterListDto>();
            var vcList = new List<PrgVillageClusterListDto>();

            var ListOfRRc = _villageClusterRepository.GetAll().Where(p => p.ProgramID == ProgrameId).ToList().GroupBy(x => new { x.RRCID })
                                        .Select(g => g.First())
                                        .ToList();
            foreach (var rrc in ListOfRRc)
            {
                rrcList.Add(new PrgVillageClusterListDto
                {
                    Name = rrc.Name,
                    Id = rrc.Id,
                    RRCID = rrc.RRCID,
                    RRcName = _LocationRepository.FirstOrDefault(r => r.Id == rrc.RRCID).Name,
                });
            }
            var ListOfVc = _villageClusterRepository.GetAll().Where(p => p.ProgramID == ProgrameId).ToList().GroupBy(x => new { x.RRCID, x.VillageClusterLocationID })
                                        .Select(g => g.First())
                                        .ToList();
            foreach (var vc in ListOfVc)
            {
                vcList.Add(new PrgVillageClusterListDto
                {
                    Name = vc.Name,
                    Id = (int)vc.VillageClusterLocationID,
                    RRCID = vc.RRCID,
                    RRcName = _LocationRepository.GetAll().Where(r => r.Id == vc.RRCID).FirstOrDefault()?.Name,
                });
            }
            result.Add(new PrgVillageClusterListDto
            {
                RRc = rrcList,
                VillageClusterList = vcList
            });


            return result;

        }

        public void Deleteprogramclustor(EntityDto<int> Input)
        {
            _LocationRepository.Delete(Input.Id);

            var query = _villageClusterRepository.GetAll().Where(vc => vc.VillageClusterLocationID == Input.Id).ToList();
            foreach (var item in query)
            {
                _villageClusterRepository.Delete(item.Id);
            }

        }

        public async Task CreatOrUpdateVillageCluster1(PrgVillageClusterListDto Input)
        {
            if (Input.LocationId == 0 || Input.LocationId == null)
            {
                Location VillageCluster = new Location();
                VillageCluster.ParentLocation = (int)Input.RRCID;
                VillageCluster.Name = Input.Name;
                VillageCluster.LocationType = LocationType.VillageCluster;
                int newLocationId = _LocationRepository.InsertAndGetId(VillageCluster);

                await CreateVillageCluster(Input, newLocationId);
            }
            else
            {
                Location VillageCluster = new Location();
                VillageCluster.ParentLocation = (int)Input.RRCID;
                VillageCluster.Id = (int)Input.LocationId;
                VillageCluster.Name = Input.Name;
                VillageCluster.TenantId = (int)AbpSession.TenantId;
                VillageCluster.LocationType = LocationType.VillageCluster;
                await _LocationRepository.UpdateAsync(VillageCluster);

                var cluster = await _villageClusterRepository.GetAll().Where(t => t.VillageClusterLocationID == Input.LocationId).ToListAsync();
                foreach (var item in cluster)
                {
                    await _villageClusterRepository.DeleteAsync(item);
                }
                await CreateVillageCluster(Input, Input.LocationId);
            }            
        }

        public async Task CreateVillageCluster(PrgVillageClusterListDto Input, int? locId)
        {
            int? talukaid;
            foreach (var pmId in Input.PrgClusterUser)
            {
                foreach (var vid in Input.villageID)
                {
                    talukaid = _VillageRepository.GetAll().Where(dd => dd.Id == vid).FirstOrDefault().TalukaId;
                    VillageCluster PrgVillageCluster = new VillageCluster();
                    PrgVillageCluster.ProgramID = Input.ProgramID;
                    PrgVillageCluster.Name = Input.Name;
                    PrgVillageCluster.RRCID = Input.RRCID;
                    PrgVillageCluster.StateID = Input.StateID;
                    PrgVillageCluster.DistrictID = Input.DistrictID;
                    PrgVillageCluster.TalukaID = talukaid;
                    PrgVillageCluster.UserId = pmId.UserId;
                    PrgVillageCluster.UserRoleId = pmId.UserRoleId;
                    PrgVillageCluster.VillageID = vid;
                    PrgVillageCluster.VillageClusterLocationID = locId;
                    await _villageClusterRepository.InsertAsync(PrgVillageCluster);
                }
            }
        }

        public string getprgramname(int ProgramId)
        {
            var Program = _programRepository.FirstOrDefault(x => x.Id == ProgramId);
            return Program.Name;
        }

        public void Deletevillage(int Id, int VillageClustorlocationId, int ProgramId)
        {

            var query = _villageClusterRepository.GetAll().Where(vc => vc.VillageID == Id && vc.VillageClusterLocationID == VillageClustorlocationId && vc.ProgramID == ProgramId).ToList();
            foreach (var item in query)
            {
                _villageClusterRepository.Delete(item.Id);
            }

        }
        public void Deletetalukas(int Id, int VillageClustorlocationId, int ProgramId)
        {

            var query = _villageClusterRepository.GetAll().Where(vc => vc.TalukaID == Id && vc.VillageClusterLocationID == VillageClustorlocationId && vc.ProgramID == ProgramId).ToList();
            foreach (var item in query)
            {
                _villageClusterRepository.Delete(item.Id);
            }

        }

        public void DeleteUser(int userid, int VillageClustorlocationId, int ProgramId)
        {

            var query = _villageClusterRepository.GetAll().Where(vc => vc.UserId == userid && vc.VillageClusterLocationID == VillageClustorlocationId && vc.ProgramID == ProgramId).ToList();
            foreach (var item in query)
            {
                _villageClusterRepository.Delete(item.Id);
            }

        }
        public void deleteTaluka(int takulaID, int VillageClustorlocationId, int ProgramId)
        {

            var query = _villageClusterRepository.GetAll().Where(vc => vc.TalukaID == takulaID && vc.VillageClusterLocationID == VillageClustorlocationId && vc.ProgramID == ProgramId).ToList();
            foreach (var item in query)
            {
                _villageClusterRepository.Delete(item.Id);
            }

        }
    }
}
