﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WOTR.PM.ImpactIndicators.Dto;
using Abp.Domain.Repositories;

namespace WOTR.PM.ImpactIndicators
{
    public class ImpactIndicatorActivityAppService : PMAppServiceBase, IImpactIndicatorActivity
    {

        private readonly IRepository<ImpactIndicatorActivity> _impactindicatorActivityRepository;

        public ImpactIndicatorActivityAppService(IRepository<ImpactIndicatorActivity> impactindicatorActivityRepository)
        {
            _impactindicatorActivityRepository = impactindicatorActivityRepository;
        }





        public List<string> creatImpacatIndicatorActivity(ImpactIndicatorActivityListDto Input)
        {
            ImpactIndicatorActivity impactindicatoractivity = new ImpactIndicatorActivity();
            List<string> result = new List<string>();
            impactindicatoractivity.Name = Input.Name;
            impactindicatoractivity.Description = Input.Description;
            if (Input.Id == 0)
            {
                try
                {
                    int id = _impactindicatorActivityRepository.InsertAndGetId(impactindicatoractivity);
                    if (Convert.ToBoolean(id))

                        result.Add("Record Add Sucessfully");
                }
                catch (Exception e)
                {

                    result.Add(e.ToString());
                }


            }
            else
            {
                _impactindicatorActivityRepository.Update(impactindicatoractivity);
                result.Add("Record UpdateSucessfully");
            }
            return result;
        }

        public  async Task Delete(EntityDto<int> input)
        {
            await _impactindicatorActivityRepository.DeleteAsync(input.Id);
        }

        public List<ImpactIndicatorActivityListDto> GetAll()
        {
            
            var query = (from im in _impactindicatorActivityRepository.GetAll()select new ImpactIndicatorActivityListDto
            {
                Name=im.Name,
                Id=im.Id,
                Description=im.Description
                
            }).ToList().OrderByDescending(im=>im.Name) ;
         
           

            return new List<ImpactIndicatorActivityListDto>(ObjectMapper.Map<List<ImpactIndicatorActivityListDto>>(query));
        }
    }
}
