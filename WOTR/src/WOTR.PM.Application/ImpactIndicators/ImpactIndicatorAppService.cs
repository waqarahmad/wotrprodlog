﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WOTR.PM.ImpactIndicators.Dto;
using Abp.Domain.Repositories;

namespace WOTR.PM.ImpactIndicators
{
    public class ImpactIndicatorAppService : PMServiceBase, IImpactIndicator
    {
        private readonly IRepository<ImpactIndicator> _impactindicatorRepository;

        public ImpactIndicatorAppService(IRepository<ImpactIndicator> impactindicatoryRepository)
        {
            _impactindicatorRepository = impactindicatoryRepository;
        }



        public List<string> creatImpacatIndicator(ImpactIndicatorsListDto Input)
        {
            ImpactIndicator impactindicator = new ImpactIndicator();
           List< string> result=new List<string>();
            impactindicator.Name = Input.Name;
            if (Input.Id==0)
            {
                try
                {
                  int id=  _impactindicatorRepository.InsertAndGetId(impactindicator);
                    if (Convert.ToBoolean(id))
                   
                    result.Add("Record Add Sucessfully");
                }
                catch (Exception e)
                {

                    result.Add(e.ToString());
                }
               

            }
            else
            {
                _impactindicatorRepository.Update(impactindicator);
                result.Add("Record UpdateSucessfully");
            }
            return result;
        }

        public async Task Delete(EntityDto<int> input)
        {
           await _impactindicatorRepository.DeleteAsync(input.Id);
        }

        public List<ImpactIndicatorsListDto> GetAll()
        {
            var query = (from im in _impactindicatorRepository.GetAll()
                         select new ImpactIndicatorsListDto()
                         {
                             Name = im.Name,
                             Id = im.Id,

                         }).ToList().OrderByDescending(im => im.Name);

            return new List<ImpactIndicatorsListDto>(ObjectMapper.Map<List<ImpactIndicatorsListDto>>(query));
        }
    }
}
