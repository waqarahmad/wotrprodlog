﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WOTR.PM.ActionAreas;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Authorization.Users;
using WOTR.PM.EntityFrameworkCore;
using WOTR.PM.Locations;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.PrgQuarterUnits;
using WOTR.PM.PrgRequestAAPlas;
using WOTR.PM.ProgramCostEstimations;
using WOTR.PM.ProgramExpenses;
using WOTR.PM.ProgramFunds.Dto;
using WOTR.PM.Programs;
using WOTR.PM.ProjectFund;
using WOTR.PM.UnitOfMeasures;
using Abp.Authorization.Users;

namespace WOTR.PM.ProgramFunds
{
    public class ProgramFundsAppService : PMAppServiceBase, IProgramFundsAppService
    {
        private readonly IRepository<RequestAAplas> _RequestAAplasRepository;
        private readonly IRepository<Program> _ProgrameRepository;
        private readonly IRepository<Location> _LocationRepository;
        private readonly IRepository<PrgActionAreaActivityMapping> _PrgActionAreaActivityMappingRepository;
        private readonly IRepository<ActionArea> _ActionAreaRepository;
        private readonly IRepository<SubActionArea> _SubActionAreaRepository;
        private readonly IRepository<Activity> _ActivityRepository;
        private readonly IRepository<UnitofMeasure> _UnitofMeasureRepository;
        private readonly IRepository<ProgramCostEstimation> _PrgCostEstRepository;
        private IRepository<Village> _villageRepository;
        private IRepository<ProgramFund> _ProgramFundRepository;
        private IRepository<ProgramQuqterUnitMapping> _ProgramQuqterUnitMappingRepository;
        private IRepository<ProgramExpense> _ProgramExpenseRepository;
        private readonly IRepository<Role> _abproleRepository;
        private readonly IRepository<ProjectFunds> _ProjectFundRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<User, long> _userRepository;
        public ProgramFundsAppService(IRepository<RequestAAplas> RequestAAplasRepository,
             IRepository<Program> ProgrameRepository, IRepository<Location> LocationRepository,
              IRepository<PrgActionAreaActivityMapping> PrgActionAreaActivityMappingRepository, IRepository<ProgramExpense> ProgramExpenseRepository,
               IRepository<ActionArea> ActionAreaRepository, IRepository<UnitofMeasure> UnitofMeasureRepository,
            IRepository<SubActionArea> SubActionAreaRepository, IRepository<Activity> ActivityRepository, IRepository<ProgramFund> ProgramFundRepository,
            IRepository<ProgramCostEstimation> PrgCostEstRepository, IRepository<ProgramQuqterUnitMapping> ProgramQuqterUnitMappingRepository,
            IRepository<Village> villageRepository, IRepository<Role> abproleRepository, IRepository<ProjectFunds> projectFundRepository,
            IRepository<UserRole, long> userRoleRepository, IRepository<Role> roleRepository, IRepository<User, long> userRepository)
        {
            _RequestAAplasRepository = RequestAAplasRepository;
            _ProgrameRepository = ProgrameRepository;
            _LocationRepository = LocationRepository;
            _PrgActionAreaActivityMappingRepository = PrgActionAreaActivityMappingRepository;
            _ActionAreaRepository = ActionAreaRepository;
            _SubActionAreaRepository = SubActionAreaRepository;
            _ActivityRepository = ActivityRepository;
            _UnitofMeasureRepository = UnitofMeasureRepository;
            _PrgCostEstRepository = PrgCostEstRepository;
            _villageRepository = villageRepository;
            _ProgramQuqterUnitMappingRepository = ProgramQuqterUnitMappingRepository;
            _ProgramExpenseRepository = ProgramExpenseRepository;
            _ProgramFundRepository = ProgramFundRepository;
            _abproleRepository = abproleRepository;
            _ProjectFundRepository = projectFundRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _userRepository = userRepository;
        }

        public async Task<List<string>> CreateProjectFund(List<ListActionSubactionListImplemenTationDto1> Input)
        {
            var mse = new List<string>();

            var prjFund = new ProjectFunds();

            prjFund.ProgramID = Input.FirstOrDefault().ProgramID;
            prjFund.DemandAmount = Input.Sum(x => x.DemandAmmount);
            prjFund.ProjectManagerID = Input.FirstOrDefault().ManagerID;
            prjFund.Status = Status1.Pending;

            await _ProjectFundRepository.InsertAsync(prjFund);

            foreach (var item in Input)
            {
                var prgFund = new ProgramFund();
                try
                {
                    if (item.PrgActionAreaActivityMappingID != null)
                    {
                        prgFund.ProgramID = item.ProgramID;
                        prgFund.Amount = (decimal)item.DemandAmmount;
                        prgFund.Unit = item.DemandUnits;
                        prgFund.ProgramManagerID = item.ProgramManagerID;
                        prgFund.ProjectManagerID = item.ManagerID;
                        prgFund.PrgActionAreaActivityMappingID = item.PrgActionAreaActivityMappingID;
                        prgFund.Status = Status1.Pending;
                        prgFund.UnitOfMeasuresID = item.UnitOfMeasuresID;
                        prgFund.CostEstimationYear = item.CostEstimationYear;
                        await _ProgramFundRepository.InsertAsync(prgFund);
                        mse.Add("SAVE");
                    }
                    else
                    {
                        await _ProjectFundRepository.UpdateAsync(prjFund);
                        mse.Add("uPDATE");
                    }
                }
                catch (Exception e)
                {
                    mse.Add(e.ToString());
                }
            }
            return mse;
        }

        public async Task<List<string>> UpdateProjectFundStatus1(int id, Status1 prgstatus)
        {
            var mse = new List<string>();
            var qu = await _ProjectFundRepository.FirstOrDefaultAsync(r => r.Id == id);
            qu.Status = prgstatus;
            await _ProjectFundRepository.UpdateAsync(qu);
            mse.Add("uPDATE");
            return mse;
        }

        public List<string> UpdateProjectFundStatus(int id, Status1 prgstatus)
        {
            var mse = new List<string>();
            var qu = _ProgramFundRepository.GetAll().Where(r => r.Id == id).ToList();
            foreach (var fund in qu)
            {
                fund.Status = prgstatus;
                _ProgramFundRepository.Update(fund);
                mse.Add("uPDATE");
            }
            return mse;
        }


        public List<ProgramFundsListDto> GetProjectFundsForProgramManager()
        {
            var query = (from pf in _ProgramFundRepository.GetAll()
                         group pf by new
                         {
                             pf.Program,
                         } into g
                         select new ProgramFundsListDto
                         {

                             ProgramName = g.Key.Program.Name,
                             prgFundDetails = (from pf in _ProgramFundRepository.GetAll().Where(pf => pf.ProjectManagerID == g.Key.Program.Id)
                                               select new prgFundDetails
                                               {
                                                   ProgramManagerName = pf.ProgramManager.Name,
                                                   ProjectManagerName = pf.ProjectManager.Name,
                                                   ProgramName = pf.Program.Name,
                                                   DemandAmount = pf.Amount,
                                                   DemandDate = pf.CreationTime,
                                                   Status1 = pf.Status,
                                                   Id = pf.Id
                                               }).ToList()
                         }).ToList();
            return query;

        }
        public List<ProgramFundsListDto> GetProjectFundsForAccountant()
        {
            var query = (from pf in _ProgramFundRepository.GetAll().Where(p => p.Status == Status1.Pending || p.Status == Status1.FundRelease)
                         group pf by new
                         {
                             pf.Program,
                         } into g
                         select new ProgramFundsListDto
                         {

                             ProgramName = g.Key.Program.Name,
                             prgFundDetails = (from pf in _ProgramFundRepository.GetAll().Where(pf => pf.ProgramID == g.Key.Program.Id && (pf.Status == Status1.Pending || pf.Status == Status1.FundRelease))
                                               select new prgFundDetails
                                               {
                                                   ProgramManagerName = g.FirstOrDefault().ProgramManager.Name,
                                                   ProjectManagerName = g.FirstOrDefault().ProjectManager.Name,
                                                   ProgramName = g.FirstOrDefault().Program.Name,
                                                   DemandAmount = g.Sum(x => x.Amount),
                                                   DemandDate = g.FirstOrDefault().CreationTime,
                                                   Status1 = g.FirstOrDefault().Status,
                                                   Id = g.Key.Program.Id
                                               }).Take(1).ToList()
                         }).ToList();

            return query.ToList();

        }


        private List<ProgramFundsListDto> ConditionCheck(List<ProgramFundsListDto> hh)
        {
            var d = hh.GroupBy(o => o.ProgramID);
            hh = new List<ProgramFundsListDto>();
            d.ToList().ForEach(f => hh.Add(f.FirstOrDefault()));
            return hh;
        }

        public async Task<List<ProgramFundsListDto>> GetProjectFundsForAdmin()
        {
            var managerId = (long)AbpSession.UserId;
            var programtManagerRoleId = await _abproleRepository.FirstOrDefaultAsync(a => a.DisplayName == "Program Manager");
            var query1 = UserManager.Users.FirstOrDefault(r => r.Id == managerId)?.UserRole;
            var hh = new List<ProgramFundsListDto>();

            var query = await _ProjectFundRepository
                .GetAllIncluding(k => k.Program, k => k.ProjectManager, k => k.Program.ProgramManager)
                .Where(p => p.Status == Status1.Pending || p.Status == Status1.FundRelease).ToListAsync();
            if (query1 == programtManagerRoleId.Id)
            {
                query = query.Where(p => p.Program.CreatorUserId == managerId || p.Program.ProgramManager.Id == managerId).ToList();
            }


            var d = (from p in query
                     orderby p.ReleaseDate descending
                     group p by new
                     {
                         p.Program,
                         p.ProjectManager
                     } into gg
                     select gg).ToList();

            d.ForEach(k =>
            {
                ProgramFundsListDto programFundsListDto = null;
                if (hh.Find(l => l.ProgramID == k.Key.Program.Id) != null)
                {
                    programFundsListDto = (from l in hh
                                           where l.ProgramID == k.Key.Program.Id
                                           select l).SingleOrDefault();
                }
                else
                {
                    programFundsListDto = new ProgramFundsListDto()
                    {
                        ProgramName = k.Key.Program.Name,
                        prgFundDetails = new List<prgFundDetails>(),
                        ProgramID = k.Key.Program.Id,
                    };
                }
                var pp = new prgFundDetails();
                var w = k.OrderByDescending(q => q.CreationTime).FirstOrDefault();
                pp.DemandAmount = w.DemandAmount;
                pp.ProjectManagerID = w.ProjectManagerID;
                pp.ReleaseAmount = w.ReleaseAmount;
                pp.ProjectManagerName = w.ProjectManager == null ? null : w.ProjectManager.Name;
                pp.ProgramName = w.Program.Name;
                pp.ReleaseDate = w.ReleaseDate;
                pp.Status1 = w.Status;
                pp.Id = w.ProgramID;
                pp.ReleaseList = new List<prgFundDetails>();
                pp.ProgramManagerName = w.Program.ProgramManager == null ? null : w.Program.ProgramManager.Name;

                k.ToList().ForEach(j =>
                {
                    if (j.ReleaseAmount != null)
                    {
                        var prgFundDetails = new prgFundDetails
                        {
                            ReleaseAmount = j.ReleaseAmount,
                            ReleaseDate = j.ReleaseDate
                        };
                        pp.ReleaseList.Add(prgFundDetails);
                    }
                });
                if (hh.Find(h => h.ProgramID == w.ProgramID) != null)
                {
                    programFundsListDto.prgFundDetails = hh.Find(h => h.ProgramID == w.ProgramID).prgFundDetails;
                }
                programFundsListDto.prgFundDetails.Add(pp);
                if (hh.Find(h => h.ProgramID == w.ProgramID) == null)
                {
                    hh.Add(programFundsListDto);
                }
            });


            hh = ConditionCheck(hh);

            var d1 = (from p in query
                      group p by new
                      {
                          p.Program,
                          p.ProjectManager
                      } into gg
                      select gg).ToList();

            d1.ForEach(k =>
            {
                if (hh.Find(v => v.ProgramID == k.Key.Program.Id) == null)
                {
                    ProgramFundsListDto programFundsListDto = null;
                    if (hh.Find(l => l.ProgramID == k.Key.Program.Id) != null)
                    {
                        programFundsListDto = (from l in hh
                                               where l.ProgramID == k.Key.Program.Id
                                               select l).SingleOrDefault();
                    }
                    else
                    {
                        programFundsListDto = new ProgramFundsListDto()
                        {
                            ProgramName = k.Key.Program.Name,
                            prgFundDetails = new List<prgFundDetails>(),
                            ProgramID = k.Key.Program.Id,
                        };
                    }
                    var pp = new prgFundDetails();
                    var w = k.OrderByDescending(q => q.CreationTime).FirstOrDefault();
                    pp.DemandAmount = k.Sum(j => j.DemandAmount);
                    pp.ProjectManagerID = w.ProjectManagerID;
                    pp.ReleaseAmount = null;
                    pp.ProjectManagerName = w.ProjectManager == null ? null : w.ProjectManager.Name;
                    pp.ProgramName = w.Program.Name;
                    pp.ReleaseDate = null;
                    pp.Status1 = w.Status;
                    pp.Id = w.ProgramID;
                    pp.ReleaseList = new List<prgFundDetails>();
                    pp.ProgramManagerName = w.Program.ProgramManager == null ? null : w.Program.ProgramManager.Name;
                    if (hh.Find(h => h.ProgramID == w.ProgramID) != null)
                    {
                        programFundsListDto.prgFundDetails = hh.Find(h => h.ProgramID == w.ProgramID).prgFundDetails;
                    }
                    programFundsListDto.prgFundDetails.Add(pp);
                    if (hh.Find(h => h.ProgramID == w.ProgramID) == null)
                    {
                        hh.Add(programFundsListDto);
                    }
                }

            });
            return hh;
        }

        public async Task<List<ProgramFundsListDto>> GetProjectFundsForAdminNew(int ProgramId)
        {
            var managerId = (long)AbpSession.UserId;
            var programtManagerRoleId = await _abproleRepository.FirstOrDefaultAsync(a => a.DisplayName == "Program Manager");
            var query1 = UserManager.Users.FirstOrDefault(r => r.Id == managerId)?.UserRole;
            var hh = new List<ProgramFundsListDto>();

            var query = await _ProjectFundRepository
                .GetAllIncluding(k => k.Program, k => k.ProjectManager, k => k.Program.ProgramManager)
                .Where(p => p.ProgramID == ProgramId && p.Status == Status1.Pending || p.Status == Status1.FundRelease  ).ToListAsync();
            if (query1 == programtManagerRoleId.Id)
            {
                query = query.Where(p => p.Program.CreatorUserId == managerId || p.Program.ProgramManager.Id == managerId).ToList();
            }


            var d = (from p in query
                     orderby p.ReleaseDate descending
                     group p by new
                     {
                         p.Program,
                         p.ProjectManager
                     } into gg
                     select gg).ToList();

            d.ForEach(k =>
            {
                ProgramFundsListDto programFundsListDto = null;
                if (hh.Find(l => l.ProgramID == k.Key.Program.Id) != null)
                {
                    programFundsListDto = (from l in hh
                                           where l.ProgramID == k.Key.Program.Id
                                           select l).SingleOrDefault();
                }
                else
                {
                    programFundsListDto = new ProgramFundsListDto()
                    {
                        ProgramName = k.Key.Program.Name,
                        prgFundDetails = new List<prgFundDetails>(),
                        ProgramID = k.Key.Program.Id,
                    };
                }
                var pp = new prgFundDetails();
                var w = k.OrderByDescending(q => q.CreationTime).FirstOrDefault();
                pp.DemandAmount = w.DemandAmount;
                pp.ProjectManagerID = w.ProjectManagerID;
                pp.ReleaseAmount = w.ReleaseAmount;
                pp.ProjectManagerName = w.ProjectManager == null ? null : w.ProjectManager.Name;
                pp.ProgramName = w.Program.Name;
                pp.ReleaseDate = w.ReleaseDate;
                pp.Status1 = w.Status;
                pp.Id = w.ProgramID;
                pp.ReleaseList = new List<prgFundDetails>();
                pp.ProgramManagerName = w.Program.ProgramManager == null ? null : w.Program.ProgramManager.Name;

                k.ToList().ForEach(j =>
                {
                    if (j.ReleaseAmount != null)
                    {
                        var prgFundDetails = new prgFundDetails
                        {
                            ReleaseAmount = j.ReleaseAmount,
                            ReleaseDate = j.ReleaseDate
                        };
                        pp.ReleaseList.Add(prgFundDetails);
                    }
                });
                if (hh.Find(h => h.ProgramID == w.ProgramID) != null)
                {
                    programFundsListDto.prgFundDetails = hh.Find(h => h.ProgramID == w.ProgramID).prgFundDetails;
                }
                programFundsListDto.prgFundDetails.Add(pp);
                if (hh.Find(h => h.ProgramID == w.ProgramID) == null)
                {
                    hh.Add(programFundsListDto);
                }
            });


            hh = ConditionCheck(hh);

            var d1 = (from p in query
                      group p by new
                      {
                          p.Program,
                          p.ProjectManager
                      } into gg
                      select gg).ToList();

            d1.ForEach(k =>
            {
                if (hh.Find(v => v.ProgramID == k.Key.Program.Id) == null)
                {
                    ProgramFundsListDto programFundsListDto = null;
                    if (hh.Find(l => l.ProgramID == k.Key.Program.Id) != null)
                    {
                        programFundsListDto = (from l in hh
                                               where l.ProgramID == k.Key.Program.Id
                                               select l).SingleOrDefault();
                    }
                    else
                    {
                        programFundsListDto = new ProgramFundsListDto()
                        {
                            ProgramName = k.Key.Program.Name,
                            prgFundDetails = new List<prgFundDetails>(),
                            ProgramID = k.Key.Program.Id,
                        };
                    }
                    var pp = new prgFundDetails();
                    var w = k.OrderByDescending(q => q.CreationTime).FirstOrDefault();
                    pp.DemandAmount = k.Sum(j => j.DemandAmount);
                    pp.ProjectManagerID = w.ProjectManagerID;
                    pp.ReleaseAmount = null;
                    pp.ProjectManagerName = w.ProjectManager == null ? null : w.ProjectManager.Name;
                    pp.ProgramName = w.Program.Name;
                    pp.ReleaseDate = null;
                    pp.Status1 = w.Status;
                    pp.Id = w.ProgramID;
                    pp.ReleaseList = new List<prgFundDetails>();
                    pp.ProgramManagerName = w.Program.ProgramManager == null ? null : w.Program.ProgramManager.Name;
                    if (hh.Find(h => h.ProgramID == w.ProgramID) != null)
                    {
                        programFundsListDto.prgFundDetails = hh.Find(h => h.ProgramID == w.ProgramID).prgFundDetails;
                    }
                    programFundsListDto.prgFundDetails.Add(pp);
                    if (hh.Find(h => h.ProgramID == w.ProgramID) == null)
                    {
                        hh.Add(programFundsListDto);
                    }
                }

            });
            return hh;
        }

        public List<ProgramFundsListDto> GetProjectFunds(int ProgramId)
        {
            string finyear = "";
            DateTime dt = Convert.ToDateTime(DateTime.Now.ToString());
            int m = dt.Month;
            int y = dt.Year;
            if (m > 3)
            {
                finyear = y.ToString() + "-" + Convert.ToString((y + 1));
                //get last  two digits (eg: 10 from 2010);
            }
            else
            {
                finyear = Convert.ToString((y - 1)) + "-" + y.ToString();
            }
            string NYear = finyear.Substring(5, 4);

            var managerId = AbpSession.UserId;


            var Query = (from r in _RequestAAplasRepository.GetAll().Where(p => p.RequestStatus == RequestStatus.Approved && p.ManagerID == managerId && p.ProgramID == ProgramId)
                         group r by new
                         {
                             r.LocationID,
                             r.ManagerID,
                             r.ProgramID,
                             r.RequestStatus
                         } into gcs
                         join l in _LocationRepository.GetAll()
                         on gcs.Key.LocationID equals l.Id
                         join p in _ProgrameRepository.GetAll()
                         on gcs.Key.ProgramID equals p.Id


                         select new ProgramFundsListDto()
                         {
                             ProgramName = p.Name,
                             ProgramID = p.Id,
                             ProgrameStartDate = p.ProgramStartDate,
                             ProgrameEndDate = p.PrgramEndDate,
                             LocationID = gcs.Key.LocationID,
                             ManagerID = gcs.Key.ManagerID,
                             ProgramManagerName = "select Program Manager Finance Or Admin Manager",
                             Managers = (from u in UserManager.Users
                                         join ur in _abproleRepository.GetAll()
                                         on u.UserRole equals ur.Id
                                         where (ur.Name == "ProgramManagerFinanceOrAdmin")
                                         select new PrgProgramManagerListDto
                                         {
                                             Name = u.FullName,
                                             ManagerId = u.Id
                                         }).ToList(),
                             prgFundDetails = (from pf in _ProgramFundRepository.GetAll().Where(pf => pf.ProgramID == gcs.Key.ProgramID)
                                               select new prgFundDetails
                                               {
                                                   ProgramManagerName = pf.ProgramManager.Name,
                                                   ProjectManagerName = pf.ProjectManager.Name,
                                                   ProgramName = pf.Program.Name,
                                                   DemandAmount = pf.Amount,
                                                   DemandDate = pf.CreationTime,
                                                   Status1 = pf.Status
                                               }).ToList(),
                             FundRelease = (from pf in _ProgramFundRepository.GetAll().Where(pf => pf.ProgramID == gcs.Key.ProgramID && pf.Status == Status1.FundRelease)
                                            select new prgFundDetails
                                            {
                                                ProgramManagerName = pf.ProgramManager.Name,
                                                ProjectManagerName = pf.ProjectManager.Name,
                                                ProgramName = pf.Program.Name,
                                                DemandAmount = pf.Amount,
                                                DemandDate = pf.CreationTime,
                                                Status1 = pf.Status
                                            }).ToList(),
                             OpneninBalanceAmmount = _ProgramFundRepository.GetAll().Where(pq => pq.ProgramID == gcs.Key.ProgramID && pq.Status == Status1.FundRelease).Select(t => t.Amount).Sum() - _ProgramExpenseRepository.GetAll().Where(pq => pq.ProgramID == gcs.Key.ProgramID).Select(t => t.Amount).Sum(),
                             FundReleaseDate = _ProgramFundRepository.GetAll().Where(pf => pf.ProgramID == gcs.Key.ProgramID && pf.Status == Status1.FundRelease).Count() == 0 ? DateTime.Today : (DateTime)_ProgramFundRepository.GetAll().Where(pf => pf.ProgramID == gcs.Key.ProgramID && pf.Status == Status1.FundRelease).FirstOrDefault().LastModificationTime,
                             ManagerName = UserManager.Users.Where(u => u.Id == gcs.Key.ManagerID).SingleOrDefault().Name,
                             LocationName = l.Name,
                             Id = l.Id,
                             RequestStatus = gcs.Key.RequestStatus,
                             CostEstimationYear = (from q in _RequestAAplasRepository.GetAll().Where(ma => ma.LocationID == gcs.Key.LocationID && ma.ProgramID == gcs.Key.ProgramID && Convert.ToInt32(ma.CostEstimationYear.Substring(0, 4)) < Convert.ToInt32(NYear)) // This Change by sonali as on we can't see future demands on 9 Jan 20 -- sachin T
                                                   group q by new
                                                   {
                                                       q.CostEstimationYear
                                                   } into gcs1

                                                   select new PrgFundsYearDto()
                                                   {
                                                       Id = l.Id,
                                                       CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                       ActionSubactionCost = (from a in _RequestAAplasRepository.GetAll().Where(mn => mn.LocationID == gcs.Key.LocationID
                                                                              && mn.CostEstimationYear == gcs1.Key.CostEstimationYear && mn.ProgramID == gcs.Key.ProgramID && mn.VillageID == null && mn.ManagerID == gcs.Key.ManagerID)
                                                                              group a by new
                                                                              {
                                                                                  a.UnitOfMeasuresID,
                                                                                  a.TotalUnits,
                                                                                  a.PrgActionAreaActivityMappingID,
                                                                                  a.CommunityContribution,
                                                                                  a.UnitCost,
                                                                                  a.TotalUnitCost,
                                                                                  a.FunderContribution,
                                                                                  a.OtherContribution,
                                                                                  a.RequestStatus,
                                                                                  a.Quarter1PhysicalUnits,
                                                                                  a.Quarter1FinancialRs,
                                                                                  a.Quarter2PhysicalUnits,
                                                                                  a.Quarter2FinancialRs,
                                                                                  a.Quarter3PhysicalUnits,
                                                                                  a.Quarter3FinancialRs,
                                                                                  a.Quarter4PhysicalUnits,
                                                                                  a.Quarter4FinancialRs,
                                                                                  a.Id,
                                                                                  a.LocationID,
                                                                                  a.ProgramID,
                                                                                  a.VillageID,
                                                                                  a.ManagerID,
                                                                              } into gcs2
                                                                              join pa in _PrgActionAreaActivityMappingRepository.GetAll()
                                                                              on gcs2.Key.PrgActionAreaActivityMappingID equals pa.Id
                                                                              join aa in _ActionAreaRepository.GetAll()
                                                                              on pa.ActionAreaID equals aa.Id
                                                                              join sa in _SubActionAreaRepository.GetAll()
                                                                              on pa.SubActionAreaID equals sa.Id
                                                                              join act in _ActivityRepository.GetAll()
                                                                              on pa.ActivityID equals act.Id
                                                                              join um in _UnitofMeasureRepository.GetAll()
                                                                              on gcs2.Key.UnitOfMeasuresID equals um.Id
                                                                              join v in _villageRepository.GetAll()
                                                                              on gcs2.Key.VillageID equals v.Id into joinedT
                                                                              from pd in joinedT.DefaultIfEmpty()

                                                                              join pc in _PrgCostEstRepository.GetAll()
                                                                              on pa.ActivityID equals pc.ActivityID
                                                                              where (pc.ComponentID == pa.ComponentID && pc.CostEstimationYear == gcs1.Key.CostEstimationYear
                                                                              && pc.ProgramID == gcs.Key.ProgramID)

                                                                              select new ListActionSubactionListImplemenTationDto1()
                                                                              {
                                                                                  Id = gcs2.Key.Id,
                                                                                  ActionAreaName = aa.Name,
                                                                                  SubActionAreaName = sa.Name,
                                                                                  ActivityName = act.Name,
                                                                                  ActivityId = act.Id,
                                                                                  LocationID = gcs2.Key.LocationID,
                                                                                  ProgramID = gcs2.Key.ProgramID,
                                                                                  CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                                                  ActivityLevel = pc.ActivityLevel,
                                                                                  VillageID = gcs2.Key.VillageID,
                                                                                  VillageName = pd == null ? String.Empty : pd.Name,
                                                                                  UnitOfMeasuresName = um.Name,
                                                                                  UnitOfMeasuresID = gcs2.Key.UnitOfMeasuresID,
                                                                                  ManagerID = gcs2.Key.ManagerID,
                                                                                  ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == gcs2.Key.ManagerID).Name,
                                                                                  PrgActionAreaActivityMappingID = gcs2.Key.PrgActionAreaActivityMappingID,
                                                                                  TotalUnits = gcs2.Key.TotalUnits,
                                                                                  UnitCost = gcs2.Key.UnitCost,
                                                                                  TotalUnitCost = gcs2.Key.TotalUnitCost,
                                                                                  CommunityContribution = gcs2.Key.CommunityContribution,
                                                                                  FunderContribution = gcs2.Key.FunderContribution,
                                                                                  OtherContribution = gcs2.Key.OtherContribution,
                                                                                  RequestStatus = gcs2.Key.RequestStatus,
                                                                                  Expenditure = _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == gcs2.Key.PrgActionAreaActivityMappingID && pq.ProgramID == gcs2.Key.ProgramID).Select(t => t.Amount).Sum(),
                                                                                  BalanceAmmount = gcs2.Key.TotalUnitCost - _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == gcs2.Key.PrgActionAreaActivityMappingID && pq.ProgramID == gcs2.Key.ProgramID).Select(t => t.Amount).Sum(),
                                                                                  AchivementUnits = _ProgramQuqterUnitMappingRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == gcs2.Key.PrgActionAreaActivityMappingID && pq.ProgramID == gcs2.Key.ProgramID && pq.Status == ProgramQuarterUnitMappingStatus.Completed).Count(),
                                                                                  BalanceUnits = gcs2.Key.TotalUnits - _ProgramQuqterUnitMappingRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == gcs2.Key.PrgActionAreaActivityMappingID && pq.ProgramID == gcs2.Key.ProgramID && pq.Status == ProgramQuarterUnitMappingStatus.Completed).Count(),
                                                                                  DemandUnits = (int)gcs2.Key.TotalUnits - _ProgramFundRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == gcs2.Key.PrgActionAreaActivityMappingID && pq.ProgramID == gcs2.Key.ProgramID && pq.CostEstimationYear == gcs1.Key.CostEstimationYear).Select(t => t.Unit).Sum(),
                                                                                  DemandAmmount = gcs2.Key.TotalUnitCost - _ProgramFundRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == gcs2.Key.PrgActionAreaActivityMappingID && pq.ProgramID == gcs2.Key.ProgramID && pq.CostEstimationYear == gcs1.Key.CostEstimationYear).Select(t => t.Amount).Sum(),
                                                                                  Quarter1PhysicalUnits = gcs2.Key.Quarter1PhysicalUnits,
                                                                                  Quarter1FinancialRs = gcs2.Key.Quarter1FinancialRs,

                                                                                  Quarter2PhysicalUnits = gcs2.Key.Quarter2PhysicalUnits,
                                                                                  Quarter2FinancialRs = gcs2.Key.Quarter2FinancialRs,

                                                                                  Quarter3PhysicalUnits = gcs2.Key.Quarter3PhysicalUnits,
                                                                                  Quarter3FinancialRs = gcs2.Key.Quarter3FinancialRs,

                                                                                  Quarter4PhysicalUnits = gcs2.Key.Quarter4PhysicalUnits,
                                                                                  Quarter4FinancialRs = gcs2.Key.Quarter4FinancialRs,

                                                                              }).ToList(),
                                                   }).ToList(),

                         }).ToList();
            return new List<ProgramFundsListDto>(ObjectMapper.Map<List<ProgramFundsListDto>>(Query));

        }




        public async Task<List<ListActionSubactionListImplemenTationDto1>> GetProjectViewDetails(int programId, string Year, int locationId, DateTime? startFilter, DateTime? endFilter)
        {
            var filterRequest = _RequestAAplasRepository.GetAll().Include(t => t.PrgActionAreaActivityMapping).
                ThenInclude(t => t.SubActionArea).ThenInclude(t => t.ActionArea).Include(t => t.PrgActionAreaActivityMapping)
                .ThenInclude(t => t.Activity).Include(t => t.Village).Include(t => t.UnitOfMeasures);

            var expenseList = await _ProgramExpenseRepository.GetAll().Where(p => p.ProgramID == programId).ToListAsync();
            var fundList = await _ProgramFundRepository.GetAll().Where(p => p.ProgramID == programId).ToListAsync();

            var query = (from y in filterRequest.Where(p => p.ProgramID == programId && p.ManagerID == AbpSession.UserId && p.LocationID == locationId && p.VillageID == null)
                         group y by y.PrgActionAreaActivityMapping.Activity.Name into g
                         select new ListActionSubactionListImplemenTationDto1()
                         {
                             Id = g.FirstOrDefault().Id,
                             ActivityName = g.FirstOrDefault().PrgActionAreaActivityMapping.Activity.Name,
                             ActivityId = g.FirstOrDefault().PrgActionAreaActivityMapping.Activity.Id,
                             LocationID = g.FirstOrDefault().LocationID,
                             ProgramID = g.FirstOrDefault().ProgramID,
                             CostEstimationYear = g.FirstOrDefault().CostEstimationYear,
                             PrgActionAreaActivityMappingID = g.FirstOrDefault().PrgActionAreaActivityMappingID,
                             UnitCost = g.FirstOrDefault().UnitCost,
                         });

            var result = await query.ToListAsync();


            foreach (var item in result)
            {
                item.TotalUnitCost = 0;
                item.TotalUnits = 0;
                item.Quarter1PhysicalUnits = 0;
                item.Quarter1FinancialRs = 0;
                item.Quarter2PhysicalUnits = 0;
                item.Quarter2FinancialRs = 0;
                item.Quarter3PhysicalUnits = 0;
                item.Quarter3FinancialRs = 0;
                var c = startFilter.Value.Year;
                var d = endFilter.Value.Year;
                var hh1 = "";
                var f = "";
                for (var dt = startFilter.Value; dt < endFilter.Value; dt = dt.AddMonths(3))
                {   
                    if (dt.Month >= 4 && dt.Month < 7)
                    {
                        hh1 = (dt.Year) + "-" + (dt.Year + 1);
                        item.TotalUnits += filterRequest.Where(t => t.CostEstimationYear == hh1 && t.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID).Select(t => t.Quarter1PhysicalUnits).Sum();
                        item.TotalUnitCost += filterRequest.Where(t => t.CostEstimationYear == hh1 && t.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID).Select(t => t.Quarter1FinancialRs).Sum();                        
                    }
                    else if (dt.Month >= 7 && dt.Month < 10)
                    {
                        hh1 = (dt.Year) + "-" + (dt.Year + 1);
                        item.TotalUnits += filterRequest.Where(t => t.CostEstimationYear == hh1 && t.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID).Select(t => t.Quarter2PhysicalUnits).Sum();
                        item.TotalUnitCost += filterRequest.Where(t => t.CostEstimationYear == hh1 && t.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID).Select(t => t.Quarter2FinancialRs).Sum();
                    }
                    else if (dt.Month >= 10 && dt.Month <= 12)
                    {
                        hh1 = (dt.Year) + "-" + (dt.Year + 1);
                        item.TotalUnits += filterRequest.Where(t => t.CostEstimationYear == hh1 && t.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID).Select(t => t.Quarter3PhysicalUnits).Sum();
                        item.TotalUnitCost += filterRequest.Where(t => t.CostEstimationYear == hh1 && t.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID).Select(t => t.Quarter3FinancialRs).Sum();
                    }
                    else if (dt.Month >= 1 && dt.Month < 4)
                    {
                        hh1 = (dt.Year - 1) + "-" + (dt.Year);
                        item.TotalUnits += filterRequest.Where(t => t.CostEstimationYear == hh1 && t.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID).Select(t => t.Quarter4PhysicalUnits).Sum();
                        item.TotalUnitCost += filterRequest.Where(t => t.CostEstimationYear == hh1 && t.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID).Select(t => t.Quarter4FinancialRs).Sum();
                    }

                    item.DemandUnits = Convert.ToInt32(item.TotalUnits - fundList.Where(pq => pq.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID).Select(i => i.Unit).Sum());
                    item.DemandAmmount = item.TotalUnitCost - fundList.Where(pq => pq.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID).Select(j => j.Amount).Sum();

                    if (f != hh1)
                    {
                        item.Quarter1PhysicalUnits += expenseList.Where(t => t.ExpensesYear == hh1 && t.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID).Select(t => t.Unit).Sum();
                        item.Quarter1FinancialRs += expenseList.Where(t => t.ExpensesYear == hh1 && t.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID).Select(t => t.Amount).Sum();

                        var hh2 = (dt.Year + 1).ToString() + "-" + (dt.Year + 2).ToString();

                        var dtt2 = expenseList.FirstOrDefault(t => t.ExpensesYear == hh2 && t.PrgActionAreaActivityMappingID == item.PrgActionAreaActivityMappingID);

                        item.Quarter2PhysicalUnits = dtt2 == null ? 0 : dtt2.Unit;
                        item.Quarter2FinancialRs = dtt2 == null ? 0 : dtt2.Amount;

                        item.Quarter3PhysicalUnits = item.Quarter1PhysicalUnits + item.Quarter2PhysicalUnits;
                        item.Quarter3FinancialRs = item.Quarter1FinancialRs + item.Quarter2FinancialRs;

                        f = hh1;
                    }
                }
            }
            return result;
        }

        public List<ProgramFundsListDto> GetViewDemand(int programId, string Year, DateTime startdate, DateTime EnddDate)
        {
            string ddd = "2016-05-28 11:58:08.0543126";
            startdate = Convert.ToDateTime(ddd);

            string dd = "2021-05-28 11:58:08.0543126";
            EnddDate = Convert.ToDateTime(dd);

            List<string> yearLists = new List<string>();
            for (var i = startdate.Year; i < EnddDate.Year; i++)
            {
                string y = i + "-" + (int.Parse(i.ToString()) + 1);
                yearLists.Add(y);
            }


            var Query = _RequestAAplasRepository.GetAll().Where(p => p.ProgramID == programId && yearLists.Contains(p.CostEstimationYear) && p.VillageID != null).ToList();
            var CostEstimationYear = (from q in Query
                                      group q by new
                                      {
                                          q.CostEstimationYear
                                      } into gcs1

                                      select new PrgFundsYearDto()
                                      {
                                          CostEstimationYear = gcs1.Key.CostEstimationYear,
                                          ActionSubactionCost = (from a in Query
                                                                 group a by new
                                                                 {
                                                                     //a.ChecklistID,
                                                                     a.UnitOfMeasuresID,
                                                                     a.TotalUnits,
                                                                     a.PrgActionAreaActivityMappingID,
                                                                     a.CommunityContribution,
                                                                     a.UnitCost,
                                                                     a.TotalUnitCost,
                                                                     a.FunderContribution,
                                                                     a.OtherContribution,
                                                                     a.RequestStatus,
                                                                     a.Quarter1PhysicalUnits,
                                                                     a.Quarter1FinancialRs,
                                                                     a.Quarter2PhysicalUnits,
                                                                     a.Quarter2FinancialRs,
                                                                     a.Quarter3PhysicalUnits,
                                                                     a.Quarter3FinancialRs,
                                                                     a.Quarter4PhysicalUnits,
                                                                     a.Quarter4FinancialRs,
                                                                     a.Id,
                                                                     a.LocationID,
                                                                     a.ProgramID,
                                                                     a.VillageID,
                                                                     a.ManagerID,
                                                                 } into gcs2
                                                                 join pa in _PrgActionAreaActivityMappingRepository.GetAll()
                                                                 on gcs2.Key.PrgActionAreaActivityMappingID equals pa.Id
                                                                 join aa in _ActionAreaRepository.GetAll()
                                                                 on pa.ActionAreaID equals aa.Id
                                                                 join sa in _SubActionAreaRepository.GetAll()
                                                                 on pa.SubActionAreaID equals sa.Id
                                                                 join act in _ActivityRepository.GetAll()
                                                                 on pa.ActivityID equals act.Id
                                                                 join um in _UnitofMeasureRepository.GetAll()
                                                                 on gcs2.Key.UnitOfMeasuresID equals um.Id
                                                                 join v in _villageRepository.GetAll()
                                                                 on gcs2.Key.VillageID equals v.Id into joinedT
                                                                 from pd in joinedT.DefaultIfEmpty()

                                                                 join pc in _PrgCostEstRepository.GetAll()
                                                                 on pa.ActivityID equals pc.ActivityID
                                                                 where (pc.ComponentID == pa.ComponentID && pc.CostEstimationYear == gcs1.Key.CostEstimationYear
                                                                 && pc.ProgramID == programId)

                                                                 select new ListActionSubactionListImplemenTationDto1()
                                                                 {
                                                                     Id = gcs2.Key.Id,
                                                                     ActionAreaName = aa.Name,
                                                                     SubActionAreaName = sa.Name,
                                                                     ActivityName = act.Name,
                                                                     ActivityId = act.Id,
                                                                     LocationID = gcs2.Key.LocationID,
                                                                     ProgramID = gcs2.Key.ProgramID,
                                                                     CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                                     ActivityLevel = pc.ActivityLevel,
                                                                     VillageID = gcs2.Key.VillageID,
                                                                     VillageName = pd == null ? String.Empty : pd.Name,
                                                                     UnitOfMeasuresName = um.Name,
                                                                     UnitOfMeasuresID = gcs2.Key.UnitOfMeasuresID,
                                                                     ManagerID = gcs2.Key.ManagerID,
                                                                     ManagerName = UserManager.Users.Where(u => u.Id == gcs2.Key.ManagerID).SingleOrDefault().Name,
                                                                     PrgActionAreaActivityMappingID = gcs2.Key.PrgActionAreaActivityMappingID,
                                                                     TotalUnits = gcs2.Key.TotalUnits,
                                                                     UnitCost = gcs2.Key.UnitCost,
                                                                     TotalUnitCost = gcs2.Key.TotalUnitCost,
                                                                     CommunityContribution = gcs2.Key.CommunityContribution,
                                                                     FunderContribution = gcs2.Key.FunderContribution,
                                                                     OtherContribution = gcs2.Key.OtherContribution,
                                                                     RequestStatus = gcs2.Key.RequestStatus,
                                                                     Expenditure = _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == gcs2.Key.PrgActionAreaActivityMappingID && pq.ProgramID == gcs2.Key.ProgramID).Select(t => t.Amount).Sum(),
                                                                     BalanceAmmount = gcs2.Key.TotalUnitCost - _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == gcs2.Key.PrgActionAreaActivityMappingID && pq.ProgramID == gcs2.Key.ProgramID).Select(t => t.Amount).Sum(),
                                                                     AchivementUnits = _ProgramQuqterUnitMappingRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == gcs2.Key.PrgActionAreaActivityMappingID && pq.ProgramID == gcs2.Key.ProgramID && pq.Status == ProgramQuarterUnitMappingStatus.Completed).Count(),
                                                                     BalanceUnits = gcs2.Key.TotalUnits - _ProgramQuqterUnitMappingRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == gcs2.Key.PrgActionAreaActivityMappingID && pq.ProgramID == gcs2.Key.ProgramID && pq.Status == ProgramQuarterUnitMappingStatus.Completed).Count(),
                                                                     DemandUnits = (int)gcs2.Key.TotalUnits - _ProgramFundRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == gcs2.Key.PrgActionAreaActivityMappingID && pq.ProgramID == gcs2.Key.ProgramID).Select(t => t.Unit).Sum(),
                                                                     DemandAmmount = gcs2.Key.TotalUnitCost - _ProgramFundRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == gcs2.Key.PrgActionAreaActivityMappingID && pq.ProgramID == gcs2.Key.ProgramID).Select(t => t.Amount).Sum(),
                                                                     Quarter1PhysicalUnits = gcs2.Key.Quarter1PhysicalUnits,
                                                                     Quarter1FinancialRs = gcs2.Key.Quarter1FinancialRs,

                                                                     Quarter2PhysicalUnits = gcs2.Key.Quarter2PhysicalUnits,
                                                                     Quarter2FinancialRs = gcs2.Key.Quarter2FinancialRs,

                                                                     Quarter3PhysicalUnits = gcs2.Key.Quarter3PhysicalUnits,
                                                                     Quarter3FinancialRs = gcs2.Key.Quarter3FinancialRs,

                                                                     Quarter4PhysicalUnits = gcs2.Key.Quarter4PhysicalUnits,
                                                                     Quarter4FinancialRs = gcs2.Key.Quarter4FinancialRs,

                                                                 }).Take(1).ToList(),
                                      }).ToList();

            return new List<ProgramFundsListDto>(ObjectMapper.Map<List<ProgramFundsListDto>>(Query));

        }

        public async Task SaveProjectFund(ProgramFundDto prgFundDetails)
        {

            ProjectFunds projectFunds = new ProjectFunds
            {
                ProjectManagerID = prgFundDetails.ProjectManagerID,
                DemandAmount = prgFundDetails.demandAmount - prgFundDetails.releasedAmount,
                ReleaseAmount = prgFundDetails.releasedAmount,
                ReleaseDate = DateTime.Now,
                Status = prgFundDetails.demandAmount - prgFundDetails.releasedAmount == 0 ? Status1.FundRelease : Status1.Pending,
                ProgramID = prgFundDetails.ID
            };
            await _ProjectFundRepository.InsertAsync(projectFunds);
        }

        public List<ProgramFundsListDto> GetProjectFundDetailForResult(string input, int ProgramID)
        {


            List<ProgramFundsListDto> result = new List<ProgramFundsListDto>();
            var Query = (from p in _ProjectFundRepository.GetAll().Where(x => x.ProgramID == ProgramID)

                         select new ProgramFundsListDto()
                         {
                             DemandAmount = p.DemandAmount == null ? _ProgramFundRepository.GetAll().Where(g => g.ProgramID == ProgramID && g.CostEstimationYear == input).Select(t => t.Amount).Sum() : p.DemandAmount,
                             FundReleaseDate = Convert.ToDateTime(p.ReleaseDate),
                             ReleasedAmount = p.ReleaseAmount,
                         }).ToList();
            return new List<ProgramFundsListDto>(ObjectMapper.Map<List<ProgramFundsListDto>>(Query));

        }

        public string GetUserRoleById(int input)
        {
            //var apbrole = _userRoleRepository.GetAll().Where(x => x.UserId == input).FirstOrDefault();
            //var rolename = _roleRepository.GetAll().Where(x => x.Id == apbrole.RoleId).FirstOrDefault();

            var role = _userRepository.GetAll().Where(ss => ss.Id == input).FirstOrDefault();
            var rolename = _roleRepository.GetAll().Where(x => x.Id == role.UserRole).FirstOrDefault();

            if (rolename == null)
            {
                return "Admin";
            }
            return rolename.DisplayName;

        }
    }
}