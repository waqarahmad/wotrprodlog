﻿using Abp.Auditing;
using Abp.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using WOTR.PM.Authorization;
using WOTR.PM.Configuration;
using WOTR.PM.Dashboards.Dto;
using WOTR.PM.NewImpactIndicator.IIForms.Dto;
using WOTR.PM.Utils;
using WOTR.PM.States;
using Abp.Domain.Repositories;
using WOTR.PM.Locations;
using WOTR.PM.PrgRequestAAPlas.Dto;

namespace WOTR.PM.Dashboards
{
    [DisableAuditing]
    [AbpAuthorize(AppPermissions.Pages_Tenant_Dashboard)]
    public class DashboardAppService : PMAppServiceBase, IDashboardAppService
    {
        private readonly IConfigurationRoot _appConfiguration;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<VillageCluster> _villageClusterRepository;

        public DashboardAppService(IHostingEnvironment env, IRepository<State> stateRepository, IRepository<VillageCluster> villageClusterRepository)
        {
            _appConfiguration = env.GetAppConfiguration();
            _stateRepository = stateRepository;
            _villageClusterRepository = villageClusterRepository;
        }

        public async Task<List<dualListDto>> FetchAllStates()
        {
            try
            {

                var query = (from s in _stateRepository.GetAll()
                             join vc in _villageClusterRepository.GetAll()
                             on s.Id equals vc.StateID
                             group s by s into g
                             select new dualListDto()
                             {
                                name = g.Key.StateName,
                                id = g.Key.Id
                             }).ToList();

                return query;
                //string value;
                //var dualList = new List<dualListDto>();
                //AdminDashboardData adminDashboardData = new AdminDashboardData();
                //var connectionString = _appConfiguration["ConnectionStrings:Default"];
                //using (SqlConnection conn = new SqlConnection(connectionString))
                //{
                //    //await ConnectionConnect(conn);
                //    var da = new SqlDataAdapter();
                //    var ds = new DataSet();

                //    await ConnectionConnect(conn);

                //    using (var cmdThree = new SqlCommand("", conn))
                //    {
                //        //ProjectWiseCostEstimationYear
                //        var dataTable = new DataTable();
                //        cmdThree.CommandTimeout = 0;
                //        cmdThree.CommandText = "[dbo].[sp_PBI_StatesForWindow1]";
                //        cmdThree.CommandType = CommandType.StoredProcedure;

                //        SqlDataReader dr = cmdThree.ExecuteReader();
                //        dataTable.Load(dr);
                //        if (dataTable.Rows.Count != 0)
                //        {
                //            value = dataTable.Rows[0][0].ToString();

                //            if (value != "")
                //            {
                //                adminDashboardData.dataTable = dataTable;
                //                FillDashboardDatatableDto.FillStates(adminDashboardData);
                //            }
                //        }
                //        conn.Close();
                //    }

                //}

                //return adminDashboardData.getAllState;

            }
            catch (Exception)
            {

                throw;
            }



        }


        public async Task<BudgetProjectMembersByStateDto> GetBudgetProjectMembersdetailsAllStates(List<int> States)
        {
            try
            {
                string value;
                string StateIds = String.Join("," ,States );
                var dualList = new List<dualListDto>();
                AdminDashboardData adminDashboardData = new AdminDashboardData();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);

                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_Adm_Dash_BudgetProgramMemberByState]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@StateIds", StateIds));
                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                adminDashboardData.dataTable = dataTable;
                                FillDashboardDatatableDto.FillBudgetProjectDetailsByState(adminDashboardData);
                            }
                        }
                        conn.Close();
                    }

                }

                return adminDashboardData.budgetProjectMembersDetailsByStateDto;

            }
            catch (Exception ex)
            {

                throw;
            }



        }

        public async Task<List<UserDetailsDto>> GetMembersdetailsAllStates(List<int> States)
        {
            try
            {
                string value;
                string StateIds = String.Join(",", States);
                var dualList = new List<dualListDto>();
                AdminDashboardData adminDashboardData = new AdminDashboardData();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);

                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_Adm_Dash_MemberDetailsByState]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@StateIds", StateIds));
                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                adminDashboardData.dataTable = dataTable;
                                FillDashboardDatatableDto.FillMemberDetailsByState(adminDashboardData);
                            }
                        }
                        conn.Close();
                    }

                }

                return adminDashboardData.userDetailsDto;

            }
            catch (Exception ex)
            {

                throw;
            }



        }

        public async Task<AdminDashboardData> GetBudgetAndExpenseByStates(List<int> States)
        {
            try
            {
                string value;
                string StateIds = String.Join(",", States);
                var dualList = new List<dualListDto>();
                AdminDashboardData adminDashboardData = new AdminDashboardData();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);

                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_Adm_Dash_BudgetAndExpenseByState]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@StateIds", StateIds));
                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                adminDashboardData.dataTable = dataTable;
                                FillDashboardDatatableDto.FillBudgetAndExpenseByState(adminDashboardData);
                            }
                        }
                        conn.Close();
                    }

                }

                return adminDashboardData;

            }
            catch (Exception ex)
            {

                throw;
            }



        }

        public async Task<List<ProjectDemandAndRemainingByStateDto>> GetDemandAndDemandRemaingByStates(List<int> States)
        {
            try
            {
                string value;
                string StateIds = String.Join(",", States);
                var dualList = new List<dualListDto>();
                AdminDashboardData adminDashboardData = new AdminDashboardData();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);

                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_Adm_Dash_ProjectDemandByState]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                       // cmdThree.Parameters.Add(new SqlParameter("@StateIds", StateIds));
                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                adminDashboardData.dataTable = dataTable;
                                FillDashboardDatatableDto.FillDemandAndRemainingByState(adminDashboardData);
                            }
                        }
                        conn.Close();
                    }

                }

                return adminDashboardData.projectDemandAndRemainingByStateDto;

            }
            catch (Exception ex)
            {

                throw;
            }



        }

        public async Task<AdminDashboardData> GetDemandSummaryByStates(List<int> States)
        {
            try
            {
                string value;
                string StateIds = String.Join(",", States);
                var dualList = new List<dualListDto>();
                AdminDashboardData adminDashboardData = new AdminDashboardData();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);

                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_Adm_Dash_DemandSummuryByState]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@StateIds", StateIds));
                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                adminDashboardData.dataTable = dataTable;
                                FillDashboardDatatableDto.FillDemandSummaryByState(adminDashboardData);
                            }
                        }
                        conn.Close();
                    }

                }

                return adminDashboardData;

            }
            catch (Exception ex)
            {

                throw;
            }



        }

        public async Task<List<AmountSanctionByStateDto>> GetAmountSanctionByStates(List<int> States)
        {
            try
            {
                string value;
                string StateIds = String.Join(",", States);
                var dualList = new List<dualListDto>();
                AdminDashboardData adminDashboardData = new AdminDashboardData();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);

                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_Adm_Dash_AmountSactionByState]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@StateIds", StateIds));
                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                adminDashboardData.dataTable = dataTable;
                                FillDashboardDatatableDto.FillAmountSanctionByState(adminDashboardData);
                            }
                        }
                        conn.Close();
                    }

                }

                return adminDashboardData.amountSanctionByStateDto;

            }
            catch (Exception ex)
            {

                throw;
            }



        }

        public async Task<List<ContributionsByStateDto>> GetContributionsByStates(List<int> States)
        {
            try
            {
                string value;
                string StateIds = String.Join(",", States);
                var dualList = new List<dualListDto>();
                AdminDashboardData adminDashboardData = new AdminDashboardData();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);

                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_Adm_Dash_ContributionsByState]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@StateIds", StateIds));
                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                adminDashboardData.dataTable = dataTable;
                                FillDashboardDatatableDto.FillContributionsByState(adminDashboardData);
                            }
                        }
                        conn.Close();
                    }

                }

                return adminDashboardData.contributionsByStateDto;

            }
            catch (Exception ex)
            {

                throw;
            }



        }

        public async Task<List<ActionSubActionByStateDto>> GetActionSubActionByStates(List<int> States)
        {
            try
            {
                string value;
                string StateIds = String.Join(",", States);
                AdminDashboardData adminDashboardData = new AdminDashboardData();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);

                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_Adm_Dash_ActionSubActionByState]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@StateIds", StateIds));
                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                adminDashboardData.dataTable = dataTable;
                                FillDashboardDatatableDto.FillActionSubActionByState(adminDashboardData);
                            }
                        }
                        conn.Close();
                    }

                }

                return adminDashboardData.actionSubActionByStateDto;

            }
            catch (Exception ex)
            {

                throw;
            }



        }
        public async Task<List<ActionByStateDto>> GetActionByStates(List<int> States)
        {
            try
            {
                string value;
                string StateIds = String.Join(",", States);
                AdminDashboardData adminDashboardData = new AdminDashboardData();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);

                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_Adm_Dash_ActionByState]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@StateIds", StateIds));
                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                adminDashboardData.dataTable = dataTable;
                                FillDashboardDatatableDto.FillActionByState(adminDashboardData);
                            }
                        }
                        conn.Close();
                    }

                }

                return adminDashboardData.actionByStateDto;

            }
            catch (Exception ex)
            {

                throw;
            }



        }


        public async Task<string> GetBudgetByStates(List<int> States)
        {
            try
            {
                string value;
                string Budget = "";
                string StateIds = String.Join(",", States);
                var dualList = new List<dualListDto>();
                AdminDashboardData adminDashboardData = new AdminDashboardData();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);

                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_Adm_Dash_BudgetByState]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@StateIds", StateIds));
                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                Budget = dataTable.Rows[0][0].ToString();
                            }
                        }
                        conn.Close();
                    }

                }

                return Budget;

            }
            catch (Exception ex)
            {

                throw;
            }



        }


        public async Task<List<DemandWiseProject>> GetDemandAndDemandRemaingByStateId(int StateId)
        {
            try
            {
                string value;
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var demandWiseProject = new List<DemandWiseProject>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdFive = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdFive.CommandTimeout = 0;
                        cmdFive.CommandText = "[dbo].[sp_Adm_Dash_ProjectDemandByStateId]";
                        cmdFive.CommandType = CommandType.StoredProcedure;
                        cmdFive.Parameters.Add(new SqlParameter("@StateId", StateId));

                        SqlDataReader dr = cmdFive.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    demandWiseProject.Add(new DemandWiseProject
                                    {
                                        //StateName = (row["StateName"] == DBNull.Value) ? "" : Convert.ToString(row["StateName"]),
                                        ProjectName = (row["Name"] == DBNull.Value) ? "" : Convert.ToString(row["Name"]),
                                        Demand = (row["programFund"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["programFund"]),
                                        DemandRemaining = (row["remaining"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["remaining"]),
                                        ProgramId = (row["Id"] == DBNull.Value) ? 00 : Convert.ToInt32(row["Id"]),
                                      //  StateId = (row["StateId"] == DBNull.Value) ? 00 : Convert.ToInt32(row["StateId"]),
                                    });
                                }
                            }
                        }
                        conn.Close();
                    }

                }
                return demandWiseProject;
            }
            catch (Exception ex)
            {

                throw;
            }



        }

        public async Task<List<ProgramManagerAmountSanction>> GetAmountSanctionByProjectId(int programId)
        {
            try
            {
                string value;
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var ProgramManagerAmountSanctions = new List<ProgramManagerAmountSanction>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdSix = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdSix.CommandTimeout = 0;
                        cmdSix.CommandText = "[dbo].[sp_Adm_Dash_AmountSactionByStateId]";
                        cmdSix.CommandType = CommandType.StoredProcedure;
                        cmdSix.Parameters.Add(new SqlParameter("@ProgramId", programId));

                        SqlDataReader dr = cmdSix.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    ProgramManagerAmountSanctions.Add(new ProgramManagerAmountSanction
                                    {
                                       // ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),
                                        Date = (row["Date"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["Date"]),
                                        AmountSanction = (row["AmountSanction"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["AmountSanction"]),
                                    });
                                }

                            }
                        }
                        conn.Close();
                    }
                }
                return ProgramManagerAmountSanctions;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<RRCAndProgramWiseExpenseDto>> GetRRCAndProgramWiseExpense(List<int> States)
        {
            try
            {
                string value;
                string StateIds = String.Join(",", States);
                var dualList = new List<dualListDto>();
                AdminDashboardData adminDashboardData = new AdminDashboardData();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);

                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_Adm_Dash_RRCAndProgramExpenseByRCC]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@StateIds", StateIds));
                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                adminDashboardData.dataTable = dataTable;
                                FillDashboardDatatableDto.FillRRCAndProgramWiseExpense(adminDashboardData);
                            }
                        }
                        conn.Close();
                    }

                }

                return adminDashboardData.rRCAndProgramWiseExpenseDto;

            }
            catch (Exception ex)
            {

                throw;
            }



        }

        
        public async Task AddDataInDataTable(AdminDashboardData adminDashboardDatas, int fillDataTable = 0)
        {
            try
            {
                foreach (DataRow row in adminDashboardDatas.dataTable.Rows)
                {
                    if (fillDataTable == 1)
                    {
                        adminDashboardDatas.getAllState.Add(new dualListDto
                        {
                            id = (row["StateId"] == DBNull.Value) ? 00 : Convert.ToInt32(row["StateId"]),
                            name = (row["StateName"] == DBNull.Value) ? "" : Convert.ToString(row["StateName"]),
                        });
                    }
                }
            }
            catch (Exception)
            {
                throw;

            }
        }


        public async Task ConnectionConnect(SqlConnection conn)
        {
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    await conn.OpenAsync();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
