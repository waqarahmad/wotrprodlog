﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WOTR.PM.MultiTenancy.HostDashboard.Dto;

namespace WOTR.PM.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}