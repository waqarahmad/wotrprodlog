﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using WOTR.PM.MultiTenancy.Accounting.Dto;

namespace WOTR.PM.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
