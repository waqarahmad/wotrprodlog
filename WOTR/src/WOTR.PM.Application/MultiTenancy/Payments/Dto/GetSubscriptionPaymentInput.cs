namespace WOTR.PM.MultiTenancy.Payments.Dto
{
    public class GetSubscriptionPaymentInput
    {
        public long Id { get; set; }
    }
}