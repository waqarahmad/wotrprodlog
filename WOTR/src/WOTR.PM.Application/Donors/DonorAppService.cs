﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Countrys;
using WOTR.PM.Donors.Dto;
using WOTR.PM.Programs;

namespace WOTR.PM.Donors
{
    public class DonorAppService : PMAppServiceBase, IDonerAppService
    {
        private readonly IRepository<Donor> _DonorAppRepository;
        private readonly IRepository<Country> _CountryRepository;
        private readonly IRepository<Program> _programRepository;

        int DonorId;

        public DonorAppService(IRepository<Donor> DonorAppRepository, IRepository<Country> CountryRepository, IRepository<Program> ProgramRepository)
        {
            _DonorAppRepository = DonorAppRepository;
            _CountryRepository = CountryRepository;
            _programRepository = ProgramRepository;


        }

        public List<string> CreateOrUpdateDoner(DonorListDto Input)
        {

            List<string> result = new List<string>();            

                Donor donor = new Donor();
                //..donor table
                donor.Id = Input.Id;
                donor.AmountReceived = Input.AmountReceived;
                donor.AmountReceivedDateTime = Input.AmountReceivedDateTime;
                donor.CompanyAddress = Input.CompanyAddress;
                donor.CompanyName = Input.CompanyName;
                donor.CompanyContactNo = Input.CompanyContactNo;
                donor.CompanyEmailID = Input.CompanyEmailID;
                donor.CompanyAddressCountry = Input.CompanyAddressCountry;

                donor.FirstName = Input.FirstName;
                donor.FirstEmailAddress = Input.FirstEmailAddress;
                donor.FirstAddress = Input.FirstAddress;
                donor.FirstContactNo = Input.FirstContactNo;
                donor.FirstOtherEmail = Input.FirstOtherEmail;

                donor.SecondName = Input.SecondName;
                donor.SecondEmailAddress = Input.SecondEmailAddress;
                donor.SecondAddress = Input.SecondAddress;
                donor.SecondContactNo = Input.SecondContactNo;
                donor.SecondOtherEmail = Input.SecondOtherEmail;

                donor.ThridName = Input.ThridName;
                donor.ThridEmailAddress = Input.ThridEmailAddress;
                donor.ThridAddress = Input.ThridAddress;
                donor.ThridContactNo = Input.ThridContactNo;
                donor.ThridOtherEmail = Input.ThridOtherEmail;

                if (Input.Id == 0)
                {
                var QueryCheck = (from a in _DonorAppRepository.GetAll()
                                  where (a.CompanyName == Input.CompanyName && a.CompanyEmailID ==Input.CompanyEmailID)
                                  select a.Id).ToList();
                if (QueryCheck.Count == 0)
                {
                    try
                    {
                        //...ProgramId PrimaryKey gererated after insert pass to ProgramManagerMapping and ProgramRegionCoverage Table.
                        DonorId = _DonorAppRepository.InsertAndGetId(donor);

                        //...Just confirmation for program table.
                        if (Convert.ToBoolean(DonorId))
                            result.Add("Record Add in program Table Sucessfully");

                    }
                    catch (Exception e)
                    {

                        result.Add(e.ToString());
                    }
                }
                else result.Add("Record is already Present");
            }
                else
                {
                    donor.TenantId = Convert.ToInt16(AbpSession.TenantId);
                    _DonorAppRepository.UpdateAsync(donor);
                    result.Add("Record UpdateSucessfully in program Table");
                }                

            return result;
        }


        public List<DonorListDto> GetAllDonorsearch(string input)
        {
            //var query= (from d in _DonorAppRepository.GetAll() select d).ToList().OrderByDescending(d=>d.CompanyName);

            //return new List<DonorListDto>(ObjectMapper.Map<List<DonorListDto>>(query)); ;

            var qu = (from aa in _DonorAppRepository.GetAll().WhereIf(
            !input.IsNullOrEmpty(),
            p => p.CompanyName.ToUpper().Contains(input.ToUpper()) ||
            p.CompanyName.ToLower().Contains(input.ToLower()))

                      select new DonorListDto()
                      {
                          CompanyName = aa.CompanyName,
                          CompanyAddress = aa.CompanyAddress,
                          CompanyContactNo = aa.CompanyContactNo,
                          CompanyEmailID=aa.CompanyEmailID,
                          TenantId = aa.TenantId,
                          CompanyAddressCountry = aa.CompanyAddressCountry,
                          Id = aa.Id
                      }).ToList();
            return new List<DonorListDto>(ObjectMapper.Map<List<DonorListDto>>(qu));


        }

        public List<DonorListDto> GetAllDonor()
        {

            //var query = (from d in _DonorAppRepository.GetAll() select d).ToList().OrderByDescending(d => d.CompanyName);

            //return new List<DonorListDto>(ObjectMapper.Map<List<DonorListDto>>(query)); ;

            var qu = (from aa in _DonorAppRepository.GetAll()

                      select new DonorListDto()
                      {
                          CompanyName = aa.CompanyName,
                          CompanyAddress = aa.CompanyAddress,
                          CompanyContactNo = aa.CompanyContactNo,
                          CompanyEmailID = aa.CompanyEmailID,
                          TenantId = aa.TenantId,
                          CompanyAddressCountry = aa.CompanyAddressCountry,
                          Id = aa.Id
                      }).ToList();
            return new List<DonorListDto>(ObjectMapper.Map<List<DonorListDto>>(qu));


        }

        //var QueryCheck = (from a in _actionAreaAppRepository.GetAll() where (a.Code == Input.Code && a.Name == Input.Name) select a.Id).ToList();
        //                if (QueryCheck.Count == 0)
        //                {
        //                    _actionAreaAppRepository.Insert(actionareas);

        //                    message.Add("ActionArea Added  sucessfully !");
        //                }
        //                else message.Add("Record is already Present");

        public string  DeleteDonor(EntityDto<int> Input)
        {

        var QueryCheck = (from a in _programRepository.GetAll() where (a.DonorID == Input.Id ) select a.Id).ToList();
            if (QueryCheck.Count == 0)
            {
                _DonorAppRepository.Delete(Input.Id);
                return "Record Deleted ";
            }
            else
            {
                return "Record Exist";
            }
        }


        public List<CountryListDto> GetAllCountry()
        {
            var query = (from c in _CountryRepository.GetAll() select new CountryListDto() { CountryName = c.CountryName, id = c.Id }).ToList();
            return query;
        }

    }
}
