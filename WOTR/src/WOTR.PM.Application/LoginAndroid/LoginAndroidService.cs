﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.Authorization.Users;
using WOTR.PM.Checklists;
using WOTR.PM.LoginAndroid.Dto;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.Programs;
using WOTR.PM.UnitOfMeasures;
using WOTR.PM.Components;
using WOTR.PM.PrgImplementationPlans;
using WOTR.PM.UnitOfMeasures.Dtos;
using WOTR.PM.ProgramExpenses.Dto;
using WOTR.PM.ProgramExpenses;
using WOTR.PM.PrgQuarterUnits;
using WOTR.PM.Storage;
using WOTR.PM.ProgramFunds;
using System.IO;
using System.Drawing;
using WOTR.PM.PrgRequestAAPlas;
using WOTR.PM.PrgImplementationPlans.Dto;
using WOTR.PM.Locations;
using WOTR.PM.ProgramCostEstimations;
using WOTR.PM.ChekLists;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.PrgImplementationsPlans;
using WOTR.PM.Authorization.Roles.Dto;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;


namespace WOTR.PM.LoginAndroid
{
    public class LoginAndroidService : PMAppServiceBase, ILoginAndroidService
    {
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Program> _programRepository;
        private readonly IRepository<ImplementationPlanCheckList> _ImplementationPlanCheckListRepository;
        private readonly IRepository<PrgActionAreaActivityMapping> _PrgActionAreaActivityMappingRepository;
        private readonly IRepository<Activity> _ActivityRepository;
        private readonly IRepository<ChecklistItem> _ChecklistItemRepository;
        private readonly IRepository<UnitofMeasure> _unitofMeasureRepository;
        private readonly IRepository<ExpensesType> _ExpensesTypeRepository;
        private readonly IRepository<RequestAAplas> _RequestAAplasRepository;
        private readonly IRepository<ProgramQuqterUnitMapping> _ProgramQuqterUnitMappingRepository;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IRepository<ProgramComponentsActivitesMapping> _ProgramComponentsActivitesMappingRepository;



        private readonly IComponentActivityMappingRepository _ProgramComponentActivityMappingRepository;
        private readonly IRepository<ProgramExpense> _ProgramExpenseRepository;
        private readonly IRepository<ProgramExpenseImage> _ProgramExpenseImageRepository;
        private readonly IRepository<Location> _LocationRepository;
        private readonly IRepository<Program> _ProgrameRepository;
        private readonly IRepository<ProgramCostEstimation> _PrgCostEstRepository;
        private readonly IRepository<Checklist> _checkListRepository;
        private readonly IRepository<ChecklistItem> _CheklistItemRepository;
        private readonly IRepository<Role> _abproleRepository;
        private readonly IRepository<SubActivityList> _SubActivityListRepository;
        private readonly IRepository<SubActivityQuestionaryMapping> _SubActivityQuestionaryMappingRepository;
        private readonly IRepository<Questionarie> _QuestionarieRepository;
        private readonly IRepository<prgImplementationPlanQuestionariesMapping> _prgImplementationPlanQuestionariesMappingRepository;
        private readonly IRepository<prgImplementationPlanQuestionariesUrlMapping> _prgImplementationPlanQuestionariesUrlMappingRepository;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;

        public LoginAndroidService(IRepository<User, long> userRepository, RoleManager roleManager,
            IRepository<Program> programRepository, IRepository<ImplementationPlanCheckList> ImplementationPlanCheckListRepository,
            IRepository<PrgActionAreaActivityMapping> PrgActionAreaActivityMappingRepository,
            IRepository<Activity> ActivityRepository, UserManager userManager,
            IRepository<ChecklistItem> ChecklistItemRepository, IRepository<UnitofMeasure> unitofMeasureRepository,
            IComponentActivityMappingRepository ProgramComponentActivityMappingRepository,
            IRepository<ExpensesType> ExpensesTypeRepository, IRepository<ProgramQuqterUnitMapping> ProgramQuqterUnitMappingRepository,
            IBinaryObjectManager binaryObjectManager, IRepository<ProgramExpense> ProgramExpenseRepository,
            IRepository<ProgramExpenseImage> ProgramExpenseImageRepository, IRepository<RequestAAplas> RequestAAplasRepository,
             IRepository<Location> LocationRepository, IRepository<Program> ProgrameRepository, IRepository<ProgramCostEstimation> PrgCostEstRepository
             , IRepository<Checklist> checkListRepository, IRepository<ChecklistItem> CheklistItemRepository,
             IRepository<Role> abproleRepository, IRepository<ProgramComponentsActivitesMapping> ProgramComponentsActivitesMappingRepository,
             IRepository<SubActivityList> SubActivityListRepository, IRepository<SubActivityQuestionaryMapping> SubActivityQuestionaryMappingRepository,
             IRepository<Questionarie> QuestionarieRepository,
             IRepository<prgImplementationPlanQuestionariesMapping> prgImplementationPlanQuestionariesMappingRepository,
             IRepository<prgImplementationPlanQuestionariesUrlMapping> prgImplementationPlanQuestionariesUrlMappingRepository)
        {
            _roleManager = roleManager;
            _userRepository = userRepository;
            _programRepository = programRepository;
            _ImplementationPlanCheckListRepository = ImplementationPlanCheckListRepository;
            _PrgActionAreaActivityMappingRepository = PrgActionAreaActivityMappingRepository;
            _ActivityRepository = ActivityRepository;
            _ChecklistItemRepository = ChecklistItemRepository;
            _unitofMeasureRepository = unitofMeasureRepository;
            _ProgramComponentActivityMappingRepository = ProgramComponentActivityMappingRepository;
            _ExpensesTypeRepository = ExpensesTypeRepository;
            _RequestAAplasRepository = RequestAAplasRepository;
            _ProgramQuqterUnitMappingRepository = ProgramQuqterUnitMappingRepository;
            _binaryObjectManager = binaryObjectManager;
            _ProgramExpenseRepository = ProgramExpenseRepository;
            _ProgramExpenseImageRepository = ProgramExpenseImageRepository;
            _RequestAAplasRepository = RequestAAplasRepository;
            _LocationRepository = LocationRepository;
            _ProgrameRepository = ProgrameRepository;
            _PrgCostEstRepository = PrgCostEstRepository;
            _checkListRepository = checkListRepository;
            _CheklistItemRepository = CheklistItemRepository;
            _abproleRepository = abproleRepository;
            _ProgramComponentsActivitesMappingRepository = ProgramComponentsActivitesMappingRepository;
            _SubActivityListRepository = SubActivityListRepository;
            _SubActivityQuestionaryMappingRepository = SubActivityQuestionaryMappingRepository;
            _QuestionarieRepository = QuestionarieRepository;
            _prgImplementationPlanQuestionariesMappingRepository = prgImplementationPlanQuestionariesMappingRepository;
            _prgImplementationPlanQuestionariesUrlMappingRepository = prgImplementationPlanQuestionariesUrlMappingRepository;
            _userManager = userManager;
        }


        //public async Task<ListResultDto<RoleListDto>> GetAllRoles()
        //{
        //    var roles = await _roleManager
        //        .Roles

        //        .ToListAsync();

        //    return new ListResultDto<RoleListDto>(ObjectMapper.Map<List<RoleListDto>>(roles));
        //}

        public List<loginUserDetailsDto> GetLoginInfoforAndroid(string mobilenumber, string name, int TenantId)
        {

            var query = _ProgramComponentActivityMappingRepository.GetLoginInfo(mobilenumber, name, TenantId);

            return query;
        }

        public List<ProjectDetilsDto> GetProjectDetailsForAndroid(int userId)
        {
            List<int> pId = (from i in _ImplementationPlanCheckListRepository.GetAll() where (i.AssingUserId == userId) select i.ProgramID).ToList();


            var query = (from i in _ImplementationPlanCheckListRepository.GetAll().Where(y => y.AssingUserId == userId)
                         group i by new
                         {
                             i.ProgramID,
                             i.Program
                         }
                         into gcs
                         select new ProjectDetilsDto()
                         {
                             ProgramName = gcs.Key.Program.Name,
                             Id = gcs.Key.Program.Id,
                             ProgramStartDate = gcs.Key.Program.ProgramStartDate,
                             PrgramEndDate = gcs.Key.Program.PrgramEndDate,
                             prgimage = gcs.Key.Program.ProgramBadgeImage

                         }).ToList();





            return query;




        }


        public async Task<List<SubActivityForAndri>> GetSubActivityForAndri(int userId, int TenantId, int programId, int PrgActionAreaActivityMappingID, string role)
        {
            try
            {


                //var query = _ProgramComponentActivityMappingRepository.GetSubActivityForAndri(userId, TenantId, programId, PrgActionAreaActivityMappingID, role);
                var exp = _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == PrgActionAreaActivityMappingID && pq.ProgramID == programId).Select(t => t.Amount);

                var query = (from i in _ImplementationPlanCheckListRepository.GetAll().Include(t => t.UnitOfMeasures).Include(t => t.ChecklistItem)
                             join u in _userManager.Users on i.CreatorUserId equals u.Id
                             join r in _RequestAAplasRepository.GetAll().Include(t => t.Village) on i.PrgActionAreaActivityMappingID equals r.PrgActionAreaActivityMappingID
                             where (i.AssingUserId == userId && i.ProgramID == programId && i.TenantId == TenantId && i.PrgActionAreaActivityMappingID == PrgActionAreaActivityMappingID)

                             select new SubActivityForAndri
                             {
                                 SubActivityname = i.ChecklistItem.Name,
                                 Unitofmeasure = i.UnitOfMeasures.Name,
                                 Description = i.Description,
                                 startDate = i.CheklistStartDate,
                                 EndDate = i.CheklistEndDate,
                                 LastModificationTime = i.status == SubActivityStatus.Completed ? i.LastModificationTime : null,
                                 status = i.status,
                                 Id = i.Id,
                                 ManagerId = Convert.ToInt32(i.CreatorUserId),
                                 ProgramID = i.ProgramID,
                                 CostEstimationYear = i.CostEstimationYear,
                                 QuarterID = i.PrgQuarter,
                                 PrgActionAreaActivityMappingID = i.PrgActionAreaActivityMappingID,
                                 ProgramQuqterUnitMappingID = Convert.ToInt32(i.ProgramQuqterUnitMappingID),
                                 unit = i.Units,
                                 AchieveUnits = i.AchieveUnits,
                                 SubActivityId = i.ChecklistItemID.Value,
                                 ManagerName = u.Name + ' ' + u.Surname,
                                 AssingtoUser = u.Name + ' ' + u.Surname,
                                 VillageId = r.VillageID,
                                 VillageName = r.VillageID == null ? string.Empty : r.Village.Name,
                                 TotalCost = r.TotalUnitCost,
                                 RemainingBalance = r.TotalUnitCost - exp.DefaultIfEmpty(0).Sum(),
                             });

                var list = await query.ToListAsync();
                // change for distinct record

                var costEstimationYears = list.GroupBy(t => new { t.CostEstimationYear, t.QuarterID }).Select(t =>
                  new
                  {
                      CostEstimationYear = t.Key.CostEstimationYear,
                      QuarterID = t.Key.QuarterID
                  }).ToList();
                var listToSend = new List<SubActivityForAndri>();
                foreach (var year in costEstimationYears)
                {
                    list.ForEach(z => {
                        if (z.CostEstimationYear == year.CostEstimationYear && z.QuarterID == year.QuarterID) {
                           if (!listToSend.Any(y => y.SubActivityId == z.SubActivityId)) { 
                            listToSend.Add(z);
                          }
                        }
                    });
                    // For mobile side it is showing only 1st activity (sachin 20/5/20)
                    // listToSend.Add(list.Where(z => z.CostEstimationYear == year.CostEstimationYear && z.QuarterID == year.QuarterID).FirstOrDefault());
                }


                return listToSend;
                //    return list;
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public List<TaskDto> GetAllTask(int userID)
        {
            var query = (from i in _ImplementationPlanCheckListRepository.GetAll()
                         join ch in _ChecklistItemRepository.GetAll() on i.ChecklistItemID equals ch.Id
                         where (i.AssingUserId == userID)
                         select new TaskDto
                         {
                             startDate = i.CheklistStartDate,
                             EndDate = i.CheklistEndDate,
                             Description = i.Description,
                             SubActivityName = ch.Name,
                             Id = i.Id,
                             status = i.status,
                             LastModificationTime = i.LastModificationTime
                         }).ToList();

            foreach (var item in query)
            {
                if (item.status == SubActivityStatus.Completed)
                {
                    item.LastModificationTime = item.LastModificationTime;
                }
                else
                {
                    item.LastModificationTime = null;
                }
            }

            return query;
        }


        public String UpdateStatusOfSubActivityForAndroid(int ImplementationPlanCheckListID, int status)
        {
            string msg = "";

            var query = (from i in _ImplementationPlanCheckListRepository.GetAll().Where(i => i.Id == ImplementationPlanCheckListID) select i);
            foreach (var item in query)
            {
                ImplementationPlanCheckList imp = new ImplementationPlanCheckList();


                if (status == 2)
                {
                    item.status = SubActivityStatus.InProgress;
                    msg = "InProgress";
                }
                else if (status == 3)
                {
                    item.status = SubActivityStatus.Completed;
                    msg = "Completed";
                }
                else if (status == 4)
                {
                    item.status = SubActivityStatus.Overdue;
                    msg = "Overdue";
                }

                _ImplementationPlanCheckListRepository.Update(item);
            }

            return msg;
        }


        public List<UnitofMeasureDto> GetAllUnitOfMeasureforAndroid(int TenantId)
        {

            var query = _ProgramComponentActivityMappingRepository.GetUnitofmeasures(TenantId);

            return new List<UnitofMeasureDto>(ObjectMapper.Map<List<UnitofMeasureDto>>(query));

        }

        public List<ExpensesTypeDto> GetAllExpensesTypeforAndroid(int TenantId)
        {
            var Query = _ExpensesTypeRepository.GetAll().ToList().Where(t => t.TenantId == TenantId).OrderByDescending(e => e.TypeName);

            return new List<ExpensesTypeDto>(ObjectMapper.Map<List<ExpensesTypeDto>>(Query));
        }


        public List<SubActivityDto> GetActivityByprogramIdForAndroidUser(int programId, int userId)
        {

            var query = (from i in _ImplementationPlanCheckListRepository.GetAll().Where(y => y.AssingUserId == userId && y.ProgramID == programId)
                         group i by new
                         {
                             i.PrgActionAreaActivityMappingID
                         }
                        into gcs
                         join pac in _PrgActionAreaActivityMappingRepository.GetAll()
                         on gcs.Key.PrgActionAreaActivityMappingID equals pac.Id
                         select new SubActivityDto()
                         {
                             SubActivityName = pac.Activity.Name,
                             Id = (int)pac.ActivityID,
                             prgActionAreaID = (int)gcs.Key.PrgActionAreaActivityMappingID
                         }).ToList();

            return query;
        }

        public string CreateORUpdateExpensesForAndroid(ExpenseDto Input)
        {
            Console.Write(Input);
            string mse = "";
            ProgramExpense prgExp = new ProgramExpense();
            try
            {
                if (Input.Id == 0)
                {
                    if (Input.PrgActionAreaActivityMappingID != null)
                    {
                        prgExp.ProgramID = Input.ProgramID;
                        prgExp.ExpensesYear = Input.ExpensesYear;
                        prgExp.PrgActionAreaActivityMappingID = Input.PrgActionAreaActivityMappingID;
                        prgExp.QuarterId = Input.QuarterId;
                        prgExp.ExpensesTypeID = Input.ExpensesTypeID;
                        prgExp.ExpenseDate = Input.ExpenseDate;
                        prgExp.Remark = Input.Remark;
                        prgExp.Unit = Input.Unit;
                        prgExp.ExpenseTitle = Input.ExpenseTitle;
                        prgExp.Amount = Input.Amount;
                        prgExp.Image = Input.Image;
                        prgExp.ProgramQuqterUnitMappingID = Input.ProgramQuqterUnitMappingID;
                        prgExp.UnitOfMeasuresID = Input.UnitOfMeasuresID;
                        prgExp.CreatorUserId = Input.CreatorUserId;
                        prgExp.ImplementationPlanCheckListID = Input.ImplementationPlanCheckListID;
                        prgExp.Status = Status1.Pending;
                        prgExp.ManagerID = Input.ManagerID;
                        prgExp.TenantId = Input.TenantId;

                        int proexid = _ProgramExpenseRepository.InsertAndGetId(prgExp);
                        if (proexid > 0)
                        {
                            foreach (var item in Input.Expensesimage)
                            {
                                ProgramExpenseImage image = new ProgramExpenseImage();
                                image.ProgramExpenseID = proexid;
                                image.image = item.image;
                                image.TenantId = Input.TenantId;
                                image.CreatorUserId = Input.CreatorUserId;

                                int imID = _ProgramExpenseImageRepository.InsertAndGetId(image);
                            }
                        }
                        mse = "SAVE";

                    }

                }
                else
                {
                    var QUERY = _ProgramExpenseRepository.GetAll().Where(E => E.Id == Input.Id).SingleOrDefault();


                    QUERY.ExpensesTypeID = Input.ExpensesTypeID;
                    QUERY.ExpenseDate = Input.ExpenseDate;
                    QUERY.Remark = Input.Remark;
                    QUERY.Unit = Input.Unit;
                    QUERY.ExpenseTitle = Input.ExpenseTitle;
                    QUERY.Amount = Input.Amount;

                    QUERY.UnitOfMeasuresID = Input.UnitOfMeasuresID;
                    QUERY.CreatorUserId = Input.CreatorUserId;

                    QUERY.Status = Status1.Pending;




                    _ProgramExpenseRepository.Update(QUERY);
                    mse = "UPDATE";

                    foreach (var item in Input.Expensesimage)
                    {
                        if (item.Id == 0)
                        {
                            ProgramExpenseImage image1 = new ProgramExpenseImage();
                            image1.ProgramExpenseID = Input.Id;
                            image1.image = item.image;
                            image1.TenantId = Input.TenantId;
                            image1.CreatorUserId = Input.CreatorUserId;

                            int imID = _ProgramExpenseImageRepository.InsertAndGetId(image1);
                            mse = "save";
                        }
                        else
                        {
                            var image = _ProgramExpenseImageRepository.GetAll().Where(i => i.Id == item.Id).SingleOrDefault();

                            image.ProgramExpenseID = Input.Id;
                            image.image = item.image;

                            image.CreatorUserId = Input.CreatorUserId;

                            _ProgramExpenseImageRepository.Update(image);
                            mse = "UPDATE";
                        }

                    }
                }


            }
            catch (Exception e)
            {
                mse = (e.ToString());

            }
            return mse;
        }


        public List<ExpenseDto> GetExpenseDetails(GetExpenseInput Input)
        {
            try
            {


                var query = (from e in _ProgramExpenseRepository.GetAll()
                             where (e.CreatorUserId == Input.CreatorUserId
                             && e.TenantId == Input.TenantId && e.ProgramID == Input.ProgramID
                             && e.PrgActionAreaActivityMappingID == Input.PrgActionAreaActivityMappingID
                             && e.QuarterId == Input.QuarterId
                             && e.ImplementationPlanCheckListID == Input.ImplementationPlanCheckListID)

                             select new ExpenseDto
                             {
                                 ExpenseTitle = e.ExpenseTitle,
                                 TenantId = e.TenantId,
                                 ExpensesYear = e.ExpensesYear,
                                 QuarterId = e.QuarterId,
                                 ExpensesTypeID = e.ExpensesTypeID,
                                 ExpenseDate = e.ExpenseDate,
                                 Remark = e.Remark,
                                 Unit = e.Unit,
                                 Amount = e.Amount,
                                 status = e.Status,
                                 ManagerName = UserManager.Users.Where(U => U.Id == e.ManagerID).FirstOrDefault().FullName,
                                 ExpensesTypeName = e.ExpensesType.TypeName,
                                 UnitOfMeasuresIDName = e.UnitOfMeasures.Name,
                                 Id = e.Id,
                                 UnitOfMeasuresID = e.UnitOfMeasuresID,
                                 subActivityName = _ImplementationPlanCheckListRepository.GetAll().Where(i => i.Id == Input.ImplementationPlanCheckListID).SingleOrDefault().ChecklistItem.Name,
                                 Expensesimage = (from image in _ProgramExpenseImageRepository.GetAll()
                                                  where (image.ProgramExpenseID == e.Id)
                                                  select new expensesimage
                                                  {
                                                      image = image.image,
                                                      Id = image.Id
                                                  }).ToList(),

                             }).ToList().OrderByDescending(e => e.ExpenseDate);

                return new List<ExpenseDto>(ObjectMapper.Map<List<ExpenseDto>>(query));
            }
            catch (Exception e)
            {

                throw e;
            }


        }


        public string UpdateSubActivityUnits(int ImplementationPlanCheckListID, decimal AchieveUnit)
        {
            string msg = "";

            var item = _ImplementationPlanCheckListRepository.GetAll().Where(i => i.Id == ImplementationPlanCheckListID).SingleOrDefault();

            //   ImplementationPlanCheckList imp = new ImplementationPlanCheckList();
            if (item.AchieveUnits == null)
            {
                item.AchieveUnits = 0;
            }
            //item.AchieveUnits = AchieveUnit + item.AchieveUnits;
            item.AchieveUnits = AchieveUnit;

            if (item.Units <= item.AchieveUnits && item.CheklistEndDate > DateTime.Today)
            {
                item.status = SubActivityStatus.Completed;
                msg = "Completed";
            }
            else if (item.Units <= item.AchieveUnits && item.CheklistEndDate < DateTime.Today)
            {
                item.status = SubActivityStatus.Overdue;
                msg = "Overdue";
            }
            else
            {
                item.status = SubActivityStatus.InProgress;
                msg = "InProgress";
            }

            _ImplementationPlanCheckListRepository.Update(item);

            _ProgramComponentActivityMappingRepository.UpdatesUnit();
            var query1 = _ImplementationPlanCheckListRepository.GetAll().Where(im => im.ProgramQuqterUnitMappingID == item.ProgramQuqterUnitMappingID && (im.status == SubActivityStatus.NotStarted || im.status == SubActivityStatus.InProgress)).ToList();
            var programUnit = _ProgramQuqterUnitMappingRepository.GetAll().Where(pu => pu.Id == item.ProgramQuqterUnitMappingID).SingleOrDefault();
            programUnit.Status = query1.Count() == 0 ? ProgramQuarterUnitMappingStatus.Completed : ProgramQuarterUnitMappingStatus.Inprogress;
            _ProgramQuqterUnitMappingRepository.Update(programUnit);

            return msg;
        }


        public string UpdatecommunitycontributionReceived(int Preimpimantationactionareaid, int ProgramId, string costEstimationYear, decimal CommunitycontributionRecevied, long managerid)
        {
            string msg = "Success";




            var item = _RequestAAplasRepository.GetAll().Where(i => i.PrgActionAreaActivityMappingID == Preimpimantationactionareaid && i.ProgramID == ProgramId && i.CostEstimationYear == costEstimationYear && i.ManagerID == managerid && i.VillageID == null).SingleOrDefault();

            //   ImplementationPlanCheckList imp = new ImplementationPlanCheckList();
            if (item.CommunityContributionRecived == null)
            {
                item.CommunityContributionRecived = 0;
            }
            //item.AchieveUnits = AchieveUnit + item.AchieveUnits;
            item.CommunityContributionRecived = CommunitycontributionRecevied + item.CommunityContributionRecived;



            _RequestAAplasRepository.Update(item);



            return msg;
        }


        public List<ImplementationListDto> GetManagerprojectforAndroid(long? userId)
        {
            long? managerId = userId;

            var Query = (from r in _RequestAAplasRepository.GetAll().Where(p => p.RequestStatus == RequestStatus.Approved && p.ManagerID == managerId)
                         group r by new
                         {
                             r.LocationID,
                             r.ManagerID,
                             r.ProgramID,
                             r.RequestStatus,

                         } into gcs
                         join l in _LocationRepository.GetAll()
                        on gcs.Key.LocationID equals l.Id
                         join p in _ProgrameRepository.GetAll()
                         on gcs.Key.ProgramID equals p.Id


                         select new ImplementationListDto()
                         {
                             ProgramName = p.Name,
                             ProgramID = p.Id,
                             ProgrameStartDate = p.ProgramStartDate,
                             ProgrameEndDate = p.PrgramEndDate,
                             LocationID = gcs.Key.LocationID,
                             ManagerID = gcs.Key.ManagerID,
                             ManagerName = UserManager.Users.Where(u => u.Id == gcs.Key.ManagerID).SingleOrDefault().Name,
                             LocationName = l.Name,
                             Id = l.Id,
                             RequestStatus = gcs.Key.RequestStatus,
                             CostEstimationYear = (from q in _RequestAAplasRepository.GetAll().Where(ma => ma.LocationID == gcs.Key.LocationID && ma.ProgramID == gcs.Key.ProgramID)
                                                   group q by new
                                                   {
                                                       q.CostEstimationYear
                                                   } into gcs1

                                                   select new PrgCostEstimationYearDto()
                                                   {
                                                       Id = l.Id,
                                                       CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                       ActionSubactionCost = (from a in _RequestAAplasRepository.GetAll().Where(mn => mn.LocationID == gcs.Key.LocationID
                                                                              && mn.CostEstimationYear == gcs1.Key.CostEstimationYear && mn.ProgramID == gcs.Key.ProgramID)

                                                                              join pa in _PrgActionAreaActivityMappingRepository.GetAll()
                                                                              on a.PrgActionAreaActivityMappingID equals pa.Id

                                                                              join pc in _PrgCostEstRepository.GetAll()
                                                                              on pa.ActivityID equals pc.ActivityID
                                                                              where (pc.ComponentID == pa.ComponentID && pc.CostEstimationYear == gcs1.Key.CostEstimationYear && pc.ProgramID == gcs.Key.ProgramID)

                                                                              select new ListActionSubactionListImplemenTationDto()
                                                                              {
                                                                                  Id = a.Id,
                                                                                  ActionAreaName = a.VillageID != null ? string.Empty : pa.ActionArea.Name,
                                                                                  SubActionAreaName = a.VillageID != null ? string.Empty : pa.SubActionArea.Name,
                                                                                  ActivityName = a.VillageID != null ? string.Empty : pa.Activity.Name,
                                                                                  ActivityId = pa.Activity.Id,
                                                                                  LocationID = a.LocationID,
                                                                                  ProgramID = a.ProgramID,
                                                                                  CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                                                  ActivityLevel = pc.ActivityLevel,
                                                                                  VillageID = a.VillageID,
                                                                                  VillageName = a.Village.Name,
                                                                                  UnitOfMeasuresName = a.UnitOfMeasures.Name,
                                                                                  UnitOfMeasuresID = a.UnitOfMeasuresID,

                                                                                  PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                  TotalUnits = a.TotalUnits,
                                                                                  UnitCost = a.UnitCost,
                                                                                  TotalUnitCost = a.TotalUnitCost,
                                                                                  CommunityContribution = a.CommunityContribution,
                                                                                  CommunityContributionRecived = a.CommunityContributionRecived,
                                                                                  FunderContribution = a.FunderContribution,
                                                                                  OtherContribution = a.OtherContribution,
                                                                                  RequestStatus = a.RequestStatus,
                                                                                  Quarter1PhysicalUnits = a.Quarter1PhysicalUnits,
                                                                                  Quarter1FinancialRs = a.Quarter1FinancialRs,

                                                                                  Quarter2PhysicalUnits = a.Quarter2PhysicalUnits,
                                                                                  Quarter2FinancialRs = a.Quarter2FinancialRs,

                                                                                  Quarter3PhysicalUnits = a.Quarter3PhysicalUnits,
                                                                                  Quarter3FinancialRs = a.Quarter3FinancialRs,

                                                                                  Quarter4PhysicalUnits = a.Quarter4PhysicalUnits,
                                                                                  Quarter4FinancialRs = a.Quarter4FinancialRs,
                                                                                  Quarter1WisePlan = (from qa in _ProgramQuqterUnitMappingRepository.GetAll().Where(qa => qa.ProgramID == a.ProgramID && qa.QuarterId == 1 && qa.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa.VillageID == a.VillageID)
                                                                                                      select new ListActionSubactionListImplemenTationDto
                                                                                                      {
                                                                                                          UnitId = qa.Id,
                                                                                                          Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                          Quarter1FinancialRs = qa.Rs,
                                                                                                          ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                          subUnitActivityQuarterWise = (from s in _ImplementationPlanCheckListRepository.GetAll().Where(su => su.ProgramQuqterUnitMappingID == qa.Id)
                                                                                                                                        select new subUnitActivityQuarterWise
                                                                                                                                        {
                                                                                                                                            UnitOfMeasureName = s.UnitOfMeasures.Name,
                                                                                                                                            AssignedUserName = UserManager.Users.Where(u => u.Id == s.AssingUserId).FirstOrDefault().FullName,
                                                                                                                                            ChecklistItemName = s.ChecklistItem.Name,
                                                                                                                                            ChecklistItemID = s.ChecklistItemID,
                                                                                                                                            Description = s.Description,
                                                                                                                                            Units = s.Units,
                                                                                                                                            AchieveUnits = s.AchieveUnits,
                                                                                                                                            CheklistStartDate = s.CheklistStartDate,
                                                                                                                                            CheklistEndDate = s.CheklistEndDate,
                                                                                                                                            ImplementationPlanCheckListID = s.Id,
                                                                                                                                            SubActivityStatus = s.status,
                                                                                                                                            CreatorUserId = s.CreatorUserId,
                                                                                                                                            Id = s.ChecklistItem.Id,
                                                                                                                                            ProgramID = s.Program.Id,
                                                                                                                                            CostEstimationYear = s.CostEstimationYear,
                                                                                                                                            QuarterID = s.PrgQuarter,
                                                                                                                                            ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                            PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMapping.Id,
                                                                                                                                            ManagerName = UserManager.Users.Where(u => u.Id == s.CreatorUserId).FirstOrDefault().FullName,

                                                                                                                                            RemainingBalance = _RequestAAplasRepository.GetAll().Where(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).FirstOrDefault().TotalUnitCost -
                                                                                                                                         _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                        }).ToList(),
                                                                                                      }).ToList(),
                                                                                  Quarter2WisePlan = (from qa in _ProgramQuqterUnitMappingRepository.GetAll().Where(qa1 => qa1.ProgramID == a.ProgramID && qa1.QuarterId == 2 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID)
                                                                                                      select new ListActionSubactionListImplemenTationDto
                                                                                                      {
                                                                                                          UnitId = qa.Id,
                                                                                                          Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                          Quarter1FinancialRs = qa.Rs,
                                                                                                          ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                          subUnitActivityQuarterWise = (from s in _ImplementationPlanCheckListRepository.GetAll().Where(su => su.ProgramQuqterUnitMappingID == qa.Id)
                                                                                                                                        select new subUnitActivityQuarterWise
                                                                                                                                        {
                                                                                                                                            UnitOfMeasureName = s.UnitOfMeasures.Name,
                                                                                                                                            AssignedUserName = UserManager.Users.Where(u => u.Id == s.AssingUserId).FirstOrDefault().FullName,
                                                                                                                                            ChecklistItemName = s.ChecklistItem.Name,
                                                                                                                                            ChecklistItemID = s.ChecklistItemID,
                                                                                                                                            Description = s.Description,
                                                                                                                                            Units = s.Units,
                                                                                                                                            AchieveUnits = s.AchieveUnits,
                                                                                                                                            CheklistStartDate = s.CheklistStartDate,
                                                                                                                                            CheklistEndDate = s.CheklistEndDate,
                                                                                                                                            ImplementationPlanCheckListID = s.Id,
                                                                                                                                            SubActivityStatus = s.status,
                                                                                                                                            CreatorUserId = s.CreatorUserId,
                                                                                                                                            Id = s.ChecklistItem.Id,
                                                                                                                                            ProgramID = s.Program.Id,
                                                                                                                                            CostEstimationYear = s.CostEstimationYear,
                                                                                                                                            QuarterID = s.PrgQuarter,
                                                                                                                                            ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                            PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMapping.Id,
                                                                                                                                            ManagerName = UserManager.Users.Where(u => u.Id == s.CreatorUserId).FirstOrDefault().FullName,
                                                                                                                                            RemainingBalance = _RequestAAplasRepository.GetAll().Where(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).FirstOrDefault().TotalUnitCost -
                                                                                                                                               _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                        }).ToList(),
                                                                                                      }).ToList(),
                                                                                  Quarter3WisePlan = (from qa in _ProgramQuqterUnitMappingRepository.GetAll().Where(qa1 => qa1.ProgramID == a.ProgramID && qa1.QuarterId == 3 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID)
                                                                                                      select new ListActionSubactionListImplemenTationDto
                                                                                                      {
                                                                                                          UnitId = qa.Id,
                                                                                                          Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                          Quarter1FinancialRs = qa.Rs,
                                                                                                          ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                          subUnitActivityQuarterWise = (from s in _ImplementationPlanCheckListRepository.GetAll().Where(su => su.ProgramQuqterUnitMappingID == qa.Id)
                                                                                                                                        select new subUnitActivityQuarterWise
                                                                                                                                        {
                                                                                                                                            UnitOfMeasureName = s.UnitOfMeasures.Name,
                                                                                                                                            AssignedUserName = UserManager.Users.Where(u => u.Id == s.AssingUserId).FirstOrDefault().FullName,
                                                                                                                                            ChecklistItemName = s.ChecklistItem.Name,
                                                                                                                                            ChecklistItemID = s.ChecklistItemID,
                                                                                                                                            Description = s.Description,
                                                                                                                                            Units = s.Units,
                                                                                                                                            AchieveUnits = s.AchieveUnits,
                                                                                                                                            CheklistStartDate = s.CheklistStartDate,
                                                                                                                                            CheklistEndDate = s.CheklistEndDate,
                                                                                                                                            ImplementationPlanCheckListID = s.Id,
                                                                                                                                            SubActivityStatus = s.status,
                                                                                                                                            CreatorUserId = s.CreatorUserId,
                                                                                                                                            Id = s.ChecklistItem.Id,
                                                                                                                                            ProgramID = s.Program.Id,
                                                                                                                                            CostEstimationYear = s.CostEstimationYear,
                                                                                                                                            QuarterID = s.PrgQuarter,
                                                                                                                                            ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                            PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMapping.Id,
                                                                                                                                            ManagerName = UserManager.Users.Where(u => u.Id == s.CreatorUserId).FirstOrDefault().FullName,
                                                                                                                                            RemainingBalance = _RequestAAplasRepository.GetAll().Where(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).FirstOrDefault().TotalUnitCost -
                                                                                                                                               _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                        }).ToList(),
                                                                                                      }).ToList(),
                                                                                  Quarter4WisePlan = (from qa in _ProgramQuqterUnitMappingRepository.GetAll().Where(qa1 => qa1.ProgramID == a.ProgramID && qa1.QuarterId == 4 && qa1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && qa1.VillageID == a.VillageID)
                                                                                                      select new ListActionSubactionListImplemenTationDto
                                                                                                      {
                                                                                                          UnitId = qa.Id,
                                                                                                          Quarter1PhysicalUnits = qa.NosOfUnit,
                                                                                                          Quarter1FinancialRs = qa.Rs,
                                                                                                          ProgramQuarterUnitMappingStatus = qa.Status,
                                                                                                          subUnitActivityQuarterWise = (from s in _ImplementationPlanCheckListRepository.GetAll().Where(su => su.ProgramQuqterUnitMappingID == qa.Id)
                                                                                                                                        select new subUnitActivityQuarterWise
                                                                                                                                        {
                                                                                                                                            UnitOfMeasureName = s.UnitOfMeasures.Name,
                                                                                                                                            AssignedUserName = UserManager.Users.Where(u => u.Id == s.AssingUserId).FirstOrDefault().FullName,
                                                                                                                                            ChecklistItemName = s.ChecklistItem.Name,
                                                                                                                                            ChecklistItemID = s.ChecklistItemID,
                                                                                                                                            Description = s.Description,
                                                                                                                                            Units = s.Units,
                                                                                                                                            AchieveUnits = s.AchieveUnits,
                                                                                                                                            CheklistStartDate = s.CheklistStartDate,
                                                                                                                                            CheklistEndDate = s.CheklistEndDate,
                                                                                                                                            ImplementationPlanCheckListID = s.Id,
                                                                                                                                            SubActivityStatus = s.status,
                                                                                                                                            CreatorUserId = s.CreatorUserId,
                                                                                                                                            Id = s.ChecklistItem.Id,
                                                                                                                                            ProgramID = s.Program.Id,
                                                                                                                                            CostEstimationYear = s.CostEstimationYear,
                                                                                                                                            QuarterID = s.PrgQuarter,
                                                                                                                                            ProgramQuqterUnitMappingID = s.ProgramQuqterUnitMappingID,
                                                                                                                                            PrgActionAreaActivityMappingID = s.PrgActionAreaActivityMapping.Id,
                                                                                                                                            ManagerName = UserManager.Users.Where(u => u.Id == s.CreatorUserId).FirstOrDefault().FullName,
                                                                                                                                            RemainingBalance = _RequestAAplasRepository.GetAll().Where(re => re.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && re.ProgramID == s.Program.Id).FirstOrDefault().TotalUnitCost -
                                                                                                                                               _ProgramExpenseRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == s.PrgActionAreaActivityMapping.Id && pq.ProgramID == s.Program.Id).Select(t => t.Amount).DefaultIfEmpty(0).Sum(),
                                                                                                                                        }).ToList(),
                                                                                                      }).ToList(),

                                                                                  Managers = (from u in UserManager.Users
                                                                                              join l in _LocationRepository.GetAll().Where(lc => lc.Id == gcs.Key.LocationID)
                                                                                              on true equals true
                                                                                              where (l.Id == u.LocationId || l.ParentLocation == u.LocationId)
                                                                                              select new PrgManagerImplentationListDto()
                                                                                              {
                                                                                                  Name = u.FullName,
                                                                                                  ManagerId = u.Id,
                                                                                              }).ToList(),


                                                                                  SubActivityandIteams = (from ch in _checkListRepository.GetAll().Where(c => c.ActivityID == pa.Activity.Id)
                                                                                                          select new SubActivityandIteams()
                                                                                                          {
                                                                                                              ChecklistID = ch.Id,
                                                                                                              ChecklistName = ch.ChecklistName,
                                                                                                              Description = ch.Description,
                                                                                                              ActivityID = ch.ActivityID,
                                                                                                              ActivityName = ch.Activity.Name,
                                                                                                              ProgramID = p.Id,
                                                                                                              PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                                              Selectediteams = new List<cheklistandSublistDto1>(),
                                                                                                              iteam = (from chI in _CheklistItemRepository.GetAll().Where(ci => ci.ChecklistID == ch.Id)
                                                                                                                       join ch1 in _checkListRepository.GetAll()
                                                                                                                 on chI.ChecklistID equals ch1.Id
                                                                                                                       select new cheklistandSublistDto1()
                                                                                                                       {
                                                                                                                           CheklisiteamtName = chI.Name,
                                                                                                                           checklistID_subiteamTable = chI.Id,
                                                                                                                       }).ToList(),
                                                                                                          }).ToList(),

                                                                              }).OrderBy(pe => pe.PrgActionAreaActivityMappingID).ToList(),
                                                   }).ToList(),

                         }).ToList();
            return new List<ImplementationListDto>(ObjectMapper.Map<List<ImplementationListDto>>(Query));

        }


        public List<ProjectDetilsDto> GetProjectDetailsForAndroid101(int userId, int? tenentId, string role)
        {
            var projectManagerProject = new List<ProjectDetilsDto>();


            //List<int> pId = (from i in _ImplementationPlanCheckListRepository.GetAll() where (i.AssingUserId == userId) select i.ProgramID).ToList();

            //int? useroleid = UserManager.Users.Where(u => u.Id == userId && u.TenantId == tenentId).Select(u => u.UserRole).SingleOrDefault();
            //int? roleid = _abproleRepository.GetAll().Where(r => r.Name == "ProjectManager").FirstOrDefault()?.Id;
            //int? rrcId = _abproleRepository.GetAll().Where(r => r.Name == "RRcIncharge").FirstOrDefault()?.Id;
            //int? prgMangId = _abproleRepository.GetAll().Where(r => r.Name == "ProgramManager").FirstOrDefault()?.Id;


            if (role == "ProjectManager" || role == "RRcIncharge")
            {
                projectManagerProject = (from re in _RequestAAplasRepository.GetAll().Where(r => r.ManagerID == userId)
                                         group re by new
                                         {
                                             re.ProgramID,
                                             re.Program
                                         } into gcs
                                         select new ProjectDetilsDto()
                                         {
                                             ProgramName = gcs.Key.Program.Name,
                                             Id = gcs.Key.Program.Id,
                                             ProgramStartDate = gcs.Key.Program.ProgramStartDate,
                                             PrgramEndDate = gcs.Key.Program.PrgramEndDate,
                                             prgimage = gcs.Key.Program.ProgramBadgeImage
                                         }).ToList();


            }
            else if (role == "ProgramManager")
            {
                projectManagerProject = (from p in _programRepository.GetAll().Where(p => p.ManagerID == userId)
                                         select new ProjectDetilsDto()
                                         {
                                             ProgramName = p.Name,
                                             Id = p.Id,
                                             ProgramStartDate = p.ProgramStartDate,
                                             PrgramEndDate = p.PrgramEndDate,
                                             prgimage = p.ProgramBadgeImage
                                         }).ToList();

            }
            else
            {
                projectManagerProject = (from i in _ImplementationPlanCheckListRepository.GetAll().Where(y => y.AssingUserId == userId)
                                         group i by new
                                         {
                                             i.ProgramID,
                                             i.Program
                                         }
                        into gcs
                                         select new ProjectDetilsDto()
                                         {
                                             ProgramName = gcs.Key.Program.Name,
                                             Id = gcs.Key.Program.Id,
                                             ProgramStartDate = gcs.Key.Program.ProgramStartDate,
                                             PrgramEndDate = gcs.Key.Program.PrgramEndDate,
                                             prgimage = gcs.Key.Program.ProgramBadgeImage

                                         }).ToList();



            }




            return projectManagerProject;




        }

        public List<SubActivityDto> GetActivityByprogramIdForAndroidUser101(int programId, int userId, string role)
        {
            var Activity = new List<SubActivityDto>();

            if (role == "ProjectManager" || role == "RRcIncharge")
            {
                Activity = (from a in _RequestAAplasRepository.GetAll().Where(p => p.ProgramID == programId && p.ManagerID == userId && p.VillageID == null)

                            join pa in _PrgActionAreaActivityMappingRepository.GetAll()
                            on a.PrgActionAreaActivityMappingID equals pa.Id
                            join pc in _PrgCostEstRepository.GetAll()
                            on pa.ActivityID equals pc.ActivityID
                            where (pc.ComponentID == pa.ComponentID && pc.ProgramID == pa.ProgramID)
                            group a by new
                            {
                                a.PrgActionAreaActivityMappingID,
                                a.PrgActionAreaActivityMapping,
                                a.VillageID,
                                pa.ActivityID,
                                pa.Activity

                            } into gcs

                            select new SubActivityDto()
                            {
                                ActivityId = gcs.Key.ActivityID,

                                SubActivityName = gcs.Key.VillageID != null ? string.Empty : gcs.Key.Activity.Name,
                                prgActionAreaID = (int)gcs.Key.PrgActionAreaActivityMappingID

                            }).ToList();
            }
            else if (role == "ProgramManager")
            {






                Activity = (from pm in _ProgramComponentsActivitesMappingRepository.GetAll().Where(p => p.ProgramID == programId)
                            join ar in _PrgActionAreaActivityMappingRepository.GetAll().Where(p => p.ProgramID == programId)
                            on new { f1 = pm.ComponentID, f2 = pm.ActivityID, f3 = pm.ProgramID } equals new { f1 = ar.ComponentID, f2 = ar.ActivityID, f3 = ar.ProgramID } into temp
                            from x in temp.DefaultIfEmpty()

                            group x by new
                            {
                                x.Component,
                                x.ComponentID,
                                x.Activity,
                                x.ActivityID,
                                x.Id
                            } into gcs
                            select new SubActivityDto()
                            {
                                ActivityId = gcs.Key.ActivityID,

                                SubActivityName = gcs.Key.Activity.Name,
                                prgActionAreaID = (int)gcs.Key.Id

                            }).ToList();

            }
            else
            {


                Activity = (from i in _ImplementationPlanCheckListRepository.GetAll().Where(y => y.AssingUserId == userId && y.ProgramID == programId)
                            group i by new
                            {
                                i.PrgActionAreaActivityMappingID
                            }
                                into gcs
                            join pac in _PrgActionAreaActivityMappingRepository.GetAll()
                            on gcs.Key.PrgActionAreaActivityMappingID equals pac.Id
                            select new SubActivityDto()
                            {
                                SubActivityName = pac.Activity.Name,
                                Id = (int)pac.ActivityID,
                                prgActionAreaID = (int)gcs.Key.PrgActionAreaActivityMappingID
                            }).ToList();
            }





            return Activity;
        }


        public List<QuestionaryListDto> GetQuestionaryBySubActivity(int? subActivityId)
        {

            var query = (from che in _CheklistItemRepository.GetAll().Where(ch => ch.Id == subActivityId)
                         join subAct in _SubActivityListRepository.GetAll()
                         on che.SubActivityId equals subAct.Id
                         join sub in _SubActivityQuestionaryMappingRepository.GetAll()
                         on subAct.Id equals sub.SubActivityId
                         join q in _QuestionarieRepository.GetAll()
                         on sub.QuestionarieID equals q.Id
                         select new QuestionaryListDto()
                         {
                             Question = q.Name,
                             InputType = q.InputType,
                             QuestionId = q.Id
                         }).ToList();
            return query;

        }

        public string CreatOrUpdateQuestionaryBySubActivity(CreatOrUpdateQuestionaryListDto Input)
        {
            string mse = "";
            int prgQansId = 0;
            int prgUrlId = 0;


            if (Input.Id == 0)
            {
                foreach (var item in Input.Questionary)
                {
                    if (item.QuestionarymappngId == 0)
                    {
                        prgImplementationPlanQuestionariesMapping prgQans = new prgImplementationPlanQuestionariesMapping();

                        prgQans.QuestionarieID = item.QuestionaryId;
                        prgQans.Answer = item.answer;
                        prgQans.ImplementationPlanCheckListID = Input.PrgImplementationsPlansID;
                        prgQans.TenantId = Input.TenantId;
                        prgQans.ProgramID = Input.ProgramId;
                        prgQans.Status = prgImplementationPlanQuestionariesMappingStatus.pending;
                        prgQans.CreatorUserId = Input.CreatorUserId;



                        prgQansId = _prgImplementationPlanQuestionariesMappingRepository.InsertAndGetId(prgQans);
                        if (prgQansId > 0)
                        {
                            mse = "Save";
                        }
                    }
                    else
                    {
                        var Q = _prgImplementationPlanQuestionariesMappingRepository.GetAll().Where(q => q.Id == item.QuestionarymappngId).SingleOrDefault();


                        Q.Answer = item.answer;
                        Q.Status = prgImplementationPlanQuestionariesMappingStatus.pending;
                        Q.CreatorUserId = Input.CreatorUserId;
                        _prgImplementationPlanQuestionariesMappingRepository.Update(Q);
                        mse = "Update";
                    }



                }

                foreach (var item in Input.url)
                {
                    if (item.urlTableId == 0)
                    {
                        if (prgQansId > 0)
                        {
                            prgImplementationPlanQuestionariesUrlMapping prgUrl = new prgImplementationPlanQuestionariesUrlMapping();
                            prgUrl.ImplementationPlanCheckListID = Input.PrgImplementationsPlansID;
                            prgUrl.url = item.url;
                            prgUrl.TenantId = Input.TenantId;
                            prgUrl.CreatorUserId = Input.CreatorUserId;
                            prgUrl.QuestionaryId = item.questionryId;

                            prgUrlId = _prgImplementationPlanQuestionariesUrlMappingRepository.InsertAndGetId(prgUrl);
                            if (prgUrlId > 0)
                            {
                                mse = "Save";
                            }
                        }
                        else
                        {

                            if (Input.Questionary[0].QuestionarymappngId != 0)
                            {

                                prgImplementationPlanQuestionariesUrlMapping prgUrl = new prgImplementationPlanQuestionariesUrlMapping();
                                prgUrl.ImplementationPlanCheckListID = Input.PrgImplementationsPlansID;
                                prgUrl.url = item.url;
                                prgUrl.TenantId = Input.TenantId;
                                prgUrl.CreatorUserId = Input.CreatorUserId;
                                prgUrl.QuestionaryId = item.questionryId;


                                prgUrlId = _prgImplementationPlanQuestionariesUrlMappingRepository.InsertAndGetId(prgUrl);
                                if (prgUrlId > 0)
                                {
                                    mse = "Update";
                                }
                            }

                        }



                    }
                    else
                    {

                        var urlrecord = _prgImplementationPlanQuestionariesUrlMappingRepository.GetAll().Where(q => q.Id == item.urlTableId).SingleOrDefault();

                        urlrecord.url = item.url;
                        urlrecord.CreatorUserId = Input.CreatorUserId;

                        _prgImplementationPlanQuestionariesUrlMappingRepository.Update(urlrecord);
                        mse = "Update";


                    }


                }




            }




            return mse;

        }

        public List<GetQuestionandAnswerBySubActivityDto> GetQuestionaryBySubActivityandAnswer(int? subActivityId, int? implementationplanId)
        {
            int? SubActId = _ChecklistItemRepository.GetAll().Where(xx => xx.Id == subActivityId).FirstOrDefault().SubActivityId;
            string imp = _prgImplementationPlanQuestionariesMappingRepository.GetAll().Where(aa => aa.ImplementationPlanCheckListID == implementationplanId && aa.QuestionarieID == 1).ToList().Count() == 0 ? "bnnnn" : _prgImplementationPlanQuestionariesMappingRepository.GetAll().Where(aa => aa.ImplementationPlanCheckListID == implementationplanId && aa.QuestionarieID == 1).FirstOrDefault().Answer;
            var a = _SubActivityQuestionaryMappingRepository.GetAll().Where(xx => xx.SubActivityId == SubActId).ToList();
            var result = new List<GetQuestionandAnswerBySubActivityDto>();
            var i = new List<GetQuestionandAnswerBySubActivityDto>();
            foreach (var item in a)
            {


                i.Add(new GetQuestionandAnswerBySubActivityDto
                {
                    QuestionaryName = _QuestionarieRepository.GetAll().Where(z => z.Id == item.QuestionarieID).FirstOrDefault().Name,
                    QuestionaryId = _QuestionarieRepository.GetAll().Where(z => z.Id == item.QuestionarieID).FirstOrDefault().Id,
                    InputType = _QuestionarieRepository.GetAll().Where(z => z.Id == item.QuestionarieID).FirstOrDefault().InputType,

                    answer = _prgImplementationPlanQuestionariesMappingRepository.GetAll().Where(aa => aa.ImplementationPlanCheckListID == implementationplanId && aa.QuestionarieID == item.QuestionarieID).ToList().Count() == 0 ? null : _prgImplementationPlanQuestionariesMappingRepository.GetAll().Where(aa => aa.ImplementationPlanCheckListID == implementationplanId && aa.QuestionarieID == item.QuestionarieID).FirstOrDefault().Answer,
                    QuestionarymappngId = _prgImplementationPlanQuestionariesMappingRepository.GetAll().Where(imps => imps.ImplementationPlanCheckListID == implementationplanId && imps.QuestionarieID == item.QuestionarieID).ToList().Count() == 0 ? 0 : _prgImplementationPlanQuestionariesMappingRepository.GetAll().Where(impr => impr.ImplementationPlanCheckListID == implementationplanId && impr.QuestionarieID == item.QuestionarieID).Select(aa => aa.Id).DefaultIfEmpty(0).FirstOrDefault(),
                    Status = _prgImplementationPlanQuestionariesMappingRepository.GetAll().Where(imps => imps.ImplementationPlanCheckListID == implementationplanId && imps.QuestionarieID == item.QuestionarieID).ToList().Count() == 0 ? 0 : _prgImplementationPlanQuestionariesMappingRepository.GetAll().Where(impc => impc.ImplementationPlanCheckListID == implementationplanId && impc.QuestionarieID == item.QuestionarieID).Select(aa => aa.Status).DefaultIfEmpty().FirstOrDefault(),

                    url = (from url in _prgImplementationPlanQuestionariesUrlMappingRepository.GetAll().Where(impx => impx.ImplementationPlanCheckListID == implementationplanId && impx.QuestionaryId == item.QuestionarieID)
                           select new Url1
                           {
                               url = url.url,
                               urlTableId = url.Id
                           }).ToList(),
                });


            }


            //foreach (var e in i )
            //{
            //    if (e.InputType != "CheckBox")
            //    {
            //        e.url = null;
            //    }
            //}

            return i;
        }

    }
}


//public string CreateORUpdateExpensesForAndroid(ExpenseDto Input)
//{
//    Console.Write(Input);
//    string mse = "";
//    ProgramExpense prgExp = new ProgramExpense();
//    try
//    {


//        if (Input.PrgActionAreaActivityMappingID != null)
//        {


//            prgExp.ProgramID = Input.ProgramID;
//            prgExp.ExpensesYear = Input.ExpensesYear;
//            prgExp.PrgActionAreaActivityMappingID = Input.PrgActionAreaActivityMappingID;
//            prgExp.QuarterId = Input.QuarterId;
//            prgExp.ExpensesTypeID = Input.ExpensesTypeID;
//            prgExp.ExpenseDate = Input.ExpenseDate;
//            prgExp.Remark = Input.Remark;
//            prgExp.Unit = Input.Unit;
//            prgExp.ExpenseTitle = Input.ExpenseTitle;
//            prgExp.Amount = Input.Amount;
//            prgExp.Image = Input.Image;
//            prgExp.ProgramQuqterUnitMappingID = Input.ProgramQuqterUnitMappingID;
//            prgExp.UnitOfMeasuresID = Input.UnitOfMeasuresID;
//            prgExp.CreatorUserId = Input.CreatorUserId;
//            prgExp.ImplementationPlanCheckListID = Input.ImplementationPlanCheckListID;
//            prgExp.Status = Status1.Pending;
//            prgExp.ManagerID = Input.ManagerID;
//            prgExp.TenantId = Input.TenantId;

//            if (Input.Id == 0)
//            {
//                int proexid = _ProgramExpenseRepository.InsertAndGetId(prgExp);
//                if (proexid > 0)
//                {
//                    foreach (var item in Input.Expensesimage)
//                    {
//                        ProgramExpenseImage image = new ProgramExpenseImage();
//                        image.ProgramExpenseID = proexid;
//                        image.image = item.image;
//                        image.TenantId = Input.TenantId;
//                        image.CreatorUserId = Input.CreatorUserId;

//                        int imID = _ProgramExpenseImageRepository.InsertAndGetId(image);
//                    }
//                }
//                mse = "SAVE";
//            }
//            else
//            {
//                var QUERY = _ProgramExpenseRepository.GetAll().Where(E => E.Id == Input.Id).SingleOrDefault();

//                QUERY.ProgramID = Input.ProgramID;
//                QUERY.ExpensesYear = Input.ExpensesYear;
//                QUERY.PrgActionAreaActivityMappingID = Input.PrgActionAreaActivityMappingID;
//                QUERY.QuarterId = Input.QuarterId;
//                QUERY.ExpensesTypeID = Input.ExpensesTypeID;
//                QUERY.ExpenseDate = Input.ExpenseDate;
//                QUERY.Remark = Input.Remark;
//                QUERY.Unit = Input.Unit;
//                QUERY.ExpenseTitle = Input.ExpenseTitle;
//                QUERY.Amount = Input.Amount;
//                QUERY.Image = Input.Image;
//                QUERY.ProgramQuqterUnitMappingID = Input.ProgramQuqterUnitMappingID;
//                QUERY.UnitOfMeasuresID = Input.UnitOfMeasuresID;
//                QUERY.CreatorUserId = Input.CreatorUserId;
//                QUERY.ImplementationPlanCheckListID = Input.ImplementationPlanCheckListID;
//                QUERY.Status = Status1.Pending;
//                QUERY.ManagerID = Input.ManagerID;
//                QUERY.TenantId = Input.TenantId;



//                _ProgramExpenseRepository.Update(QUERY);

//                foreach (var item in Input.Expensesimage)
//                {
//                    if (item.Id == 0)
//                    {
//                        ProgramExpenseImage image1 = new ProgramExpenseImage();
//                        image1.ProgramExpenseID = Input.Id;
//                        image1.image = item.image;
//                        image1.TenantId = Input.TenantId;
//                        image1.CreatorUserId = Input.CreatorUserId;

//                        int imID = _ProgramExpenseImageRepository.InsertAndGetId(image1);
//                    }
//                    else
//                    {
//                        var image = _ProgramExpenseImageRepository.GetAll().Where(i => i.Id == item.Id).SingleOrDefault();

//                        image.ProgramExpenseID = Input.Id;
//                        image.image = item.image;
//                        image.TenantId = Input.TenantId;
//                        image.CreatorUserId = Input.CreatorUserId;

//                        _ProgramExpenseImageRepository.Update(image);
//                        mse = "UPDATE";
//                    }

//                }
//            }
//        }
//    }
//    catch (Exception e)
//    {
//        mse = (e.ToString());

//    }
//    return mse;
//}


//public string CreatOrUpdateQuestionaryBySubActivity(CreatOrUpdateQuestionaryListDto Input)
//{
//    string mse = "";
//    int prgQansId=0;
//    int prgUrlId = 0;


//    if (Input.Id==0)
//        {
//            foreach (var item in Input.Questionary)
//            {
//                    prgImplementationPlanQuestionariesMapping prgQans = new prgImplementationPlanQuestionariesMapping();

//                    prgQans.QuestionarieID = item.QuestionaryId;
//                    prgQans.Answer = item.answer;
//                    prgQans.ImplementationPlanCheckListID = Input.PrgImplementationsPlansID;
//                    prgQans.TenantId = Input.TenantId;
//                    prgQans.ProgramID = Input.ProgramId;
//            prgQans.Status = prgImplementationPlanQuestionariesMappingStatus.pending;



//                    prgQansId = _prgImplementationPlanQuestionariesMappingRepository.InsertAndGetId(prgQans);
//            if (prgQansId>0)
//            {
//                mse = "Save";
//            }


//             }
//                if (prgQansId>0)
//                {
//                    foreach (var item in Input.url)
//                    {
//                        prgImplementationPlanQuestionariesUrlMapping prgUrl = new prgImplementationPlanQuestionariesUrlMapping();
//                       prgUrl.ImplementationPlanCheckListID = Input.PrgImplementationsPlansID;
//                        prgUrl.url = item.url;
//                         prgUrl.TenantId = Input.TenantId;


//                         prgUrlId= _prgImplementationPlanQuestionariesUrlMappingRepository.InsertAndGetId(prgUrl);
//                                if (prgUrlId>0)
//                                {
//                                    mse = "Save";
//                                }

//                     }
//                }



//        }
//        else if(Input.Id>0)
//        {
//            foreach (var item in Input.Questionary)
//      {
//            if (item.QuestionarymappngId>0)
//             {
//                         var Q=   _prgImplementationPlanQuestionariesMappingRepository.GetAll().Where(q => q.Id == item.QuestionarymappngId).SingleOrDefault();
//                            Q.ImplementationPlanCheckListID = Input.PrgImplementationsPlansID;
//                            Q.QuestionarieID = item.QuestionaryId;
//                            Q.Answer = item.answer;
//                Q.Status = prgImplementationPlanQuestionariesMappingStatus.pending;
//                            _prgImplementationPlanQuestionariesMappingRepository.Update(Q);
//                mse = "Update";
//            }

//        }
//        foreach (var item in Input.url)
//        {

//            if (item.urlTableId > 0)
//            {
//                var url = _prgImplementationPlanQuestionariesUrlMappingRepository.GetAll().Where(q => q.Id == item.urlTableId).SingleOrDefault();
//                url.ImplementationPlanCheckListID = Input.PrgImplementationsPlansID;
//                url.url = item.url;

//                _prgImplementationPlanQuestionariesUrlMappingRepository.Update(url);
//                mse = "Update";
//            }
//        }



//        }



//    return mse;

//}