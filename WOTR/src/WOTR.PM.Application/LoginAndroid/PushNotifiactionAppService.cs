﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.LoginAndroid.Dto;

namespace WOTR.PM.LoginAndroid
{
   public  class PushNotifiactionAppService :PMAppServiceBase,IPushNotifiactionAppService
    {

        private readonly IRepository<AppInfo> _appInfoRepository;
        private readonly IRepository<AppDeviceLists> _appDeviceListsRepository;

        public PushNotifiactionAppService(IRepository<AppDeviceLists> appDeviceListsRepository, IRepository<AppInfo> appInfoRepository)
        {
            _appDeviceListsRepository = appDeviceListsRepository;
            _appInfoRepository = appInfoRepository;
        }
        public string SendPushNotification(string userId, string message, string tag)
        {
            // userId = 86.ToString();

           var deviceInfo= _appDeviceListsRepository.GetAll().Where(x => x.UserId == Convert.ToInt64(userId)).ToList();
            var inputdata = new List<InAppNotificationInputDto>();
          

            foreach (var item in deviceInfo)
            {
                var entry = new InAppNotificationInputDto();
                entry.applicationId = item.ApplicationId;
                entry.senderId = item.SenderId;
                entry.deviceId = item.DeviceId;
                entry.messages = message;
                entry.title = tag;
                inputdata.Add(entry);
            }

            string response = "";
            foreach (var input in inputdata)
            {
                var APPLICATION_ID = input.applicationId;  // applicationID is a google Api key 
                //var APPLICATION_ID = "AIzaSyDLyFJwxEflp91RhLOxbvX8nCzzrhDkSEk";  // applicationID is a google Api key 
                var SENDER_ID = input.senderId;           // SENDER_ID is nothing but your ProjectID (from API Console- google code) 

                var REGISTER_ID = input.deviceId;  // var REGISTER_ID = input.deviceIds;   

                var title = input.title;
                var value = input.messages;

                WebRequest tRequest;
                tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = " application/json";
                tRequest.Headers.Add(string.Format("Authorization: key={0}", APPLICATION_ID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

                string postData = "{\"collapse_key\":\"score_update\",\"time_to_live\":108,\"delay_while_idle\":true,\"notification\": { \"title\" : " + "\"" + title + "\",\"body\" : " + "\"" + value + "\"},\"data\": {\"message\" : " + "\"" + value + "\"},\"registration_ids\":[\"" + REGISTER_ID + "\"]}";

                Console.WriteLine(postData);
                Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                tRequest.ContentLength = byteArray.Length;
                var dataStream = tRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                var tResponse = tRequest.GetResponse();
                dataStream = tResponse.GetResponseStream();
                var tReader = new StreamReader(dataStream);

                var sResponseFromServer = tReader.ReadToEnd();   //Get response from GCM server.
                response = sResponseFromServer;

                //Assigning GCM response to Label text
                tReader.Close();
                dataStream.Close();
                tResponse.Close();

            }
            return response;
        }

        public string CreateOrUpadteAppInfoDetails(AppInfo Input)
        {
            String messsage = "";

            AppInfo appinfo = new AppInfo();

            appinfo= ObjectMapper.Map<AppInfo> (Input);
            if (Input.Id ==0)
            {
                if (Input.TenantId == 0)
                    return messsage = "please set TenantId";

                else if (Input.SenderId == null)
                    messsage = "Please Send SenderId";
                
                else if (_appInfoRepository.Insert(appinfo).IsTransient())
                    return messsage = "save";

            }else if (Input.Id>0)
            {
                AppInfo Updateappinfo = new AppInfo();

                Updateappinfo.Name = Input.Name;
                Updateappinfo.Description = Input.Description;
                Updateappinfo.ApplicationId = Input.ApplicationId;
                Updateappinfo.SenderId = Input.SenderId;
                Updateappinfo.PackageName = Input.PackageName;
                _appInfoRepository.Update(Updateappinfo);
                    messsage = "Upadte"; 
            }
            return messsage;
        }

        
       public async Task<string> CreateOrUpadteAppDeviceListsDetails(AppDeviceListsDto Input)
        {
            var messsage = "";

            var appDevice = new AppDeviceLists();

            var c = await _appDeviceListsRepository.FirstOrDefaultAsync(a => a.UserId == Input.UserId);
            
            if (c==null)
            {
                appDevice.AppId = 1; //Wort applicationID 
                appDevice.DeviceStatus = true;

                // senderKey(ApplicationId) generatefor fireBase.
                appDevice.ApplicationId = "AIzaSyBgUNIqBLoyBC_NXyOnitV3WAfXxuZvt6E";

                //senderID genarate for FireBase. both key are constant never change for any condution untill fire databse userid chnages.
                appDevice.SenderId = "346790784524";
                if (Input.UserId != 0)
                {
                    appDevice.UserId = Input.UserId;
                }
                else return messsage = "userid";
               

                if (Input.TenantId == 0) { return messsage = "please set TenantId"; } else { appDevice.TenantId = Input.TenantId; }

                //DeviceToken and DeviceUniqueId are respect to mobile user 
                if (Input.DeviceUniqueId == null || Input.DeviceToken == null) 
                { messsage = "you may not send DeviceUniqueId or DeviceId"; }
                else{appDevice.DeviceUniqueId = Input.DeviceUniqueId;
                    appDevice.DeviceId = Input.DeviceToken;
                }

                 if (_appDeviceListsRepository.Insert(appDevice).IsTransient())
                    return messsage = "save";
                else
                {
                    return messsage = "NotSave";
                }

            }
            else 
            {
                c.DeviceId = Input.DeviceToken;
                c.DeviceUniqueId = Input.DeviceUniqueId;
                c.TenantId = Input.TenantId;

               await _appDeviceListsRepository.UpdateAsync(c);
                    messsage = "Upadte";

            }
            return messsage;
           
        }

        public RegisterOutputDto GetCredentialToAndroid(string packagename)
        {
            RegisterOutputDto obj = new RegisterOutputDto();
            AppInfo appobj = _appInfoRepository.GetAll().Where(x => x.PackageName == packagename).FirstOrDefault();

            obj.AppId = appobj.ApplicationId;
            obj.SenderId = appobj.SenderId;
            return obj;
        }

        public async Task<string> GetDeviceId(string deviceId, string packagename, string deviceUniqueId)
        {
            string returnId;

            AppDeviceLists obj = new AppDeviceLists();
            AppInfo appobj = _appInfoRepository.GetAll().Where(x => x.PackageName == packagename).FirstOrDefault();

            var isExist = _appDeviceListsRepository.GetAll().Any(y => y.DeviceUniqueId == deviceUniqueId && y.AppId == appobj.Id);
            if (isExist == false)
            {
                obj.AppId = appobj.Id;
                obj.DeviceId = deviceId;
                obj.DeviceUniqueId = deviceUniqueId;
                obj.DeviceStatus = true;
                var AppId = await _appDeviceListsRepository.InsertAndGetIdAsync(obj);
                returnId = "Inserted " + AppId.ToString();
            }
            else
            {
                AppDeviceLists result = _appDeviceListsRepository.GetAll().Where(y => y.DeviceUniqueId == deviceUniqueId && y.AppId == appobj.Id).FirstOrDefault();
                result.DeviceId = deviceId;
                var AppId = await _appDeviceListsRepository.UpdateAsync(result);
                returnId = "Updated " + AppId.ToString();
            }

            return returnId;
        }

         

    }
}
