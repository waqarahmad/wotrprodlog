﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.NewImpactIndicator;
using WOTR.PM.NewImpactIndicator.Category;
using WOTR.PM.NewImpactIndicator.Category.Dto;

namespace WOTR.PM.ImpactIndicatorCategories
{
    public class CategoryAppService : PMAppServiceBase, ICategoryAppService
    {
        private readonly IRepository<ImpactIndicatorCategory> _IICategoryRepository;

        public CategoryAppService(
            IRepository<ImpactIndicatorCategory> ImpactIndicatorCategory
            ) {
            _IICategoryRepository = ImpactIndicatorCategory;
        }

        public string CreateEditImapactIndiactorCategory(ImpactIndicatorCategory Input)
        {
            string message;

            ImpactIndicatorCategory category = new ImpactIndicatorCategory();
            category.Category = char.ToUpper(Input.Category[0]) + Input.Category.Substring(1);

            if (Input.Id == 0)
            {
                try
                {
                    var QueryCheck = _IICategoryRepository.GetAll().Where(aa => aa.Category == Input.Category).ToList();
                    if (QueryCheck.Count == 0)
                    {
                        _IICategoryRepository.Insert(category);
                        message = "Category Added  sucessfully !";
                    }

                    else { message = "Record Already Present!"; }
                }
                catch (Exception ex)
                {

                    throw new UserFriendlyException(L("Record Not Added"));
                }

            }
            else
            {
                try
                {
                    category.TenantId = Input.TenantId;
                    category.Id = Input.Id;
                    category.Category = Input.Category;
                    _IICategoryRepository.Update(category);
                    message = "Category Update  sucessfully !";

                }
                catch (Exception)
                {

                    throw new UserFriendlyException(L("Record Not Added"));
                }
            }
            return message;
        }

        public List<GetAllCategory> GetAllImapactIndiactorCategory(string filter)
        {
            var CategoryListreturn = new List<GetAllCategory>();
            // List<GetAllCrops> CropList = new List<GetAllCrops>();
            if (filter == null)
            {
                var CropList = _IICategoryRepository.GetAll().ToList();
                CategoryListreturn = ObjectMapper.Map<List<GetAllCategory>>(CropList);

            }
            else
            {
                var CropList = _IICategoryRepository.GetAll().ToList().Where(x => x.Category.ToLower().Contains(filter.Trim().ToLower()));
                CategoryListreturn = ObjectMapper.Map<List<GetAllCategory>>(CropList);
            }
            return CategoryListreturn;
        }

        public async Task Delete(EntityDto input)
        {
            try
            {
                await _IICategoryRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
    }
}
