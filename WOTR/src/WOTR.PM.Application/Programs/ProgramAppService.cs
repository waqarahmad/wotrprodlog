﻿using Abp.Domain.Repositories;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.Authorization.Users.Exporting;
using WOTR.PM.Components;
using WOTR.PM.Donors;
using WOTR.PM.Dto;
using WOTR.PM.Locations;
using WOTR.PM.PrgVillageCluster.Dto;
using WOTR.PM.ProgramCostEstimations;
using WOTR.PM.Programs.Dto;
using WOTR.PM.Programs.Exporting;
using WOTR.PM.PrgActionAreaActivitysMappings;
using System.IO;
//archita
using A = DocumentFormat.OpenXml.Drawing;
using Xdr = DocumentFormat.OpenXml.Drawing.Spreadsheet;
using A14 = DocumentFormat.OpenXml.Office2010.Drawing;
using System.Drawing;
using ShapeProperties = DocumentFormat.OpenXml.Drawing.Spreadsheet.ShapeProperties;
using Color = DocumentFormat.OpenXml.Spreadsheet.Color;
using WOTR.PM.PrgQuarterUnits;
using WOTR.PM.PrgRequestAAPlas.Dto;
using WOTR.PM.PrgRequestAAPlas;
using Abp.Linq.Extensions;
using Abp.Extensions;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using WOTR.PM.Authorization;
using System.Text;
using System.Net;
//using OfficeOpenXml.Drawing;
//using Picture = DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture;
using DocumentFormat.OpenXml.ExtendedProperties;
using Microsoft.EntityFrameworkCore;
using WOTR.PM.EntityFrameworkCore.Repositories;
using WOTR.PM.ProgramRegionSpendings.Dto;
using WOTR.PM.Checklists;
using Abp.Authorization.Users;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.ProgramFunds;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using WOTR.PM.Configuration;
using System.Data.SqlClient;
using System.Data;
//using OfficeOpenXml;
//using DocumentFormat.OpenXml.Drawing;


//archita



namespace WOTR.PM.Programs
{
    public class ProgramAppService : PMAppServiceBase, IProgarmAppService
    {
        private readonly IComponentActivityMappingRepository _ProgramComponentActivityMappingRepository;//...RepositoryToGetAllComponentAnd Activity details.

        private readonly IRepository<Program> _programRepository;
        private readonly IRepository<ProgramManagerMapping> _ProgramManagerMappingRepository;
        private readonly IRepository<Donor> _DonorRepository;
        private readonly IRepository<VillageCluster> _villageClusterRepository;
        private readonly IRepository<Locations.Location> _LocationRepository;
        private readonly IRepository<Village> _VillageRepository;
        private readonly IRepository<Component> _componentRepository;
        private readonly IRepository<ProgramCostEstimationOverall> _ProgramCostEstimationOverAll;
        private readonly IRepository<ProgramCostEstimation> _PrgCostEstRepository;

        private readonly IRepository<Activity> _activityRepository;
        private readonly INamespaceStripper _namespaceStripper;
        private readonly IRepository<PrgActionAreaActivityMapping> _PrgActionAreaActivityMappingRepository;
        private readonly IRepository<SubActionArea> _subactionareaRepository;
        private readonly IRepository<ActionArea> _actionAreaAppRepository;
        private readonly IRepository<ProgramQuqterUnitMapping> _ProgramQuqterUnitMappingRepository;
        private readonly IRepository<RequestAAplas> _RequestAAplasRepository;
        private readonly IRepository<ImplementationPlanCheckList> _ImplementationPlanCheckListRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _rolesRepository;
        private readonly IRepository<RequestAAplas> _requestAAplasRepository;
        private readonly IRepository<ProgramFund> _programFundRepository;
        private readonly IConfigurationRoot _appConfiguration;

        int ProgramId;
        public static string ImageFile = @"\WOTR.pm\WOTR\src\WOTR.PM.Web.Host\src\assets\common\images\WOTR LogoReport.png";
        public static string sourceFile = @"..\..\Data\InputTemplate.xlsx";
        public static string targetFile = @"..\..\Data\Test.xlsx";


        //archita
        public ProgramAppService(IRepository<Program> programRepository,
            IRepository<ProgramManagerMapping> ProgramManagerMappingRepository,
            IRepository<VillageCluster> villageClusterRepository,
            IRepository<Locations.Location> LocationRepository,
            IRepository<Village> VillageRepository,
            IRepository<Donor> DonorRepository, IRepository<Component> ComponentRepository, IRepository<ProgramCostEstimationOverall> ProgramCostEstimationOverAll,
            IRepository<Activity> activityRepository,
            INamespaceStripper namespaceStripper,
            IComponentActivityMappingRepository ProgramComponentActivityMappingRepository,
            IRepository<PrgActionAreaActivityMapping> PrgActionAreaActivityMapping,
            IRepository<SubActionArea> SubActionArea,
            IRepository<ActionArea> ActionArea,
            IRepository<ProgramQuqterUnitMapping> ProgramQuqterUnitMappingRepository,
            IRepository<RequestAAplas> RequestAAplasRepository, IRepository<UserRole, long> userRoleRepository, IRepository<Role> rolesRepository,
            IRepository<ImplementationPlanCheckList> implementationPlanCheckListRepository, IRepository<RequestAAplas> requestAAplasRepository,
            IRepository<ProgramFund> programFundRepository,
            IRepository<ProgramCostEstimation> PrgCostEstRepository, IHostingEnvironment env)

        {
            _programRepository = programRepository;
            _ProgramManagerMappingRepository = ProgramManagerMappingRepository;
            _LocationRepository = LocationRepository;
            _VillageRepository = VillageRepository;
            _villageClusterRepository = villageClusterRepository;
            _DonorRepository = DonorRepository;
            _componentRepository = ComponentRepository;
            _ProgramCostEstimationOverAll = ProgramCostEstimationOverAll;
            _activityRepository = activityRepository;
            _namespaceStripper = namespaceStripper;
            _ProgramComponentActivityMappingRepository = ProgramComponentActivityMappingRepository;
            _PrgActionAreaActivityMappingRepository = PrgActionAreaActivityMapping;
            _subactionareaRepository = SubActionArea;
            _actionAreaAppRepository = ActionArea;
            _ProgramQuqterUnitMappingRepository = ProgramQuqterUnitMappingRepository;
            _RequestAAplasRepository = RequestAAplasRepository;
            _ImplementationPlanCheckListRepository = implementationPlanCheckListRepository;
            _userRoleRepository = userRoleRepository;
            _rolesRepository = rolesRepository;
            _requestAAplasRepository = requestAAplasRepository;
            _programFundRepository = programFundRepository;
            _PrgCostEstRepository = PrgCostEstRepository;
            _appConfiguration = env.GetAppConfiguration();

        }



        public async Task<List<long>> CreateOrUpdateProgram(ProgramInformationListDto Input)
        {
            int? tenantId = AbpSession.TenantId;

            var Program = new Program();
            var ProgramManagerMapping = new ProgramManagerMapping();



            var result = new List<long>();

            //..program table
            Program.Id = Input.Id;
            Program.Name = Input.ProgramName;
            Program.ShortDescription = Input.ShortDescription;
            Program.LongDescription = Input.LongDescription;
            if (Input.DonorID == 0 || Input.DonorID == null)
                Program.DonorID = null;
            else
                Program.DonorID = Input.DonorID;
            Program.ProgramSanctionedYear = Input.ProgramSanctionedYear;
            Program.ProgramStartDate = Input.ProgramStartDate;
            Program.PrgramEndDate = Input.PrgramEndDate;
            Program.GantnAmountSanctionRupess = Input.GantnAmountSanctionRupess;
            Program.ProgramApprovaltDate = Input.ProgramApprovaltDate;
            Program.ProgramSubmitDate = Input.ProgramSubmitDate;
            Program.ProgramBadgeColor = Input.ProgramBadgeColor;
            Program.ProgramBadgeImage = Input.ProgramBadgeImage;
            Program.ManagerID = Input.ProgramManagerId;
            Program.TenantId = (int)tenantId;

            if (Input.Id == 0)
            {
                Program.ProgramManagerID = AbpSession.UserId;
                try
                {
                    //...ProgramId PrimaryKey gererated after insert pass to ProgramManagerMapping and ProgramRegionCoverage Table.
                    ProgramId = await _programRepository.InsertAndGetIdAsync(Program);

                    //...Just confirmation for program table.
                    if (Convert.ToBoolean(ProgramId))
                        result.Add(ProgramId);

                }
                catch (Exception e)
                {
                    result.Add(Convert.ToInt32(e));
                }
            }
            else
            {
                try
                {
                    Program.TenantId = (int)tenantId;
                    Program.ProgramManagerID = Input.CurrentProgramManagerID;
                    await _programRepository.UpdateAsync(Program);
                    result.Add(Input.Id);
                }
                catch (Exception e)
                {
                    result.Add(Convert.ToInt32(e));
                }

            }
            //......ProgramManagerMapping Table.
            if (ProgramId > 0)
            {
                try
                {
                    //......ProgramManagerMapping Table.
                    foreach (var item in Input.ManagerID)
                    {
                        ProgramManagerMapping.ManagerID = item;
                        ProgramManagerMapping.ProgramID = ProgramId;
                        await _ProgramManagerMappingRepository.InsertAsync(ProgramManagerMapping);
                    }

                }
                catch (Exception e)
                {

                    result.Add(Convert.ToInt32(e));
                }
            }
            return result;
        }



        public async Task<List<ProgramInformationListDto>> GetAllProgram()
        {
            var userId = AbpSession.UserId;            
            var prg = _programRepository.GetAll();

            var usr =await _userRoleRepository.FirstOrDefaultAsync(u => u.UserId == userId);
            var role = _rolesRepository.FirstOrDefault(u => u.Id == usr.RoleId);

            if (role.Name == "ProgramManager")
            {
                prg = prg.Where(p => p.ProgramManagerID == userId || p.ManagerID == userId);
            }

            else if (role.Name == "ProjectManager" || role.Name == "ActionAreaManager")
            {
                prg = (from p in prg
                       join r in _requestAAplasRepository.GetAll()
                        on p.Id equals r.ProgramID
                       where (r.ManagerID == userId)
                       select p).Distinct();
            }
            else if (role.Name == "RRcIncharge" || role.Name == "FieldStaffMember")
            {
                prg = (from p in prg
                       join vc in _villageClusterRepository.GetAll()
                        on p.Id equals vc.ProgramID
                       where (vc.UserId == userId)
                       select p).Distinct();
            }
            else if (role.Name == "ProgramManagerFinanceOrAdmin")
            {
                prg = (from p in prg
                       join r in _programFundRepository.GetAll()
                        on p.Id equals r.ProgramID
                       where (r.ProgramManagerID == userId)
                       select p).Distinct();
            }

            var query = (from p in prg
                         select new ProgramInformationListDto()
                         {
                             ProgramName = p.Name,
                             ShortDescription = p.ShortDescription,
                             LongDescription = p.LongDescription,
                             DonorID = p.DonorID,
                             ProgramSanctionedYear = p.ProgramSanctionedYear,
                             ProgramStartDate = p.ProgramStartDate,
                             PrgramEndDate = p.PrgramEndDate,
                             Id = p.Id
                         }).ToList();
            return query;
        }

        public List<ProgramInformationListDto> GetAllPrograms()
        {
            var query = (from p in _programRepository.GetAll()

                         join d in _DonorRepository.GetAll() on p.DonorID equals d.Id
                         select new ProgramInformationListDto()
                         {

                             ProgramName = p.Name,
                             ShortDescription = p.ShortDescription,
                             LongDescription = p.LongDescription,
                             DonorID = p.DonorID,
                             ProgramSanctionedYear = p.ProgramSanctionedYear,
                             ProgramStartDate = p.ProgramStartDate,
                             PrgramEndDate = p.PrgramEndDate,
                             Id = p.Id,

                             DonorName = d.CompanyName,

                         }).ToList();
            return query;
        }

        public List<ProgramInformationListDto> GetAllProgramsbyid(int programID)
        {

            var query = (from p in _programRepository.GetAll()
                         where (p.Id == programID)
                         join d in _DonorRepository.GetAll() on p.DonorID equals d.Id
                         where (p.Id == programID)
                         select new ProgramInformationListDto()
                         {

                             ProgramName = p.Name,
                             ShortDescription = p.ShortDescription,
                             LongDescription = p.LongDescription,
                             DonorID = p.DonorID,
                             ProgramSanctionedYear = p.ProgramSanctionedYear,
                             ProgramStartDate = p.ProgramStartDate,
                             PrgramEndDate = p.PrgramEndDate,
                             DonorName = d.CompanyName,
                             GantnAmountSanctionRupess = p.GantnAmountSanctionRupess

                         }).ToList();
            return query;
        }
        public List<string> GetallAnnualYear()
        {
            var program = _programRepository.GetAll().ToList();

            long start = 0;
            long end = 0;
            var dates = new List<programnamedto>();
            var datayear = new List<string>();

            foreach (var item in program)
            {
                if (item.ProgramStartDate.Month < 4)
                {
                    start = item.ProgramStartDate.Year - 1;
                    end = item.ProgramStartDate.Year;
                }
                else
                {
                    start = item.ProgramStartDate.Year;
                    end = item.ProgramStartDate.Year + 1;
                }

                for (var dt = start; dt <= item.PrgramEndDate.Year; dt = dt + 1)
                {
                    var from = DateTime.Parse("April 1," + (start));
                    var to = DateTime.Parse("March 31, " + end);
                    if (item.PrgramEndDate.Month > to.Month || item.PrgramEndDate.Year > from.Year)
                    {
                        datayear.Add(start.ToString() + "-" + end.ToString());
                        dates.Add(new programnamedto
                        {
                            date = start.ToString() + "-" + end.ToString(),
                            programname = item.Name,
                            donorId = item.DonorID,
                            GrantAmount = item.GantnAmountSanctionRupess,
                            ProgramStartDate = item.ProgramStartDate,
                            PrgramEndDate = item.PrgramEndDate

                        });

                    }
                    start++;
                    end++;
                }
            }

            var d = datayear.Distinct().OrderBy(q => q);
            return d.ToList();
        }

        public List<ProgramInformationListDto> GetAllProgramsDetailsforexcelreport(string year)
        {
            var program = _programRepository.GetAll().ToList();

            long start = 0;
            long end = 0;
            var dates = new List<programnamedto>();

            foreach (var item in program)
            {
                if (item.ProgramStartDate.Month < 4)
                {
                    start = item.ProgramStartDate.Year - 1;
                    end = item.ProgramStartDate.Year;
                }
                else
                {
                    start = item.ProgramStartDate.Year;
                    end = item.ProgramStartDate.Year + 1;
                }

                for (var dt = start; dt <= item.PrgramEndDate.Year; dt = dt + 1)
                {
                    var from = DateTime.Parse("April 1," + (start));
                    var to = DateTime.Parse("March 31, " + end);
                    if (item.PrgramEndDate.Month > to.Month || item.PrgramEndDate.Year > from.Year)
                    {
                        dates.Add(new programnamedto
                        {
                            date = start.ToString() + "-" + end.ToString(),
                            programname = item.Name,
                            donorId = item.DonorID,
                            GrantAmount = item.GantnAmountSanctionRupess,
                            ProgramStartDate = item.ProgramStartDate,
                            PrgramEndDate = item.PrgramEndDate

                        });

                    }
                    start++;
                    end++;
                }
            }

            var data = new List<ProgramInformationListDto>();


            foreach (var item in dates)
            {
                if (item.date == year)
                {
                    data.Add(new ProgramInformationListDto
                    {
                        ProgramName = item.programname,
                        DonorName = _DonorRepository.GetAll().Where(aa => aa.Id == item.donorId).FirstOrDefault().CompanyName,
                        ProgramStartDate = item.ProgramStartDate,
                        PrgramEndDate = item.PrgramEndDate,
                        GantnAmountSanctionRupess = item.GrantAmount,
                    });
                }

            }


            return data.OrderBy(xx => xx.ProgramName).ToList();
        }

        public List<ProgramInformationListDto> GetAllProgramsDetails()
        {
            var query = (from p in _programRepository.GetAll()

                         join d in _DonorRepository.GetAll() on p.DonorID equals d.Id
                         select new ProgramInformationListDto()
                         {
                             ProgramName = p.Name,
                             ShortDescription = p.ShortDescription,
                             LongDescription = p.LongDescription,
                             DonorID = p.DonorID,
                             ProgramSanctionedYear = p.ProgramSanctionedYear,
                             ProgramStartDate = p.ProgramStartDate,
                             PrgramEndDate = p.PrgramEndDate,
                             GantnAmountSanctionRupess = p.GantnAmountSanctionRupess,
                             Id = p.Id,
                             DonorName = d.CompanyName
                         }).OrderBy(p => p.ProgramName).ToList();
            return query;
        }

        public List<ProgrameComponentListDto> GetComponentCostEstimationforallprogram()
        {
            long? userId = AbpSession.UserId;
            int? TenantId = AbpSession.TenantId;

            var result = new List<ProgrameComponentListDto>();

            var query = _ProgramComponentActivityMappingRepository.GetComponentandActivityForCostEstimationforallprograms(TenantId, userId);

            if (query != null)
            {
                var ListOfUsers = query.ToList().GroupBy(x => x.ComponetTableId)
                                      .Select(g => g.First())
                                      .ToList();

                var dates = new List<string>();
                dates.Add("OverAll");
                long Start = 0;
                long end = 0;
                if (query[0].ProgrameStartDate.Month < 4)
                {
                    Start = query[0].ProgrameStartDate.Year - 1;
                    end = query[0].ProgrameStartDate.Year;
                }
                else
                {
                    Start = query[0].ProgrameStartDate.Year;
                    end = query[0].ProgrameStartDate.Year + 1;
                }

                for (var dt = Start; dt <= query[0].ProgrameEndDate.Year; dt = dt + 1)
                {
                    var from = DateTime.Parse("April 1," + (Start));
                    var to = DateTime.Parse("March 31, " + end);
                    if (query[0].ProgrameEndDate.Month > to.Month || query[0].ProgrameEndDate.Year > from.Year)
                    {
                        dates.Add(Start.ToString() + "-" + end.ToString());
                    }
                    Start++;
                    end++;
                }

                foreach (var item in ListOfUsers)
                {
                    var obj1 = new List<ProgrameCostEstimationYearDto>();

                    var obj = query[0].CostEstimationYear[0].Activity.Where(r => r.ActivityID != 0 && r.ComponetTableId == item.ComponetTableId).ToList();

                    foreach (var item2 in dates)
                    {
                        var i = new List<ComponentAndactivityMappingForList>();
                        foreach (var u in obj)
                        {
                            u.ProgramCostEstimate_UnitOfMeasuresID = u.ProgramCostEstimate_UnitOfMeasuresID == 0 ? 1 : u.ProgramCostEstimate_UnitOfMeasuresID;
                            if (u.CostEstimationYear != "")
                            {
                                if (u.CostEstimationYear == item2.ToString())
                                {
                                    i.Add(u);
                                }
                            }
                            else
                            {
                                i.Add(u);
                            }
                        }
                        decimal? SubTotalofTotalCost = 0;
                        decimal? SubTotalTotalunits = 0;
                        int id = 0;
                        foreach (var year in query[0].CostEstimationYear)
                        {
                            if (year.CostEstimationYear == item2.ToString() && year.ComponetTableId == item.ComponetTableId)
                            {
                                id = year.Id;
                                SubTotalofTotalCost = year.SubTotalofTotalCost;
                                SubTotalTotalunits = year.SubTotalTotalunits;
                            }
                        }

                        obj1.Add(new ProgrameCostEstimationYearDto
                        {
                            CostEstimationYear = item2,
                            Id = id,
                            SubTotalofTotalCost = SubTotalofTotalCost,
                            SubTotalTotalunits = SubTotalTotalunits,
                            Activity = i
                        });
                    }

                    result.Add(new ProgrameComponentListDto
                    {
                        Id = item.Id,
                        ComponetTableId = item.ComponetTableId,
                        componentName = item.componentName,
                        ComponentCode = item.ComponentCode,
                        ComponentCreatime = item.ComponentCreatime,
                        ProgrameId = item.ProgrameId,
                        ProgrameStartDate = item.ProgrameStartDate,
                        ProgrameEndDate = item.ProgrameEndDate,
                        CostEstimationYear = obj1,
                        TotalCost = item.TotalCost,
                        TotalUnits = item.TotalUnits,

                    });
                }


            }
            return result;

        }
        public List<ProgramInformationListDto> GetAllProgramsDetailsIII(string year)
        {

            try
            {
                string value;
                var allChartData = new ProgramInformationListDto();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramYearwiseStateWiseBudgets = new List<ProgramInformationListDto>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    ConnectionConnect(conn);
                    using (var cmdTwo = new SqlCommand("", conn))
                    {
                        //ProjectWiseInfoLineChart
                        var dataTable = new System.Data.DataTable();
                        cmdTwo.CommandTimeout = 0;
                        cmdTwo.CommandText = "[dbo].[GetAllProgramsDetailsIII]";
                        cmdTwo.CommandType = CommandType.StoredProcedure;
                        cmdTwo.Parameters.Add(new SqlParameter("@CostEstimationYear", year));


                        SqlDataReader dr = cmdTwo.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerProgramYearwiseStateWiseBudgets.Add(new ProgramInformationListDto
                                    {
                                        ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),
                                        DonorName = (row["DonorName"] == DBNull.Value) ? "" : Convert.ToString(row["DonorName"]),
                                        TotalCommunityCost = (row["CommunityContribution"] == DBNull.Value) ? "" : Convert.ToString(row["CommunityContribution"]),
                                        TotalFunderCost = (row["FunderContribution"] == DBNull.Value) ? "" : Convert.ToString(row["FunderContribution"]),
                                        TotalotherCost = (row["OtherContribution"] == DBNull.Value) ? "" : Convert.ToString(row["OtherContribution"]),
                                        AllTotal = (row["Total"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Total"]),
                                    });
                                }

                            }
                        }
                        conn.Close();
                    }


                }
                return programManagerProgramYearwiseStateWiseBudgets;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<Donorwisedto>> GetAllProgramsDetailsDonorWise(string Year)
        {
            try
            {
                string value;
                var allChartData = new Donorwisedto();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramYearwiseStateWiseBudgets = new List<Donorwisedto>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdTwo = new SqlCommand("", conn))
                    {
                        //ProjectWiseInfoLineChart
                        var dataTable = new System.Data.DataTable();
                        cmdTwo.CommandTimeout = 0;
                        cmdTwo.CommandText = "[dbo].[sp_DonorwiseWiseExcelReport]";
                        cmdTwo.CommandType = CommandType.StoredProcedure;
                        cmdTwo.Parameters.Add(new SqlParameter("@CostEstimationYear", Year));


                        SqlDataReader dr = cmdTwo.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerProgramYearwiseStateWiseBudgets.Add(new Donorwisedto
                                    {
                                        DonorName = (row["DonorName"] == DBNull.Value) ? "" : Convert.ToString(row["DonorName"]),
                                        allTotalDonortotalcost = (row["Amount"] == DBNull.Value) ? "" : Convert.ToString(row["Amount"])

                                    });
                                }

                            }
                        }
                        conn.Close();
                    }


                }
                return programManagerProgramYearwiseStateWiseBudgets;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task ConnectionConnect(SqlConnection conn)
        {
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    await conn.OpenAsync();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public List<string> PrgCostEstimationYears()
        {

            List<string> list = (from cost in _PrgCostEstRepository.GetAll()
                                 select cost.CostEstimationYear.ToString()).Distinct().OrderBy(x=> x).ToList();
            return list;
        }
        public async Task<FileDto> CreateExcelDocallDonorWise(string fileName, string Year)
        {
            var result = await GetAllProgramsDetailsDonorWise(Year);
            var a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";

            using (SpreadsheetDocument document = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))
            {
                // new code
                WorkbookPart wbp = document.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();

                //old code
                //var wbp = document.AddWorkbookPart();
                //var wsp = wbp.AddNewPart<WorksheetPart>();
                //var wb = new Workbook();
                //var fv = new FileVersion();
                //fv.ApplicationName = "Microsoft Office Excel";
                //var ws = new Worksheet();
                //var sd = new SheetData();

                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                var dp = wsp.AddNewPart<DrawingsPart>();
                var imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                try
                {
                    using (FileStream fs = new FileStream(sImagePath, FileMode.Open, FileAccess.Read))
                    {
                        imgp.FeedData(fs);
                    }
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }
                var nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                var nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                var nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                var blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                var bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                var sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                var pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                var ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                var anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                var wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                var drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                var sheets = new Sheets();
                var sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                wb.Append(fv);


                var row177 = new Row() { RowIndex = 1U };

                //create a new inline string cell
                var cell = new Cell() { CellReference = "C1" };
                cell.DataType = CellValues.InlineString;

                //create a run for the bold text
                var run177 = new Run();
                run177.Append(new Text("Watershed Organisation Trust (WOTR)"));
                //create runproperties and append a "Bold" to them
                var run1Properties = new RunProperties();
                run1Properties.Append(new Bold());

                var fontSize1 = new FontSize() { Val = 22 };
                run1Properties.Append(fontSize1);

                //set the first runs RunProperties to the RunProperties containing the bold
                run177.RunProperties = run1Properties;

                //create a second run for the non-bod text
                var run2 = new Run();
                run2.Append(new Text(Environment.NewLine + " ") { Space = SpaceProcessingModeValues.Preserve });

                //create a new inline string and append both runs
                var inlineString = new InlineString();
                inlineString.Append(run177);
                inlineString.Append(run2);

                //append the inlineString to the cell.
                cell.Append(inlineString);

                //append the cell to the row
                row177.Append(cell);

                sd.Append(row177);

                var cell8 = InsertCellInWorksheet("D", 2, wsp);
                cell8.CellValue = new CellValue("List of the Funding organisations");
                cell8.DataType = new EnumValue<CellValues>(CellValues.Number);

                cell8.StyleIndex = 1;
                Run run3 = new Run();
                run3.Append(new Text("List of the Funding organisations"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run3.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run3);
                cell8.Append(inlineString1);

                var cell81 = InsertCellInWorksheet("D", 4, wsp);
                cell81.CellValue = new CellValue("Year: "+Year);
                cell81.DataType = new EnumValue<CellValues>(CellValues.Number);



                var row8 = new Row();



                var cnt = 1;
                Row roea = new Row();


                roea.Append(
                    ConstructCell("SR.No", CellValues.Number, 1),

                    ConstructCell("Donor Name", CellValues.Number, 1),
                    ConstructCell("Grant Amount", CellValues.Number, 1));

                sd.AppendChild(roea);

                foreach (var acti in result)
                {
                    row8 = new Row();
                    row8.Append(
                        ConstructCell(cnt++.ToString(), CellValues.Number, 1),
                        ConstructCell(acti.DonorName.ToString(), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(acti.allTotalDonortotalcost).ToString("#,##0.00"), CellValues.Number, 1));

                    sd.AppendChild(row8);
                }

                sheets.Append(sheet);
                wb.Append(sheets);
                document.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();
            }

            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;
        }
        //
        //archita
        private static void InsertChartInSpreadsheet(string docName, string worksheetName, string title,
     Dictionary<string, int> data)
        {
            // Open the document for editing.

            using (SpreadsheetDocument document = SpreadsheetDocument.Open(docName, true))
            {
                IEnumerable<Sheet> sheets = document.WorkbookPart.Workbook.Descendants<Sheet>().
        Where(s => s.Name == worksheetName);
                if (sheets.Count() == 0)
                {
                    // The specified worksheet does not exist.
                    return;
                }
                WorksheetPart worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheets.First().Id);

                // Add a new drawing to the worksheet.
                DrawingsPart drawingsPart = worksheetPart.AddNewPart<DrawingsPart>();
                worksheetPart.Worksheet.Append(new DocumentFormat.OpenXml.Spreadsheet.Drawing()
                { Id = worksheetPart.GetIdOfPart(drawingsPart) });
                worksheetPart.Worksheet.Save();

                // Add a new chart and set the chart language to English-US.
                ChartPart chartPart = drawingsPart.AddNewPart<ChartPart>();
                chartPart.ChartSpace = new ChartSpace();
                chartPart.ChartSpace.Append(new EditingLanguage() { Val = new StringValue("en-US") });
                DocumentFormat.OpenXml.Drawing.Charts.Chart chart = chartPart.ChartSpace.AppendChild<DocumentFormat.OpenXml.Drawing.Charts.Chart>(
                    new DocumentFormat.OpenXml.Drawing.Charts.Chart());

                // Create a new clustered column chart.
                PlotArea plotArea = chart.AppendChild<PlotArea>(new PlotArea());
                Layout layout = plotArea.AppendChild<Layout>(new Layout());
                BarChart barChart = plotArea.AppendChild<BarChart>(new BarChart(new BarDirection()
                { Val = new EnumValue<BarDirectionValues>(BarDirectionValues.Column) },
                    new BarGrouping() { Val = new EnumValue<BarGroupingValues>(BarGroupingValues.Clustered) }));

                uint i = 0;

                // Iterate through each key in the Dictionary collection and add the key to the chart Series
                // and add the corresponding value to the chart Values.
                foreach (string key in data.Keys)
                {
                    BarChartSeries barChartSeries = barChart.AppendChild<BarChartSeries>(new BarChartSeries(new Index()
                    {
                        Val =
         new UInt32Value(i)
                    },
                        new Order() { Val = new UInt32Value(i) },
                        new SeriesText(new NumericValue() { Text = key })));

                    StringLiteral strLit = barChartSeries.AppendChild<CategoryAxisData>(new CategoryAxisData()).AppendChild<StringLiteral>(new StringLiteral());
                    strLit.Append(new PointCount() { Val = new UInt32Value(1U) });
                    strLit.AppendChild<StringPoint>(new StringPoint() { Index = new UInt32Value(0U) }).Append(new NumericValue(title));

                    NumberLiteral numLit = barChartSeries.AppendChild<DocumentFormat.OpenXml.Drawing.Charts.Values>(
                        new DocumentFormat.OpenXml.Drawing.Charts.Values()).AppendChild<NumberLiteral>(new NumberLiteral());
                    numLit.Append(new FormatCode("General"));
                    numLit.Append(new PointCount() { Val = new UInt32Value(1U) });
                    numLit.AppendChild<NumericPoint>(new NumericPoint() { Index = new UInt32Value(0u) }).Append
        (new NumericValue(data[key].ToString()));

                    i++;
                }

                barChart.Append(new AxisId() { Val = new UInt32Value(48650112u) });
                barChart.Append(new AxisId() { Val = new UInt32Value(48672768u) });

                // Add the Category Axis.
                CategoryAxis catAx = plotArea.AppendChild<CategoryAxis>(new CategoryAxis(new AxisId()
                { Val = new UInt32Value(48650112u) }, new Scaling(new Orientation()
                {
                    Val = new EnumValue<DocumentFormat.
        OpenXml.Drawing.Charts.OrientationValues>(DocumentFormat.OpenXml.Drawing.Charts.OrientationValues.MinMax)
                }),
                    new AxisPosition() { Val = new EnumValue<AxisPositionValues>(AxisPositionValues.Bottom) },
                    new TickLabelPosition() { Val = new EnumValue<TickLabelPositionValues>(TickLabelPositionValues.NextTo) },
                    new CrossingAxis() { Val = new UInt32Value(48672768U) },
                    new Crosses() { Val = new EnumValue<CrossesValues>(CrossesValues.AutoZero) },
                    new AutoLabeled() { Val = new BooleanValue(true) },
                    new LabelAlignment() { Val = new EnumValue<LabelAlignmentValues>(LabelAlignmentValues.Center) },
                    new LabelOffset() { Val = new UInt16Value((ushort)100) }));

                // Add the Value Axis.
                ValueAxis valAx = plotArea.AppendChild<ValueAxis>(new ValueAxis(new AxisId() { Val = new UInt32Value(48672768u) },
                    new Scaling(new Orientation()
                    {
                        Val = new EnumValue<DocumentFormat.OpenXml.Drawing.Charts.OrientationValues>(
                        DocumentFormat.OpenXml.Drawing.Charts.OrientationValues.MinMax)
                    }),
                    new AxisPosition() { Val = new EnumValue<AxisPositionValues>(AxisPositionValues.Left) },
                    new MajorGridlines(),
                    new DocumentFormat.OpenXml.Drawing.Charts.NumberingFormat()
                    {
                        FormatCode = new StringValue("General"),
                        SourceLinked = new BooleanValue(true)
                    }, new TickLabelPosition()
                    {
                        Val = new EnumValue<TickLabelPositionValues>
        (TickLabelPositionValues.NextTo)
                    }, new CrossingAxis() { Val = new UInt32Value(48650112U) },
                    new Crosses() { Val = new EnumValue<CrossesValues>(CrossesValues.AutoZero) },
                    new CrossBetween() { Val = new EnumValue<CrossBetweenValues>(CrossBetweenValues.Between) }));

                // Add the chart Legend.
                Legend legend = chart.AppendChild<Legend>(new Legend(new LegendPosition() { Val = new EnumValue<LegendPositionValues>(LegendPositionValues.Right) },
                    new Layout()));

                chart.Append(new PlotVisibleOnly() { Val = new BooleanValue(true) });

                // Save the chart part.
                chartPart.ChartSpace.Save();

                // Position the chart on the worksheet using a TwoCellAnchor object.
                drawingsPart.WorksheetDrawing = new WorksheetDrawing();
                TwoCellAnchor twoCellAnchor = drawingsPart.WorksheetDrawing.AppendChild<TwoCellAnchor>(new TwoCellAnchor());
                twoCellAnchor.Append(new DocumentFormat.OpenXml.Drawing.Spreadsheet.FromMarker(new ColumnId("9"),
                    new ColumnOffset("581025"),
                    new RowId("17"),
                    new RowOffset("114300")));
                twoCellAnchor.Append(new DocumentFormat.OpenXml.Drawing.Spreadsheet.ToMarker(new ColumnId("17"),
                    new ColumnOffset("276225"),
                    new RowId("32"),
                    new RowOffset("0")));

                // Append a GraphicFrame to the TwoCellAnchor object.
                DocumentFormat.OpenXml.Drawing.Spreadsheet.GraphicFrame graphicFrame =
                    twoCellAnchor.AppendChild<DocumentFormat.OpenXml.
        Drawing.Spreadsheet.GraphicFrame>(new DocumentFormat.OpenXml.Drawing.
        Spreadsheet.GraphicFrame());
                graphicFrame.Macro = "";

                graphicFrame.Append(new DocumentFormat.OpenXml.Drawing.Spreadsheet.NonVisualGraphicFrameProperties(
                    new DocumentFormat.OpenXml.Drawing.Spreadsheet.NonVisualDrawingProperties() { Id = new UInt32Value(2u), Name = "Chart 1" },
                    new DocumentFormat.OpenXml.Drawing.Spreadsheet.NonVisualGraphicFrameDrawingProperties()));

                graphicFrame.Append(new Transform(new A.Offset() { X = 0L, Y = 0L },
                                                                        new A.Extents() { Cx = 0L, Cy = 0L }));

                graphicFrame.Append(new A.Graphic(new A.GraphicData(new ChartReference() { Id = drawingsPart.GetIdOfPart(chartPart) })
                { Uri = "http://schemas.openxmlformats.org/drawingml/2006/chart" }));

                twoCellAnchor.Append(new ClientData());

                // Save the WorksheetDrawing object.
                drawingsPart.WorksheetDrawing.Save();
            }

        }

        public async Task<FileDto> CreateExcelDocallprojectlistIII(string fileName, string year)
        {
            var result = GetAllProgramsDetailsIII(year);
            string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";

            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))


            {


                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();

                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                BlipFill blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                Bitmap bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                ShapeProperties sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                Position pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                Extent ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                AbsoluteAnchor anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                WorksheetDrawing wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                // sheets.Append(sheet);
                wb.Append(fv);

                //SheetData sheetData = wsp.Worksheet.AppendChild(new SheetData());


                Row row8 = new Row();
                Row row9 = new Row();

                Cell cell8 = InsertCellInWorksheet("D", 1, wsp);
                cell8.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                cell8.DataType = new EnumValue<CellValues>(CellValues.Number);


                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cell8.Append(inlineString);

                Cell cell1 = InsertCellInWorksheet("D", 2, wsp);
                cell1.CellValue = new CellValue("Program Details Information");
                cell1.DataType = new EnumValue<CellValues>(CellValues.Number);


                Run run2 = new Run();
                run2.Append(new Text("Project wise santioned details"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cell1.Append(inlineString1);

                Cell cell12 = InsertCellInWorksheet("D", 3, wsp);
                cell12.CellValue = new CellValue("Program Details Information");
                cell12.DataType = new EnumValue<CellValues>(CellValues.Number);

                Cell cellString12 = InsertCellInWorksheet("D", 3, wsp);
                cellString12.CellValue = new CellValue("Year: " + year);
                cellString12.DataType = new EnumValue<CellValues>(CellValues.String);

                row9.Append(

                 ConstructCell("", CellValues.String));
                sd.AppendChild(row9);
                row8.Append(

                    ConstructCell("Program Title", CellValues.String, 2),
                    ConstructCell("Name of Donor Agency ", CellValues.String, 2),
                    ConstructCell("Project Cost (CBOs)", CellValues.Number, 2),
                    ConstructCell("Local Contribution ", CellValues.String, 2),
                    ConstructCell("Govt. Contribution ", CellValues.Number, 2),
                    ConstructCell("Total ", CellValues.Number, 2));

                sd.AppendChild(row8);

                foreach (var acti in result)
                {
                    row8 = new Row();

                    row8.Append(

                        ConstructCell(acti.ProgramName.ToString(), CellValues.String, 1),
                        ConstructCell(acti.DonorName.ToString(), CellValues.Number, 1),
                        ConstructCell(Convert.ToDecimal(acti.TotalFunderCost).ToString("#,##0.00"), CellValues.Number, 1),

                        ConstructCell(Convert.ToDecimal(acti.TotalCommunityCost).ToString("#,##0.00"), CellValues.Number, 1),
                        ConstructCell(Convert.ToDecimal(acti.TotalotherCost).ToString("#,##0.00"), CellValues.Number, 1),
                        ConstructCell(Convert.ToDecimal(acti.AllTotal).ToString("#,##0.00"), CellValues.Number, 1));







                    sd.AppendChild(row8);
                }
                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();

                //wsp.Worksheet.Save();





            }


            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;



        }


        public async Task<FileDto> CreateExcelDocallprojectlist(string fileName, string year)
        {
            var result = GetAllProgramsDetailsforexcelreport(year);
            string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";

            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))


            {

                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();


                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                BlipFill blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                Bitmap bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                ShapeProperties sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                Position pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                Extent ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                AbsoluteAnchor anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                WorksheetDrawing wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                // sheets.Append(sheet);
                wb.Append(fv);

                //Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                //Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "ProgramDetails" };

                //sheets.Append(sheet);

                //workbookPart.Workbook.Save();



                //SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());

                // Constructing header
                Row row = new Row();
                Row row1 = new Row();
                Row row2 = new Row();
                Row row3 = new Row();
                Row row4 = new Row();
                Row row5 = new Row();
                Row row6 = new Row();
                Row row7 = new Row();
                Row row8 = new Row();
                Row row9 = new Row();

                Cell cell8 = InsertCellInWorksheet("D", 1, wsp);
                cell8.CellValue = new CellValue("Watershed Organisation Trust(WOTR)");
                cell8.DataType = new EnumValue<CellValues>(CellValues.Number);
                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cell8.Append(inlineString);


                Cell cellString8 = InsertCellInWorksheet("D", 2, wsp);
                cellString8.CellValue = new CellValue("List of ongoing projects during the year");
                cellString8.DataType = new EnumValue<CellValues>(CellValues.String);
                Run run2 = new Run();
                run2.Append(new Text("List of ongoing projects during the year"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cellString8.Append(inlineString1);



                Cell cell = InsertCellInWorksheet("D", 3, wsp);
                cell.CellValue = new CellValue("Year: " + year);
                cell.DataType = new EnumValue<CellValues>(CellValues.Number);

                Cell cellString = InsertCellInWorksheet("D", 3, wsp);
                cellString.CellValue = new CellValue("Year: " + year);
                cellString.DataType = new EnumValue<CellValues>(CellValues.String);

                row9.Append(

                 ConstructCell("", CellValues.String));
                sd.AppendChild(row9);
                row8.Append(

                   ConstructCell("Program Title", CellValues.String, 2),
                 ConstructCell("Name of Donor Agency ", CellValues.String, 2),
                    ConstructCell("Project Duration", CellValues.String, 2),
                    ConstructCell("Sanctioned Budget Rs.", CellValues.Number, 2));

                sd.AppendChild(row8);

                foreach (var acti in result)
                {
                    row8 = new Row();

                    row8.Append(

                        ConstructCell(acti.ProgramName.ToString(), CellValues.String, 1),
                        ConstructCell(acti.DonorName.ToString(), CellValues.Number, 1),
                        ConstructCell(acti.ProgramStartDate.ToString("dd/MM/yyyy") + '-' + acti.PrgramEndDate.ToString("dd/MM/yyyy"), CellValues.String, 1),
                        ConstructCell(Convert.ToDecimal(acti.GantnAmountSanctionRupess).ToString("#,##0.00"), CellValues.Number, 1)
                     );







                    sd.AppendChild(row8);
                }
                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();
                //worksheetPart.Worksheet.Save();





            }


            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;



        }

        public List<ProgramInformationListDto> GetProgramDetailsbyID(int ProgramId)
        {

            var query = (from p in _programRepository.GetAll()
                         where (p.Id == ProgramId)
                         join d in _DonorRepository.GetAll() on p.DonorID equals d.Id
                         where (p.Id == ProgramId)
                         select new ProgramInformationListDto()
                         {

                             ProgramName = p.Name,
                             ShortDescription = p.ShortDescription,
                             LongDescription = p.LongDescription,
                             DonorID = p.DonorID,
                             ProgramSanctionedYear = p.ProgramSanctionedYear,
                             ProgramStartDate = p.ProgramStartDate,
                             PrgramEndDate = p.PrgramEndDate,
                             DonorName = d.CompanyName,
                             GantnAmountSanctionRupess = p.GantnAmountSanctionRupess

                         }).ToList();

            return query;

        }

        public List<PrgVillageClusterListDto> GetRegionSpendingDetailsbyclustor(int ProgramId)
        {


            List<PrgVillageClusterListDto> result = new List<PrgVillageClusterListDto>();

            List<PrgVillageClusterListDto> vcList = new List<PrgVillageClusterListDto>();
            var ListOfVc = _villageClusterRepository.GetAll().Where(p => p.ProgramID == ProgramId).ToList().GroupBy(x => new { x.Name })
                                        .Select(g => g.First())
                                        .ToList();
            foreach (var vc in ListOfVc)
            {
                vcList.Add(new PrgVillageClusterListDto
                {
                    Name = vc.Name,

                });
            }
            List<PrgVillageClusterVillages> result1 = new List<PrgVillageClusterVillages>();

            var ListOfVillage = _villageClusterRepository.GetAll().Where(p => p.ProgramID == ProgramId).ToList().GroupBy(y => new { y.VillageID, y.VillageClusterLocationID })
                                  .Select(g => g.First())
                                  .ToList();
            foreach (var village in ListOfVillage)
            {
                var VillageName = _VillageRepository.GetAll().Where(v => v.Id == village.VillageID).FirstOrDefault()?.Name;
                result1.Add(new PrgVillageClusterVillages
                {
                    Name = VillageName,

                });
            }
            List<Precostestimationoverall> result2 = new List<Precostestimationoverall>();

            var Components = _ProgramCostEstimationOverAll.GetAll().Where(p => p.ProgramID == ProgramId).ToList().GroupBy(y => new { y.ComponentID, })
                                  .Select(g => g.First())
                                  .ToList();
            foreach (var compo in Components)
            {
                var componentname = _componentRepository.GetAll().Where(v => v.Id == compo.ComponentID).FirstOrDefault()?.Name;
                result2.Add(new Precostestimationoverall
                {
                    ComponentName = componentname,

                });
            }

            List<PrecostestimationoverallActivity> result3 = new List<PrecostestimationoverallActivity>();

            var Activity = _ProgramCostEstimationOverAll.GetAll().Where(p => p.ProgramID == ProgramId).ToList().GroupBy(y => new { y.ActivityID, })
                                  .Select(g => g.First())
                                  .ToList();
            foreach (var Acti in Activity)
            {
                var ActivityName = _activityRepository.GetAll().Where(v => v.Id == Acti.ActivityID).FirstOrDefault()?.Name;
                result3.Add(new PrecostestimationoverallActivity
                {
                    ActivityName = ActivityName,
                    TotalUnitCost = Convert.ToInt64(Acti.TotalUnitCost),
                    UnitCost = Convert.ToInt64(Acti.UnitCost),
                    TotalUnit = Convert.ToInt64(Acti.TotalUnits),
                    CommunityConstribution = Convert.ToInt64(Acti.CommunityContribution),
                    FunderConstribution = Convert.ToInt64(Acti.FunderContribution),
                    OtherConstribution = Convert.ToInt64(Acti.OtherContribution),



                });
            }

            result.Add(new PrgVillageClusterListDto
            {

                Villages = result1,
                VillageClusterList = vcList,
                components = result2,
                Activity = result3,

            });


            return result;

        }


        public async Task<List<PrgVillageClusterListDto>> GetProgramDetailsbyvillageclustor(int ProgramId)
        {
            var result = new List<PrgVillageClusterListDto>();

            var villageCluster = await _villageClusterRepository.GetAll().Include(t => t.Village).Where(p => p.ProgramID == ProgramId).ToListAsync();
            var prgCostEstOverall = await _ProgramCostEstimationOverAll.GetAll().Include(c => c.Component).Include(c => c.Activity)
                .Where(p => p.ProgramID == ProgramId).ToListAsync();

            var listOfVc = villageCluster.GroupBy(x => new { x.Name })
                                        .Select(g => g.First())
                                        .ToList();

            var vcList = (from l in listOfVc
                          select new PrgVillageClusterListDto()
                          {
                              Name = l.Name
                          }).ToList();

            var listOfVillage = villageCluster.GroupBy(y => new { y.VillageID, y.VillageClusterLocationID })
                                  .Select(g => g.First())
                                  .ToList();

            var result1 = (from l in listOfVillage
                           select new PrgVillageClusterVillages()
                           {
                               Name = l.VillageID == null ? string.Empty : l.Village.Name
                           }).ToList();

            var components = prgCostEstOverall.GroupBy(y => new { y.ComponentID, })
                                  .Select(g => g.First())
                                  .ToList();
            var result2 = (from l in components
                           select new Precostestimationoverall()
                           {
                               ComponentName = l.ComponentID == null ? string.Empty : l.Component.Name
                           }).ToList();

            var activity = prgCostEstOverall.GroupBy(y => new { y.ActivityID, })
                                  .Select(g => g.First())
                                  .ToList();

            var result3 = (from a in activity
                           select new PrecostestimationoverallActivity()
                           {
                               ActivityName = a.ActivityID == null ? string.Empty : a.Activity.Name,
                               TotalUnitCost = Convert.ToInt64(a.TotalUnitCost),
                               UnitCost = Convert.ToInt64(a.UnitCost),
                               TotalUnit = Convert.ToInt64(a.TotalUnits),
                               CommunityConstribution = Convert.ToInt64(a.CommunityContribution),
                               FunderConstribution = Convert.ToInt64(a.FunderContribution),
                               OtherConstribution = Convert.ToInt64(a.OtherContribution),
                               AllTotal = Convert.ToInt64(a.TotalUnitCost + a.UnitCost + a.TotalUnits)
                           }).ToList();

            result.Add(new PrgVillageClusterListDto
            {
                Villages = result1,
                VillageClusterList = vcList,
                components = result2,
                Activity = result3
            });

            return result;
        }

        public async Task<List<PrgVillageClusterListDto>> GetProgramDetailsbyvillageclustorforExcel(int ProgramId, string costyear)
        {
            var result = new List<PrgVillageClusterListDto>();

            var villageCluster = await _villageClusterRepository.GetAll().Include(t => t.Village).Where(p => p.ProgramID == ProgramId).ToListAsync();
            var prgCostEstOverall = await _PrgCostEstRepository.GetAll().Include(c => c.Component).Include(c => c.Activity)
                .Where(p => p.ProgramID == ProgramId && p.CostEstimationYear == costyear).ToListAsync();

            var listOfVc = villageCluster.GroupBy(x => new { x.Name })
                                        .Select(g => g.First())
                                        .ToList();

            var vcList = (from l in listOfVc
                          select new PrgVillageClusterListDto()
                          {
                              Name = l.Name
                          }).ToList();

            var listOfVillage = villageCluster.GroupBy(y => new { y.VillageID, y.VillageClusterLocationID })
                                  .Select(g => g.First())
                                  .ToList();

            var result1 = (from l in listOfVillage
                           select new PrgVillageClusterVillages()
                           {
                               Name = l.VillageID == null ? string.Empty : l.Village.Name
                           }).ToList();

            var components = prgCostEstOverall.GroupBy(y => new { y.ComponentID, })
                                  .Select(g => g.First())
                                  .ToList();
            var result2 = (from l in components
                           select new Precostestimationoverall()
                           {
                               ComponentName = l.ComponentID == null ? string.Empty : l.Component.Name
                           }).ToList();

            var activity = prgCostEstOverall.GroupBy(y => new { y.ActivityID, })
                                  .Select(g => g.First())
                                  .ToList();

            var result3 = (from a in activity
                           select new PrecostestimationoverallActivity()
                           {
                               ActivityName = a.ActivityID == null ? string.Empty : a.Activity.Name,
                               TotalUnitCost = Convert.ToInt64(a.TotalUnitCost),
                               UnitCost = Convert.ToInt64(a.UnitCost),
                               TotalUnit = Convert.ToInt64(a.TotalUnits),
                               CommunityConstribution = Convert.ToInt64(a.CommunityContribution),
                               FunderConstribution = Convert.ToInt64(a.FunderContribution),
                               OtherConstribution = Convert.ToInt64(a.OtherContribution),
                               AllTotal = Convert.ToInt64(a.TotalUnitCost + a.UnitCost + a.TotalUnits)
                           }).ToList();

            result.Add(new PrgVillageClusterListDto
            {
                Villages = result1,
                VillageClusterList = vcList,
                components = result2,
                Activity = result3
            });

            return result;
        }


        public async Task<FileDto> GetprogramdetailsToExcel(int ProgramId)
        {

            List<PreprogramdetailsDto> result = new List<PreprogramdetailsDto>();

            List<prgVillageClusterListDto> vcList = new List<prgVillageClusterListDto>();
            var ListOfVc = _villageClusterRepository.GetAll().Where(p => p.ProgramID == ProgramId).ToList().GroupBy(x => new { x.Name })
                                        .Select(g => g.First())
                                        .ToList();
            foreach (var vc in ListOfVc)
            {
                vcList.Add(new prgVillageClusterListDto
                {
                    Name = vc.Name,

                });
            }
            List<prgVillageClusterVillages> result1 = new List<prgVillageClusterVillages>();

            var ListOfVillage = _villageClusterRepository.GetAll().Where(p => p.ProgramID == ProgramId).ToList().GroupBy(y => new { y.VillageID, y.VillageClusterLocationID })
                                  .Select(g => g.First())
                                  .ToList();
            foreach (var village in ListOfVillage)
            {
                var VillageName = _VillageRepository.GetAll().Where(v => v.Id == village.VillageID).FirstOrDefault()?.Name;
                result1.Add(new prgVillageClusterVillages
                {
                    villageName = VillageName,

                });
            }

            List<programInformationListDto> result5 = new List<programInformationListDto>();

            var query = (from p in _programRepository.GetAll()
                         where (p.Id == ProgramId)
                         join d in _DonorRepository.GetAll() on p.DonorID equals d.Id
                         where (p.Id == ProgramId)
                         select new ProgramInformationListDto()
                         {

                             ProgramName = p.Name,
                             ShortDescription = p.ShortDescription,
                             LongDescription = p.LongDescription,
                             DonorID = p.DonorID,
                             ProgramSanctionedYear = p.ProgramSanctionedYear,
                             ProgramStartDate = p.ProgramStartDate,
                             PrgramEndDate = p.PrgramEndDate,
                             DonorName = d.CompanyName,
                             GantnAmountSanctionRupess = p.GantnAmountSanctionRupess

                         }).ToList();

            foreach (var q1 in query)
            {
                // var componentname = _componentRepository.GetAll().Where(v => v.Id == compo.ComponentID).FirstOrDefault()?.Name;
                result5.Add(new programInformationListDto
                {

                    ProgramName = q1.ProgramName,
                    ShortDescription = q1.ShortDescription,
                    LongDescription = q1.LongDescription,
                    DonorID = q1.DonorID,
                    ProgramSanctionedYear = q1.ProgramSanctionedYear,
                    ProgramStartDate = q1.ProgramStartDate,
                    PrgramEndDate = q1.PrgramEndDate,
                    //DonorName = d.CompanyName,
                    GantnAmountSanctionRupess = q1.GantnAmountSanctionRupess
                });
            }
            List<Precostestimationoverall> result2 = new List<Precostestimationoverall>();

            var Components = _ProgramCostEstimationOverAll.GetAll().Where(p => p.ProgramID == ProgramId).ToList().GroupBy(y => new { y.ComponentID, })
                                  .Select(g => g.First())
                                  .ToList();
            foreach (var compo in Components)
            {
                var componentname = _componentRepository.GetAll().Where(v => v.Id == compo.ComponentID).FirstOrDefault()?.Name;
                result2.Add(new Precostestimationoverall
                {
                    ComponentName = componentname,

                });
            }

            List<precostestimationoverallActivity> result3 = new List<precostestimationoverallActivity>();

            var Activity = _ProgramCostEstimationOverAll.GetAll().Where(p => p.ProgramID == ProgramId).ToList().GroupBy(y => new { y.ActivityID, })
                                  .Select(g => g.First())
                                  .ToList();
            foreach (var Acti in Activity)
            {
                var ActivityName = _activityRepository.GetAll().Where(v => v.Id == Acti.ActivityID).FirstOrDefault()?.Name;
                result3.Add(new precostestimationoverallActivity
                {
                    ActivityName = ActivityName,
                    TotalUnitCost = Convert.ToInt64(Acti.TotalUnitCost),
                    UnitCost = Convert.ToInt64(Acti.UnitCost),
                    TotalUnit = Convert.ToInt64(Acti.TotalUnits),
                    CommunityConstribution = Convert.ToInt64(Acti.CommunityContribution),
                    FunderConstribution = Convert.ToInt64(Acti.FunderContribution),
                    OtherConstribution = Convert.ToInt64(Acti.OtherContribution),



                });
            }

            result.Add(new PreprogramdetailsDto
            {

                Villages = result1,
                VillageClusterList = vcList,
                components = result2,
                Activity = result3,
                programdetails = result5

            });



            var file = new FileDto("ProgramDetails28_11_2018_113128.xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;

            // return _userListExcelExporter.Exportprogramdetails(result);



        }


        public void CreatePackage(string sFile, string imageFileName)
        {
            try
            {
                // Create a spreadsheet document by supplying the filepath. 
                SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.
                    Create(sFile, SpreadsheetDocumentType.Workbook);

                // Add a WorkbookPart to the document. 
                WorkbookPart workbookpart = spreadsheetDocument.AddWorkbookPart();
                workbookpart.Workbook = new Workbook();

                // Add a WorksheetPart to the WorkbookPart. 
                WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                // Add Sheets to the Workbook. 
                Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.
                    AppendChild<Sheets>(new Sheets());

                // Append a new worksheet and associate it with the workbook. 
                Sheet sheet = new Sheet()
                {
                    Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
                    SheetId = 1,
                    Name = "mySheet"
                };
                sheets.Append(sheet);

                var drawingsPart = worksheetPart.AddNewPart<DrawingsPart>();

                if (!worksheetPart.Worksheet.ChildElements.OfType<Drawing>().Any())
                {
                    worksheetPart.Worksheet.Append(new Drawing { Id = worksheetPart.GetIdOfPart(drawingsPart) });
                }

                if (drawingsPart.WorksheetDrawing == null)
                {
                    drawingsPart.WorksheetDrawing = new WorksheetDrawing();
                }

                var worksheetDrawing = drawingsPart.WorksheetDrawing;

                var imagePart = drawingsPart.AddImagePart(ImagePartType.Jpeg);


                //using (var excel = new OfficeOpenXml.ExcelPackage())
                //{
                //    var wks = excel.Workbook.Worksheets.Add("Sheet1");
                //    wks.Cells[1, 1].Value = "Adding picture below:";
                //    var pic = wks.Drawings.AddPicture("MyPhoto", new FileInfo(imageFileName));
                //    pic.SetPosition(2, 0, 1, 0);
                //    excel.SaveAs(new FileInfo("outputfile1234.xlsx"));
                //}
                using (var stream = new FileStream(imageFileName, FileMode.Open))
                {
                    imagePart.FeedData(stream);
                }

                Bitmap bm = new Bitmap(imageFileName);
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                var extentsCx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                var extentsCy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();

                var colOffset = 0;
                var rowOffset = 0;
                int colNumber = 5;
                int rowNumber = 10;

                var nvps = worksheetDrawing.Descendants<Xdr.NonVisualDrawingProperties>();
                var nvpId = nvps.Count() > 0 ?
                    (UInt32Value)worksheetDrawing.Descendants<Xdr.NonVisualDrawingProperties>().Max(p => p.Id.Value) + 1 :
                    1U;

                var oneCellAnchor = new Xdr.OneCellAnchor(
                    new Xdr.FromMarker
                    {
                        ColumnId = new Xdr.ColumnId((colNumber - 1).ToString()),
                        RowId = new Xdr.RowId((rowNumber - 1).ToString()),
                        ColumnOffset = new Xdr.ColumnOffset(colOffset.ToString()),
                        RowOffset = new Xdr.RowOffset(rowOffset.ToString())
                    },
                    new Xdr.Extent { Cx = extentsCx, Cy = extentsCy },
                    new Xdr.Picture(
                        new Xdr.NonVisualPictureProperties(
                            new Xdr.NonVisualDrawingProperties { Id = nvpId, Name = "Picture " + nvpId, Description = imageFileName },
                            new Xdr.NonVisualPictureDrawingProperties(new A.PictureLocks { NoChangeAspect = true })
                        ),
                        new Xdr.BlipFill(
                            new A.Blip { Embed = drawingsPart.GetIdOfPart(imagePart), CompressionState = A.BlipCompressionValues.Print },
                            new A.Stretch(new A.FillRectangle())
                        ),
                        new Xdr.ShapeProperties(
                            new A.Transform2D(
                                new A.Offset { X = 0, Y = 0 },
                                new A.Extents { Cx = extentsCx, Cy = extentsCy }
                            ),
                            new A.PresetGeometry { Preset = A.ShapeTypeValues.Rectangle }
                        )
                    ),
                    new Xdr.ClientData()
                );

                worksheetDrawing.Append(oneCellAnchor);

                workbookpart.Workbook.Save();




                // Close the document. 
                spreadsheetDocument.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        private Bitmap Base64StringToBitmap(string base64String)
        {
            var bitmapData = Convert.FromBase64String(FixBase64ForImage(base64String));
            var streamBitmap = new System.IO.MemoryStream(bitmapData);
            var bitmap = new Bitmap((Bitmap)Image.FromStream(streamBitmap));
            return bitmap;
        }

        private string FixBase64ForImage(string Image)
        {
            var sbText = new System.Text.StringBuilder(Image, Image.Length);
            sbText.Replace("\r\n", String.Empty);
            sbText.Replace(" ", String.Empty);
            return sbText.ToString();
        }


        //archita
        public async Task<FileDto> CreateExcelDoc(string fileName, int programId, string year)
        {


            var abccc = GetProgramDetailsbyvillageclustor(programId);
            string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";
            string b = @"E:\Reports\";
            string abc = b + a;
            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))
            {

                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();
                // var sImagePath = _appFolders.LogoImgFolder + "Reportlogo.png";
                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));

                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                BlipFill blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                Bitmap bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                ShapeProperties sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                Position pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                Extent ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                AbsoluteAnchor anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                WorksheetDrawing wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                // sheets.Append(sheet);
                wb.Append(fv);

                //archita
                List<PreprogramdetailsDto> result = new List<PreprogramdetailsDto>();

                //List<prgVillageClusterListDto> vcList = new List<prgVillageClusterListDto>();
                //var ListOfVc = _villageClusterRepository.GetAll().Where(p => p.ProgramID == programId).ToList().GroupBy(x => new { x.Name })
                //                            .Select(g => g.First())
                //                            .ToList();
                //foreach (var vc in ListOfVc)
                //{
                //    vcList.Add(new prgVillageClusterListDto
                //    {
                //        Name = vc.Name,
                //        VillageName = vc.Village.Name,
                //    });
                //}
                List<prgVillageClusterVillages> result1 = new List<prgVillageClusterVillages>();

                var ListOfVillage = _villageClusterRepository.GetAll().Where(p => p.ProgramID == programId).ToList().GroupBy(y => new { y.VillageID, y.VillageClusterLocationID })
                                      .Select(g => g.First())
                                      .ToList();
                foreach (var village in ListOfVillage)
                {
                    var VillageName = _VillageRepository.GetAll().Where(v => v.Id == village.VillageID).FirstOrDefault()?.Name;
                    result1.Add(new prgVillageClusterVillages
                    {
                        villageName = VillageName,


                    });
                }

                List<programInformationListDto> result5 = new List<programInformationListDto>();

                var query = (from p in _programRepository.GetAll()
                             where (p.Id == programId)
                             join d in _DonorRepository.GetAll() on p.DonorID equals d.Id
                             where (p.Id == programId)
                             select new ProgramInformationListDto()
                             {

                                 ProgramName = p.Name,
                                 ShortDescription = p.ShortDescription,
                                 LongDescription = p.LongDescription,
                                 DonorID = p.DonorID,
                                 ProgramSanctionedYear = p.ProgramSanctionedYear,
                                 ProgramStartDate = p.ProgramStartDate,
                                 PrgramEndDate = p.PrgramEndDate,
                                 DonorName = d.CompanyName,
                                 GantnAmountSanctionRupess = p.GantnAmountSanctionRupess

                             }).ToList();

                foreach (var q1 in query)
                {
                    // var componentname = _componentRepository.GetAll().Where(v => v.Id == compo.ComponentID).FirstOrDefault()?.Name;
                    result5.Add(new programInformationListDto
                    {

                        ProgramName = q1.ProgramName,
                        ShortDescription = q1.ShortDescription,
                        LongDescription = q1.LongDescription,
                        DonorID = q1.DonorID,
                        ProgramSanctionedYear = q1.ProgramSanctionedYear,
                        ProgramStartDate = q1.ProgramStartDate,
                        PrgramEndDate = q1.PrgramEndDate,
                        DonorName = q1.DonorName,
                        GantnAmountSanctionRupess = q1.GantnAmountSanctionRupess
                    });
                }
                List<Precostestimationoverall> result2 = new List<Precostestimationoverall>();

                var Components = _PrgCostEstRepository.GetAll().Where(p => p.ProgramID == programId && p.CostEstimationYear == year).ToList().GroupBy(y => new { y.ComponentID })

                                      .ToList();
                //foreach (var compo in Components)
                //{
                //    var componentname = _componentRepository.GetAll().Where(v => v.Id == compo.ComponentID).FirstOrDefault()?.Name;
                //    result2.Add(new Precostestimationoverall
                //    {
                //        ComponentName = componentname,

                //    });
                //}

                List<precostestimationoverallActivity> result3 = new List<precostestimationoverallActivity>();

                var Activity = _PrgCostEstRepository.GetAll().Where(p => p.ProgramID == programId && p.CostEstimationYear == year).ToList().GroupBy(y => new { y.ActivityID, })
                                      .Select(g => g.First())
                                      .ToList();
                foreach (var Acti in Activity)
                {
                    var ActivityName = _activityRepository.GetAll().Where(v => v.Id == Acti.ActivityID).FirstOrDefault()?.Name;
                    result3.Add(new precostestimationoverallActivity
                    {
                        ActivityName = ActivityName,
                        TotalUnitCost = Convert.ToInt64(Acti.TotalUnitCost),
                        UnitCost = Convert.ToInt64(Acti.UnitCost),
                        TotalUnit = Convert.ToInt64(Acti.TotalUnits),
                        CommunityConstribution = Convert.ToInt64(Acti.CommunityContribution),
                        FunderConstribution = Convert.ToInt64(Acti.FunderContribution),
                        OtherConstribution = Convert.ToInt64(Acti.OtherContribution),



                    });
                }

                result.Add(new PreprogramdetailsDto
                {

                    Villages = result1,
                    //VillageClusterList = vcList,
                    components = result2,
                    Activity = result3,
                    programdetails = result5

                });

                // SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());
                uint CellNO = 12;

                // Constructing header
                Row row = new Row();
                Row row1 = new Row();
                Row row2 = new Row();
                Row row3 = new Row();
                Row row4 = new Row();
                Row row5 = new Row();
                Row row6 = new Row();
                Row row7 = new Row();
                Row row8 = new Row();
                Row row9 = new Row();





                foreach (var employee in result5)

                {

                    Cell cellString811 = InsertCellInWorksheet("D", 1, wsp);

                    cellString811.CellValue = new CellValue("Watershed Organisation Trust(WOTR)");
                    cellString811.DataType = new EnumValue<CellValues>(CellValues.String);
                    Run run1 = new Run();
                    run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                    RunProperties run1Properties = new RunProperties();
                    run1Properties.Append(new Bold());
                    run1.RunProperties = run1Properties;
                    InlineString inlineString = new InlineString();
                    inlineString.Append(run1);
                    cellString811.Append(inlineString);

                    //Cell cell0 = InsertCellInWorksheet("A", 2, wsp);
                    //cell0.CellValue = new CellValue(" ");
                    //cell0.DataType = new EnumValue<CellValues>(CellValues.Number);

                    Cell cellString81 = InsertCellInWorksheet("D", 2, wsp);

                    cellString81.CellValue = new CellValue("Details of the Programme- Project Area, Components and Activities");
                    cellString81.DataType = new EnumValue<CellValues>(CellValues.String);
                    Run run2 = new Run();
                    run2.Append(new Text("Details of the Programme- Project Area, Components and Activities"));
                    RunProperties run2Properties = new RunProperties();
                    Color color3 = new Color() { Rgb = "2F75B5" };
                    FontSize fontSize3 = new FontSize() { Val = 16D };
                    run2Properties.Append(new Bold());
                    run2Properties.Append(color3);
                    run2Properties.Append(fontSize3);
                    run2.RunProperties = run2Properties;
                    InlineString inlineString1 = new InlineString();
                    inlineString1.Append(run2);
                    cellString81.Append(inlineString1);

                    //Cell cell0 = InsertCellInWorksheet("A", 2, wsp);
                    //cell0.CellValue = new CellValue(" ");
                    //cell0.DataType = new EnumValue<CellValues>(CellValues.Number);


                    Cell cell45 = InsertCellInWorksheet("B", 4, wsp);
                    cell45.CellValue = new CellValue("Program Title");
                    cell45.DataType = new EnumValue<CellValues>(CellValues.Number);

                    Cell cellString = InsertCellInWorksheet("C", 4, wsp);
                    cellString.CellValue = new CellValue(employee.ProgramName.ToString());
                    cellString.DataType = new EnumValue<CellValues>(CellValues.String);


                    Cell cell1 = InsertCellInWorksheet("B", 5, wsp);
                    cell1.CellValue = new CellValue("Funding Agency");
                    cell1.DataType = new EnumValue<CellValues>(CellValues.Number);

                    Cell cellString1 = InsertCellInWorksheet("C", 5, wsp);
                    cellString1.CellValue = new CellValue(employee.DonorName.ToString());
                    cellString1.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cell2 = InsertCellInWorksheet("B", 6, wsp);
                    cell2.CellValue = new CellValue("Sanction Year");
                    cell2.DataType = new EnumValue<CellValues>(CellValues.Number);

                    Cell cellString2 = InsertCellInWorksheet("C", 6, wsp);
                    cellString2.CellValue = new CellValue(employee.ProgramSanctionedYear.ToString());
                    cellString2.DataType = new EnumValue<CellValues>(CellValues.Number);

                    Cell cell3 = InsertCellInWorksheet("B", 7, wsp);
                    cell3.CellValue = new CellValue("Program Start Date");
                    cell3.DataType = new EnumValue<CellValues>(CellValues.Number);

                    Cell cellString3 = InsertCellInWorksheet("C", 7, wsp);
                    cellString3.CellValue = new CellValue(employee.ProgramStartDate.ToString("dd/MM/yyyy"));
                    cellString3.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cell4 = InsertCellInWorksheet("B", 8, wsp);
                    cell4.CellValue = new CellValue("Program End Date");
                    cell4.DataType = new EnumValue<CellValues>(CellValues.Number);

                    Cell cellString4 = InsertCellInWorksheet("C", 8, wsp);
                    cellString4.CellValue = new CellValue(employee.PrgramEndDate.ToString("dd/MM/yyyy"));
                    cellString4.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cell5 = InsertCellInWorksheet("B", 9, wsp);
                    cell5.CellValue = new CellValue("Grant Amount");
                    cell5.DataType = new EnumValue<CellValues>(CellValues.Number);

                    Cell cellString5 = InsertCellInWorksheet("C", 9, wsp);
                    cellString5.CellValue = new CellValue(employee.GantnAmountSanctionRupess.ToString());
                    cellString5.DataType = new EnumValue<CellValues>(CellValues.Number);

                    Cell cell51 = InsertCellInWorksheet("B", 10, wsp);
                    cell51.CellValue = new CellValue("Year");
                    cell51.DataType = new EnumValue<CellValues>(CellValues.Number);

                    Cell cellString51 = InsertCellInWorksheet("C", 10, wsp);
                    cellString51.CellValue = new CellValue(year.ToString());
                    cellString51.DataType = new EnumValue<CellValues>(CellValues.Number);


                }


                row5.Append(

                 ConstructCell("", CellValues.String));
                sd.AppendChild(row5);

                row6.Append(

                 ConstructCell("List Of Villages For Project Clustor Wise", CellValues.String));
                sd.AppendChild(row6);







                List<VillageCluster> villageClusters = new List<VillageCluster>();




                var prevName = "";


                foreach (var item in ListOfVillage)
                {
                    Row row13 = new Row();

                    if (prevName != item.Name)
                    {
                        prevName = item.Name;
                        row13.Append(


                       ConstructCell(item.Name.ToString(), CellValues.String, 1),
                        ConstructCell(item.Village.Name.ToString(), CellValues.String, 1));

                    }
                    else
                    if (item.Name == prevName)
                    {
                        row13.Append(

                        ConstructCell(" ", CellValues.String, 1),
                        ConstructCell(item.Village.Name.ToString(), CellValues.String, 1));

                    }
                    sd.AppendChild(row13);




                }




                row7.Append(

                 ConstructCell("", CellValues.String));
                sd.AppendChild(row7);






                //archita
                row9.Append(

                 ConstructCell("", CellValues.String));
                sd.AppendChild(row9);
                row8.Append(
                    ConstructCell("Component", CellValues.String, 2),
                   ConstructCell("Activity", CellValues.String, 2),
                 ConstructCell("TotalUnit ", CellValues.String, 2),
                    ConstructCell("Unit Cost", CellValues.String, 2),
                    ConstructCell("Total Unit Cost", CellValues.Number, 2),
                    ConstructCell("Community Contribution", CellValues.String, 2),
                    ConstructCell("Funder Contribution", CellValues.String, 2),
                    ConstructCell("Other Contribution", CellValues.Number, 2));
                sd.AppendChild(row8);

                var totalcost = 0.00;
                var toatlcost12 = 0.00;
                var TotalUnits = 0.00;
                var unitcost = 0.00;
                var TotalUnitCost = 0.00;
                var communitycontribution = 0.00;
                var fundercontribution = 0.00;
                var othercontribution = 0.00;
                var comp = "";

                foreach (var item in Components)
                {

                    Row row17 = new Row();
                    Row row18 = new Row();


                    foreach (var item1 in item)
                    {

                        TotalUnits += Convert.ToInt64(item1.TotalUnits);
                        unitcost += Convert.ToInt64(item1.UnitCost);
                        TotalUnitCost += Convert.ToInt64(item1.TotalUnitCost);
                        communitycontribution += Convert.ToDouble(item1.CommunityContribution);
                        fundercontribution += Convert.ToDouble(item1.FunderContribution);
                        othercontribution += Convert.ToDouble(item1.OtherContribution);
                        row17 = new Row();
                        row18 = new Row();
                        if (comp != item1.Component.Name)
                        {
                            comp = item1.Component.Name;
                            row17.Append(

                           ConstructCell(item1.Component.Name.ToString(), CellValues.String, 1),
                           ConstructCell(item1.Activity.Name.ToString(), CellValues.Number, 1),
                            ConstructCell(item1.TotalUnits.ToString(), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(item1.UnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(item1.TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(item1.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(item1.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(item1.OtherContribution).ToString("#,##0.00"), CellValues.Number, 1));



                        }
                        else

                        if (item1.Component.Name == comp)
                        {
                            row17.Append(
                                ConstructCell("", CellValues.String, 1),
                           ConstructCell(item1.Activity.Name.ToString(), CellValues.Number, 1),
                           ConstructCell(item1.TotalUnits.ToString(), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(item1.UnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(item1.TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(item1.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(item1.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(item1.OtherContribution).ToString("#,##0.00"), CellValues.Number, 1));




                        }
                        sd.AppendChild(row17);

                    }

                    row18.Append(
                           ConstructCell("Total", CellValues.String, 2),
                            ConstructCell(" ", CellValues.String, 2),

                            ConstructCell(TotalUnits.ToString(), CellValues.Number, 2),
                              ConstructCell(Convert.ToDecimal(unitcost).ToString("#,##0.00"), CellValues.Number, 2),
                              ConstructCell(Convert.ToDecimal(TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 2),
                              ConstructCell(Convert.ToDecimal(communitycontribution).ToString("#,##0.00"), CellValues.Number, 2),
                              ConstructCell(Convert.ToDecimal(fundercontribution).ToString("#,##0.00"), CellValues.Number, 2),
                              ConstructCell(Convert.ToDecimal(othercontribution).ToString("#,##0.00"), CellValues.Number, 2)
                              );
                    sd.Append(row18);






                    // if (comp != item.Component.Name)
                    //{
                    //    row18.Append(
                    //       ConstructCell("Total", CellValues.String, 2),
                    //        ConstructCell(" ", CellValues.String, 2),

                    //        ConstructCell(TotalUnits.ToString(), CellValues.Number, 2),
                    //          ConstructCell(Convert.ToDecimal(unitcost).ToString("#,##0.00"), CellValues.Number, 2),
                    //          ConstructCell(Convert.ToDecimal(TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 2),
                    //          ConstructCell(Convert.ToDecimal(communitycontribution).ToString("#,##0.00"), CellValues.Number, 2),
                    //          ConstructCell(Convert.ToDecimal(fundercontribution).ToString("#,##0.00"), CellValues.Number, 2),
                    //          ConstructCell(Convert.ToDecimal(othercontribution).ToString("#,##0.00"), CellValues.Number, 2)
                    //          );
                    //    sd.Append(row18);

                    //}

                }









                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();
                // sd.Worksheet.Save();





            }


            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;



        }

        //archita
        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
        private Cell ConstructCell(string value, CellValues dataType, uint styleIndex = 0)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType),
                StyleIndex = styleIndex
            };
        }



        private Stylesheet GenerateStylesheet()
        {
            Stylesheet styleSheet = null;

            Fonts fonts = new Fonts(
                new DocumentFormat.OpenXml.Spreadsheet.Font( // Index 0 - default
                    new FontSize() { Val = 11 }

                ),
                new DocumentFormat.OpenXml.Spreadsheet.Font( // Index 1 - header
                    new FontSize() { Val = 11 },
                    new Bold(),
                    new Color() { Rgb = "000000" }

                ));

            Fills fills = new Fills(
                    new Fill(new PatternFill() { PatternType = PatternValues.None }), // Index 0 - default
                    new Fill(new PatternFill() { PatternType = PatternValues.Gray125 }), // Index 1 - default
                    new Fill(new PatternFill(new ForegroundColor { Rgb = new HexBinaryValue() { Value = "D0CECE" } })
                    { PatternType = PatternValues.Solid }) // Index 2 - header
                );

            Borders borders = new Borders(
                    new Border(), // index 0 default
                    new Border( // index 1 black border
                        new LeftBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new RightBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new TopBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new BottomBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                );

            CellFormats cellFormats = new CellFormats(
                    new CellFormat(), // default
                    new CellFormat { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }, // body
                    new CellFormat { FontId = 1, FillId = 2, BorderId = 1, ApplyFill = true } // header
                );

            styleSheet = new Stylesheet(fonts, fills, borders, cellFormats);

            return styleSheet;
        }


        private List<ProgramInformationListDto> ConvertToAuditLogListDtos(List<ProgramInformationListDto> results)
        {
            return results.Select(
                result =>
                {
                    var auditLogListDto = ObjectMapper.Map<ProgramInformationListDto>(result.ExecutionTime);
                    auditLogListDto.ProgramName = result.ProgramName;
                    auditLogListDto.ServiceName = _namespaceStripper.StripNameSpace(auditLogListDto.ServiceName);
                    return auditLogListDto;
                }).ToList();
        }

        private IQueryable<ProgramInformationListDto> CreateprogramDetailsQuery(Precostestimationoverall input)
        {
            var query = from programinfo in _programRepository.GetAll()
                        where programinfo.Name == input.ComponentName
                        select new ProgramInformationListDto { ProgramName = programinfo.Name };

            //query = query
            //    .WhereIf(!input.ProgramName.IsNullOrWhiteSpace(), item => item.ProgramName.Contains(input.ProgramName));
            return query;
        }
        public List<ProgramInformationListDto> GetProgramDetailssbyID(int ProgramId)
        {


            var query = (from p in _programRepository.GetAll()

                         join d in _DonorRepository.GetAll() on p.DonorID equals d.Id

                         where (p.Id == ProgramId)

                         select new ProgramInformationListDto()
                         {

                             ProgramName = p.Name,
                             ShortDescription = p.ShortDescription,
                             LongDescription = p.LongDescription,
                             DonorID = p.DonorID,
                             ProgramSanctionedYear = p.ProgramSanctionedYear,
                             ProgramStartDate = p.ProgramStartDate,
                             PrgramEndDate = p.PrgramEndDate,
                             DonorName = d.CompanyName,
                             GantnAmountSanctionRupess = p.GantnAmountSanctionRupess
                         }).ToList();

            return query;

        }




        public ProgramInformationListDto GetProgramDetailsByProgramID(int input)
        {
            var query = (from p in _programRepository.GetAll()
                         where (p.Id == input)
                         join pmr in _ProgramManagerMappingRepository.GetAll()
                         on p.Id equals pmr.ProgramID

                         select new ProgramInformationListDto()
                         {
                             Id = input,
                             ProgramName = p.Name,
                             ShortDescription = p.ShortDescription,
                             LongDescription = p.LongDescription,
                             DonorID = p.DonorID,
                             ProgramSanctionedYear = p.ProgramSanctionedYear,
                             GantnAmountSanctionRupess = p.GantnAmountSanctionRupess,
                             ProgramStartDate = p.ProgramStartDate,
                             PrgramEndDate = p.PrgramEndDate,
                             ProgramSubmitDate = p.ProgramSubmitDate,
                             ProgramApprovaltDate = p.ProgramApprovaltDate,
                             ProgramManagerId = p.ManagerID,
                             ProgramBadgeImage = p.ProgramBadgeImage,
                             ProgramBadgeColor = p.ProgramBadgeColor,
                             CurrentProgramManagerID = p.ProgramManagerID
                         }).FirstOrDefault();

            return query;
        }
        public List<ProgramInformationListDto> GetAllProgramsDetailsRegionWiseWise(string year)
        {

            var programManagerProgramYearwiseStateWiseBudgets = new List<ProgramInformationListDto>();

            var result = GetAllRegionSummary(year);

            var results = from line in result
                          group line by new
                          {
                              line.RegionName,

                          } into g
                          select new ProgramInformationListDto
                          {
                              ProjectCostCBO = g.Sum(pc => pc.FunderConstribution).ToString(),
                              LocalContribution = g.Sum(pc => pc.CommunityConstribution).ToString(),
                              GovtContribution = g.Sum(pc => pc.OtherConstribution).ToString(),
                              GrandTotal = g.Sum(pc => pc.GrantTotal).ToString(),
                              RegionName = g.Key.RegionName
                          };
            foreach (var item in results)
            {
                programManagerProgramYearwiseStateWiseBudgets.Add(new ProgramInformationListDto
                {
                    LocationName = item.RegionName,
                    ProjectCostGrant = item.GrandTotal,
                    ProjectCostCBO = item.ProjectCostCBO,
                    LocalContribution = item.LocalContribution,
                    GovtContribution = item.GovtContribution,
                    GrandTotal = item.GrandTotal

                });
            }
            return programManagerProgramYearwiseStateWiseBudgets;




            //throw new NotImplementedException();
        }

        public List<RegionWiseDetailsList> GetAllProgramsDetailsRegionDetailsWise(string regionId, string year)
        {

            try
            {
                string value;
                var allChartData = new RegionWiseDetailsList();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramYearwiseStateWiseBudgets = new List<RegionWiseDetailsList>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    ConnectionConnect(conn);
                    using (var cmdTwo = new SqlCommand("", conn))
                    {
                        //ProjectWiseInfoLineChart
                        var dataTable = new System.Data.DataTable();
                        cmdTwo.CommandTimeout = 0;
                        cmdTwo.CommandText = "[dbo].[SP_RegionWisedetailsReport]";
                        cmdTwo.CommandType = CommandType.StoredProcedure;
                        cmdTwo.Parameters.Add(new SqlParameter("@CostEstimationYear", year));
                        cmdTwo.Parameters.Add(new SqlParameter("@RRCID", regionId));


                        SqlDataReader dr = cmdTwo.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerProgramYearwiseStateWiseBudgets.Add(new RegionWiseDetailsList
                                    {
                                        ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),
                                        CompanyName = (row["CompanyName"] == DBNull.Value) ? "" : Convert.ToString(row["CompanyName"]),
                                        ActionAreaName = (row["ActionAreaName"] == DBNull.Value) ? "" : Convert.ToString(row["ActionAreaName"]),
                                        SubactionAreaName = (row["SubActionAreaName"] == DBNull.Value) ? "" : Convert.ToString(row["SubActionAreaName"]),
                                        ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),

                                        ProjectCostGrant = (row["programGrantTotal"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["programGrantTotal"]),
                                        ProjectCostCBO = (row["CommunityContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["CommunityContribution"]),
                                        LocalContribution = (row["FunderContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["FunderContribution"]),
                                        GovtContribution = (row["OtherContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["OtherContribution"]),
                                        GrandTotal = (row["Total"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Total"])


                                    });
                                }

                            }
                        }
                        conn.Close();
                    }


                }
                return programManagerProgramYearwiseStateWiseBudgets;
            }
            catch (Exception ex)
            {

                throw;
            }




            //throw new NotImplementedException();
        }

        public List<precostestimationoverallregionsummuery> GetAllRegionSummary(string year)
        {


            try
            {
                string value;
                var allChartData = new precostestimationoverallregionsummuery();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramYearwiseStateWiseBudgets = new List<precostestimationoverallregionsummuery>();
                var regionwisesummuryreport = new List<precostestimationoverallregionsummuery>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    ConnectionConnect(conn);
                    using (var cmdTwo = new SqlCommand("", conn))
                    {
                        //ProjectWiseInfoLineChart
                        var dataTable = new System.Data.DataTable();
                        cmdTwo.CommandTimeout = 0;
                        cmdTwo.CommandText = "[dbo].[SP_RegionWiseSummury]";
                        cmdTwo.CommandType = CommandType.StoredProcedure;
                        cmdTwo.Parameters.Add(new SqlParameter("@CostEstimationYear", year));


                        SqlDataReader dr = cmdTwo.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerProgramYearwiseStateWiseBudgets.Add(new precostestimationoverallregionsummuery
                                    {
                                        locationId = (row["LocationId"] == DBNull.Value) ? 0 : Convert.ToInt16(row["LocationId"]),
                                        programName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),
                                        RegionName = (row["LocationName"] == DBNull.Value) ? "" : Convert.ToString(row["LocationName"]),
                                        CommunityConstribution = (row["CommunityContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["CommunityContribution"]),
                                        FunderConstribution = (row["FunderContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["FunderContribution"]),
                                        OtherConstribution = (row["OtherContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["OtherContribution"]),
                                        TotalAll = (row["Total"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Total"])

                                    });
                                }

                            }
                        }
                        conn.Close();
                    }


                }

                foreach (var item in programManagerProgramYearwiseStateWiseBudgets)
                {
                    var regionName = "";
                    if (_LocationRepository.GetAll().Where(ss => ss.Id == item.locationId).FirstOrDefault().ParentLocation == 0)
                    {
                        regionName = _LocationRepository.GetAll().Where(ss => ss.Id == item.locationId).FirstOrDefault().Name;

                    }
                    else
                    {
                        int Parentlocation = _LocationRepository.GetAll().Where(ss => ss.Id == item.locationId).FirstOrDefault().ParentLocation;
                        regionName = _LocationRepository.GetAll().Where(ss => ss.Id == Parentlocation).FirstOrDefault().Name;
                    }

                    regionwisesummuryreport.Add(new precostestimationoverallregionsummuery
                    {
                        locationId = item.locationId,
                        programName = item.programName,
                        RegionName = regionName,
                        CommunityConstribution = item.CommunityConstribution,
                        FunderConstribution = item.FunderConstribution,
                        OtherConstribution = item.OtherConstribution,
                        TotalAll = item.TotalAll,
                        GrantTotal = _programRepository.GetAll().Where(cc => cc.Name == item.programName).FirstOrDefault().GantnAmountSanctionRupess,

                    });
                }


                return regionwisesummuryreport.OrderBy(xx => xx.RegionName).ToList();
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public async Task<FileDto> CreateExcelRegionWisWise(string fileName, string year)
        {
            var result = GetAllProgramsDetailsRegionWiseWise(year);

            string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";

            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))


            {

                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();


                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                BlipFill blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                Bitmap bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                ShapeProperties sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                Position pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                Extent ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                AbsoluteAnchor anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                WorksheetDrawing wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                // sheets.Append(sheet);
                wb.Append(fv);
                // wb.Append(sheets);


                // xl.WorkbookPart.Workbook.Save();
                // xl.Close();

                //archita

                Row row9 = new Row();
                Row row8 = new Row();
                Row row10 = new Row();
                Row row11 = new Row();


                Cell cell8 = InsertCellInWorksheet("D", 1, wsp);
                cell8.CellValue = new CellValue("Watershed Organisation Trust(WOTR)");
                cell8.DataType = new EnumValue<CellValues>(CellValues.Number);
                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cell8.Append(inlineString);

                Cell cellString8 = InsertCellInWorksheet("D", 2, wsp);
                cellString8.CellValue = new CellValue("Regionwise Project sanction details");
                cellString8.DataType = new EnumValue<CellValues>(CellValues.String);
                cell8.StyleIndex = 1;
                Run run2 = new Run();
                run2.Append(new Text("Regionwise Project sanction details"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cellString8.Append(inlineString1);

                
                Cell cellString = InsertCellInWorksheet("D", 3, wsp);
                cellString.CellValue = new CellValue("Year: "+ year);
                cellString.DataType = new EnumValue<CellValues>(CellValues.String);



                row9.Append(

                 ConstructCell("", CellValues.String));
                sd.AppendChild(row9);
                row10.Append(

                 ConstructCell("", CellValues.String));
                sd.AppendChild(row10);
                row11.Append(

                 ConstructCell("", CellValues.String));
                sd.AppendChild(row11);


                row8.Append(


                    ConstructCell("Region ", CellValues.String, 2),
                    ConstructCell("Project Cost (Grant) ", CellValues.String, 2),

                    // ConstructCell("Project Cost (Grant)", CellValues.Number, 2),
                    ConstructCell("Project Cost (CBOs)", CellValues.Number, 2),
                    ConstructCell("Local Contribution", CellValues.Number, 2),
                    ConstructCell("Govt. Contribution", CellValues.Number, 2),
                    ConstructCell("Total", CellValues.Number, 2));

                sd.AppendChild(row8);

                foreach (var acti in result)
                {
                    row8 = new Row();

                    row8.Append(


                           ConstructCell(acti.LocationName.ToString(), CellValues.Number, 1),
                          ConstructCell(acti.ProjectCostGrant.ToString(), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(acti.ProjectCostCBO).ToString("#,##0.00"), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(acti.LocalContribution).ToString("#,##0.00"), CellValues.Number, 1),
                           ConstructCell(Convert.ToDecimal(acti.GovtContribution).ToString("#,##0.00"), CellValues.Number, 1),
                           ConstructCell(acti.GrandTotal.ToString(), CellValues.Number, 1));

                    sd.AppendChild(row8);
                }

                sheets.Append(sheet);
                wb.Append(sheets);
                //AddImage(worksheetPart, imagePath1, "My first image", 1, 1); // A1
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();


            }


            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;




        }
        //archita
        //archita
        private void BuildWorkbook(string filename)
        {
            try
            {

                using (SpreadsheetDocument xl = SpreadsheetDocument.Create(filename, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart wbp = xl.AddWorkbookPart();
                    WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                    Workbook wb = new Workbook();
                    FileVersion fv = new FileVersion();
                    fv.ApplicationName = "Microsoft Office Excel";
                    Worksheet ws = new Worksheet();
                    SheetData sd = new SheetData();
                    WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                    stylePart.Stylesheet = GenerateStylesheet();
                    stylePart.Stylesheet.Save();

                    //string sImagePath = "D:/wotr28-08-2018/WOTR.pm/WOTR/src/WOTR.PM.Web.Host/src/assets/common/images/WOTRLogoReport.png";
                    var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                    DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                    ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                    using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                    {
                        imgp.FeedData(fs);
                    }

                    NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                    nvdp.Id = 1025;
                    nvdp.Name = "WOTRLogoReport";
                    nvdp.Description = "WOTRLogoReport";
                    DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                    picLocks.NoChangeAspect = true;
                    picLocks.NoChangeArrowheads = true;
                    NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                    nvpdp.PictureLocks = picLocks;
                    NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                    nvpp.NonVisualDrawingProperties = nvdp;
                    nvpp.NonVisualPictureDrawingProperties = nvpdp;

                    DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                    stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                    BlipFill blipFill = new BlipFill();
                    DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                    blip.Embed = dp.GetIdOfPart(imgp);
                    blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                    blipFill.Blip = blip;
                    blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                    blipFill.Append(stretch);

                    DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                    DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                    offset.X = 0;
                    offset.Y = 0;
                    t2d.Offset = offset;
                    Bitmap bm = new Bitmap(sImagePath);
                    //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                    //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                    //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                    DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                    extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                    extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                    bm.Dispose();
                    t2d.Extents = extents;
                    ShapeProperties sp = new ShapeProperties();
                    sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                    sp.Transform2D = t2d;
                    DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                    prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                    prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                    sp.Append(prstGeom);
                    sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                    DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                    picture.NonVisualPictureProperties = nvpp;
                    picture.BlipFill = blipFill;
                    picture.ShapeProperties = sp;

                    Position pos = new Position();
                    pos.X = 0;
                    pos.Y = 0;
                    Extent ext = new Extent();
                    ext.Cx = extents.Cx;
                    ext.Cy = extents.Cy;
                    AbsoluteAnchor anchor = new AbsoluteAnchor();
                    anchor.Position = pos;
                    anchor.Extent = ext;
                    anchor.Append(picture);
                    anchor.Append(new ClientData());
                    WorksheetDrawing wsd = new WorksheetDrawing();
                    wsd.Append(anchor);
                    Drawing drawing = new Drawing();
                    drawing.Id = dp.GetIdOfPart(imgp);

                    wsd.Save(dp);

                    ws.Append(sd);
                    ws.Append(drawing);
                    wsp.Worksheet = ws;
                    wsp.Worksheet.Save();
                    Sheets sheets = new Sheets();
                    Sheet sheet = new Sheet();
                    sheet.Name = "Sheet1";
                    sheet.SheetId = 1;
                    sheet.Id = wbp.GetIdOfPart(wsp);
                    sheets.Append(sheet);
                    wb.Append(fv);
                    wb.Append(sheets);

                    xl.WorkbookPart.Workbook = wb;
                    xl.WorkbookPart.Workbook.Save();
                    xl.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.ReadLine();
            }
        }



        public List<ProgramInformationListDto> GetAllRegions()
        {

            var query = (from p in _LocationRepository.GetAll()
                         select new ProgramInformationListDto()
                         {
                             LocationName = p.Name,
                             RRCID = p.Id
                             //ProgramName = p.Name,
                             //ShortDescription = p.ShortDescription,
                             //LongDescription = p.LongDescription,
                             //DonorID = p.DonorID,
                             //ProgramSanctionedYear = p.ProgramSanctionedYear,
                             //ProgramStartDate = p.ProgramStartDate,
                             //PrgramEndDate = p.PrgramEndDate,
                             //Id=p.Id

                         }).ToList();
            return query;
            //throw new NotImplementedException();
        }


        public List<ProgramInformationListDto> GetAllActionWise()
        {

            var temp1 = (from p in _ProgramCostEstimationOverAll.GetAll()

                         join L in _villageClusterRepository.GetAll() on p.ProgramID equals L.ProgramID
                         where L.RRCID != null
                         join r in _LocationRepository.GetAll() on L.RRCID equals r.Id
                         where L.RRCID != null
                         join prog in _programRepository.GetAll() on p.ProgramID equals prog.Id
                         join ac in _activityRepository.GetAll() on p.ActivityID equals ac.Id


                         group prog by

                            new { prog.Id } into g

                         select new ProgramInformationListDto()
                         {


                             //RRCID = g.Key.RRCID,
                             // ProjectCostGrant = _programRepository.GetAll().Where(p => p.Id == g.Key.RRCID).Sum(t => t.GantnAmountSanctionRupess).ToString(),

                             ProjectCostGrant = _programRepository.GetAll().Where(p => p.Id == g.Key.Id).Sum(t => t.GantnAmountSanctionRupess).ToString(),
                             ProgramName = _programRepository.GetAll().Where(p => p.Id == g.Key.Id).FirstOrDefault().Name,
                             //RRCID = g.Key.RRCID,
                             LocationName = _LocationRepository.GetAll().Where(p => p.Id == g.Key.Id).FirstOrDefault().Name,
                             ProjectCostCBO = _ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.Id).Sum(t => t.TotalUnitCost).ToString(),
                             LocalContribution = _ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.Id).Sum(t => t.CommunityContribution).ToString(),
                             GovtContribution = _ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.Id).Sum(t => t.OtherContribution).ToString(),
                             GrandTotal = (_ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.Id).Sum(t => t.TotalUnitCost) + _ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.Id).Sum(t => t.CommunityContribution) + _ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.Id).Sum(t => t.OtherContribution)).ToString(),
                             Units = _ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.Id).FirstOrDefault().TotalUnits.ToString(),


                         }).ToList();



            return temp1;

            //throw new NotImplementedException();
        }
        public async Task<FileDto> CreateExcelRegionWiseSummary(string fileName, string year)
        {
            var result = GetAllRegionSummary(year);

            var results = from line in result
                          group line by new
                          {
                              line.RegionName,

                          } into g
                          select new precostestimationoverallregionsummuery
                          {
                              CommunityConstribution = g.Sum(pc => pc.CommunityConstribution),
                              FunderConstribution = g.Sum(pc => pc.FunderConstribution),
                              OtherConstribution = g.Sum(pc => pc.OtherConstribution),
                              GrantTotal = g.Sum(pc => pc.GrantTotal),
                              RegionName = g.Key.RegionName,
                              TotalAll = g.Sum(pc => pc.TotalAll),

                          };

            string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";
            using (SpreadsheetDocument document = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))


            {

                WorkbookPart wbp = document.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();

                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";

                // var sImagePath = _appFolders.LogoImgFolder + "Reportlogo.png"; ;
                DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                BlipFill blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                Bitmap bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                ShapeProperties sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                Position pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                Extent ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                AbsoluteAnchor anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                WorksheetDrawing wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);

                Row row9 = new Row();
                Row row8 = new Row();
                Row row18 = new Row();





                Cell cell8 = InsertCellInWorksheet("D", 1, wsp);
                cell8.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                cell8.DataType = new EnumValue<CellValues>(CellValues.Number);


                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cell8.Append(inlineString);

                Cell cell81 = InsertCellInWorksheet("D", 2, wsp);
                cell81.CellValue = new CellValue("Regionwise-programme wise sanction details");
                cell81.DataType = new EnumValue<CellValues>(CellValues.Number);

               
                Run run2 = new Run();
                run2.Append(new Text("Regionwise-programme wise sanction details"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cell81.Append(inlineString1);

                

                Cell cellString811 = InsertCellInWorksheet("D", 3, wsp);
                cellString811.CellValue = new CellValue("Year: "+year);
                cellString811.DataType = new EnumValue<CellValues>(CellValues.String);

                var prevName = "";

                row9.Append(

                ConstructCell("", CellValues.String));
                sd.AppendChild(row9);
                var GrantTotal = 0.00;
                var communitycontribution = 0.00;
                var fundercontribution = 0.00;
                var othercontribution = 0.00;
                foreach (var acti in results)

                {


                    Row row56 = new Row();



                    Row row81 = new Row();
                    Row row82 = new Row();
                    Row row83 = new Row();

                    Row row84 = new Row();


                    if (prevName != acti.RegionName)
                    {
                        prevName = acti.RegionName;

                        row83.Append(


                           ConstructCell(" ", CellValues.String, 1));
                        sd.AppendChild(row83);
                        row81.Append(


                           ConstructCell(acti.RegionName.ToString(), CellValues.String, 1));
                        sd.AppendChild(row81);

                        row84.Append(


                           ConstructCell(" ", CellValues.String, 1));
                        sd.AppendChild(row84);
                        row18 = new Row();

                        row56 = new Row();
                        row56.Append(


                   ConstructCell("Program Name ", CellValues.String, 2),
                   ConstructCell("Project Cost(Grant) ", CellValues.String, 2),
                   ConstructCell("Project Cost(CBOs) ", CellValues.Number, 2),
                   ConstructCell("Local Contribution", CellValues.Number, 2),
                   ConstructCell("Gov. Contribution", CellValues.Number, 2),
                   ConstructCell("Total", CellValues.Number, 2));
                        sd.AppendChild(row56);

                        foreach (var item in result)
                        {
                            if (item.RegionName == acti.RegionName)
                            {
                                row8 = new Row();

                                row8.Append(


                                          ConstructCell(item.programName.ToString(), CellValues.Number, 1),
                                          ConstructCell(Convert.ToDecimal(item.GrantTotal).ToString("#,##0.00"), CellValues.Number, 1),
                                         ConstructCell(Convert.ToDecimal(item.FunderConstribution).ToString("#,##0.00"), CellValues.Number, 1),
                                         ConstructCell(Convert.ToDecimal(item.CommunityConstribution).ToString("#,##0.00"), CellValues.Number, 1),
                                         ConstructCell(Convert.ToDecimal(item.OtherConstribution).ToString("#,##0.00"), CellValues.Number, 1),
                                         ConstructCell(Convert.ToDecimal(item.TotalAll).ToString("#,##0.00"), CellValues.Number, 1));
                                sd.AppendChild(row8);
                            }

                        }
                        row18 = new Row();

                        row18.Append(
                              ConstructCell("Total", CellValues.String, 2),

                               ConstructCell(Convert.ToDecimal(acti.GrantTotal).ToString("#,##0.00"), CellValues.Number, 2),

                                 ConstructCell(Convert.ToDecimal(acti.CommunityConstribution).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(acti.FunderConstribution).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(acti.OtherConstribution).ToString("#,##0.00"), CellValues.Number, 2),
                                ConstructCell(Convert.ToDecimal(acti.TotalAll).ToString("#,##0.00"), CellValues.Number, 2)

                                 );
                        sd.Append(row18);



                    }
                    else
                         if (acti.RegionName == prevName)
                    {
                        foreach (var item in result)
                        {
                            if (item.RegionName == acti.RegionName)
                            {
                                row8 = new Row();

                                row8.Append(


                                        ConstructCell(item.programName.ToString(), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(item.GrantTotal).ToString("#,##0.00"), CellValues.Number, 1),
                                       ConstructCell(Convert.ToDecimal(item.CommunityConstribution).ToString("#,##0.00"), CellValues.Number, 1),
                                       ConstructCell(Convert.ToDecimal(item.FunderConstribution).ToString("#,##0.00"), CellValues.Number, 1),
                                       ConstructCell(Convert.ToDecimal(item.OtherConstribution).ToString("#,##0.00"), CellValues.Number, 1),
                                       ConstructCell(Convert.ToDecimal(item.TotalAll).ToString("#,##0.00"), CellValues.Number, 1));
                                sd.AppendChild(row8);
                            }

                        }
                        row18 = new Row();
                        row18.Append(
                               ConstructCell("Total", CellValues.String, 2),


                               ConstructCell(Convert.ToDecimal(acti.GrantTotal).ToString("#,##0.00"), CellValues.Number, 2),

                                  ConstructCell(Convert.ToDecimal(acti.CommunityConstribution).ToString("#,##0.00"), CellValues.Number, 2),
                                  ConstructCell(Convert.ToDecimal(acti.FunderConstribution).ToString("#,##0.00"), CellValues.Number, 2),
                                  ConstructCell(Convert.ToDecimal(acti.OtherConstribution).ToString("#,##0.00"), CellValues.Number, 2),
                                  ConstructCell(Convert.ToDecimal(acti.TotalAll).ToString("#,##0.00"), CellValues.Number, 2)
                                  );
                        sd.Append(row18);

                    }


                }

                sheets.Append(sheet);
                wb.Append(sheets);
                document.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();



            }


            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;




        }

        //archita
        public List<ProgramInformationListDto> GetOverallAnnualAction()
        {

            var temp1 = (from p in _actionAreaAppRepository.GetAll()

                         join Pr in _PrgActionAreaActivityMappingRepository.GetAll() on p.Id equals Pr.ActionAreaID

                         //join prog in _programRepository.GetAll() on Pr.ProgramID equals prog.Id


                         join progcost in _ProgramCostEstimationOverAll.GetAll() on Pr.ProgramID equals progcost.ProgramID



                         group Pr by

                            new
                            {
                                Pr.ActionAreaID,
                                Pr.ProgramID,
                                p.Name

                            } into g

                         select new ProgramInformationListDto()
                         {


                             ActionAreaName = g.Key.Name.ToString(),
                             ProjectCostGrant = _programRepository.GetAll().Where(p => p.Id == g.Key.ProgramID).Sum(t => t.GantnAmountSanctionRupess).ToString(),
                             ProjectCostCBO = _ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.ProgramID).Sum(t => t.TotalUnitCost).ToString(),
                             GovtContribution = _ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.ProgramID).Sum(t => t.OtherContribution).ToString(),
                             LocalContribution = _ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.ProgramID).Sum(t => t.CommunityContribution).ToString(),
                             GrandTotal = (_ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.ProgramID).Sum(t => t.TotalUnitCost) + _ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.ProgramID).Sum(t => t.CommunityContribution) + _ProgramCostEstimationOverAll.GetAll().Where(p => p.Id == g.Key.ProgramID).Sum(t => t.OtherContribution)).ToString(),
                             // ProgramSubmitDate= _programRepository.GetAll().Where(p => p.Id == g.Key.ProgramID).pr,
                         }).ToList();



            return temp1;

            //throw new NotImplementedException();
        }
        public async Task<FileDto> CreateExcelOverAllAnnualAction(string fileName)
        {
            var result = GetOverallAnnualAction();
            var tempres = GetOverallAnnualAction();
            List<ProgramInformationListDto> programInformation = new List<ProgramInformationListDto>();
            foreach (var item in result)
            {
                var indexs = tempres.FindAll(xx => xx.ActionAreaName == item.ActionAreaName);

                if (tempres.FindAll(x => x.ActionAreaName == item.ActionAreaName).Count > 0)
                {
                    var resultofprogram = DoGrandTotal(indexs);

                    tempres.RemoveAll(xx => xx.ActionAreaName == item.ActionAreaName);
                    programInformation.Add(resultofprogram);
                }
            }

            string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";
            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))


            {
                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();


                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                BlipFill blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                Bitmap bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                ShapeProperties sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                Position pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                Extent ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                AbsoluteAnchor anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                WorksheetDrawing wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);

                Row row9 = new Row();
                Row row8 = new Row();
                Row row177 = new Row() { RowIndex = 1U };

                //create a new inline string cell
                Cell cell = new Cell() { CellReference = "D1" };
                cell.DataType = CellValues.InlineString;

                //create a run for the bold text
                Run run177 = new Run();
                run177.Append(new Text("Watershed Organisation Trust (WOTR)"));
                //create runproperties and append a "Bold" to them
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());

                FontSize fontSize1 = new FontSize() { Val = 22 };
                run1Properties.Append(fontSize1);

                //set the first runs RunProperties to the RunProperties containing the bold
                run177.RunProperties = run1Properties;

                //create a second run for the non-bod text
                Run run2 = new Run();
                run2.Append(new Text(Environment.NewLine + " ") { Space = SpaceProcessingModeValues.Preserve });

                //create a new inline string and append both runs
                InlineString inlineString = new InlineString();
                inlineString.Append(run177);
                inlineString.Append(run2);


                //append the inlineString to the cell.
                cell.Append(inlineString);

                //append the cell to the row
                row177.Append(cell);

                sd.Append(row177);
                Cell cellString8 = InsertCellInWorksheet("D", 2, wsp);

                cellString8.CellValue = new CellValue("Overall Annual Action Plan and Budget");
                cellString8.DataType = new EnumValue<CellValues>(CellValues.String);
                Run run3 = new Run();
                run3.Append(new Text("Overall Annual Action Plan and Budget"));
                RunProperties run3Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run3Properties.Append(new Bold());
                run3Properties.Append(color3);
                run3Properties.Append(fontSize3);
                run3.RunProperties = run3Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cellString8.Append(inlineString1);

                row9.Append(

                 ConstructCell("", CellValues.String));
                sd.AppendChild(row9);
                row8.Append(

                     ConstructCell("Sr.no ", CellValues.String, 2),
                    ConstructCell("Action Area", CellValues.String, 2),
                    ConstructCell("Project Cost(Grant)", CellValues.String, 2),
                    ConstructCell("Project Cost (CBOs)", CellValues.Number, 2),
                    ConstructCell("Local Contribution", CellValues.Number, 2),
                    ConstructCell("Govt. Contribution", CellValues.Number, 2),
                    ConstructCell("Total", CellValues.Number, 2));

                sd.AppendChild(row8);

                int cnt = 1;

                foreach (var acti in programInformation)
                {
                    row8 = new Row();

                    row8.Append(

                      ConstructCell(cnt++.ToString(), CellValues.Number, 1),
                            ConstructCell(acti.ActionAreaName.ToString(), CellValues.Number, 1),
                            ConstructCell(acti.ProjectCostGrant.ToString(), CellValues.Number, 1),
                           ConstructCell(acti.ProjectCostCBO.ToString(), CellValues.Number, 1),
                           ConstructCell(acti.LocalContribution.ToString(), CellValues.Number, 1),
                           ConstructCell(acti.GovtContribution.ToString(), CellValues.Number, 1),
                           ConstructCell(acti.GrandTotal.ToString(), CellValues.Number, 1));









                    sd.AppendChild(row8);

                }


                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();



            }


            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;




        }

        public ProgramInformationListDto DoGrandTotal(List<ProgramInformationListDto> result)
        {


            ProgramInformationListDto programInformationListDto = new ProgramInformationListDto();

            result.ForEach(x =>
            {
                var sumtemp = 0.00;
                var ProjectCostGrant = 0.00;
                var ProjectCostCBO = 0.00;
                var GovtContribution = 0.00;
                var LocalContribution = 0.00;
                programInformationListDto.ActionAreaName = x.ActionAreaName;
                if (programInformationListDto.GrandTotal == null)
                {
                    sumtemp = 0;
                }
                else
                {
                    sumtemp = Convert.ToDouble(programInformationListDto.GrandTotal) + Convert.ToDouble(x.GrandTotal);
                }


                programInformationListDto.GrandTotal = sumtemp.ToString();

                if (programInformationListDto.ProjectCostGrant == null)
                {
                    ProjectCostGrant = 0;
                }
                else
                {
                    ProjectCostGrant = Convert.ToDouble(programInformationListDto.ProjectCostGrant) + Convert.ToDouble(x.ProjectCostGrant);
                }


                programInformationListDto.ProjectCostGrant = ProjectCostGrant.ToString();


                if (programInformationListDto.ProjectCostCBO == null)
                {
                    ProjectCostCBO = 0;
                }
                else
                {
                    ProjectCostCBO = Convert.ToDouble(programInformationListDto.ProjectCostCBO) + Convert.ToDouble(x.ProjectCostCBO);
                }


                programInformationListDto.ProjectCostCBO = ProjectCostCBO.ToString();

                if (programInformationListDto.GovtContribution == null)
                {
                    GovtContribution = 0;
                }
                else
                {
                    GovtContribution = Convert.ToDouble(programInformationListDto.GovtContribution) + Convert.ToDouble(x.GovtContribution);
                }


                programInformationListDto.GovtContribution = GovtContribution.ToString();


                if (programInformationListDto.LocalContribution == null)
                {
                    LocalContribution = 0;
                }
                else
                {
                    LocalContribution = Convert.ToDouble(programInformationListDto.LocalContribution) + Convert.ToDouble(x.LocalContribution);
                }


                programInformationListDto.LocalContribution = LocalContribution.ToString();


            });

            return programInformationListDto;
        }
        public async Task<FileDto> CreateExcelClusterWise(string fileName, int ProgramId, string year)
        {
            var result = GetClusterWiseReport(ProgramId, year);
            var results = from line in result
                          group line by new
                          {
                              line.ClustorName,
                              line.VillageName
                          } into g
                          select new clustorwise
                          {
                              TotalUnits = g.Sum(pc => pc.TotalUnits),
                              TotalUnitCost = g.Sum(pc => pc.TotalUnitCost),
                              UnitCost = g.Sum(pc => pc.UnitCost),
                              CommunityContribution = g.Sum(pc => pc.CommunityContribution),
                              FunderContribution = g.Sum(pc => pc.FunderContribution),
                              OtherContribution = g.Sum(pc => pc.OtherContribution),

                              ClustorName = g.Key.ClustorName,
                              VillageName = g.Key.VillageName,
                          };

            string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";
            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))


            {

                //WorkbookPart wbp = xl.AddWorkbookPart();
                //WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                //Workbook wb = new Workbook();
                //FileVersion fv = new FileVersion();
                //fv.ApplicationName = "Microsoft Office Excel";
                //Worksheet ws = new Worksheet();
                //SheetData sd = new SheetData();


                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();

                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                BlipFill blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                Bitmap bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                ShapeProperties sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                Position pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                Extent ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                AbsoluteAnchor anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                WorksheetDrawing wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);

                Row row9 = new Row();
                Row row8 = new Row();

                Row row13 = new Row();

                //Cell cellString81 = InsertCellInWorksheet("E", 2, wsp);

                //cellString81.CellValue = new CellValue("Clusterwise Program Report");
                //cellString81.DataType = new EnumValue<CellValues>(CellValues.String);



                Cell cell451 = InsertCellInWorksheet("D", 1, wsp);
                cell451.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                cell451.DataType = new EnumValue<CellValues>(CellValues.Number);

                Cell cellString22 = InsertCellInWorksheet("D", 1, wsp);
                cellString22.CellValue = new CellValue("Watershed Organisation Trust(WOTR)");
                cellString22.DataType = new EnumValue<CellValues>(CellValues.String);
                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cell451.Append(inlineString);


                Cell cellString811 = InsertCellInWorksheet("D", 2, wsp);
                cellString811.CellValue = new CellValue("Clusterwise Program Report");
                cellString811.DataType = new EnumValue<CellValues>(CellValues.String);
                Run run2 = new Run();
                run2.Append(new Text("Clusterwise Program Report"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cellString811.Append(inlineString1);

                //Cell cell0 = InsertCellInWorksheet("A", 2, wsp);
                //cell0.CellValue = new CellValue(" ");
                //cell0.DataType = new EnumValue<CellValues>(CellValues.Number);


                Cell cell45 = InsertCellInWorksheet("D", 4, wsp);
                cell45.CellValue = new CellValue("Program Title");
                cell45.DataType = new EnumValue<CellValues>(CellValues.Number);

                Cell cellString = InsertCellInWorksheet("D", 4, wsp);
                cellString.CellValue = new CellValue("Program Title: " + result[0].programName.ToString());
                cellString.DataType = new EnumValue<CellValues>(CellValues.String);





                Cell cell2 = InsertCellInWorksheet("D", 6, wsp);
                cell2.CellValue = new CellValue("Year");
                cell2.DataType = new EnumValue<CellValues>(CellValues.Number);

                Cell cellString2 = InsertCellInWorksheet("D", 6, wsp);
                cellString2.CellValue = new CellValue("Year: "+ year.ToString());
                cellString2.DataType = new EnumValue<CellValues>(CellValues.Number);



                Row row2 = new Row();
                row2.Append(

                   );
                //ConstructCell("Funder Contribution", CellValues.Number, 2),
                //ConstructCell("Community Contribution", CellValues.Number, 2));



                sd.AppendChild(row2);



                row8.Append(

                    ConstructCell("Clustor Name", CellValues.String, 2),
                    ConstructCell("Village Name", CellValues.String, 2),
                    ConstructCell("Activity", CellValues.String, 2),
                    ConstructCell("Unit", CellValues.Number, 2),
                    ConstructCell("Total Unit", CellValues.Number, 2),
                    ConstructCell("Cost Per Unit", CellValues.Number, 2),
                ConstructCell("Community Contribution", CellValues.Number, 2),
                 ConstructCell("Funder Contribution", CellValues.Number, 2),
                  ConstructCell("Other Contribution", CellValues.Number, 2),
                    ConstructCell("Total", CellValues.Number, 2)

                );



                sd.AppendChild(row8);



                var prevName = "";
                var villageName = "";
                var activityName = "";
                var TotalUnits = 0.00;

                Row row18 = new Row();

                foreach (var item in results)
                {

                    if (prevName != item.ClustorName)
                    {


                        prevName = item.ClustorName;
                        villageName = item.VillageName;
                        var activities = result.ToList().Where(aa => aa.ClustorName == item.ClustorName && aa.VillageName == item.VillageName).ToList();

                        foreach (var items in activities)
                        {
                            

                                row13 = new Row();

                                row13.Append(



                                   ConstructCell(item.ClustorName.ToString(), CellValues.String, 1),
                                   ConstructCell(item.VillageName.ToString(), CellValues.String, 1),
                                   ConstructCell(items.ActivityName.ToString(), CellValues.String, 1),

                                   ConstructCell(items.TotalUnits.ToString(), CellValues.Number, 1),
                                   ConstructCell(items.TotalUnitCost.ToString(), CellValues.Number, 1),
                                   ConstructCell(items.UnitCost.ToString(), CellValues.Number, 1),
                                  ConstructCell(items.CommunityContribution.ToString(), CellValues.Number, 1),
                                   ConstructCell(items.FunderContribution.ToString(), CellValues.Number, 1),
                                    ConstructCell(items.OtherContribution.ToString(), CellValues.Number, 1),
                                    ConstructCell(items.Total.ToString(), CellValues.Number, 1)
                                  );
                            item.ClustorName = "";
                            item.VillageName = "";
                            sd.AppendChild(row13);

                           
                        }
                        row18 = new Row();
                        row18.Append(
                            ConstructCell(" Total ", CellValues.String, 1),
                            ConstructCell(" ", CellValues.String, 1),
                            ConstructCell(" ", CellValues.String, 1),
                                 ConstructCell(item.TotalUnits.ToString(), CellValues.Number, 1),
                                   ConstructCell(item.TotalUnitCost.ToString(), CellValues.Number, 1),
                                   ConstructCell(item.UnitCost.ToString(), CellValues.Number, 1),
                                  ConstructCell(item.CommunityContribution.ToString(), CellValues.Number, 1),
                                   ConstructCell(item.FunderContribution.ToString(), CellValues.Number, 1),
                                    ConstructCell(item.OtherContribution.ToString(), CellValues.Number, 1),
                                    ConstructCell(item.Total.ToString(), CellValues.Number, 1)
                                    
                            );
                        sd.AppendChild(row18);



                    }
                    else
                    {
                        if (item.ClustorName == prevName)
                        {
                            if (villageName == item.VillageName)
                            {
                                villageName = item.VillageName;
                                var activities = result.ToList().Where(aa => aa.ClustorName == item.ClustorName && aa.VillageName == item.VillageName).ToList();

                                foreach (var items in activities)
                                {
                                    
                                        row13 = new Row();

                                        row13.Append(



                                           ConstructCell(item.ClustorName.ToString(), CellValues.String, 1),
                                           ConstructCell(item.VillageName.ToString(), CellValues.String, 1),
                                           ConstructCell(items.ActivityName.ToString(), CellValues.String, 1),

                                           ConstructCell(items.TotalUnits.ToString(), CellValues.Number, 1),
                                           ConstructCell(items.TotalUnitCost.ToString(), CellValues.Number, 1),
                                           ConstructCell(items.UnitCost.ToString(), CellValues.Number, 1),
                                          ConstructCell(items.CommunityContribution.ToString(), CellValues.Number, 1),
                                           ConstructCell(items.FunderContribution.ToString(), CellValues.Number, 1),
                                            ConstructCell(items.OtherContribution.ToString(), CellValues.Number, 1),
                                            ConstructCell(items.Total.ToString(), CellValues.Number, 1)
                                          );
                                        item.ClustorName = "";
                                        item.VillageName = "";
                                        sd.AppendChild(row13);

                                   
                                }
                                row18 = new Row();

                                row18.Append(
                                    ConstructCell(" Total ", CellValues.String, 1),
                                    ConstructCell(" ", CellValues.String, 1),
                                    ConstructCell(" ", CellValues.String, 1),
                                         ConstructCell(item.TotalUnits.ToString(), CellValues.Number, 1),
                                           ConstructCell(item.TotalUnitCost.ToString(), CellValues.Number, 1),
                                           ConstructCell(item.UnitCost.ToString(), CellValues.Number, 1),
                                          ConstructCell(item.CommunityContribution.ToString(), CellValues.Number, 1),
                                           ConstructCell(item.FunderContribution.ToString(), CellValues.Number, 1),
                                            ConstructCell(item.OtherContribution.ToString(), CellValues.Number, 1),
                                            ConstructCell(item.Total.ToString(), CellValues.Number, 1)

                                    );
                                sd.AppendChild(row18);
                            }
                            if (villageName != item.VillageName)
                            {
                                villageName = item.VillageName;
                                var activities = result.ToList().Where(aa => aa.ClustorName == item.ClustorName && aa.VillageName == item.VillageName).ToList();

                                foreach (var items in activities)
                                {
                                     
                                    
                                        row13 = new Row();
                                        row13.Append(



                                           ConstructCell(item.ClustorName.ToString(), CellValues.String, 1),
                                           ConstructCell(item.VillageName.ToString(), CellValues.String, 1),
                                           ConstructCell(items.ActivityName.ToString(), CellValues.String, 1),

                                           ConstructCell(items.TotalUnits.ToString(), CellValues.Number, 1),
                                           ConstructCell(items.TotalUnitCost.ToString(), CellValues.Number, 1),
                                           ConstructCell(items.UnitCost.ToString(), CellValues.Number, 1),
                                          ConstructCell(items.CommunityContribution.ToString(), CellValues.Number, 1),
                                           ConstructCell(items.FunderContribution.ToString(), CellValues.Number, 1),
                                            ConstructCell(items.OtherContribution.ToString(), CellValues.Number, 1),
                                            ConstructCell(items.Total.ToString(), CellValues.Number, 1)
                                          );
                                    item.ClustorName = "";
                                    item.VillageName = "";
                                        sd.AppendChild(row13);

                                   
                                }
                                row18 = new Row();
                                row18.Append(
                                    ConstructCell(" Total ", CellValues.String, 1),
                                    ConstructCell(" ", CellValues.String, 1),
                                    ConstructCell(" ", CellValues.String, 1),
                                         ConstructCell(item.TotalUnits.ToString(), CellValues.Number, 1),
                                           ConstructCell(item.TotalUnitCost.ToString(), CellValues.Number, 1),
                                           ConstructCell(item.UnitCost.ToString(), CellValues.Number, 1),
                                          ConstructCell(item.CommunityContribution.ToString(), CellValues.Number, 1),
                                           ConstructCell(item.FunderContribution.ToString(), CellValues.Number, 1),
                                            ConstructCell(item.OtherContribution.ToString(), CellValues.Number, 1),
                                            ConstructCell(item.Total.ToString(), CellValues.Number, 1)

                                    );
                                sd.AppendChild(row18);
                            }
                        }
                    }

                }

                sheets.Append(sheet);
                wb.Append(sheets);
                //AddImage(worksheetPart, imagePath1, "My first image", 1, 1); // A1
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();
            }


            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;

        }
        //archita

        public List<clustorwise> GetClusterWiseReport(int ProgramId, string year)
        {


            try
            {
                string value;
                var allChartData = new clustorwise();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramYearwiseStateWiseBudgets = new List<clustorwise>();
                var regionwisesummuryreport = new List<clustorwise>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    ConnectionConnect(conn);
                    using (var cmdTwo = new SqlCommand("", conn))
                    {
                        //ProjectWiseInfoLineChart
                        var dataTable = new System.Data.DataTable();
                        cmdTwo.CommandTimeout = 0;
                        cmdTwo.CommandText = "[dbo].[SP_clustorwisedetails]";
                        cmdTwo.CommandType = CommandType.StoredProcedure;
                        cmdTwo.Parameters.Add(new SqlParameter("@CostEstimationYear", year));
                        cmdTwo.Parameters.Add(new SqlParameter("@ProgramId", ProgramId));


                        SqlDataReader dr = cmdTwo.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerProgramYearwiseStateWiseBudgets.Add(new clustorwise
                                    {
                                        programName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),
                                        ClustorName = (row["LocationName"] == DBNull.Value) ? "" : Convert.ToString(row["LocationName"]),
                                        ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),
                                        villageId = (row["VillageId"] == DBNull.Value) ? 00 : Convert.ToInt32(row["VillageId"]),
                                        TotalUnits = (row["TotalUnits"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["TotalUnits"]),
                                        TotalUnitCost = (row["TotalUnitCost"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["TotalUnitCost"]),
                                        UnitCost = (row["UnitCost"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["UnitCost"]),
                                        CommunityContribution = (row["CommunityContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["CommunityContribution"]),
                                        FunderContribution = (row["FunderContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["FunderContribution"]),
                                        OtherContribution = (row["OtherContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["OtherContribution"]),
                                        Total = (row["Total"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Total"]),


                                    });
                                }

                            }
                        }
                        conn.Close();
                    }


                }

                foreach (var item in programManagerProgramYearwiseStateWiseBudgets)
                {
                    var villageName = item.villageId != 0 ? _VillageRepository.GetAll().Where(ss => ss.Id == item.villageId).FirstOrDefault().Name : "";



                    regionwisesummuryreport.Add(new clustorwise
                    {
                        programName = item.programName,
                        ClustorName = item.ClustorName,
                        VillageName = villageName,
                        TotalUnits = item.TotalUnits,
                        TotalUnitCost = item.TotalUnitCost,
                        UnitCost = item.UnitCost,
                        CommunityContribution = item.CommunityContribution,
                        FunderContribution = item.FunderContribution,
                        OtherContribution = item.OtherContribution,
                        Total = item.Total,
                        ActivityName = item.ActivityName

                    });
                }


                return regionwisesummuryreport;
            }
            catch (Exception ex)
            {

                throw;
            }




            //throw new NotImplementedException();
        }

        public List<ProgramInformationListDto> GetProgramDetailsbyIDforRegion(int ProgramId)
        {

            var query = (from p in _programRepository.GetAll()
                         where (p.Id == ProgramId)
                         join d in _villageClusterRepository.GetAll() on p.Id equals d.ProgramID
                         where (p.Id == ProgramId)
                         join l in _LocationRepository.GetAll() on d.RRCID equals l.Id
                         select new ProgramInformationListDto()
                         {

                             ProgramName = p.Name,
                             RegionName = l.Name
                             //ShortDescription = p.ShortDescription,
                             //LongDescription = p.LongDescription,
                             //DonorID = p.DonorID,
                             //ProgramSanctionedYear = p.ProgramSanctionedYear,
                             //ProgramStartDate = p.ProgramStartDate,
                             //PrgramEndDate = p.PrgramEndDate,
                             //DonorName = d.CompanyName,
                             //GantnAmountSanctionRupess = p.GantnAmountSanctionRupess

                         }).ToList();

            return query;

        }
        public List<RequestAAPlasListDto> GetDetailsforQuarterWise(int quarterId)
        {

            var query = (from p in _ProgramQuqterUnitMappingRepository.GetAll()
                         where (p.QuarterId == quarterId)

                         select new RequestAAPlasListDto()
                         {

                             ProgramID = p.ProgramID,
                             QuaterWise = (from q in _RequestAAplasRepository.GetAll()
                                           where (q.ProgramID == p.ProgramID)

                                           select new ReportQuarterwise()
                                           {
                                               Unit = q.TotalUnits.ToString(),
                                               CommunityContribution = q.CommunityContribution.ToString(),
                                               FunderContribution = q.FunderContribution.ToString()

                                           }).ToList()

                         }).ToList();

            return query;

        }

        public List<ProgDto> GetAllProgramsWithUserId(int userId)
        {
            var prog_list = _programRepository.GetAll().Where(p => p.CreatorUserId == userId).ToList();

            return new List<ProgDto>(ObjectMapper.Map<List<ProgDto>>(prog_list));
        }

        public async Task<PagedResultDto<ProgDtoList>> GetAllProgramsWithFilter(GetAllProgramsInput input)
        {
            try
            {
                var queryTotal = _programRepository.GetAll()
                .Where(r => r.IsDeleted == false)
                 .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u => u.Name.Contains(input.Filter)
                ).OrderByDescending(c => c.LastModificationTime).ToList();

                var query = _programRepository.GetAll()
                .Where(r => r.IsDeleted == false)
                 .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    u => u.Name.Contains(input.Filter)
                ).OrderByDescending(c => c.LastModificationTime).Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

                var progs = query.OrderByDescending(c => c.LastModificationTime).ToList();


                var progsCount = queryTotal.Count();

                var progListDtos = ObjectMapper.Map<List<ProgDtoList>>(progs);

                return new PagedResultDto<ProgDtoList>(
                    progsCount,
                    progListDtos);
            }
            catch (Exception)
            {
                return null;
            }
        }



        public async Task DeleteProgram(int input)
        {
            await _programRepository.DeleteAsync(input);
        }

        public List<string> Allyear()
        {
            List<string> list = (from cost in _PrgCostEstRepository.GetAll()
                                 select cost.CostEstimationYear.ToString()).Distinct().OrderBy(x=> x).ToList();
            return list.OrderBy(xx => xx).ToList();
        }

        public async Task<FileDto> CreateExcelRegiondetailsWisWise(string fileName, string rrcId, string year)
        {
            var result = GetAllProgramsDetailsRegionDetailsWise(rrcId, year);
            var results = from line in result
                          group line by new
                          {
                              line.ProgramName,
                              line.CompanyName,
                              line.SubactionAreaName,
                              line.ActionAreaName,
                          } into g
                          select new RegionWiseDetailsList
                          {
                              ProjectCostCBO = g.Sum(pc => pc.ProjectCostCBO),
                              LocalContribution = g.Sum(pc => pc.LocalContribution),
                              GovtContribution = g.Sum(pc => pc.GovtContribution),
                              GrandTotal = g.Sum(pc => pc.GrandTotal),
                              SubactionAreaName = g.Key.SubactionAreaName,
                              ProgramName = g.Key.ProgramName,
                              ActionAreaName = g.Key.ActionAreaName,
                              CompanyName = g.Key.CompanyName,
                          };
            var ListOfUsers = result.ToList().GroupBy(x => x.ProgramName).ToList();



            string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";

            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))


            {

                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();


                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                BlipFill blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                Bitmap bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                ShapeProperties sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                Position pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                Extent ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                AbsoluteAnchor anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                WorksheetDrawing wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                // sheets.Append(sheet);
                wb.Append(fv);
                // wb.Append(sheets);


                // xl.WorkbookPart.Workbook.Save();
                // xl.Close();

                //archita

                Row row9 = new Row();
                Row row8 = new Row();
                Row row10 = new Row();
                Row row11 = new Row();
                Row row12 = new Row();
                Row row13 = new Row();
                Row row14 = new Row();
                Row row99 = new Row();


                Cell cell8 = InsertCellInWorksheet("D", 1, wsp);
                cell8.CellValue = new CellValue("Watershed Organisation Trust(WOTR)");
                cell8.DataType = new EnumValue<CellValues>(CellValues.Number);
                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cell8.Append(inlineString);

                Cell cellString8 = InsertCellInWorksheet("D", 2, wsp);
                cellString8.CellValue = new CellValue("Regionwise Overall Annual Action Plan");
                cellString8.DataType = new EnumValue<CellValues>(CellValues.String);
                cell8.StyleIndex = 1;
                Run run2 = new Run();
                run2.Append(new Text("Regionwise Overall Annual Action Plan"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cellString8.Append(inlineString1);

                Cell cell = InsertCellInWorksheet("D", 3, wsp);
                cell.CellValue = new CellValue("Year: " + year);
                cell.DataType = new EnumValue<CellValues>(CellValues.Number);

                Cell cellString = InsertCellInWorksheet("D", 3, wsp);
                cellString.CellValue = new CellValue("Year: " + year);
                cellString.DataType = new EnumValue<CellValues>(CellValues.String);



                row9.Append(

                 ConstructCell("", CellValues.String));
                sd.AppendChild(row9);
                row10.Append(

                 ConstructCell("", CellValues.String));
                sd.AppendChild(row10);
                row11.Append(

                 ConstructCell("", CellValues.String));
                sd.AppendChild(row11);


                var prevName = "";
                var ActionAreaName = "";
                var SubActionAreaName = "";
                var TotalUnitCost = 0.00;



                //foreach (var acti in result)
                //{
                //    var aaa = results.ToList().Where(xx => xx.ProgramName == acti.ProgramName).ToList();

                //    TotalUnitCost += Convert.ToInt64(acti.ProjectCostCBO);
                //    var aaaa = results.ToList().Where(xx => xx.ProgramName == acti.ProgramName && xx.SubactionAreaName == acti.SubactionAreaName).ToList();

                //    if (prevName != acti.ProgramName)
                //    {

                //        prevName = acti.ProgramName;
                //        ActionAreaName = acti.ActionAreaName;
                //        SubActionAreaName = acti.SubactionAreaName;

                //        row13 = new Row();

                //        row13.Append(


                //               ConstructCell(" ", CellValues.String, 1)
                //               );
                //        sd.AppendChild(row13);
                //        row14 = new Row();



                //        row12 = new Row();

                //        row12.Append(


                //               ConstructCell(" ProgramName: " + acti.ProgramName.ToString(), CellValues.String, 1)
                //               );
                //        sd.AppendChild(row12);
                //        row14.Append(


                //               ConstructCell(" ", CellValues.String, 1)
                //               );
                //        sd.AppendChild(row14);
                //        row9 = new Row();


                //        row9.Append(


                //            ConstructCell("Action Area ", CellValues.String, 2),
                //             ConstructCell("Sub Action Area ", CellValues.String, 2),
                //            ConstructCell("ActivityName ", CellValues.String, 2),
                //            ConstructCell("Project Cost (Grant) ", CellValues.String, 2),
                //            ConstructCell("Project Cost (CBOs)", CellValues.Number, 2),
                //            ConstructCell("Local Contribution", CellValues.Number, 2),
                //            ConstructCell("Govt. Contribution", CellValues.Number, 2),
                //            ConstructCell("Total", CellValues.Number, 2));

                //        sd.AppendChild(row9);
                //        row8 = new Row();

                //        row8.Append(


                //               ConstructCell(acti.ActionAreaName.ToString(), CellValues.String, 2),
                //               ConstructCell(acti.SubactionAreaName.ToString(), CellValues.String, 2),
                //            ConstructCell(acti.ActivityName.ToString(), CellValues.String, 2),
                //              ConstructCell(acti.ProjectCostGrant.ToString(), CellValues.Number, 1),
                //               ConstructCell(Convert.ToDecimal(acti.ProjectCostCBO).ToString("#,##0.00"), CellValues.Number, 1),
                //               ConstructCell(Convert.ToDecimal(acti.LocalContribution).ToString("#,##0.00"), CellValues.Number, 1),
                //               ConstructCell(Convert.ToDecimal(acti.GovtContribution).ToString("#,##0.00"), CellValues.Number, 1),
                //               ConstructCell(Convert.ToDecimal(acti.GrandTotal).ToString("#,##0.00"), CellValues.Number, 1));

                //    }
                //    else

                //        if (acti.ProgramName == prevName)
                //    {
                //        if (ActionAreaName != acti.ActionAreaName)
                //        {
                //            ActionAreaName = acti.ActionAreaName;
                //            SubActionAreaName = acti.SubactionAreaName;
                //            row8 = new Row();
                //            row8.Append(
                //                   ConstructCell(acti.ActionAreaName.ToString(), CellValues.String, 2),
                //                   ConstructCell(acti.SubactionAreaName.ToString(), CellValues.String, 2),
                //                ConstructCell(acti.ActivityName.ToString(), CellValues.String, 2),
                //                  ConstructCell(acti.ProjectCostGrant.ToString(), CellValues.Number, 1),
                //                   ConstructCell(Convert.ToDecimal(acti.ProjectCostCBO).ToString("#,##0.00"), CellValues.Number, 1),
                //                   ConstructCell(Convert.ToDecimal(acti.LocalContribution).ToString("#,##0.00"), CellValues.Number, 1),
                //                   ConstructCell(Convert.ToDecimal(acti.GovtContribution).ToString("#,##0.00"), CellValues.Number, 1),
                //                   ConstructCell(Convert.ToDecimal(acti.GrandTotal).ToString("#,##0.00"), CellValues.Number, 1));



                //        }
                //        else
                //         if (acti.ActionAreaName == ActionAreaName)
                //        {
                //            if (acti.SubactionAreaName == SubActionAreaName)
                //            {
                //                SubActionAreaName = acti.SubactionAreaName;

                //                row8 = new Row();

                //                row8.Append(


                //                       ConstructCell(" ", CellValues.String, 2),
                //                       ConstructCell(" ", CellValues.String, 2),
                //                    ConstructCell(acti.ActivityName.ToString(), CellValues.String, 2),


                //                      ConstructCell(acti.ProjectCostGrant.ToString(), CellValues.Number, 1),
                //                       ConstructCell(Convert.ToDecimal(acti.ProjectCostCBO).ToString("#,##0.00"), CellValues.Number, 1),
                //                       ConstructCell(Convert.ToDecimal(acti.LocalContribution).ToString("#,##0.00"), CellValues.Number, 1),
                //                       ConstructCell(Convert.ToDecimal(acti.GovtContribution).ToString("#,##0.00"), CellValues.Number, 1),
                //                       ConstructCell(Convert.ToDecimal(acti.GrandTotal).ToString("#,##0.00"), CellValues.Number, 1));

                //            }
                //            else
                //            {
                //                SubActionAreaName = acti.SubactionAreaName;


                //                row8 = new Row();

                //                row8.Append(


                //                       ConstructCell(" ", CellValues.String, 2),
                //                       ConstructCell(acti.SubactionAreaName.ToString(), CellValues.String, 2),
                //                    ConstructCell(acti.ActivityName.ToString(), CellValues.String, 2),


                //                      ConstructCell(acti.ProjectCostGrant.ToString(), CellValues.Number, 1),
                //                       ConstructCell(Convert.ToDecimal(acti.ProjectCostCBO).ToString("#,##0.00"), CellValues.Number, 1),
                //                       ConstructCell(Convert.ToDecimal(acti.LocalContribution).ToString("#,##0.00"), CellValues.Number, 1),
                //                       ConstructCell(Convert.ToDecimal(acti.GovtContribution).ToString("#,##0.00"), CellValues.Number, 1),
                //                       ConstructCell(Convert.ToDecimal(acti.GrandTotal).ToString("#,##0.00"), CellValues.Number, 1));


                //            }




                //        }

                //    }


                //    sd.AppendChild(row8);
                //}

                foreach (var acti in results)
                {
                    var aaa = results.ToList().Where(xx => xx.ProgramName == acti.ProgramName).ToList();

                    TotalUnitCost += Convert.ToInt64(acti.ProjectCostCBO);
                    var aaaa = results.ToList().Where(xx => xx.ProgramName == acti.ProgramName && xx.SubactionAreaName == acti.SubactionAreaName).ToList();

                    if (prevName != acti.ProgramName)
                    {

                        prevName = acti.ProgramName;
                        ActionAreaName = acti.ActionAreaName;
                        SubActionAreaName = acti.SubactionAreaName;

                        row13 = new Row();

                        row13.Append(


                               ConstructCell(" ", CellValues.String, 1)
                               );
                        sd.AppendChild(row13);
                        row14 = new Row();



                        row12 = new Row();

                        row12.Append(


                               ConstructCell(" ProgramName: " + acti.ProgramName.ToString(), CellValues.String, 1)
                               );
                        sd.AppendChild(row12);
                        row14.Append(


                               ConstructCell("Company Name: " + acti.CompanyName.ToString(), CellValues.String, 1)
                               );
                        sd.AppendChild(row14);
                        row9 = new Row();


                        row9.Append(


                            ConstructCell("Action Area ", CellValues.String, 2),
                             ConstructCell("Sub Action Area ", CellValues.String, 2),
                            ConstructCell("ActivityName ", CellValues.String, 2),
                        ConstructCell("Project Cost (Grant) ", CellValues.String, 2),
                        ConstructCell("Project Cost (CBOs)", CellValues.Number, 2),
                        ConstructCell("Local Contribution", CellValues.Number, 2),
                        ConstructCell("Govt. Contribution", CellValues.Number, 2),
                        ConstructCell("Total", CellValues.Number, 2));

                        sd.AppendChild(row9);
                        row8 = new Row();

                        var activities = result.ToList().Where(aa => aa.ProgramName == acti.ProgramName && aa.SubactionAreaName == acti.SubactionAreaName).ToList();

                        //row8.Append(


                        //   ConstructCell(acti.ActionAreaName.ToString(), CellValues.String, 2),
                        //   ConstructCell(acti.SubactionAreaName.ToString(), CellValues.String, 2),
                        //ConstructCell("Act1".ToString(), CellValues.String, 2));
                        //  ConstructCell(acti.ProjectCostGrant.ToString(), CellValues.Number, 1),
                        //   ConstructCell(Convert.ToDecimal(acti.ProjectCostCBO).ToString("#,##0.00"), CellValues.Number, 1),
                        //   ConstructCell(Convert.ToDecimal(acti.LocalContribution).ToString("#,##0.00"), CellValues.Number, 1),
                        //   ConstructCell(Convert.ToDecimal(acti.GovtContribution).ToString("#,##0.00"), CellValues.Number, 1),
                        //   ConstructCell(Convert.ToDecimal(acti.GrandTotal).ToString("#,##0.00"), CellValues.Number, 1));
                        var SubactionAreaNames = acti.SubactionAreaName;
                        foreach (var item in activities)
                        {
                            row8 = new Row();

                            row8.Append(


                              ConstructCell(acti.ActionAreaName.ToString(), CellValues.String, 2),
                              ConstructCell(acti.SubactionAreaName.ToString(), CellValues.String, 2),
                           ConstructCell(item.ActivityName.ToString(), CellValues.String, 2),
                             ConstructCell(item.ProjectCostGrant.ToString(), CellValues.Number, 1),
                              ConstructCell(Convert.ToDecimal(item.ProjectCostCBO).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(item.LocalContribution).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(item.GovtContribution).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(item.GrandTotal).ToString("#,##0.00"), CellValues.Number, 1));
                            acti.ActionAreaName = "";
                            acti.SubactionAreaName = "";
                            sd.AppendChild(row8);
                        }



                        row99 = new Row();
                        row99.Append(ConstructCell("Total ", CellValues.String, 2),
                            ConstructCell("", CellValues.String, 2),
                            ConstructCell("", CellValues.String, 2),
                            ConstructCell("", CellValues.String, 2),
                            ConstructCell(acti.ProjectCostCBO.ToString(), CellValues.String, 2),
                            ConstructCell(acti.LocalContribution.ToString(), CellValues.String, 2),
                            ConstructCell(acti.GovtContribution.ToString(), CellValues.String, 2),
                            ConstructCell(acti.GrandTotal.ToString(), CellValues.String, 2));
                    }
                    else

                        if (acti.ProgramName == prevName)
                    {
                        if (ActionAreaName != acti.ActionAreaName)
                        {
                            ActionAreaName = acti.ActionAreaName;
                            SubActionAreaName = acti.SubactionAreaName;
                            row8 = new Row();
                            var activities = result.ToList().Where(aa => aa.ProgramName == acti.ProgramName && aa.SubactionAreaName == acti.SubactionAreaName).ToList();
                            var SubactionAreaNames = acti.SubactionAreaName;
                            foreach (var item in activities)
                            {
                                row8 = new Row();

                                row8.Append(


                                  ConstructCell(acti.ActionAreaName.ToString(), CellValues.String, 2),
                                  ConstructCell(acti.SubactionAreaName.ToString(), CellValues.String, 2),
                               ConstructCell(item.ActivityName.ToString(), CellValues.String, 2),
                               ConstructCell(item.ProjectCostGrant.ToString(), CellValues.Number, 1),
                              ConstructCell(Convert.ToDecimal(item.ProjectCostCBO).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(item.LocalContribution).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(item.GovtContribution).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(item.GrandTotal).ToString("#,##0.00"), CellValues.Number, 1));
                                acti.ActionAreaName = "";
                                acti.SubactionAreaName = "";
                                sd.AppendChild(row8);
                            }
                            row99 = new Row();

                            row99.Append(ConstructCell("Total ", CellValues.String, 2),
                                                        ConstructCell("", CellValues.String, 2),
                                                        ConstructCell("", CellValues.String, 2),
                                                        ConstructCell("", CellValues.String, 2),
                                                        ConstructCell(acti.ProjectCostCBO.ToString(), CellValues.String, 2),
                                                        ConstructCell(acti.LocalContribution.ToString(), CellValues.String, 2),
                                                        ConstructCell(acti.GovtContribution.ToString(), CellValues.String, 2),
                                                        ConstructCell(acti.GrandTotal.ToString(), CellValues.String, 2));
                        }
                        else
                         if (acti.ActionAreaName == ActionAreaName)
                        {
                            if (acti.SubactionAreaName == SubActionAreaName)
                            {
                                SubActionAreaName = acti.SubactionAreaName;

                                row8 = new Row();
                                var activities = result.ToList().Where(aa => aa.ProgramName == acti.ProgramName && aa.SubactionAreaName == acti.SubactionAreaName).ToList();
                                var SubactionAreaNames = acti.SubactionAreaName;

                                foreach (var item in activities)
                                {
                                    row8 = new Row();

                                    row8.Append(


                                      ConstructCell(acti.ActionAreaName.ToString(), CellValues.String, 2),
                                      ConstructCell(acti.SubactionAreaName.ToString(), CellValues.String, 2),
                                   ConstructCell(item.ActivityName.ToString(), CellValues.String, 2),
                                   ConstructCell(item.ProjectCostGrant.ToString(), CellValues.Number, 1),
                              ConstructCell(Convert.ToDecimal(item.ProjectCostCBO).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(item.LocalContribution).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(item.GovtContribution).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(item.GrandTotal).ToString("#,##0.00"), CellValues.Number, 1));
                                    acti.ActionAreaName = "";
                                    acti.SubactionAreaName = "";
                                    sd.AppendChild(row8);
                                }
                                row99 = new Row();
                                row99.Append(ConstructCell("Total ", CellValues.String, 2),
                                                            ConstructCell("", CellValues.String, 2),
                                                            ConstructCell("", CellValues.String, 2),
                                                            ConstructCell("", CellValues.String, 2),
                                                            ConstructCell(acti.ProjectCostCBO.ToString(), CellValues.String, 2),
                                                        ConstructCell(acti.LocalContribution.ToString(), CellValues.String, 2),
                                                        ConstructCell(acti.GovtContribution.ToString(), CellValues.String, 2),
                                                        ConstructCell(acti.GrandTotal.ToString(), CellValues.String, 2));
                            }
                            else
                            {
                                SubActionAreaName = acti.SubactionAreaName;


                                row8 = new Row();
                                var activities = result.ToList().Where(aa => aa.ProgramName == acti.ProgramName && aa.SubactionAreaName == acti.SubactionAreaName).ToList();
                                var SubactionAreaNames = acti.SubactionAreaName;

                                foreach (var item in activities)
                                {
                                    row8 = new Row();

                                    row8.Append(


                                      ConstructCell(acti.ActionAreaName.ToString(), CellValues.String, 2),
                                      ConstructCell(acti.SubactionAreaName.ToString(), CellValues.String, 2),
                                   ConstructCell(item.ActivityName.ToString(), CellValues.String, 2), ConstructCell(item.ProjectCostGrant.ToString(), CellValues.Number, 1),
                              ConstructCell(Convert.ToDecimal(item.ProjectCostCBO).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(item.LocalContribution).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(item.GovtContribution).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(item.GrandTotal).ToString("#,##0.00"), CellValues.Number, 1));
                                    acti.ActionAreaName = "";
                                    acti.SubactionAreaName = "";
                                    sd.AppendChild(row8);
                                }
                                row99 = new Row();
                                row99.Append(ConstructCell("Total ", CellValues.String, 2),
                                                            ConstructCell("", CellValues.String, 2),
                                                            ConstructCell("", CellValues.String, 2),
                                                            ConstructCell("", CellValues.String, 2),
                                                            ConstructCell(acti.ProjectCostCBO.ToString(), CellValues.String, 2),
                                                        ConstructCell(acti.LocalContribution.ToString(), CellValues.String, 2),
                                                        ConstructCell(acti.GovtContribution.ToString(), CellValues.String, 2),
                                                        ConstructCell(acti.GrandTotal.ToString(), CellValues.String, 2));
                            }




                        }

                    }


                    //sd.AppendChild(row8);
                    sd.AppendChild(row99);

                }



                sheets.Append(sheet);
                wb.Append(sheets);
                //AddImage(worksheetPart, imagePath1, "My first image", 1, 1); // A1
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();


            }


            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;




        }

    }
}
