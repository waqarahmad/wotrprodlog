﻿using System.Collections.Generic;
using Abp.Extensions;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using WOTR.PM.Auditing.Dto;
using WOTR.PM.DataExporting.Excel.EpPlus;
using WOTR.PM.Dto;
using WOTR.PM.Programs.Dto;
using WOTR.PM.Programs.Exporting;

namespace WOTR.PM.Programs.Exporting
{
    public class ProgramDetailsListExcelExporter : EpPlusExcelExporterBase, IProgramDetailsListExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public ProgramDetailsListExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFile(List<ProgramInformationListDto> auditLogListDtos)
        {
            return CreateExcelPackage(
                "ProgramDetails.xlsx",
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("AuditLogs"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        L("Time"),
                        L("UserName"),
                        L("Service"),
                        L("Action"),
                        L("Parameters")
                       
                    );

                    AddObjects(
                        sheet, 2, auditLogListDtos,
                        _ => _timeZoneConverter.Convert(_.ExecutionTime, _abpSession.TenantId, _abpSession.GetUserId()),
                        _ => _.ProgramName,
                        _ => _.ProgramSanctionedYear,
                        _ => _.ProgramStartDate,
                        _ => _.PrgramEndDate,
                        _ => _.GantnAmountSanctionRupess
                       
                     
                        );

                    //Formatting cells

                    var timeColumn = sheet.Column(1);
                    timeColumn.Style.Numberformat.Format = "yyyy-mm-dd hh:mm:ss";

                    for (var i = 1; i <= 10; i++)
                    {
                        if (i.IsIn(5, 10)) //Don't AutoFit Parameters and Exception
                        {
                            continue;
                        }

                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}