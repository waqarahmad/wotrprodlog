﻿using System.Collections.Generic;
using WOTR.PM.Dto;
using WOTR.PM.Programs.Dto;

namespace WOTR.PM.Programs.Exporting
{
    public interface IProgramDetailsListExcelExporter
    {
        FileDto ExportToFile(List<ProgramInformationListDto> auditLogListDtos);
    }
}
