﻿namespace WOTR.PM.Programs
{
    public interface INamespaceStripper
    {
        string StripNameSpace(string serviceName);
    }
}