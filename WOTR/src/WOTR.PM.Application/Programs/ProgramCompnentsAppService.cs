﻿using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.Authorization.Users;
using WOTR.PM.Components;
using WOTR.PM.Programs.Dto;
using Abp.Linq.Extensions;
using Abp.UI;
using Castle.Core.Internal;
using WOTR.PM.ActionAreas.Dto;
using Abp.Application.Services.Dto;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using WOTR.PM.Dto;
using DocumentFormat.OpenXml.Packaging;
//archita
using A = DocumentFormat.OpenXml.Drawing;
using Xdr = DocumentFormat.OpenXml.Drawing.Spreadsheet;
using A14 = DocumentFormat.OpenXml.Office2010.Drawing;
using System.Drawing;
using ShapeProperties = DocumentFormat.OpenXml.Drawing.Spreadsheet.ShapeProperties;
using Color = DocumentFormat.OpenXml.Spreadsheet.Color;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using Font = DocumentFormat.OpenXml.Spreadsheet.Font;
using System.IO;
//archita

namespace WOTR.PM.Programs
{
    public class ProgramCompnentsAppService : PMAppServiceBase, IProgramCompnentsAppService
    {
        private readonly IRepository<ProgramComponentsMapping> _ProgramComponentsMappingRepository;
        private readonly IRepository<ProgramComponentsActivitesMapping> _ProgramComponentsActivitesMappingRepository;//..programComponentActiviteTable
        private readonly IRepository<Program> _ProgramRepository;
        private readonly IRepository<ComponentActivityMapping> _ComponentActivityMappingRepository;
        private readonly IRepository<Activity> _ActiviteRepository;
        private readonly IRepository<Component> _ComponentRepository;
        private readonly IComponentActivityMappingRepository _ProgramComponentActivityMappingRepository;//...RepositoryToGetAllComponentAnd Activity details.

       

        public ProgramCompnentsAppService(IRepository<ProgramComponentsMapping> ProgramComponentsMappingRepository,
            IRepository<ProgramComponentsActivitesMapping> ProgramComponentsActivitesMappingRepository,
            IRepository<ComponentActivityMapping> ComponentActivityMappingRepository, IRepository<Activity> ActiviteRepository, IRepository<Component> ComponentRepository,
            IRepository<Program> ProgramRepository, IComponentActivityMappingRepository ProgramComponentActivityMappingRepository)
        {
            _ProgramComponentsMappingRepository = ProgramComponentsMappingRepository;
            _ProgramComponentsActivitesMappingRepository = ProgramComponentsActivitesMappingRepository;
            _ComponentActivityMappingRepository = ComponentActivityMappingRepository;
            _ActiviteRepository = ActiviteRepository;
            _ComponentRepository = ComponentRepository;
            _ProgramRepository = ProgramRepository;
            _ProgramComponentActivityMappingRepository = ProgramComponentActivityMappingRepository;
        }



        public async Task<List<string>> CreateOrUpdateProgramCompnents(List<ProgramCompnentsListDto> Input)
        {
            var ProgramComponentMappingID = 0;

            var result = new List<string>();

            if (Input.Count != 0)
            {
                try
                {
                    foreach (var item in Input)
                    {
                        if (item.Id == 0)
                        {
                            if (item.ComponentID != 0)
                            {
                                var pcm = new ProgramComponentsMapping();
                                pcm.ProgramID = item.ProgramID;
                                pcm.ComponentID = item.ComponentID;

                                ProgramComponentMappingID =await _ProgramComponentsMappingRepository.InsertAndGetIdAsync(pcm);
                                result.Add("ProgramComponentMappingID GENRATED");
                            }
                            else result.Add("please Add altest one component");

                            if (ProgramComponentMappingID > 0)
                            {
                                foreach (var item1 in item.ComponentActivityID)
                                {

                                    var pcam = new ProgramComponentsActivitesMapping();
                                    pcam.ActivityID = item1;
                                    pcam.ProgramID = item.ProgramID;
                                    pcam.ComponentID = item.ComponentID;
                                   await _ProgramComponentsActivitesMappingRepository.InsertAsync(pcam);
                                }
                            }
                            else result.Add("ProgramComponentMappingID");
                        }
                    }
                }
                catch (Exception e)
                {
                    result.Add(e.ToString());
                }

            }
            return result;
        }



        public List<ProgramCompnentsListDto> GetAllProgramCompnent()
        {
            var query = (from pc in _ProgramComponentsMappingRepository.GetAll()
                         join p in _ProgramRepository.GetAll() on pc.ProgramID equals p.Id
                         join c in _ComponentRepository.GetAll() on pc.ComponentID equals c.Id
                         select new ProgramCompnentsListDto
                         {
                             componentName = c.Name,
                             ComponentID = c.Id
                         }).ToList();
            return query;
        }

        public List<GetComponentActivityMappingsstDto> GetComponentActivityMapping(List<int> ID)
        {
            var q = new List<GetComponentActivityMappingsstDto>();
            foreach (var item in ID)
            {
                var query = (from mapping in _ComponentActivityMappingRepository.GetAll().Where(m => m.ComponentID == item)
                             join co in _ComponentRepository.GetAll() on mapping.ComponentID equals co.Id
                             join a in _ActiviteRepository.GetAll() on mapping.ActivityID equals a.Id
                             join ur in UserManager.Users.ToList() on mapping.CreatorUserId equals ur.Id
                             select new GetComponentActivityMappingsstDto
                             {
                                 Name = a.Name,
                                 Description = a.Description,
                                 CreationTime = mapping.CreationTime,
                                 Createdby = ur.FullName,
                                 code = co.Code,
                                 ComponentId = mapping.ComponentID,
                                 ActivityId = mapping.ActivityID

                             }).ToList().OrderByDescending(ma => ma.Name);

                foreach (var item1 in query)
                {
                    q.Add(new GetComponentActivityMappingsstDto
                    {
                        Name = item1.Name,
                        Description = item1.Description,
                        CreationTime = item1.CreationTime,
                        Createdby = item1.Createdby,
                        code = item1.code
                    });
                }
            }
            return q;
        }


        //..Add Component To Program Component Mapping Table on ADD Button of Component Page. Anuj Adkine.
        /*ADD component and Activity to ProgramComponet And ProgramComopnentActivity Table */
        public List<String> AddComponetToProgramComponetMapping(ProgramComponentMappingListDto Input)
        {
            List<string> result = new List<string>();
            int programComponentMappingId;

            if (Input.Id == 0)
            {
                foreach (var item in Input.ComponentID)
                {

                    var id = (from pc in _ProgramComponentsMappingRepository.GetAll() where (pc.ComponentID == item && pc.ProgramID == Input.ProgramID) select pc.ComponentID).ToList();


                    if (id.Count == 0)

                    {
                        try
                        {
                            var pcm = new ProgramComponentsMapping();
                            pcm.ProgramID = Input.ProgramID;
                            pcm.ComponentID = item;


                            programComponentMappingId = _ProgramComponentsMappingRepository.InsertAndGetId(pcm);
                            result.Add("ProgramComponentMappingID GENRATED");


                            //..Add the Activity For the Component to ProgramComonentActivityMapping Table.

                            if (programComponentMappingId > 0)
                            {
                                var Query = (from c in _ComponentActivityMappingRepository.GetAll() where (c.ComponentID == item) select c.ActivityID).ToList();
                                foreach (var Qresult in Query)
                                {
                                    ProgramComponentsActivitesMapping pcam = new ProgramComponentsActivitesMapping();

                                    pcam.ActivityID = Qresult;
                                    pcam.ComponentID = item;
                                    pcam.ProgramID = Input.ProgramID;

                                    _ProgramComponentsActivitesMappingRepository.Insert(pcam);
                                }

                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }


                    }


                }


            }
            return result;
        }

        public List<GetAllComponentandActivityMappingDtoList> GetAllComponentandActvity(int projectId)
        {
            var query = (from pca in _ProgramComponentsActivitesMappingRepository.GetAll()
                         where (pca.ComponentID == pca.ComponentID && pca.ProgramID == projectId)
                         join pc in _ProgramComponentsMappingRepository.GetAll()
                         on pca.ComponentID equals pc.ComponentID
                         join ur in UserManager.Users.ToList() on pca.CreatorUserId equals ur.Id
                         where (pc.ProgramID == projectId && pca.ProgramID == projectId)
                         select new GetAllComponentandActivityMappingDtoList
                         {
                             ActivityName = pca.Activity.Name,
                             Descri = pca.Activity.Description,
                             CreationTime = pca.CreationTime,
                             CreateBy = ur.FullName,
                             ComponentID = pca.Component.Id,
                             ActivityID = pca.Activity.Id,
                             ComponentName = pca.Component.Name

                         }).ToList();

            return query;
        }

        //..Delet


        //..Get ProgramComponentandActivityForProgram....Service for Component and progarm Cost Estimate.

        public List<GetComponentActivityMappingListDto> GetComponentandActivityForProgram(int ProjectId)
        {
            long? userId = AbpSession.UserId;
            int? TenantId = AbpSession.TenantId;

            List<GetComponentActivityMappingListDto> result = new List<GetComponentActivityMappingListDto>();

            var query = _ProgramComponentActivityMappingRepository.GetComponentandActivityForProgram(TenantId, userId, ProjectId);
            if (query != null)
            {
                var ListOfUsers = query.ToList().GroupBy(x => x.ComponetTableId)
                                      .Select(g => g.First())
                                      .ToList();

                foreach (var item in ListOfUsers)
                {
                    List<ComponentAndactivityMappingForList> obj = new List<ComponentAndactivityMappingForList>();
                    foreach (var act in query)
                    {
                        if (act.ActivityID != 0)
                        {
                            if (item.ComponetTableId == act.ComponetTableId)
                            {
                                obj.Add(new ComponentAndactivityMappingForList
                                {
                                    CreationTime = act.CreationTime,
                                    ActivityDescri = act.ActivityDescri,
                                    ActivityName = act.ActivityName,
                                    ActivityID = act.ActivityID,
                                    TenantId = act.TenantId,
                                    FullName = act.FullName,



                                });
                            }
                        }
                    }

                    result.Add(new GetComponentActivityMappingListDto
                    {
                        ComponetTableId = item.ComponetTableId,
                        componentName = item.componentName,
                        ComponentCode = item.ComponentCode,
                        ComponentCreatime = item.ComponentCreatime,
                        Activity = obj,

                        ActivityCount = obj.Count,

                    });
                }
            }
            return result;
        }


        public List<ProgrameComponentListDto> GetComponentCostEstimation(int programeId)
        {
            var userId = AbpSession.UserId;
            var TenantId = AbpSession.TenantId;

            var result = new List<ProgrameComponentListDto>();

            var query = _ProgramComponentActivityMappingRepository.GetComponentandActivityForCostEstimation(TenantId, userId, programeId);

            if (query != null)
            {
                var listOfUsers = query.ToList().GroupBy(x => x.ComponetTableId)
                                      .Select(g => g.First())
                                      .ToList();

                var dates = new List<string>();
                dates.Add("OverAll");
                long start = 0;
                long end = 0;
                if (query[0].ProgrameStartDate.Month < 4)
                {
                    start = query[0].ProgrameStartDate.Year - 1;
                    end = query[0].ProgrameStartDate.Year;
                }
                else
                {
                    start = query[0].ProgrameStartDate.Year;
                    end = query[0].ProgrameStartDate.Year + 1;
                }

                for (var dt = start; dt <= query[0].ProgrameEndDate.Year; dt = dt + 1)
                {
                    var from = DateTime.Parse("April 1," + (start));
                    var to = DateTime.Parse("March 31, " + end);
                    if (query[0].ProgrameEndDate.Month > to.Month || query[0].ProgrameEndDate.Year > from.Year)
                    {
                        dates.Add(start.ToString() + "-" + end.ToString());
                    }
                    start++;
                    end++;
                }

                foreach (var item in listOfUsers)
                {
                    var obj1 = new List<ProgrameCostEstimationYearDto>();

                    var obj = query[0].CostEstimationYear[0].Activity.Where(t => t.ActivityID != 0 && t.ComponetTableId == item.ComponetTableId).ToList();
                   
                    foreach (var item2 in dates)
                    {
                        var i = new List<ComponentAndactivityMappingForList>();
                        foreach (var u in obj)
                        {
                            u.ProgramCostEstimate_UnitOfMeasuresID = u.ProgramCostEstimate_UnitOfMeasuresID == 0 ? 1 : u.ProgramCostEstimate_UnitOfMeasuresID;
                            if (u.CostEstimationYear != "")
                            {
                                if (u.CostEstimationYear == item2)
                                {
                                    i.Add(u);
                                }
                                else
                                {
                                    if (u.CostEstimationYear == "OverAll" && !obj.Any(t=>t.CostEstimationYear == item2))
                                    {
                                        var c = new ComponentAndactivityMappingForList();
                                        c.ActivityDescri = u.ActivityDescri;
                                        c.ActivityID = u.ActivityID;
                                        c.ComponetTableId = u.ComponetTableId;
                                        c.ActivityName = u.ActivityName;
                                        c.ActionArea = u.ActionArea;
                                        c.ActivityLevel = u.ActivityLevel;
                                        c.ProgramCostEstimate_TotalUnits = 0;
                                        c.ProgramCostEstimate_TotalUnitCost = 0;
                                        c.ProgramCostEstimate_Total = 0;
                                        c.ProgramCostEstimate_ProgramID = u.ProgramCostEstimate_ProgramID;
                                        c.ProgramCostEstimate_UnitCost = u.ProgramCostEstimate_UnitCost;
                                        c.ProgramCostEstimate_UnitOfMeasuresID = u.ProgramCostEstimate_UnitOfMeasuresID;
                                        c.ProgramCostEstimate_CommunityContribution = 0;
                                        c.ProgramCostEstimate_FunderContribution = 0;
                                        c.ProgramCostEstimate_OtherContribution = 0;
                                        i.Add(c);
                                    }
                                }
                            }
                            else
                            {
                                i.Add(u);
                            }
                        }
                        decimal? subTotalOfTotalCost = 0;
                        decimal? subTotalTotalUnits = 0;
                        var id = 0;
                        foreach (var year in query[0].CostEstimationYear)
                        {
                            if (year.CostEstimationYear == item2 && year.ComponetTableId == item.ComponetTableId)
                            {
                                id = year.Id;
                                subTotalOfTotalCost = year.SubTotalofTotalCost;
                                subTotalTotalUnits = year.SubTotalTotalunits;
                                
                            }
                        }

                        obj1.Add(new ProgrameCostEstimationYearDto
                        {
                            CostEstimationYear = item2,
                            Id = id,
                            SubTotalofTotalCost = subTotalOfTotalCost,
                            SubTotalTotalunits = subTotalTotalUnits,
                            Activity = i,
                            
                        });
                    }

                    result.Add(new ProgrameComponentListDto
                    {
                        Id = item.Id,
                        ComponetTableId = item.ComponetTableId,
                        componentName = item.componentName,
                        ComponentCode = item.ComponentCode,
                        ComponentCreatime = item.ComponentCreatime,
                        ProgrameId = item.ProgrameId,
                        ProgrameStartDate = item.ProgrameStartDate,
                        ProgrameEndDate = item.ProgrameEndDate,
                        CostEstimationYear = obj1,
                        TotalCost = item.TotalCost,
                        TotalUnits = item.TotalUnits,
                      
                    });
                }
            }
            return result;

        }

        public string ProgramName(int programId)
        {
            var program = _ProgramRepository.FirstOrDefault(p => p.Id == programId);
            return program.Name;
        }

        public List<ProgrameComponentListDto> OverallRegionspendingdetails(int programeId,string Year)
        {
            var userId = AbpSession.UserId;
            var TenantId = AbpSession.TenantId;

            var result = new List<ProgrameComponentListDto>();

            var query = _ProgramComponentActivityMappingRepository.GetOverallRegionSpendingdetailsCostEstimation(TenantId, userId, programeId,Year);

            if (query != null)
            {
                var listOfUsers = query.ToList().GroupBy(x => x.ComponetTableId)
                                      .Select(g => g.First())
                                      .ToList();

                var dates = new List<string>();
                dates.Add("OverAll");
                long start = 0;
                long end = 0;
                if (query[0].ProgrameStartDate.Month < 4)
                {
                    start = query[0].ProgrameStartDate.Year - 1;
                    end = query[0].ProgrameStartDate.Year;
                }
                else
                {
                    start = query[0].ProgrameStartDate.Year;
                    end = query[0].ProgrameStartDate.Year + 1;
                }

                for (var dt = start; dt <= query[0].ProgrameEndDate.Year; dt = dt + 1)
                {
                    var from = DateTime.Parse("April 1," + (start));
                    var to = DateTime.Parse("March 31, " + end);
                    if (query[0].ProgrameEndDate.Month > to.Month || query[0].ProgrameEndDate.Year > from.Year)
                    {
                        dates.Add(start.ToString() + "-" + end.ToString());
                    }
                    start++;
                    end++;
                }

                foreach (var item in listOfUsers)
                {
                    var obj1 = new List<ProgrameCostEstimationYearDto>();

                    var obj = query[0].CostEstimationYear[0].Activity.Where(t => t.ActivityID != 0 && t.ComponetTableId == item.ComponetTableId).ToList();

                    foreach (var item2 in dates)
                    {
                        var i = new List<ComponentAndactivityMappingForList>();
                        foreach (var u in obj)
                        {
                            u.ProgramCostEstimate_UnitOfMeasuresID = u.ProgramCostEstimate_UnitOfMeasuresID == 0 ? 1 : u.ProgramCostEstimate_UnitOfMeasuresID;
                            if (u.CostEstimationYear != "")
                            {
                                if (u.CostEstimationYear == item2)
                                {
                                    i.Add(u);
                                }
                            }
                            else
                            {
                                i.Add(u);
                            }
                        }
                        decimal? subTotalOfTotalCost = 0;
                        decimal? subTotalTotalUnits = 0;
                        var id = 0;
                        foreach (var year in query[0].CostEstimationYear)
                        {
                            if (year.CostEstimationYear == item2 && year.ComponetTableId == item.ComponetTableId)
                            {
                                id = year.Id;
                                subTotalOfTotalCost = year.SubTotalofTotalCost;
                                subTotalTotalUnits = year.SubTotalTotalunits;

                            }
                        }

                        obj1.Add(new ProgrameCostEstimationYearDto
                        {
                            CostEstimationYear = item2,
                            Id = id,
                            SubTotalofTotalCost = subTotalOfTotalCost,
                            SubTotalTotalunits = subTotalTotalUnits,
                            Activity = i,

                        });
                    }

                    result.Add(new ProgrameComponentListDto
                    {
                        Id = item.Id,
                        ComponetTableId = item.ComponetTableId,
                        componentName = item.componentName,
                        ComponentCode = item.ComponentCode,
                        ComponentCreatime = item.ComponentCreatime,
                        ProgrameId = item.ProgrameId,
                        ProgrameStartDate = item.ProgrameStartDate,
                        ProgrameEndDate = item.ProgrameEndDate,
                        CostEstimationYear = obj1,
                        TotalCost = item.TotalCost,
                        TotalUnits = item.TotalUnits,

                    });
                }
            }
            return result;

        }
        private Stylesheet GenerateStylesheet()
        {
            Stylesheet styleSheet = null;

            Fonts fonts = new Fonts(
                new DocumentFormat.OpenXml.Spreadsheet.Font( // Index 0 - default
                    new FontSize() { Val = 11 }

                ),
                new DocumentFormat.OpenXml.Spreadsheet.Font( // Index 1 - header
                    new FontSize() { Val = 11 },
                    new Bold(),
                    new Color() { Rgb = "000000" }

                ));

            Fills fills = new Fills(
                    new Fill(new PatternFill() { PatternType = PatternValues.None }), // Index 0 - default
                    new Fill(new PatternFill() { PatternType = PatternValues.Gray125 }), // Index 1 - default
                    new Fill(new PatternFill(new ForegroundColor { Rgb = new HexBinaryValue() { Value = "D0CECE" } })
                    { PatternType = PatternValues.Solid }) // Index 2 - header
                );

            Borders borders = new Borders(
                    new Border(), // index 0 default
                    new Border( // index 1 black border
                        new LeftBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new RightBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new TopBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new BottomBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                );

            CellFormats cellFormats = new CellFormats(
                    new CellFormat(), // default
                    new CellFormat { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }, // body
                    new CellFormat { FontId = 1, FillId = 2, BorderId = 1, ApplyFill = true } // header
                );

            styleSheet = new Stylesheet(fonts, fills, borders, cellFormats);

            return styleSheet;
        }

        public async Task<FileDto> CreateExcelDoc(string fileName, int programId, string year)
        {

            var result = OverallRegionspendingdetails(programId,year);
            var result1 = ProgramName(programId);
            var a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";

            using (SpreadsheetDocument xl = SpreadsheetDocument.Create( a, SpreadsheetDocumentType.Workbook))
            {
                var wbp = xl.AddWorkbookPart();
                var wsp = wbp.AddNewPart<WorksheetPart>();
                var wb = new Workbook();
                var fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();

                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                var dp = wsp.AddNewPart<DrawingsPart>();
                var imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                try
                {
                    using (FileStream fs = new FileStream(sImagePath, FileMode.Open, FileAccess.Read))
                    {
                        imgp.FeedData(fs);
                    }
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }
                var nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                var nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                var nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                var blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                var bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                var sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                var pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                var ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                var anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                var wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                var drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                var sheets = new Sheets();
                var sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                wb.Append(fv);

                // Constructing header
                var row3 = new Row();
                var row4 = new Row();
                var row5 = new Row();


                
                    var cell12 = InsertCellInWorksheet("D", 1, wsp);
                cell12.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                cell12.DataType = new EnumValue<CellValues>(CellValues.Number);
                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cell12.Append(inlineString);

                var cell1 = InsertCellInWorksheet("D", 2, wsp);
                cell1.CellValue = new CellValue("Region spending plan cluster village wise ");
                cell1.DataType = new EnumValue<CellValues>(CellValues.Number);
                Run run2 = new Run();
                run2.Append(new Text("Region spending plan cluster village wise "));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cell1.Append(inlineString1);

                var cell333 = InsertCellInWorksheet("D", 4, wsp);
                cell333.CellValue = new CellValue("Program Title: " + result1.ToString());
                cell333.DataType = new EnumValue<CellValues>(CellValues.Number);

                

                var cellString1 = InsertCellInWorksheet("D", 5, wsp);
                cellString1.CellValue = new CellValue("Year: " + year.ToString());
                cellString1.DataType = new EnumValue<CellValues>(CellValues.Number);
                

                var rowindex = 13;

               
                    
               

                row3.Append(ConstructCell(" ", CellValues.String));

                sd.AppendChild(row3);
               
                row5 = new Row();
                row5.Append(
                    ConstructCell("Component Name", CellValues.String, 2),
                  ConstructCell("Action Level", CellValues.String, 2),

                ConstructCell("Activity Name", CellValues.String, 2),
                ConstructCell("Total Unit", CellValues.String, 2),
                ConstructCell("Unit Cost", CellValues.String, 2),
                ConstructCell("Total Unit Cost", CellValues.String, 2),
                ConstructCell("Community Contribution", CellValues.String, 2),
                ConstructCell("Funder Contribution", CellValues.String, 2),
                ConstructCell("Other Contribution", CellValues.String, 2));

                sd.AppendChild(row5);

                foreach (var imp in result)
                {

                    foreach (var c in imp.CostEstimationYear)
                    {
                        if (c.CostEstimationYear != "OverAll")
                        {
                            var component = imp.componentName;
                            var totalcost = 0.00;
                            var toatlcost12 = 0.00;
                            var TotalUnits = 0.00;
                            var unitcost = 0.00;
                            var TotalUnitCost = 0.00;
                            var communitycontribution = 0.00;
                            var fundercontribution = 0.00;
                            var othercontribution = 0.00;

                            foreach (var act in c.Activity)
                            {
                                totalcost += Convert.ToDouble(act.ProgramCostEstimate_Total);
                                TotalUnits += Convert.ToInt64(act.ProgramCostEstimate_TotalUnits);
                                unitcost += Convert.ToInt64(act.ProgramCostEstimate_UnitCost);
                                TotalUnitCost += Convert.ToInt64(act.ProgramCostEstimate_TotalUnitCost);
                                totalcost += Convert.ToDouble(act.ProgramCostEstimate_Total);
                                toatlcost12 += Convert.ToDouble(act.ProgramCostEstimate_Total);
                                communitycontribution += Convert.ToDouble(act.ProgramCostEstimate_CommunityContribution);
                                fundercontribution += Convert.ToDouble(act.ProgramCostEstimate_FunderContribution);
                                othercontribution += Convert.ToDouble(act.ProgramCostEstimate_OtherContribution);
                                row4 = new Row();

                                row4.Append(ConstructCell(component, CellValues.String, 1),
                                    ConstructCell(act.ActivityLevel.ToString(), CellValues.String, 1),
                                    ConstructCell(act.ActivityName.ToString(), CellValues.String, 1),
                                    ConstructCell(act.ProgramCostEstimate_TotalUnits.ToString(), CellValues.Number, 1),
                                    ConstructCell(act.ProgramCostEstimate_UnitCost.ToString(), CellValues.Number, 1),
                                    ConstructCell(Convert.ToDecimal (act.ProgramCostEstimate_TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                    ConstructCell(Convert.ToDecimal(act.ProgramCostEstimate_CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                    ConstructCell(Convert.ToDecimal(act.ProgramCostEstimate_FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                    ConstructCell(Convert.ToDecimal(act.ProgramCostEstimate_OtherContribution).ToString("#,##0.00"), CellValues.Number, 1)
                                    );

                                sd.AppendChild(row4);

                                component = "";
                            }

                            if (c.CostEstimationYear == year)
                            {
                                
                                var row18 = new Row();
                                row18.Append(
                                     ConstructCell("Total", CellValues.String, 2),
                                      ConstructCell(" ", CellValues.String, 2),
                                       ConstructCell(" ", CellValues.String, 2),

                                      ConstructCell(TotalUnits.ToString(), CellValues.Number,  2),
                                        ConstructCell(Convert.ToDecimal( unitcost).ToString("#,##0.00"), CellValues.Number, 2),
                                        ConstructCell(Convert.ToDecimal(TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 2),
                                        ConstructCell(Convert.ToDecimal(communitycontribution).ToString("#,##0.00"), CellValues.Number, 2),
                                        ConstructCell(Convert.ToDecimal(fundercontribution).ToString("#,##0.00"), CellValues.Number, 2),
                                        ConstructCell(othercontribution.ToString("#,##0.00"), CellValues.Number, 2)
                                        );

                                sd.Append(row18);
                            }
                        }
                    }
                }
                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();
            }

            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;
        }

        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
        private Cell ConstructCell(string value, CellValues dataType, uint styleIndex = 0)
        {
            
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType),
                StyleIndex = styleIndex,
                
        };
        }



        public void AddActivityToProgrameComponentActivityMapping(GetAllComponentandActivityMappingDtoList Input)
        {
            List<string> message = new List<string>();
            if (Input.Id == 0)
            {
                var id = (from pc in _ProgramComponentsActivitesMappingRepository.GetAll()
                          where (pc.ComponentID == Input.ComponentID && pc.ProgramID == Input.ProgramID && pc.ActivityID == Input.ActivityID)
                          select pc.ComponentID).ToList();


                if (id.Count == 0)

                {

                    try
                    {

                        ProgramComponentsActivitesMapping addActivity = new ProgramComponentsActivitesMapping();
                        addActivity.ComponentID = Input.ComponentID;
                        addActivity.ProgramID = Input.ProgramID;
                        addActivity.ActivityID = Input.ActivityID;

                        if (Input.ActivityID == 0)
                        {
                            message.Add("Please Attach Subactivity");
                        }
                        else
                        {
                            _ProgramComponentsActivitesMappingRepository.Insert(addActivity);
                        }



                    }
                    catch (Exception e)
                    {


                    }

                }
            }
        }






        public void deleteProgramComponent(int ComponentId, int programId)
        {
            var query = _ProgramComponentsMappingRepository.GetAll().Where(pc => pc.ProgramID == programId && pc.ComponentID == ComponentId).FirstOrDefault().Id;
            _ProgramComponentsMappingRepository.Delete(query);
            var act = _ProgramComponentsActivitesMappingRepository.GetAll().Where(pa => pa.ProgramID == programId && pa.ComponentID == ComponentId).ToList();
            foreach (var item in act)
            {
                _ProgramComponentsActivitesMappingRepository.Delete(item.Id);
            }
        }

        public void deleteProgramComponentActivity(int ComponentId, int programId, int ActivityId)
        {
            var act = _ProgramComponentsActivitesMappingRepository.GetAll().Where(pa => pa.ProgramID == programId && pa.ComponentID == ComponentId && pa.ActivityID == ActivityId).ToList();
            foreach (var item in act)
            {
                _ProgramComponentsActivitesMappingRepository.Delete(item.Id);
            }
        }

        public void deleteProgramActivity(EntityDto<int> Input)
        {
            //throw new NotImplementedException();
            _ProgramComponentsActivitesMappingRepository.Delete(Input.Id);
        }
        //archita
        //public List<ProgrameComponentListDto> GetComponentCostEstimationforallprogramRegionDetails(int ProgramID)
        //{

        //    long? userId = AbpSession.UserId;
        //    int? TenantId = AbpSession.TenantId;

        //    List<ProgrameComponentListDto> result = new List<ProgrameComponentListDto>();

        //    //var query = _ProgramComponentActivityMappingRepository.GetComponentandActivityForCostEstimationforallprograms(TenantId, userId);
        //    // var query = _ProgramComponentActivityMappingRepository
        //    var query = _ProgramComponentActivityMappingRepository.GetdataforRegionwiseDetails(TenantId, userId, ProgramID);
        //    if (query != null)
        //    {
        //        var ListOfUsers = query.ToList().GroupBy(x => x.ComponetTableId)
        //                              .Select(g => g.First())
        //                              .ToList();
        //        //


        //        foreach (var item in ListOfUsers)
        //        {
        //            List<ProgrameCostEstimationYearDto> obj1 = new List<ProgrameCostEstimationYearDto>();

        //            List<ComponentAndactivityMappingForList> obj = new List<ComponentAndactivityMappingForList>();

        //            foreach (var act in query[0].CostEstimationYear[0].Activity)
        //            {
        //                if (act.ActivityID != 0)
        //                {
        //                    if (item.ComponetTableId == act.ComponetTableId)
        //                    {
        //                        obj.Add(act);

        //                    }

        //                }
        //            }
        //        }



        //        //

        //        var dates = new List<string>();
        //        dates.Add("OverAll");
        //        long Start = 0;
        //        long end = 0;
        //        if (query[0].ProgrameStartDate.Month < 4)
        //        {
        //            Start = query[0].ProgrameStartDate.Year - 1;
        //            end = query[0].ProgrameStartDate.Year;
        //        }
        //        else
        //        {
        //            Start = query[0].ProgrameStartDate.Year;
        //            end = query[0].ProgrameStartDate.Year + 1;
        //        }

        //        for (var dt = Start; dt <= query[0].ProgrameEndDate.Year; dt = dt + 1)
        //        {
        //            DateTime from = DateTime.Parse("April 1," + (Start));
        //            DateTime to = DateTime.Parse("March 31, " + end);
        //            if (query[0].ProgrameEndDate.Month > to.Month || query[0].ProgrameEndDate.Year > from.Year)
        //            {
        //                dates.Add(Start.ToString() + "-" + end.ToString());
        //            }
        //            Start++;
        //            end++;
        //        }

        //        foreach (var item in ListOfUsers)
        //        {
        //            List<ProgrameCostEstimationYearDto> obj1 = new List<ProgrameCostEstimationYearDto>();

        //            List<ComponentAndactivityMappingForList> obj = new List<ComponentAndactivityMappingForList>();

        //            foreach (var act in query[0].CostEstimationYear[0].Activity)
        //            {
        //                if (act.ActivityID != 0)
        //                {
        //                    if (item.ComponetTableId == act.ComponetTableId)
        //                    {
        //                        obj.Add(act);
        //                    }

        //                }
        //            }
        //            foreach (var item2 in dates)
        //            {
        //                List<ComponentAndactivityMappingForList> i = new List<ComponentAndactivityMappingForList>();
        //                foreach (var u in obj)
        //                {
        //                    u.ProgramCostEstimate_UnitOfMeasuresID = u.ProgramCostEstimate_UnitOfMeasuresID == 0 ? 1 : u.ProgramCostEstimate_UnitOfMeasuresID;
        //                    if (u.CostEstimationYear != "")
        //                    {
        //                        if (u.CostEstimationYear == item2.ToString())
        //                        {
        //                            i.Add(u);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        i.Add(u);
        //                    }
        //                }
        //                decimal? SubTotalofTotalCost = 0;
        //                decimal? SubTotalTotalunits = 0;
        //                int id = 0;
        //                foreach (var year in query[0].CostEstimationYear)
        //                {
        //                    if (year.CostEstimationYear == item2.ToString() && year.ComponetTableId == item.ComponetTableId)
        //                    {
        //                        id = year.Id;
        //                        SubTotalofTotalCost = year.SubTotalofTotalCost;
        //                        SubTotalTotalunits = year.SubTotalTotalunits;
        //                    }
        //                }

        //                obj1.Add(new ProgrameCostEstimationYearDto
        //                {
        //                    CostEstimationYear = item2,
        //                    Id = id,
        //                    SubTotalofTotalCost = SubTotalofTotalCost,
        //                    SubTotalTotalunits = SubTotalTotalunits,
        //                    Activity = i
        //                });
        //            }

        //            result.Add(new ProgrameComponentListDto
        //            {
        //                Id = item.Id,
        //                ComponetTableId = item.ComponetTableId,
        //                componentName = item.componentName,
        //                ComponentCode = item.ComponentCode,
        //                ComponentCreatime = item.ComponentCreatime,
        //                ProgrameId = item.ProgrameId,
        //                ProgrameStartDate = item.ProgrameStartDate,
        //                ProgrameEndDate = item.ProgrameEndDate,
        //                CostEstimationYear = obj1,
        //                TotalCost = item.TotalCost,
        //                TotalUnits = item.TotalUnits,

        //            });
        //        }


        //    }
        //    return result;



        //}
        //archita

    }
}

