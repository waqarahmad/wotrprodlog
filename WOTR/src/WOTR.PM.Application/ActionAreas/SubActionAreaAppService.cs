﻿using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas.Dto;
using WOTR.PM.PrgActionAreaActivitysMappings;

namespace WOTR.PM.ActionAreas
{
    public class SubActionAreaAppService : PMAppServiceBase, ISubActionAreaAppService
    {
        private readonly IRepository<SubActionArea> _subactionareaRepository;
        private readonly IRepository<PrgActionAreaActivityMapping> _PrgActionAreaActivityMappingRepository;

        public SubActionAreaAppService(IRepository<SubActionArea> subactionareaRepository, IRepository<PrgActionAreaActivityMapping> PrgActionAreaActivityMappingRepository)
        {
            _PrgActionAreaActivityMappingRepository = PrgActionAreaActivityMappingRepository;
       
            _subactionareaRepository = subactionareaRepository;
        }

        public List<string> CreatOrUpdateSubActionArea(SubActionAreaListDto Input)
        {

            List<string> message = new List<string>();            

                SubActionArea subactionareas = new SubActionArea();
                subactionareas.Id = Input.Id;
                subactionareas.Code = Input.Code;
                subactionareas.ActionAreaId = Input.ActionAreaId;
                subactionareas.Name = char.ToUpper(Input.Name[0]) + Input.Name.Substring(1);

                if (Input.Id == 0)
                {
                try
                {
                    var QueryCheck = (from a in _subactionareaRepository.GetAll() where (a.Code == Input.Code && a.Name == Input.Name) select a.Id).ToList();
                    if (QueryCheck.Count == 0)
                    {
                        _subactionareaRepository.Insert(subactionareas);
                        message.Add("SubActionArea Added  sucessfully !");
                    }

                    else { message.Add("Record alread Present!"); }
                }
                catch (Exception)
                {

                    throw new UserFriendlyException(L("Record NOt Add"));
                }

                }
                else
                {
                    try
                    {
                    subactionareas.TenantId = Input.TenantId;
                    subactionareas.CreatorUserId = Input.CreatorUserId;
                        _subactionareaRepository.Update(subactionareas);
                        message.Add("SubActionArea Update  sucessfully !");

                    }
                    catch (Exception)
                    {

                        throw new UserFriendlyException(L("Record NOt Update"));
                    }
                }

            return message;
        }

        public List<SubActionAreaListDto> GetAllSubActionAreaRecord()
        {


            var query = (from sa in _subactionareaRepository.GetAll()
                         join ur in UserManager.Users.ToList()
                         on sa.CreatorUserId equals ur.Id
                         select new SubActionAreaListDto()
                         {
                             Name = sa.Name,
                             Code = sa.Code,
                             CreationTime = sa.CreationTime,
                             FullName = ur.FullName,
                             Id = sa.Id,
                             ActionAreaId =sa.ActionAreaId,
                             TenantId =sa.TenantId
                         }).ToList().OrderByDescending(c=>c.CreationTime);


            return  new List<SubActionAreaListDto>(ObjectMapper.Map<List<SubActionAreaListDto>>(query));
           // return query;
        }

        public List<SubActionAreaListDto> Getsearch(string input)
        {
           
            var qu = (from aa in _subactionareaRepository.GetAll().WhereIf(
                 !input.IsNullOrEmpty(),
                 p => p.Name.ToUpper().Contains(input.ToUpper()) ||
                       p.Name.ToLower().Contains(input.ToLower()) ||
                         p.Code.Contains(input))
                      join ur in UserManager.Users.ToList()
                     on aa.CreatorUserId equals ur.Id
                      select new SubActionAreaListDto()
                      {
                          Name = aa.Name,
                          Code = aa.Code,
                          CreationTime = aa.CreationTime,
                          FullName = ur.FullName,
                          Id = aa.Id,
                          ActionAreaId = aa.ActionAreaId,                         
                          TenantId = aa.TenantId,
                          CreatorUserId =aa.CreatorUserId,
                      }).ToList().OrderBy(c => c.Code); ;

            return new List<SubActionAreaListDto>(ObjectMapper.Map<List<SubActionAreaListDto>>(qu));
           
        }

        public List<SubActionAreaListDto> GetSubactionaginsActionarea(int actionAreaId)
        {
            var Query = (from a in _subactionareaRepository.GetAll() where( a.ActionAreaId == actionAreaId)
                         select new SubActionAreaListDto
                         {
                             Name = a.Name,
                             Code=a.Code,
                             Id=a.Id
            }).ToList();
            return Query;
        }


        public string DeleteSubactionArea(EntityDto<int> Input)
        {


            var QueryCheck = (from a in _PrgActionAreaActivityMappingRepository.GetAll() where (a.SubActionAreaID == Input.Id) select a.Id).ToList();
            if (QueryCheck.Count == 0)
            {
                _subactionareaRepository.Delete(Input.Id);

                return "Record Deleted ";
            }
            else
            {
                return "Record Exist";
            }
           
            
        }


    }
}
