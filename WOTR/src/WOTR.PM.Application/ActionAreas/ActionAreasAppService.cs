﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Castle.Core.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas.Dto;
using WOTR.PM.PrgActionAreaActivitysMappings;

namespace WOTR.PM.ActionAreas
{
    public class ActionAreasAppService : PMAppServiceBase, IActionAreasAppService
    {
        private readonly IRepository<ActionArea> _actionAreaAppRepository;
        private readonly IRepository<PrgActionAreaActivityMapping> _PrgActionAreaActivityMappingRepository;

        public ActionAreasAppService(IRepository<ActionArea> actionAreaAppRepository, IRepository<PrgActionAreaActivityMapping> PrgActionAreaActivityMappingRepository)
        {
            _PrgActionAreaActivityMappingRepository = PrgActionAreaActivityMappingRepository;


            _actionAreaAppRepository = actionAreaAppRepository;
        }

        public List<string> CreatOrUpdateActionArea(ActionAreasListDto Input)
        {
            List<string> message = new List<string>();
            try
            {
                ActionArea actionareas = ObjectMapper.Map<ActionArea>(Input);
                actionareas.Name = char.ToUpper(Input.Name[0]) + Input.Name.Substring(1);
                actionareas.Code = Input.Code;
                actionareas.CreatorUserId = Input.CreatorUserId;
                actionareas.Id = Input.Id;
                if (Input.Id == 0)
                {
                    try
                    {
                        var QueryCheck = (from a in _actionAreaAppRepository.GetAll() where (a.Code == Input.Code && a.Name == Input.Name) select a.Id).ToList();
                        if (QueryCheck.Count == 0)
                        {
                            _actionAreaAppRepository.Insert(actionareas);

                            message.Add("ActionArea Added  sucessfully !");
                        }
                        else message.Add("Record is already Present");
                    }
                    catch (Exception)
                    {

                        throw new UserFriendlyException(L("Record NOt Add"));
                    }


                }
                else
                {
                    try
                    {
                        actionareas.TenantId = Input.TenantId;
                        actionareas.CreatorUserId = Input.CreatorUserId;
                        _actionAreaAppRepository.Update(actionareas);
                        message.Add("ActionArea Update  sucessfully !");

                    }
                    catch (Exception)
                    {

                        throw new Abp.UI.UserFriendlyException(L("Record NOt Update"));
                    }

                }
            }
            catch (Exception Ex)
            {
                message.Add("Error= " + Ex.Message);
            }
            return message;
        }

        public List<ActionAreasListDto> GetAllActionAreaRecord()
        {
            var query = (from aa in _actionAreaAppRepository.GetAll()
                         join ur in UserManager.Users.ToList()
                         on aa.CreatorUserId equals ur.Id
                         select new ActionAreasListDto()
                         {
                             Name = aa.Name,
                             Code = aa.Code,
                             CreationTime = aa.CreationTime,
                             FirstName = ur.FullName,
                             TenantId =aa.TenantId,
                             Id = aa.Id,
                             CreatorUserId = aa.CreatorUserId

                         }).ToList().OrderBy(c => c.Code); ;
            return new List<ActionAreasListDto>(ObjectMapper.Map<List<ActionAreasListDto>>(query));

        }

        public List<ActionAreasListDto> Getsearch(string input)
        {

            var qu = (from aa in _actionAreaAppRepository.GetAll().WhereIf(
                 !input.IsNullOrEmpty(),
                 p => p.Name.ToUpper().Contains(input.ToUpper()) ||
                 p.Name.ToLower().Contains(input.ToLower()) ||
                         p.Code.Contains(input))
                      join ur in UserManager.Users.ToList()
                        on aa.CreatorUserId equals ur.Id
                      select new ActionAreasListDto()
                      {
                          Name = aa.Name,
                          Code = aa.Code,
                          CreationTime = aa.CreationTime,
                          FirstName = ur.FullName,
                          TenantId =aa.TenantId,
                          CreatorUserId=aa.CreatorUserId,
                          Id = aa.Id,

                      }).ToList().OrderBy(c => c.Code); ;
            return new List<ActionAreasListDto>(ObjectMapper.Map<List<ActionAreasListDto>>(qu));
        }


        public string DeleteActoinArea(EntityDto<int> Input)
        {


            var check = (from aa in _actionAreaAppRepository.GetAll() where (aa.Id==Input.Id &&  aa.CreatorUserId == 2) select aa.Id).ToList();

            if (check.Count == 0)
            {
                var QueryCheck = (from a in _PrgActionAreaActivityMappingRepository.GetAll() where (a.ActionAreaID == Input.Id) select a.Id).ToList();
                if (QueryCheck.Count == 0)
                {
                    _actionAreaAppRepository.Delete(Input.Id);

                    return "Record Deleted ";
                }
                else
                {
                    return "Record Exist";
                }
            }
            else
                return "Record Exist";

        }
    }
}


