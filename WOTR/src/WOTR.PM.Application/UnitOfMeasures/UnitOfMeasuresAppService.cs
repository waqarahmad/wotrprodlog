using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using WOTR.PM.UnitOfMeasures.Dtos;
using Abp.Application.Services.Dto;
using WOTR.PM.Authorization;
using Abp.Authorization;
using Microsoft.EntityFrameworkCore;
using WOTR.PM.ProgramExpenses;

namespace WOTR.PM.UnitOfMeasures
{
	//[AbpAuthorize(AppPermissions.Pages_UnitofMeasures)]
    public class UnitOfMeasuresAppService : PMAppServiceBase, IUnitOfMeasuresAppService
    {
		 private readonly IRepository<UnitofMeasure> _unitofMeasureRepository;
        private readonly IRepository<ProgramExpense> _ProgramExpenseRepository;

        public UnitOfMeasuresAppService(IRepository<UnitofMeasure> unitofMeasureRepository, IRepository<ProgramExpense> ProgramExpenseRepository) 
		  {
			_unitofMeasureRepository = unitofMeasureRepository;
            _ProgramExpenseRepository = ProgramExpenseRepository;
        }

		 public async Task<PagedResultDto<UnitofMeasureDto>> GetAll(GetAllUnitofMeasuresInput input)
         {
            var query = _unitofMeasureRepository.GetAll().WhereIf(
                    !string.IsNullOrWhiteSpace(input.Filter),
                    e =>  e.Name.Contains(input.Filter) 
                );

            var totalCount = await query.CountAsync();

            var unitofMeasures = await query
                .OrderBy(input.Sorting ?? "id asc")
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<UnitofMeasureDto>(
                totalCount,
                ObjectMapper.Map<List<UnitofMeasureDto>>(unitofMeasures)
            );
         }

        public List<UnitofMeasureDto> GetAllUnitOfMeasure()
        {
            var query = _unitofMeasureRepository.GetAll().ToList();
            return new List<UnitofMeasureDto>(ObjectMapper.Map<List<UnitofMeasureDto>>(query));
            //return new List<UnitofMeasureDto>(
            //    totalCount,
            //    ObjectMapper.Map<List<UnitofMeasureDto>>(unitofMeasures)
            //);
        }

        // [AbpAuthorize(AppPermissions.Pages_UnitofMeasures_Edit)]
        public async Task<CreateOrEditUnitofMeasureDto> GetUnitofMeasureForEdit(EntityDto<int> input)
         {
            var unitofMeasure = await _unitofMeasureRepository.FirstOrDefaultAsync(input.Id);
            return ObjectMapper.Map<CreateOrEditUnitofMeasureDto>(unitofMeasure);
         }

		 public async Task CreateOrEdit(CreateOrEditUnitofMeasureDto input)
         {
            if(input.Id == null){
				await Create(input);
			}
			else{
				await Update(input);
			}
         }

		// [AbpAuthorize(AppPermissions.Pages_UnitofMeasures_Create)]
		 private async Task Create(CreateOrEditUnitofMeasureDto input)
         {
            var unitofMeasure = ObjectMapper.Map<UnitofMeasure>(input);

			if (AbpSession.TenantId != null)
            { 
            unitofMeasure.TenantId =  (int) AbpSession.TenantId;
            }

            await _unitofMeasureRepository.InsertAsync(unitofMeasure);
         }

		// [AbpAuthorize(AppPermissions.Pages_UnitofMeasures_Edit)]
		 private async Task Update(CreateOrEditUnitofMeasureDto input)
         {
            var unitofMeasure = await _unitofMeasureRepository.FirstOrDefaultAsync((int)input.Id);
             ObjectMapper.Map(input, unitofMeasure);
         }

		// [AbpAuthorize(AppPermissions.Pages_UnitofMeasures_Delete)]
         public string Delete(EntityDto<int> Input)
         {
            var check = (from aa in _unitofMeasureRepository.GetAll() where (aa.Id == Input.Id && aa.CreatorUserId == 2) select aa.Id).ToList();

            if (check.Count == 0)
            {

                var QueryCheck = (from a in _ProgramExpenseRepository.GetAll() where (a.UnitOfMeasuresID == Input.Id) select a.Id).ToList();

                if (QueryCheck.Count == 0)
                {
                    _unitofMeasureRepository.DeleteAsync(Input.Id);

                    return "Record Deleted ";
                }
                else
                {
                    return "Record Exist";
                }
            }

            else
            {
                return "Record Exist";
            }

        }
    }
}