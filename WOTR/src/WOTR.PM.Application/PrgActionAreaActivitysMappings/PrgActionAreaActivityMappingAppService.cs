﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using DocumentFormat.OpenXml.Packaging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Authorization.Users.Dto;
using WOTR.PM.Components;
using WOTR.PM.Configuration;
using WOTR.PM.Dto;
using WOTR.PM.Locations;
using WOTR.PM.PrgActionAreaActivitysMappings.Dto;
using WOTR.PM.PrgRequestAAPlas;
using WOTR.PM.ProgramCostEstimations;
using WOTR.PM.ProgramFunds;
using WOTR.PM.Programs;
using System.Drawing;
using ShapeProperties = DocumentFormat.OpenXml.Drawing.Spreadsheet.ShapeProperties;
using Color = DocumentFormat.OpenXml.Spreadsheet.Color;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using Font = DocumentFormat.OpenXml.Spreadsheet.Font;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;

namespace WOTR.PM.PrgActionAreaActivitysMappings
{
    public class PrgActionAreaActivityMappingAppService : PMAppServiceBase, IPrgActionAreaActivityMappingAppService
    {
        private readonly IRepository<PrgActionAreaActivityMapping> _PrgActionAreaActivityMappingRepository;
        private readonly IPrgActionAreaActivityMappingRepository _PrgActionAreaActivityMappingSQLRepository;
        private readonly IRepository<prgActionAreaJSTree> _prgActionAreaJSTreeRepository;
        private readonly IRepository<ActionArea> _ActionAreaRepository;
        private readonly IRepository<SubActionArea> _SubActionAreaRepository;
        private readonly IRepository<Activity> _ActivityRepository;
        private readonly IRepository<Program> _programRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _rolesRepository;
        private readonly RoleManager _roleManager;
        private readonly IRepository<RequestAAplas> _requestAAplasRepository;
        private readonly IRepository<ProgramFund> _programFundRepository;
        private readonly IRepository<VillageCluster> _villageClusterRepository;
        private readonly IComponentActivityMappingRepository _ProgramComponentActivityMappingRepository;//...RepositoryToGetAllComponentAnd Activity details.
        private readonly IConfigurationRoot _appConfiguration;


        public PrgActionAreaActivityMappingAppService(IRepository<PrgActionAreaActivityMapping> PrgActionAreaActivityMappingRepository,
            IPrgActionAreaActivityMappingRepository PrgActionAreaActivityMappingSQLRepository,
            IRepository<prgActionAreaJSTree> prgActionAreaJSTreeRepository,
            IRepository<ActionArea> ActionAreaRepository,
            IRepository<SubActionArea> SubActionAreaRepository,
            IRepository<Activity> ActivityRepository,
            IRepository<Program> programRepository,
            IRepository<UserRole, long> userRoleRepository, IRepository<VillageCluster> villageClusterRepository,
            IRepository<Role> rolesRepository, RoleManager roleManager, IRepository<RequestAAplas> requestAAplasRepository, IRepository<ProgramFund> programFundRepository,
            IComponentActivityMappingRepository ProgramComponentActivityMappingRepository, IHostingEnvironment env)
        {
            _PrgActionAreaActivityMappingRepository = PrgActionAreaActivityMappingRepository;
            _PrgActionAreaActivityMappingSQLRepository = PrgActionAreaActivityMappingSQLRepository;
            _prgActionAreaJSTreeRepository = prgActionAreaJSTreeRepository;
            _ActionAreaRepository = ActionAreaRepository;
            _SubActionAreaRepository = SubActionAreaRepository;
            _ActivityRepository = ActivityRepository;
            _programRepository = programRepository;
            _userRoleRepository = userRoleRepository;
            _rolesRepository = rolesRepository;
            _roleManager = roleManager;
            _requestAAplasRepository = requestAAplasRepository;
            _programFundRepository = programFundRepository;
            _villageClusterRepository = villageClusterRepository;
            _ProgramComponentActivityMappingRepository = ProgramComponentActivityMappingRepository;
            _appConfiguration = env.GetAppConfiguration();


        }

        public async Task<List<string>> CreateOrUpdatePrgActivityMapping(List<PrgActionAreaActivityMappingRepositoryDto> Input)
        {
            var result = new List<string>();

            foreach (var item in Input)
            {

                if (item.Id == 0)
                {
                    foreach (var item1 in item.Activity.Where(t=>t.prgActionAreaID == 0 && t.ActionAreaID != null).ToList())
                    {
                                var PrgActionAreaActivityMapping = new PrgActionAreaActivityMapping();

                                PrgActionAreaActivityMapping.ActivityID = item1.ActivityID;
                                PrgActionAreaActivityMapping.ComponentID = item1.ComponentID;
                                PrgActionAreaActivityMapping.SubActionAreaID = item1.SubActionAreaID;
                                PrgActionAreaActivityMapping.ActionAreaID = item1.ActionAreaID;
                                PrgActionAreaActivityMapping.ProgramID = item.ProgramID;

                                var id = await _PrgActionAreaActivityMappingRepository.InsertAndGetIdAsync(PrgActionAreaActivityMapping);
                                if (id > 0)
                                    result.Add("Record Added");
                    }
                }
            }
            return result;
        }

        public async Task<UserRoleDto> GetUserRoleNameForEdit(NullableIdDto<long> input)
        {
            //Getting all available roles
            var userRoleDtos = await _roleManager.Roles
                .Select(r => new UserRoleDto
                {
                    RoleId = r.Id,
                    RoleName = r.Name,
                    RoleDisplayName = r.DisplayName
                })
                .FirstOrDefaultAsync(r => r.RoleId == input.Id);
            return userRoleDtos;

        }

        public List<PrgActionAreaActivityMappingRepositoryDto> GetAllprogramActionAreaActivity(int ProjectId)
        {
            var userId = AbpSession.UserId;
            var TenantId = AbpSession.TenantId;

            var result = new List<PrgActionAreaActivityMappingRepositoryDto>();

            var query = _PrgActionAreaActivityMappingSQLRepository.GetPrgActionAreaActivityMappingList(TenantId, userId, ProjectId);


            if (query != null)
            {
                var ListOfUsers = query.ToList().GroupBy(x => x.ComponentID)
                                      .Select(g => g.First())
                                      .ToList();

                foreach (var item in ListOfUsers)
                {
                    var ActivityActionandSubactionArea = new List<ActivityActionandSubactionArea>();


                    var query2 = query[0].Activity.Where(s => s.ComponentID == item.ComponentID).ToList();
                    var query3 = query[0].Activity.Where(s => s.ComponentID == item.ComponentID).ToList().GroupBy(x => x.ActivityID)
                              .Select(g => g.First())
                              .ToList();
                    foreach (var act in query3)
                    {
                        var actionSubActionList1 = new List<ActionSubActionList>();
                        foreach (var com in query2)
                        {
                            if (com.ActionAreaName != "")
                            {
                                if (act.ActivityID == com.ActivityID)
                                {
                                    actionSubActionList1.Add(new ActionSubActionList
                                    {
                                        ActionAreaID = com.ActionAreaID,
                                        ActionAreaName = com.ActionAreaName,
                                        SubActionAreaID = com.SubActionAreaID,
                                        SubActionAreaName = com.SubActionAreaName,
                                        ActivityID = com.ActivityID,
                                        ActivityName = com.ActivityName,
                                        subActionCode = com.subActionAreaCode,

                                    });
                                }
                            }
                        }

                        ActivityActionandSubactionArea.Add(new ActivityActionandSubactionArea
                        {
                            ActivityID = act.ActivityID,
                            ActivityName = act.ActivityName,
                            TotalUnitCost = act.TotalUnitCost,
                            UnitCost = act.UnitCost,
                            TotalUnits = act.TotalUnits,
                            ComponentID = act.ComponentID,
                            CostEstimationYear = act.CostEstimationYear,
                            ProgramID = item.ProgramID,
                            subActionAreaCode = act.subActionAreaCode,
                            ActionSubAction = actionSubActionList1,
                            prgActionAreaAcivityMapId =act.prgActionAreaAcivityMapId,
                            prgActionAreaID =act.prgActionAreaID

                        });

                    }


                    result.Add(new PrgActionAreaActivityMappingRepositoryDto
                    {
                        
                        ComponentID = item.ComponentID,
                        ComponentName = item.ComponentName,
                        ProgrameStartDate = item.ProgrameStartDate,
                        ProgrameEndDate = item.ProgrameEndDate,
                        ProgramID = item.ProgramID,
                        Activity = ActivityActionandSubactionArea

                    });
                }

            }
            return result;


        }

        public async Task<List<string>> CreateOrUpdateActivityActionAreaJSTree(prgActionAreaJSTreeDto Input)
        {

            var result = new List<string>();

            prgActionAreaJSTree funvalue = new prgActionAreaJSTree();

            int pranentID1, pranentID2;


            funvalue = Func(Input);



            if (Input.Id == 0)
            {
                var Query =await _prgActionAreaJSTreeRepository.GetAll().Where(p=>p.ActionAreaID == Input.ActionAreaID && p.ProgramID == Input.ProgramID).ToListAsync();
                             

                if (Query.Count != 0)
                {
                    foreach (var item in Query)
                    {
                        if (Input.ActionAreaID != item.ActionAreaID) //...nOT Equal
                        {
                            return null;

                        }
                        else
                        {
                            var Query1 = _prgActionAreaJSTreeRepository.GetAll().Where(p => p.SubActionAreaID == Input.SubActionAreaID && p.ProgramID == Input.ProgramID && p.ActionAreaID == null && p.ActivityID == null).ToList();
                                          
                            if (Query1.Count != 0)
                            {


                                foreach (var item1 in Query1)
                                {
                                    if (Input.SubActionAreaID != item1.SubActionAreaID) //...nOT Equal
                                    {
                                        return null;


                                    }
                                    else
                                    {
                                        var Query2 =_prgActionAreaJSTreeRepository.GetAll().Where(p => p.ActivityID == Input.ActivityID && p.ProgramID == Input.ProgramID && p.SubActionAreaID ==Input.SubActionAreaID).ToList();
                                                      

                                        if (Query2.Count != 0)
                                        {
                                            foreach (var item2 in Query2)
                                            {
                                                if (Input.ActivityID != item2.ActivityID)
                                                {
                                                    return null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            var Newactivity = new prgActionAreaJSTree();
                                            Newactivity = Func(Input);

                                            Newactivity.PranteId = Convert.ToInt32(item1.Id);
                                            Newactivity.ActionAreaID = null;
                                            Newactivity.Name = _ActivityRepository.FirstOrDefault(x => x.Id == Newactivity.ActivityID).Name;

                                           await _prgActionAreaJSTreeRepository.InsertAndGetIdAsync(Newactivity);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var odo = new prgActionAreaJSTree();
                                odo = Func(Input);
                                odo.ActivityID = null;
                                odo.ActionAreaID = null;
                                odo.PranteId = item.Id;
                                odo.Name = _SubActionAreaRepository.FirstOrDefault(x => x.Id == odo.SubActionAreaID).Name;
                                pranentID2 =await _prgActionAreaJSTreeRepository.InsertAndGetIdAsync(odo);

                                if (pranentID2 > 0)
                                {
                                    var uyu = new prgActionAreaJSTree();
                                    uyu = Func(Input);
                                    uyu.ActionAreaID = null;
                                    uyu.PranteId = Convert.ToInt32(pranentID2);
                                    uyu.Name = _ActivityRepository.FirstOrDefault(x => x.Id == uyu.ActivityID).Name;
                                    await _prgActionAreaJSTreeRepository.InsertAsync(uyu);
                                }

                            }
                        }

                    }
                }
                else
                {
                    funvalue.ActivityID = null;
                    funvalue.Name = _ActionAreaRepository.FirstOrDefault(x => x.Id == funvalue.ActionAreaID).Name;

                    pranentID1 =await _prgActionAreaJSTreeRepository.InsertAndGetIdAsync(funvalue);

                    if (pranentID1 > 0)
                    {
                        var odo = new prgActionAreaJSTree();
                        odo = Func(Input);
                        odo.PranteId = pranentID1;
                        odo.ActionAreaID = null;
                        odo.ActivityID = null;
                        odo.Name = _SubActionAreaRepository.FirstOrDefault(x => x.Id == odo.SubActionAreaID).Name;
                        pranentID2 = _prgActionAreaJSTreeRepository.InsertAndGetId(odo);

                        if (pranentID2 > 0)
                        {
                            prgActionAreaJSTree yoy = new prgActionAreaJSTree();
                            yoy = Func(Input);
                            yoy.PranteId = pranentID2;
                            yoy.ActionAreaID = null;
                            yoy.Name = _ActivityRepository.FirstOrDefault(x => x.Id == yoy.ActivityID).Name;
                            await _prgActionAreaJSTreeRepository.InsertAndGetIdAsync(yoy);

                        }
                    }
                }
            }
            return result;
        }

        public List<prgActionAreaJSTreeDto> GetAllJsTree(int? ProjectId)
        {
            var query = (from j in _prgActionAreaJSTreeRepository.GetAll().Where(a => a.ProgramID == ProjectId)
                         select new prgActionAreaJSTreeDto()
                         {
                             Id = j.Id,
                             PranteId = j.PranteId,

                             ActionAreaID = j.ActionAreaID,
                             SubActionAreaID = j.SubActionAreaID,

                             ActivityID = j.ActivityID,
                             Name = j.Name

                         }).ToList();
            return query;

        }


        public List<prgActionAreaJSTreeDto> GetAllProgrameComponentWithActivityJsTree(string Input, int user_id)
        {
            var userId = AbpSession.UserId;
            var Programs = new List<Program>();
            var ProgrameComponentActivityTree = new List<prgActionAreaJSTreeDto>();


            var usr = _userRoleRepository.GetAll().FirstOrDefault(u => u.UserId == userId);
            var role = _rolesRepository.GetAll().FirstOrDefault(u => u.Id == usr.RoleId);

            Programs = string.IsNullOrEmpty(Input) ? _programRepository.GetAll().ToList(): _programRepository.GetAll().Where(v => v.Name.Contains(Input)).ToList();
           
            if (role.Name == "ProgramManager")
            {
                Programs = Programs.Where(p => p.ProgramManagerID == userId || p.ManagerID == userId).ToList();
            }
            else if (role.Name == "ProjectManager" || role.Name == "ActionAreaManager")
            {
                //Changed by sonali On 18 Dec Wednesday 19
                var Pid = (from p in Programs
                            join ra in _requestAAplasRepository.GetAll()
                            on p.Id equals ra.ProgramID
                            where(ra.ManagerID == userId)
                            select p.Id).Distinct().ToList();

                Programs = _programRepository.GetAll().Where(xx=>Pid.Any(yy => yy == xx.Id)).ToList();




            }
            else if (role.Name == "RRcIncharge" || role.Name == "FieldStaffMember")
            {
                Programs = (from p in Programs
                            join vc in _villageClusterRepository.GetAll()
                            on p.Id equals vc.ProgramID
                            where (vc.UserId == userId)
                            select p).Distinct().ToList();
            }
            else if (role.Name == "ProgramManagerFinanceOrAdmin")
            {
                Programs = (from p in Programs
                            join r in _programFundRepository.GetAll()
                            on p.Id equals r.ProgramID
                            where (r.ProgramManagerID == userId)
                            select p).Distinct().ToList();
            }

            Programs.ForEach(item =>
            {
                     ProgrameComponentActivityTree.Add(
                        new prgActionAreaJSTreeDto()
                        {
                            Name = item.Name,
                            PrgComponentActivityId = item.Id,
                        });
            });

            ProgrameComponentActivityTree = ProgrameComponentActivityTree.Where(p => p != null).ToList();
            return ProgrameComponentActivityTree;
        }

        public List<prgActionAreaJSTreeDto> GetAllProgrameComponentByRoleId(string Input, int user_id,int RoleId)
        {
            var userId = AbpSession.UserId;
            if (RoleId == 0) {
                RoleId = 2;
            }
            var role = _rolesRepository.FirstOrDefault(u => u.Id == RoleId);

           var Programs = string.IsNullOrEmpty(Input) ? _programRepository.GetAll() : _programRepository.GetAll().Where(v => v.Name.Contains(Input));

            if (role.Name == "ProgramManager")
            {
                Programs = Programs.Where(p => p.ProgramManagerID == userId || p.ManagerID == userId);
            }
            else if (role.Name == "ProjectManager" || role.Name == "ActionAreaManager")
            {
                 Programs = (from p in Programs
                           join ra in _requestAAplasRepository.GetAll()
                           on p.Id equals ra.ProgramID
                           where (ra.ManagerID == userId)
                           select p).Distinct();
            }
            else if (role.Name == "RRcIncharge" || role.Name == "FieldStaffMember")
            {
                //Updated Query By Sachin On WednesDay 18 Dec 19
                Programs = (from p in Programs
                            join vc in _villageClusterRepository.GetAll()
                            on p.Id equals vc.ProgramID
                            where (vc.UserId == userId)
                            select p).Distinct();
            }
            else if (role.Name == "ProgramManagerFinanceOrAdmin")
            {
                Programs = (from p in Programs
                            join r in _programFundRepository.GetAll()
                            on p.Id equals r.ProgramID
                            where (r.ProgramManagerID == userId)
                            select p).Distinct();
            }

            var ProgrameComponentActivityTree = Programs.Select(t => new prgActionAreaJSTreeDto
            {
                Name = t.Name,
                PrgComponentActivityId = t.Id,
            });

            return ProgrameComponentActivityTree.ToList();
        }

        prgActionAreaJSTree Func(prgActionAreaJSTreeDto Input)
        {
            var prgActionAreaJSTree = new prgActionAreaJSTree();
            prgActionAreaJSTree.PranteId = Input.PranteId;
            prgActionAreaJSTree.ComponentID = Input.ComponentID;
            prgActionAreaJSTree.ActivityID = Input.ActivityID;
            prgActionAreaJSTree.ActionAreaID = Input.ActionAreaID;
            prgActionAreaJSTree.SubActionAreaID = Input.SubActionAreaID;
            prgActionAreaJSTree.ProgramID = Input.ProgramID;
            return prgActionAreaJSTree;
        }
        public async Task ConnectionConnect(SqlConnection conn)
        {
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    await conn.OpenAsync();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<List<ActionAreawaisedto>> ActionAreawisereport(int ActionAreaId,string costYear)
        {

            try
            {
                string value;
                var allChartData = new ActionAreawaisedto();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramYearwiseStateWiseBudgets = new List<ActionAreawaisedto>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdTwo = new SqlCommand("", conn))
                    {
                        //ProjectWiseInfoLineChart
                        var dataTable = new DataTable();
                        cmdTwo.CommandTimeout = 0;
                        cmdTwo.CommandText = "[dbo].[sp_ActionAreaWiseExcelReport]";
                        cmdTwo.CommandType = CommandType.StoredProcedure;
                        cmdTwo.Parameters.Add(new SqlParameter("@ActionAreaId", ActionAreaId));
                        cmdTwo.Parameters.Add(new SqlParameter("@CostEstimationYear", costYear));


                        SqlDataReader dr = cmdTwo.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerProgramYearwiseStateWiseBudgets.Add(new ActionAreawaisedto
                                    {
                                        ProgramName = (row["programName"] == DBNull.Value) ? "" : Convert.ToString(row["programName"]),
                                        SubActionAreaName = (row["SubactionAreaName"] == DBNull.Value) ? "" : Convert.ToString(row["SubactionAreaName"]),
                                        ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),
                                        TotalUnits= (row["TotalUnits"] == DBNull.Value) ? "" : Convert.ToString(row["TotalUnits"]),
                                        grantamount= (row["GrantAmount"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["GrantAmount"]),
                                        CommunityContribution= (row["community"] == DBNull.Value) ? "" : Convert.ToString(row["community"]),
                                        FunderContribution= (row["Funder"] == DBNull.Value) ? "" : Convert.ToString(row["Funder"]),
                                        OtherConstribution = (row["Other"] == DBNull.Value) ? "" : Convert.ToString(row["Other"]),
                                        Total= (row["Total"] == DBNull.Value) ? "" : Convert.ToString(row["Total"]),
                                    });
                                }

                            }
                        }
                        conn.Close();
                    }

                   
                }
                return programManagerProgramYearwiseStateWiseBudgets;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<FileDto> CreateExcelDoc(string fileName, int ActionAreaId,string year)
        {
            var result = await ActionAreawisereport(ActionAreaId,year);
            var ActionAReaName = _ActionAreaRepository.GetAll().Where(xx => xx.Id == ActionAreaId).FirstOrDefault().Name;
           
            var a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";
            var b = @"E:\Reports\";
            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))
            {
                var wbp = xl.AddWorkbookPart();
                var wsp = wbp.AddNewPart<WorksheetPart>();
                var wb = new Workbook();
                var fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                var ws = new Worksheet();
                var sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();
                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                var dp = wsp.AddNewPart<DrawingsPart>();
                var imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                var nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                var nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                var nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                var blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                var bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                var sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                var pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                var ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                var anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                var wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                var drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                var sheets = new Sheets();
                var sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                wb.Append(fv);

                // Constructing header
                var row1 = new Row();
                var row10 = new Row();
                var col1 = new Column();

                var row3 = new Row();
                var row5 = new Row();
                Row row6 = new Row();
                Row row7 = new Row();
                Row row8 = new Row();
                Row row4 = new Row();
                
                    var cell8 = InsertCellInWorksheet("D", 1, wsp);
                    cell8.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                    cell8.DataType = new EnumValue<CellValues>(CellValues.Number);

                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cell8.Append(inlineString);


                var cell91 = InsertCellInWorksheet("D", 2, wsp);
                cell91.CellValue = new CellValue("Overall Actionareawise Activitity sanctioned budget");
                cell91.DataType = new EnumValue<CellValues>(CellValues.Number);

                Run run2 = new Run();
                run2.Append(new Text("Overall Actionareawise Activitity sanctioned budget"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cell91.Append(inlineString1);


                var cell92 = InsertCellInWorksheet("D", 3, wsp);
                cell92.CellValue = new CellValue("Year: ");
                cell92.DataType = new EnumValue<CellValues>(CellValues.Number);
                var cell93 = InsertCellInWorksheet("E", 3, wsp);
                cell93.CellValue = new CellValue(year);
                cell93.DataType = new EnumValue<CellValues>(CellValues.Number);

                var cell81 = InsertCellInWorksheet("D", 4, wsp);
                cell81.CellValue = new CellValue("Action Area Name: ");
                cell81.DataType = new EnumValue<CellValues>(CellValues.Number);
                var cell9 = InsertCellInWorksheet("E", 4, wsp);
                cell9.CellValue = new CellValue(ActionAReaName);
                cell9.DataType = new EnumValue<CellValues>(CellValues.Number);


                row6 = new Row();
                row5 = new Row();
                row1 = new Row();
                row4 = new Row();
                row1.Append(ConstructCell(" ", CellValues.String, 2));
                sd.Append(row1);
                row6.Append(ConstructCell(" ", CellValues.String, 2));
                sd.Append(row6);
                row8.Append(ConstructCell(" ", CellValues.String, 2));
                sd.Append(row8);
                row7.Append(ConstructCell(" ", CellValues.String, 2));
                sd.Append(row7);


                row5.Append(
              ConstructCell("ProgramName", CellValues.String, 2),
               ConstructCell("Sub Action Area Name", CellValues.String, 2),
               ConstructCell("ActivityName", CellValues.String, 2),
               ConstructCell("Total Unit", CellValues.String, 2),
               ConstructCell("Project Cost(Grant)", CellValues.String, 2),
               ConstructCell("Project Cost CBOs", CellValues.String, 2),
               ConstructCell("Local Contribution", CellValues.String, 2),
               ConstructCell(" Govt. Contribution", CellValues.String, 2),
               ConstructCell("Total", CellValues.String, 2));
                sd.AppendChild(row5);

                var prevName = "";
                var SubAction = "";

                foreach (var imp in result)
                {
                    
                    var ProgramName = imp.ProgramName;
                    var subactionarea = imp.SubActionAreaName;
                    if (prevName != imp.ProgramName && SubAction!= imp.SubActionAreaName)
                    {
                        prevName = imp.ProgramName;
                        SubAction = imp.SubActionAreaName;
                        row4 = new Row();
                        row4.Append(
                           ConstructCell(imp.ProgramName.ToString(), CellValues.String, 1),
                            ConstructCell(imp.SubActionAreaName.ToString(), CellValues.String, 1),
                            ConstructCell(imp.ActivityName.ToString(), CellValues.Number, 1),
                            ConstructCell(imp.TotalUnits.ToString(), CellValues.Number, 1),
                            ConstructCell(Convert.ToDecimal(imp.grantamount).ToString("#,##0.00"), CellValues.Number, 1),
                            ConstructCell(Convert.ToDecimal(imp.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                            ConstructCell(Convert.ToDecimal(imp.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),
                            ConstructCell(Convert.ToDecimal(imp.OtherConstribution).ToString("#,##0.00"), CellValues.String, 1),
                                 ConstructCell(Convert.ToDecimal(imp.Total).ToString("#,##0.00"), CellValues.String, 1)
                            );
                        sd.AppendChild(row4);
                    }
                    else
                    {
                       if (imp.ProgramName == prevName && imp.SubActionAreaName==SubAction)
                        {
                            prevName = imp.ProgramName;
                            SubAction = imp.SubActionAreaName;
                            row4 = new Row();
                            row4.Append(
                               ConstructCell("", CellValues.String, 1),
                                ConstructCell("", CellValues.String, 1),
                                ConstructCell(imp.ActivityName.ToString(), CellValues.Number, 1),
                                ConstructCell(imp.TotalUnits.ToString(), CellValues.Number, 1),
                            ConstructCell(Convert.ToDecimal(imp.grantamount).ToString("#,##0.00"), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(imp.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(imp.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),
                              ConstructCell(Convert.ToDecimal(imp.OtherConstribution).ToString("#,##0.00"), CellValues.String, 1),
                                 ConstructCell(Convert.ToDecimal(imp.Total).ToString("#,##0.00"), CellValues.String, 1)
                                );
                            sd.AppendChild(row4);
                        }
                        else if(imp.ProgramName != prevName && imp.SubActionAreaName == SubAction)
                        {
                            prevName = imp.ProgramName;
                            SubAction = imp.SubActionAreaName;
                            row4 = new Row();
                            row4.Append(
                               ConstructCell(imp.ProgramName, CellValues.String, 1),
                                ConstructCell(imp.SubActionAreaName.ToString(), CellValues.String, 1),
                                ConstructCell(imp.ActivityName.ToString(), CellValues.Number, 1),
                                ConstructCell(imp.TotalUnits.ToString(), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(imp.grantamount).ToString("#,##0.00"), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(imp.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(imp.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),
                              ConstructCell(Convert.ToDecimal(imp.OtherConstribution).ToString("#,##0.00"), CellValues.String, 1),
                                 ConstructCell(Convert.ToDecimal(imp.Total).ToString("#,##0.00"), CellValues.String, 1)
                                );
                            sd.AppendChild(row4);
                        }
                        else if (imp.ProgramName == prevName && imp.SubActionAreaName != SubAction)
                        {
                            prevName = imp.ProgramName;
                            SubAction = imp.SubActionAreaName;
                            row4 = new Row();
                            row4.Append(
                               ConstructCell("", CellValues.String, 1),
                                ConstructCell(imp.SubActionAreaName.ToString(), CellValues.String, 1),
                                ConstructCell(imp.ActivityName.ToString(), CellValues.Number, 1),
                                ConstructCell(imp.TotalUnits.ToString(), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(imp.grantamount).ToString("#,##0.00"), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(imp.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                               ConstructCell(Convert.ToDecimal(imp.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),
                              ConstructCell(Convert.ToDecimal(imp.OtherConstribution).ToString("#,##0.00"), CellValues.String, 1),
                                 ConstructCell(Convert.ToDecimal(imp.Total).ToString("#,##0.00"), CellValues.String, 1)
                                );
                            sd.AppendChild(row4);
                        }
                    }
                              
                }
                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();
            }

            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;
        }
        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
        private Cell ConstructCell(string value, CellValues dataType, uint styleIndex = 0)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType),
                StyleIndex = styleIndex
            };
        }

        private Stylesheet GenerateStylesheet()
        {

            Stylesheet styleSheet = null;

            Fonts fonts = new Fonts(
                new Font( // Index 0 - default
                    new FontSize() { Val = 11 }

                ),
                new Font( // Index 1 - header
                    new FontSize() { Val = 11 },
                    new Bold(),
                    new Color() { Rgb = "000000" }

                ));

            Fills fills = new Fills(
                    new Fill(new PatternFill() { PatternType = PatternValues.None }), // Index 0 - default
                    new Fill(new PatternFill() { PatternType = PatternValues.Gray125 }), // Index 1 - default
                    new Fill(new PatternFill(new ForegroundColor { Rgb = new HexBinaryValue() { Value = "D0CECE" } })
                    { PatternType = PatternValues.Solid }) // Index 2 - header
                );

            Borders borders = new Borders(
                    new Border(), // index 0 default
                    new Border( // index 1 black border
                        new LeftBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new RightBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new TopBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new BottomBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                );

            CellFormats cellFormats = new CellFormats(
                    new CellFormat(), // default
                    new CellFormat { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }, // body
                    new CellFormat { FontId = 1, FillId = 2, BorderId = 1, ApplyFill = true } // header
                );

            styleSheet = new Stylesheet(fonts, fills, borders, cellFormats);

            return styleSheet;

        }

        public async Task<List<overallAnnualActionAreadto>> OverallAnnualActionAreawisereport(string Year)
        {

            try
            {
                string value;
                var allChartData = new overallAnnualActionAreadto();
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramYearwiseStateWiseBudgets = new List<overallAnnualActionAreadto>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdTwo = new SqlCommand("", conn))
                    {
                        //ProjectWiseInfoLineChart
                        var dataTable = new DataTable();
                        cmdTwo.CommandTimeout = 0;
                        cmdTwo.CommandText = "[dbo].[sp_OverallAnnualActionAreaWiseExcelReport]";
                        cmdTwo.CommandType = CommandType.StoredProcedure;
                        cmdTwo.Parameters.Add(new SqlParameter("@CostEstimationYear", Year));


                        SqlDataReader dr = cmdTwo.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerProgramYearwiseStateWiseBudgets.Add(new overallAnnualActionAreadto
                                    {
                                        ActionAreaName = (row["ActionAreaName"] == DBNull.Value) ? "" : Convert.ToString(row["ActionAreaName"]),
                                        grantamount = (row["GrantAmount"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["GrantAmount"]),
                                        CommunityContribution = (row["community"] == DBNull.Value) ? "" : Convert.ToString(row["community"]),
                                        FunderContribution = (row["Funder"] == DBNull.Value) ? "" : Convert.ToString(row["Funder"]),
                                        OtherConstribution = (row["Other"] == DBNull.Value) ? "" : Convert.ToString(row["Other"]),
                                        Total = (row["Total"] == DBNull.Value) ? "" : Convert.ToString(row["Total"]),
                                    });
                                }

                            }
                        }
                        conn.Close();
                    }


                }
                return programManagerProgramYearwiseStateWiseBudgets;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<FileDto> OverallAnnualActionCreateExcelDoc(string fileName, string Year)
        {
            var result = await OverallAnnualActionAreawisereport(Year);

            var a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";
            var b = @"E:\Reports\";
            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))
            {
                var wbp = xl.AddWorkbookPart();
                var wsp = wbp.AddNewPart<WorksheetPart>();
                var wb = new Workbook();
                var fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                var ws = new Worksheet();
                var sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();
                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                var dp = wsp.AddNewPart<DrawingsPart>();
                var imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                var nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                var nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                var nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                var blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                var bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                var sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                var pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                var ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                var anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                var wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                var drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                var sheets = new Sheets();
                var sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                wb.Append(fv);

                // Constructing header
                var row1 = new Row();
                var row10 = new Row();
                var col1 = new Column();

                var row3 = new Row();
                var row5 = new Row();
                Row row6 = new Row();
                Row row7 = new Row();
                Row row8 = new Row();
                Row row4 = new Row();

                var cell8 = InsertCellInWorksheet("D", 1, wsp);
                cell8.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                cell8.DataType = new EnumValue<CellValues>(CellValues.Number);

                var cellString8 = InsertCellInWorksheet("D", 1, wsp);

                cellString8.CellValue = new CellValue("Watershed Organisation Trust(WOTR)");
                cellString8.DataType = new EnumValue<CellValues>(CellValues.String);
                cell8.StyleIndex = 1;
                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cell8.Append(inlineString);

                Cell cell9 = InsertCellInWorksheet("D", 2, wsp);
                cell9.CellValue = new CellValue("Overall Annual Action Plan and Budget");
                cell9.DataType = new EnumValue<CellValues>(CellValues.Number);
                Run run2 = new Run();
                run2.Append(new Text("Overall Annual Action Plan and Budget"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cell9.Append(inlineString1);


                Cell cell13 = InsertCellInWorksheet("D", 3, wsp);
                cell13.CellValue = new CellValue("Year: " + Year);
                cell13.DataType = new EnumValue<CellValues>(CellValues.Number);

                row6 = new Row();
                row5 = new Row();
                row1 = new Row();
                row4 = new Row();
                row1.Append(ConstructCell(Year, CellValues.String, 2));
                sd.Append(row1);
                row6.Append(ConstructCell(" ", CellValues.String, 2));
                sd.Append(row6);
                row8.Append(ConstructCell(" ", CellValues.String, 2));
                sd.Append(row8);
                row7.Append(ConstructCell(" ", CellValues.String, 2));
                sd.Append(row7);


                row5.Append(
              ConstructCell("Action Area", CellValues.String, 2),
               ConstructCell("Project Cost(Grant)", CellValues.String, 2),
               ConstructCell("Project Cost CBOs", CellValues.String, 2),
               ConstructCell("Local Contribution", CellValues.String, 2),
               ConstructCell(" Govt. Contribution", CellValues.String, 2),
               ConstructCell("Total", CellValues.String, 2));
                sd.AppendChild(row5);

             
                foreach (var imp in result)
                {
                    row4 = new Row();
                    row4.Append(
                           ConstructCell(imp.ActionAreaName.ToString(), CellValues.String, 1),
                            ConstructCell(imp.grantamount.ToString(), CellValues.Number, 1),
                            ConstructCell(imp.CommunityContribution.ToString(), CellValues.Number, 1),
                            ConstructCell(imp.FunderContribution.ToString(), CellValues.Number, 1),
                            ConstructCell(imp.OtherConstribution, CellValues.String, 1),
                             ConstructCell(imp.Total, CellValues.String, 1)
                            );
                        sd.AppendChild(row4);
                    
                }
                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();
            }

            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;
        }

    }
}
