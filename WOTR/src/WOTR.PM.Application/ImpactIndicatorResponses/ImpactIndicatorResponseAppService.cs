﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WOTR.PM.NewImpactIndicator;
using WOTR.PM.NewImpactIndicator.IIResponses;
using WOTR.PM.NewImpactIndicator.IIResponses.Dto;

using System.Drawing;
using ShapeProperties = DocumentFormat.OpenXml.Drawing.Spreadsheet.ShapeProperties;
using Color = DocumentFormat.OpenXml.Spreadsheet.Color;
using Microsoft.EntityFrameworkCore.Internal;
using WOTR.PM.Locations;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services.Dto;
using WOTR.PM.Dto;
using System.Linq.Dynamic.Core;
using Abp.Linq.Extensions;
using WOTR.PM.Authorization.Users.Dto;
using Abp.Authorization.Users;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Programs;
using WOTR.PM.Programs.Dto;
using Abp.Domain.Repositories;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using System.IO;
using WOTR.PM.PrgRequestAAPlas;
using WOTR.PM.ProgramFunds;
using System.Collections;
using WOTR.PM.Authorization.Users;

//using Abp.Application.Services.Dto;
//using Microsoft.EntityFrameworkCore;

namespace WOTR.PM.ImpactIndicatorResponses
{
    public class ImpactIndicatorResponseAppService : PMAppServiceBase, IImpactIndicatorResponseAppService
    {
        private readonly IRepository<ImpactIndicatorCategory> _ImpactIndicatorCategoryRepository;
        private readonly IRepository<ImpactIndicatorSubCategory> _ImpactIndicatorSubCategoryRepository;
        private readonly IRepository<IIQuestionary> _IIQuestionaryRepository;
        private readonly IRepository<FrequencyOfoccurrence> _FrequencyOfoccurrenceRepository;
        private readonly IRepository<IIFormResponse, long> _IIFormResponseRepository;
        private readonly IRepository<ProjectAndIIFormMapping> _ProjectAndIIFormMappingRepository;
        private readonly IRepository<ImpactIndicatorForm> _ImpactIndicatorFormRepository;
        private readonly IRepository<IIFormQuestionMapping> _IIFormQuestionMappingRepository;
        private readonly IRepository<IIFormAnswer, long> _IIFormAnswerRepository;
        private readonly IRepository<VillageCluster> _VillageClusterRepository;
        private readonly IRepository<Village> _VillageRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _abproleRepository;
        private readonly IRepository<Program> _programRepository;
        private readonly IRepository<ProgramManagerMapping> _ProgramManagerMappingRepository;
        private readonly IRepository<Crop> _CropRepository;
        private readonly IRepository<Role> _rolesRepository;
        private readonly IRepository<RequestAAplas> _requestAAplasRepository;
        private readonly IRepository<ProgramFund> _programFundRepository;


        public ImpactIndicatorResponseAppService(IRepository<ImpactIndicatorCategory> impactIndicatorCategoryRepository,
            IRepository<ImpactIndicatorSubCategory> impactIndicatorSubCategoryRepository,
            IRepository<IIQuestionary> iIQuestionaryRepository, IRepository<FrequencyOfoccurrence> frequencyOfoccurrenceRepository,
            IRepository<IIFormResponse, long> iIFormResponseRepository, IRepository<ProjectAndIIFormMapping> projectAndIIFormMappingRepository,
            IRepository<ImpactIndicatorForm> impactIndicatorFormRepository, IRepository<IIFormQuestionMapping> iIFormQuestionMappingRepository
            , IRepository<IIFormAnswer, long> iIFormAnswerRepository, IRepository<VillageCluster> villageClusterRepository,
            IRepository<Village> villageRepository, IRepository<UserRole, long> userRoleRepository, IRepository<Role> abproleRepository,
           IRepository<Program> programRepository, IRepository<ProgramManagerMapping> ProgramManagerMappingRepository,
             IRepository<Crop> cropRepository,IRepository<Role> rolesRepository, IRepository<RequestAAplas> requestAAplasRepository,
             IRepository<ProgramFund> programFundRepository)
        {
            _ImpactIndicatorCategoryRepository = impactIndicatorCategoryRepository;
            _ImpactIndicatorSubCategoryRepository = impactIndicatorSubCategoryRepository;
            _IIQuestionaryRepository = iIQuestionaryRepository;
            _FrequencyOfoccurrenceRepository = frequencyOfoccurrenceRepository;
            _IIFormResponseRepository = iIFormResponseRepository;
            _ProjectAndIIFormMappingRepository = projectAndIIFormMappingRepository;
            _ImpactIndicatorFormRepository = impactIndicatorFormRepository;
            _IIFormQuestionMappingRepository = iIFormQuestionMappingRepository;
            _IIFormAnswerRepository = iIFormAnswerRepository;
            _VillageClusterRepository = villageClusterRepository;
            _VillageRepository = villageRepository;
            _programRepository = programRepository;
            _ProgramManagerMappingRepository = ProgramManagerMappingRepository;
            _CropRepository = cropRepository;
            _userRoleRepository = userRoleRepository; _rolesRepository = rolesRepository;
             _abproleRepository = abproleRepository; _requestAAplasRepository = requestAAplasRepository;
            _programFundRepository = programFundRepository;
        }

        public async Task<List<FrequencyOccurenceDto>> GetAllFormResponseDetailsByProgramId(int ProgramId, int? IIFormResponseId)
        {
            var FormResponseDetailsList = new List<FrequencyOccurenceDto>();

            var FormId = await _ProjectAndIIFormMappingRepository.FirstOrDefaultAsync(x => x.programId == ProgramId && x.Status == "Live");

            var fromAns = _IIFormAnswerRepository.GetAll();

            if (FormId != null)
            {
                if (!IIFormResponseId.HasValue)
                {
                    FormResponseDetailsList = (from i in _IIFormQuestionMappingRepository.GetAll().Include(t => t.iIQuestionary).ThenInclude(x => x.impactIndicatorSubCategory).Include(t => t.iIQuestionary).ThenInclude(x => x.impactIndicatorCategory)
                                 .Where(t => t.impactIndicatorFormId == FormId.impactIndicatorFormId)
                                               group new { i } by i.iIQuestionary.frequencyOfOccurrence into g
                                               select new FrequencyOccurenceDto()
                                               {
                                                   FrequencyOfOccurrenceId = g.Key.Id,
                                                   FrequencyOfOccurrenceName = g.Key.FreqOfOccurrence,
                                                   EntryDate = DateTime.Now,
                                                   CollectedDate = DateTime.Now,
                                                   CategoryList = g.Select(t => t.i.iIQuestionary).Where(t => t.FrequencyOfOccurrenceId == g.Key.Id).GroupBy(t => t.impactIndicatorCategory).Select(t => new GetAllCategoryFromForm()
                                                   {
                                                       CategoryId = t.Key.Id,
                                                       Category = t.Key.Category,
                                                       SubCategoryList = g.Select(y => y.i.iIQuestionary).Where(y => y.ImpactIndicatorCategoryId == t.Key.Id).GroupBy(y => y.impactIndicatorSubCategory).Select(y => new GetAllSubCategoryFromForm()
                                                       {
                                                           SubCategory = y.Key.SubCategory,
                                                           SubCategoryId = y.Key.Id,
                                                           QuestionTypesList = g.Select(z => z.i).Where(z => z.iIQuestionary.ImpactIndicatorSubCategoryId == y.Key.Id).GroupBy(z => z.impactIndicatorFormId).Select(z => new GetQuestionType()
                                                           {
                                                               Type = 0,
                                                               DateOfEntry = DateTime.Now,
                                                               DateOfCollection = DateTime.Now,
                                                               SourceId = 0,
                                                               QuestionList = z.Select(b => b.iIQuestionary).Select(b => new GetAllQuestionsFromForm()
                                                               {
                                                                   IIQuestionaryId = b.Id,
                                                                   Question = b.Question,
                                                                   QuestionType = b.QuestionType,
                                                                   Answer = "",
                                                                   SourceId = 0,
                                                                   IIFormResponseId = 0
                                                               }).OrderBy(l => l.Question).ToList()
                                                           }).ToList()
                                                           //QuestionsList = g.Select(z => z.i.iIQuestionary).Where(z => z.ImpactIndicatorSubCategoryId == y.Key.Id).Select(z => new GetAllQuestionsFromForm()
                                                           //{
                                                           //    IIQuestionaryId = z.Id,
                                                           //    Question = z.Question,
                                                           //    QuestionType = z.QuestionType,
                                                           //    Answer = string.Empty,
                                                           //    SourceId = 0,
                                                           //    IIFormResponseId = 0
                                                           //}).ToList()
                                                       }).OrderBy(m=> m.SubCategory).ToList()
                                                   }).OrderBy(n=>n.Category).ToList()
                                               }).ToList();
                }
                else
                {
                    FormResponseDetailsList = (from i in _IIFormAnswerRepository.GetAll().Include(t => t.IIQuestionary).ThenInclude(x => x.impactIndicatorSubCategory).Include(t => t.IIQuestionary).ThenInclude(x => x.impactIndicatorCategory)
                                 .Where(t => t.IIFormResponseId == IIFormResponseId)
                                               group new { i } by i.FrequencyOfoccurrence into g
                                               select new FrequencyOccurenceDto()
                                               {
                                                   FrequencyOfOccurrenceId = g.Key.Id,
                                                   FrequencyOfOccurrenceName = g.Key.FreqOfOccurrence,
                                                   EntryDate = g.Select(t => t.i.DateOfEntry).FirstOrDefault(),
                                                   CollectedDate = g.Select(t => t.i.DateOfCollection).FirstOrDefault(),
                                                   CategoryList = g.Select(t => t.i.IIQuestionary).Where(t => t.FrequencyOfOccurrenceId == g.Key.Id).GroupBy(t => t.impactIndicatorCategory).Select(t => new GetAllCategoryFromForm()
                                                   {
                                                       CategoryId = t.Key.Id,
                                                       Category = t.Key.Category,
                                                       SubCategoryList = g.Select(y => y.i.IIQuestionary).Where(y => y.ImpactIndicatorCategoryId == t.Key.Id).GroupBy(y => y.impactIndicatorSubCategory).Select(y => new GetAllSubCategoryFromForm()
                                                       {
                                                           SubCategory = y.Key.SubCategory,
                                                           SubCategoryId = y.Key.Id,
                                                           QuestionTypesList = g.Select(z => z.i).Where(z => z.IIQuestionary.ImpactIndicatorSubCategoryId == y.Key.Id).GroupBy(z => z.QuestionType).Select(z => new GetQuestionType()
                                                           {
                                                               Type = z.Key,
                                                               DateOfEntry = z.Select(d => d.DateOfEntry).FirstOrDefault(),
                                                               DateOfCollection = z.Select(d => d.DateOfCollection).FirstOrDefault(),
                                                               SourceId = z.Select(d => d.SourceId).FirstOrDefault(),
                                                               CropId = z.Select(d => d.CropId).FirstOrDefault(),
                                                               QuestionList = z.Select(b => b.IIQuestionary).Select(b => new GetAllQuestionsFromForm()
                                                               {
                                                                   IIQuestionaryId = b.Id,
                                                                   Question = b.Question,
                                                                   QuestionType = b.QuestionType,
                                                                   Answer = z.Where(c => c.IIQuestionaryId == b.Id).Select(c => c.Answer).FirstOrDefault(),
                                                                   SourceId = z.Where(d => d.IIQuestionaryId == b.Id).Select(d => d.SourceId).FirstOrDefault(),
                                                                   IIFormResponseId = z.Where(d => d.IIQuestionaryId == b.Id).Select(d => d.IIFormResponseId).FirstOrDefault(),
                                                                   Id = z.Where(d => d.IIQuestionaryId == b.Id).Select(d => d.Id).FirstOrDefault()
                                                               }).OrderBy(l => l.Question).ToList()
                                                           }).ToList()
                                                       }).OrderBy(m => m.SubCategory).ToList()
                                                   }).OrderBy(n => n.Category).ToList()
                                               }).ToList();
                }

                return FormResponseDetailsList;

            }
            else
            {
                return FormResponseDetailsList;
            }

            //try
            //{
            //    var FormResponseDetailsList = new List<GetAllFrequencyOfOccurrence>();

            //    var FormId = await _ProjectAndIIFormMappingRepository.FirstOrDefaultAsync(x => x.programId == ProgramId && x.Status == "Live");

            //    if (FormId != null)
            //    {
            //        var id = FormId.impactIndicatorFormId;

            //        var questionList = _IIFormQuestionMappingRepository.GetAll().Where(x => x.impactIndicatorFormId == FormId.impactIndicatorFormId).Select(xx => xx.iIQuestionaryId);

            //        var QuestionDetails = _IIQuestionaryRepository.GetAll()
            //                                    .Where(xx => xx.QuestionStatus == "Active" && questionList.Any(yy => yy == xx.Id)).OrderBy(z => z.FrequencyOfOccurrenceId);

            //        var Categories = _ImpactIndicatorCategoryRepository.GetAll();

            //        var SubCategories = _ImpactIndicatorSubCategoryRepository.GetAll();

            //        var FormRespoinseDetails = (from a in QuestionDetails
            //                                    group a by new
            //                                    {
            //                                        a.FrequencyOfOccurrenceId,
            //                                        //   a.ImpactIndicatorCategoryId,
            //                                    } into d
            //                                    join foo in _FrequencyOfoccurrenceRepository.GetAll() on d.Key.FrequencyOfOccurrenceId equals foo.Id
            //                                    select new GetAllFrequencyOfOccurrence()
            //                                    {
            //                                        FreqOfOccurrenceId = (int)d.Key.FrequencyOfOccurrenceId,
            //                                        FreqOfOccurrence = foo.FreqOfOccurrence,

            //                                        CategoryList = (from b in Categories.Where(p => (from y in QuestionDetails where y.FrequencyOfOccurrenceId == d.Key.FrequencyOfOccurrenceId select y.ImpactIndicatorCategoryId).Any(yy => yy == p.Id))
            //                                                        group b by new
            //                                                        {
            //                                                            b.Id,
            //                                                            b.Category
            //                                                        } into cat
            //                                                        select new GetAllCategoryFromForm()
            //                                                        {
            //                                                            CategoryId = cat.Key.Id,
            //                                                            Category = cat.Key.Category,
            //                                                            SubCategoryList = (from c in SubCategories.Where(rr => (from z in QuestionDetails where z.FrequencyOfOccurrenceId == d.Key.FrequencyOfOccurrenceId && z.ImpactIndicatorCategoryId == cat.Key.Id select z.ImpactIndicatorSubCategoryId).Any(kk => kk == rr.Id))
            //                                                                               group c by new
            //                                                                               {
            //                                                                                   c.Id,
            //                                                                                   c.SubCategory
            //                                                                               } into SubCat
            //                                                                               select new GetAllSubCategoryFromForm()
            //                                                                               {
            //                                                                                   SubCategoryId = SubCat.Key.Id,
            //                                                                                   SubCategory = SubCat.Key.SubCategory,
            //                                                                                   QuestionsList = (from e in QuestionDetails
            //                                                                                                    .Where(ss => (from z in QuestionDetails
            //                                                                                                                  where z.FrequencyOfOccurrenceId == d.Key.FrequencyOfOccurrenceId && z.ImpactIndicatorSubCategoryId == SubCat.Key.Id
            //                                                                                                                  select z.Id)
            //                                                                                                     .Any(kk => kk == ss.Id))
            //                                                                                                    group e by new
            //                                                                                                    {
            //                                                                                                        e.Id,
            //                                                                                                        e.Question,
            //                                                                                                        e.QuestionType
            //                                                                                                    } into Que
            //                                                                                                    select new GetAllQuestionsFromForm()
            //                                                                                                    {
            //                                                                                                        IIQuestionaryId = Que.Key.Id,
            //                                                                                                        Question = Que.Key.Question,
            //                                                                                                        QuestionType = Que.Key.QuestionType,
            //                                                                                                        Answer = IIFormResponseId.HasValue ? _IIFormAnswerRepository.GetAll().Where(x=> x.IIFormResponseId == IIFormResponseId && x.IIQuestionaryId == Que.Key.Id).Select(x=> x.Answer ).FirstOrDefault(): string.Empty,
            //                                                                                                        SourceId = IIFormResponseId.HasValue ? _IIFormAnswerRepository.GetAll().Where(x=> x.IIFormResponseId == IIFormResponseId && x.IIQuestionaryId == Que.Key.Id).Select(x=> x.SourceId ).FirstOrDefault(): 0,
            //                                                                                                        DateOfEntryStr = IIFormResponseId.HasValue ? _IIFormAnswerRepository.GetAll().Where(x=> x.IIFormResponseId == IIFormResponseId && x.IIQuestionaryId == Que.Key.Id).Select(x=> x.DateOfEntry.ToString("yyyy-MM-dd") ).FirstOrDefault(): DateTime.Now.ToString("yyyy-MM-dd"),
            //                                                                                                        DateOfCollectionStr = IIFormResponseId.HasValue ? _IIFormAnswerRepository.GetAll().Where(x => x.IIFormResponseId == IIFormResponseId && x.IIQuestionaryId == Que.Key.Id).Select(x => x.DateOfCollection.ToString("yyyy-MM-dd")).FirstOrDefault() : DateTime.Now.ToString("yyyy-MM-dd"),
            //                                                                                                        IIFormResponseId = (int)(IIFormResponseId.HasValue ? _IIFormAnswerRepository.GetAll().Where(x => x.IIFormResponseId == IIFormResponseId && x.IIQuestionaryId == Que.Key.Id).Select(x => x.Id).FirstOrDefault() : 0)
            //                                                                                                    }).ToList()
            //                                                                               }).ToList()
            //                                                        }).ToList()
            //                                    }).ToList();

            //        return FormRespoinseDetails;
            //    }
            //    else
            //    {
            //        return FormResponseDetailsList;
            //    }
            //}
            //catch (Exception ex)
            //{

            //    throw;
            //}
        }

        public async Task<PagedResultDto<UserListDto>> GetProgramUsers(int? programId)
        {
            var query = _VillageClusterRepository.GetAll().Where(vc => vc.ProgramID == programId).Select(t => t.User).Distinct();

            var userId = AbpSession.UserId;
            var prg = _programRepository.GetAll();

            var usr = await _userRoleRepository.FirstOrDefaultAsync(u => u.UserId == userId);
            var role = _rolesRepository.FirstOrDefault(u => u.Id == usr.RoleId);


            var users = await query
                .ToListAsync();
            if (role.Name == "Admin" || role.Name == "ADMIN") {
                users.Add(new User
                {
                    Id = 2,
                    Name = "admin",
                    Surname = " "
                    
                });
            }
            var userCount = users.Count();
            var userListDtos = ObjectMapper.Map<List<UserListDto>>(users);

            return new PagedResultDto<UserListDto>(
                userCount,
                userListDtos
                );
        }

        public string SaveImpactIndicatorResponse(AddFormResponseDto input)
        {
            string returnMsg = "";

            var username = GetCurrentUser();


            var FormResponse = new IIFormResponse();
            FormResponse.Id = input.Id;
            FormResponse.TenantId = (int)username.TenantId;
            FormResponse.CreatorUserId = (int)username.Id;
            FormResponse.ProgramId = input.ProgramId;
            FormResponse.villageId = input.villageId;
            FormResponse.DataFillPersonId = input.DataFillPersonId;
            FormResponse.impactIndicatorFormId = input.impactIndicatorFormId;
            var FormResponseId = _IIFormResponseRepository.InsertOrUpdateAndGetId(FormResponse);


            if (input.FrequencyOccurenceList.Count > 0)
            {
                foreach (var item2 in input.FrequencyOccurenceList)
                {
                    foreach (var item in item2.CategoryList)
                    {
                        foreach (var item1 in item.SubCategoryList)
                        {
                            foreach (var item3 in item1.QuestionTypesList)
                            {
                                foreach (var i in item3.QuestionList)
                                {
                                    var IIFormAnswers = new IIFormAnswer();
                                    IIFormAnswers.Id = i.Id;
                                    IIFormAnswers.IIFormResponseId = FormResponseId;
                                    IIFormAnswers.IIQuestionaryId = i.IIQuestionaryId;
                                    IIFormAnswers.Answer = i.Answer;
                                    IIFormAnswers.QuestionType = item3.Type;

                                    IIFormAnswers.FrequencyOfoccurrenceId = item2.FrequencyOfOccurrenceId;
                                    if (item2.FrequencyOfOccurrenceName == "Continuous")
                                    {
                                        IIFormAnswers.DateOfCollection = item3.DateOfCollection;
                                        IIFormAnswers.DateOfEntry = item3.DateOfEntry;
                                        IIFormAnswers.SourceId = item3.SourceId == 0 ? 1 : item3.SourceId;
                                        IIFormAnswers.CropId = item3.CropId;
                                    }
                                    else
                                    {
                                        IIFormAnswers.DateOfCollection = item2.CollectedDate;
                                        IIFormAnswers.DateOfEntry = item2.EntryDate;
                                        IIFormAnswers.SourceId = i.SourceId == 0 ? 1 : i.SourceId;
                                    }

                                    _IIFormAnswerRepository.InsertOrUpdate(IIFormAnswers);
                                }
                            }
                        }
                    }
                }
            }

            returnMsg = "Response Added  sucessfully !";
            return returnMsg;
        }

        public List<Village> GetVillagesByProgramIdForResponse(int ProgramId)
        {
            var VillageList = new List<Village>();

            try
            {
                if (ProgramId != 0)
                {
                    var VillageIds = _VillageClusterRepository.GetAll().Where(x => x.ProgramID == ProgramId).Select(x => x.VillageID);
                    VillageList = _VillageRepository.GetAll().Where(x => VillageIds.Any(i => i == x.Id)).ToList();
                    return VillageList;
                }
                else
                {
                    return VillageList;
                }
            }
            catch (Exception)
            {

                throw;
            }


        }

        public List<Village> GetVillagesByProgramIdForReport(int ProgramId)
        {
            var VillageList = new List<Village>();

            try
            {
                if (ProgramId != 0)
                {
                    var VillageIds = _VillageClusterRepository.GetAll().Where(x => x.ProgramID == ProgramId).Select(x => x.VillageID);
                    VillageList = _VillageRepository.GetAll().Where(x => VillageIds.Any(i => i == x.Id)).ToList();
                    return VillageList;
                }
                else
                {
                    return VillageList;
                }
            }
            catch (Exception)
            {

                throw;
            }


        }

        public async Task<AddFormResponseDto> GetResponseById(EntityDto input)
        {

            var query = await _IIFormResponseRepository.GetAll().Include(x => x.iIFormAnswers).Where(z => z.Id == input.Id)
                    .Select(x => new AddFormResponseDto
                    {
                        Id = (int)x.Id,
                        ProgramId = x.ProgramId,
                        villageId = x.villageId,
                        DataFillPersonId = x.DataFillPersonId,
                        //AddFormResponseDetailsDtoList = ObjectMapper.Map<List<AddFormResponseDetailsDto>>(x.iIFormAnswers)

                    }).FirstOrDefaultAsync();


            return query;
        }

        public async Task<PagedResultDto<GetAllFormResponseGridView>> GetResponseForGrid(PagedSortedAndFilteredInputDto input)
        {

            var userId = AbpSession.UserId;
            var prg = await _programRepository.GetAll().ToListAsync();

            var usr = await _userRoleRepository.FirstOrDefaultAsync(u => u.UserId == userId);
            var role = _rolesRepository.FirstOrDefault(u => u.Id == usr.RoleId);


            if (role.Name == "ProgramManager")
            {
                prg = (from p in prg
                           // join r in _requestAAplasRepository.GetAll()
                           // on p.Id equals r.ProgramID
                       where (p.ProgramManagerID == userId)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "Admin" || role.Name == "ADMIN")
            {
                prg = (from p in prg
                           //join r in _requestAAplasRepository.GetAll()
                           //on p.Id equals r.ProgramID
                           //where (r.RequestStatus == RequestStatus.RequestSend)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "ProjectManager" || role.Name == "ActionAreaManager")
            {
                prg = (from p in prg
                       join ra in _requestAAplasRepository.GetAll()
                           on p.Id equals ra.ProgramID
                       where (p.ManagerID == userId || ra.ManagerID == userId)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "RRcIncharge" || role.Name == "FieldStaffMember")
            {
                prg = (from p in prg
                       join vc in _VillageClusterRepository.GetAll()
                      on p.Id equals vc.ProgramID
                       // join r in _requestAAplasRepository.GetAll()
                       //  on p.Id equals r.ProgramID
                       where (vc.UserId == userId) //&& r.RequestStatus == RequestStatus.RequestSend)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "ProgramManagerFinanceOrAdmin")
            {
                prg = (from p in prg

                       where (p.ProgramManagerID == userId)
                       select p).Distinct().ToList();
            }


            var pIds = prg.Select(x => x.Id).ToList();


            var query = _IIFormResponseRepository.GetAll().Where(x => string.IsNullOrEmpty(input.Filter)
                                                                    || x.program.Name.Contains(input.Filter)
                                                                    || x.village.Name.Contains(input.Filter)
                                                                    || x.DataFillPerson.Name.Contains(input.Filter))
                                                          .Select(z => new GetAllFormResponseGridView
                                                          {
                                                              ProgramId = z.ProgramId,
                                                              IIFormResponseId = z.Id,
                                                              Name = z.program.Name,
                                                              Village = z.village.Name,
                                                              UserName = z.DataFillPerson.Name + " " + z.DataFillPerson.Surname
                                                          });
            var query1 = query.Where(z => pIds.Any(p => p == z.ProgramId));
            var totalCount = await query1.CountAsync();

            var responseList = await query1.OrderBy(input.Sorting ?? "IIFormResponseId desc").PageBy(input).ToListAsync();

            return new PagedResultDto<GetAllFormResponseGridView>(
                totalCount,
               responseList
                );

        }

       

        public async Task<string> DeleteFormAnswer(long input)
        {
            await _IIFormAnswerRepository.DeleteAsync(input);
            return "";
        }

        public void DeleteFormResponse(EntityDto input)
        {

            _IIFormResponseRepository.DeleteAsync(x => x.Id == input.Id);

            _IIFormAnswerRepository.DeleteAsync(x => x.IIFormResponseId == input.Id);


        }

        public  List<GetUserDetailsForMailDto> GetProgramAndProjectManagerByProgramId(int ProgramId)
        {
            var UserList = new List<int>();

            var query = (from p in _programRepository.GetAll()
                         where (p.Id == ProgramId)
                         join pmr in _ProgramManagerMappingRepository.GetAll()
                         on p.Id equals pmr.ProgramID

                         select new ProgramInformationListDto()
                         {
                             Id = ProgramId,
                             ProgramName = p.Name,
                             ShortDescription = p.ShortDescription,
                             LongDescription = p.LongDescription,
                             DonorID = p.DonorID,
                             ProgramSanctionedYear = p.ProgramSanctionedYear,
                             GantnAmountSanctionRupess = p.GantnAmountSanctionRupess,
                             ProgramStartDate = p.ProgramStartDate,
                             PrgramEndDate = p.PrgramEndDate,
                             ProgramSubmitDate = p.ProgramSubmitDate,
                             ProgramApprovaltDate = p.ProgramApprovaltDate,
                             ProgramManagerId = p.ManagerID,
                             ProgramBadgeImage = p.ProgramBadgeImage,
                             ProgramBadgeColor = p.ProgramBadgeColor,
                             CurrentProgramManagerID = p.ProgramManagerID
                         }).FirstOrDefault();

            UserList.Add(query.ProgramManagerId);
            UserList.Add((int)query.CurrentProgramManagerID);

            var query1 = (from u in UserManager.Users.Distinct()
                          join ur in _userRoleRepository.GetAll() on u.Id equals ur.UserId
                          join r in _abproleRepository.GetAll() on ur.RoleId equals r.Id
                          where (UserList.Any(x => x == u.Id))
                          select  new GetUserDetailsForMailDto()
                          {
                              Name = u.FullName,
                              EmailId = u.EmailAddress
                          }).OrderBy(r => r.Name).Distinct().ToList();
            var query2 = query1.Distinct().ToList();
           
            var query3 = query2.GroupBy(car => car.EmailId).Select(g => g.First()).ToList();
            return  query3;
        }

        public List<ImpactIndicatorForm> GetFormsByProgramId(int ProgramId)
        {

            var FormIds = _IIFormResponseRepository.GetAll().Where(x => x.ProgramId == ProgramId).Select(z => z.impactIndicatorFormId);

            var FormDetails = _ImpactIndicatorFormRepository.GetAll().Where(x => FormIds.Any(z => z == x.Id)).ToList();

            return FormDetails;
        }

        // Report
        #region Report

        public async Task<List<ProgramInformationListDto>> GetAllProgramForReport()
        {
            var userId = AbpSession.UserId;
            var usr = _userRoleRepository.GetAll().FirstOrDefault(u => u.UserId == userId);
            var role = _rolesRepository.GetAll().FirstOrDefault(u => u.Id == usr.RoleId);
            var prg = await _programRepository.GetAll().ToListAsync();

            if (role.Name == "ProgramManager")
            {
                prg = (from p in prg
                           // join r in _requestAAplasRepository.GetAll()
                           // on p.Id equals r.ProgramID
                       where (p.ProgramManagerID == userId)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "Admin" || role.Name == "ADMIN")
            {
                prg = (from p in prg
                           //join r in _requestAAplasRepository.GetAll()
                           //on p.Id equals r.ProgramID
                           //where (r.RequestStatus == RequestStatus.RequestSend)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "ProjectManager" || role.Name == "ActionAreaManager")
            {
                prg = (from p in prg
                       join ra in _requestAAplasRepository.GetAll()
                           on p.Id equals ra.ProgramID
                       where (p.ManagerID == userId || ra.ManagerID == userId)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "RRcIncharge" || role.Name == "FieldStaffMember")
            {
                prg = (from p in prg
                       join vc in _VillageClusterRepository.GetAll()
                      on p.Id equals vc.ProgramID
                      // join r in _requestAAplasRepository.GetAll()
                     //  on p.Id equals r.ProgramID
                       where (vc.UserId == userId ) //&& r.RequestStatus == RequestStatus.RequestSend)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "ProgramManagerFinanceOrAdmin")
            {
                prg = (from p in prg

                       where (p.ProgramManagerID == userId)
                       select p).Distinct().ToList();
            }

            var query = (from p in prg
                         select new ProgramInformationListDto()
                         {
                             ProgramName = p.Name,
                             ShortDescription = p.ShortDescription,
                             LongDescription = p.LongDescription,
                             DonorID = p.DonorID,
                             ProgramSanctionedYear = p.ProgramSanctionedYear,
                             ProgramStartDate = p.ProgramStartDate,
                             PrgramEndDate = p.PrgramEndDate,
                             Id = p.Id
                         }).ToList();
            return query;
        }

        public List<ImpactIndicatorForm> GetFormsForReportByProgramIdAndVillageId(int ProgramId, List<int> VillageId)
        {

            var FormIds = _IIFormResponseRepository.GetAll().Where(x => x.ProgramId == ProgramId && VillageId.Any(z => z == x.villageId)).Select(z => z.impactIndicatorFormId);

            var FormDetails = _ImpactIndicatorFormRepository.GetAll().Where(x => FormIds.Any(z => z == x.Id)).ToList();

            return FormDetails;
        }

        public void GetImpactIndicatorReportToExcel(ProjectReportInputDto input)
        {

            var FormResponses = _IIFormResponseRepository.GetAll().Where(z => z.ProgramId == input.ProgramId && input.VillageIds.Any(x => x == z.villageId));
        }

        public async Task<int> GetImpactIndicatorFormByProgramId(int ProgramId)
        {
           

            var FormId = await _ProjectAndIIFormMappingRepository.FirstOrDefaultAsync(x => x.programId == ProgramId && x.Status == "Live");
            if (FormId == null) {
                return 0;
            }
            else {
            return  (int)FormId.impactIndicatorFormId;
            }
        }

        public async Task<List<string>> GetVillageNamesById(List<int> VillageIds) {

            var VillageName =  _VillageRepository.GetAll().Where(x => VillageIds.Contains(x.Id)).Select(z => z.Name).ToList();

            return VillageName;
        }

        public async Task<Village> GetVillageDeatilsByVillageId(int VillageId)
        {

            var VillageDetails = _VillageRepository.FirstOrDefault(x => x.Id == VillageId);

            return VillageDetails;
        }

        public async Task<FileDto> CreateImpactIndicatorReportExcelDoc(string fileName, ProjectReportInputDto input)
        {
          

            var programName = _programRepository.FirstOrDefault(x => x.Id == input.ProgramId).Name;
            var villageName = _VillageRepository.FirstOrDefault(x => x.Id == input.VillageIds[0]).Name;

            var result =  GetImpactIndicatorDetailsForReport(input);
            string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";

            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();

                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                BlipFill blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                Bitmap bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                ShapeProperties sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                Position pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                Extent ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                AbsoluteAnchor anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                WorksheetDrawing wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                // sheets.Append(sheet);
                wb.Append(fv);

                Row row = new Row();
                Row row1 = new Row();
                Row row2 = new Row();
                Row row3 = new Row();
                Row row4 = new Row();
                Row row5 = new Row();
                Row row6 = new Row();
                Row row7 = new Row();
                Row row8 = new Row();
                Row row9 = new Row();
                Row row10 = new Row();

                Cell cell11 = InsertCellInWorksheet("D", 1, wsp);
                cell11.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                cell11.DataType = new EnumValue<CellValues>(CellValues.Number);

                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cell11.Append(inlineString);

                Cell cell1 = InsertCellInWorksheet("D", 2, wsp);
                cell1.CellValue = new CellValue("Impact Indicator Report");
                cell1.DataType = new EnumValue<CellValues>(CellValues.Number);

                Run run2 = new Run();
                run2.Append(new Text("Impact Indicator Report"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cell1.Append(inlineString1);

                Cell cell12 = InsertCellInWorksheet("D", 3, wsp);
                cell12.CellValue = new CellValue("Program Title: " + programName);
                cell12.DataType = new EnumValue<CellValues>(CellValues.Number);

                Cell cell13 = InsertCellInWorksheet("D", 4, wsp);
                cell13.CellValue = new CellValue("Village Name: " + villageName);
                cell13.DataType = new EnumValue<CellValues>(CellValues.Number);

                row3.Append(

                    ConstructCell(" ", CellValues.String));

                sd.AppendChild(row3);

                //bind Data here


                foreach (var imp in result)
                {




                    row3 = new Row();
                    row3.Append(

                        ConstructCell("", CellValues.String, 1));
                    sd.AppendChild(row3);

                    row2 = new Row();

                    row2.Append(

                        ConstructCell("Freuency Of Occurance :" + imp.FreqOfOccurrence.ToString(), CellValues.String, 1));
                    sd.AppendChild(row2);

                    row6 = new Row();
                    row5 = new Row();
                    row1 = new Row();
                    row8 = new Row();
                    row9 = new Row();
                    row10 = new Row();

                    if (imp.FreqOfOccurrence == "Pre-Intervention" || imp.FreqOfOccurrence == "Post-Intervention")
                    {
                        row9 = new Row();
                        row9.Append(
                        ConstructCell("Date Of Entry :" + String.Format("{0:M/d/yyyy}", imp.answerDetails[0].EntryDate) , CellValues.String, 1));
                        sd.AppendChild(row9);

                        row10 = new Row();
                        row10.Append(
                        ConstructCell("Date Of Collection :" + String.Format("{0:M/d/yyyy}", imp.answerDetails[0].DateOfCollection) , CellValues.String, 1));
                        sd.AppendChild(row10);

                    }

                    if (imp.FreqOfOccurrence == "Continuous")
                    {
                        row5 = new Row();
                        row5.Append(
                   ConstructCell("Village", CellValues.String, 2),
                   ConstructCell("Category", CellValues.String, 2),
                   ConstructCell("SubCategory", CellValues.String, 2),
                   ConstructCell("Question", CellValues.String, 2),
                   ConstructCell("Crop", CellValues.String, 2),
                   ConstructCell("Answer", CellValues.String, 2),
                   ConstructCell("Date Of Entry", CellValues.String, 2));
                        sd.AppendChild(row5);


                        foreach (var imp1 in imp.answerDetails)
                        {

                            row8 = new Row();
                            row8.Append(
                                ConstructCell(imp1.Village.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Category.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Subcategory.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Question == null ? " ": imp1.Question.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Crop == null ? " " : imp1.Crop.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Answer == null ? " ":imp1.Answer.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.EntryDate == null ? " " : String.Format("{0:M/d/yyyy}", imp1.EntryDate), CellValues.String, 1)
                                );
                            sd.AppendChild(row8);
                        }
                    }
                    else { 
                     row5 = new Row();
                            row5.Append(
                       ConstructCell("Village", CellValues.String, 2),
                       ConstructCell("Category", CellValues.String, 2),
                       ConstructCell("SubCategory", CellValues.String, 2),
                       ConstructCell("Question", CellValues.String, 2),
                       ConstructCell("Answer", CellValues.String, 2));
                    sd.AppendChild(row5);

                    
                    foreach (var imp1 in imp.answerDetails)
                       {
                       
                        row8 = new Row();
                            row8.Append(
                                ConstructCell(imp1.Village.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Category.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Subcategory.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Question.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Answer == null ? " " : imp1.Answer.ToString(), CellValues.String, 1)
                                );
                            sd.AppendChild(row8);
                       }
                    }
                   
                }


                //bind data here
                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();
            }

            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;
        }

        public async Task<FileDto> CreateImpactIndicatorReportExcelDocVersion1(string fileName, ProjectReportInputDto input)
        {


            var programName = _programRepository.FirstOrDefault(x => x.Id == input.ProgramId).Name;
            var villageName = _VillageRepository.FirstOrDefault(x => x.Id == input.VillageIds[0]).Name;

            var result = GetImpactIndicatorDetailsForReport(input);
            string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";

            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();

                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                BlipFill blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                Bitmap bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                ShapeProperties sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                Position pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                Extent ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                AbsoluteAnchor anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                WorksheetDrawing wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                // sheets.Append(sheet);
                wb.Append(fv);

                Row row = new Row();
                Row row1 = new Row();
                Row row2 = new Row();
                Row row3 = new Row();
                Row row4 = new Row();
                Row row5 = new Row();
                Row row6 = new Row();
                Row row7 = new Row();
                Row row8 = new Row();
                Row row9 = new Row();
                Row row10 = new Row();

                Cell cell11 = InsertCellInWorksheet("D", 1, wsp);
                cell11.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                cell11.DataType = new EnumValue<CellValues>(CellValues.Number);

                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cell11.Append(inlineString);

                Cell cell1 = InsertCellInWorksheet("D", 2, wsp);
                cell1.CellValue = new CellValue("Impact Indicator Report");
                cell1.DataType = new EnumValue<CellValues>(CellValues.Number);

                Run run2 = new Run();
                run2.Append(new Text("Impact Indicator Report"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cell1.Append(inlineString1);

                Cell cell12 = InsertCellInWorksheet("D", 3, wsp);
                cell12.CellValue = new CellValue("Program Title: " + programName);
                cell12.DataType = new EnumValue<CellValues>(CellValues.Number);

                Cell cell13 = InsertCellInWorksheet("D", 4, wsp);
                cell13.CellValue = new CellValue("Village Name: " + villageName);
                cell13.DataType = new EnumValue<CellValues>(CellValues.Number);

                row3.Append(

                    ConstructCell(" ", CellValues.String));

                sd.AppendChild(row3);

                //bind Data here

                ArrayList postAnswers = new ArrayList();
                foreach (var imp in result)
                {
                    if (imp.FreqOfOccurrence == "Post-Intervention") {
                        foreach (var imp1 in imp.answerDetails)
                        {
                            postAnswers.Add(imp1.Answer);
                                
                        }
                    }

                }

                foreach (var imp in result)
                {
                    row9 = new Row();
                    row10 = new Row();

                    if (imp.FreqOfOccurrence == "Pre-Intervention" || imp.FreqOfOccurrence == "Post-Intervention")
                    {
                        row4 = new Row();

                        row4.Append(

                            ConstructCell("Freuency Of Occurance :" + imp.FreqOfOccurrence.ToString(), CellValues.String, 1));
                        sd.AppendChild(row4);

                        row9 = new Row();
                        row9.Append(
                        ConstructCell("Date Of Entry :" + String.Format("{0:M/d/yyyy}", imp.answerDetails[0].EntryDate), CellValues.String, 1));
                        sd.AppendChild(row9);

                        row10 = new Row();
                        row10.Append(
                        ConstructCell("Date Of Collection :" + String.Format("{0:M/d/yyyy}", imp.answerDetails[0].DateOfCollection), CellValues.String, 1));
                        sd.AppendChild(row10);
                    }
                }



                foreach (var imp in result)
                {

                    row3 = new Row();
                    row3.Append(

                        ConstructCell("", CellValues.String, 1));
                    sd.AppendChild(row3);

                   

                    row6 = new Row();
                    row5 = new Row();
                    row1 = new Row();
                    row8 = new Row();
                    


                    if (imp.FreqOfOccurrence == "Continuous")
                    {
                        row2 = new Row();

                        row2.Append(

                            ConstructCell("Freuency Of Occurance :" + imp.FreqOfOccurrence.ToString(), CellValues.String, 1));
                        sd.AppendChild(row2);

                        row5 = new Row();
                        row5.Append(
                   ConstructCell("Village", CellValues.String, 2),
                   ConstructCell("Category", CellValues.String, 2),
                   ConstructCell("SubCategory", CellValues.String, 2),
                   ConstructCell("Question", CellValues.String, 2),
                   ConstructCell("Crop", CellValues.String, 2),
                   ConstructCell("Answer", CellValues.String, 2),
                   ConstructCell("Date Of Entry", CellValues.String, 2));
                        sd.AppendChild(row5);


                        foreach (var imp1 in imp.answerDetails)
                        {

                            row8 = new Row();
                            row8.Append(
                                ConstructCell(imp1.Village.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Category.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Subcategory.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Question == null ? " " : imp1.Question.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Crop == null ? " " : imp1.Crop.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Answer == null ? " " : imp1.Answer.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.EntryDate == null ? " " : String.Format("{0:M/d/yyyy}", imp1.EntryDate), CellValues.String, 1)
                                );
                            sd.AppendChild(row8);
                        }
                    }
                    else if(imp.FreqOfOccurrence == "Pre-Intervention")
                    {
                        row5 = new Row();
                        row5.Append(
                   ConstructCell("Village", CellValues.String, 2),
                   ConstructCell("Category", CellValues.String, 2),
                   ConstructCell("SubCategory", CellValues.String, 2),
                   ConstructCell("Question", CellValues.String, 2),
                   ConstructCell("Pre", CellValues.String, 2),
                   ConstructCell("Post", CellValues.String, 2));
                        sd.AppendChild(row5);

                        var i = 0;
                        foreach (var imp1 in imp.answerDetails)
                        {

                            row8 = new Row();
                            row8.Append(
                                ConstructCell(imp1.Village.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Category.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Subcategory.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Question.ToString(), CellValues.String, 1),
                                ConstructCell(imp1.Answer == null ? " " : imp1.Answer.ToString(), CellValues.String, 1),
                                ConstructCell(postAnswers[i] == null ? " " : postAnswers[i].ToString(), CellValues.String, 1)
                                );
                            sd.AppendChild(row8);
                            i++;
                        }
                    }

                }


                //bind data here
                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();
            }

            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;
        }


        public List<GetAllResponseReportDto> GetImpactIndicatorDetailsForReport(ProjectReportInputDto input)
        {
             var result = (from ans in _IIFormAnswerRepository.GetAll().Include(t => t.IIFormResponse).ThenInclude(a => a.village).Include(t => t.FrequencyOfoccurrence)
                         .Include(t => t.IIQuestionary).ThenInclude(a => a.impactIndicatorCategory).Include(t => t.IIQuestionary).ThenInclude(a => a.impactIndicatorSubCategory)
                         .Include(t=>t.Crop)
                         where (ans.IIFormResponse.ProgramId == input.ProgramId && ans.IIFormResponse.impactIndicatorFormId == input.FormId && input.VillageIds.Any(z=> z == ans.IIFormResponse.villageId))
                         group ans by ans.FrequencyOfoccurrence into g
                         select new GetAllResponseReportDto()
                         {
                             FreqOfOccurrence = g.Key.FreqOfOccurrence,
                             answerDetails = g.Select(x => new GetAllAnswersReportDto()
                             {
                                 Village = x.IIFormResponse.village.Name,
                                 Category = x.IIQuestionary.impactIndicatorCategory.Category,
                                 Subcategory = x.IIQuestionary.impactIndicatorSubCategory.SubCategory,
                                 Question = x.IIQuestionary.Question,
                                 Crop = x.CropId == null ? string.Empty : x.Crop.Name,
                                 Answer = x.Answer,
                                 EntryDate = x.DateOfEntry,
                                 DateOfCollection = x.DateOfCollection,
                             }).ToList()
                         });

            return result.ToList();

        }

        private Stylesheet GenerateStylesheet()
        {
            Stylesheet styleSheet = null;

            Fonts fonts = new Fonts(
                new DocumentFormat.OpenXml.Spreadsheet.Font( // Index 0 - default
                    new FontSize() { Val = 11 }

                ),
                new DocumentFormat.OpenXml.Spreadsheet.Font( // Index 1 - header
                    new FontSize() { Val = 11 },
                    new Bold(),
                    new Color() { Rgb = "000000" }
                ));

            Fills fills = new Fills(
                    new Fill(new PatternFill() { PatternType = PatternValues.None }), // Index 0 - default
                    new Fill(new PatternFill() { PatternType = PatternValues.LightGray }), // Index 1 - default
                    new Fill(new PatternFill(new ForegroundColor { Rgb = new HexBinaryValue() { Value = "D0CECE" } })
                    { PatternType = PatternValues.Solid }) // Index 2 - header

                );

            Borders borders = new Borders(
                    new Border(), // index 0 default
                    new Border( // index 1 black border
                        new LeftBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new RightBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new TopBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new BottomBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                );

            CellFormats cellFormats = new CellFormats(
                    new CellFormat(), // default
                    new CellFormat { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }, // body
                    new CellFormat { FontId = 1, FillId = 2, BorderId = 1, ApplyFill = true } // header
                );

            styleSheet = new Stylesheet(fonts, fills, borders, cellFormats);

            return styleSheet;
        }

        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }

        private Cell ConstructCell(string value, CellValues dataType, uint styleIndex = 0)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType),
                StyleIndex = styleIndex
            };
        }
        #endregion
    }
}
