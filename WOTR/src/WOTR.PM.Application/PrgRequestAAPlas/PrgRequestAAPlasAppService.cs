﻿using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Authorization.Users.Dto;
using WOTR.PM.Dto;
using WOTR.PM.EntityFrameworkCore.Repositories;
using WOTR.PM.Locations;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.PrgImplementationPlans.Dto;
using WOTR.PM.PrgQuarterUnits;
using WOTR.PM.PrgRequestAAPlas.Dto;
using WOTR.PM.ProgramCostEstimations;
using WOTR.PM.ProgramExpenses;
using WOTR.PM.ProgramRegionSpendings;
using WOTR.PM.Programs;
using WOTR.PM.ProgramRegionSpendings.Dto;

using WOTR.PM.UnitOfMeasures;
//archita

using System.Drawing;
using ShapeProperties = DocumentFormat.OpenXml.Drawing.Spreadsheet.ShapeProperties;
using Color = DocumentFormat.OpenXml.Spreadsheet.Color;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using Font = DocumentFormat.OpenXml.Spreadsheet.Font;
using System.IO;
using Abp.Application.Services.Dto;
using WOTR.PM.Programs.Dto;
using WOTR.PM.Authorization.Users;
using Microsoft.EntityFrameworkCore;
using WOTR.PM.Checklists;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using WOTR.PM.Configuration;
using WOTR.PM.PrgImplementationPlans;
using WOTR.PM.ProgramFunds;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Microsoft.EntityFrameworkCore.Internal;
using Abp.UI;

//archita
namespace WOTR.PM.PrgRequestAAPlas
{
    public class PrgRequestAAPlasAppService : PMAppServiceBase, IPrgRequestAAPlasAppService
    {
        private readonly IRepository<Program> _programRepository;
        private readonly IRepository<RequestAAplas> _RequestAAplasRepository;
        private readonly IRepository<PrgActionAreaActivityMapping> _PrgActionAreaActivityMappingRepository;
        private readonly IRepository<Locations.Location> _LocationRepository;
        private readonly IRepository<SubActionArea> _SubActionAreaRepository;
        private readonly IRepository<ProgramCostEstimation> _PrgCostEstRepository;
        private readonly IRepository<Program> _ProgrameRepository;
        private readonly IRepository<Role> _abproleRepository;
        private RequestApplsRepository _RequestApplsSQLRepository;
        private IRepository<VillageCluster> _villageClusterRepository;
        private IRepository<Village> _villageRepository;
        private IRepository<ProgramQuqterUnitMapping> _ProgramQuqterUnitMappingRepository;
        private readonly IRepository<ProgramExpense> _ProgramExpenseRepository;
        private readonly IRepository<ImplementationPlanCheckList> _implementationPlanCheckListRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IConfigurationRoot _appConfiguration;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _rolesRepository;
        private readonly IRepository<ProgramFund> _programFundRepository;


        public PrgRequestAAPlasAppService(IRepository<Program> programRepository, IRepository<RequestAAplas> RequestAAplasRepository, IRepository<UserRole, long> userRoleRepository,
        IRepository<PrgActionAreaActivityMapping> PrgActionAreaActivityMappingRepository,
            IRepository<Locations.Location> LocationRepository,
            IRepository<VillageCluster> villageClusterRepository,
            IRepository<Village> villageRepository,
            IRepository<SubActionArea> SubActionAreaRepository,
            IRepository<Role> abproleRepository,
            IRepository<ProgramCostEstimation> PrgCostEstRepository,
            IRepository<Program> ProgrameRepository,
            IRepository<ProgramQuqterUnitMapping> ProgramQuqterUnitMappingRepository,
            IRepository<User, long> userRepository, IRepository<ImplementationPlanCheckList> implementationPlanCheckListRepository,
            RequestApplsRepository RequestApplsSQLRepository, IRepository<ProgramExpense> ProgramExpenseRepository, IHostingEnvironment env, IRepository<Role> rolesRepository, IRepository<ProgramFund> programFundRepository)
        {
            _userRoleRepository = userRoleRepository;
            _programRepository = programRepository;
            _RequestAAplasRepository = RequestAAplasRepository;
            _PrgActionAreaActivityMappingRepository = PrgActionAreaActivityMappingRepository;
            _LocationRepository = LocationRepository;
            _villageClusterRepository = villageClusterRepository;
            _villageRepository = villageRepository;
            _SubActionAreaRepository = SubActionAreaRepository;
            _PrgCostEstRepository = PrgCostEstRepository;
            _ProgrameRepository = ProgrameRepository;
            _RequestApplsSQLRepository = RequestApplsSQLRepository;
            _abproleRepository = abproleRepository;
            _ProgramQuqterUnitMappingRepository = ProgramQuqterUnitMappingRepository;
            _ProgramExpenseRepository = ProgramExpenseRepository;
            _userRepository = userRepository;
            _implementationPlanCheckListRepository = implementationPlanCheckListRepository;
            _appConfiguration = env.GetAppConfiguration();
            _rolesRepository = rolesRepository;
            _programFundRepository = programFundRepository;
        }

        public async Task<ProgDto> CreatOrUpdateRequestAppl(ProgrameCostEstimationYearRequestApplDto Input)
        {
            var tenantId = AbpSession.TenantId;
            try
            {
                var managerId = (long)AbpSession.UserId;
                var program = await _programRepository.FirstOrDefaultAsync(pr => pr.Id == Input.ProgramID);
                foreach (var item1 in Input.ActionSubactionCost)
                {
                    RequestAAplas RequestAAplas = new RequestAAplas();
                    RequestAAplas.ProgramID = Input.ProgramID;
                    RequestAAplas.LocationID = Input.LocationID;


                    RequestAAplas.ManagerID = item1.ActivityLevel == ActivityLevel.ProgramLevel ? managerId : Input.ManagerID;
                    RequestAAplas.CostEstimationYear = Input.CostEstimationYear;
                    RequestAAplas.UnitOfMeasuresID = item1.UnitOfMeasuresID;

                    RequestAAplas.PrgActionAreaActivityMappingID = item1.PrgActionAreaActivityMappingID;
                    RequestAAplas.VillageID = item1.VillageID;
                    RequestAAplas.TotalUnits = item1.TotalUnits;
                    RequestAAplas.UnitCost = item1.UnitCost;
                    RequestAAplas.TotalUnitCost = item1.TotalUnitCost;
                    RequestAAplas.CommunityContribution = item1.CommunityContribution;


                    RequestAAplas.FunderContribution = item1.FunderContribution;
                    RequestAAplas.OtherContribution = item1.OtherContribution;
                    RequestAAplas.Quarter1PhysicalUnits = item1.Quarter1PhysicalUnits;
                    RequestAAplas.Quarter1FinancialRs = item1.Quarter1FinancialRs;
                    RequestAAplas.Quarter2PhysicalUnits = item1.Quarter2PhysicalUnits;
                    RequestAAplas.Quarter2FinancialRs = item1.Quarter2FinancialRs;
                    RequestAAplas.Quarter3PhysicalUnits = item1.Quarter3PhysicalUnits;
                    RequestAAplas.Quarter3FinancialRs = item1.Quarter3FinancialRs;
                    RequestAAplas.Quarter4PhysicalUnits = item1.Quarter4PhysicalUnits;
                    RequestAAplas.Quarter4FinancialRs = item1.Quarter4FinancialRs;
                    RequestAAplas.Id = item1.Id;
                    RequestAAplas.TenantId = (int)tenantId;
                    if (item1.Id == 0)
                    {
                        if (item1.RequestStatus == RequestStatus.RequestSend) { RequestAAplas.RequestStatus = RequestStatus.planRecieved; }
                        else if (item1.RequestStatus == RequestStatus.RequestNotSend) { RequestAAplas.RequestStatus = RequestStatus.RequestSend; }
                        else { RequestAAplas.RequestStatus = RequestStatus.planRecieved; };

                        await _RequestAAplasRepository.InsertAsync(RequestAAplas);
                    }
                    else
                    {
                        if (item1.RequestStatus == RequestStatus.planRecieved) { RequestAAplas.RequestStatus = RequestStatus.Approved; }
                        else if (item1.RequestStatus == RequestStatus.ChangeRequest) { RequestAAplas.RequestStatus = RequestStatus.ChangeRequest; }
                        else { RequestAAplas.RequestStatus = RequestStatus.planRecieved; }

                        await _RequestAAplasRepository.UpdateAsync(RequestAAplas);
                    }
                    foreach (var item2 in item1.SubActionSubActionActivity)
                    {
                        RequestAAplas RequestAAplas1 = new RequestAAplas();
                        RequestAAplas1.ProgramID = Input.ProgramID;
                        RequestAAplas1.LocationID = Input.LocationID;

                        RequestAAplas1.ManagerID = item1.ActivityLevel == ActivityLevel.ProgramLevel ? managerId : Input.ManagerID;
                        RequestAAplas1.CostEstimationYear = Input.CostEstimationYear;
                        RequestAAplas1.UnitOfMeasuresID = item1.UnitOfMeasuresID;
                        RequestAAplas1.PrgActionAreaActivityMappingID = item2.PrgActionAreaActivityMappingID;
                        RequestAAplas1.VillageID = item2.VillageID;
                        RequestAAplas1.TotalUnits = item2.TotalUnits;
                        RequestAAplas1.UnitCost = item2.UnitCost;
                        RequestAAplas1.TotalUnitCost = item2.TotalUnitCost;
                        RequestAAplas1.CommunityContribution = item2.CommunityContribution;
                        RequestAAplas1.FunderContribution = item2.FunderContribution;
                        RequestAAplas1.OtherContribution = item2.OtherContribution;
                        RequestAAplas1.Quarter1PhysicalUnits = item2.Quarter1PhysicalUnits;
                        RequestAAplas1.Quarter1FinancialRs = item2.Quarter1FinancialRs;
                        RequestAAplas1.Quarter2PhysicalUnits = item2.Quarter2PhysicalUnits;
                        RequestAAplas1.Quarter2FinancialRs = item2.Quarter2FinancialRs;
                        RequestAAplas1.Quarter3PhysicalUnits = item2.Quarter3PhysicalUnits;
                        RequestAAplas1.Quarter3FinancialRs = item2.Quarter3FinancialRs;
                        RequestAAplas1.Quarter4PhysicalUnits = item2.Quarter4PhysicalUnits;
                        RequestAAplas1.Quarter4FinancialRs = item2.Quarter4FinancialRs;
                        RequestAAplas1.Id = item2.Id;
                        if (item2.Id == 0)
                        {
                            try
                            {
                                if (item1.RequestStatus == RequestStatus.RequestSend) { RequestAAplas1.RequestStatus = RequestStatus.planRecieved; }
                                else if (item1.RequestStatus == RequestStatus.RequestNotSend) { RequestAAplas1.RequestStatus = RequestStatus.RequestSend; }
                                else { RequestAAplas1.RequestStatus = RequestStatus.planRecieved; }

                                await _RequestAAplasRepository.InsertAsync(RequestAAplas1);
                            }
                            catch (Exception)
                            {
                                throw;
                            }
                        }
                        else
                        {
                            try
                            {
                                if (item1.RequestStatus == RequestStatus.planRecieved) { RequestAAplas1.RequestStatus = RequestStatus.Approved; }
                                else if (item1.RequestStatus == RequestStatus.ChangeRequest) { RequestAAplas1.RequestStatus = RequestStatus.ChangeRequest; }
                                else { RequestAAplas1.RequestStatus = RequestStatus.planRecieved; }

                                RequestAAplas1.TenantId = (int)AbpSession.TenantId;

                                await _RequestAAplasRepository.UpdateAsync(RequestAAplas1);
                            }
                            catch (Exception)
                            {
                                throw;
                            }
                        }

                    }
                }
                if (Input.ManagerID == 0)
                {
                    Input.ManagerID = managerId;
                }
                var query = UserManager.Users.Where(m => m.Id == Input.ManagerID).ToList();
                var ProgDtoInfo = new ProgDto();
                ProgDtoInfo.Name = program.Name;
                ProgDtoInfo.ManagerName = _userRepository.GetAll().IgnoreQueryFilters().Where(u => u.Id == Input.ManagerID).Select(u => u.Name + " " + u.Surname).FirstOrDefault();
                ProgDtoInfo.Users = new List<UserListDto>(ObjectMapper.Map<List<UserListDto>>(query));
                return ProgDtoInfo;

            }
            catch (Exception e)
            {
                throw;
            }

        }

        public async Task CreatePrgUnitMapping(ListActionSubactionListImplemenTationDto input)
        {
            if (input.UnitId == 0)
            {
                var ProgramQuqterUnitMappings = new ProgramQuqterUnitMapping();
                ProgramQuqterUnitMappings.NosOfUnit = (int)input.Quarter1PhysicalUnits;
                ProgramQuqterUnitMappings.PrgActionAreaActivityMappingID = input.PrgActionAreaActivityMappingID;
                ProgramQuqterUnitMappings.ProgramID = input.ProgramID;
                ProgramQuqterUnitMappings.Rs = (decimal)input.Quarter1FinancialRs;
                ProgramQuqterUnitMappings.ProgramQuaterYear = input.CostEstimationYear;
                ProgramQuqterUnitMappings.QuarterId = input.QuarterId;
                ProgramQuqterUnitMappings.Status = ProgramQuarterUnitMappingStatus.Inprogress;
                ProgramQuqterUnitMappings.VillageID = input.VillageID;
                await _ProgramQuqterUnitMappingRepository.InsertAsync(ProgramQuqterUnitMappings);
            }
            else
            {
                var query = _ProgramQuqterUnitMappingRepository.GetAll().Where(pu => pu.Id == input.UnitId).ToList();
                foreach (var item in query)
                {
                    item.NosOfUnit = (int)input.Quarter1PhysicalUnits;
                    item.Rs = input.Quarter1FinancialRs;
                    await _ProgramQuqterUnitMappingRepository.UpdateAsync(item);
                }

            }
        }

        public async Task<List<RequestAAPlasListDto>> GetRequestApplaDeatils(int ProgramId)
        {
            try
            {

                int? TenantId = AbpSession.TenantId;
                RequestStatus p;
                var result = new List<RequestAAPlasListDto>();
                var results = new List<RequestAAPlasListDto>();


                var Query = _RequestApplsSQLRepository.GetRquestApplData(TenantId, ProgramId);

                if (Query != null)
                {
                    var listOfLocation = Query.ToList().GroupBy(x => x.LocationID)
                                          .Select(g => g.First())
                                          .ToList();

                    var dates = new List<string>();

                    long Start = 0;
                    long end = 0;
                    if (Query[0].ProgrameStartDate.Month < 4)
                    {
                        Start = Query[0].ProgrameStartDate.Year - 1;
                        end = Query[0].ProgrameStartDate.Year;
                    }
                    else
                    {
                        Start = Query[0].ProgrameStartDate.Year;
                        end = Query[0].ProgrameStartDate.Year + 1;
                    }

                    for (var dt = Start; dt <= Query[0].ProgrameEndDate.Year; dt = dt + 1)
                    {
                        var from = DateTime.Parse("April 1," + (Start));
                        var to = DateTime.Parse("March 31, " + end);
                        if (Query[0].ProgrameEndDate.Month > to.Month || Query[0].ProgrameEndDate.Year > from.Year)
                        {
                            dates.Add(Start.ToString() + "-" + end.ToString());
                        }
                        Start++;
                        end++;
                    }

                    var listOfUsers = _villageClusterRepository.GetAll().Where(vc => vc.ProgramID == ProgramId).ToList().GroupBy(x => x.UserId)
                                       .Select(g => g.First())
                                       .ToList();


                    foreach (var item in listOfLocation)
                    {
                        int? ProjectmanagerId = 0;
                        int? LocationWiseId = 0;
                        var obj1 = new List<ProgrameCostEstimationYearRequestApplDto>();

                        var obj = new List<ListActionSubactionListDto>();
                        var subUnitActivity = new List<ListActionSubactionListDto>();

                        var ManagersList = new List<PrgManagerListDto>();

                        var xyz = Query[0].CostEstimationYear[0].ActionSubactionCost
                            .Where(a => a.LocationID != 0 && a.LocationID == item.LocationID).ToList().GroupBy(x => new { x.ActivityName, x.CostEstimationYear }).ToList();
                        List<ListActionSubactionListDto> result11 = new List<ListActionSubactionListDto>();
                        xyz.ForEach(obj11 =>
                        {
                            result11.Add(obj11.FirstOrDefault());
                        });

                        //var D1 = xyz.Distinct(car => car.CarCode);
                        foreach (var act in result11)
                        {
                            var abc = UserManager.Users.Any(ppp => ppp.ActionId == act.ActionId);

                            if (abc == false)
                            {
                                obj.Add(act);
                            }
                        }

                        var PreviousYears = new List<string>();

                        foreach (var item2 in dates)
                        {
                            var i = new List<ListActionSubactionListDto>();

                            foreach (var u in obj.Where(t => t.CostEstimationYear == item2.ToString()))
                            {
                                long? previousUnits = 0;
                                decimal? previousRs = 0;
                                foreach (var yr in PreviousYears)
                                {
                                    previousUnits += obj.FirstOrDefault(pu => pu.PrgActionAreaActivityMappingID == u.PrgActionAreaActivityMappingID && pu.CostEstimationYear == yr)?.TotalUnits;
                                    previousUnits = (previousUnits == null ? 0 : previousUnits) - _ProgramQuqterUnitMappingRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == u.PrgActionAreaActivityMappingID && pq.ProgramQuaterYear == yr && pq.ProgramID == item.ProgramID && pq.Status == ProgramQuarterUnitMappingStatus.Completed).Select(t => t.NosOfUnit).Sum();
                                    previousRs = previousRs - _ProgramExpenseRepository.GetAll().Where(pq1 => pq1.PrgActionAreaActivityMappingID == u.PrgActionAreaActivityMappingID && pq1.ExpensesYear == yr && pq1.ProgramID == item.ProgramID).Select(t => t.Amount).Sum();
                                    previousRs += obj.FirstOrDefault(pu => pu.PrgActionAreaActivityMappingID == u.PrgActionAreaActivityMappingID && pu.CostEstimationYear == yr) == null ? 0 : obj.FirstOrDefault(pu => pu.PrgActionAreaActivityMappingID == u.PrgActionAreaActivityMappingID && pu.CostEstimationYear == yr).TotalUnitCost;
                                }
                                i.Add(new ListActionSubactionListDto
                                {
                                    ActionAreaName = u.VillageID != 0 ? string.Empty : u.ActionAreaName,
                                    SubActionAreaName = u.VillageID != 0 ? string.Empty : u.SubActionAreaName,
                                    ActivityName = u.VillageID != 0 ? string.Empty : u.ActivityName,
                                    VillageName = u.VillageName,
                                    UnitOfMeasuresName = u.UnitOfMeasuresName,
                                    UnitOfMeasuresID = u.UnitOfMeasuresID,
                                    TotalUnits = u.TotalUnits,
                                    PreviousUnits = previousUnits,
                                    PreviousBalanceRs = previousRs,
                                    UnitCost = u.UnitCost,
                                    TotalUnitCost = u.TotalUnitCost,
                                    CommunityContribution = u.CommunityContribution,
                                    FunderContribution = u.FunderContribution,
                                    OtherContribution = u.OtherContribution,
                                    CostEstimationYear = u.CostEstimationYear,
                                    PrgActionAreaActivityMappingID = u.PrgActionAreaActivityMappingID,
                                    Quarter1FinancialRs = u.Quarter1FinancialRs,
                                    Quarter1PhysicalUnits = u.Quarter1PhysicalUnits,
                                    Quarter2FinancialRs = u.Quarter2FinancialRs,
                                    Quarter2PhysicalUnits = u.Quarter2PhysicalUnits,
                                    Quarter3FinancialRs = u.Quarter3FinancialRs,
                                    Quarter3PhysicalUnits = u.Quarter3PhysicalUnits,
                                    Quarter4FinancialRs = u.Quarter4FinancialRs,
                                    Quarter4PhysicalUnits = u.Quarter4PhysicalUnits,
                                    ActivityLevel = u.ActivityLevel,
                                    Id = u.Id,
                                    SubActionSubActionActivity = subUnitActivity
                                });
                            }

                            var currentYear = false;
                            PreviousYears.Add(item2.ToString());
                            p = !Query.Any(c => c.LocationID == item.LocationID && c.Year == item2.ToString()) ? RequestStatus.RequestNotSend : Query.FirstOrDefault(c => c.LocationID == item.LocationID && c.Year == item2.ToString()).RequestStatus;
                            var currentYr = item2.Split('-').Select(Int32.Parse).ToList();
                            var from = DateTime.Parse("April 1," + currentYr[0]);
                            var to = DateTime.Parse("March 31, " + currentYr[1]);
                            if (from <= DateTime.Now && to >= DateTime.Now)
                            {
                                currentYear = true;
                            }
                            obj1.Add(new ProgrameCostEstimationYearRequestApplDto
                            {
                                CostEstimationYear = item2.ToString(),
                                LocationID = item.LocationID,
                                CurrentYear = currentYear,
                                ManagerID = item.ManagerID,
                                ProgramID = item.ProgramID,
                                RequestStatus = p,
                                ActionSubactionCost = i
                            });
                        }

                        if (item.ManagerID == 0 && (item.ActivityLevel == ActivityLevel.VillageClusterLevel || item.ActivityLevel == ActivityLevel.VillageLevel))
                        {
                            item.ManagerName = "Select Project Manager";
                            ProjectmanagerId = _abproleRepository.FirstOrDefault(r => r.Name == "ProjectManager")?.Id;
                            LocationWiseId = _LocationRepository.FirstOrDefault(l => l.Id == item.LocationID)?.ParentLocation;
                        }
                        else if (item.ManagerID == 0 && item.ActivityLevel == ActivityLevel.RRCLevel)
                        {
                            item.ManagerName = "Select RRc Incharge";
                            ProjectmanagerId = _abproleRepository.FirstOrDefault(r => r.Name == "RRcIncharge")?.Id;
                            LocationWiseId = item.LocationID;
                        }
                        else
                        {
                            var que = UserManager.Users.Where(u => u.Id == item.ManagerID).ToList();
                            foreach (var user in que)
                            {
                                item.ManagerName = user.FullName;
                            }
                        }

                        foreach (var manager in listOfUsers)
                        {
                            #region previous code
                            //var query = UserManager.Users.FirstOrDefault(u => u.Id == manager.UserId).UserRole;
                            //var user = UserManager.Users.Where(ur => ur.Id == manager.UserId && ur.LocationId == LocationWiseId);
                            //foreach (var users in user)
                            //{
                            //    if (query == ProjectmanagerId)
                            //    {
                            //        ManagersList.Add(new PrgManagerListDto
                            //        {
                            //            Name = users.FullName,
                            //            ManagerId = users.Id,

                            //        });
                            //    }

                            //}
                            #endregion
                            //new Code by sachin for rrc name for project in  dropdown checklist On 20 Feb 2020
                            var query = UserManager.Users.FirstOrDefault(u => u.Id == manager.UserId).UserRole;
                            var userRole = _villageClusterRepository.FirstOrDefault(x => x.UserId == manager.UserId && x.ProgramID == ProgramId).UserRoleId;
                            var Role = _userRoleRepository.GetAll().FirstOrDefault(x => x.Id == userRole).RoleId;
                            var user = UserManager.Users.Where(ur => ur.Id == manager.UserId && ur.LocationId == LocationWiseId);
                            foreach (var users in user)
                            {
                                if (Role == ProjectmanagerId)
                                {
                                    ManagersList.Add(new PrgManagerListDto
                                    {
                                        Name = users.FullName,
                                        ManagerId = users.Id,

                                    });
                                }

                            }
                        }

                        result.Add(new RequestAAPlasListDto
                        {
                            LocationID = item.LocationID,
                            LocationName = item.LocationName,
                            ManagerID = item.ManagerID,
                            ManagerName = item.ManagerName,
                            ProgramID = item.ProgramID,
                            RequestStatus = item.RequestStatus,
                            ActivityLevel = item.ActivityLevel,
                            ProgrameStartDate = item.ProgrameStartDate,
                            ProgrameEndDate = item.ProgrameEndDate,
                            CostEstimationYear = obj1,
                            Managers = ManagersList
                        });
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("Error Db" + ex.Message);
            }


        }
        public async Task<List<RequestAAPlasListDto>> GetRequestApplaDeatilsForActionArea(int ProgramId)
        {
            int? TenantId = AbpSession.TenantId;
            RequestStatus p;
            var result = new List<RequestAAPlasListDto>();
            var results = new List<RequestAAPlasListDto>();

            var user = "";
            var Query = _RequestApplsSQLRepository.GetRquestApplData(TenantId, ProgramId);

            if (Query != null)
            {
                var listOfLocation = Query.ToList().GroupBy(x => x.LocationID)
                                      .Select(g => g.First())
                                      .ToList();

                var dates = new List<string>();

                long Start = 0;
                long end = 0;
                if (Query[0].ProgrameStartDate.Month < 4)
                {
                    Start = Query[0].ProgrameStartDate.Year - 1;
                    end = Query[0].ProgrameStartDate.Year;
                }
                else
                {
                    Start = Query[0].ProgrameStartDate.Year;
                    end = Query[0].ProgrameStartDate.Year + 1;
                }

                for (var dt = Start; dt <= Query[0].ProgrameEndDate.Year; dt = dt + 1)
                {
                    var from = DateTime.Parse("April 1," + (Start));
                    var to = DateTime.Parse("March 31, " + end);
                    if (Query[0].ProgrameEndDate.Month > to.Month || Query[0].ProgrameEndDate.Year > from.Year)
                    {
                        dates.Add(Start.ToString() + "-" + end.ToString());
                    }
                    Start++;
                    end++;
                }


                foreach (var item in listOfLocation)
                {
                    int? ProjectmanagerId = 0;
                    int? LocationWiseId = 0;
                    var obj1 = new List<ProgrameCostEstimationYearRequestApplDto>();

                    var obj = new List<ListActionSubactionListDto>();
                    var subUnitActivity = new List<ListActionSubactionListDto>();

                    var managerList = new List<PrgManagerListDto>();

                    foreach (var act in Query[0].CostEstimationYear[0].ActionSubactionCost)
                    {
                        if (act.LocationID != 0)
                        {
                            if (item.LocationID == act.LocationID)
                            {
                                var action = await _SubActionAreaRepository.FirstOrDefaultAsync(x => x.Name == act.SubActionAreaName);


                                if (UserManager.Users.Any(ppp => ppp.ActionId == action.ActionAreaId) && act.ActivityLevel != ActivityLevel.ProgramLevel)
                                {
                                    user = UserManager.Users.FirstOrDefault(pp => pp.ActionId == action.ActionAreaId).UserName;

                                    var user11 = UserManager.Users.Where(ur => ur.ActionId == action.ActionAreaId);
                                    foreach (var users in user11)
                                    {
                                        if (managerList.Count == 0)
                                        {
                                            managerList.Add(new PrgManagerListDto
                                            {
                                                Name = users.FullName,
                                                ManagerId = users.Id,

                                            });
                                        }

                                    }
                                    obj.Add(act);
                                }
                                else
                                {
                                }

                            }

                        }
                    }
                    var PreviousYears = new List<string>();
                    //string Yr = "";

                    foreach (var item2 in dates)
                    {
                        var i = new List<ListActionSubactionListDto>();

                        foreach (var u in obj)
                        {
                            long? previousUnits = 0;
                            decimal? previousRs = 0;
                            if (u.CostEstimationYear == item2.ToString())
                            {
                                foreach (var yr in PreviousYears)
                                {
                                    previousUnits += obj.FirstOrDefault(pu => pu.PrgActionAreaActivityMappingID == u.PrgActionAreaActivityMappingID && pu.CostEstimationYear == yr)?.TotalUnits;
                                    previousUnits = (previousUnits == null ? 0 : previousUnits) - _ProgramQuqterUnitMappingRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == u.PrgActionAreaActivityMappingID && pq.ProgramQuaterYear == yr && pq.ProgramID == item.ProgramID && pq.Status == ProgramQuarterUnitMappingStatus.Completed).Select(t => t.NosOfUnit).Sum();
                                    previousRs = previousRs - _ProgramExpenseRepository.GetAll().Where(pq1 => pq1.PrgActionAreaActivityMappingID == u.PrgActionAreaActivityMappingID && pq1.ExpensesYear == yr && pq1.ProgramID == item.ProgramID).Select(t => t.Amount).Sum();
                                    previousRs += obj.FirstOrDefault(pu => pu.PrgActionAreaActivityMappingID == u.PrgActionAreaActivityMappingID && pu.CostEstimationYear == yr) == null ? 0 : obj.FirstOrDefault(pu => pu.PrgActionAreaActivityMappingID == u.PrgActionAreaActivityMappingID && pu.CostEstimationYear == yr).TotalUnitCost;
                                }

                                i.Add(new ListActionSubactionListDto
                                {
                                    ActionAreaName = u.VillageID != 0 ? string.Empty : u.ActionAreaName,
                                    SubActionAreaName = u.VillageID != 0 ? string.Empty : u.SubActionAreaName,
                                    ActivityName = u.VillageID != 0 ? string.Empty : u.ActivityName,
                                    UnitOfMeasuresName = u.UnitOfMeasuresName,
                                    UnitOfMeasuresID = u.UnitOfMeasuresID,
                                    TotalUnits = u.TotalUnits,
                                    PreviousUnits = previousUnits,
                                    PreviousBalanceRs = previousRs,
                                    UnitCost = u.UnitCost,
                                    TotalUnitCost = u.TotalUnitCost,
                                    CommunityContribution = u.CommunityContribution,
                                    FunderContribution = u.FunderContribution,
                                    OtherContribution = u.OtherContribution,
                                    CostEstimationYear = u.CostEstimationYear,
                                    PrgActionAreaActivityMappingID = u.PrgActionAreaActivityMappingID,
                                    Quarter1FinancialRs = u.Quarter1FinancialRs,
                                    Quarter1PhysicalUnits = u.Quarter1PhysicalUnits,
                                    Quarter2FinancialRs = u.Quarter2FinancialRs,
                                    Quarter2PhysicalUnits = u.Quarter2PhysicalUnits,
                                    Quarter3FinancialRs = u.Quarter3FinancialRs,
                                    Quarter3PhysicalUnits = u.Quarter3PhysicalUnits,
                                    Quarter4FinancialRs = u.Quarter4FinancialRs,
                                    Quarter4PhysicalUnits = u.Quarter4PhysicalUnits,
                                    ActivityLevel = u.ActivityLevel,
                                    Id = u.Id,
                                    SubActionSubActionActivity = subUnitActivity,
                                    //VillageID = u.VillageID,
                                });
                            }
                        }

                        var currentYear = false;
                        var managerId = 0;

                        PreviousYears.Add(item2.ToString());
                        if (managerList.Count != 0)
                        {
                            managerId = Convert.ToInt32(managerList.FirstOrDefault().ManagerId);

                        }
                        p = !Query.Any(c => c.LocationID == item.LocationID && c.Year == item2.ToString() && c.ManagerID == managerId) ? RequestStatus.RequestNotSend : Query.FirstOrDefault(c => c.LocationID == item.LocationID && c.Year == item2.ToString()).RequestStatus;

                        var currentYr = item2.Split('-').Select(Int32.Parse).ToList();
                        var from = DateTime.Parse("April 1," + currentYr[0]);
                        var to = DateTime.Parse("March 31, " + currentYr[1]);
                        if (from <= DateTime.Now && to >= DateTime.Now)
                        {
                            currentYear = true;
                        }
                        obj1.Add(new ProgrameCostEstimationYearRequestApplDto
                        {
                            CostEstimationYear = item2.ToString(),
                            LocationID = item.LocationID,
                            CurrentYear = currentYear,
                            ManagerID = item.ManagerID,
                            ProgramID = item.ProgramID,
                            RequestStatus = p,
                            ActionSubactionCost = i
                        });
                    }


                    if (user != null)
                    {
                        user = "Select Action Area Manager";
                    }
                    else
                    {
                        var que = UserManager.Users.Where(u => u.Id == item.ManagerID).ToList();
                        foreach (var user1 in que)
                        {
                            item.ManagerName = user1.FullName;
                        }
                    }


                    result.Add(new RequestAAPlasListDto
                    {
                        LocationID = item.LocationID,
                        LocationName = item.LocationName,
                        ManagerID = item.ManagerID,
                        ManagerName = user,
                        ProgramID = item.ProgramID,
                        RequestStatus = item.RequestStatus,
                        ActivityLevel = item.ActivityLevel,
                        ProgrameStartDate = item.ProgrameStartDate,
                        ProgrameEndDate = item.ProgrameEndDate,
                        CostEstimationYear = obj1,
                        Managers = managerList
                    });
                }
                foreach (var item in result)
                {
                    if (item.Managers.Count != 0)
                    {
                        results.Add(item);

                    }
                }

            }
            return results;
        }

        public async Task<List<RequestAAPlasListDto>> GetReQuestApplaForProjectManager(int ProgramId)
        {
            try
            {
                var managerId = (long)AbpSession.UserId;
                var projectManagerRoleId = await _abproleRepository.FirstOrDefaultAsync(a => a.DisplayName == "Project Manager");
                var rrcInchargeId = await _abproleRepository.FirstOrDefaultAsync(a => a.DisplayName == "RRc Incharge");
                var actionAreaManagerRoleId = await _abproleRepository.FirstOrDefaultAsync(a => a.DisplayName == "Action Area Manager");


                var query1 = UserManager.Users.FirstOrDefault(r => r.Id == managerId)?.UserRole;
                var query2 = projectManagerRoleId?.Id;
                var query3 = rrcInchargeId?.Id;
                var query4 = actionAreaManagerRoleId?.Id;

                var requestAaplaList = _RequestAAplasRepository.GetAll();


                if (query1 == query2 || query1 == query3 || query1 == query4)
                {
                    var Query = await (from r in requestAaplaList.Where(r => r.ManagerID == managerId && r.ProgramID == ProgramId)
                                       group r by new
                                       {
                                           r.LocationID,
                                           r.ManagerID,
                                           r.ProgramID,

                                       } into gcs
                                       join l in _LocationRepository.GetAll()
                                      on gcs.Key.LocationID equals l.Id
                                       join p in _ProgrameRepository.GetAll()
                                       on gcs.Key.ProgramID equals p.Id


                                       select new RequestAAPlasListDto()
                                       {
                                           ProgramID = gcs.Key.ProgramID,
                                           ProgramName = p.Name,
                                           ProgrameStartDate = p.ProgramStartDate,
                                           ProgrameEndDate = p.PrgramEndDate,
                                           LocationID = gcs.Key.LocationID,
                                           ManagerID = gcs.Key.ManagerID,
                                           LocationName = l.Name,
                                           Id = l.Id,
                                           CostEstimationYear = (from q in requestAaplaList.Where(ma => ma.ManagerID == managerId && ma.LocationID == gcs.Key.LocationID && ma.ProgramID == gcs.Key.ProgramID)
                                                                 group q by new
                                                                 {
                                                                     q.CostEstimationYear,
                                                                     q.RequestStatus,
                                                                 } into gcs1

                                                                 select new ProgrameCostEstimationYearRequestApplDto()
                                                                 {
                                                                     Id = l.Id,
                                                                     LocationID = gcs.Key.LocationID,
                                                                     ManagerID = gcs.Key.ManagerID,
                                                                     ProgramID = gcs.Key.ProgramID,
                                                                     RequestStatus = gcs1.Key.RequestStatus,
                                                                     CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                                     ActionSubactionCost = (from a in requestAaplaList.Where(mn => mn.ManagerID == managerId && mn.LocationID == gcs.Key.LocationID
                                                                                            && mn.CostEstimationYear == gcs1.Key.CostEstimationYear && mn.ProgramID == gcs.Key.ProgramID && mn.VillageID == null)
                                                                                            join pa in _PrgActionAreaActivityMappingRepository.GetAll()
                                                                                            on a.PrgActionAreaActivityMappingID equals pa.Id
                                                                                            join pc in _PrgCostEstRepository.GetAll()
                                                                                            on pa.ActivityID equals pc.ActivityID
                                                                                            where (pc.ComponentID == pa.ComponentID && pc.CostEstimationYear == a.CostEstimationYear && pc.ProgramID == gcs.Key.ProgramID)
                                                                                            select new ListActionSubactionListDto()
                                                                                            {
                                                                                                Id = a.Id,
                                                                                                ActionAreaName = pa.ActionArea.Name,
                                                                                                ActivityLevel = pc.ActivityLevel,
                                                                                                SubActionAreaName = pa.SubActionArea.Name,
                                                                                                ActivityName = pa.Activity.Name,
                                                                                                UnitOfMeasuresName = a.UnitOfMeasures.Name,
                                                                                                UnitOfMeasuresID = a.UnitOfMeasuresID,
                                                                                                PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                                TotalUnits = a.TotalUnits,
                                                                                                UnitCost = a.UnitCost,
                                                                                                TotalUnitCost = a.TotalUnitCost,
                                                                                                CommunityContribution = a.CommunityContribution,
                                                                                                FunderContribution = a.FunderContribution,
                                                                                                OtherContribution = a.OtherContribution,
                                                                                                RequestStatus = a.RequestStatus,
                                                                                                Quarter1PhysicalUnits = a.Quarter1PhysicalUnits,
                                                                                                Quarter1FinancialRs = a.Quarter1FinancialRs,
                                                                                                Quarter2PhysicalUnits = a.Quarter2PhysicalUnits,
                                                                                                Quarter2FinancialRs = a.Quarter2FinancialRs,
                                                                                                Quarter3PhysicalUnits = a.Quarter3PhysicalUnits,
                                                                                                Quarter3FinancialRs = a.Quarter3FinancialRs,
                                                                                                Quarter4PhysicalUnits = a.Quarter4PhysicalUnits,
                                                                                                Quarter4FinancialRs = a.Quarter4FinancialRs,
                                                                                                PreviousBalanceRs = (gcs.Where(t => t.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && t.VillageID == null && t.CostEstimationYear == (Convert.ToInt32(a.CostEstimationYear.Split('-')[0]) - 1 + "-" + a.CostEstimationYear.Split('-')[0]))
                                                                                                                       .Select(t => t.TotalUnitCost).FirstOrDefault() ?? 0) - _ProgramExpenseRepository.GetAll().Where(pq1 => pq1.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID
                                                                                                                         && pq1.ExpensesYear == (Convert.ToInt32(a.CostEstimationYear.Split('-')[0]) - 1 + "-" + a.CostEstimationYear.Split('-')[0]) && pq1.ProgramID == a.ProgramID).Select(t => t.Amount).Sum(),
                                                                                                PreviousUnits = (gcs.Where(t => t.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID && t.VillageID == null
                                                                                                                       && t.CostEstimationYear == (Convert.ToInt32(a.CostEstimationYear.Split('-')[0]) - 1 + "-" + a.CostEstimationYear.Split('-')[0])).Select(t => t.TotalUnits).FirstOrDefault() ?? 0) - _ProgramQuqterUnitMappingRepository.GetAll().Where(pq => pq.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID
                                                                                                                       && pq.ProgramQuaterYear == (Convert.ToInt32(a.CostEstimationYear.Split('-')[0]) - 1 + "-" + a.CostEstimationYear.Split('-')[0]) && pq.ProgramID == a.ProgramID && pq.Status == ProgramQuarterUnitMappingStatus.Completed).Select(t => t.NosOfUnit).Sum(),
                                                                                                SubActionSubActionActivity = (from a1 in requestAaplaList.Where(mn => mn.ManagerID == managerId && mn.LocationID == gcs.Key.LocationID
                                                                                                                   && mn.CostEstimationYear == gcs1.Key.CostEstimationYear && mn.ProgramID == gcs.Key.ProgramID && mn.VillageID != null && mn.PrgActionAreaActivityMappingID == a.PrgActionAreaActivityMappingID)

                                                                                                                              select new ListActionSubactionListDto()
                                                                                                                              {
                                                                                                                                  Id = a1.Id,
                                                                                                                                  UnitOfMeasuresName = a1.UnitOfMeasures.Name,
                                                                                                                                  UnitOfMeasuresID = a1.UnitOfMeasuresID,
                                                                                                                                  VillageID = a1.VillageID,
                                                                                                                                  PrgActionAreaActivityMappingID = a1.PrgActionAreaActivityMappingID,
                                                                                                                                  TotalUnits = a1.TotalUnits,
                                                                                                                                  UnitCost = a1.UnitCost,
                                                                                                                                  TotalUnitCost = a1.TotalUnitCost,
                                                                                                                                  CommunityContribution = a1.CommunityContribution,
                                                                                                                                  FunderContribution = a1.FunderContribution,
                                                                                                                                  OtherContribution = a1.OtherContribution,
                                                                                                                                  RequestStatus = a1.RequestStatus,
                                                                                                                                  Quarter1PhysicalUnits = a1.Quarter1PhysicalUnits,
                                                                                                                                  Quarter1FinancialRs = a1.Quarter1FinancialRs,
                                                                                                                                  Quarter2PhysicalUnits = a1.Quarter2PhysicalUnits,
                                                                                                                                  Quarter2FinancialRs = a1.Quarter2FinancialRs,
                                                                                                                                  Quarter3PhysicalUnits = a1.Quarter3PhysicalUnits,
                                                                                                                                  Quarter3FinancialRs = a1.Quarter3FinancialRs,
                                                                                                                                  Quarter4PhysicalUnits = a1.Quarter4PhysicalUnits,
                                                                                                                                  Quarter4FinancialRs = a1.Quarter4FinancialRs,
                                                                                                                              }).ToList(),
                                                                                            }).ToList(),
                                                                 }).ToList(),

                                       }).ToListAsync();
                    var tempList = Query.FirstOrDefault().CostEstimationYear.FirstOrDefault().ActionSubactionCost.ToList().
                        GroupBy(x => x.ActivityName).ToList();

                    Query.FirstOrDefault().CostEstimationYear.FirstOrDefault().ActionSubactionCost = new List<ListActionSubactionListDto>();


                    tempList.ForEach(obj =>
                    {
                        Query.FirstOrDefault().CostEstimationYear.FirstOrDefault().ActionSubactionCost.Add(obj.FirstOrDefault());
                    });

                    return new List<RequestAAPlasListDto>(ObjectMapper.Map<List<RequestAAPlasListDto>>(Query));
                }
                else
                {
                    var subUnitActivity = new List<ListActionSubactionListDto>();
                    var Query = await (from r in requestAaplaList.Where(p => p.ProgramID == ProgramId && p.RequestStatus != RequestStatus.RequestSend)
                                       group r by new
                                       {
                                           r.LocationID,
                                           r.ManagerID,
                                           r.ProgramID
                                       } into gcs
                                       join l in _LocationRepository.GetAll()
                                      on gcs.Key.LocationID equals l.Id
                                       join p in _ProgrameRepository.GetAll()
                                       on gcs.Key.ProgramID equals p.Id


                                       select new RequestAAPlasListDto()
                                       {
                                           ProgramID = gcs.Key.ProgramID,
                                           ProgramName = p.Name,
                                           ProgrameStartDate = p.ProgramStartDate,
                                           ProgrameEndDate = p.PrgramEndDate,
                                           LocationID = gcs.Key.LocationID,
                                           ManagerID = gcs.Key.ManagerID,
                                           ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == gcs.Key.ManagerID).Name,
                                           LocationName = l.Name,
                                           Id = l.Id,
                                           //&& ma.RequestStatus != RequestStatus.RequestSend
                                           CostEstimationYear = (from q in requestAaplaList.Where(ma => ma.LocationID == gcs.Key.LocationID && ma.ProgramID == gcs.Key.ProgramID && ma.ManagerID == gcs.Key.ManagerID)
                                                                 group q by new
                                                                 {
                                                                     q.CostEstimationYear,
                                                                     q.RequestStatus
                                                                 } into gcs1

                                                                 select new ProgrameCostEstimationYearRequestApplDto()
                                                                 {
                                                                     Id = l.Id,
                                                                     LocationID = gcs.Key.LocationID,
                                                                     ManagerID = gcs.Key.ManagerID,
                                                                     ProgramID = gcs.Key.ProgramID,
                                                                     CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                                     RequestStatus = gcs1.Key.RequestStatus,
                                                                     ActionSubactionCost = (from a in requestAaplaList.Where(mn => mn.LocationID == gcs.Key.LocationID
                                                                                            && mn.CostEstimationYear == gcs1.Key.CostEstimationYear && mn.ProgramID == gcs.Key.ProgramID && mn.ManagerID == gcs.Key.ManagerID)

                                                                                            join pa in _PrgActionAreaActivityMappingRepository.GetAll()
                                                                                            on a.PrgActionAreaActivityMappingID equals pa.Id
                                                                                            join pc in _PrgCostEstRepository.GetAll()
                                                                                           on pa.ActivityID equals pc.ActivityID
                                                                                            where (pc.ComponentID == pa.ComponentID && pc.CostEstimationYear == gcs1.Key.CostEstimationYear && pc.ProgramID == gcs.Key.ProgramID)
                                                                                            select new ListActionSubactionListDto()
                                                                                            {
                                                                                                Id = a.Id,
                                                                                                //Actname= pa.Activity.Name,
                                                                                                ActionAreaName = a.VillageID != null ? string.Empty : pa.ActionArea.Name,
                                                                                                SubActionAreaName = a.VillageID != null ? string.Empty : pa.SubActionArea.Name,
                                                                                                ActivityName = a.VillageID != null ? string.Empty : pa.Activity.Name,
                                                                                                ActivityLevel = pc.ActivityLevel,
                                                                                                UnitOfMeasuresName = a.UnitOfMeasures.Name,
                                                                                                UnitOfMeasuresID = a.UnitOfMeasuresID,
                                                                                                //ChecklistID = a.ChecklistID,
                                                                                                PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                                VillageID = a.VillageID,
                                                                                                VillageName = a.Village.Name,
                                                                                                TotalUnits = a.TotalUnits,
                                                                                                UnitCost = a.UnitCost,
                                                                                                TotalUnitCost = a.TotalUnitCost,
                                                                                                CommunityContribution = a.CommunityContribution,
                                                                                                FunderContribution = a.FunderContribution,
                                                                                                OtherContribution = a.OtherContribution,
                                                                                                RequestStatus = a.RequestStatus,
                                                                                                Quarter1PhysicalUnits = a.Quarter1PhysicalUnits,
                                                                                                Quarter1FinancialRs = a.Quarter1FinancialRs,

                                                                                                Quarter2PhysicalUnits = a.Quarter2PhysicalUnits,
                                                                                                Quarter2FinancialRs = a.Quarter2FinancialRs,

                                                                                                Quarter3PhysicalUnits = a.Quarter3PhysicalUnits,
                                                                                                Quarter3FinancialRs = a.Quarter3FinancialRs,

                                                                                                Quarter4PhysicalUnits = a.Quarter4PhysicalUnits,
                                                                                                Quarter4FinancialRs = a.Quarter4FinancialRs,
                                                                                                SubActionSubActionActivity = subUnitActivity,


                                                                                            }).OrderBy(pe => pe.PrgActionAreaActivityMappingID).ThenByDescending(pe => pe.ActivityName).ToList(),
                                                                 }).ToList(),

                                       }).ToListAsync();

                    return new List<RequestAAPlasListDto>(ObjectMapper.Map<List<RequestAAPlasListDto>>(Query));
                }
            }
            catch (Exception ex)
            {

                throw;
            }


        }

        public void Test(string res)
        {
            var d = res.Split('-');
            var f = (Convert.ToInt32(res.Split('-')[0]) - 1 + "-" + res.Split('-')[0]);
        }

        public List<Village> GetAllVillagesClusterWise(int programId, int locationId)
        {
            var query = (from vc in _villageClusterRepository.GetAll().Where(p => p.ProgramID == programId && p.VillageClusterLocationID == locationId)
                         group vc by new
                         {
                             vc.VillageID
                         } into gcs
                         join v in _villageRepository.GetAll()
                         on gcs.Key.VillageID equals v.Id
                         select new Village
                         {
                             Name = v.Name,
                             Id = v.Id
                         }).ToList();
            return query;
        }

        //export to excel method
        public async Task<List<RequestAAPlasListDto>> GetRequestSendDetailsProjectManager(int ProgramId, string Year)
        {
            var managerId = (long)AbpSession.UserId;
            var projectManagerRoleId = await _abproleRepository.FirstOrDefaultAsync(a => a.DisplayName == "Project Manager");
            var rrcInchargeId = await _abproleRepository.FirstOrDefaultAsync(a => a.DisplayName == "RRc Incharge");
            var actionAreaManagerRoleId = await _abproleRepository.FirstOrDefaultAsync(a => a.DisplayName == "Action Area Manager");


            var query1 = UserManager.Users.FirstOrDefault(r => r.Id == managerId)?.UserRole;
            var query2 = projectManagerRoleId?.Id;
            var query3 = rrcInchargeId?.Id;
            var query4 = actionAreaManagerRoleId?.Id;
            var subUnitActivity = new List<ListActionSubactionListDto>();
            var Query = await (from r in _RequestAAplasRepository.GetAll().Where(p => p.ProgramID == ProgramId && p.RequestStatus == RequestStatus.RequestSend && p.CostEstimationYear == Year)
                               group r by new
                               {
                                   r.LocationID,
                                   r.ManagerID,
                                   r.ProgramID
                               } into gcs
                               join l in _LocationRepository.GetAll()
                              on gcs.Key.LocationID equals l.Id
                               join p in _ProgrameRepository.GetAll()
                               on gcs.Key.ProgramID equals p.Id


                               select new RequestAAPlasListDto()
                               {
                                   ProgramID = gcs.Key.ProgramID,
                                   ProgramName = p.Name,
                                   ProgrameStartDate = p.ProgramStartDate,
                                   ProgrameEndDate = p.PrgramEndDate,
                                   LocationID = gcs.Key.LocationID,
                                   ManagerID = gcs.Key.ManagerID,
                                   ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == gcs.Key.ManagerID).Name,
                                   LocationName = l.Name,
                                   Id = l.Id,

                                   //&& ma.RequestStatus != RequestStatus.RequestSend
                                   CostEstimationYear = (from q in _RequestAAplasRepository.GetAll().Where(ma => ma.LocationID == gcs.Key.LocationID && ma.RequestStatus == RequestStatus.RequestSend && ma.ProgramID == gcs.Key.ProgramID && ma.ManagerID == gcs.Key.ManagerID && ma.CostEstimationYear == Year)
                                                         group q by new
                                                         {
                                                             q.CostEstimationYear,
                                                             q.RequestStatus
                                                         } into gcs1

                                                         select new ProgrameCostEstimationYearRequestApplDto()
                                                         {
                                                             Id = l.Id,
                                                             LocationID = gcs.Key.LocationID,
                                                             ManagerID = gcs.Key.ManagerID,
                                                             ProgramID = gcs.Key.ProgramID,
                                                             CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                             RequestStatus = gcs1.Key.RequestStatus,
                                                             ActionSubactionCost = (from a in _RequestAAplasRepository.GetAll().Where(mn => mn.LocationID == gcs.Key.LocationID && mn.RequestStatus == RequestStatus.RequestSend
                                                                                    && mn.CostEstimationYear == Year && mn.ProgramID == gcs.Key.ProgramID && mn.ManagerID == gcs.Key.ManagerID)

                                                                                    join pa in _PrgActionAreaActivityMappingRepository.GetAll()
                                                                                    on a.PrgActionAreaActivityMappingID equals pa.Id
                                                                                    join pc in _PrgCostEstRepository.GetAll()
                                                                                   on pa.ActivityID equals pc.ActivityID
                                                                                    where (pc.ComponentID == pa.ComponentID && pc.ProgramID == gcs.Key.ProgramID && pc.CostEstimationYear == Year)
                                                                                    select new ListActionSubactionListDto()
                                                                                    {
                                                                                        Id = a.Id,
                                                                                        //Actname= pa.Activity.Name,
                                                                                        ActionAreaName = a.VillageID != null ? string.Empty : pa.ActionArea.Name,
                                                                                        SubActionAreaName = a.VillageID != null ? string.Empty : pa.SubActionArea.Name,
                                                                                        ActivityName = a.VillageID != null ? string.Empty : pa.Activity.Name,
                                                                                        ActivityLevel = pc.ActivityLevel,
                                                                                        UnitOfMeasuresName = a.UnitOfMeasures.Name,
                                                                                        UnitOfMeasuresID = a.UnitOfMeasuresID,
                                                                                        //ChecklistID = a.ChecklistID,
                                                                                        PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                        VillageID = a.VillageID,
                                                                                        VillageName = a.Village.Name,
                                                                                        TotalUnits = a.TotalUnits,
                                                                                        UnitCost = a.UnitCost,
                                                                                        TotalUnitCost = a.TotalUnitCost,
                                                                                        CommunityContribution = a.CommunityContribution,
                                                                                        FunderContribution = a.FunderContribution,
                                                                                        OtherContribution = a.OtherContribution,
                                                                                        RequestStatus = a.RequestStatus,
                                                                                        Quarter1PhysicalUnits = a.Quarter1PhysicalUnits,
                                                                                        Quarter1FinancialRs = a.Quarter1FinancialRs,

                                                                                        Quarter2PhysicalUnits = a.Quarter2PhysicalUnits,
                                                                                        Quarter2FinancialRs = a.Quarter2FinancialRs,

                                                                                        Quarter3PhysicalUnits = a.Quarter3PhysicalUnits,
                                                                                        Quarter3FinancialRs = a.Quarter3FinancialRs,

                                                                                        Quarter4PhysicalUnits = a.Quarter4PhysicalUnits,
                                                                                        Quarter4FinancialRs = a.Quarter4FinancialRs,
                                                                                        SubActionSubActionActivity = subUnitActivity,


                                                                                    }).ToList(),
                                                         }).ToList(),

                               }).ToListAsync();

            return new List<RequestAAPlasListDto>(ObjectMapper.Map<List<RequestAAPlasListDto>>(Query));





        }

        public List<string> PrgCostEstimationYears(int programId)
        {

            List<string> list = (from cost in _RequestAAplasRepository.GetAll().Where(ss => ss.ProgramID == programId)
                                 select cost.CostEstimationYear.ToString()).Distinct().OrderBy(x => x).ToList();
            return list;
        }
        public List<string> OverallPrgCostEstimationYears()
        {

            List<string> list = (from cost in _RequestAAplasRepository.GetAll()
                                 select cost.CostEstimationYear.ToString()).Distinct().OrderBy(x => x).ToList();
            return list;
        }
        public async Task<FileDto> CreateExcelDoc(string fileName, int ProgramId, string Year)
        {
            var result = await GetRequestSendDetailsProjectManager(ProgramId, Year);
            var a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";
            var b = @"E:\Reports\";
            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))
            {
                var wbp = xl.AddWorkbookPart();
                var wsp = wbp.AddNewPart<WorksheetPart>();
                var wb = new Workbook();
                var fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();
                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                var dp = wsp.AddNewPart<DrawingsPart>();
                var imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                var nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                var nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                var nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                var blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                var bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                var sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                var pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                var ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                var anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                var wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                var drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                var sheets = new Sheets();
                var sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                wb.Append(fv);

                // Constructing header
                var row1 = new Row();
                var row10 = new Row();
                var col1 = new Column();

                var row3 = new Row();
                var row5 = new Row();
                Row row6 = new Row();
                Row row7 = new Row();
                Row row8 = new Row();
                Row row4 = new Row();
                foreach (var program in result)
                {
                    var cell8 = InsertCellInWorksheet("D", 1, wsp);
                    cell8.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                    cell8.DataType = new EnumValue<CellValues>(CellValues.Number);

                    var cellString8 = InsertCellInWorksheet("D", 1, wsp);

                    cellString8.CellValue = new CellValue("Watershed Organisation Trust(WOTR)");
                    cellString8.DataType = new EnumValue<CellValues>(CellValues.String);
                    cell8.StyleIndex = 1;
                    Run run1 = new Run();
                    run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                    RunProperties run1Properties = new RunProperties();
                    run1Properties.Append(new Bold());
                    run1.RunProperties = run1Properties;
                    InlineString inlineString = new InlineString();
                    inlineString.Append(run1);
                    cell8.Append(inlineString);

                    var cell = InsertCellInWorksheet("D", 2, wsp);
                    cell.CellValue = new CellValue("Program Title : " + program.ProgramName);
                    cell.DataType = new EnumValue<CellValues>(CellValues.Number);
                    Run run2 = new Run();
                    run2.Append(new Text("Program Title : " + program.ProgramName));
                    RunProperties run2Properties = new RunProperties();
                    Color color3 = new Color() { Rgb = "2F75B5" };
                    FontSize fontSize3 = new FontSize() { Val = 16D };
                    run2Properties.Append(new Bold());
                    run2Properties.Append(color3);
                    run2Properties.Append(fontSize3);
                    run2.RunProperties = run2Properties;
                    InlineString inlineString1 = new InlineString();
                    inlineString1.Append(run2);
                    cell.Append(inlineString1);

                    Cell cell13 = InsertCellInWorksheet("D", 3, wsp);
                    cell13.CellValue = new CellValue("Year: " + Year);
                    cell13.DataType = new EnumValue<CellValues>(CellValues.Number);

                }
                Row t1 = new Row();
                t1.Append(
                    ConstructCell(" ", CellValues.String),
                    ConstructCell(" ", CellValues.String),
                    ConstructCell(" ", CellValues.String),
                      ConstructCell("Clustor Name", CellValues.String),
                       ConstructCell("Manager Name", CellValues.String));
                sd.AppendChild(t1);
                foreach (var item in result)
                {
                    row10 = new Row();
                    row10.Append(ConstructCell(" ".ToString(), CellValues.String),
                        ConstructCell(" ", CellValues.String),
                        ConstructCell(" ", CellValues.String),
                         ConstructCell(item.LocationName.ToString(), CellValues.String),
                                   ConstructCell(item.ManagerName, CellValues.String)
                                   );
                    sd.AppendChild(row10);
                }
                row5.Append(ConstructCell("", CellValues.String));
                sd.AppendChild(row5);
                row3.Append(ConstructCell(" ", CellValues.String));
                sd.AppendChild(row3);


                var actname = "";

                row6 = new Row();
                row5 = new Row();
                row1 = new Row();
                row4 = new Row();

                int countt = 7;
                List<ListActivityforProgamRegionSpendingDto> listActivityforProgamRegionSpendingDtos = new List<ListActivityforProgamRegionSpendingDto>();

                int costYearCnt = 0;

                if (costYearCnt == 0)
                {
                    row5.Append(
              ConstructCell("Clustor", CellValues.String, 2),
               ConstructCell("Activity Name", CellValues.String, 2),
               ConstructCell("Total Unit", CellValues.String, 2),
               ConstructCell("Unit Cost", CellValues.String, 2),
               ConstructCell("Total Unit Cost", CellValues.String, 2),
               ConstructCell("Community Contribution", CellValues.String, 2),
               ConstructCell("Funder Contribution", CellValues.String, 2),

               ConstructCell("Other Contribution", CellValues.String, 2),
               ConstructCell(" ", CellValues.String, 2));
                }
                else
                {
                    row5 = new Row();
                    row5.Append(
             ConstructCell("Clustor", CellValues.String, 2),

             ConstructCell("Activity Name", CellValues.String, 2),
             ConstructCell("Total Unit", CellValues.String, 2),
             ConstructCell("Unit Cost", CellValues.String, 2),
             ConstructCell("Total Unit Cost", CellValues.String, 2),
             ConstructCell("Community Contribution", CellValues.String, 2),
              ConstructCell("Funder Contribution", CellValues.String, 2),

             ConstructCell("Other Contribution", CellValues.String, 2),
             ConstructCell(" ", CellValues.String, 2));
                }
                costYearCnt++;

                for (var i = 0; i < countt; i++)
                {
                    if (i == countt / 2)
                    {
                        row6 = new Row();
                        row6.Append(
                   ConstructCell(Year, CellValues.String));
                        sd.AppendChild(row6);
                        sd.AppendChild(row5);
                    }
                    else
                    {
                        row6 = new Row();
                        row6.Append(
                ConstructCell(" ", CellValues.String));
                    }
                }
                foreach (var imp in result)
                {
                    var prevName = "";

                    foreach (var c in imp.CostEstimationYear)
                    {

                        //var re = c.ActionSubactionCost.OrderByDescending()
                        var sa = c.ActionSubactionCost.OrderBy(v => v.PrgActionAreaActivityMappingID);
                        foreach (var act in sa)
                        {

                            var LocationName = imp.LocationName;
                            if (act.ActivityLevel == ActivityLevel.ProgramLevel)
                            {
                                if (prevName != imp.LocationName)
                                {
                                    prevName = imp.LocationName;

                                    var villagename = act.VillageName == null ? " " : act.VillageName.ToString();
                                    row4 = new Row();
                                    row4.Append(
                                       ConstructCell(LocationName.ToString() + ' ' + villagename, CellValues.String, 1),
                                        ConstructCell(act.ActivityName.ToString(), CellValues.String, 1),
                                        ConstructCell(act.TotalUnits.ToString(), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.UnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                         ConstructCell(Convert.ToDecimal(act.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),

                                        ConstructCell(Convert.ToDecimal(act.OtherContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell("", CellValues.String, 1)
                                        );
                                    sd.AppendChild(row4);
                                }
                                else
                                {
                                    if (imp.LocationName == prevName)
                                    {
                                        prevName = imp.LocationName;
                                        var villagename = act.VillageName == null ? " " : act.VillageName.ToString();
                                        row4 = new Row();
                                        row4.Append(
                                           ConstructCell(" " + ' ' + villagename, CellValues.String, 1),
                                            ConstructCell(act.ActivityName.ToString(), CellValues.String, 1),
                                            ConstructCell(act.TotalUnits.ToString(), CellValues.Number, 1),
                                            ConstructCell(Convert.ToDecimal(act.UnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),

                                        ConstructCell(Convert.ToDecimal(act.OtherContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                            ConstructCell("", CellValues.String, 1)
                                            );
                                        sd.AppendChild(row4);
                                    }
                                }
                            }



                        }



                    }
                }
                foreach (var imp1 in result)
                {
                    var prevName = "";
                    foreach (var c in imp1.CostEstimationYear)
                    {

                        //var re = c.ActionSubactionCost.OrderByDescending()
                        var sa = c.ActionSubactionCost.OrderBy(v => v.PrgActionAreaActivityMappingID);
                        foreach (var act in sa)
                        {

                            var LocationName = imp1.LocationName;
                            if (act.ActivityLevel == ActivityLevel.VillageClusterLevel)
                            {
                                if (prevName != imp1.LocationName)
                                {
                                    prevName = imp1.LocationName;

                                    var villagename = act.VillageName == null ? " " : act.VillageName.ToString();
                                    row4 = new Row();
                                    row4.Append(
                                       ConstructCell(LocationName.ToString() + ' ' + villagename, CellValues.String, 1),
                                        ConstructCell(act.ActivityName.ToString(), CellValues.String, 1),
                                        ConstructCell(act.TotalUnits.ToString(), CellValues.Number, 1),
                                         ConstructCell(Convert.ToDecimal(act.UnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),

                                        ConstructCell(Convert.ToDecimal(act.OtherContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell("", CellValues.String, 1)
                                        );
                                    sd.AppendChild(row4);
                                }
                                else
                                {
                                    if (imp1.LocationName == prevName)
                                    {
                                        prevName = imp1.LocationName;
                                        var villagename = act.VillageName == null ? " " : act.VillageName.ToString();
                                        row4 = new Row();
                                        row4.Append(
                                           ConstructCell(" " + ' ' + villagename, CellValues.String, 1),
                                            ConstructCell(act.ActivityName.ToString(), CellValues.String, 1),
                                            ConstructCell(act.TotalUnits.ToString(), CellValues.Number, 1),
                                             ConstructCell(Convert.ToDecimal(act.UnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.OtherContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                            ConstructCell("", CellValues.String, 1)
                                            );
                                        sd.AppendChild(row4);
                                    }
                                }
                            }



                        }


                    }
                }
                foreach (var imp1 in result)
                {
                    var prevName = "";
                    foreach (var c in imp1.CostEstimationYear)
                    {



                        //var re = c.ActionSubactionCost.OrderByDescending()
                        var sa = c.ActionSubactionCost.OrderBy(v => v.PrgActionAreaActivityMappingID);
                        foreach (var act in sa)
                        {

                            var LocationName = imp1.LocationName;
                            if (act.ActivityLevel == ActivityLevel.VillageLevel)
                            {
                                if (prevName != imp1.LocationName)
                                {
                                    prevName = imp1.LocationName;

                                    var villagename = act.VillageName == null ? " " : act.VillageName.ToString();
                                    row4 = new Row();
                                    row4.Append(
                                       ConstructCell(LocationName.ToString() + ' ' + villagename, CellValues.String, 1),
                                        ConstructCell(act.ActivityName.ToString(), CellValues.String, 1),
                                        ConstructCell(act.TotalUnits.ToString(), CellValues.Number, 1),
                                         ConstructCell(Convert.ToDecimal(act.UnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.OtherContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell("", CellValues.String, 1)
                                        );
                                    sd.AppendChild(row4);
                                }
                                else
                                {
                                    if (imp1.LocationName == prevName)
                                    {
                                        prevName = imp1.LocationName;
                                        var villagename = act.VillageName == null ? " " : act.VillageName.ToString();
                                        row4 = new Row();
                                        row4.Append(
                                           ConstructCell(" " + ' ' + villagename, CellValues.String, 1),
                                            ConstructCell(act.ActivityName.ToString(), CellValues.String, 1),
                                            ConstructCell(act.TotalUnits.ToString(), CellValues.Number, 1),
                                             ConstructCell(Convert.ToDecimal(act.UnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.OtherContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                            ConstructCell("", CellValues.String, 1)
                                            );
                                        sd.AppendChild(row4);
                                    }
                                }
                            }



                        }



                    }
                }
                foreach (var imp1 in result)
                {
                    var prevName = "";
                    foreach (var c in imp1.CostEstimationYear)
                    {

                        //var re = c.ActionSubactionCost.OrderByDescending()
                        var sa = c.ActionSubactionCost.OrderBy(v => v.PrgActionAreaActivityMappingID);
                        foreach (var act in sa)
                        {

                            var LocationName = imp1.LocationName;
                            if (act.ActivityLevel == ActivityLevel.RRCLevel)
                            {
                                if (prevName != imp1.LocationName)
                                {
                                    prevName = imp1.LocationName;

                                    var villagename = act.VillageName == null ? " " : act.VillageName.ToString();
                                    row4 = new Row();
                                    row4.Append(
                                       ConstructCell(LocationName.ToString() + ' ' + villagename, CellValues.String, 1),
                                        ConstructCell(act.ActivityName.ToString(), CellValues.String, 1),
                                        ConstructCell(act.TotalUnits.ToString(), CellValues.Number, 1),
                                         ConstructCell(Convert.ToDecimal(act.UnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.OtherContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell("", CellValues.String, 1)
                                        );
                                    sd.AppendChild(row4);
                                }
                                else
                                {
                                    if (imp1.LocationName == prevName)
                                    {
                                        prevName = imp1.LocationName;
                                        var villagename = act.VillageName == null ? " " : act.VillageName.ToString();
                                        row4 = new Row();
                                        row4.Append(
                                           ConstructCell(" " + ' ' + villagename, CellValues.String, 1),
                                            ConstructCell(act.ActivityName.ToString(), CellValues.String, 1),
                                            ConstructCell(act.TotalUnits.ToString(), CellValues.Number, 1),
                                             ConstructCell(Convert.ToDecimal(act.UnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.FunderContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                        ConstructCell(Convert.ToDecimal(act.OtherContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                            ConstructCell("", CellValues.String, 1)
                                            );
                                        sd.AppendChild(row4);
                                    }
                                }
                            }



                        }
                    }
                }


                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();
            }

            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;
        }

        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
        private Cell ConstructCell(string value, CellValues dataType, uint styleIndex = 0)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType),
                StyleIndex = styleIndex
            };
        }

        private Stylesheet GenerateStylesheet()
        {

            Stylesheet styleSheet = null;

            Fonts fonts = new Fonts(
                new Font( // Index 0 - default
                    new FontSize() { Val = 11 }

                ),
                new Font( // Index 1 - header
                    new FontSize() { Val = 11 },
                    new Bold(),
                    new Color() { Rgb = "000000" }

                ));

            Fills fills = new Fills(
                    new Fill(new PatternFill() { PatternType = PatternValues.None }), // Index 0 - default
                    new Fill(new PatternFill() { PatternType = PatternValues.Gray125 }), // Index 1 - default
                    new Fill(new PatternFill(new ForegroundColor { Rgb = new HexBinaryValue() { Value = "D0CECE" } })
                    { PatternType = PatternValues.Solid }) // Index 2 - header
                );

            Borders borders = new Borders(
                    new Border(), // index 0 default
                    new Border( // index 1 black border
                        new LeftBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new RightBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new TopBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new BottomBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                );

            CellFormats cellFormats = new CellFormats(
                    new CellFormat(), // default
                    new CellFormat { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }, // body
                    new CellFormat { FontId = 1, FillId = 2, BorderId = 1, ApplyFill = true } // header
                );

            styleSheet = new Stylesheet(fonts, fills, borders, cellFormats);

            return styleSheet;

        }

        public async Task<List<RequestAAPlasListDto>> GetReQuestApplaForprogramId(int programId)
        {

            var subUnitActivity = new List<ListActionSubactionListDto>();
            var query = await (from r in _RequestAAplasRepository.GetAll().Where(p => p.ProgramID == programId && p.RequestStatus != RequestStatus.RequestSend)
                               group r by new
                               {
                                   r.LocationID,
                                   r.ManagerID,
                                   r.ProgramID
                               } into gcs
                               join l in _LocationRepository.GetAll()
                              on gcs.Key.LocationID equals l.Id
                               join p in _ProgrameRepository.GetAll()
                               on gcs.Key.ProgramID equals p.Id


                               select new RequestAAPlasListDto()
                               {
                                   ProgramID = gcs.Key.ProgramID,
                                   ProgramName = p.Name,
                                   ProgrameStartDate = p.ProgramStartDate,
                                   ProgrameEndDate = p.PrgramEndDate,
                                   LocationID = gcs.Key.LocationID,
                                   ManagerID = gcs.Key.ManagerID,
                                   ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == gcs.Key.ManagerID).Name,
                                   LocationName = l.Name,
                                   Id = l.Id,
                                   CostEstimationYear = (from q in _RequestAAplasRepository.GetAll().Where(ma => ma.LocationID == gcs.Key.LocationID && ma.ProgramID == gcs.Key.ProgramID)
                                                         group q by new
                                                         {
                                                             q.CostEstimationYear,
                                                             q.RequestStatus
                                                         } into gcs1

                                                         select new ProgrameCostEstimationYearRequestApplDto()
                                                         {
                                                             Id = l.Id,
                                                             LocationID = gcs.Key.LocationID,
                                                             ManagerID = gcs.Key.ManagerID,
                                                             ProgramID = gcs.Key.ProgramID,
                                                             CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                             RequestStatus = gcs1.Key.RequestStatus,
                                                             ActionSubactionCost = (from a in _RequestAAplasRepository.GetAll().Where(mn => mn.LocationID == gcs.Key.LocationID
                                                                                    && mn.CostEstimationYear == gcs1.Key.CostEstimationYear && mn.ProgramID == gcs.Key.ProgramID && mn.ManagerID == gcs.Key.ManagerID)

                                                                                    join pa in _PrgActionAreaActivityMappingRepository.GetAll()
                                                                                    on a.PrgActionAreaActivityMappingID equals pa.Id
                                                                                    join pc in _PrgCostEstRepository.GetAll()
                                                                                   on pa.ActivityID equals pc.ActivityID
                                                                                    where (pc.ComponentID == pa.ComponentID && pc.CostEstimationYear == gcs1.Key.CostEstimationYear && pc.ProgramID == gcs.Key.ProgramID)
                                                                                    select new ListActionSubactionListDto()
                                                                                    {
                                                                                        Id = a.Id,
                                                                                        ActionAreaName = a.VillageID != null ? string.Empty : pa.ActionArea.Name,
                                                                                        SubActionAreaName = a.VillageID != null ? string.Empty : pa.SubActionArea.Name,
                                                                                        ActivityName = a.VillageID != null ? string.Empty : pa.Activity.Name,
                                                                                        ActivityLevel = pc.ActivityLevel,
                                                                                        UnitOfMeasuresName = a.UnitOfMeasures.Name,
                                                                                        UnitOfMeasuresID = a.UnitOfMeasuresID,
                                                                                        //ChecklistID = a.ChecklistID,
                                                                                        PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                        VillageID = a.VillageID,
                                                                                        VillageName = a.Village.Name,
                                                                                        TotalUnits = a.TotalUnits,
                                                                                        UnitCost = a.UnitCost,
                                                                                        TotalUnitCost = a.TotalUnitCost,
                                                                                        CommunityContribution = a.CommunityContribution,
                                                                                        FunderContribution = a.FunderContribution,
                                                                                        OtherContribution = a.OtherContribution,
                                                                                        RequestStatus = a.RequestStatus,
                                                                                        Quarter1PhysicalUnits = a.Quarter1PhysicalUnits,
                                                                                        Quarter1FinancialRs = a.Quarter1FinancialRs,

                                                                                        Quarter2PhysicalUnits = a.Quarter2PhysicalUnits,
                                                                                        Quarter2FinancialRs = a.Quarter2FinancialRs,

                                                                                        Quarter3PhysicalUnits = a.Quarter3PhysicalUnits,
                                                                                        Quarter3FinancialRs = a.Quarter3FinancialRs,

                                                                                        Quarter4PhysicalUnits = a.Quarter4PhysicalUnits,
                                                                                        Quarter4FinancialRs = a.Quarter4FinancialRs,
                                                                                        SubActionSubActionActivity = subUnitActivity,


                                                                                    }).ToList(),
                                                         }).ToList(),

                               }).ToListAsync();

            return new List<RequestAAPlasListDto>(ObjectMapper.Map<List<RequestAAPlasListDto>>(query));


        }


        // To show data for UI only
        public async Task<List<RequestAAPlasListDto>> GetApproveApplaForprogramId(int programId, string Year)
        {

            var subUnitActivity = new List<ListActionSubactionListDto>();
            var query = await (from r in _RequestAAplasRepository.GetAll().Where(p => p.ProgramID == programId && p.RequestStatus == RequestStatus.Approved && p.CostEstimationYear == Year)
                               group r by new
                               {
                                   r.LocationID,
                                   r.ManagerID,
                                   r.ProgramID
                               } into gcs
                               join l in _LocationRepository.GetAll()
                              on gcs.Key.LocationID equals l.Id
                               join p in _ProgrameRepository.GetAll()
                               on gcs.Key.ProgramID equals p.Id


                               select new RequestAAPlasListDto()
                               {
                                   ProgramID = gcs.Key.ProgramID,
                                   ProgramName = p.Name,
                                   ProgrameStartDate = p.ProgramStartDate,
                                   ProgrameEndDate = p.PrgramEndDate,
                                   LocationID = gcs.Key.LocationID,
                                   ManagerID = gcs.Key.ManagerID,
                                   ManagerName = UserManager.Users.FirstOrDefault(u => u.Id == gcs.Key.ManagerID).Name,
                                   LocationName = l.Name,
                                   Id = l.Id,
                                   CostEstimationYear = (from q in _RequestAAplasRepository.GetAll().Where(ma => ma.LocationID == gcs.Key.LocationID && ma.RequestStatus == RequestStatus.Approved && ma.ProgramID == gcs.Key.ProgramID && ma.CostEstimationYear == Year)
                                                         group q by new
                                                         {
                                                             q.CostEstimationYear,
                                                             q.RequestStatus
                                                         } into gcs1

                                                         select new ProgrameCostEstimationYearRequestApplDto()
                                                         {
                                                             Id = l.Id,
                                                             LocationID = gcs.Key.LocationID,
                                                             ManagerID = gcs.Key.ManagerID,
                                                             ProgramID = gcs.Key.ProgramID,
                                                             CostEstimationYear = gcs1.Key.CostEstimationYear,
                                                             RequestStatus = gcs1.Key.RequestStatus,
                                                             ActionSubactionCost = (from a in _RequestAAplasRepository.GetAll().Where(mn => mn.LocationID == gcs.Key.LocationID && mn.RequestStatus == RequestStatus.Approved
                                                                                    && mn.CostEstimationYear == Year && mn.ProgramID == gcs.Key.ProgramID && mn.ManagerID == gcs.Key.ManagerID)

                                                                                    join pa in _PrgActionAreaActivityMappingRepository.GetAll()
                                                                                    on a.PrgActionAreaActivityMappingID equals pa.Id
                                                                                    join pc in _PrgCostEstRepository.GetAll()
                                                                                   on pa.ActivityID equals pc.ActivityID
                                                                                    where (pc.ComponentID == pa.ComponentID && pc.CostEstimationYear == Year && pc.ProgramID == gcs.Key.ProgramID)
                                                                                    select new ListActionSubactionListDto()
                                                                                    {
                                                                                        Id = a.Id,
                                                                                        ActionAreaName = a.VillageID != null ? string.Empty : pa.ActionArea.Name,
                                                                                        SubActionAreaName = a.VillageID != null ? string.Empty : pa.SubActionArea.Name,
                                                                                        ActivityName = a.VillageID != null ? string.Empty : pa.Activity.Name,
                                                                                        ActivityLevel = pc.ActivityLevel,
                                                                                        UnitOfMeasuresName = a.UnitOfMeasures.Name,
                                                                                        UnitOfMeasuresID = a.UnitOfMeasuresID,
                                                                                        //ChecklistID = a.ChecklistID,
                                                                                        PrgActionAreaActivityMappingID = a.PrgActionAreaActivityMappingID,
                                                                                        VillageID = a.VillageID,
                                                                                        VillageName = a.Village.Name,
                                                                                        TotalUnits = a.TotalUnits,
                                                                                        UnitCost = a.UnitCost,
                                                                                        TotalUnitCost = a.TotalUnitCost,
                                                                                        CommunityContribution = a.CommunityContribution,
                                                                                        FunderContribution = a.FunderContribution,
                                                                                        OtherContribution = a.OtherContribution,
                                                                                        RequestStatus = a.RequestStatus,
                                                                                        Quarter1PhysicalUnits = a.Quarter1PhysicalUnits,
                                                                                        Quarter1FinancialRs = a.Quarter1FinancialRs,

                                                                                        Quarter2PhysicalUnits = a.Quarter2PhysicalUnits,
                                                                                        Quarter2FinancialRs = a.Quarter2FinancialRs,

                                                                                        Quarter3PhysicalUnits = a.Quarter3PhysicalUnits,
                                                                                        Quarter3FinancialRs = a.Quarter3FinancialRs,

                                                                                        Quarter4PhysicalUnits = a.Quarter4PhysicalUnits,
                                                                                        Quarter4FinancialRs = a.Quarter4FinancialRs,
                                                                                        SubActionSubActionActivity = subUnitActivity,


                                                                                    }).ToList(),
                                                         }).ToList(),

                               }).ToListAsync();

            return new List<RequestAAPlasListDto>(ObjectMapper.Map<List<RequestAAPlasListDto>>(query));


        }



        //export to Excel for Status of the Annual Action Plan Approval
        public async Task<FileDto> CreateApproveapplaExcelDoc(string fileName, int ProgramId, string costestimationyear)
        {
            try
            {
                // var result = await GetRequestSendDetailsProjectManager(ProgramId, costestimationyear);

                var result = await GetApproveApplaForprogramId(ProgramId, costestimationyear);
                string prg_manager_name = UserManager.Users.Where(r => r.Id == AbpSession.UserId).SingleOrDefault()?.Name;
                long? programmngr = _programRepository.GetAll().Where(xx => xx.Id == ProgramId).FirstOrDefault().ManagerID;
                var pname = UserManager.Users.Where(r => r.Id == programmngr).SingleOrDefault()?.Name;

                var prgmanager = (from v in _villageClusterRepository.GetAll().Where(cc => cc.ProgramID == ProgramId)
                                  join
                                  u in UserManager.Users on v.UserId equals u.Id
                                  join ur in _userRoleRepository.GetAll() on u.Id equals ur.UserId
                                  join r in _abproleRepository.GetAll() on ur.RoleId equals r.Id
                                  where (r.Name.Trim().ToUpper() == "ProgramManager")
                                  select new GetAllManagerDto()
                                  {
                                      Name = u.FullName,
                                      ManagerId = u.Id,
                                  }).OrderBy(r => r.Name).Distinct().ToList();



                string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";

                using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))
                {
                    var wbp = xl.AddWorkbookPart();
                    var wsp = wbp.AddNewPart<WorksheetPart>();
                    var wb = new Workbook();
                    var fv = new FileVersion();
                    fv.ApplicationName = "Microsoft Office Excel";

                    Worksheet ws = new Worksheet();
                    SheetData sd = new SheetData();
                    WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                    stylePart.Stylesheet = GenerateStylesheet();
                    stylePart.Stylesheet.Save();
                    var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                    var dp = wsp.AddNewPart<DrawingsPart>();
                    var imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                    using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                    {
                        imgp.FeedData(fs);
                    }

                    var nvdp = new NonVisualDrawingProperties();
                    nvdp.Id = 1025;
                    nvdp.Name = "WOTRLogoReport";
                    nvdp.Description = "WOTRLogoReport";
                    DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                    picLocks.NoChangeAspect = true;
                    picLocks.NoChangeArrowheads = true;
                    var nvpdp = new NonVisualPictureDrawingProperties();
                    nvpdp.PictureLocks = picLocks;
                    var nvpp = new NonVisualPictureProperties();
                    nvpp.NonVisualDrawingProperties = nvdp;
                    nvpp.NonVisualPictureDrawingProperties = nvpdp;

                    DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                    stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                    var blipFill = new BlipFill();
                    DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                    blip.Embed = dp.GetIdOfPart(imgp);
                    blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                    blipFill.Blip = blip;
                    blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                    blipFill.Append(stretch);

                    DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                    DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                    offset.X = 0;
                    offset.Y = 0;
                    t2d.Offset = offset;
                    var bm = new Bitmap(sImagePath);
                    //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                    //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                    //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                    DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                    extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                    extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                    bm.Dispose();
                    t2d.Extents = extents;
                    var sp = new ShapeProperties();
                    sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                    sp.Transform2D = t2d;
                    DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                    prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                    prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                    sp.Append(prstGeom);
                    sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                    DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                    picture.NonVisualPictureProperties = nvpp;
                    picture.BlipFill = blipFill;
                    picture.ShapeProperties = sp;

                    var pos = new Position();
                    pos.X = 0;
                    pos.Y = 0;
                    var ext = new Extent();
                    ext.Cx = extents.Cx;
                    ext.Cy = extents.Cy;
                    var anchor = new AbsoluteAnchor();
                    anchor.Position = pos;
                    anchor.Extent = ext;
                    anchor.Append(picture);
                    anchor.Append(new ClientData());
                    var wsd = new WorksheetDrawing();
                    wsd.Append(anchor);
                    var drawing = new Drawing();
                    drawing.Id = dp.GetIdOfPart(imgp);

                    wsd.Save(dp);

                    ws.Append(sd);
                    ws.Append(drawing);
                    wsp.Worksheet = ws;
                    wsp.Worksheet.Save();
                    var sheets = new Sheets();
                    var sheet = new Sheet();
                    sheet.Name = "Sheet1";
                    sheet.SheetId = 1;
                    sheet.Id = wbp.GetIdOfPart(wsp);
                    wb.Append(fv);

                    // Constructing header
                    var row1 = new Row();
                    var row3 = new Row();
                    var row5 = new Row();
                    var row10 = new Row();
                    Row row4 = new Row();
                    Row row6 = new Row();
                    Row row7 = new Row();
                    Row row8 = new Row();
                    Row row9 = new Row();


                    foreach (var program in result)
                    {
                        var cell8 = InsertCellInWorksheet("D", 1, wsp);
                        cell8.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                        cell8.DataType = new EnumValue<CellValues>(CellValues.Number);

                        var cellString8 = InsertCellInWorksheet("D", 1, wsp);

                        cellString8.CellValue = new CellValue("Watershed Organisation Trust(WOTR)");
                        cellString8.DataType = new EnumValue<CellValues>(CellValues.String);
                        cell8.StyleIndex = 1;
                        Run run1 = new Run();
                        run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                        RunProperties run1Properties = new RunProperties();
                        run1Properties.Append(new Bold());
                        run1.RunProperties = run1Properties;
                        InlineString inlineString = new InlineString();
                        inlineString.Append(run1);
                        cell8.Append(inlineString);


                        var cell31 = InsertCellInWorksheet("D", 2, wsp);
                        cell31.CellValue = new CellValue("Status of the Annual Action Plan Approval");
                        cell31.DataType = new EnumValue<CellValues>(CellValues.Number);

                        Run run2 = new Run();
                        run2.Append(new Text("Status of the Annual Action Plan Approval"));
                        RunProperties run2Properties = new RunProperties();
                        Color color3 = new Color() { Rgb = "2F75B5" };
                        FontSize fontSize3 = new FontSize() { Val = 16D };
                        run2Properties.Append(new Bold());
                        run2Properties.Append(color3);
                        run2Properties.Append(fontSize3);
                        run2.RunProperties = run2Properties;
                        InlineString inlineString1 = new InlineString();
                        inlineString1.Append(run2);
                        cell31.Append(inlineString1);



                        var cell34 = InsertCellInWorksheet("C", 3, wsp);
                        cell34.CellValue = new CellValue("Program Title ");
                        cell34.DataType = new EnumValue<CellValues>(CellValues.Number);




                        var cellString = InsertCellInWorksheet("D", 3, wsp);
                        cellString.CellValue = new CellValue(program.ProgramName.ToString());
                        cellString.DataType = new EnumValue<CellValues>(CellValues.String);





                        var cell2 = InsertCellInWorksheet("C", 4, wsp);
                        cell2.CellValue = new CellValue("Year");
                        cell2.DataType = new EnumValue<CellValues>(CellValues.Number);

                        var cellString1 = InsertCellInWorksheet("D", 4, wsp);
                        cellString1.CellValue = new CellValue(program.CostEstimationYear[0].CostEstimationYear.ToString());
                        cellString1.DataType = new EnumValue<CellValues>(CellValues.String);

                        break;
                    }

                    var TotalTotalUnits = 0.00;
                    var Totalunitcost = 0.00;
                    var TotalTotalUnitCost = 0.00;
                    var TotalCommunity_Contri = 0.00;
                    var TotalFunder_Contri = 0.00;
                    var TotalOther_Contri = 0.00;
                    var TotalQuarter1PhysicalUnits = 0.00;
                    var TotalQuarter1FinancialRs = 0.00;
                    var TotalQuarter2PhysicalUnits = 0.00;
                    var TotalQuarter2FinancialRs = 0.00;
                    var TotalQuarter3PhysicalUnits = 0.00;
                    var TotalQuarter3FinancialRs = 0.00;
                    var TotalQuarter4PhysicalUnits = 0.00;
                    var TotalQuarter4FinancialRs = 0.00;
                    var Totalothercontribution = 0.00;


                    foreach (var program in result)
                    {
                        row1 = new Row();
                        row3 = new Row();
                        row5 = new Row();
                        row10 = new Row();
                        row4 = new Row();
                        row6 = new Row();
                        row7 = new Row();
                        row8 = new Row();
                        row9 = new Row();

                        //var cellString2 = InsertCellInWorksheet("D", 4, wsp);
                        //cellString2.CellValue = new CellValue(costestimationyear.ToString());
                        //cellString2.DataType = new EnumValue<CellValues>(CellValues.String);
                        Row t1 = new Row();
                        t1.Append(
                              ConstructCell(" ", CellValues.String),
                              ConstructCell("Clustor Name", CellValues.String),
                              ConstructCell("Program Manager", CellValues.String),
                               ConstructCell("Project Manager", CellValues.String) );
                        sd.AppendChild(t1);
                        var prevName = "";

                        //var ProgramMangerName = prgmanager[0].Name.ToString();
                        var ProgramMangerName = "";
                        if (pname != "")
                        {
                            ProgramMangerName = pname;
                        }
                        else
                        {
                            ProgramMangerName = prg_manager_name;
                        }
                        if (prevName != ProgramMangerName) {
                            prevName = ProgramMangerName;
                            row10 = new Row();
                            row10.Append(ConstructCell("", CellValues.String),
                                ConstructCell(program.LocationName.ToString(), CellValues.String),
                                ConstructCell(ProgramMangerName, CellValues.String),
                                ConstructCell(program.ManagerName, CellValues.String) );
                            sd.AppendChild(row10);
                        } else  {
                            if (ProgramMangerName == prevName)  {
                                row10 = new Row();
                                row10.Append(ConstructCell("", CellValues.String),
                                    ConstructCell(program.LocationName.ToString(), CellValues.String),
                                    ConstructCell("", CellValues.String),
                                    ConstructCell(program.ManagerName, CellValues.String));
                                sd.AppendChild(row10);
                            }
                        }

                        #region RESET Total
                        TotalTotalUnits = 0.00;
                        Totalunitcost = 0.00;
                        TotalTotalUnitCost = 0.00;
                        TotalCommunity_Contri = 0.00;
                        TotalFunder_Contri = 0.00;
                        TotalOther_Contri = 0.00;
                        TotalQuarter1PhysicalUnits = 0.00;
                        TotalQuarter1FinancialRs = 0.00;
                        TotalQuarter2PhysicalUnits = 0.00;
                        TotalQuarter2FinancialRs = 0.00;
                        TotalQuarter3PhysicalUnits = 0.00;
                        TotalQuarter3FinancialRs = 0.00;
                        TotalQuarter4PhysicalUnits = 0.00;
                        TotalQuarter4FinancialRs = 0.00;
                        Totalothercontribution = 0.00;
                        #endregion


                        #region Bind Headers
                        row5.Append(
                         ConstructCell("Activity", CellValues.String, 2),
                         ConstructCell("TotalUnit ", CellValues.String, 2),
                         ConstructCell("Unit Cost", CellValues.String, 2),
                         ConstructCell("Total Unit Cost", CellValues.Number, 2),
                         ConstructCell("Community Contribution", CellValues.Number, 2),
                         ConstructCell("Funder Contribution", CellValues.Number, 2),
                         ConstructCell("Other Contribution", CellValues.Number, 2),
                         ConstructCell("Quarter 1", CellValues.Number, 2),
                         ConstructCell(" ", CellValues.Number, 2),
                         ConstructCell("Quarter 2 ", CellValues.Number, 2),
                         ConstructCell(" ", CellValues.Number, 2),
                         ConstructCell("Quarter 3 ", CellValues.Number, 2),
                         ConstructCell(" ", CellValues.Number, 2),
                         ConstructCell("Quarter 4 ", CellValues.Number, 2),
                         ConstructCell(" ", CellValues.Number, 2));

                        sd.AppendChild(row5);
                        row1.Append(
                            ConstructCell("", CellValues.String, 2),
                            ConstructCell(" ", CellValues.String, 2),
                            ConstructCell("", CellValues.String, 2),
                            ConstructCell("", CellValues.Number, 2),
                            ConstructCell("", CellValues.Number, 2),
                            ConstructCell("", CellValues.Number, 2),
                            ConstructCell("", CellValues.Number, 2),
                            ConstructCell(" Unit", CellValues.Number, 2),
                            ConstructCell(" Rs", CellValues.Number, 2),
                            ConstructCell(" Unit", CellValues.Number, 2),
                            ConstructCell(" Rs", CellValues.Number, 2),
                            ConstructCell(" Unit", CellValues.Number, 2),
                            ConstructCell(" Rs", CellValues.Number, 2),
                            ConstructCell(" unit", CellValues.Number, 2),
                            ConstructCell(" Rs", CellValues.Number, 2));
                        sd.AppendChild(row1);
                        #endregion
                        var prevNames = "";


                        var p = program.CostEstimationYear[0].ActionSubactionCost;
                        foreach (var res in program.CostEstimationYear[0].ActionSubactionCost)
                        {
                            if (res.ActivityName.Length > 0  && res.ActionAreaName.Length > 0 ) {
                               var LocationName = program.LocationName;
                                    prevName = program.LocationName;
                                    var villagename = res.VillageName == null ? " " : res.VillageName.ToString();
                                    row5 = new Row();
                                #region append row values
                                row5.Append(
                        ConstructCell(res.ActivityName.ToString(), CellValues.String, 1),
                        ConstructCell(res.TotalUnits.ToString(), CellValues.Number, 1),
                        ConstructCell(res.UnitCost.ToString(), CellValues.Number, 1),
                        ConstructCell(res.TotalUnitCost.ToString(), CellValues.Number, 1),
                        ConstructCell(res.CommunityContribution.ToString(), CellValues.Number, 1),
                        ConstructCell(res.FunderContribution.ToString(), CellValues.Number, 1),
                        ConstructCell(res.OtherContribution.ToString(), CellValues.Number, 1),
                        ConstructCell(res.Quarter1PhysicalUnits.ToString(), CellValues.Number, 1),
                        ConstructCell(res.Quarter1FinancialRs.ToString(), CellValues.Number, 1),
                        ConstructCell(res.Quarter2PhysicalUnits.ToString(), CellValues.Number, 1),
                        ConstructCell(res.Quarter2FinancialRs.ToString(), CellValues.Number, 1),
                        ConstructCell(res.Quarter3PhysicalUnits.ToString(), CellValues.Number, 1),
                        ConstructCell(res.Quarter3FinancialRs.ToString(), CellValues.Number, 1),
                        ConstructCell(res.Quarter4PhysicalUnits.ToString(), CellValues.Number, 1),
                        ConstructCell(res.Quarter4FinancialRs.ToString(), CellValues.Number, 1));
                                #endregion

                                #region Add total
                                TotalTotalUnits += Convert.ToInt64(res.TotalUnits);
                                    Totalunitcost += Convert.ToInt64(res.UnitCost);
                                    TotalTotalUnitCost += Convert.ToInt64(res.TotalUnitCost);
                                    TotalCommunity_Contri += Convert.ToInt64(res.CommunityContribution);
                                    TotalFunder_Contri += Convert.ToInt64(res.FunderContribution);
                                    TotalOther_Contri += Convert.ToInt64(res.OtherContribution);
                                    TotalQuarter1PhysicalUnits += Convert.ToDouble(res.Quarter1PhysicalUnits);
                                    TotalQuarter1FinancialRs += Convert.ToDouble(res.Quarter1FinancialRs);
                                    TotalQuarter2PhysicalUnits += Convert.ToDouble(res.Quarter2PhysicalUnits);
                                    TotalQuarter2FinancialRs += Convert.ToDouble(res.Quarter2FinancialRs);
                                    TotalQuarter3PhysicalUnits += Convert.ToDouble(res.Quarter3PhysicalUnits);
                                    TotalQuarter3FinancialRs += Convert.ToDouble(res.Quarter3FinancialRs);
                                    TotalQuarter4PhysicalUnits += Convert.ToDouble(res.Quarter4PhysicalUnits);
                                    TotalQuarter4FinancialRs += Convert.ToDouble(res.Quarter4FinancialRs);
                                    Totalothercontribution += Convert.ToDouble(res.OtherContribution);
                                #endregion
                                sd.AppendChild(row5);
                            

                            foreach (var v in p)
                            {
                                if (res.PrgActionAreaActivityMappingID == v.PrgActionAreaActivityMappingID && v.ActivityName.Length == 0 && v.ActionAreaName.Length == 0) {
                                    if (res.PrgActionAreaActivityMappingID == v.PrgActionAreaActivityMappingID && v.ActivityName.Length == 0 && v.ActionAreaName.Length ==0)
                                    {
                                       var LocationName1 = program.LocationName;
                                        prevName = program.LocationName;
                                        var villagename1 = res.VillageName == null ? " " : res.VillageName.ToString();
                                        row5 = new Row();
                                        #region append row values
                                        row5.Append(
                            ConstructCell(v.ActivityName.ToString(), CellValues.String, 1),
                            ConstructCell(v.TotalUnits.ToString(), CellValues.Number, 1),
                            ConstructCell(v.UnitCost.ToString(), CellValues.Number, 1),
                            ConstructCell(v.TotalUnitCost.ToString(), CellValues.Number, 1),
                            ConstructCell(v.CommunityContribution.ToString(), CellValues.Number, 1),
                            ConstructCell(v.FunderContribution.ToString(), CellValues.Number, 1),
                            ConstructCell(v.OtherContribution.ToString(), CellValues.Number, 1),
                            ConstructCell(v.Quarter1PhysicalUnits.ToString(), CellValues.Number, 1),
                            ConstructCell(v.Quarter1FinancialRs.ToString(), CellValues.Number, 1),
                            ConstructCell(v.Quarter2PhysicalUnits.ToString(), CellValues.Number, 1),
                            ConstructCell(v.Quarter2FinancialRs.ToString(), CellValues.Number, 1),
                            ConstructCell(v.Quarter3PhysicalUnits.ToString(), CellValues.Number, 1),
                            ConstructCell(v.Quarter3FinancialRs.ToString(), CellValues.Number, 1),
                            ConstructCell(v.Quarter4PhysicalUnits.ToString(), CellValues.Number, 1),
                            ConstructCell(v.Quarter4FinancialRs.ToString(), CellValues.Number, 1));
                                        #endregion

                                        #region Add total
                                        TotalTotalUnits += Convert.ToInt64(v.TotalUnits);
                                        Totalunitcost += Convert.ToInt64(v.UnitCost);
                                        TotalTotalUnitCost += Convert.ToInt64(v.TotalUnitCost);
                                        TotalCommunity_Contri += Convert.ToInt64(v.CommunityContribution);
                                        TotalFunder_Contri += Convert.ToInt64(v.FunderContribution);
                                        TotalOther_Contri += Convert.ToInt64(v.OtherContribution);
                                        TotalQuarter1PhysicalUnits += Convert.ToDouble(v.Quarter1PhysicalUnits);
                                        TotalQuarter1FinancialRs += Convert.ToDouble(v.Quarter1FinancialRs);
                                        TotalQuarter2PhysicalUnits += Convert.ToDouble(v.Quarter2PhysicalUnits);
                                        TotalQuarter2FinancialRs += Convert.ToDouble(v.Quarter2FinancialRs);
                                        TotalQuarter3PhysicalUnits += Convert.ToDouble(v.Quarter3PhysicalUnits);
                                        TotalQuarter3FinancialRs += Convert.ToDouble(v.Quarter3FinancialRs);
                                        TotalQuarter4PhysicalUnits += Convert.ToDouble(v.Quarter4PhysicalUnits);
                                        TotalQuarter4FinancialRs += Convert.ToDouble(v.Quarter4FinancialRs);
                                        Totalothercontribution += Convert.ToDouble(v.OtherContribution);
                                        #endregion
                                        sd.AppendChild(row5);
                                    }
                                }
                            }
                            }
                        }

                        Row row18 = new Row();

                        row18.Append(
                              ConstructCell("Total", CellValues.String, 2),
                                 ConstructCell(TotalTotalUnits.ToString(), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(Totalunitcost).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(TotalTotalUnitCost).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(TotalCommunity_Contri).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(TotalFunder_Contri).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(TotalOther_Contri).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(TotalQuarter1PhysicalUnits).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(TotalQuarter1FinancialRs).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(TotalQuarter2PhysicalUnits).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(TotalQuarter2FinancialRs).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(TotalQuarter3PhysicalUnits).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(TotalQuarter3FinancialRs).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(TotalQuarter4PhysicalUnits).ToString("#,##0.00"), CellValues.Number, 2),
                                 ConstructCell(Convert.ToDecimal(TotalQuarter4FinancialRs).ToString("#,##0.00"), CellValues.Number, 2)
                                );
                        sd.Append(row18);

                        row9.Append(ConstructCell(" ", CellValues.String));
                    }

                    sheets.Append(sheet);
                    wb.Append(sheets);
                    xl.WorkbookPart.Workbook = wb;
                    wbp.Workbook.Save();

                }
               

                var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                return file;
            }
            catch (Exception ex)
            {

                throw;
            }


        }



        public async Task ConnectionConnect(SqlConnection conn)
        {
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    await conn.OpenAsync();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task AddDataInDataTable(ProjectWiseDashboardData projectWiseDashboardData, int fillDataTable = 0)
        {
            try
            {
                foreach (DataRow row in projectWiseDashboardData.dataTable.Rows)
                {
                    if (fillDataTable == 1)
                    {
                        projectWiseDashboardData.projectWiseInfoDTOs.Add(new ProjectWiseInfoDTO
                        {
                            ProjectName = (row["ProjectName"] == DBNull.Value) ? "" : Convert.ToString(row["ProjectName"]),
                            ProjectManager = (row["ProjectManager"] == DBNull.Value) ? "" : Convert.ToString(row["ProjectManager"]),
                            ProjectStart = (row["ProjectStart"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["ProjectStart"]),
                            ProjectEnd = (row["ProjectEnd"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["ProjectEnd"]),
                            ProjectStatus = (row["ProjectStatus"] == DBNull.Value) ? "" : Convert.ToString(row["ProjectStatus"]),
                        });
                    }
                    else if (fillDataTable == 2)
                    {
                        projectWiseDashboardData.projectWiseCostEstimationYears.Add(new ProjectWiseCostEstimationYear
                        {
                            CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                        });
                    }
                    else if (fillDataTable == 3)
                    {
                        projectWiseDashboardData.projectWiseInfoPieCharts.Add(new ProjectWiseInfoPieChart
                        {
                            AssignName = (row["AssignName"] == DBNull.Value) ? "" : Convert.ToString(row["AssignName"]),
                            ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),
                            status = (row["status"] == DBNull.Value) ? 00 : Convert.ToInt32(row["status"]),
                            CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                            ProjectStatus = (row["ProjectStatus"] == DBNull.Value) ? "" : Convert.ToString(row["ProjectStatus"]),
                        });
                    }
                    else if (fillDataTable == 4)
                    {
                        projectWiseDashboardData.projectWiseInfoLineCharts.Add(new ProjectWiseInfoLineChart
                        {
                            ComponentID = (row["ComponentID"] == DBNull.Value) ? 00 : Convert.ToInt32(row["ComponentID"]),
                            CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                            FunderContribution = (row["FunderContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["FunderContribution"]),
                            OtherContribution = (row["OtherContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["OtherContribution"]),
                            CommunityContribution = (row["CommunityContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["CommunityContribution"]),
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<AllChartDto> SumarryForProjectWise(int input)
        {
            AllChartDto allChartData = new AllChartDto();
            List<PieChartData> piechart = new List<PieChartData>();
            List<BarChartData> barchart = new List<BarChartData>();

            try
            {
                ProjectWiseDashboardData projectWiseDashboardData = new ProjectWiseDashboardData();
                projectWiseDashboardData.projectWiseCostEstimationYears = new List<ProjectWiseCostEstimationYear>();
                projectWiseDashboardData.projectWiseInfoDTOs = new List<ProjectWiseInfoDTO>();
                projectWiseDashboardData.projectWiseInfoLineCharts = new List<ProjectWiseInfoLineChart>();
                projectWiseDashboardData.projectWiseInfoPieCharts = new List<ProjectWiseInfoPieChart>();

                string value;
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    using (var cmd = new SqlCommand("", conn))
                    {
                        // ProjectWiseInfo
                        var dataTable = new DataTable();
                        cmd.CommandTimeout = 0;
                        cmd.CommandText = "[dbo].[ProjectWiseInfo]";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ProgramId", input));

                        SqlDataReader dr = cmd.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();
                            if (value != "")
                            {
                                projectWiseDashboardData.dataTable = null;
                                projectWiseDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(projectWiseDashboardData, 1);
                            }
                        }
                        conn.Close();
                    }

                }

                var lineGorup = projectWiseDashboardData.projectWiseInfoLineCharts.GroupBy(l => l.CostEstimationYear).ToList();






                allChartData.projectWiseDashboardDatas = new List<ProjectWiseDashboardData>();
                allChartData.projectWiseDashboardDatas.Add(projectWiseDashboardData);

                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async Task<AllChartDto> YearWisecomponent(int inputs, string year)
        {
            var programName = _programRepository.GetAll().Where(hh => hh.Id == inputs).FirstOrDefault().Name;

            try
            {
                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramYearwiseStateWiseBudgets = new List<ProgramManagerProgramYearwiseStateWiseBudget>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdTwo = new SqlCommand("", conn))
                    {
                        //ProjectWiseInfoLineChart
                        var dataTable = new DataTable();
                        cmdTwo.CommandTimeout = 0;
                        cmdTwo.CommandText = "[dbo].[YearWiseComponentfundercommunitydata]";
                        cmdTwo.CommandType = CommandType.StoredProcedure;
                        cmdTwo.Parameters.Add(new SqlParameter("@ProgramId", inputs));
                        cmdTwo.Parameters.Add(new SqlParameter("@CostEstimationYear", year));

                        SqlDataReader dr = cmdTwo.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerProgramYearwiseStateWiseBudgets.Add(new ProgramManagerProgramYearwiseStateWiseBudget
                                    {

                                        Component = (row["Component"] == DBNull.Value) ? "" : Convert.ToString(row["Component"]),
                                        OtherContribution = (row["OtherContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["OtherContribution"]),
                                        CommunityContribution = (row["CommunityContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["CommunityContribution"]),
                                        FunderContribution = (row["FunderContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["FunderContribution"]),

                                    });
                                }

                            }
                        }
                        conn.Close();
                    }

                    //
                    var programManagerBarChart = new List<BarChartData>();
                    var ProgramManagerProgramYearwiseStateWiseBudgetBarChart = programManagerProgramYearwiseStateWiseBudgets.ToList().GroupBy(l => l.Component).ToList();

                    ProgramManagerProgramYearwiseStateWiseBudgetBarChart.ForEach(l =>
                    {
                        BarChartData ProgramManagerProgramYearwiseBudgetBarChart = new BarChartData();
                        ProgramManagerProgramYearwiseBudgetBarChart.labels = new List<string>();
                        ProgramManagerProgramYearwiseBudgetBarChart.datasets = new List<BarChartDataset>();
                        ProgramManagerProgramYearwiseBudgetBarChart.datasets.ForEach(x =>
                        {
                            x.data = new List<decimal>();
                        });
                        programManagerBarChart.Add(ProgramManagerProgramYearwiseBudgetBarChart);
                    });


                    for (int i = 0; i < ProgramManagerProgramYearwiseStateWiseBudgetBarChart.Count; i++)
                    {
                        var items = ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList();
                        programManagerBarChart[i].labels.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].Key);

                        List<BarChartDataset> a = new List<BarChartDataset>();

                        for (int j = 0; j < 3; j++)
                        {

                            if (j == 0)
                            {
                                BarChartDataset obj = new BarChartDataset();
                                obj.data = new List<decimal>();
                                obj.backgroundColor = "red";
                                obj.borderColor = "blue";
                                obj.label = "FunderContribution";

                                obj.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.FunderContribution));
                                a.Add(obj);

                            }
                            else if (j == 1)
                            {
                                BarChartDataset obj1 = new BarChartDataset();
                                obj1.data = new List<decimal>();
                                obj1.backgroundColor = "green";
                                obj1.borderColor = "pink";
                                obj1.label = "CommunityContribution";

                                obj1.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.CommunityContribution));

                                a.Add(obj1);
                            }
                            else
                            {
                                BarChartDataset obj2 = new BarChartDataset();
                                obj2.data = new List<decimal>();
                                obj2.backgroundColor = "yellow";
                                obj2.borderColor = "pink";
                                obj2.label = "OtherContribution";

                                obj2.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.OtherContribution));

                                a.Add(obj2);
                            }
                        }

                        programManagerBarChart[i].datasets = a;
                    }

                    allChartData.ProgramManagerProgramYearwiseStateWiseBudgetBarChart = programManagerBarChart;
                    allChartData.statuswisecounts = (from ipc in _implementationPlanCheckListRepository.GetAll().Where(xx => xx.ProgramID == inputs && xx.CostEstimationYear == year)
                                                     group ipc by ipc.status into status
                                                     select new statuswisecount()
                                                     {
                                                         ProjectStatus = status.Key.ToString(),
                                                         statuscount = status.Count()

                                                     }).ToList();

                    allChartData.projectWiseYearlyclustorinfo = new List<ProjectWiseYearlyclustorinfo>();
                    var Yearwiseclustorvillages = await Yearwiseclustorvillage(inputs, year);
                    allChartData.projectWiseYearlyclustorinfo = Yearwiseclustorvillages.projectWiseYearlyclustorinfo.ToList();
                    allChartData.notificationdtos = await GetNotificationDataAsync(programName, year);
                }
                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<notificationdto>> GetNotificationDataAsync(string input, string Year)
        {

            var url = "https://advancechatdemo.firebaseio.com/" + input + "/chatmodel.json";
            WebRequest request = HttpWebRequest.Create(url);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string urlText = reader.ReadToEnd(); // it takes the response from your url. now you can use as your need  

            List<usernamesdto> usernamesdto = new List<usernamesdto>();
            List<notificationdto> notificationdtos = new List<notificationdto>();
            List<notificationdto> result = new List<notificationdto>();
            dynamic obj = JsonConvert.DeserializeObject<object>(urlText);
            if (obj != null)
            {
                foreach (var item in obj)
                {

                    usernamesdto.Add(new usernamesdto
                    {
                        usernamess = item.Name
                    });
                }

                foreach (var items in usernamesdto)
                {
                    var url1 = "https://advancechatdemo.firebaseio.com/" + input + "/chatmodel/" + items.usernamess + ".json";
                    WebRequest request1 = HttpWebRequest.Create(url1);
                    WebResponse response1 = request1.GetResponse();
                    StreamReader reader1 = new StreamReader(response1.GetResponseStream());
                    string urlText1 = reader1.ReadToEnd(); // it takes the response from your url. now you can use as your need  
                                                           //Response.Write(urlText.ToString());

                    dynamic obj1 = JsonConvert.DeserializeObject<Rootobject>(urlText1);

                    double timestamp = Convert.ToDouble(obj1.timeStamp) / 1000.0;
                    System.DateTime dateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                    dateTime = dateTime.AddSeconds(Convert.ToDouble(timestamp));


                    notificationdtos.Add(new notificationdto
                    {


                        message = obj1.message,
                        timeStamp = dateTime,
                        name = obj1.userModel.name,

                    });
                }


                var year = Year.Split('-');

                var fromDate = new DateTime(int.Parse(year[0].ToString()), 4, 1);
                var toDate = new DateTime(int.Parse(year[1].ToString()), 3, 31);

                notificationdtos.ForEach(object1 =>
                {
                    string substr1 = ":";
                    if (object1.message == null) object1.message = ""; // set empty string if it is null
                    if (object1.timeStamp >= fromDate && object1.timeStamp <= toDate && object1.message != "" && object1.message.Contains(substr1))
                    {
                        result.Add(object1);
                    }
                });
            }

            return result.ToList();
        }

        public async Task<ProjectWiseYearlyActivity> YearWiseActivity(int input, string year, string status)
        {


            var left_text = status.Substring(0, status.IndexOf(":"));
            int text;
            if (left_text == "NotStarted")
            {
                text = 1;
            }
            else if (left_text == "InProgress")
            {
                text = 2;
            }
            else if (left_text == "Completed")
            {
                text = 3;
            }
            else
            {
                text = 4;
            }

            try
            {
                ProjectWiseYearlyActivity projectWiseYearlyActivity = new ProjectWiseYearlyActivity();
                projectWiseYearlyActivity.projectWiseInfoPieCharts = new List<ProjectWiseYearlyActivityInfo>();
                string value;
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);
                    using (var cmdOne = new SqlCommand("", conn))
                    {

                        //ProjectWiseInfoPieChart
                        var dataTable = new DataTable();
                        cmdOne.CommandTimeout = 0;
                        cmdOne.CommandText = "[dbo].[ProjectyearWiseActivity]";
                        cmdOne.CommandType = CommandType.StoredProcedure;
                        cmdOne.Parameters.Add(new SqlParameter("@ProgramId", input));
                        cmdOne.Parameters.Add(new SqlParameter("@CostEstimationYear", year));
                        cmdOne.Parameters.Add(new SqlParameter("@Status", text));


                        SqlDataReader dr = cmdOne.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                projectWiseYearlyActivity.dataTable = null;
                                projectWiseYearlyActivity.dataTable = dataTable;
                                await ProjectWiseYearlyActivityAddDataInDataTable(projectWiseYearlyActivity, 1);
                            }
                        }
                        conn.Close();
                    }
                }
                return projectWiseYearlyActivity;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task ProjectWiseYearlyActivityAddDataInDataTable(ProjectWiseYearlyActivity projectWiseYearlyActivity, int fillDataTable = 0)
        {
            try
            {
                foreach (DataRow row in projectWiseYearlyActivity.dataTable.Rows)
                {
                    if (fillDataTable == 1)
                    {
                        projectWiseYearlyActivity.projectWiseInfoPieCharts.Add(new ProjectWiseYearlyActivityInfo
                        {
                            AssignName = (row["AssignName"] == DBNull.Value) ? "" : Convert.ToString(row["AssignName"]),
                            ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<PrgCostEstimationYearDto> CostEstimationYear(int programId)
        {
            var result = (from programcost in _PrgCostEstRepository.GetAll().Where(xx => xx.ProgramID == programId)
                          group programcost by programcost.CostEstimationYear into userGrouped
                          select new PrgCostEstimationYearDto()
                          {
                              CostEstimationYear = userGrouped.Key
                          }).ToList();
            return result;
        }

        public async Task<List<statuswisecount>> statuswisecount(int programId, string year)
        {
            var result = (from ipc in _implementationPlanCheckListRepository.GetAll().Where(xx => xx.ProgramID == programId && xx.CostEstimationYear == year)
                          group ipc by ipc.status into status
                          select new statuswisecount()
                          {
                              ProjectStatus = status.Key.ToString(),
                              statuscount = status.Count()

                          }).ToList();
            return result;
        }

        public async Task<ProjectWiseYearlyClustor> Yearwiseclustorvillage(int input, string year)
        {

            try
            {
                ProjectWiseYearlyClustor projectWiseYearlyClustor = new ProjectWiseYearlyClustor();
                projectWiseYearlyClustor.projectWiseYearlyclustorinfo = new List<ProjectWiseYearlyclustorinfo>();
                string value;
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();

                    await ConnectionConnect(conn);
                    using (var cmdOne = new SqlCommand("", conn))
                    {

                        //ProjectWiseInfoPieChart
                        var dataTable = new DataTable();
                        cmdOne.CommandTimeout = 0;
                        cmdOne.CommandText = "[dbo].[ProjectyearWiseclustorvillageinfo]";
                        cmdOne.CommandType = CommandType.StoredProcedure;
                        cmdOne.Parameters.Add(new SqlParameter("@ProgramId", input));
                        cmdOne.Parameters.Add(new SqlParameter("@CostEstimationYear", year));


                        SqlDataReader dr = cmdOne.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                projectWiseYearlyClustor.dataTable = null;
                                projectWiseYearlyClustor.dataTable = dataTable;
                                await ProjectWiseYearlyClustorAddDataInDataTable(projectWiseYearlyClustor, 1);
                            }
                        }
                        conn.Close();
                    }
                }
                return projectWiseYearlyClustor;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task ProjectWiseYearlyClustorAddDataInDataTable(ProjectWiseYearlyClustor projectWiseYearlyClustor, int fillDataTable = 0)
        {
            try
            {
                foreach (DataRow row in projectWiseYearlyClustor.dataTable.Rows)
                {
                    if (fillDataTable == 1)
                    {
                        projectWiseYearlyClustor.projectWiseYearlyclustorinfo.Add(new ProjectWiseYearlyclustorinfo
                        {
                            clustorName = (row["clustorName"] == DBNull.Value) ? "" : Convert.ToString(row["clustorName"]),
                            //VillageName = (row["VillageName"] == DBNull.Value) ? "" : Convert.ToString(row["VillageName"]),
                            AssignName = (row["AssignName"] == DBNull.Value) ? "" : Convert.ToString(row["AssignName"]),
                            SubactivityName = (row["SubactivityName"] == DBNull.Value) ? "" : Convert.ToString(row["SubactivityName"]),
                            TergetUnit = (row["TergetUnit"] == DBNull.Value) ? "" : Convert.ToString(row["TergetUnit"]),
                            AchievedUnit = (row["AchievedUnit"] == DBNull.Value) ? "" : Convert.ToString(row["AchievedUnit"]),
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<ProgramInformationListDto>> GetAllProgram()
        {
            var userId = AbpSession.UserId;
            var usr = _userRoleRepository.GetAll().FirstOrDefault(u => u.UserId == userId);
            var role = _rolesRepository.GetAll().FirstOrDefault(u => u.Id == usr.RoleId);
            var prg = await _programRepository.GetAll().ToListAsync();

            if (role.Name == "ProgramManager")
            {
                prg = (from p in prg
                       join r in _RequestAAplasRepository.GetAll()
                        on p.Id equals r.ProgramID
                       where (p.ProgramManagerID == userId && r.RequestStatus == RequestStatus.RequestSend)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "Admin")
            {
                prg = (from p in prg
                       join r in _RequestAAplasRepository.GetAll()
                        on p.Id equals r.ProgramID
                       where (r.RequestStatus == RequestStatus.RequestSend)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "ProjectManager" || role.Name == "ActionAreaManager")
            {
                prg = (from p in prg
                       join r in _RequestAAplasRepository.GetAll()
                        on p.Id equals r.ProgramID
                       where (r.ManagerID == userId && r.RequestStatus == RequestStatus.RequestSend)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "RRcIncharge" || role.Name == "FieldStaffMember")
            {
                prg = (from p in prg
                       join vc in _villageClusterRepository.GetAll()
                        on p.Id equals vc.ProgramID
                       join r in _RequestAAplasRepository.GetAll()
                       on p.Id equals r.ProgramID
                       where (vc.UserId == userId && r.RequestStatus == RequestStatus.RequestSend)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "ProgramManagerFinanceOrAdmin")
            {
                prg = (from p in prg
                       join r in _programFundRepository.GetAll()
                        on p.Id equals r.ProgramID
                       join rr in _RequestAAplasRepository.GetAll()
                      on p.Id equals rr.ProgramID
                       where (r.ProgramManagerID == userId && rr.RequestStatus == RequestStatus.RequestSend)
                       select p).Distinct().ToList();
            }

            var query = (from p in prg
                         select new ProgramInformationListDto()
                         {
                             ProgramName = p.Name,
                             ShortDescription = p.ShortDescription,
                             LongDescription = p.LongDescription,
                             DonorID = p.DonorID,
                             ProgramSanctionedYear = p.ProgramSanctionedYear,
                             ProgramStartDate = p.ProgramStartDate,
                             PrgramEndDate = p.PrgramEndDate,
                             Id = p.Id
                         }).ToList();
            return query;
        }
        public async Task<List<ProgramInformationListDto>> GetAllProgramForApproveAppla()
        {
            var userId = AbpSession.UserId;
            var usr = _userRoleRepository.GetAll().FirstOrDefault(u => u.UserId == userId);
            var role = _rolesRepository.GetAll().FirstOrDefault(u => u.Id == usr.RoleId);
            var prg = await _programRepository.GetAll().ToListAsync();

            if (role.Name == "ProgramManager")
            {
                prg = (from p in prg
                       join r in _RequestAAplasRepository.GetAll()
                        on p.Id equals r.ProgramID
                       where (p.ProgramManagerID == userId && r.RequestStatus == RequestStatus.Approved)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "Admin")
            {
                prg = (from p in prg
                       join r in _RequestAAplasRepository.GetAll()
                        on p.Id equals r.ProgramID
                       where (r.RequestStatus == RequestStatus.Approved)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "ProjectManager" || role.Name == "ActionAreaManager")
            {
                prg = (from p in prg
                       join r in _RequestAAplasRepository.GetAll()
                        on p.Id equals r.ProgramID
                       where (r.ManagerID == userId && r.RequestStatus == RequestStatus.Approved)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "RRcIncharge" || role.Name == "FieldStaffMember")
            {
                prg = (from p in prg
                       join vc in _villageClusterRepository.GetAll()
                        on p.Id equals vc.ProgramID
                       join r in _RequestAAplasRepository.GetAll()
                       on p.Id equals r.ProgramID
                       where (vc.UserId == userId && r.RequestStatus == RequestStatus.Approved)
                       select p).Distinct().ToList();
            }
            else if (role.Name == "ProgramManagerFinanceOrAdmin")
            {
                prg = (from p in prg
                       join r in _programFundRepository.GetAll()
                        on p.Id equals r.ProgramID
                       join rr in _RequestAAplasRepository.GetAll()
                      on p.Id equals rr.ProgramID
                       where (r.ProgramManagerID == userId && rr.RequestStatus == RequestStatus.Approved)
                       select p).Distinct().ToList();
            }

            var query = (from p in prg
                         select new ProgramInformationListDto()
                         {
                             ProgramName = p.Name,
                             ShortDescription = p.ShortDescription,
                             LongDescription = p.LongDescription,
                             DonorID = p.DonorID,
                             ProgramSanctionedYear = p.ProgramSanctionedYear,
                             ProgramStartDate = p.ProgramStartDate,
                             PrgramEndDate = p.PrgramEndDate,
                             Id = p.Id
                         }).ToList();
            return query;
        }

        public List<budgetachievmentdto> RRC()
        {
            var Result = (from ra in _RequestAAplasRepository.GetAll().Where(aa => aa.ManagerID == 26)

                          join pe in _ProgramExpenseRepository.GetAll().Where(aa => aa.ManagerID == 26) on ra.ProgramID equals pe.ProgramID
                          group ra by new
                          {
                              raa = ra.ProgramID,
                              pee = pe.ProgramID,
                              manager = pe.ManagerID
                          } into g

                          select new budgetachievmentdto
                          {
                              //FunderContribution= _RequestAAplasRepository.GetAll().Where(xx => xx.ProgramID == g.Key.pee && xx.ManagerID == g.Key.manager).Sum(aa => aa.FunderContribution),
                              ExpenseAmount = _ProgramExpenseRepository.GetAll().Where(xx => xx.ProgramID == g.Key.pee && xx.ManagerID == g.Key.manager).Sum(aa => aa.Amount),

                          }).ToList();
            return Result;
        }

    }
}