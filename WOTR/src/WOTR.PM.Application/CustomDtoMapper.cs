using WOTR.PM.ChekLists;
using WOTR.PM.ChekLists.Dtos;
using WOTR.PM.UnitOfMeasures;
using WOTR.PM.UnitOfMeasures.Dtos;
using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Localization;
using Abp.Notifications;
using Abp.Organizations;
using Abp.UI.Inputs;
using AutoMapper;
using WOTR.PM.Auditing.Dto;
using WOTR.PM.Authorization.Accounts.Dto;
using WOTR.PM.Authorization.Permissions.Dto;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Authorization.Roles.Dto;
using WOTR.PM.Authorization.Users;
using WOTR.PM.Authorization.Users.Dto;
using WOTR.PM.Authorization.Users.Profile.Dto;
using WOTR.PM.Chat;
using WOTR.PM.Chat.Dto;
using WOTR.PM.Editions;
using WOTR.PM.Editions.Dto;
using WOTR.PM.Friendships;
using WOTR.PM.Friendships.Cache;
using WOTR.PM.Friendships.Dto;
using WOTR.PM.Localization.Dto;
using WOTR.PM.Locations;
using WOTR.PM.MultiTenancy;
using WOTR.PM.MultiTenancy.Dto;
using WOTR.PM.MultiTenancy.HostDashboard.Dto;
using WOTR.PM.MultiTenancy.Payments;
using WOTR.PM.MultiTenancy.Payments.Dto;
using WOTR.PM.Notifications.Dto;
using WOTR.PM.Organizations.Dto;
using WOTR.PM.Sessions.Dto;
using WOTR.PM.Programs;
using WOTR.PM.PrgActionAreaActivitysMappings.Dto;
using WOTR.PM.Programs.Dto;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.PrgVillageCluster.Dto;
using WOTR.PM.States;
using WOTR.PM.NewImpactIndicator;
using WOTR.PM.NewImpactIndicator.Crops.Dto;

namespace WOTR.PM
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<Program, ProgDto>();
            configuration.CreateMap<Program, ProgDtoList>();
            configuration.CreateMap<CreateOrEditChekListDto, Checklist>();
           configuration.CreateMap<Checklist, ChekListDto>();
           configuration.CreateMap<CreateOrEditUnitofMeasureDto, UnitofMeasure>();
           configuration.CreateMap<UnitofMeasure, UnitofMeasureDto>();
            //Inputs
            configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
            configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<IInputType, FeatureInputTypeDto>()
                .Include<CheckboxInputType, FeatureInputTypeDto>()
                .Include<SingleLineStringInputType, FeatureInputTypeDto>()
                .Include<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
                .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
            configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
                .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

           

            //Chat
            configuration.CreateMap<ChatMessage, ChatMessageDto>();

            //Feature
            configuration.CreateMap<FlatFeatureSelectDto, Feature>().ReverseMap();
            configuration.CreateMap<Feature, FlatFeatureDto>();

            //Role
            configuration.CreateMap<RoleEditDto, Role>().ReverseMap();
            configuration.CreateMap<Role, RoleListDto>();
            configuration.CreateMap<UserRole, UserListRoleDto>();
            configuration.CreateMap<Role, UserListRoleDto>().ReverseMap();

            //Edition
            configuration.CreateMap<EditionEditDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<EditionSelectDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionInfoDto>().Include<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionListDto>();
            configuration.CreateMap<Edition, EditionEditDto>();
            configuration.CreateMap<Edition, SubscribableEdition>();
            configuration.CreateMap<Edition, EditionSelectDto>();


            //Payment
            configuration.CreateMap<SubscriptionPaymentDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPaymentListDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPayment, SubscriptionPaymentInfoDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();

            //Language
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>()
                .ForMember(ldto => ldto.IsEnabled, options => options.MapFrom(l => !l.IsDisabled));

            //Tenant
            configuration.CreateMap<Tenant, RecentTenant>();
            configuration.CreateMap<Tenant, TenantLoginInfoDto>();
            configuration.CreateMap<Tenant, TenantListDto>();
            configuration.CreateMap<TenantEditDto, Tenant>().ReverseMap();
            configuration.CreateMap<CurrentTenantInfoDto, Tenant>().ReverseMap();
            configuration.CreateMap<ActivityActionandSubactionArea, ActionSubActionList>();
            configuration.CreateMap<VillageCluster, PrgVillageClusterListDto>();
            configuration.CreateMap<Village, PrgVillageClusterVillages>();
            configuration.CreateMap<Taluka, PrgVillageClusterTaluka>();
            configuration.CreateMap<User, PrgManagersListDtos>();
            configuration.CreateMap<User, PrgManagersListDtosrrc>();

            //User
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
            configuration.CreateMap<User, UserLoginInfoDto>();
            configuration.CreateMap<User, UserListDto>();
            configuration.CreateMap<User, ChatUserDto>();
            configuration.CreateMap<User, OrganizationUnitUserListDto>();
            configuration.CreateMap<CurrentUserProfileEditDto, User>().ReverseMap();
            configuration.CreateMap<UserLoginAttemptDto, UserLoginAttempt>().ReverseMap();

            //AuditLog
            configuration.CreateMap<AuditLog, AuditLogListDto>();

            //Friendship
            configuration.CreateMap<Friendship, FriendDto>();
            configuration.CreateMap<FriendCacheItem, FriendDto>();

            //OrganizationUnit
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();


            //
            configuration.CreateMap<Crop, GetAllCrops>().ReverseMap();
            /* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */
        }
    }
}