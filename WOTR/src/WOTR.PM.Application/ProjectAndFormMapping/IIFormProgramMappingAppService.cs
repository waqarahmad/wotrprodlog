﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Locations;
using WOTR.PM.NewImpactIndicator;
using WOTR.PM.NewImpactIndicator.IIFormProgramMappings.Dto;
using WOTR.PM.NewImpactIndicator.Questionaries.Dto;
using WOTR.PM.PrgRequestAAPlas;
using WOTR.PM.ProgramFunds;
using WOTR.PM.Programs;

namespace WOTR.PM.ProjectAndFormMapping
{
    public class IIFormProgramMappingAppService : PMAppServiceBase
    {
        private readonly IRepository<ProjectAndIIFormMapping> _projectAndIIFormMapping;
        private readonly IRepository<ImpactIndicatorForm> _impactIndicatorForm;
        private readonly IRepository<Program> _program;
        private readonly IRepository<ProgramFund> _programFundRepository;
        private readonly IRepository<VillageCluster> _villageClusterRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<Role> _rolesRepository;
        private readonly IRepository<RequestAAplas> _requestAAplasRepository;


        public IIFormProgramMappingAppService(IRepository<ProjectAndIIFormMapping> projectAndIIFormMapping, 
            IRepository<ImpactIndicatorForm> impactIndicatorForm,
            IRepository<Program> program, IRepository<ProgramFund> programFundRepository, IRepository<VillageCluster> villageClusterRepository,
            IRepository<UserRole, long> userRoleRepository, IRepository<Role> rolesRepository,
            IRepository<RequestAAplas> requestAAplasRepository)
        {
            _projectAndIIFormMapping = projectAndIIFormMapping;
            _impactIndicatorForm = impactIndicatorForm;
            _program = program;
            _programFundRepository = programFundRepository;
            _villageClusterRepository = villageClusterRepository;
            _userRoleRepository = userRoleRepository;
            _rolesRepository = rolesRepository;
            _requestAAplasRepository = requestAAplasRepository;
        }

        public async Task<List<GetAllProgramAndFormMapping>> GetAllProgramFormMapping(string input)
        {
            try
            {

                var userId = AbpSession.UserId;
                var prg = _program.GetAll();

                var usr = await _userRoleRepository.FirstOrDefaultAsync(u => u.UserId == userId);
                var role = _rolesRepository.FirstOrDefault(u => u.Id == usr.RoleId);

                if (role.Name == "ProgramManager")
                {
                    prg = prg.Where(p => p.ProgramManagerID == userId || p.ManagerID == userId);
                }

                else if (role.Name == "ProjectManager" || role.Name == "ActionAreaManager")
                {
                    prg = (from p in prg
                          join r in _requestAAplasRepository.GetAll()
                            on p.Id equals r.ProgramID
                           where (p.ManagerID == userId || r.ManagerID ==userId)
                           select p).Distinct();
                }
                else if (role.Name == "RRcIncharge" || role.Name == "FieldStaffMember")
                {
                    prg = (from p in prg
                           join vc in _villageClusterRepository.GetAll()
                            on p.Id equals vc.ProgramID
                           where (vc.UserId == userId)
                           select p).Distinct();
                }
                else if (role.Name == "ProgramManagerFinanceOrAdmin")
                {
                    prg = (from p in prg
                         //  join r in _programFundRepository.GetAll()
                        //    on p.Id equals r.ProgramID
                           where (p.ProgramManagerID == userId)
                           select p).Distinct();
                }


                var AllProgramFormMap = (from pf in _projectAndIIFormMapping.GetAll()
                                   join f in _impactIndicatorForm.GetAll() on pf.impactIndicatorFormId equals f.Id
                                   join p in prg on pf.programId equals p.Id
                                   select new GetAllProgramAndFormMapping
                                   {
                                       Id = pf.Id,
                                       programId = pf.programId,
                                       impactIndicatorFormId = pf.impactIndicatorFormId,
                                       programName = p.Name,
                                       FormName = f.FormName,
                                       Status = pf.Status,
                                   }).ToList();

                return AllProgramFormMap;
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public string CreateOrEdit(ProjectAndIIFormMapping input)
        {
            string returnMsg;
            if (input.Id == null || input.Id == 0)
            {
                returnMsg =  Create(input);
                return returnMsg;
            }
            else
            {
                returnMsg = Update(input);
                return returnMsg;
            }
        }

        private string Create(ProjectAndIIFormMapping input)
        {
            try
            {


                var MappingExist = _projectAndIIFormMapping.GetAll().Where(x => x.programId == input.programId);
                if (MappingExist.Count() == 0)
                {
                    _projectAndIIFormMapping.InsertAsync(input);
                    return "Mapping Updated Sucessfully";
                }
                else
                {
                    return "Mapping allready exist";

                }



            }
            catch (Exception ex) {
                throw;
            }
        }

        private string Update(ProjectAndIIFormMapping input)
        {
            try
            {
                var MappingExist = _projectAndIIFormMapping.GetAll().Where(x => x.impactIndicatorFormId == input.impactIndicatorFormId && x.programId == input.programId);
                if (MappingExist.Count() == 0)
                {
                    _projectAndIIFormMapping.Update(input);
                    return "Mapping Updated Sucessfully";
                }
                else {
                    return "Mapping allready exist";
                     }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public async Task Delete(EntityDto input)
        {
            try
            {
                await _projectAndIIFormMapping.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }
    }
}
