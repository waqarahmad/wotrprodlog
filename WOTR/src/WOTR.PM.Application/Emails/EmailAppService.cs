﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using MimeKit;
using Org.BouncyCastle.Crypto.Tls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Configuration;
using WOTR.PM.Emails;
using WOTR.PM.Emails.Dto;
//using SmtpClient = MailKit.Net.Smtp.SmtpClient;

namespace WOTR.PM.Emails
{
    public class EmailAppService : PMAppServiceBase, IEmailAppService
    {
        private readonly IWotrEmailSender _IWotrEmailSender;
        private readonly IConfigurationRoot _appConfiguration;

        public EmailAppService(IWotrEmailSender IWotrEmailSender, IHostingEnvironment env)
        {
            _IWotrEmailSender = IWotrEmailSender;
            _appConfiguration = env.GetAppConfiguration();
        }


        //public void mailSending(string emailBody, string emailSubject, string toAddress, string fromAddr, string replyAddr)
        //{
        //    fromAddr = "anything@intime.waiin.com";
        //    replyAddr = "reply@intime.waiin.com";
        //    var a = _IWotrEmailSender.SendEmailAsync(emailBody, emailSubject, toAddress, fromAddr, replyAddr);
        //}

        public void mailSending(string emailBody, string emailSubject, string toAddress, string fromAddr, string replyAddr)
        {
            var userName = _appConfiguration["EmailCredintials:UserName"];
            string passWord = _appConfiguration["EmailCredintials:Password"];
            var fromAddress1 = new MailAddress(userName, "WOTR");
            var toAddress11 = new MailAddress(toAddress, "To Name");
            // const string fromPassword = "Micro#20$%^soft2019";
             string fromPassword = passWord;
            string subject = emailSubject;
            string body = emailBody;


            MailMessage message1 = new MailMessage();
            message1.To.Add(toAddress11); // Add Receiver mail Address  
            message1.From = new MailAddress(fromAddress1.Address); // Sender address  
            message1.Subject = emailSubject;
            message1.IsBodyHtml = true;
            message1.Body = body;

            var smtp = new System.Net.Mail.SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress1.Address, fromPassword)
            };


            //using (var message = new MailMessage(fromAddress1, toAddress11)
            //{
            //    Subject = subject,
            //    Body = body
            //})


            {
                ServicePointManager.ServerCertificateValidationCallback =
            delegate (
                object s,
                X509Certificate certificate,
                X509Chain chain,
                SslPolicyErrors sslPolicyErrors
            )
            {
                return true;
            };
                smtp.Send(message1);
            }


        }


        public void mailSending1(string emailBody, string emailSubject, string toAddress, string toCC, string fromAddr, string replyAddr)
        {
            var userName = _appConfiguration["EmailCredintials:UserName"];
            string passWord = _appConfiguration["EmailCredintials:Password"];
            var fromAddress1 = new MailAddress(userName, "WOTR");
            var toAddress11 = new MailAddress(toAddress, "To Name");
            var tocc = new MailAddress("prakash.keskar@wotr.org.in", "WOTR");
           // const string fromPassword = "Micro#20$%^soft2019";
            string fromPassword = passWord;
            string subject = emailSubject;
            string body = emailBody;


            MailMessage message1 = new MailMessage();
            message1.To.Add(toAddress11); // Add Receiver mail Address  

            message1.CC.Add(tocc); // Add CC mail address
            
            message1.From = new MailAddress(fromAddress1.Address); // Sender address  
            message1.Subject = emailSubject;
            message1.IsBodyHtml = true;
            message1.Body = body;

            var smtp = new System.Net.Mail.SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress1.Address, fromPassword)
            };


            //using (var message = new MailMessage(fromAddress1, toAddress11)
            //{
            //    Subject = subject,
            //    Body = body
            //})
            try
            {
                {


                    ServicePointManager.ServerCertificateValidationCallback =
                delegate (
                    object s,
                    X509Certificate certificate,
                    X509Chain chain,
                    SslPolicyErrors sslPolicyErrors
                )


                {
                    return true;
                };
                    smtp.Send(message1);
                }

            }
            catch (Exception ex)
            {

                throw;
            }

            

        }
    }
}
