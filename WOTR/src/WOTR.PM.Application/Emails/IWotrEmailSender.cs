﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Emails.Dto;

namespace WOTR.PM.Emails
{
    public interface IWotrEmailSender
    {
        Task<TransmissionStatus> SendEmailAsync(string emailBody, string emailSubject, string toAddress, string fromAddr, string replyAddr);
    }
}
