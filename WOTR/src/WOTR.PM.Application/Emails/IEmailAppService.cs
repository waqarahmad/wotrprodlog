﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace WOTR.PM.Emails
{
    public interface IEmailAppService: IApplicationService
    {
        void mailSending(string emailBody, string emailSubject, string toAddress, string fromAddr, string replyAddr);
        // Task sendEmailAsync(string emailBody, string emailSubject, string toAddress, string fromAddr, string replyAddr);
        void mailSending1(string emailBody, string emailSubject, string toAddress, string toCC,string fromAddr, string replyAddr);
    }
}
