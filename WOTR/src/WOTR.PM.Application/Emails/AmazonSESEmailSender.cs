﻿using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Emails;
using WOTR.PM.Emails.Dto;

namespace WOTR.PM.Emails
{
   public  class AmazonSESEmailSender: IWotrEmailSender
    {
        public async Task<TransmissionStatus> SendEmailAsync(string emailBody, string emailSubject, string toAddress, string fromAddr, string replyAddr)
        {

            var oMessage = new Message
            {
                Body = new Body { Html = new Content { Data = emailBody } },
                Subject = new Content { Data = emailSubject }
            };

            // Create the Amazon SES request.
            var request = new SendEmailRequest
            {
                Destination = new Destination { ToAddresses = new List<string> { toAddress } },
                Message = oMessage,
                ReplyToAddresses = new List<string> { "contact@intime.waiin.com" },
               // ReplyToAddresses = new List<string> { "apps@wotr.org.in" },

                Source = "contact@intime.waiin.com"
              //Source= "apps@wotr.org.in"
                //ReturnPath
            };
            try
            {
                using (var client = new AmazonSimpleEmailServiceClient())
                {
                    try
                    {
                        var response = await client.SendEmailAsync(request);

                        return new TransmissionStatus()
                        {
                            EmailAddress = toAddress,
                            Response = new EmailResponse
                            {
                                Metadata = response.ResponseMetadata.Metadata,
                                RequestId = response.ResponseMetadata.RequestId,
                                MessageId = response.MessageId,
                                Code = response.HttpStatusCode
                            },
                            Code = EmailStatusCode.Ok
                        };
                    }
                    catch (Exception ex)
                    {
                        return new TransmissionStatus()
                        {
                            EmailAddress = toAddress,
                            Exception = ex.Message,
                            Code = EmailStatusCode.Error
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
