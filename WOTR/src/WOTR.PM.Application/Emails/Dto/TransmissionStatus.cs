﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WOTR.PM.Emails.Dto
{
    public enum EmailStatusCode
    {
        Ok = 1,
        Error = 2
    }

    public class TransmissionStatus
    {
        public EmailStatusCode Code { get; set; }
        public EmailResponse Response { get; set; }
        public string Exception { get; set; }
        public string EmailAddress { get; set; }
    }

    public class EmailResponse
    {
        public string MessageId { get; set; }
        public string RequestId { get; set; }
        public IDictionary<string, string> Metadata { get; set; }
        public HttpStatusCode Code { get; set; }
    }
}
