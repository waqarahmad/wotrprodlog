﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Locations.Dto;
using WOTR.PM.States;
using Abp.Application.Services.Dto;
using WOTR.PM.Authorization.Users;
using Abp.UI;
using Abp.Collections.Extensions;
using Abp.Extensions;

namespace WOTR.PM.Locations
{
    public class LocationAppService : PMAppServiceBase, ILocationAppService
    {
        private readonly IRepository<Location> _locationRepository;
        private readonly IRepository<State> _stateRepository;

        private readonly IRepository<District> _districtRepository;//district addition
        private readonly IRepository<Taluka> _talukaRepository;//Taluka addition
        private readonly IRepository<Village> _villageRepository;

        private readonly IRepository<User, long> _userRepository;
        private List<string> strMessage = new List<string>();

        public LocationAppService(
            IRepository<Location> locationRepository,
            IRepository<State> stateRepository,
            IRepository<District> districtRepository,
            IRepository<User, long> userRepository,
            IRepository<Taluka> talukaRepository,
            IRepository<Village> villageRepository)

        {
            _locationRepository = locationRepository;
            _stateRepository = stateRepository;
            _userRepository = userRepository;
            _districtRepository = districtRepository;
            _talukaRepository = talukaRepository;
            _villageRepository = villageRepository;
        }

        //...Add new Location data
        public List<string> CreateLocation(LocationListDto Input)
        {
            List<string> message = new List<string>();

            var QueryCheck = (from a in _locationRepository.GetAll()
                              where (a.LocationType == LocationType.Village &&
                              a.Name == Input.Name && a.StateID == Input.StateID &&
                            a.TalukaId == Input.TalukaId && a.DistrictId == Input.DistrictId)
                              select a.Id).ToList().Count();


            var QueryCheck2 = _locationRepository.GetAll();
            if (Input.LocationType == LocationType.RRC)
            {
                QueryCheck2 = QueryCheck2.Where(b => b.Name == Input.Name && b.StateID == Input.StateID && b.ParentLocation==Input.ParentLocation);
            }
            else if (Input.LocationType == LocationType.VillageCluster)
            {
                QueryCheck2 = QueryCheck2.Where(b => b.Name == Input.Name && b.StateID == Input.StateID && b.ParentLocation == Input.ParentLocation);
            }
            else if  (Input.LocationType == LocationType.Village)
            {
                QueryCheck2 = QueryCheck2.Where(b => b.Name == Input.Name && b.StateID == Input.StateID && b.ParentLocation == Input.ParentLocation && b.DistrictId==Input.DistrictId
                && b.TalukaId == Input.TalukaId );
            }
            var result = QueryCheck2.ToList().Count();





            if (result == 0)
            {


                try
                {
                    Location location = new Location();
                    location.Name = Input.Name;
                    if (Input.LocationType==LocationType.Village)
                    {
                        location.Name = "_";
                    }
                    // YO!!-Converting first LowerCase Letter To upperCase
                    location.Name = char.ToUpper(location.Name[0]) + location.Name.Substring(1);
                    location.ParentLocation = Input.ParentLocation;

                    location.LocationType = Input.LocationType;

                    //modified yo
                    location.StateID = Input.StateID;
                    location.DistrictId = Input.DistrictId;
                    location.TalukaId = Input.TalukaId;
                    location.VillageId = Input.VillageId;

                    //location.State = Input.State;
                    location.TenantId = Input.TenantId;
                    location.Id = Input.Id;
                    if (Input.Id == 0) //....Create New Location
                    {
                        _locationRepository.Insert(location);
                        strMessage.Add("Location Added Successfully");
                    }
                    else   //...Update Exsisting Locations
                    {
                        _locationRepository.Update(location);
                        strMessage.Add("Location Updated Successfully");
                    }
                }//try
                catch (Exception Ex)
                {
                    strMessage.Add("Error= " + Ex.Message);
                }//Catch
            }
            else strMessage.Add("Record is already Present!");
            return strMessage;
        }

        //....Delete specific location
        public void DeleteLocation(EntityDto<int> Input)
        {


            _locationRepository.Delete(Input.Id);
            // throw new NotImplementedException();
        }

        public List<LocationListDto> GetAllRRcList()
        {
            var query = (from u in _locationRepository.GetAll().ToList().Where(u=>u.LocationType == LocationType.RRC || u.LocationType ==LocationType.HQ)select new LocationListDto() {

            Name = u.Name,
                Id=u.Id,
                StateID=u.StateID,
                DistrictId=u.DistrictId,
                TalukaId=u.TalukaId,

            }).ToList();

            return query;
          
        }

        //....Get All Locations
        public List<LocationListDto> GetAllLocations()
        {
            var query = (from l in _locationRepository.GetAll()
                         select new LocationListDto()
                         {
                             Id = l.Id,
                             Name = l.Name,
                             ParentLocation = l.ParentLocation,
                             //State = l.State,
                             
                             CreationTime = l.CreationTime,
                             TenantId = l.TenantId,
                             LocationType = l.LocationType,
                             StateID = l.StateID,
                             DistrictId = l.DistrictId,
                             TalukaId = l.TalukaId

                         }).ToList();
            return query;
        }

        public List<LocationListDto> GetAllParentLocations(LocationType input,int StateId)//
        {
            if (input == LocationType.RRC)
            {
                input = LocationType.HQ;
            }
            else if (input == LocationType.VillageCluster)
            {
                input = LocationType.RRC;
            }
            else if (input == LocationType.Village)
            {
                input = LocationType.VillageCluster;
            }
            var query = (from l in _locationRepository
                         .GetAll()
                         .Where(p => p.LocationType == input && p.StateID == StateId)
                         select new LocationListDto()
                         {
                             Id = l.Id,
                             Name = l.Name,
                             ParentLocation = l.ParentLocation,
                             //State = l.State,
                           
                             CreationTime = l.CreationTime,
                             TenantId = l.TenantId,
                             StateID = l.StateID,
                             DistrictId = l.DistrictId,
                             TalukaId = l.TalukaId

                         }).ToList();
            return query;
        }




        //public List<string> GetAllParentLocationsWithRespectToState(int StateId)
        //{
        //    List<string> ParentName = new List<string>();
           


        //    var Query = _locationRepository.GetAll().Where(l => l.StateID == StateId).Select(l => l.ParentLocation).ToList();
        //    foreach (var item in Query)
        //    {
        //       var  qu = (from l in _locationRepository.GetAll()where(l.ParentLocation==item)select(l.Name));

        //        foreach (var item1 in qu)
        //        {
        //            ParentName.Add(item1);
        //        }
        //    }
            
        //    return ParentName;
        //}




        public List<State> GetAllState()
        {
            var query = _stateRepository.GetAll().ToList();
            return query;

        }
        //yo
        public List<District> GetAllDistrict(int StateId)
        {
            var query = _districtRepository
                .GetAll()
                .Where(d => d.StateId == StateId)
                .ToList();
                
            return query;
        }
        public List<string> CreatOrUpdateDistrict(District Input)
        {

            List<string> message = new List<string>();

            District district = new District();
            district.Id = Input.Id;
            district.CreatorUserId = Input.CreatorUserId;
            district.StateId = Input.StateId;
            district.DistrictName = char.ToUpper(Input.DistrictName[0]) + Input.DistrictName.Substring(1);

            if (Input.Id == 0)
            {
                try
                {
                    var QueryCheck = (from a in _districtRepository.GetAll() where (a.DistrictName == Input.DistrictName) select a.Id).ToList();
                    if (QueryCheck.Count == 0)
                    {
                        _districtRepository.Insert(district);
                        message.Add("District Added  sucessfully !");
                    }

                    else { message.Add("Record alread Present!"); }
                }
                catch (Exception ex)
                {

                    throw new UserFriendlyException(L("Record NOt Add"));
                }

            }
            else
            {
                try
                {
                    district.TenantId = Input.TenantId;
                    district.CreatorUserId = Input.CreatorUserId;
                    _districtRepository.Update(district);
                    message.Add("District Update  sucessfully !");

                }
                catch (Exception)
                {

                    throw new UserFriendlyException(L("Record NOt Update"));
                }
            }

            return message;
        }

        public List<string> CreatOrUpdateTaluka(Taluka Input)
        {

            List<string> message = new List<string>();

            Taluka taluka = new Taluka();
            taluka.Id = Input.Id;
            taluka.StateId = Input.StateId;
            taluka.CreatorUserId = Input.CreatorUserId;
            taluka.DistrictId = Input.DistrictId;
            taluka.TalukaName = char.ToUpper(Input.TalukaName[0]) + Input.TalukaName.Substring(1);

            if (Input.Id == 0)
            {
                try
                {
                    var QueryCheck =  _talukaRepository.GetAll().Where(aa=>aa.TalukaName == Input.TalukaName && aa.StateId==Input.StateId && aa.DistrictId==Input.DistrictId ).ToList();
                    if (QueryCheck.Count == 0)
                    {
                        _talukaRepository.Insert(taluka);
                        message.Add("Taluka Added  sucessfully !");
                    }

                    else { message.Add("Record alread Present!"); }
                }
                catch (Exception ex)
                {

                    throw new UserFriendlyException(L("Record NOt Add"));
                }

            }
            else
            {
                try
                {
                    taluka.TenantId = Input.TenantId;
                    taluka.CreatorUserId = Input.CreatorUserId;
                    _talukaRepository.Update(taluka);
                    message.Add("Taluka Update  sucessfully !");

                }
                catch (Exception)
                {

                    throw new UserFriendlyException(L("Record NOt Update"));
                }
            }

            return message;
        }

        public List<string> CreatOrUpdateVillage(Village Input)
        {

            List<string> message = new List<string>();

            Village village = new Village();
            village.Id = Input.Id;
            village.StateID = Input.StateID;
            village.CreatorUserId = Input.CreatorUserId;
            village.DistrictId = Input.DistrictId;
            village.TalukaId = Input.TalukaId;
            village.Name = char.ToUpper(Input.Name[0]) + Input.Name.Substring(1);

            if (Input.Id == 0)
            {
                try
                {
                    var viilagename = _villageRepository.GetAll().Where(x => x.Name == Input.Name && x.TalukaId== Input.TalukaId).FirstOrDefault();

                    // var QueryCheck = (from a in _villageRepository.GetAll() where (a.Name == Input.Name) select a.Id).ToList();
                    //if (QueryCheck.Count == 0)
                    //{
                    if (viilagename == null)
                    {
                        _villageRepository.Insert(village);
                        message.Add("Village Added  sucessfully !");
                    }
                    else
                    {
                        message.Add("Village Already  Exist   !");
                    }
                  //  }

                   // else { message.Add("Record alread Present!"); }
                }
                catch (Exception ex)
                {

                    throw new UserFriendlyException(L("Record NOt Add"));
                }

            }
            else
            {
                try
                {
                    village.TenantId = Input.TenantId;
                    village.CreatorUserId = Input.CreatorUserId;
                    _villageRepository.Update(village);
                    message.Add("Village Update  sucessfully !");

                }
                catch (Exception)
                {

                    throw new UserFriendlyException(L("Record NOt Update"));
                }
            }

            return message;
        }
        public List<Taluka> GetAllTaluka(int DistrictId)
        {
            var query = _talukaRepository
                .GetAll()
                .Where(d => d.DistrictId == DistrictId)
                .ToList();
            return query;
        }


        public List<Taluka> GetallTalukas(string input)
     {


            var query = (from sa in _talukaRepository.GetAll().WhereIf(
                 !input.IsNullOrEmpty(),
                 p => p.TalukaName.ToUpper().Contains(input.ToUpper()) ||
                 p.TalukaName.ToLower().Contains(input.ToLower()))

                         join ur in UserManager.Users.ToList()
                         on sa.CreatorUserId equals ur.Id
                         select new Taluka()
                         {
                             TalukaName = sa.TalukaName,

                             CreationTime = sa.CreationTime,
                             CreatorUserId = sa.CreatorUserId,
                             Id = sa.Id,
                             DistrictId = sa.DistrictId,
                             StateId=sa.StateId,
                             TenantId = sa.TenantId
                         }).ToList().OrderByDescending(c => c.CreationTime);


           return new List<Taluka>(ObjectMapper.Map<List<Taluka>>(query));
            // return query;
        }

        public List<Village> Getallvillages(string input)
        {


            var query = (from sa in _villageRepository.GetAll().WhereIf(
                 !input.IsNullOrEmpty(),
                 p => p.Name.ToUpper().Contains(input.ToUpper()) ||
                 p.Name.ToLower().Contains(input.ToLower()))

                         join ur in UserManager.Users.ToList()
                         on sa.CreatorUserId equals ur.Id
                         
                         select new Village()
                         {
                             Name = sa.Name,

                             CreationTime = sa.CreationTime,
                             CreatorUserId = sa.CreatorUserId,
                             Id = sa.Id,
                             DistrictId = sa.DistrictId,
                             StateID = sa.StateID,
                             TalukaId=sa.TalukaId,
                             
                             TenantId = sa.TenantId
                         }).ToList().OrderBy(c => c.Name);


            return new List<Village>(ObjectMapper.Map<List<Village>>(query));
            // return query;
        }

        public string DeleteTaluka(EntityDto<int> Input)
        {


           // var check = (from aa in _talukaRepository.GetAll() where (aa.Id == Input.Id && aa.CreatorUserId == 2) select aa.Id).ToList();

          //  if (check.Count == 0)
          //  {
                _talukaRepository.Delete(Input.Id);

                return "Record Deleted ";


          //  }
         //   else
       //         return "Record Exist";

        }

        public string DeleteVillage(EntityDto<int> Input)
        {
     //    var check = (from aa in _villageRepository.GetAll() where (aa.Id == Input.Id && aa.CreatorUserId == 2) select aa.Id).ToList();
//    if (check.Count == 0)
        //    {
                _villageRepository.Delete(Input.Id);
                   return "Record Deleted ";
             //  }
         //   else
        //        return "Record Exist";
         }

        public List<District> GetAllDistricts(string input)
        {


            var query = (from sa in _districtRepository.GetAll().WhereIf(
                 !input.IsNullOrEmpty(),
                 p => p.DistrictName.ToUpper().Contains(input.ToUpper()) ||
                 p.DistrictName.ToLower().Contains(input.ToLower()) )
                         
                         join ur in UserManager.Users.ToList()
                         on sa.CreatorUserId equals ur.Id
                         select new District()
                         {
                             DistrictName = sa.DistrictName,
                            
                             CreationTime = sa.CreationTime,
                             CreatorUserId = sa.CreatorUserId,
                             Id = sa.Id,
                             StateId = sa.StateId,
                             TenantId = sa.TenantId
                         }).ToList().OrderByDescending(c => c.CreationTime);


            return new List<District>(ObjectMapper.Map<List<District>>(query));
            // return query;
        }
        public string DeleteDistrict(EntityDto<int> Input)
        {
        //     var check = (from aa in _districtRepository.GetAll() where (aa.Id == Input.Id && aa.CreatorUserId == 2) select aa.Id).ToList();
         //       if (check.Count == 0)
         //   {
                      _districtRepository.Delete(Input.Id);
                    return "Record Deleted ";
        //      }
       //     else
        //        return "Record Exist";
             }

        public List<Village> GetAllVillageByTalukaId(int TalukaID)
        {
            //  var query = _villageRepository.GetAll().Where(v => v.TalukaId == TalukaID).ToList();
            var query = (from v in _villageRepository.GetAll()
                         where (v.TalukaId==TalukaID)
                         select new Village()
                         {
                             Name = v.Name,
                             Id = v.Id
                         }).ToList().OrderBy(c => c.Name).ToList();
            return query;
        }

        //Parent Location List with Respect to perticular Talula
        //public List<Location> GetAllParentLocationWithRespectToTaluka(int TalukaId)
        //{
        //    var query = _locationRepository
        //        .GetAll()
        //        .Where(d => d.TalukaId == TalukaId)
        //        .ToList();
        //    return query;
        //}

        public List<LocationListDto> GetAllLocationWithUsers(string Input, int user_id)
        {
            var locationList = new List<Dto.LocationListDto>();
            var locations = _locationRepository.GetAll();          

            if (!string.IsNullOrEmpty(Input))
            {
                locations = locations.Where(v => v.Name.Contains(Input) && v.CreatorUserId == user_id);
            }
            else
            {
                locations = locations.Where(xx => xx.LocationType == LocationType.RRC);
            }

            foreach (var item in locations)
            {
                locationList.Add(
                            new LocationListDto()
                            {
                                Name = item.Name,
                                UserLocationId = item.Id.ToString()
                            });
            }

            var c = (from u in _userRepository.GetAll()
                     join l in locations on u.LocationId equals l.Id
                     select new LocationListDto()
                     {
                         Name = u.Name,
                         UserLocationId = Guid.NewGuid().ToString(),
                         parentId = u.LocationId,
                         UserId = u.Id,
                         CreatorUserID = u.CreatorUserId
                     });

            locationList = locationList.Union(c).Where(p => p != null).ToList();
            return locationList;
        }


        public List<LocationListDto>GetRespeactedLocation(List<int> pranetLocation,LocationType type)
        {
            List<LocationListDto> LocationList = new List<LocationListDto>();
            List<LocationListDto> Query=new List<LocationListDto>();
            foreach (var item in pranetLocation)
            {
                Query = (from l in _locationRepository.GetAll()
                             where (l.LocationType == type && l.ParentLocation == item)
                             select new LocationListDto
                             {
                                 Name = l.Name,
                                 Id = l.Id
                             }).ToList();
                foreach (var item1 in Query)
                {
                    LocationList.Add(item1);
                }
            }
            
            return LocationList;
        }

    }
}
