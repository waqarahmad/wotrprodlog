﻿using System.Threading.Tasks;
using Abp.Application.Services;
using WOTR.PM.Install.Dto;

namespace WOTR.PM.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}