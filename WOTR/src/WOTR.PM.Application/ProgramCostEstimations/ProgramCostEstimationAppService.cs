﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Components;
using WOTR.PM.ProgramCostEstimations.Dto;
using Abp.Application.Services.Dto;

namespace WOTR.PM.ProgramCostEstimations
{
    public class ProgramCostEstimationAppService : PMAppServiceBase, IProgramCostEstimationAppService
    {
        private readonly IRepository<ProgramCostEstimation> _ProgramCostEstimationRepository;
        private readonly IRepository<programCESubTotalCost> _programCESubTotalCostRepository;
        private readonly IRepository<ProgramCETotalCost> _programCETotalCostRepository;
        private readonly IRepository<ProgramCostEstimationOverall> _ProgramCostEstimationOverall;

        public ProgramCostEstimationAppService(IRepository<ProgramCostEstimation> ProgramCostEstimationRepository,
            IRepository<programCESubTotalCost> programCESubTotalCostRepository,
            IRepository<ProgramCETotalCost> programCETotalCostRepository, IRepository<ProgramCostEstimationOverall> ProgramCostEstimationOverall)

        {
            _ProgramCostEstimationRepository = ProgramCostEstimationRepository;
            _programCESubTotalCostRepository = programCESubTotalCostRepository;
            _programCETotalCostRepository = programCETotalCostRepository;
            _ProgramCostEstimationOverall = ProgramCostEstimationOverall;
        }


        public List<string> CreateOrUpdateProgramCostEstimation(List<ProgrameComponentListDto> Input)
        {
            List<string> result = new List<string>();
            ProgramCETotalCost ProgramCETotalCost = new ProgramCETotalCost();

            foreach (var item in Input)
            {
                ProgramCETotalCost.ProgramID = item.ProgrameId;
                ProgramCETotalCost.TotalCost = item.TotalCost;
                ProgramCETotalCost.TotalUnits = item.TotalUnits;
                ProgramCETotalCost.TotalUnitsCost = item.TotalUnitsCost;
                ProgramCETotalCost.Id = item.Id;
                ProgramCETotalCost.TenantId = (int)AbpSession.TenantId;
                if (item.Id == 0)
                {                    
                    _programCETotalCostRepository.Insert(ProgramCETotalCost);
                    result.Add("save");
                }
                else
                {                   
                        _programCETotalCostRepository.Update(ProgramCETotalCost);                    
                }


                foreach (var item2 in item.CostEstimationYear)
                {
                    if (item2.SubTotalTotalunits != 0 && item2.SubTotalofTotalCost != 0)
                    {
                        try
                        {
                            programCESubTotalCost programCESubTotalCost = new programCESubTotalCost();
                            programCESubTotalCost.ProgramID = item.ProgrameId;
                            programCESubTotalCost.CostEstimationYear = item2.CostEstimationYear;
                            programCESubTotalCost.ComponentID = item.ComponetTableId;
                            programCESubTotalCost.SubTotalofCommunityContribution = item2.SubTotalofCommunityContribution;
                            programCESubTotalCost.SubTotalofFunderContribution = item2.SubTotalofFunderContribution;
                            programCESubTotalCost.SubTotalofOtherContribution = item2.SubTotalofOtherContribution;
                            programCESubTotalCost.SubTotalofTotalCost = item2.SubTotalofTotalCost;
                            programCESubTotalCost.SubTotalTotalunits = item2.SubTotalTotalunits;
                            programCESubTotalCost.SubTotalUnitCost = item2.SubTotalUnitCost;
                            programCESubTotalCost.Id = item2.Id;
                            programCESubTotalCost.TenantId = (int)AbpSession.TenantId;
                            if (item2.Id == 0)
                            {
                                _programCESubTotalCostRepository.Insert(programCESubTotalCost);
                                result.Add("record Add");
                            }
                            else
                            {
                                _programCESubTotalCostRepository.Update(programCESubTotalCost);
                                result.Add("Record Updated");
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                    }
                    foreach (var item1 in item2.Activity)
                    {

                        if (item2.CostEstimationYear != "OverAll" && item1.ProgramCostEstimate_UnitOfMeasuresID != 0)
                        {
                            ProgramCostEstimation ProgramCostEstimation = new ProgramCostEstimation();
                            ProgramCostEstimation.CostEstimationYear = item2.CostEstimationYear;
                            ProgramCostEstimation.ActivityID = item1.ActivityID;
                            ProgramCostEstimation.ComponentID = item.ComponetTableId;
                            ProgramCostEstimation.ActivityLevel = item1.ActivityLevel;
                            ProgramCostEstimation.ProgramID = item1.ProgramCostEstimate_ProgramID;
                            ProgramCostEstimation.CommunityContribution = item1.ProgramCostEstimate_CommunityContribution;
                            ProgramCostEstimation.FunderContribution = item1.ProgramCostEstimate_FunderContribution;
                            ProgramCostEstimation.OtherContribution = item1.ProgramCostEstimate_OtherContribution;
                            ProgramCostEstimation.TotalUnitCost = item1.ProgramCostEstimate_TotalUnitCost;
                            ProgramCostEstimation.TotalUnits = item1.ProgramCostEstimate_TotalUnits;
                            ProgramCostEstimation.UnitCost = item1.ProgramCostEstimate_UnitCost;
                            ProgramCostEstimation.UnitOfMeasuresID = item1.ProgramCostEstimate_UnitOfMeasuresID;
                            ProgramCostEstimation.Id = item1.Id;
                            ProgramCostEstimation.TenantId = (int)AbpSession.TenantId;
                            if (item1.Id == 0)
                            {
                                try
                                {
                                    _ProgramCostEstimationRepository.Insert(ProgramCostEstimation);
                                    result.Add("Cost for Activity is SAVE");
                                }
                                catch (Exception)
                                {

                                    throw;
                                }

                            }
                            else
                            {                                
                                    _ProgramCostEstimationRepository.Update(ProgramCostEstimation);
                                    result.Add("Cost for Activity is Updated ");                               
                            }
                        }
                        else if (item1.ProgramCostEstimate_UnitOfMeasuresID != 0) {
                            ProgramCostEstimationOverall ProgramCostEstimationOverall = new ProgramCostEstimationOverall();
                            ProgramCostEstimationOverall.CostEstimationYear = item2.CostEstimationYear;
                            ProgramCostEstimationOverall.ActivityID = item1.ActivityID;
                            ProgramCostEstimationOverall.ComponentID = item.ComponetTableId;
                            ProgramCostEstimationOverall.ActivityLevel = item1.ActivityLevel;
                            ProgramCostEstimationOverall.ProgramID = item1.ProgramCostEstimate_ProgramID;
                            ProgramCostEstimationOverall.CommunityContribution = item1.ProgramCostEstimate_CommunityContribution;
                            ProgramCostEstimationOverall.FunderContribution = item1.ProgramCostEstimate_FunderContribution;
                            ProgramCostEstimationOverall.OtherContribution = item1.ProgramCostEstimate_OtherContribution;
                            ProgramCostEstimationOverall.TotalUnitCost = item1.ProgramCostEstimate_TotalUnitCost;
                            ProgramCostEstimationOverall.TotalUnits = item1.ProgramCostEstimate_TotalUnits;
                            ProgramCostEstimationOverall.UnitCost = item1.ProgramCostEstimate_UnitCost;
                            ProgramCostEstimationOverall.UnitOfMeasuresID = item1.ProgramCostEstimate_UnitOfMeasuresID;
                            ProgramCostEstimationOverall.Id = item1.Id;
                            ProgramCostEstimationOverall.TenantId = (int)AbpSession.TenantId;
                            if (item1.Id == 0)
                            {
                                try
                                {
                                    _ProgramCostEstimationOverall.Insert(ProgramCostEstimationOverall);
                                    result.Add("Cost for Activity is SAVE");
                                }
                                catch (Exception)
                                {

                                    throw;
                                }

                            }
                            else
                            {                                
                                    _ProgramCostEstimationOverall.Update(ProgramCostEstimationOverall);
                                    result.Add("Cost for Activity is Updated ");                                
                            }

                        }
                    }
                }
            }

            return result;
        }


        //...Delete
        public void DeleteActivityCostDetails(EntityDto<int> Input)
        {
            _ProgramCostEstimationRepository.Delete(Input.Id);
        }


    }
}
