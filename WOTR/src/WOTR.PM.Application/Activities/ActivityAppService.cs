﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Castle.Core.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.Activities.Dto;
using WOTR.PM.PrgActionAreaActivitysMappings;

namespace WOTR.PM.Activities
{
    public class ActivityAppService : PMAppServiceBase, IActivityAppService
    {

        private readonly IRepository<Activity> _activityRepository;
        private readonly IRepository<PrgActionAreaActivityMapping> _PrgActionAreaActivityMappingRepository;
        private List<string> strMessage = new List<string>();
        public ActivityAppService(IRepository<Activity> activityRepository, IRepository<PrgActionAreaActivityMapping> PrgActionAreaActivityMappingRepository)
        {
            _PrgActionAreaActivityMappingRepository = PrgActionAreaActivityMappingRepository;

            _activityRepository = activityRepository;
        }

        public List<string> CreateActivity(ActivitiyListDto Input)
        {
            try
            {
                Activity activity = new Activity();
                activity.Name = char.ToUpper(Input.Name[0]) + Input.Name.Substring(1);
                activity.Description = Input.Description;
                activity.UnitOfMeasuresId = Input.UnitOfMeasuresId;
                activity.Id = Input.Id;

                if (Input.Id == 0) //....Create New Activity
                {
                    var QueryCheck = (from a in _activityRepository.GetAll() where (a.Name == Input.Name) select a.Id).ToList();
                    if (QueryCheck.Count == 0)
                    {
                        _activityRepository.Insert(activity);
                        strMessage.Add("Activity Added Successfully");
                    }
                    else
                    {                    
                       strMessage.Add("Record is already is Present!");
                    }
                }
                else   //...Update Exsisting Acitvity
                {
                    activity.TenantId = Input.TenantId;
                    activity.CreatorUserId = Input.CreatorUserId;
                    _activityRepository.Update(activity);
                    strMessage.Add("Activity Updated Successfully");
                }
            }
            catch (Exception Ex)
            {
                strMessage.Add("Error= " + Ex.Message);
            }




            return strMessage;

        }

        public string DeleteActivity(EntityDto<int> Input)
        {

            var check = (from aa in _activityRepository.GetAll() where (aa.Id == Input.Id && aa.CreatorUserId == 2) select aa.Id).ToList();

            if (check.Count == 0)
            {
                var QueryCheck = (from a in _PrgActionAreaActivityMappingRepository.GetAll() where (a.ActivityID == Input.Id) select a.Id).ToList();
                if (QueryCheck.Count == 0)
                {
                    _activityRepository.Delete(Input.Id);

                    return "Record Deleted ";
                }
                else
                {
                    return "Record Exist";
                }
            }
            else
            {
                return "Record Exist";
            }

            //throw new NotImplementedException();
        }

        public List<ActivitiyListDto> GetAllActivities(string input)
        {

            var query = (from a in _activityRepository.GetAll()
                         join ur in UserManager.Users
                         on a.CreatorUserId equals ur.Id
                         select new ActivitiyListDto()
                         {
                             Name = a.Name,
                             Description = a.Description,
                             TenantId = a.TenantId,
                             FirstName = ur.Surname,
                             Id = a.Id,
                             UnitOfMeasuresId = a.UnitOfMeasuresId,
                             CreatorUserId =a.CreatorUserId
                         }).WhereIf(!string.IsNullOrEmpty(input), p => p.Name.ToLower().Contains(input.ToLower())).ToList();

            var result = ObjectMapper.Map<List<ActivitiyListDto>>(query);

            return result;
        }
    }
}
