﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using WOTR.PM.NewImpactIndicator;
using WOTR.PM.NewImpactIndicator.Sources.Dto;
using WOTR.PM.NewImpactIndicator.Sources;
using Abp.UI;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;

namespace WOTR.PM.Sources
{
    public class SourceAppService : PMAppServiceBase, ISourceAppService
    {
        private readonly IRepository<Source> _SourceRepository;

        public SourceAppService(IRepository<Source> SourceRepository)
        {
            _SourceRepository = SourceRepository;
        }

        public List<GetAllSource> GetAllSource(string filter)
        {
            var CropListreturn = new List<GetAllSource>();
            // List<GetAllCrops> CropList = new List<GetAllCrops>();
            if (filter == null)
            {
                var CropList = _SourceRepository.GetAll().ToList();
                CropListreturn = ObjectMapper.Map<List<GetAllSource>>(CropList);

            }
            else
            {
                var CropList = _SourceRepository.GetAll().ToList().Where(x => x.SourceName.ToLower().Contains(filter.Trim().ToLower())); ;
                CropListreturn = ObjectMapper.Map<List<GetAllSource>>(CropList);
            }
            return CropListreturn;
        }

        public string CreatOrUpdateSource(Source Input)
        {

            string message;

            Source source = new Source();
            source.SourceName = char.ToUpper(Input.SourceName[0]) + Input.SourceName.Substring(1);

            if (Input.Id == 0)
            {
                try
                {
                    var QueryCheck = _SourceRepository.GetAll().Where(aa => aa.SourceName == Input.SourceName).ToList();
                    if (QueryCheck.Count == 0)
                    {
                        _SourceRepository.Insert(source);
                        message = "Source Added  sucessfully !";
                    }

                    else { message = "Record alread Present!"; }
                }
                catch (Exception ex)
                {

                    throw new UserFriendlyException(L("Record NOt Add"));
                }

            }
            else
            {
                try
                {
                    source.TenantId = Input.TenantId;
                    source.CreatorUserId = Input.CreatorUserId;
                    source.Id = Input.Id;
                    source.SourceName = Input.SourceName;
                    _SourceRepository.Update(source);
                    message = "Source Update  sucessfully !";

                }
                catch (Exception)
                {

                    throw new UserFriendlyException(L("Record NOt Update"));
                }
            }

            return message;
        }

        public async Task Delete(EntityDto input)
        {
           await _SourceRepository.DeleteAsync(input.Id);
        }
    }
}
