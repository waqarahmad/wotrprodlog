﻿using Abp.Domain.Repositories;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Dto;
using WOTR.PM.EntityFrameworkCore.Repositories;
using WOTR.PM.Locations;
using WOTR.PM.ProgramCostEstimations;
using WOTR.PM.ProgramRegionSpendings.Dto;
using WOTR.PM.Programs;
//archita
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using WOTR.PM.ActionAreas;
using WOTR.PM.Authorization.Users.Exporting;
using WOTR.PM.Components;
using WOTR.PM.Donors;
using WOTR.PM.Programs.Dto;
using WOTR.PM.Programs.Exporting;
using WOTR.PM.PrgActionAreaActivitysMappings;
using System.IO;
//archita
using A = DocumentFormat.OpenXml.Drawing;
using Xdr = DocumentFormat.OpenXml.Drawing.Spreadsheet;
using A14 = DocumentFormat.OpenXml.Office2010.Drawing;
using System.Drawing;
using ShapeProperties = DocumentFormat.OpenXml.Drawing.Spreadsheet.ShapeProperties;
using Color = DocumentFormat.OpenXml.Spreadsheet.Color;
using WOTR.PM.PrgRequestAAPlas.Dto;
using WOTR.PM.PrgRequestAAPlas;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services.Dto;
using FontFamily = DocumentFormat.OpenXml.Spreadsheet.FontFamily;
//archita



namespace WOTR.PM.ProgramRegionSpendings
{
    public class ProgramRegionSpendingAppService : PMAppServiceBase, IProgramRegionSpendingAppServices
    {
        private readonly IRepository<ProgramCostEstimation> _ProgramCostEstimationRepository;
        private readonly IRepository<programCESubTotalCost> _programCESubTotalCostRepository;
        private readonly IRepository<ProgramCETotalCost> _ProgramCETotalCostRepository;
        private readonly IRepository<Locations.Location> _locationRepository;
        private readonly IRepository<VillageCluster> _villageClusterRepository;
        private readonly IRepository<ProgramRegionSpending> _ProgramRegionSpendingRepository;
        private readonly ProgramRegionandSpendingRepository _programRegionSpendingRepossitorySQL;
        private readonly IRepository<Program> _ProgramRepository;
        private readonly IAppFolders _appFolders;
        private IRepository<Village> _villageRepository;


        private readonly IRepository<RequestAAplas> _RequestAAplasRepository;
        public ProgramRegionSpendingAppService(IRepository<ProgramCostEstimation> ProgramCostEstimationRepository, IRepository<RequestAAplas> RequestAAplasRepository,
            IRepository<programCESubTotalCost> programCESubTotalCostRepository,
            IRepository<ProgramCETotalCost> ProgramCETotalCostRepository,
            IRepository<Locations.Location> LocationRepository,
            IRepository<Program> ProgramRepository,
            IRepository<VillageCluster> villageClusterRepository,
            IRepository<ProgramRegionSpending> ProgramRegionSpendingRepository,
            ProgramRegionandSpendingRepository programRegionSpendingRepossitorySQL, IRepository<Village> villageRepository,
            IAppFolders appFolders
         )

        {
            _villageRepository = villageRepository;
            _RequestAAplasRepository = RequestAAplasRepository;
            _ProgramCostEstimationRepository = ProgramCostEstimationRepository;
            _programCESubTotalCostRepository = programCESubTotalCostRepository;
            _ProgramCETotalCostRepository = ProgramCETotalCostRepository;
            _locationRepository = LocationRepository;
            _ProgramRegionSpendingRepository = ProgramRegionSpendingRepository;
            _villageClusterRepository = villageClusterRepository;
            _programRegionSpendingRepossitorySQL = programRegionSpendingRepossitorySQL;
            _ProgramRepository = ProgramRepository;
            _appFolders = appFolders;

        }

        public async Task<List<string>> CreateOrUpdateProgramRegionSpending(List<ProgramRegionSpendingListDto> Input)
        {
            var result = new List<string>();

            foreach (var item in Input)
            {

                foreach (var activity in item.CostEstimationYear)
                {
                    foreach (var act in activity.Activity)
                    {

                        if (act.ActivityLevel == ActivityLevel.ProgramLevel)
                        {
                            var programeLevelLocationId = _locationRepository.FirstOrDefault(l => l.Name == "Ahmednagar(HQ)").Id;
                            if (act.regionId == 0)
                            {                                
                                var programRegionSpending = new ProgramRegionSpending();
                                programRegionSpending.ActivityID = act.ActivityID;
                                programRegionSpending.ComponentID = item.ComponentID;
                                programRegionSpending.CostEstimationYear = act.CostEstimationYear;
                                programRegionSpending.ProgramID = item.ProgramID;
                                programRegionSpending.UnitOfMeasuresID = act.UnitOfMeasuresID;
                                programRegionSpending.isProgramLevel = act.IsProgrameLevel;
                                programRegionSpending.FunderContribution = act.Grant;
                                programRegionSpending.LocationID = programeLevelLocationId;
                                programRegionSpending.OtherContribution = act.OtherContribution;
                                programRegionSpending.TotalUnitCost = act.TotalUnitCost;
                                programRegionSpending.TotalUnits = act.TotalUnits;
                                programRegionSpending.UnitCost = act.UnitCost;
                                programRegionSpending.CommunityContribution = act.CommunityContribution;

                                try
                                {
                                    await _ProgramRegionSpendingRepository.InsertAsync(programRegionSpending);
                                    result.Add("Cost for Activity is SAVE");
                                }
                                catch (Exception)
                                {
                                    throw;
                                }
                            }
                            else
                            {
                                var d =await _ProgramRegionSpendingRepository.FirstOrDefaultAsync(t => t.Id == act.regionId);
                                d.LocationID = programeLevelLocationId;
                                await _ProgramRegionSpendingRepository.UpdateAsync(d);
                            }
                        }
                        else
                        {

                            foreach (var sub in act.SubUnitActivity)
                            {
                                if (sub.regionId == 0)
                                {
                                    var programRegionSpending = new ProgramRegionSpending();
                                    programRegionSpending.ActivityID = act.ActivityID;
                                    programRegionSpending.ComponentID = item.ComponentID;
                                    programRegionSpending.CostEstimationYear = act.CostEstimationYear;
                                    programRegionSpending.ProgramID = item.ProgramID;
                                    programRegionSpending.UnitOfMeasuresID = act.UnitOfMeasuresID;
                                    programRegionSpending.isProgramLevel = act.IsProgrameLevel;
                                    programRegionSpending.FunderContribution = sub.Grant;
                                    programRegionSpending.LocationID = sub.LocationID;
                                    programRegionSpending.OtherContribution = sub.OtherContribution;
                                    programRegionSpending.TotalUnitCost = sub.TotalUnitCost;
                                    programRegionSpending.TotalUnits = sub.TotalUnits;
                                    programRegionSpending.UnitCost = sub.UnitCost;
                                    programRegionSpending.CommunityContribution = sub.CommunityContribution;
                                    programRegionSpending.TenantId = (int)AbpSession.TenantId;
                                    try
                                    {
                                        programRegionSpending.Id = 0;
                                        await _ProgramRegionSpendingRepository.InsertAsync(programRegionSpending);
                                        result.Add("Cost for Activity is SAVE");
                                    }
                                    catch (Exception)
                                    {
                                        throw;
                                    }
                                }
                                else
                                {
                                    var regionSpending =
                                        _ProgramRegionSpendingRepository.FirstOrDefault(t => t.Id == sub.regionId);
                                    regionSpending.ActivityID = act.ActivityID;
                                    regionSpending.ComponentID = item.ComponentID;
                                    regionSpending.CostEstimationYear = act.CostEstimationYear;
                                    regionSpending.ProgramID = item.ProgramID;
                                    regionSpending.UnitOfMeasuresID = act.UnitOfMeasuresID;
                                    regionSpending.isProgramLevel = act.IsProgrameLevel;
                                    regionSpending.FunderContribution = sub.Grant;
                                    regionSpending.LocationID = sub.LocationID;
                                    regionSpending.OtherContribution = sub.OtherContribution;
                                    regionSpending.TotalUnitCost = sub.TotalUnitCost;
                                    regionSpending.TotalUnits = sub.TotalUnits;
                                    regionSpending.UnitCost = sub.UnitCost;
                                    regionSpending.CommunityContribution = sub.CommunityContribution;
                                    regionSpending.TenantId = (int)AbpSession.TenantId;
                                    await _ProgramRegionSpendingRepository.UpdateAsync(regionSpending);
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public async Task<List<ProgramRegionSpendingListDto>> GetProgramRegionSpendingDeatails(int ProgramID)
        {
            var TenantId = AbpSession.TenantId;
            long? rId;
            var result = new List<ProgramRegionSpendingListDto>();


            var query = _programRegionSpendingRepossitorySQL.GetComponentandActivityForRegionandSpending(TenantId, ProgramID);

            if (query != null)
            {
                var listOfComponent = query.ToList().GroupBy(x => x.ComponentID)
                                      .Select(g => g.First())
                                      .ToList();

                var dates = new List<string>();

                long start = 0;
                long end = 0;
                if (query[0].ProgrameStartDate.Month < 4)
                {
                    start = query[0].ProgrameStartDate.Year - 1;
                    end = query[0].ProgrameStartDate.Year;
                }
                else
                {
                    start = query[0].ProgrameStartDate.Year;
                    end = query[0].ProgrameStartDate.Year + 1;
                }

                for (var dt = start; dt <= query[0].ProgrameEndDate.Year; dt = dt + 1)
                {
                    var from = DateTime.Parse("April 1," + (start));
                    var to = DateTime.Parse("March 31, " + end);
                    if (query[0].ProgrameEndDate.Month > to.Month || query[0].ProgrameEndDate.Year > from.Year)
                    {
                        dates.Add(start.ToString() + "-" + end.ToString());
                    }
                    start++;
                    end++;

                }

                foreach (var item in listOfComponent)
                {
                    var obj1 = new List<ProgrameCostEstimationYearRegionSpendingDto>();

                    var obj = new List<ListActivityforProgamRegionSpendingDto>();
                    
                    var activity = query[0].CostEstimationYear[0].Activity
                        .Where(t => t.ActivityID != 0 && t.ComponentID == item.ComponentID);
                    foreach (var act in activity)
                    {
                        var subUnit = new List<SubListActivityforProgamRegionSpendingDto>();
                        if (act.ActivityLevel != ActivityLevel.ProgramLevel)
                        {
                            subUnit = _ProgramRegionSpendingRepository.GetAll().Where(pr => pr.ProgramID == ProgramID && pr.ActivityID == act.ActivityID && pr.CostEstimationYear == act.CostEstimationYear && pr.ComponentID == act.ComponentID).
                                       Select(prg => new SubListActivityforProgamRegionSpendingDto
                                       {
                                           LocationID = prg.LocationID,
                                           TotalUnits = prg.TotalUnits,
                                           UnitCost = prg.UnitCost,
                                           TotalUnitCost = prg.TotalUnitCost,
                                           CommunityContribution = prg.CommunityContribution,
                                           Grant = prg.FunderContribution,
                                           OtherContribution = prg.OtherContribution,
                                           regionLocationName = prg.Location.Name + "," + prg.LocationID,
                                           regionId = prg.Id,
                                           villageId = prg.Location.VillageId,

                                       }).ToList();


                            rId = 0;
                        }
                        else
                        {
                            var programReg = await _ProgramRegionSpendingRepository.GetAll().Where(pr =>
                                 pr.ProgramID == ProgramID && pr.ActivityID == act.ActivityID &&
                                 pr.CostEstimationYear == act.CostEstimationYear).ToListAsync();

                            rId = !programReg.Any() ? 0 : programReg.FirstOrDefault().Id;
                        }
                        obj.Add(new ListActivityforProgamRegionSpendingDto
                        {
                            ActivityID = act.ActivityID,
                            ActivityName = act.ActivityName,
                            CostEstimationYear = act.CostEstimationYear,
                            UnitOfMeasuresID = act.UnitOfMeasuresID,
                            TotalUnits = act.TotalUnits,
                            UnitCost = act.UnitCost,
                            TotalUnitCost = act.TotalUnitCost,
                            CommunityContribution = act.CommunityContribution,
                            Grant = act.Grant,
                            OtherContribution = act.OtherContribution,
                            ComponentID = act.ComponentID,
                            UnitOfMeasuresName = act.UnitOfMeasuresName,
                            ActivityLevel = act.ActivityLevel,
                            SubUnitActivity = subUnit,
                            regionId = rId,

                        });
                    }
                    foreach (var item2 in dates)
                    {
                        var i = new List<ListActivityforProgamRegionSpendingDto>();

                        foreach (var u in obj.Where(t=>t.CostEstimationYear == item2).ToList())
                        {
                                i.Add(u);
                        }

                        obj1.Add(new ProgrameCostEstimationYearRegionSpendingDto
                        {
                            CostEstimationYear = item2,
                            Activity = i
                        });

                    }

                    result.Add(new ProgramRegionSpendingListDto
                    {

                        ComponentID = item.ComponentID,
                        ProgramID = item.ProgramID,
                        ProgrameStartDate = item.ProgrameStartDate,
                        ProgrameEndDate = item.ProgrameEndDate,
                        ComponentName = item.ComponentName,
                        CostEstimationYear = obj1

                    });
                }

            }
            return result;
        }

        public string ProgramName(int programId)
        {
            var program = _ProgramRepository.FirstOrDefault(p => p.Id == programId);
            return program.Name;
        }

        public async Task Delete(EntityDto Input)
        {
            await _ProgramRegionSpendingRepository.DeleteAsync(t => t.Id == Input.Id);
        }

        public async Task<FileDto> CreateYearWiseRegionExcelDoc(string fileName, int programId,string year)
        {

            var result = await GetProgramRegionSpendingDeatailsforexcelreports(programId, year);
            var result1 = ProgramName(programId);
            string a = fileName + DateTime.Now.ToString("dd_MM_yyyy_HHmmss") + @".xlsx";

            using (SpreadsheetDocument xl = SpreadsheetDocument.Create(a, SpreadsheetDocumentType.Workbook))
            {

                WorkbookPart wbp = xl.AddWorkbookPart();
                WorksheetPart wsp = wbp.AddNewPart<WorksheetPart>();
                Workbook wb = new Workbook();
                FileVersion fv = new FileVersion();
                fv.ApplicationName = "Microsoft Office Excel";
                Worksheet ws = new Worksheet();
                SheetData sd = new SheetData();
                WorkbookStylesPart stylePart = wbp.AddNewPart<WorkbookStylesPart>();
                stylePart.Stylesheet = GenerateStylesheet();
                stylePart.Stylesheet.Save();

                var sImagePath = Environment.CurrentDirectory + "/src/assets/common/images/Reportlogo.png";
                DrawingsPart dp = wsp.AddNewPart<DrawingsPart>();
                ImagePart imgp = dp.AddImagePart(ImagePartType.Png, wsp.GetIdOfPart(dp));
                using (FileStream fs = new FileStream(sImagePath, FileMode.Open))
                {
                    imgp.FeedData(fs);
                }

                NonVisualDrawingProperties nvdp = new NonVisualDrawingProperties();
                nvdp.Id = 1025;
                nvdp.Name = "WOTRLogoReport";
                nvdp.Description = "WOTRLogoReport";
                DocumentFormat.OpenXml.Drawing.PictureLocks picLocks = new DocumentFormat.OpenXml.Drawing.PictureLocks();
                picLocks.NoChangeAspect = true;
                picLocks.NoChangeArrowheads = true;
                NonVisualPictureDrawingProperties nvpdp = new NonVisualPictureDrawingProperties();
                nvpdp.PictureLocks = picLocks;
                NonVisualPictureProperties nvpp = new NonVisualPictureProperties();
                nvpp.NonVisualDrawingProperties = nvdp;
                nvpp.NonVisualPictureDrawingProperties = nvpdp;

                DocumentFormat.OpenXml.Drawing.Stretch stretch = new DocumentFormat.OpenXml.Drawing.Stretch();
                stretch.FillRectangle = new DocumentFormat.OpenXml.Drawing.FillRectangle();

                BlipFill blipFill = new BlipFill();
                DocumentFormat.OpenXml.Drawing.Blip blip = new DocumentFormat.OpenXml.Drawing.Blip();
                blip.Embed = dp.GetIdOfPart(imgp);
                blip.CompressionState = DocumentFormat.OpenXml.Drawing.BlipCompressionValues.Print;
                blipFill.Blip = blip;
                blipFill.SourceRectangle = new DocumentFormat.OpenXml.Drawing.SourceRectangle();
                blipFill.Append(stretch);

                DocumentFormat.OpenXml.Drawing.Transform2D t2d = new DocumentFormat.OpenXml.Drawing.Transform2D();
                DocumentFormat.OpenXml.Drawing.Offset offset = new DocumentFormat.OpenXml.Drawing.Offset();
                offset.X = 0;
                offset.Y = 0;
                t2d.Offset = offset;
                Bitmap bm = new Bitmap(sImagePath);
                //http://en.wikipedia.org/wiki/English_Metric_Unit#DrawingML
                //http://stackoverflow.com/questions/1341930/pixel-to-centimeter
                //http://stackoverflow.com/questions/139655/how-to-convert-pixels-to-points-px-to-pt-in-net-c
                DocumentFormat.OpenXml.Drawing.Extents extents = new DocumentFormat.OpenXml.Drawing.Extents();
                extents.Cx = (long)bm.Width * (long)((float)914400 / bm.HorizontalResolution);
                extents.Cy = (long)bm.Height * (long)((float)914400 / bm.VerticalResolution);
                bm.Dispose();
                t2d.Extents = extents;
                ShapeProperties sp = new ShapeProperties();
                sp.BlackWhiteMode = DocumentFormat.OpenXml.Drawing.BlackWhiteModeValues.Auto;
                sp.Transform2D = t2d;
                DocumentFormat.OpenXml.Drawing.PresetGeometry prstGeom = new DocumentFormat.OpenXml.Drawing.PresetGeometry();
                prstGeom.Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle;
                prstGeom.AdjustValueList = new DocumentFormat.OpenXml.Drawing.AdjustValueList();
                sp.Append(prstGeom);
                sp.Append(new DocumentFormat.OpenXml.Drawing.NoFill());

                DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture picture = new DocumentFormat.OpenXml.Drawing.Spreadsheet.Picture();
                picture.NonVisualPictureProperties = nvpp;
                picture.BlipFill = blipFill;
                picture.ShapeProperties = sp;

                Position pos = new Position();
                pos.X = 0;
                pos.Y = 0;
                Extent ext = new Extent();
                ext.Cx = extents.Cx;
                ext.Cy = extents.Cy;
                AbsoluteAnchor anchor = new AbsoluteAnchor();
                anchor.Position = pos;
                anchor.Extent = ext;
                anchor.Append(picture);
                anchor.Append(new ClientData());
                WorksheetDrawing wsd = new WorksheetDrawing();
                wsd.Append(anchor);
                Drawing drawing = new Drawing();
                drawing.Id = dp.GetIdOfPart(imgp);

                wsd.Save(dp);

                ws.Append(sd);
                ws.Append(drawing);
                wsp.Worksheet = ws;
                wsp.Worksheet.Save();
                Sheets sheets = new Sheets();
                Sheet sheet = new Sheet();
                sheet.Name = "Sheet1";
                sheet.SheetId = 1;
                sheet.Id = wbp.GetIdOfPart(wsp);
                // sheets.Append(sheet);
                wb.Append(fv);

                Row row = new Row();
                Row row1 = new Row();
                Row row2 = new Row();
                Row row3 = new Row();
                Row row4 = new Row();
                Row row5 = new Row();
                Row row6 = new Row();
                Row row7 = new Row();
                Row row8 = new Row();
                Row row9 = new Row();

                Cell cell11 = InsertCellInWorksheet("D", 1, wsp);
                cell11.CellValue = new CellValue("Watershed Organisation Trust (WOTR)");
                cell11.DataType = new EnumValue<CellValues>(CellValues.Number);

                Run run1 = new Run();
                run1.Append(new Text("Watershed Organisation Trust (WOTR)"));
                RunProperties run1Properties = new RunProperties();
                run1Properties.Append(new Bold());
                run1.RunProperties = run1Properties;
                InlineString inlineString = new InlineString();
                inlineString.Append(run1);
                cell11.Append(inlineString);

                Cell cell1 = InsertCellInWorksheet("D", 2, wsp);
                cell1.CellValue = new CellValue("Year Wise Region");
                cell1.DataType = new EnumValue<CellValues>(CellValues.Number);

                Run run2 = new Run();
                run2.Append(new Text("Year Wise Region"));
                RunProperties run2Properties = new RunProperties();
                Color color3 = new Color() { Rgb = "2F75B5" };
                FontSize fontSize3 = new FontSize() { Val = 16D };
                run2Properties.Append(new Bold());
                run2Properties.Append(color3);
                run2Properties.Append(fontSize3);
                run2.RunProperties = run2Properties;
                InlineString inlineString1 = new InlineString();
                inlineString1.Append(run2);
                cell1.Append(inlineString1);

                Cell cell12 = InsertCellInWorksheet("D", 3, wsp);
                cell12.CellValue = new CellValue("Program Title: " + result1.ToString());
                cell12.DataType = new EnumValue<CellValues>(CellValues.Number);

                Cell cell13 = InsertCellInWorksheet("D", 4, wsp);
                cell13.CellValue = new CellValue("Year: " + year);
                cell13.DataType = new EnumValue<CellValues>(CellValues.Number);

                row3.Append(

                    ConstructCell(" ", CellValues.String));

                sd.AppendChild(row3);

                foreach (var imp in result)
                {




                    row3 = new Row();
                    row3.Append(

                        ConstructCell("", CellValues.String, 1));
                    sd.AppendChild(row3);

                    Row row90 = new Row();
                    if (imp.CostEstimationYear[0].Activity.Count() != 0)
                    {
                        if (imp.CostEstimationYear[0].Activity[0].SubUnitActivity.Count() > 0)
                        {
                            row90.Append(

                                ConstructCell("Region  Name :" + imp.CostEstimationYear[0].Activity[0].SubUnitActivity[0].regionLocationName.ToString(), CellValues.String, 2));
                            sd.AppendChild(row90);
                        }
                    }
                    row2 = new Row();

                    row2.Append(

                        ConstructCell("Component  Name :" + imp.ComponentName.ToString(), CellValues.String, 1));
                    sd.AppendChild(row2);

                    row6 = new Row();
                    row5 = new Row();
                    row1 = new Row();
                    int countt = 7;
                    List<ListActivityforProgamRegionSpendingDto> listActivityforProgamRegionSpendingDtos = new List<ListActivityforProgamRegionSpendingDto>();

                    int costYearCnt = 0;
                    foreach (var c in imp.CostEstimationYear)
                    {
                        if (costYearCnt == 0)
                        {
                            row5.Append(
                       ConstructCell("Activity Name", CellValues.String, 2),
                       ConstructCell("Total Unit", CellValues.String, 2),
                       ConstructCell("Unit Cost", CellValues.String, 2),
                       ConstructCell("Total Unit Cost", CellValues.String, 2),
                       ConstructCell("Community Contribution", CellValues.String, 2),
                        ConstructCell("Funder Contribution", CellValues.String, 2),

                       ConstructCell("Other Contribution", CellValues.String, 2),
                       ConstructCell("Action Level", CellValues.String, 2)
                       );
                        }
                        else
                        {
                            row5 = new Row();
                            row5.Append(
                     ConstructCell("Activity Name", CellValues.String, 2),
                     ConstructCell("Total Unit", CellValues.String, 2),
                     ConstructCell("Unit Cost", CellValues.String, 2),
                     ConstructCell("Total Unit Cost", CellValues.String, 2),
                     ConstructCell("Community Contribution", CellValues.String, 2),
                    ConstructCell("Funder Contribution", CellValues.String, 2),

                     ConstructCell("Other Contribution", CellValues.String, 2),
                     ConstructCell("Action Level", CellValues.String, 2),
                     ConstructCell(" ", CellValues.String, 2));
                        }
                        costYearCnt++;

                        for (var i = 0; i < countt; i++)
                        {
                            if (i == countt / 2)
                            {
                               row6 = new Row();
                           //     row6.Append(
                           //ConstructCell("Year: "+c.CostEstimationYear, CellValues.String, 1));
                                sd.AppendChild(row6);
                                sd.AppendChild(row5);

                            }
                            else
                            {
                                row6 = new Row();
                                row6.Append(
                        ConstructCell(" ", CellValues.String, 2));
                            }
                        }
                        var totalcost = 0.00;
                        var toatlcost12 = 0.00;
                        var TotalUnits = 0.00;
                        var unitcost = 0.00;
                        var TotalUnitCost = 0.00;
                        var communitycontribution = 0.00;
                        var fundercontribution = 0.00;
                        var othercontribution = 0.00;
                        foreach (var act in c.Activity)
                        {
                            listActivityforProgamRegionSpendingDtos.Add(act);

                            TotalUnits += Convert.ToInt64(act.TotalUnits);
                            unitcost += Convert.ToInt64(act.UnitCost);
                            TotalUnitCost += Convert.ToInt64(act.TotalUnitCost);
                            communitycontribution += Convert.ToDouble(act.CommunityContribution);
                            fundercontribution += Convert.ToDouble(act.Grant);
                            othercontribution += Convert.ToDouble(act.OtherContribution);
                            row4 = new Row();
                            row4.Append(
                                ConstructCell(act.ActivityName.ToString(), CellValues.String, 1),
                                ConstructCell((act.TotalUnits).ToString(), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(act.UnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(act.TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(act.CommunityContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                ConstructCell(Convert.ToDecimal(act.Grant).ToString("#,##0.00"), CellValues.Number, 1),

                                ConstructCell(Convert.ToDecimal(act.OtherContribution).ToString("#,##0.00"), CellValues.Number, 1),
                                ConstructCell(act.ActivityLevel.ToString(), CellValues.String, 1)
                                
                                );
                            sd.AppendChild(row4);
                        }

                        row1 = new Row();
                        row1.Append(

                            ConstructCell("Total", CellValues.String, 2),
                            ConstructCell(TotalUnits.ToString(), CellValues.Number, 2),
                              ConstructCell(Convert.ToDecimal(unitcost).ToString("#,##0.00"), CellValues.Number, 2),
                              ConstructCell(Convert.ToDecimal(TotalUnitCost).ToString("#,##0.00"), CellValues.Number, 2),
                              ConstructCell(Convert.ToDecimal(communitycontribution).ToString("#,##0.00"), CellValues.Number, 2),
                              ConstructCell(Convert.ToDecimal(fundercontribution).ToString("#,##0.00"), CellValues.Number, 2),
                              ConstructCell(Convert.ToDecimal(othercontribution).ToString("#,##0.00"), CellValues.Number, 2),
                              ConstructCell(" ", CellValues.String, 2)
                              );

                        sd.AppendChild(row1);

                    }




                }


                //archita
                sheets.Append(sheet);
                wb.Append(sheets);
                xl.WorkbookPart.Workbook = wb;
                wbp.Workbook.Save();
                //archita

                //wsp.Worksheet.Save();
            }


            var file = new FileDto(a, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            return file;



        }


        //archita
        private object GetComponentCostEstimation(int programId)
        {
            throw new NotImplementedException();
        }

        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            // If the worksheet does not contain a row with the specified row index, insert one.
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            // If there is not a cell with the specified column name, insert one.  
            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                // Cells must be in sequential order according to CellReference. Determine where to insert the new cell.
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
        private Cell ConstructCell(string value, CellValues dataType, uint styleIndex = 0)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType),
                StyleIndex = styleIndex
            };
        }
        //archita
        private Stylesheet GenerateStylesheet()
        {
            Stylesheet styleSheet = null;

            Fonts fonts = new Fonts(
                new DocumentFormat.OpenXml.Spreadsheet.Font( // Index 0 - default
                    new FontSize() { Val = 11 }

                ),
                new DocumentFormat.OpenXml.Spreadsheet.Font( // Index 1 - header
                    new FontSize() { Val = 11 },
                    new Bold(),
                    new Color() { Rgb = "000000" }
                ));

            Fills fills = new Fills(
                    new Fill(new PatternFill() { PatternType = PatternValues.None }), // Index 0 - default
                    new Fill(new PatternFill() { PatternType = PatternValues.LightGray }), // Index 1 - default
                    new Fill(new PatternFill(new ForegroundColor { Rgb = new HexBinaryValue() { Value = "D0CECE" } })
                    { PatternType = PatternValues.Solid }) // Index 2 - header

                );

            Borders borders = new Borders(
                    new Border(), // index 0 default
                    new Border( // index 1 black border
                        new LeftBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new RightBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new TopBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new BottomBorder(new Color() { Auto = true }) { Style = BorderStyleValues.Thin },
                        new DiagonalBorder())
                );

            CellFormats cellFormats = new CellFormats(
                    new CellFormat(), // default
                    new CellFormat { FontId = 0, FillId = 0, BorderId = 1, ApplyBorder = true }, // body
                    new CellFormat { FontId = 1, FillId = 2, BorderId = 1, ApplyFill = true } // header
                );

            styleSheet = new Stylesheet(fonts, fills, borders, cellFormats);

            return styleSheet;
        }
        //archita


        public async Task<List<ProgramRegionSpendingListDto>> GetProgramRegionSpendingDeatailsforexcelreports(int ProgramID,string year)
        {
            var TenantId = AbpSession.TenantId;
            long? rId;
            var result = new List<ProgramRegionSpendingListDto>();
            var result1 = new List<ProgramRegionSpendingListDto>();



            var query = _programRegionSpendingRepossitorySQL.GetComponentandActivityForRegionandSpending(TenantId, ProgramID);

            if (query != null)
            {
                var listOfComponent = query.ToList().GroupBy(x => x.ComponentID)
                                      .Select(g => g.First())
                                      .ToList();

                var dates = new List<string>();

                long start = 0;
                long end = 0;
                if (query[0].ProgrameStartDate.Month < 4)
                {
                    start = query[0].ProgrameStartDate.Year - 1;
                    end = query[0].ProgrameStartDate.Year;
                }
                else
                {
                    start = query[0].ProgrameStartDate.Year;
                    end = query[0].ProgrameStartDate.Year + 1;
                }

                for (var dt = start; dt <= query[0].ProgrameEndDate.Year; dt = dt + 1)
                {
                    var from = DateTime.Parse("April 1," + (start));
                    var to = DateTime.Parse("March 31, " + end);
                    if (query[0].ProgrameEndDate.Month > to.Month || query[0].ProgrameEndDate.Year > from.Year)
                    {
                        dates.Add(start.ToString() + "-" + end.ToString());
                    }
                    start++;
                    end++;

                }

                foreach (var item in listOfComponent)
                {
                    var obj1 = new List<ProgrameCostEstimationYearRegionSpendingDto>();

                    var obj = new List<ListActivityforProgamRegionSpendingDto>();

                    var activity = query[0].CostEstimationYear[0].Activity
                        .Where(t => t.ActivityID != 0 && t.ComponentID == item.ComponentID);
                    foreach (var act in activity)
                    {
                        var subUnit = new List<SubListActivityforProgamRegionSpendingDto>();
                        if (act.ActivityLevel != ActivityLevel.ProgramLevel)
                        {
                            subUnit = _ProgramRegionSpendingRepository.GetAll().Where(pr => pr.ProgramID == ProgramID && pr.ActivityID == act.ActivityID && pr.CostEstimationYear == act.CostEstimationYear && pr.ComponentID == act.ComponentID).
                                       Select(prg => new SubListActivityforProgamRegionSpendingDto
                                       {
                                           LocationID = prg.LocationID,
                                           TotalUnits = prg.TotalUnits,
                                           UnitCost = prg.UnitCost,
                                           TotalUnitCost = prg.TotalUnitCost,
                                           CommunityContribution = prg.CommunityContribution,
                                           FunderCosntribution=prg.FunderContribution,
                                           Grant = prg.FunderContribution,
                                           OtherContribution = prg.OtherContribution,
                                           regionLocationName = prg.Location.Name ,
                                           regionId = prg.Id,
                                           villageId = prg.Location.VillageId,

                                       }).ToList();


                            rId = 0;
                        }
                        else
                        {
                            var programReg = await _ProgramRegionSpendingRepository.GetAll().Where(pr =>
                                 pr.ProgramID == ProgramID && pr.ActivityID == act.ActivityID &&
                                 pr.CostEstimationYear == act.CostEstimationYear).ToListAsync();

                            rId = !programReg.Any() ? 0 : programReg.FirstOrDefault().Id;
                        }
                        obj.Add(new ListActivityforProgamRegionSpendingDto
                        {
                            ActivityID = act.ActivityID,
                            ActivityName = act.ActivityName,
                            CostEstimationYear = act.CostEstimationYear,
                            UnitOfMeasuresID = act.UnitOfMeasuresID,
                            TotalUnits = act.TotalUnits,
                            UnitCost = act.UnitCost,
                            TotalUnitCost = act.TotalUnitCost,
                            CommunityContribution = act.CommunityContribution,
                            FunderContribution=act.FunderContribution,
                            Grant = act.Grant,
                            OtherContribution = act.OtherContribution,
                            ComponentID = act.ComponentID,
                            UnitOfMeasuresName = act.UnitOfMeasuresName,
                            ActivityLevel = act.ActivityLevel,
                            SubUnitActivity = subUnit,
                            regionId = rId,

                        });
                    }
                    foreach (var item2 in dates)
                    {
                        var i = new List<ListActivityforProgamRegionSpendingDto>();

                        foreach (var u in obj.Where(t => t.CostEstimationYear == item2).ToList())
                        {
                            i.Add(u);
                        }

                        obj1.Add(new ProgrameCostEstimationYearRegionSpendingDto
                        {
                            CostEstimationYear = item2,
                            Activity = i
                        });

                    }

                    result.Add(new ProgramRegionSpendingListDto
                    {

                        ComponentID = item.ComponentID,
                        ProgramID = item.ProgramID,
                        ProgrameStartDate = item.ProgrameStartDate,
                        ProgrameEndDate = item.ProgrameEndDate,
                        ComponentName = item.ComponentName,
                        CostEstimationYear = obj1

                    });
                  
                }
                

            }
            result.ForEach(xx =>
            {
                xx.CostEstimationYear= xx.CostEstimationYear.FindAll(ss => ss.CostEstimationYear == year);
            });

            
            return result;
        }

    }
}
