﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.NewImpactIndicator.Crops;
//using WOTR.PM.NewImpactIndicator.Crops.Dto;
using Abp.Application.Services.Dto;
using WOTR.PM.NewImpactIndicator.Crops.Dto;
using Abp.Domain.Repositories;
using WOTR.PM.NewImpactIndicator;
using Abp.UI;

namespace WOTR.PM.Crops
{
    public class CropAppService : PMAppServiceBase, ICropAppService
    {
        private readonly IRepository<Crop> _CropRepository;
        public CropAppService(
            IRepository<Crop> cropRepository
            ) {
            _CropRepository = cropRepository;
        }
        public List<GetAllCrops> GetAllCrop(string filter)
        {
            var CropListreturn = new List<GetAllCrops>();
            // List<GetAllCrops> CropList = new List<GetAllCrops>();
            if (filter == null)
            {
                var CropList = _CropRepository.GetAll().ToList();
                 CropListreturn = ObjectMapper.Map<List<GetAllCrops>>(CropList);

            }
            else {
                var CropList = _CropRepository.GetAll().ToList().Where(x => x.Name.ToLower().Contains(filter.Trim().ToLower())); ;
                CropListreturn = ObjectMapper.Map<List<GetAllCrops>>(CropList);
            }
            return CropListreturn;
        }

        public string CreatOrUpdateCrop(Crop Input)
        {

            string message;

            Crop crop = new Crop();
            crop.Name = char.ToUpper(Input.Name[0]) + Input.Name.Substring(1);

            if (Input.Id == 0)
            {
                try
                {
                    var QueryCheck = _CropRepository.GetAll().Where(aa => aa.Name == Input.Name).ToList();
                    if (QueryCheck.Count == 0)
                    {
                        _CropRepository.Insert(crop);
                        message= "Crop Added  sucessfully !";
                    }

                    else { message = "Record alread Present!"; }
                }
                catch (Exception ex)
                {

                    throw new UserFriendlyException(L("Record NOt Add"));
                }

            }
            else
            {
                try
                {
                    crop.TenantId = Input.TenantId;
                    crop.CreatorUserId = Input.CreatorUserId;
                    crop.Id = Input.Id;
                    crop.Name = Input.Name;
                    _CropRepository.Update(crop);
                    message = "Crop Update  sucessfully !";

                }
                catch (Exception)
                {

                    throw new UserFriendlyException(L("Record NOt Update"));
                }
            }

            return message;
        }

        public async Task Delete(EntityDto input)
        {
            try
            {
                await _CropRepository.DeleteAsync(input.Id);
            }
            catch (Exception ex)
            {

                throw;
            }

        }
    }
}
