﻿using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Extensions;
using Abp.Collections.Extensions;
using WOTR.PM.ActionAreas;

namespace WOTR.PM.Components
{
    public class ComponentActivityMappingAppService : PMAppServiceBase, IComponentActivityMappingAppService
    {
        private readonly IRepository<ComponentActivityMapping> _componentActivityMapRepository;
        private readonly IRepository<Component> _componentRepository;
        private readonly IRepository<Activity> _activityRepository;
        private List<string> strMessage = new List<string>();
        private readonly IComponentActivityMappingRepository _ComponentActivityMappingRepository;


        public ComponentActivityMappingAppService(IRepository<ComponentActivityMapping> componentActivityMapRepository,
            IRepository<Component> componentRepository,
            IComponentActivityMappingRepository ComponentActivityMappingRepository,
            IRepository<Activity> activityRepository)
        {
            _componentActivityMapRepository = componentActivityMapRepository;
            _componentRepository = componentRepository;
            _ComponentActivityMappingRepository = ComponentActivityMappingRepository;
            _activityRepository = activityRepository;
        }

        public List<string> CreateComponentActivityMapping(ComponentActivityMapListDto Input)
        {
            try
            {
                if (Input.ActID.Count != 0)
                {
                    foreach (var item in Input.ActID)
                    {
                        ComponentActivityMapping componentActivityMapping = new ComponentActivityMapping();
                        componentActivityMapping.ActivityID = item;
                        componentActivityMapping.ComponentID = Input.ComponentID;
                        componentActivityMapping.TenantId = Input.TenantId;
                        componentActivityMapping.Id = Input.Id;
                        Component component = new Component();
                        component.Name = Input.componentName;
                        component.Id = Input.ComponentID;

                        var cmpnt = _componentRepository.FirstOrDefault(Input.ComponentID);
                        cmpnt.Name = Input.componentName;
                        _componentRepository.Update(cmpnt);

                        if (Input.Id == 0) //....Create New Component
                        {
                            _componentActivityMapRepository.Insert(componentActivityMapping);
                            strMessage.Add("Component Activity Added Successfully");
                        }
                        else   //...Update Exsisting Component
                        {

                            _componentActivityMapRepository.Update(componentActivityMapping);
                            strMessage.Add("Component Activity Updated Successfully");
                        }
                    }//try
                }
                else if (Input.ActID.Count == 0) {
                    Component component = new Component();
                    component.Name = Input.componentName; 
                    var cmpnt = _componentRepository.FirstOrDefault(Input.ComponentID);
                    cmpnt.Name = Input.componentName;
                    _componentRepository.Update(cmpnt);
                    strMessage.Add("Component Activity Updated Successfully");
                }
            }
            catch (Exception Ex)
            {
                strMessage.Add("Error= " + Ex.Message);
            }//Catch

            return strMessage;
        }

        public void DeleteComponentActivityMapping(EntityDto<int> Input)
        {
            _componentActivityMapRepository.Delete(Input.Id);
            // throw new NotImplementedException();
        }



        public List<ComponentActivityMapListDto> GetAllComponentActivityMapping()
        {
            var query = (from c in _componentActivityMapRepository.GetAll()
                         select new ComponentActivityMapListDto()
                         {
                             ActivityID = Convert.ToInt32(c.ActivityID),
                             ComponentID = Convert.ToInt32(c.ComponentID),
                             TenantId = c.TenantId

                         }).ToList();
            return query;
        }


        public List<ComponentActivityMapListDto> GetAllComponentWithMappingActivity()
        {

            var Query = (from c in _componentRepository.GetAll()
                         join cm in _componentActivityMapRepository.GetAll()
                         on c.Id equals cm.ComponentID
                         select new ComponentActivityMapListDto()
                         {
                             ActivityID = Convert.ToInt32(cm.ActivityID),
                             ComponentID = Convert.ToInt32(cm.ComponentID),
                             TenantId = c.TenantId
                         }).ToList();

            return Query;

        }

        // GETAll ComponentActivityMapping Details BY SQL query

        public List<GetComponentActivityMappingListDto> GetAllComponentandActivitymappingforcomponenttable()
        {

            //List<GetComponentActivityMappingListDto> result = new List<GetComponentActivityMappingListDto>();
            //var Query = (from c in _componentRepository.GetAll() 

            //             join ca in _componentActivityMapRepository.GetAll() on c.Id equals ca.ComponentID
            //             join a in _activityRepository.GetAll() on ca.ActivityID equals a.Id
            //             select new GetComponentActivityMappingListDto
            //             {
            //                 Id = a.Id,
            //                 ActivityID = int.Parse(ca.ActivityID.ToString()),
            //                 ActivityName = a.Name,
            //                 ActivityDescri = a.Description,
            //                 FullName = a.CreatorUserId.ToString(),
            //                 ComponetTableId=ca.Id,
            //                 CreationTime = a.CreationTime,
            //                 ComponentMappingID = ca.ComponentID.ToString(),
            //                 ComponentCode = c.Code.ToString(),
            //                 componentName = c.Name,
            //                 TenantId = c.TenantId
            //             }).ToList().GroupBy(x => x.ComponentMappingID);


            //foreach (var item in Query)
            //{
            //    GetComponentActivityMappingListDto obj = new GetComponentActivityMappingListDto();
            //    List<ComponentAndactivityMappingForList> activityList = new List<ComponentAndactivityMappingForList>();
            //    foreach (var a in item)
            //    {
            //        ComponentAndactivityMappingForList aobj = new ComponentAndactivityMappingForList();
            //        obj.componentName = a.componentName;
            //        obj.ComponentCode = a.ComponentCode;
            //        obj.ComponetTableId = a.ComponetTableId;

            //        aobj.ActivityDescri = a.ActivityDescri;
            //        aobj.ActivityID = a.ActivityID;
            //        aobj.ActivityName = a.ActivityName;
            //        aobj.FullName = a.FullName;
            //        aobj.CreationTime = a.CreationTime;
            //        activityList.Add(aobj);
            //    }
            //    obj.Activity = activityList;
            //    result.Add(obj);
            //}

            //return result;

            long? userId = AbpSession.UserId;
            int? TenantId = AbpSession.TenantId;

            List<GetComponentActivityMappingListDto> query = _ComponentActivityMappingRepository.GetComponentActivityMappingDetils(TenantId, userId);

            List<GetComponentActivityMappingListDto> result = new List<GetComponentActivityMappingListDto>();

            if (query != null)
            {
                var ListOfUsers = query.ToList().GroupBy(x => x.ComponetTableId)
                                      .Select(g => g.First())
                                      .ToList();

                foreach (var item in ListOfUsers)
                {
                    List<ComponentAndactivityMappingForList> obj = new List<ComponentAndactivityMappingForList>();
                    foreach (var act in query)
                    {
                        if (act.ActivityID != 0)
                        {
                            if (item.ComponetTableId == act.ComponetTableId)
                            {
                                obj.Add(new ComponentAndactivityMappingForList
                                {
                                    CreationTime = act.CreationTime,
                                    ActivityDescri = act.ActivityDescri,
                                    ActivityName = act.ActivityName,
                                    ActivityID = act.ActivityID,
                                    TenantId = act.TenantId,
                                    FullName = act.FullName,
                                    Id = act.Id
                                });
                            }
                        }
                    }

                    result.Add(new GetComponentActivityMappingListDto
                    {
                        ComponetTableId = item.ComponetTableId,
                        componentName = item.componentName,
                        ComponentCode = item.ComponentCode,
                        code = Convert.ToInt64(item.ComponentCode),
                        ComponentCreatime = item.ComponentCreatime,
                        ComponentActivityMappingCreationTime = item.ComponentActivityMappingCreationTime,
                        Activity = obj,
                        ActivityCount = obj.Count()
                    });
                }
            }
            return result;
        }




          
            
        

        public List<ComponentListDto> GetAllComponentsearch(string input)
        {


            var query = (from c in _componentRepository.GetAll().WhereIf(
                 !input.IsNullOrEmpty(),
                 p => p.Name.ToUpper().Contains(input.ToUpper()) ||
                 p.Name.ToLower().Contains(input.ToLower())) 
                        
                         select new ComponentListDto()
                         {
                             Name = c.Name,
                             Code = c.Code,
                             TenantId = c.TenantId,
                             Id = c.Id

                         }).ToList();
            return query;
            // throw new NotImplementedException();
        }


        public List<GetComponentActivityMappingListDto> GetAllComponentandActivitymappingforcomponenttablesearch(string input)
        {
            long? userId = AbpSession.UserId;
            int? TenantId = AbpSession.TenantId;

            List<GetComponentActivityMappingListDto> query = _ComponentActivityMappingRepository.GetComponentActivityMappingDetils(TenantId, userId);

            List<GetComponentActivityMappingListDto> result = new List<GetComponentActivityMappingListDto>();

            if (query != null)
            {
                var ListOfUsers = query.ToList().GroupBy(x => x.ComponetTableId)
                                      .Select(g => g.First())
                                      .ToList().WhereIf(
                 !input.IsNullOrEmpty(),
                 p => p.componentName.ToUpper().Contains(input.ToUpper()) ||
                 p.componentName.ToLower().Contains(input.ToLower())

                         );

                foreach (var item in ListOfUsers)
                {
                    List<ComponentAndactivityMappingForList> obj = new List<ComponentAndactivityMappingForList>();
                    foreach (var act in query)
                    {
                        if (act.ActivityID != 0)
                        {
                            if (item.ComponetTableId == act.ComponetTableId)
                            {
                                obj.Add(new ComponentAndactivityMappingForList
                                {
                                    CreationTime = act.CreationTime,
                                    ActivityDescri = act.ActivityDescri,
                                    ActivityName = act.ActivityName,
                                    ActivityID = act.ActivityID,
                                    TenantId = act.TenantId,
                                    FullName = act.FullName,
                                    Id = act.Id
                                });
                            }
                        }
                    }

                    result.Add(new GetComponentActivityMappingListDto
                    {
                        ComponetTableId = item.ComponetTableId,
                        componentName = item.componentName,
                        ComponentCode = item.ComponentCode,
                        code = Convert.ToInt64(item.ComponentCode),
                        ComponentCreatime = item.ComponentCreatime,
                        ComponentActivityMappingCreationTime = item.ComponentActivityMappingCreationTime,
                        Activity = obj,
                        ActivityCount = obj.Count()
                    });
                }
            }

            return result;
        }

    }
}
