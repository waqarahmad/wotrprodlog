﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Castle.Core.Internal;
using WOTR.PM.Programs;

namespace WOTR.PM.Components
{
    public class ComponentAppService : PMAppServiceBase, IComponentAppService
    {
        private readonly IRepository<ProgramComponentsMapping> _ProgramComponentsMappingRepository;


        private readonly IRepository<Component> _componentRepository;
        private List<string> strMessage = new List<string>();
        public ComponentAppService(IRepository<Component> componentRepository, IRepository<ProgramComponentsMapping> ProgramComponentsMappingRepository)
        {
            _componentRepository = componentRepository;
            _ProgramComponentsMappingRepository = ProgramComponentsMappingRepository;
        }

        public long getComponentCode()
        {
            long id = (_componentRepository.GetAll().ToList().Count());
            return id;
        }



        public List<string> CreateComponent(GetComponentActivityMappingListDto Input)
        {

            List<string> message = new List<string>();


            long id = getComponentCode();
            try
            {
                Component component = new Component();
                component.Name = char.ToUpper(Input.componentName[0]) + Input.componentName.Substring(1);
                if (id == 0)
                {
                    component.Code = 1;
                }
                else
                {
                    component.Code = ++id;
                }

                component.Id = Input.Id;
                component.TenantId = Input.TenantId;

                if (Input.Id == 0) //....Create New Component
                {
                    var QueryCheck = (from a in _componentRepository.GetAll() where (a.Code == Input.code && a.Name == Input.componentName) select a.Id).ToList();
                    if (QueryCheck.Count == 0)
                    {
                        _componentRepository.Insert(component);
                        strMessage.Add("Component Added Successfully");
                    }
                    else { strMessage.Add("Record is already Preasent!"); }
                }
                else   //...Update Exsisting Component
                {
                    _componentRepository.Update(component);
                    strMessage.Add("Component Updated Successfully");
                }
            }//try
            catch (Exception Ex)
            {
                strMessage.Add("Error= " + Ex.Message);
            }//Catch




            return strMessage;
        }

        //public void DeleteComponent(EntityDto<int> Input)
        //{
        //    throw new NotImplementedException();
        //    _componentRepository.Delete(Input.Id);
        //}



        public string DeleteComponent(EntityDto<int> Input)
        {

            var QueryCheck = (from a in _ProgramComponentsMappingRepository.GetAll() where (a.ComponentID == Input.Id) select a.Id).ToList();
            if (QueryCheck.Count == 0)
            {
                _componentRepository.Delete(Input.Id);

                return "Record Deleted ";
            }
            else
            {
                return "Record Exist";
            }


        }

        public List<ComponentListDto> GetAllComponents()
        {


            var query = (from c in _componentRepository.GetAll()
                         select new ComponentListDto()
                         {
                             Name = c.Name,
                             Code = c.Code,
                             TenantId = c.TenantId,
                             Id = c.Id

                         }).ToList();
            return query;
            // throw new NotImplementedException();
        }
    }
}
