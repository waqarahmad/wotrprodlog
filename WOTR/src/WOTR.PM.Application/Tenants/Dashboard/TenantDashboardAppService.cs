﻿using Abp.Auditing;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using WOTR.PM.Authorization;
using WOTR.PM.Configuration;
using WOTR.PM.Locations;
using WOTR.PM.PrgRequestAAPlas;
using WOTR.PM.PrgRequestAAPlas.Dto;
using WOTR.PM.ProgramCostEstimations;
using WOTR.PM.ProgramExpenses;
using WOTR.PM.Programs;
using WOTR.PM.States;
using WOTR.PM.Tenants.Dashboard.Dto;

namespace WOTR.PM.Tenants.Dashboard
{


    [DisableAuditing]
    [AbpAuthorize(AppPermissions.Pages_Tenant_Dashboard)]
    public class TenantDashboardAppService : PMAppServiceBase, ITenantDashboardAppService
    {
        private readonly IConfigurationRoot _appConfiguration;
        private readonly IRepository<RequestAAplas> _RequestAAplasRepository;
        private readonly IRepository<ProgramExpense> _ProgramExpenseRepository;
        private readonly IRepository<Program> _programRepository;
        private IRepository<Locations.Location> _LocationRepository;
        private IRepository<Village> _VillageRepository;
        private readonly IRepository<VillageCluster> _villageClusterRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<ProgramCostEstimation> _programCostEstimationRepository;



        public TenantDashboardAppService(IHostingEnvironment env, IRepository<ProgramCostEstimation> programCostEstimationRepository, IRepository<State> stateRepository, IRepository<VillageCluster> villageClusterRepository, IRepository<RequestAAplas> RequestAAplasRepository, IRepository<ProgramExpense> ProgramExpenseRepository, IRepository<Program> programRepository, IRepository<Locations.Location> LocationRepository, IRepository<Village> VillageRepository)
        {
            _appConfiguration = env.GetAppConfiguration();
            _RequestAAplasRepository = RequestAAplasRepository;
            _ProgramExpenseRepository = ProgramExpenseRepository;
            _programRepository = programRepository;
            _LocationRepository = LocationRepository;
            _VillageRepository = VillageRepository;
            _villageClusterRepository = villageClusterRepository;
            _stateRepository = stateRepository;
            _programCostEstimationRepository = programCostEstimationRepository;
        }

        public GetMemberActivityOutput GetMemberActivity()
        {
            return new GetMemberActivityOutput
            (
                DashboardRandomDataGenerator.GenerateMemberActivities()
            );
        }

        public GetDashboardDataOutput GetDashboardData(GetDashboardDataInput input)
        {
            var output = new GetDashboardDataOutput
            {
                TotalProfit = DashboardRandomDataGenerator.GetRandomInt(5000, 9000),
                NewFeedbacks = DashboardRandomDataGenerator.GetRandomInt(1000, 5000),
                NewOrders = DashboardRandomDataGenerator.GetRandomInt(100, 900),
                NewUsers = DashboardRandomDataGenerator.GetRandomInt(50, 500),
                SalesSummary = DashboardRandomDataGenerator.GenerateSalesSummaryData(input.SalesSummaryDatePeriod),
                Expenses = DashboardRandomDataGenerator.GetRandomInt(5000, 10000),
                Growth = DashboardRandomDataGenerator.GetRandomInt(5000, 10000),
                Revenue = DashboardRandomDataGenerator.GetRandomInt(1000, 9000),
                TotalSales = DashboardRandomDataGenerator.GetRandomInt(10000, 90000),
                TransactionPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                NewVisitPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                BouncePercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                DailySales = DashboardRandomDataGenerator.GetRandomArray(30, 10, 50),
                ProfitShares = DashboardRandomDataGenerator.GetRandomPercentageArray(3)
            };

            return output;
        }

        public GetSalesSummaryOutput GetSalesSummary(GetSalesSummaryInput input)
        {
            return new GetSalesSummaryOutput(DashboardRandomDataGenerator.GenerateSalesSummaryData(input.SalesSummaryDatePeriod));
        }

        public GetWorldMapOutput GetWorldMap(GetWorldMapInput input)
        {
            return new GetWorldMapOutput(DashboardRandomDataGenerator.GenerateWorldMapCountries());
        }

        public GetGeneralStatsOutput GetGeneralStats(GetGeneralStatsInput input)
        {
            return new GetGeneralStatsOutput
            {
                TransactionPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                NewVisitPercent = DashboardRandomDataGenerator.GetRandomInt(10, 100),
                BouncePercent = DashboardRandomDataGenerator.GetRandomInt(10, 100)
            };
        }


        public async Task ConnectionConnect(SqlConnection conn)
        {
            try
            {
                if (conn.State == ConnectionState.Closed)
                {
                    await conn.OpenAsync();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task AddDataInDataTable(ProgramManagerDashboardData programManagerDashboardDatas, int fillDataTable = 0)
        {
            try
            {
                foreach (DataRow row in programManagerDashboardDatas.dataTable.Rows)
                {
                    if (fillDataTable == 1)
                    {
                        programManagerDashboardDatas.ProgramManagerStateWiseBudgets.Add(new ProgramManagerStateWiseBudget
                        {
                            StateName = (row["StateName"] == DBNull.Value) ? "" : Convert.ToString(row["StateName"]),
                            FunderContribution = (row["FunderContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["FunderContribution"]),
                            Expense = (row["Expense"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Expense"]),
                            TotalProject = (row["TotalProject"] == DBNull.Value) ? 00 : Convert.ToInt32(row["TotalProject"]),

                        });
                    }
                    else if (fillDataTable == 4)
                    {
                        programManagerDashboardDatas.ProgramManagerWiseProjectManagerCounts.Add(new ProgramManagerWiseProjectManagerCount
                        {
                            ProjectManagerCount = (row["TotalProjectManager"] == DBNull.Value) ? 00 : Convert.ToInt32(row["TotalProjectManager"]),
                        });
                    }
                    else if (fillDataTable == 6)
                    {
                        programManagerDashboardDatas.StateWiseDemands.Add(new StateWiseDemand
                        {
                            StateName = (row["StateName"] == DBNull.Value) ? "" : Convert.ToString(row["StateName"]),
                            Demand = (row["Demand"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Demand"]),
                            StateId = (row["StateId"] == DBNull.Value) ? 00 : Convert.ToInt32(row["StateId"]),
                        });
                    }
                    else if (fillDataTable == 9)
                    {
                        programManagerDashboardDatas.ProgramManagerDashboardInfos.Add(new ProgramManagerDashboardInfo
                        {
                            TotalProject = (row["TotalProject"] == DBNull.Value) ? 00 : Convert.ToInt32(row["TotalProject"]),
                            FunderContribution = (row["TotalBudget"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["TotalBudget"]),
                            TotalAchivement = (row["TotalAchivement"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["TotalAchivement"]),
                            TeamMember = (row["TeamMember"] == DBNull.Value) ? 00 : Convert.ToInt32(row["TeamMember"]),
                        });
                    }
                    else if (fillDataTable == 10)
                    {
                        programManagerDashboardDatas.ProgramManagerWiseTeamMemberInfos.Add(new ProgramManagerWiseTeamMemberInfo
                        {
                            Staff_Name = (row["Staff_Name"] == DBNull.Value) ? "" : Convert.ToString(row["Staff_Name"]),
                            MobileNumber = (row["MobileNumber"] == DBNull.Value) ? "" : Convert.ToString(row["MobileNumber"]),
                            EmailAddress = (row["EmailAddress"] == DBNull.Value) ? "" : Convert.ToString(row["EmailAddress"]),
                            Designation = (row["Designation"] == DBNull.Value) ? "" : Convert.ToString(row["Designation"]),
                        });
                    }
                    else if (fillDataTable == 11)
                    {
                        programManagerDashboardDatas.ProgramManagerWiseProjectLists.Add(new ProgramManagerWiseProjectList
                        {
                            ProjectName = (row["ProjectName"] == DBNull.Value) ? "" : Convert.ToString(row["ProjectName"]),
                            ProjectManagerName = (row["ProjectManagerName"] == DBNull.Value) ? "" : Convert.ToString(row["ProjectManagerName"]),
                        });
                    }
                    else if (fillDataTable == 15)
                    {
                        programManagerDashboardDatas.managerwiseWiseDemands.Add(new ManagerwiseWiseDemand
                        {
                            ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),
                            Demand = (row["Demand"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Demand"]),
                            ProgramId = (row["PID"] == DBNull.Value) ? 00 : Convert.ToInt16(row["PID"]),
                        });
                    }

                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<AllChartDto> ProgramManagerDashboard()
        {
            AllChartDto allChartData = new AllChartDto();
            List<PieChartData> piechart = new List<PieChartData>();

            try
            {
                int input = Convert.ToInt32(GetCurrentUser().Id);

                ProgramManagerDashboardData programManagerDashboardData = new ProgramManagerDashboardData();
                programManagerDashboardData.ProgramManagerStateWiseBudgets = new List<ProgramManagerStateWiseBudget>();
                programManagerDashboardData.ProgramManagerProgramStateWiseBudgets = new List<ProgramManagerProgramStateWiseBudget>();
                programManagerDashboardData.ProgramManagerProgramYearwiseStateWiseBudgets = new List<ProgramManagerProgramYearwiseStateWiseBudget>();
                programManagerDashboardData.ProgramManagerWiseProjectManagerCounts = new List<ProgramManagerWiseProjectManagerCount>();
                programManagerDashboardData.ProgramManagerImplemenationPlanCheckListWiseBudgets = new List<ProgramManagerImplemenationPlanCheckListWiseBudget>();
                programManagerDashboardData.StateWiseDemands = new List<StateWiseDemand>();
                programManagerDashboardData.DemandWiseProjects = new List<DemandWiseProject>();
                programManagerDashboardData.ProgramManagerAmountSanctions = new List<ProgramManagerAmountSanction>();
                programManagerDashboardData.ProgramManagerDashboardInfos = new List<ProgramManagerDashboardInfo>();
                programManagerDashboardData.ProgramManagerWiseTeamMemberInfos = new List<ProgramManagerWiseTeamMemberInfo>();
                programManagerDashboardData.ProgramManagerWiseProjectLists = new List<ProgramManagerWiseProjectList>();

                string value;


                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    //await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();


                    //using (var cmd = new SqlCommand("", conn))
                    //{
                    //    // ProjectWiseInfo
                    //    var dataTable = new DataTable();
                    //    cmd.CommandTimeout = 0;
                    //    cmd.CommandText = "[dbo].[sp_ProgramManagerStateWiseBudget]";
                    //    cmd.CommandType = CommandType.StoredProcedure;
                    //    cmd.Parameters.Add(new SqlParameter("@ProgramManagerId", input));

                    //    SqlDataReader dr = cmd.ExecuteReader();
                    //    dataTable.Load(dr);
                    //    if (dataTable.Rows.Count != 0)
                    //    {
                    //        value = dataTable.Rows[0][0].ToString();
                    //        if (value != "")
                    //        {
                    //            programManagerDashboardData.dataTable = null;
                    //            programManagerDashboardData.dataTable = dataTable;
                    //            await AddDataInDataTable(programManagerDashboardData, 1);
                    //        }
                    //    }
                    //    conn.Close();
                    //}
                    //await ConnectionConnect(conn);

                    //using (var cmdOne = new SqlCommand("", conn))
                    //{

                    //    //ProjectWiseInfoPieChart
                    //    var dataTable = new DataTable();
                    //    cmdOne.CommandTimeout = 0;
                    //    cmdOne.CommandText = "[dbo].[sp_ProgramManagerProgramStateWiseBudget]";
                    //    cmdOne.CommandType = CommandType.StoredProcedure;
                    //    cmdOne.Parameters.Add(new SqlParameter("@ProgramManagerId", input));

                    //    SqlDataReader dr = cmdOne.ExecuteReader();
                    //    dataTable.Load(dr);
                    //    if (dataTable.Rows.Count != 0)
                    //    {
                    //        value = dataTable.Rows[0][0].ToString();

                    //        if (value != "")
                    //        {
                    //            programManagerDashboardData.dataTable = null;
                    //            programManagerDashboardData.dataTable = dataTable;
                    //            await AddDataInDataTable(programManagerDashboardData, 2);
                    //        }
                    //    }
                    //    conn.Close();
                    //}
                    await ConnectionConnect(conn);

                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_ProjectManagerCount]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@ProgramManagerId", input));

                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 4);
                            }
                        }
                        conn.Close();
                    }
                    await ConnectionConnect(conn);

                    using (var cmdFour = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdFour.CommandTimeout = 0;
                        cmdFour.CommandText = "[dbo].[sp_StateWiseDemand]";
                        cmdFour.CommandType = CommandType.StoredProcedure;
                        cmdFour.Parameters.Add(new SqlParameter("@ProgramManagerId", input));

                        SqlDataReader dr = cmdFour.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 6);
                            }
                        }
                        conn.Close();
                    }
                    await ConnectionConnect(conn);

                    using (var cmdSeven = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdSeven.CommandTimeout = 0;
                        cmdSeven.CommandText = "[dbo].[sp_ProgramManagerDashboardInfo]";
                        cmdSeven.CommandType = CommandType.StoredProcedure;
                        cmdSeven.Parameters.Add(new SqlParameter("@ProgramManagerId", input));

                        SqlDataReader dr = cmdSeven.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 9);
                            }
                        }
                        conn.Close();
                    }
                    await ConnectionConnect(conn);

                    using (var cmdEigth = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdEigth.CommandTimeout = 0;
                        cmdEigth.CommandText = "[dbo].[sp_ProgramManagerWiseTeamMemberInfo]";
                        cmdEigth.CommandType = CommandType.StoredProcedure;
                        cmdEigth.Parameters.Add(new SqlParameter("@ProgramManagerId", input));

                        SqlDataReader dr = cmdEigth.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 10);
                            }
                        }
                        conn.Close();
                    }
                    await ConnectionConnect(conn);

                    using (var cmdNine = new SqlCommand("", conn))
                    {

                        var dataTable = new DataTable();
                        cmdNine.CommandTimeout = 0;
                        cmdNine.CommandText = "[dbo].[sp_ProgramManagerWiseProjectList]";
                        cmdNine.CommandType = CommandType.StoredProcedure;
                        cmdNine.Parameters.Add(new SqlParameter("@ProgramManagerId", input));

                        SqlDataReader dr = cmdNine.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 11);
                            }
                        }
                        conn.Close();
                    }
                }

                // ProgramManagerStateWiseBudgetlineChart
                List<BarChartData> ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerStateWiseBudgetlineChart = programManagerDashboardData.ProgramManagerStateWiseBudgets.ToList().GroupBy(l => l.StateName).ToList();
                ProgramManagerStateWiseBudgetlineChart.ForEach(l =>
                {
                    BarChartData stateWiseBudgetbar = new BarChartData();
                    stateWiseBudgetbar.labels = new List<string>();
                    //stateWiseBudgetbar.label = string;
                    stateWiseBudgetbar.datasets = new List<BarChartDataset>();
                    stateWiseBudgetbar.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(stateWiseBudgetbar);
                });

                for (int i = 0; i < ProgramManagerStateWiseBudgetlineChart.Count; i++)
                {
                    var items = ProgramManagerStateWiseBudgetlineChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerStateWiseBudgetlineChart[i].Key);
                    ProgramManagerBarChart[i].label = ProgramManagerStateWiseBudgetlineChart[i].Key;


                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Budget";
                            obj.data.Add(ProgramManagerStateWiseBudgetlineChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "ActualPerformance";
                            obj1.data.Add(ProgramManagerStateWiseBudgetlineChart[i].ToList().Sum(c => c.Expense));
                            a.Add(obj1);
                        }
                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerBarChart = ProgramManagerBarChart;

                // ProgramManagerProgramStateWiseBudgetLineChart.
                ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerProgramStateWiseBudgetlineChart = programManagerDashboardData.ProgramManagerProgramStateWiseBudgets.ToList().GroupBy(l => l.ProjectName).ToList();

                ProgramManagerProgramStateWiseBudgetlineChart.ForEach(l =>
                {
                    BarChartData ProgramStateWiseBudgetbar = new BarChartData();
                    ProgramStateWiseBudgetbar.labels = new List<string>();
                    ProgramStateWiseBudgetbar.datasets = new List<BarChartDataset>();
                    ProgramStateWiseBudgetbar.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(ProgramStateWiseBudgetbar);
                });


                for (int i = 0; i < ProgramManagerProgramStateWiseBudgetlineChart.Count; i++)
                {
                    var items = ProgramManagerProgramStateWiseBudgetlineChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].Key);

                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Funder Contribution";
                            obj.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "Community Contribution";
                            obj1.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.CommunityContribution));
                            a.Add(obj1);
                        }
                        else
                        {
                            BarChartDataset obj2 = new BarChartDataset();
                            obj2.data = new List<decimal>();
                            obj2.backgroundColor = "yellow";
                            obj2.borderColor = "pink";
                            obj2.label = "Other Contribution";
                            obj2.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.OtherContribution));
                            a.Add(obj2);
                        }
                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerProgramStateWiseBudgetBarChart = ProgramManagerBarChart;

                // ProgramManagerProgramYearwiseStateWiseBudgetBarChart.
                ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerProgramYearwiseStateWiseBudgetBarChart = programManagerDashboardData.ProgramManagerProgramYearwiseStateWiseBudgets.ToList().GroupBy(l => l.StateName).ToList();

                ProgramManagerProgramYearwiseStateWiseBudgetBarChart.ForEach(l =>
                {
                    BarChartData ProgramManagerProgramYearwiseBudgetBarChart = new BarChartData();
                    ProgramManagerProgramYearwiseBudgetBarChart.labels = new List<string>();
                    ProgramManagerProgramYearwiseBudgetBarChart.datasets = new List<BarChartDataset>();
                    ProgramManagerProgramYearwiseBudgetBarChart.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(ProgramManagerProgramYearwiseBudgetBarChart);
                });


                for (int i = 0; i < ProgramManagerProgramYearwiseStateWiseBudgetBarChart.Count; i++)
                {
                    var items = ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].Key);

                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Funder Contribution";
                            obj.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "Community Contribution";
                            obj1.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.CommunityContribution));
                            a.Add(obj1);
                        }
                        else
                        {
                            BarChartDataset obj2 = new BarChartDataset();
                            obj2.data = new List<decimal>();
                            obj2.backgroundColor = "yellow";
                            obj2.borderColor = "pink";
                            obj2.label = "Other Contribution";
                            obj2.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.OtherContribution));
                            a.Add(obj2);
                        }
                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerProgramYearwiseStateWiseBudgetBarChart = ProgramManagerBarChart;

                allChartData.ProgramManagerDashboardDatas = new List<ProgramManagerDashboardData>();
                allChartData.ProgramManagerDashboardDatas.Add(programManagerDashboardData);

                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<AllChartDto> ProgrammeMngrStateWiseBudget_G()
        {

            try
            {
                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);


                string finyear = "";
                DateTime dt = Convert.ToDateTime(DateTime.Now.ToString());
                int m = dt.Month;
                int y = dt.Year;
                if (m > 3)
                {
                    finyear = y.ToString() + "-" + Convert.ToString((y + 1));
                    //get last  two digits (eg: 10 from 2010);
                }
                else
                {
                    finyear = Convert.ToString((y - 1)) + "-" + y.ToString();
                }


                var programManagerProgramStateWiseBudgets = new List<budgetachievmentdto>();
                var Result = (from p in _programRepository.GetAll().Where(aa => aa.ManagerID == input)
                              join v in _villageClusterRepository.GetAll() on p.Id equals v.ProgramID
                              join s in _stateRepository.GetAll() on v.StateID equals s.Id
                              group v by new
                              {
                                  programId = p.Id,
                                  pName = p.Name,
                                  stateName = s.StateName
                              } into g

                              select new ProgramManagerStateWiseBudget
                              {
                                  FunderContribution = Convert.ToDecimal(_programCostEstimationRepository.GetAll().
                                                                 Where(xx => xx.ProgramID == g.Key.programId && xx.CostEstimationYear == finyear && xx.TotalUnits != 0)
                                                                .Sum(aa => aa.FunderContribution)),
                                  Expense = _ProgramExpenseRepository.GetAll()
                                                                .Where(xx => xx.ProgramID == g.Key.programId && xx.ExpensesYear == finyear && xx.Status != ProgramFunds.Status1.Reject)
                                                                .Sum(aa => aa.Amount),
                                  StateName = g.Key.stateName
                              }).ToList();


                var ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerProgramStateWiseBudgetlineChart = Result.ToList().GroupBy(l => l.StateName).ToList();

                ProgramManagerProgramStateWiseBudgetlineChart.ForEach(l =>
                {
                    BarChartData ProgramStateWiseBudgetbar = new BarChartData();
                    ProgramStateWiseBudgetbar.labels = new List<string>();
                    ProgramStateWiseBudgetbar.datasets = new List<BarChartDataset>();
                    ProgramStateWiseBudgetbar.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(ProgramStateWiseBudgetbar);
                });


                for (int i = 0; i < ProgramManagerProgramStateWiseBudgetlineChart.Count(); i++)
                {
                    var items = ProgramManagerProgramStateWiseBudgetlineChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].Key);

                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Budget";
                            obj.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "ActualPerformance";
                            obj1.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.Expense));
                            a.Add(obj1);
                        }

                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerProgramStateWiseBudgetBarChart = ProgramManagerBarChart;

                //}
                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<AllChartDto> RRCInchargeDashboard()

        {

            //var rrcdash = RRCInchargebudget();

            AllChartDto allChartData = new AllChartDto();
            List<PieChartData> piechart = new List<PieChartData>();

            try
            {
                int input = Convert.ToInt32(GetCurrentUser().Id);

                ProgramManagerDashboardData programManagerDashboardData = new ProgramManagerDashboardData();
                programManagerDashboardData.ProgramManagerStateWiseBudgets = new List<ProgramManagerStateWiseBudget>();
                programManagerDashboardData.ProgramManagerProgramStateWiseBudgets = new List<ProgramManagerProgramStateWiseBudget>();
                programManagerDashboardData.ProgramManagerProgramYearwiseStateWiseBudgets = new List<ProgramManagerProgramYearwiseStateWiseBudget>();
                programManagerDashboardData.ProgramManagerWiseProjectManagerCounts = new List<ProgramManagerWiseProjectManagerCount>();
                programManagerDashboardData.ProgramManagerImplemenationPlanCheckListWiseBudgets = new List<ProgramManagerImplemenationPlanCheckListWiseBudget>();
                programManagerDashboardData.StateWiseDemands = new List<StateWiseDemand>();
                programManagerDashboardData.managerwiseWiseDemands = new List<ManagerwiseWiseDemand>();
                programManagerDashboardData.DemandWiseProjects = new List<DemandWiseProject>();
                programManagerDashboardData.ProgramManagerAmountSanctions = new List<ProgramManagerAmountSanction>();
                programManagerDashboardData.ProgramManagerDashboardInfos = new List<ProgramManagerDashboardInfo>();
                programManagerDashboardData.ProgramManagerWiseTeamMemberInfos = new List<ProgramManagerWiseTeamMemberInfo>();
                programManagerDashboardData.ProgramManagerWiseProjectLists = new List<ProgramManagerWiseProjectList>();

                string value;
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();


                    //using (var cmd = new SqlCommand("", conn))
                    //{
                    //    // ProjectWiseInfo
                    //    var dataTable = new DataTable();
                    //    cmd.CommandTimeout = 0;
                    //    cmd.CommandText = "[dbo].[sp_ProgramManagerStateWiseBudget]";
                    //    cmd.CommandType = CommandType.StoredProcedure;
                    //    cmd.Parameters.Add(new SqlParameter("@ProgramManagerId", input));

                    //    SqlDataReader dr = cmd.ExecuteReader();
                    //    dataTable.Load(dr);
                    //    if (dataTable.Rows.Count != 0)
                    //    {
                    //        value = dataTable.Rows[0][0].ToString();
                    //        if (value != "")
                    //        {
                    //            programManagerDashboardData.dataTable = null;
                    //            programManagerDashboardData.dataTable = dataTable;
                    //            await AddDataInDataTable(programManagerDashboardData, 1);
                    //        }
                    //    }
                    //    conn.Close();
                    //}

                    //await ConnectionConnect(conn);
                    //using (var cmdOne = new SqlCommand("", conn))
                    //{

                    //    //ProjectWiseInfoPieChart
                    //    var dataTable = new DataTable();
                    //    cmdOne.CommandTimeout = 0;
                    //    cmdOne.CommandText = "[dbo].[sp_ProgramManagerProgramStateWiseBudget]";
                    //    cmdOne.CommandType = CommandType.StoredProcedure;
                    //    cmdOne.Parameters.Add(new SqlParameter("@ProgramManagerId", input));

                    //    SqlDataReader dr = cmdOne.ExecuteReader();
                    //    dataTable.Load(dr);
                    //    if (dataTable.Rows.Count != 0)
                    //    {
                    //        value = dataTable.Rows[0][0].ToString();

                    //        if (value != "")
                    //        {
                    //            programManagerDashboardData.dataTable = null;
                    //            programManagerDashboardData.dataTable = dataTable;
                    //            await AddDataInDataTable(programManagerDashboardData, 2);
                    //        }
                    //    }
                    //    conn.Close();
                    //}

                    await ConnectionConnect(conn);
                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_RRCProjectManagerCount]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@RRCInchargeId", input));

                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 4);
                            }
                        }
                        conn.Close();
                    }

                    await ConnectionConnect(conn);
                    using (var cmdFour = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdFour.CommandTimeout = 0;
                        cmdFour.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdFour.CommandType = CommandType.StoredProcedure;
                        cmdFour.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                        cmdFour.Parameters.Add(new SqlParameter("@type", 9));


                        SqlDataReader dr = cmdFour.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 15);
                            }
                        }
                        conn.Close();
                    }

                    await ConnectionConnect(conn);
                    using (var cmdSeven = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdSeven.CommandTimeout = 0;
                        cmdSeven.CommandText = "[dbo].[sp_RRCInchargeDashboardInfo]";
                        cmdSeven.CommandType = CommandType.StoredProcedure;
                        cmdSeven.Parameters.Add(new SqlParameter("@RRCInchargeId", input));

                        SqlDataReader dr = cmdSeven.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 9);
                            }
                        }
                        conn.Close();
                    }
                    await ConnectionConnect(conn);
                    using (var cmdEigth = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdEigth.CommandTimeout = 0;
                        cmdEigth.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdEigth.CommandType = CommandType.StoredProcedure;
                        cmdEigth.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                        cmdEigth.Parameters.Add(new SqlParameter("@type", 7));

                        SqlDataReader dr = cmdEigth.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 10);
                            }
                        }
                        conn.Close();
                    }
                    await ConnectionConnect(conn);
                    using (var cmdNine = new SqlCommand("", conn))
                    {

                        var dataTable = new DataTable();
                        cmdNine.CommandTimeout = 0;
                        cmdNine.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdNine.CommandType = CommandType.StoredProcedure;
                        cmdNine.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                        cmdNine.Parameters.Add(new SqlParameter("@type", 8));


                        SqlDataReader dr = cmdNine.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 11);
                            }
                        }
                        conn.Close();
                    }
                }

                // ProgramManagerStateWiseBudgetlineChart

                var Result = (from p in _programRepository.GetAll().Where(aa => aa.ManagerID == input)
                              join v in _villageClusterRepository.GetAll() on p.Id equals v.ProgramID
                              join s in _stateRepository.GetAll() on v.StateID equals s.Id
                              group v by new
                              {
                                  programId = p.Id,
                                  pName = p.Name,
                                  stateName = s.StateName
                              } into g

                              select new ProgramManagerStateWiseBudget
                              {
                                  FunderContribution = Convert.ToDecimal(_programCostEstimationRepository.GetAll().Where(xx => xx.ProgramID == g.Key.programId).Sum(aa => aa.FunderContribution)),
                                  Expense = _ProgramExpenseRepository.GetAll().Where(xx => xx.ProgramID == g.Key.programId).Sum(aa => aa.Amount),
                                  StateName = g.Key.stateName
                              }).ToList();








                List<BarChartData> ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerStateWiseBudgetlineChart = Result.ToList().GroupBy(l => l.StateName).ToList();
                ProgramManagerStateWiseBudgetlineChart.ForEach(l =>
                {
                    BarChartData stateWiseBudgetbar = new BarChartData();
                    stateWiseBudgetbar.labels = new List<string>();
                    //stateWiseBudgetbar.label = string;
                    stateWiseBudgetbar.datasets = new List<BarChartDataset>();
                    stateWiseBudgetbar.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(stateWiseBudgetbar);
                });

                for (int i = 0; i < ProgramManagerStateWiseBudgetlineChart.Count; i++)
                {
                    var items = ProgramManagerStateWiseBudgetlineChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerStateWiseBudgetlineChart[i].Key);
                    ProgramManagerBarChart[i].label = ProgramManagerStateWiseBudgetlineChart[i].Key;


                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Budget";
                            obj.data.Add(ProgramManagerStateWiseBudgetlineChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "ActualPerformance";
                            obj1.data.Add(ProgramManagerStateWiseBudgetlineChart[i].ToList().Sum(c => c.Expense));
                            a.Add(obj1);
                        }
                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerBarChart = ProgramManagerBarChart;

                // ProgramManagerProgramStateWiseBudgetLineChart.
                ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerProgramStateWiseBudgetlineChart = programManagerDashboardData.ProgramManagerProgramStateWiseBudgets.ToList().GroupBy(l => l.ProjectName).ToList();

                ProgramManagerProgramStateWiseBudgetlineChart.ForEach(l =>
                {
                    BarChartData ProgramStateWiseBudgetbar = new BarChartData();
                    ProgramStateWiseBudgetbar.labels = new List<string>();
                    ProgramStateWiseBudgetbar.datasets = new List<BarChartDataset>();
                    ProgramStateWiseBudgetbar.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(ProgramStateWiseBudgetbar);
                });


                for (int i = 0; i < ProgramManagerProgramStateWiseBudgetlineChart.Count; i++)
                {
                    var items = ProgramManagerProgramStateWiseBudgetlineChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].Key);

                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Funder Contribution";
                            obj.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "Community Contribution";
                            obj1.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.CommunityContribution));
                            a.Add(obj1);
                        }
                        else
                        {
                            BarChartDataset obj2 = new BarChartDataset();
                            obj2.data = new List<decimal>();
                            obj2.backgroundColor = "yellow";
                            obj2.borderColor = "pink";
                            obj2.label = "Other Contribution";
                            obj2.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.OtherContribution));
                            a.Add(obj2);
                        }
                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerProgramStateWiseBudgetBarChart = ProgramManagerBarChart;

                // ProgramManagerProgramYearwiseStateWiseBudgetBarChart.
                ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerProgramYearwiseStateWiseBudgetBarChart = programManagerDashboardData.ProgramManagerProgramYearwiseStateWiseBudgets.ToList().GroupBy(l => l.StateName).ToList();

                ProgramManagerProgramYearwiseStateWiseBudgetBarChart.ForEach(l =>
                {
                    BarChartData ProgramManagerProgramYearwiseBudgetBarChart = new BarChartData();
                    ProgramManagerProgramYearwiseBudgetBarChart.labels = new List<string>();
                    ProgramManagerProgramYearwiseBudgetBarChart.datasets = new List<BarChartDataset>();
                    ProgramManagerProgramYearwiseBudgetBarChart.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(ProgramManagerProgramYearwiseBudgetBarChart);
                });


                for (int i = 0; i < ProgramManagerProgramYearwiseStateWiseBudgetBarChart.Count; i++)
                {
                    var items = ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].Key);

                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Funder Contribution";
                            obj.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "Community Contribution";
                            obj1.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.CommunityContribution));
                            a.Add(obj1);
                        }
                        else
                        {
                            BarChartDataset obj2 = new BarChartDataset();
                            obj2.data = new List<decimal>();
                            obj2.backgroundColor = "yellow";
                            obj2.borderColor = "pink";
                            obj2.label = "Other Contribution";
                            obj2.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.OtherContribution));
                            a.Add(obj2);
                        }
                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerProgramYearwiseStateWiseBudgetBarChart = ProgramManagerBarChart;

                allChartData.ProgramManagerDashboardDatas = new List<ProgramManagerDashboardData>();
                allChartData.ProgramManagerDashboardDatas.Add(programManagerDashboardData);

                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<AllChartDto> ProjectManagerDashboard()

        {

            //var rrcdash = RRCInchargebudget();

            AllChartDto allChartData = new AllChartDto();
            List<PieChartData> piechart = new List<PieChartData>();

            try
            {
                int input = Convert.ToInt32(GetCurrentUser().Id);

                ProgramManagerDashboardData programManagerDashboardData = new ProgramManagerDashboardData();
                programManagerDashboardData.ProgramManagerStateWiseBudgets = new List<ProgramManagerStateWiseBudget>();
                programManagerDashboardData.ProgramManagerProgramStateWiseBudgets = new List<ProgramManagerProgramStateWiseBudget>();
                programManagerDashboardData.ProgramManagerProgramYearwiseStateWiseBudgets = new List<ProgramManagerProgramYearwiseStateWiseBudget>();
                programManagerDashboardData.ProgramManagerWiseProjectManagerCounts = new List<ProgramManagerWiseProjectManagerCount>();
                programManagerDashboardData.ProgramManagerImplemenationPlanCheckListWiseBudgets = new List<ProgramManagerImplemenationPlanCheckListWiseBudget>();
                programManagerDashboardData.StateWiseDemands = new List<StateWiseDemand>();
                programManagerDashboardData.managerwiseWiseDemands = new List<ManagerwiseWiseDemand>();
                programManagerDashboardData.DemandWiseProjects = new List<DemandWiseProject>();
                programManagerDashboardData.ProgramManagerAmountSanctions = new List<ProgramManagerAmountSanction>();
                programManagerDashboardData.ProgramManagerDashboardInfos = new List<ProgramManagerDashboardInfo>();
                programManagerDashboardData.ProgramManagerWiseTeamMemberInfos = new List<ProgramManagerWiseTeamMemberInfo>();
                programManagerDashboardData.ProgramManagerWiseProjectLists = new List<ProgramManagerWiseProjectList>();

                string value;
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    var da = new SqlDataAdapter();
                    var ds = new DataSet();


                    //using (var cmd = new SqlCommand("", conn))
                    //{
                    //    // ProjectWiseInfo
                    //    var dataTable = new DataTable();
                    //    cmd.CommandTimeout = 0;
                    //    cmd.CommandText = "[dbo].[sp_ProgramManagerStateWiseBudget]";
                    //    cmd.CommandType = CommandType.StoredProcedure;
                    //    cmd.Parameters.Add(new SqlParameter("@ProgramManagerId", input));

                    //    SqlDataReader dr = cmd.ExecuteReader();
                    //    dataTable.Load(dr);
                    //    if (dataTable.Rows.Count != 0)
                    //    {
                    //        value = dataTable.Rows[0][0].ToString();
                    //        if (value != "")
                    //        {
                    //            programManagerDashboardData.dataTable = null;
                    //            programManagerDashboardData.dataTable = dataTable;
                    //            await AddDataInDataTable(programManagerDashboardData, 1);
                    //        }
                    //    }
                    //    conn.Close();
                    //}

                    //await ConnectionConnect(conn);
                    //using (var cmdOne = new SqlCommand("", conn))
                    //{

                    //    //ProjectWiseInfoPieChart
                    //    var dataTable = new DataTable();
                    //    cmdOne.CommandTimeout = 0;
                    //    cmdOne.CommandText = "[dbo].[sp_ProgramManagerProgramStateWiseBudget]";
                    //    cmdOne.CommandType = CommandType.StoredProcedure;
                    //    cmdOne.Parameters.Add(new SqlParameter("@ProgramManagerId", input));

                    //    SqlDataReader dr = cmdOne.ExecuteReader();
                    //    dataTable.Load(dr);
                    //    if (dataTable.Rows.Count != 0)
                    //    {
                    //        value = dataTable.Rows[0][0].ToString();

                    //        if (value != "")
                    //        {
                    //            programManagerDashboardData.dataTable = null;
                    //            programManagerDashboardData.dataTable = dataTable;
                    //            await AddDataInDataTable(programManagerDashboardData, 2);
                    //        }
                    //    }
                    //    conn.Close();
                    //}

                    await ConnectionConnect(conn);
                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_RRCProjectManagerCount]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@RRCInchargeId", input));

                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 4);
                            }
                        }
                        conn.Close();
                    }

                    await ConnectionConnect(conn);
                    using (var cmdFour = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdFour.CommandTimeout = 0;
                        cmdFour.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdFour.CommandType = CommandType.StoredProcedure;
                        cmdFour.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                        cmdFour.Parameters.Add(new SqlParameter("@type", 9));


                        SqlDataReader dr = cmdFour.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 15);
                            }
                        }
                        conn.Close();
                    }

                    await ConnectionConnect(conn);
                    using (var cmdSeven = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdSeven.CommandTimeout = 0;
                        cmdSeven.CommandText = "[dbo].[sp_ProjectManagerDashboardInfo]";
                        cmdSeven.CommandType = CommandType.StoredProcedure;
                        cmdSeven.Parameters.Add(new SqlParameter("@RRCInchargeId", input));

                        SqlDataReader dr = cmdSeven.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 9);
                            }
                        }
                        conn.Close();
                    }
                    await ConnectionConnect(conn);
                    using (var cmdEigth = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdEigth.CommandTimeout = 0;
                        cmdEigth.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdEigth.CommandType = CommandType.StoredProcedure;
                        cmdEigth.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                        cmdEigth.Parameters.Add(new SqlParameter("@type", 16));

                        SqlDataReader dr = cmdEigth.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 10);
                            }
                        }
                        conn.Close();
                    }
                    await ConnectionConnect(conn);
                    using (var cmdNine = new SqlCommand("", conn))
                    {

                        var dataTable = new DataTable();
                        cmdNine.CommandTimeout = 0;
                        cmdNine.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdNine.CommandType = CommandType.StoredProcedure;
                        cmdNine.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                        cmdNine.Parameters.Add(new SqlParameter("@type", 8));


                        SqlDataReader dr = cmdNine.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                programManagerDashboardData.dataTable = null;
                                programManagerDashboardData.dataTable = dataTable;
                                await AddDataInDataTable(programManagerDashboardData, 11);
                            }
                        }
                        conn.Close();
                    }
                }

                // ProgramManagerStateWiseBudgetlineChart

                var Result = (from p in _programRepository.GetAll().Where(aa => aa.ManagerID == input)
                              join v in _villageClusterRepository.GetAll() on p.Id equals v.ProgramID
                              join s in _stateRepository.GetAll() on v.StateID equals s.Id
                              group v by new
                              {
                                  programId = p.Id,
                                  pName = p.Name,
                                  stateName = s.StateName
                              } into g

                              select new ProgramManagerStateWiseBudget
                              {
                                  FunderContribution = Convert.ToDecimal(_programCostEstimationRepository.GetAll().Where(xx => xx.ProgramID == g.Key.programId).Sum(aa => aa.FunderContribution)),
                                  Expense = _ProgramExpenseRepository.GetAll().Where(xx => xx.ProgramID == g.Key.programId).Sum(aa => aa.Amount),
                                  StateName = g.Key.stateName
                              }).ToList();








                List<BarChartData> ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerStateWiseBudgetlineChart = Result.ToList().GroupBy(l => l.StateName).ToList();
                ProgramManagerStateWiseBudgetlineChart.ForEach(l =>
                {
                    BarChartData stateWiseBudgetbar = new BarChartData();
                    stateWiseBudgetbar.labels = new List<string>();
                    //stateWiseBudgetbar.label = string;
                    stateWiseBudgetbar.datasets = new List<BarChartDataset>();
                    stateWiseBudgetbar.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(stateWiseBudgetbar);
                });

                for (int i = 0; i < ProgramManagerStateWiseBudgetlineChart.Count; i++)
                {
                    var items = ProgramManagerStateWiseBudgetlineChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerStateWiseBudgetlineChart[i].Key);
                    ProgramManagerBarChart[i].label = ProgramManagerStateWiseBudgetlineChart[i].Key;


                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Budget";
                            obj.data.Add(ProgramManagerStateWiseBudgetlineChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "ActualPerformance";
                            obj1.data.Add(ProgramManagerStateWiseBudgetlineChart[i].ToList().Sum(c => c.Expense));
                            a.Add(obj1);
                        }
                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerBarChart = ProgramManagerBarChart;

                // ProgramManagerProgramStateWiseBudgetLineChart.
                ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerProgramStateWiseBudgetlineChart = programManagerDashboardData.ProgramManagerProgramStateWiseBudgets.ToList().GroupBy(l => l.ProjectName).ToList();

                ProgramManagerProgramStateWiseBudgetlineChart.ForEach(l =>
                {
                    BarChartData ProgramStateWiseBudgetbar = new BarChartData();
                    ProgramStateWiseBudgetbar.labels = new List<string>();
                    ProgramStateWiseBudgetbar.datasets = new List<BarChartDataset>();
                    ProgramStateWiseBudgetbar.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(ProgramStateWiseBudgetbar);
                });


                for (int i = 0; i < ProgramManagerProgramStateWiseBudgetlineChart.Count; i++)
                {
                    var items = ProgramManagerProgramStateWiseBudgetlineChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].Key);

                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Funder Contribution";
                            obj.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "Community Contribution";
                            obj1.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.CommunityContribution));
                            a.Add(obj1);
                        }
                        else
                        {
                            BarChartDataset obj2 = new BarChartDataset();
                            obj2.data = new List<decimal>();
                            obj2.backgroundColor = "yellow";
                            obj2.borderColor = "pink";
                            obj2.label = "Other Contribution";
                            obj2.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.OtherContribution));
                            a.Add(obj2);
                        }
                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerProgramStateWiseBudgetBarChart = ProgramManagerBarChart;

                // ProgramManagerProgramYearwiseStateWiseBudgetBarChart.
                ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerProgramYearwiseStateWiseBudgetBarChart = programManagerDashboardData.ProgramManagerProgramYearwiseStateWiseBudgets.ToList().GroupBy(l => l.StateName).ToList();

                ProgramManagerProgramYearwiseStateWiseBudgetBarChart.ForEach(l =>
                {
                    BarChartData ProgramManagerProgramYearwiseBudgetBarChart = new BarChartData();
                    ProgramManagerProgramYearwiseBudgetBarChart.labels = new List<string>();
                    ProgramManagerProgramYearwiseBudgetBarChart.datasets = new List<BarChartDataset>();
                    ProgramManagerProgramYearwiseBudgetBarChart.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(ProgramManagerProgramYearwiseBudgetBarChart);
                });


                for (int i = 0; i < ProgramManagerProgramYearwiseStateWiseBudgetBarChart.Count; i++)
                {
                    var items = ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].Key);

                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Funder Contribution";
                            obj.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "Community Contribution";
                            obj1.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.CommunityContribution));
                            a.Add(obj1);
                        }
                        else
                        {
                            BarChartDataset obj2 = new BarChartDataset();
                            obj2.data = new List<decimal>();
                            obj2.backgroundColor = "yellow";
                            obj2.borderColor = "pink";
                            obj2.label = "Other Contribution";
                            obj2.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.OtherContribution));
                            a.Add(obj2);
                        }
                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerProgramYearwiseStateWiseBudgetBarChart = ProgramManagerBarChart;

                allChartData.ProgramManagerDashboardDatas = new List<ProgramManagerDashboardData>();
                allChartData.ProgramManagerDashboardDatas.Add(programManagerDashboardData);

                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<DemandWiseProject>> StateWiseDemandInfo(int stateId)
        {
            try
            {
                string value;
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var demandWiseProject = new List<DemandWiseProject>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdFive = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdFive.CommandTimeout = 0;
                        cmdFive.CommandText = "[dbo].[sp_DemandWiseProject]";
                        cmdFive.CommandType = CommandType.StoredProcedure;
                        cmdFive.Parameters.Add(new SqlParameter("@ProgramManagerId", input));
                        cmdFive.Parameters.Add(new SqlParameter("@StateId", stateId));

                        SqlDataReader dr = cmdFive.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    demandWiseProject.Add(new DemandWiseProject
                                    {
                                        StateName = (row["StateName"] == DBNull.Value) ? "" : Convert.ToString(row["StateName"]),
                                        ProjectName = (row["ProjectName"] == DBNull.Value) ? "" : Convert.ToString(row["ProjectName"]),
                                        Demand = (row["Demand"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Demand"]),
                                        DemandRemaining = (row["DemandRemaining"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["DemandRemaining"]),
                                        ProgramId = (row["ProgramId"] == DBNull.Value) ? 00 : Convert.ToInt32(row["ProgramId"]),
                                        StateId = (row["StateId"] == DBNull.Value) ? 00 : Convert.ToInt32(row["StateId"]),
                                    });
                                }
                            }
                        }
                        conn.Close();
                    }

                }
                return demandWiseProject;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<RRCManagerAmountSanction>> ProgramWiseDemandInfo(int programId)
        {
            try
            {
                string value;
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var rRCManagerAmountSanction = new List<RRCManagerAmountSanction>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdFive = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdFive.CommandTimeout = 0;
                        cmdFive.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdFive.CommandType = CommandType.StoredProcedure;
                        cmdFive.Parameters.Add(new SqlParameter("@ProgramId", programId));
                        cmdFive.Parameters.Add(new SqlParameter("@type", 10));


                        SqlDataReader dr = cmdFive.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    rRCManagerAmountSanction.Add(new RRCManagerAmountSanction
                                    {
                                        ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),
                                        Date = (row["Date"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["Date"]),

                                        AmountSanction = (row["Amountsanction"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Amountsanction"]),
                                        ProgramId = (row["PID"] == DBNull.Value) ? 00 : Convert.ToInt32(row["PID"]),
                                    });
                                }
                            }
                        }
                        conn.Close();
                    }

                }
                return rRCManagerAmountSanction;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<DemandRemaningAmount>> ProgramWiseDemandRemaningAmountInfo(int programId)
        {
            try
            {
                string value;
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var demandRemaningAmount = new List<DemandRemaningAmount>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdFive = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdFive.CommandTimeout = 0;
                        cmdFive.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdFive.CommandType = CommandType.StoredProcedure;
                        cmdFive.Parameters.Add(new SqlParameter("@ProgramId", programId));
                        cmdFive.Parameters.Add(new SqlParameter("@type", 11));


                        SqlDataReader dr = cmdFive.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    demandRemaningAmount.Add(new DemandRemaningAmount
                                    {
                                        ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),

                                        DemandRemainingAmount = (row["DemandRemaining"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["DemandRemaining"]),
                                    });
                                }
                            }
                        }
                        conn.Close();
                    }

                }
                return demandRemaningAmount;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<ProgramManagerAmountSanction>> ProjectWiseAmountSanction(int programId)
        {
            try
            {
                string value;
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var ProgramManagerAmountSanctions = new List<ProgramManagerAmountSanction>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdSix = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdSix.CommandTimeout = 0;
                        cmdSix.CommandText = "[dbo].[sp_AmountSanction]";
                        cmdSix.CommandType = CommandType.StoredProcedure;
                        cmdSix.Parameters.Add(new SqlParameter("@ProgramManagerId", input));
                        cmdSix.Parameters.Add(new SqlParameter("@ProgramId", programId));

                        SqlDataReader dr = cmdSix.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    ProgramManagerAmountSanctions.Add(new ProgramManagerAmountSanction
                                    {
                                        StateName = (row["StateName"] == DBNull.Value) ? "" : Convert.ToString(row["StateName"]),
                                        ProgramId = (row["ProgramId"] == DBNull.Value) ? 00 : Convert.ToInt32(row["ProgramId"]),
                                        ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),
                                        Date = (row["Date"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["Date"]),
                                        AmountSanction = (row["AmountSanction"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["AmountSanction"]),
                                    });
                                }

                            }
                        }
                        conn.Close();
                    }
                }
                return ProgramManagerAmountSanctions;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<AllChartDto> ProgramManagerProgramStateWiseBudget(string stateName)
        {
            try
            {
                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramStateWiseBudgets = new List<ProgramManagerProgramStateWiseBudget>();

                string finyear = "";
                DateTime dt = Convert.ToDateTime(DateTime.Now.ToString());
                int m = dt.Month;
                int y = dt.Year;
                if (m > 3)
                {
                    finyear = y.ToString() + "-" + Convert.ToString((y + 1));
                    //get last  two digits (eg: 10 from 2010);
                }
                else
                {
                    finyear = Convert.ToString((y - 1)) + "-" + y.ToString();
                }


                var Result = (from p in _programRepository.GetAll().Where(aa => aa.ManagerID == input)
                              join v in _villageClusterRepository.GetAll() on p.Id equals v.ProgramID
                              join s in _stateRepository.GetAll().Where(bb => bb.StateName == stateName) on v.StateID equals s.Id
                              group v by new
                              {
                                  programId = p.Id,
                                  pName = p.Name,
                                  stateName = s.StateName,
                                  Program = p.Name
                              } into g

                              select new ProgramManagerProgramStateWiseBudget
                              {
                                  FunderContribution = Convert.ToDecimal(_programCostEstimationRepository.GetAll()
                                  .Where(xx => xx.ProgramID == g.Key.programId && xx.TotalUnits != 0 && xx.CostEstimationYear == finyear)
                                  .Sum(aa => aa.FunderContribution)),
                                  Expense = _ProgramExpenseRepository.GetAll()
                                         .Where(xx => xx.ProgramID == g.Key.programId && xx.ExpensesYear == finyear && xx.Status != ProgramFunds.Status1.Reject).Sum(aa => aa.Amount),
                                  StateName = g.Key.stateName,
                                  ProjectName = g.Key.pName,
                                  Projectid = g.Key.programId.ToString()
                              }).ToList();


                var ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerProgramStateWiseBudgetlineChart = Result.ToList().GroupBy(l => l.ProjectName).ToList();



                ProgramManagerProgramStateWiseBudgetlineChart.ForEach(l =>
                {
                    BarChartData ProgramStateWiseBudgetbar = new BarChartData();
                    ProgramStateWiseBudgetbar.labels = new List<string>();
                    ProgramStateWiseBudgetbar.datasets = new List<BarChartDataset>();
                    ProgramStateWiseBudgetbar.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(ProgramStateWiseBudgetbar);
                });


                for (int i = 0; i < ProgramManagerProgramStateWiseBudgetlineChart.Count; i++)
                {
                    var items = ProgramManagerProgramStateWiseBudgetlineChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].Key);

                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Budget";
                            obj.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "ActualPerformance";
                            obj1.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.Expense));
                            a.Add(obj1);
                        }

                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerProgramStateWiseBudgetBarChart = ProgramManagerBarChart;


                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<AllChartDto> ProjectWiseStateWiseBudget(string projectName)
        {
            try
            {
                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramYearwiseStateWiseBudgets = new List<ProgramManagerProgramYearwiseStateWiseBudget>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdTwo = new SqlCommand("", conn))
                    {
                        //ProjectWiseInfoLineChart
                        var dataTable = new DataTable();
                        cmdTwo.CommandTimeout = 0;
                        cmdTwo.CommandText = "[dbo].[sp_ProgramManagerProgramYearwiseStateWiseBudget]";
                        cmdTwo.CommandType = CommandType.StoredProcedure;
                        cmdTwo.Parameters.Add(new SqlParameter("@ProgramManagerId", input));
                        cmdTwo.Parameters.Add(new SqlParameter("@ProjectName", projectName));

                        SqlDataReader dr = cmdTwo.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerProgramYearwiseStateWiseBudgets.Add(new ProgramManagerProgramYearwiseStateWiseBudget
                                    {
                                        StateName = (row["StateName"] == DBNull.Value) ? "" : Convert.ToString(row["StateName"]),
                                        CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                                        ProjectName = (row["ProjectName"] == DBNull.Value) ? "" : Convert.ToString(row["ProjectName"]),
                                        FunderContribution = (row["FunderContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["FunderContribution"]),
                                        CommunityContribution = (row["CommunityContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["CommunityContribution"]),
                                        OtherContribution = (row["OtherContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["OtherContribution"]),

                                    });
                                }

                            }
                        }
                        conn.Close();
                    }

                    //
                    var programManagerBarChart = new List<BarChartData>();
                    var ProgramManagerProgramYearwiseStateWiseBudgetBarChart = programManagerProgramYearwiseStateWiseBudgets.ToList().GroupBy(l => l.CostEstimationYear).ToList();

                    ProgramManagerProgramYearwiseStateWiseBudgetBarChart.ForEach(l =>
                    {
                        BarChartData ProgramManagerProgramYearwiseBudgetBarChart = new BarChartData();
                        ProgramManagerProgramYearwiseBudgetBarChart.labels = new List<string>();
                        ProgramManagerProgramYearwiseBudgetBarChart.datasets = new List<BarChartDataset>();
                        ProgramManagerProgramYearwiseBudgetBarChart.datasets.ForEach(x =>
                        {
                            x.data = new List<decimal>();
                        });
                        programManagerBarChart.Add(ProgramManagerProgramYearwiseBudgetBarChart);
                    });


                    for (int i = 0; i < ProgramManagerProgramYearwiseStateWiseBudgetBarChart.Count; i++)
                    {
                        var items = ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList();
                        programManagerBarChart[i].labels.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].Key);

                        List<BarChartDataset> a = new List<BarChartDataset>();

                        for (int j = 0; j < 3; j++)
                        {

                            if (j == 0)
                            {
                                BarChartDataset obj = new BarChartDataset();
                                obj.data = new List<decimal>();
                                obj.backgroundColor = "red";
                                obj.borderColor = "blue";
                                obj.label = "FunderContribution";
                                if (ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.FunderContribution) > 0)
                                {
                                    obj.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.FunderContribution));
                                }
                                a.Add(obj);

                            }
                            else if (j == 1)
                            {
                                BarChartDataset obj1 = new BarChartDataset();
                                obj1.data = new List<decimal>();
                                obj1.backgroundColor = "green";
                                obj1.borderColor = "pink";
                                obj1.label = "CommunityContribution";
                                if (ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.CommunityContribution) > 0)
                                {
                                    obj1.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.CommunityContribution));
                                }
                                a.Add(obj1);
                            }
                            else
                            {
                                BarChartDataset obj2 = new BarChartDataset();
                                obj2.data = new List<decimal>();
                                obj2.backgroundColor = "yellow";
                                obj2.borderColor = "pink";
                                obj2.label = "OtherContribution";
                                if (ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.OtherContribution) > 0)
                                {
                                    obj2.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.OtherContribution));
                                }
                                a.Add(obj2);
                            }
                        }

                        programManagerBarChart[i].datasets = a;
                    }

                    allChartData.ProgramManagerProgramYearwiseStateWiseBudgetBarChart = programManagerBarChart;
                }
                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<ProgramManagerImplemenationPlanCheckListWiseBudget>> YearWiseAssignedReport(string prgramName, string projectYear)
        {
            try
            {
                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerImplemenationPlanCheckListWiseBudgets = new List<ProgramManagerImplemenationPlanCheckListWiseBudget>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[sp_ProgramManagerImplemenationPlanCheckListWiseBudget]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@ProgramManagerId", input));
                        cmdThree.Parameters.Add(new SqlParameter("@CostEstimationYear", projectYear));
                        cmdThree.Parameters.Add(new SqlParameter("@ProgramName", prgramName));


                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerImplemenationPlanCheckListWiseBudgets.Add(new ProgramManagerImplemenationPlanCheckListWiseBudget
                                    {
                                        CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                                        Activity = (row["Activity"] == DBNull.Value) ? "" : Convert.ToString(row["Activity"]),
                                        SubActivity = (row["SubActivity"] == DBNull.Value) ? "" : Convert.ToString(row["SubActivity"]),
                                        AssignedTo = (row["AssignedTo"] == DBNull.Value) ? "" : Convert.ToString(row["AssignedTo"]),
                                        CheckListStartDate = (row["CheckListStartDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["CheckListStartDate"]),
                                        CheklistEndDate = (row["CheklistEndDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["CheklistEndDate"]),
                                        Status = (row["Status"] == DBNull.Value) ? "" : Convert.ToString(row["Status"]),
                                    });
                                }

                            }
                        }
                        conn.Close();
                    }
                }
                return programManagerImplemenationPlanCheckListWiseBudgets;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<AllChartDto> RRCInchargebudget()
        {
            try
            {

                string finyear = "";
                DateTime dt = Convert.ToDateTime(DateTime.Now.ToString());
                int m = dt.Month;
                int y = dt.Year;
                if (m > 3)
                {
                    finyear = y.ToString() + "-" + Convert.ToString((y + 1));
                    //get last  two digits (eg: 10 from 2010);
                }
                else
                {
                    finyear = Convert.ToString((y - 1)) + "-" + y.ToString();
                }

                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);

                //var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramStateWiseBudgets = new List<budgetachievmentdto>();
                var Result = (from ra in _RequestAAplasRepository
                                   .GetAll()
                                   .Where(aa => aa.ManagerID == input)
                              join p in _programRepository
                                   .GetAll() on ra.ProgramID equals p.Id
                              group ra by new
                              {
                                  raa = ra.ProgramID,
                                  programId = p.Id,
                                  pName = p.Name,
                                  mngId = ra.ManagerID
                              } into g

                              select new budgetachievmentdto
                              {
                                  FunderContribution = Convert.ToDecimal(_RequestAAplasRepository
                                                      .GetAll().Where(xx => xx.ProgramID == g.Key.raa &&
                                                      xx.CostEstimationYear == finyear && xx.ManagerID == g.Key.mngId)
                                                      .Sum(aa => aa.FunderContribution)),
                                  ExpenseAmount = _ProgramExpenseRepository.GetAll()
                                                  .Where(xx => xx.ProgramID == g.Key.raa &&
                                                  xx.Status != ProgramFunds.Status1.Reject &&
                                                  xx.ExpensesYear == finyear && xx.ManagerID == g.Key.mngId).Sum(aa => aa.Amount),
                                  ProgramName = g.Key.pName,
                              }).ToList();

                // using (SqlConnection conn = new SqlConnection(connectionString))
                //{
                // await ConnectionConnect(conn);
                //using (var cmdOne = new SqlCommand("", conn))
                //{

                //    //ProjectWiseInfoPieChart
                //    var dataTable = new DataTable();
                //    cmdOne.CommandTimeout = 0;
                //    cmdOne.CommandText = "[dbo].[RRCInchargeDashborad]";
                //    cmdOne.CommandType = CommandType.StoredProcedure;
                //    cmdOne.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                //    cmdOne.Parameters.Add(new SqlParameter("@type", 1));



                //    SqlDataReader dr = cmdOne.ExecuteReader();
                //    dataTable.Load(dr);
                //    if (dataTable.Rows.Count != 0)
                //    {
                //        value = dataTable.Rows[0][0].ToString();

                //        if (value != "")
                //        {
                //            foreach (DataRow row in dataTable.Rows)
                //            {
                //                programManagerProgramStateWiseBudgets.Add(new budgetachievmentdto
                //                {
                //                    ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),

                //                    FunderContribution = (row["FunderContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["FunderContribution"]),
                //                    //ExpenseAmount = (row["Expense"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Expense"]),
                //                });
                //            }

                //        }
                //    }
                //    conn.Close();
                //}
                //

                var ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerProgramStateWiseBudgetlineChart = Result.ToList().GroupBy(l => l.ProgramName).ToList();

                ProgramManagerProgramStateWiseBudgetlineChart.ForEach(l =>
                {
                    BarChartData ProgramStateWiseBudgetbar = new BarChartData();
                    ProgramStateWiseBudgetbar.labels = new List<string>();
                    ProgramStateWiseBudgetbar.datasets = new List<BarChartDataset>();
                    ProgramStateWiseBudgetbar.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(ProgramStateWiseBudgetbar);
                });


                for (int i = 0; i < ProgramManagerProgramStateWiseBudgetlineChart.Count(); i++)
                {
                    var items = ProgramManagerProgramStateWiseBudgetlineChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].Key);

                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Budget";
                            obj.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "ActualPerformance";
                            obj1.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.ExpenseAmount));
                            a.Add(obj1);
                        }

                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerProgramStateWiseBudgetBarChart = ProgramManagerBarChart;

                //}
                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<AllChartDto> RRCInchargebudgetOne()
        {
            try
            {

                string finyear = "";
                DateTime dt = Convert.ToDateTime(DateTime.Now.ToString());
                int m = dt.Month;
                int y = dt.Year;
                if (m > 3)
                {
                    finyear = y.ToString() + "-" + Convert.ToString((y + 1));
                    //get last  two digits (eg: 10 from 2010);
                }
                else
                {
                    finyear = Convert.ToString((y - 1)) + "-" + y.ToString();
                }

                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);

                //var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramStateWiseBudgets = new List<budgetachievmentdto>();
                var Result = (from ra in _RequestAAplasRepository
                                   .GetAll()
                                   .Where(aa => aa.ManagerID == input)
                              join p in _programRepository
                                   .GetAll() on ra.ProgramID equals p.Id
                              group ra by new
                              {
                                  raa = ra.ProgramID,
                                  programId = p.Id,
                                  pName = p.Name,
                                  mngId = ra.ManagerID
                              } into g

                              select new budgetachievmentdto
                              {
                                  FunderContribution = Convert.ToDecimal(_RequestAAplasRepository
                                                      .GetAll().Where(xx => xx.ProgramID == g.Key.raa &&
                                                      xx.CostEstimationYear == finyear && xx.ManagerID == g.Key.mngId)
                                                      .Sum(aa => aa.FunderContribution)),
                                  ExpenseAmount = _ProgramExpenseRepository.GetAll()
                                                  .Where(xx => xx.ProgramID == g.Key.raa &&
                                                  xx.Status != ProgramFunds.Status1.Reject &&
                                                  xx.ExpensesYear == finyear && xx.ManagerID == g.Key.mngId).Sum(aa => aa.Amount),
                                  ProgramName = g.Key.pName,
                              }).ToList();

                var ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerProgramStateWiseBudgetlineChart = Result.ToList().GroupBy(l => l.ProgramName).ToList();

                ProgramManagerProgramStateWiseBudgetlineChart.ForEach(l =>
                {
                    BarChartData ProgramStateWiseBudgetbar = new BarChartData();
                    ProgramStateWiseBudgetbar.labels = new List<string>();
                    ProgramStateWiseBudgetbar.datasets = new List<BarChartDataset>();
                    ProgramStateWiseBudgetbar.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(ProgramStateWiseBudgetbar);
                });


                for (int i = 0; i < ProgramManagerProgramStateWiseBudgetlineChart.Count(); i++)
                {
                    var items = ProgramManagerProgramStateWiseBudgetlineChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].Key);

                    List<BarChartDataset> a = new List<BarChartDataset>();

                    for (int j = 0; j < 3; j++)
                    {

                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Budget";
                            obj.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);

                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "ActualPerformance";
                            obj1.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.ExpenseAmount));
                            a.Add(obj1);
                        }

                    }

                    ProgramManagerBarChart[i].datasets = a;
                }

                allChartData.ProgramManagerProgramStateWiseBudgetBarChart = ProgramManagerBarChart;

                //}
                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<AllChartDto> RRCInchargePhysVsActperformance()
        {
            try
            {



                string finyear = "";
                DateTime dt = Convert.ToDateTime(DateTime.Now.ToString());
                int m = dt.Month;
                int y = dt.Year;
                if (m > 3)
                {
                    finyear = y.ToString() + "-" + Convert.ToString((y + 1));
                    //get last  two digits (eg: 10 from 2010);
                }
                else
                {
                    finyear = Convert.ToString((y - 1)) + "-" + y.ToString();
                }



                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);

                //var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramStateWiseBudgets = new List<budgetachievmentdto>();
                var Result = (from ra in _villageClusterRepository.GetAll().Where(aa => aa.UserId == input)



                              join p in _programRepository.GetAll() on ra.ProgramID equals p.Id
                              group ra by new
                              {
                                  raa = ra.ProgramID,
                                  programId = p.Id,
                                  pName = p.Name
                              } into g


                              select new budgetachievmentdto
                              {
                                  FunderContribution = Convert.ToDecimal(_programCostEstimationRepository.GetAll().Where(xx => xx.ProgramID == g.Key.raa && xx.CostEstimationYear == finyear && xx.TotalUnits != 0).Sum(aa => aa.FunderContribution)),
                                  ExpenseAmount = _ProgramExpenseRepository.GetAll().Where(xx => xx.ProgramID == g.Key.raa && xx.Status != ProgramFunds.Status1.Reject && xx.ExpensesYear == finyear).Sum(aa => aa.Amount),
                                  ProgramName = g.Key.pName,
                              }).ToList();



                // using (SqlConnection conn = new SqlConnection(connectionString))
                //{
                // await ConnectionConnect(conn);
                //using (var cmdOne = new SqlCommand("", conn))
                //{



                //    //ProjectWiseInfoPieChart
                //    var dataTable = new DataTable();
                //    cmdOne.CommandTimeout = 0;
                //    cmdOne.CommandText = "[dbo].[RRCInchargeDashborad]";
                //    cmdOne.CommandType = CommandType.StoredProcedure;
                //    cmdOne.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                //    cmdOne.Parameters.Add(new SqlParameter("@type", 1));





                //    SqlDataReader dr = cmdOne.ExecuteReader();
                //    dataTable.Load(dr);
                //    if (dataTable.Rows.Count != 0)
                //    {
                //        value = dataTable.Rows[0][0].ToString();



                //        if (value != "")
                //        {
                //            foreach (DataRow row in dataTable.Rows)
                //            {
                //                programManagerProgramStateWiseBudgets.Add(new budgetachievmentdto
                //                {
                //                    ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),



                //                    FunderContribution = (row["FunderContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["FunderContribution"]),
                //                    //ExpenseAmount = (row["Expense"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Expense"]),
                //                });
                //            }



                //        }
                //    }
                //    conn.Close();
                //}
                //



                var ProgramManagerBarChart = new List<BarChartData>();
                var ProgramManagerProgramStateWiseBudgetlineChart = Result.ToList().GroupBy(l => l.ProgramName).ToList();



                ProgramManagerProgramStateWiseBudgetlineChart.ForEach(l =>
                {
                    BarChartData ProgramStateWiseBudgetbar = new BarChartData();
                    ProgramStateWiseBudgetbar.labels = new List<string>();
                    ProgramStateWiseBudgetbar.datasets = new List<BarChartDataset>();
                    ProgramStateWiseBudgetbar.datasets.ForEach(x =>
                    {
                        x.data = new List<decimal>();
                    });
                    ProgramManagerBarChart.Add(ProgramStateWiseBudgetbar);
                });




                for (int i = 0; i < ProgramManagerProgramStateWiseBudgetlineChart.Count(); i++)
                {
                    var items = ProgramManagerProgramStateWiseBudgetlineChart[i].ToList();
                    ProgramManagerBarChart[i].labels.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].Key);



                    List<BarChartDataset> a = new List<BarChartDataset>();



                    for (int j = 0; j < 3; j++)
                    {



                        if (j == 0)
                        {
                            BarChartDataset obj = new BarChartDataset();
                            obj.data = new List<decimal>();
                            obj.backgroundColor = "red";
                            obj.borderColor = "blue";
                            obj.label = "Budget";
                            obj.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.FunderContribution));
                            a.Add(obj);



                        }
                        else if (j == 1)
                        {
                            BarChartDataset obj1 = new BarChartDataset();
                            obj1.data = new List<decimal>();
                            obj1.backgroundColor = "green";
                            obj1.borderColor = "pink";
                            obj1.label = "ActualPerformance";
                            obj1.data.Add(ProgramManagerProgramStateWiseBudgetlineChart[i].ToList().Sum(c => c.ExpenseAmount));
                            a.Add(obj1);
                        }



                    }



                    ProgramManagerBarChart[i].datasets = a;
                }



                allChartData.ProgramManagerProgramStateWiseBudgetBarChart = ProgramManagerBarChart;



                //}
                return allChartData;
            }
            catch (Exception ex)
            {



                throw;
            }

        }

        public async Task<AllChartDto> ProjectWiseRRCInchargeCommunityFunderOther(string projectName)
        {
            try
            {
                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramYearwiseStateWiseBudgets = new List<ProgramManagerProgramYearwiseStateWiseBudget>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdTwo = new SqlCommand("", conn))
                    {
                        //ProjectWiseInfoLineChart
                        var dataTable = new DataTable();
                        cmdTwo.CommandTimeout = 0;
                        cmdTwo.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdTwo.CommandType = CommandType.StoredProcedure;
                        cmdTwo.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                        cmdTwo.Parameters.Add(new SqlParameter("@ProjectName", projectName));
                        cmdTwo.Parameters.Add(new SqlParameter("@type", 3));

                        SqlDataReader dr = cmdTwo.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerProgramYearwiseStateWiseBudgets.Add(new ProgramManagerProgramYearwiseStateWiseBudget
                                    {
                                        //StateName = (row["StateName"] == DBNull.Value) ? "" : Convert.ToString(row["StateName"]),
                                        CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                                        ProjectName = (row["ProjectName"] == DBNull.Value) ? "" : Convert.ToString(row["ProjectName"]),
                                        FunderContribution = (row["FunderContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["FunderContribution"]),
                                        CommunityContribution = (row["CommunityContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["CommunityContribution"]),
                                        OtherContribution = (row["OtherContribution"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["OtherContribution"]),

                                    });
                                }

                            }
                        }
                        conn.Close();
                    }

                    //
                    var programManagerBarChart = new List<BarChartData>();
                    var ProgramManagerProgramYearwiseStateWiseBudgetBarChart = programManagerProgramYearwiseStateWiseBudgets.ToList().GroupBy(l => l.CostEstimationYear).ToList();

                    ProgramManagerProgramYearwiseStateWiseBudgetBarChart.ForEach(l =>
                    {
                        BarChartData ProgramManagerProgramYearwiseBudgetBarChart = new BarChartData();
                        ProgramManagerProgramYearwiseBudgetBarChart.labels = new List<string>();
                        ProgramManagerProgramYearwiseBudgetBarChart.datasets = new List<BarChartDataset>();
                        ProgramManagerProgramYearwiseBudgetBarChart.datasets.ForEach(x =>
                        {
                            x.data = new List<decimal>();
                        });
                        programManagerBarChart.Add(ProgramManagerProgramYearwiseBudgetBarChart);
                    });


                    for (int i = 0; i < ProgramManagerProgramYearwiseStateWiseBudgetBarChart.Count; i++)
                    {
                        var items = ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList();
                        programManagerBarChart[i].labels.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].Key);

                        List<BarChartDataset> a = new List<BarChartDataset>();

                        for (int j = 0; j < 3; j++)
                        {

                            if (j == 0)
                            {
                                BarChartDataset obj = new BarChartDataset();
                                obj.data = new List<decimal>();
                                obj.backgroundColor = "red";
                                obj.borderColor = "blue";
                                obj.label = "FunderContribution";
                                if (ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.FunderContribution) > 0)
                                {
                                    obj.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.FunderContribution));
                                }
                                a.Add(obj);

                            }
                            else if (j == 1)
                            {
                                BarChartDataset obj1 = new BarChartDataset();
                                obj1.data = new List<decimal>();
                                obj1.backgroundColor = "green";
                                obj1.borderColor = "pink";
                                obj1.label = "CommunityContribution";
                                if (ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.CommunityContribution) > 0)
                                {
                                    obj1.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.CommunityContribution));
                                }
                                a.Add(obj1);
                            }
                            else
                            {
                                BarChartDataset obj2 = new BarChartDataset();
                                obj2.data = new List<decimal>();
                                obj2.backgroundColor = "yellow";
                                obj2.borderColor = "pink";
                                obj2.label = "OtherContribution";
                                if (ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.OtherContribution) > 0)
                                {
                                    obj2.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.OtherContribution));
                                }
                                a.Add(obj2);
                            }
                        }

                        programManagerBarChart[i].datasets = a;
                    }

                    allChartData.ProgramManagerProgramYearwiseStateWiseBudgetBarChart = programManagerBarChart;
                }
                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<AllChartDto> ProjectWiseRRCInchargeCommunityFunderOtherForRRCIncharge(string projectName)
        {
            int PID = _programRepository.FirstOrDefault(xx => xx.Name == projectName).Id;
            try
            {
                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerProgramYearwiseStateWiseBudgets = new List<ProgramManagerProgramYearwiseStateWiseBudget>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdTwo = new SqlCommand("", conn))
                    {
                        //ProjectWiseInfoLineChart
                        var dataTable = new DataTable();
                        cmdTwo.CommandTimeout = 0;
                        cmdTwo.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdTwo.CommandType = CommandType.StoredProcedure;
                        cmdTwo.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                        cmdTwo.Parameters.Add(new SqlParameter("@ProjectName", projectName));
                        cmdTwo.Parameters.Add(new SqlParameter("@ProgramId", PID));

                        cmdTwo.Parameters.Add(new SqlParameter("@type", 15));

                        SqlDataReader dr = cmdTwo.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerProgramYearwiseStateWiseBudgets.Add(new ProgramManagerProgramYearwiseStateWiseBudget
                                    {
                                        //StateName = (row["StateName"] == DBNull.Value) ? "" : Convert.ToString(row["StateName"]),
                                        CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                                        ProjectName = (row["ProjectName"] == DBNull.Value) ? "" : Convert.ToString(row["ProjectName"]),
                                        FunderContribution = (row["FunderContribution1"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["FunderContribution1"]),
                                        CommunityContribution = (row["CommunityContribution1"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["CommunityContribution1"]),
                                        OtherContribution = (row["OtherContribution1"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["OtherContribution1"]),

                                    });
                                }

                            }
                        }
                        conn.Close();
                    }

                    //
                    var programManagerBarChart = new List<BarChartData>();
                    var ProgramManagerProgramYearwiseStateWiseBudgetBarChart = programManagerProgramYearwiseStateWiseBudgets.ToList().GroupBy(l => l.CostEstimationYear).ToList();

                    ProgramManagerProgramYearwiseStateWiseBudgetBarChart.ForEach(l =>
                    {
                        BarChartData ProgramManagerProgramYearwiseBudgetBarChart = new BarChartData();
                        ProgramManagerProgramYearwiseBudgetBarChart.labels = new List<string>();
                        ProgramManagerProgramYearwiseBudgetBarChart.datasets = new List<BarChartDataset>();
                        ProgramManagerProgramYearwiseBudgetBarChart.datasets.ForEach(x =>
                        {
                            x.data = new List<decimal>();
                        });
                        programManagerBarChart.Add(ProgramManagerProgramYearwiseBudgetBarChart);
                    });


                    for (int i = 0; i < ProgramManagerProgramYearwiseStateWiseBudgetBarChart.Count; i++)
                    {
                        var items = ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList();
                        programManagerBarChart[i].labels.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].Key);

                        List<BarChartDataset> a = new List<BarChartDataset>();

                        for (int j = 0; j < 3; j++)
                        {

                            if (j == 0)
                            {
                                BarChartDataset obj = new BarChartDataset();
                                obj.data = new List<decimal>();
                                obj.backgroundColor = "red";
                                obj.borderColor = "blue";
                                obj.label = "FunderContribution";
                                if (ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.FunderContribution) > 0)
                                {
                                    obj.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.FunderContribution));
                                }
                                a.Add(obj);

                            }
                            else if (j == 1)
                            {
                                BarChartDataset obj1 = new BarChartDataset();
                                obj1.data = new List<decimal>();
                                obj1.backgroundColor = "green";
                                obj1.borderColor = "pink";
                                obj1.label = "CommunityContribution";
                                if (ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.CommunityContribution) > 0)
                                {
                                    obj1.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.CommunityContribution));
                                }
                                a.Add(obj1);
                            }
                            else
                            {
                                BarChartDataset obj2 = new BarChartDataset();
                                obj2.data = new List<decimal>();
                                obj2.backgroundColor = "yellow";
                                obj2.borderColor = "pink";
                                obj2.label = "OtherContribution";
                                if (ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.OtherContribution) > 0)
                                {
                                    obj2.data.Add(ProgramManagerProgramYearwiseStateWiseBudgetBarChart[i].ToList().Sum(c => c.OtherContribution));
                                }
                                a.Add(obj2);
                            }
                        }

                        programManagerBarChart[i].datasets = a;
                    }

                    allChartData.ProgramManagerProgramYearwiseStateWiseBudgetBarChart = programManagerBarChart;
                }
                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<AllChartDto> YearWiseRRCAssignedReport(string prgramName, string projectYear)
        {
            try
            {
                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerImplemenationPlanCheckListWiseBudgets = new List<ProgramManagerImplemenationPlanCheckListWiseBudget>();

                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                        cmdThree.Parameters.Add(new SqlParameter("@CostEstimationYear", projectYear));
                        cmdThree.Parameters.Add(new SqlParameter("@ProgramName", prgramName));
                        cmdThree.Parameters.Add(new SqlParameter("@type", 4));

                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerImplemenationPlanCheckListWiseBudgets.Add(new ProgramManagerImplemenationPlanCheckListWiseBudget
                                    {
                                        CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                                        Activity = (row["Activity"] == DBNull.Value) ? "" : Convert.ToString(row["Activity"]),
                                        SubActivity = (row["SubActivity"] == DBNull.Value) ? "" : Convert.ToString(row["SubActivity"]),
                                        AssignedTo = (row["AssignedTo"] == DBNull.Value) ? "" : Convert.ToString(row["AssignedTo"]),
                                        CheckListStartDate = (row["CheckListStartDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["CheckListStartDate"]),
                                        CheklistEndDate = (row["CheklistEndDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["CheklistEndDate"]),
                                        Status = (row["Status"] == DBNull.Value) ? "" : Convert.ToString(row["Status"]),
                                    });
                                }

                            }
                        }
                        conn.Close();
                    }
                }
                allChartData.actionAReaRRCdtos = new List<ActionAReaRRCdto>();
                allChartData.programManagerImplemenationPlanCheckListWiseBudgets = programManagerImplemenationPlanCheckListWiseBudgets;
                var budget = YearWiseRRCActionAReabudget(prgramName, projectYear);
                allChartData.actionAReaRRCdtos = budget.Result;
                allChartData.programManagerImplemenationPlanCheckListWiseBudgets = programManagerImplemenationPlanCheckListWiseBudgets;
                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<ActionAReaRRCdto>> YearWiseRRCActionAReabudget(string prgramName, string projectYear)
        {
            try
            {
                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerImplemenationPlanCheckListWiseBudgets = new List<ActionAReaRRCdto>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                        cmdThree.Parameters.Add(new SqlParameter("@CostEstimationYear", projectYear));
                        cmdThree.Parameters.Add(new SqlParameter("@ProgramName", prgramName));
                        cmdThree.Parameters.Add(new SqlParameter("@type", 5));

                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerImplemenationPlanCheckListWiseBudgets.Add(new ActionAReaRRCdto
                                    {
                                        ActionAreaName = (row["ActionAreaName"] == DBNull.Value) ? "" : Convert.ToString(row["ActionAreaName"]),
                                        FunderContribution = (row["Budget"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Budget"])

                                    });
                                }

                            }
                        }
                        conn.Close();
                    }
                }
                return programManagerImplemenationPlanCheckListWiseBudgets;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<ActionAReaRRCdto>> YearWiseRRCActionAReaSubActionbudget(string prgramName, string projectYear, string ActionAreaName)
        {
            try
            {
                //var ActionAreanm= ActionAreaName.Substring(0, ActionAreaName.IndexOf(":"));
                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var programManagerImplemenationPlanCheckListWiseBudgets = new List<ActionAReaRRCdto>();
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@RRCInchargeId", input));
                        cmdThree.Parameters.Add(new SqlParameter("@CostEstimationYear", projectYear));
                        cmdThree.Parameters.Add(new SqlParameter("@ProgramName", prgramName));
                        cmdThree.Parameters.Add(new SqlParameter("@ActionAreaName", ActionAreaName));

                        cmdThree.Parameters.Add(new SqlParameter("@type", 6));

                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    programManagerImplemenationPlanCheckListWiseBudgets.Add(new ActionAReaRRCdto
                                    {
                                        Subactionareaname = (row["SubActionAreaName"] == DBNull.Value) ? "" : Convert.ToString(row["SubActionAreaName"]),
                                        Budget = (row["Budget"] == DBNull.Value) ? 00 : Convert.ToDecimal(row["Budget"])

                                    });
                                }

                            }
                        }
                        conn.Close();
                    }
                }
                return programManagerImplemenationPlanCheckListWiseBudgets;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<AllChartDto> SummaryAllProjects()
        {
            try
            {
                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var summryAllProjects = new List<SummryAllProjects>();
                var summryAllProjectss = new List<SummryAllProjects>();

                var finyear = "";
                DateTime dt = Convert.ToDateTime(DateTime.Now.ToString());
                int m = dt.Month;
                int y = dt.Year;
                if (m > 3)
                {
                    finyear = y.ToString() + "-" + Convert.ToString((y + 1));
                    //get last  two digits (eg: 10 from 2010);
                }
                else
                {
                    finyear = Convert.ToString((y - 1)) + "-" + y.ToString();
                }


                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@RRCInchargeId", input));

                        cmdThree.Parameters.Add(new SqlParameter("@type", 12));

                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    summryAllProjects.Add(new SummryAllProjects
                                    {
                                        ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),
                                        ProjectStatus = (row["ProjectStatus"] == DBNull.Value) ? "" : Convert.ToString(row["ProjectStatus"]),

                                    });
                                }

                            }
                        }
                        conn.Close();
                    }
                }
                //allChartData.actionAReaRRCdtos = new List<ActionAReaRRCdto>();
                //allChartData.programManagerImplemenationPlanCheckListWiseBudgets = programManagerImplemenationPlanCheckListWiseBudgets;
                //var budget = YearWiseRRCActionAReabudget(prgramName, projectYear);
                //allChartData.actionAReaRRCdtos = budget.Result;

                foreach (var item in summryAllProjects)
                {
                    if (item.ProjectStatus == "InProgress" || item.ProjectStatus == "")
                    {
                        summryAllProjectss.Add(new SummryAllProjects
                        {
                            ProgramName = item.ProgramName,
                            ProjectStatus = item.ProjectStatus == "" ? "InProgress" : item.ProjectStatus,


                        });

                    }
                }
                var latestActivityAssignReports = new List<LatestActivityAssignReport>();
                var a = LatestActivityAssignReport();
                allChartData.summryAllProjects = summryAllProjectss;
                allChartData.latestActivityAssignReports = a.Result;
                //This Query is changed by Sonali

                var Result = (from ra in _RequestAAplasRepository
                                   .GetAll()
                                   .Where(aa => aa.ManagerID == input)
                              join p in _programRepository
                                   .GetAll() on ra.ProgramID equals p.Id
                              group ra by new
                              {
                                  raa = ra.ProgramID,
                                  programId = p.Id,
                                  pName = p.Name,
                                  //mngId = ra.ManagerID
                              } into g

                              select new budgetachievmentdto
                              {
                                  FunderContribution = Convert.ToDecimal(_RequestAAplasRepository
                                                      .GetAll().Where(xx => xx.ProgramID == g.Key.raa) //&& xx.CostEstimationYear == finyear)// && xx.ManagerID == g.Key.mngId)
                                                      .Sum(aa => aa.FunderContribution)),
                                  ExpenseAmount = _ProgramExpenseRepository.GetAll()
                                                  .Where(xx => xx.ProgramID == g.Key.raa &&
                                                  xx.Status != ProgramFunds.Status1.Reject) //&&  xx.ExpensesYear == finyear)  && xx.ManagerID == g.Key.mngId)
                                                  .Sum(aa => aa.Amount),
                                  ProgramName = g.Key.pName,
                              }).ToList();




                //var Result = (
                //              //from ra in _villageClusterRepository.GetAll().Where(aa => aa.UserId == input)
                //              //join p in _programRepository.GetAll() on ra.ProgramID equals p.Id
                //              from p in _programRepository.GetAll() //on ra.ProgramID equals p.Id
                //              group p by new
                //              {
                //                  raa = p.Id,
                //                  programId = p.Id,
                //                  pName = p.Name
                //              } into g
                //            select new budgetachievmentdto
                //              {
                //                  FunderContribution = Convert.ToDecimal(_ProgramExpenseRepository.GetAll().Where(xx => xx.ProgramID == g.Key.raa
                //                                                         &&xx.ManagerID == input 
                //                                                        //&& xx.ExpensesYear == finyear
                //                                                        && xx.Status != ProgramFunds.Status1.Reject).Sum(aa => aa.Amount)),
                //                  ExpenseAmount = Convert.ToDecimal(_RequestAAplasRepository.GetAll().Where(xx => xx.ProgramID == g.Key.raa
                //                                                    && xx.ManagerID == input 
                //                                                    && xx.TotalUnits != 0) //&& xx.CostEstimationYear == finyear)
                //                                                    .Sum(aa => aa.CommunityContributionRecived)),
                //                  ProgramName = g.Key.pName,
                //              }).ToList();
                allChartData.budgetachievmentdtos = Result;

                //For Start fincial Year to Current fincial year 
                var allYearPCE = _programCostEstimationRepository.GetAll().Where(x => x.CostEstimationYear != null).OrderBy(xx => xx.Id).Select(xx => xx.CostEstimationYear).Distinct().ToList();
                var allYearPER = _ProgramExpenseRepository.GetAll().Where(x => x.ExpensesYear != null).OrderBy(xx => xx.Id).Select(xx => xx.ExpensesYear).Distinct().ToList();
                var allYearRA = _RequestAAplasRepository.GetAll().Where(x => x.CostEstimationYear != null).OrderBy(xx => xx.Id).Select(xx => xx.CostEstimationYear).Distinct().ToList();

                var yearRangesPCE = new List<string>();
                var yearRangesPER = new List<string>();
                var yearRangesRA = new List<string>();
                string NYear = finyear.Substring(5, 4);
                foreach (var i in allYearPCE)
                {
                    if (i.Length > 8)
                    {
                        if (Convert.ToInt32(i.Substring(0, 4)) < Convert.ToInt32(NYear))
                        {
                            yearRangesPCE.Add(i);
                        }
                    }
                }
                foreach (var i in allYearPER)
                {
                    if (i.Length > 8)
                    {
                        if (Convert.ToInt32(i.Substring(0, 4)) < Convert.ToInt32(NYear))
                        {
                            yearRangesPER.Add(i);
                        }
                    }
                }
                foreach (var i in allYearRA)
                {
                    if (i.Length > 8)
                    {
                        if (Convert.ToInt32(i.Substring(0, 4)) < Convert.ToInt32(NYear))
                        {
                            yearRangesRA.Add(i);
                        }
                    }
                }


                var result1 = (from ra in _villageClusterRepository.GetAll().Where(aa => aa.UserId == input)

                               join p in _programRepository.GetAll() on ra.ProgramID equals p.Id
                               group ra by new
                               {
                                   raa = ra.ProgramID,
                                   programId = p.Id,
                                   pName = p.Name
                               } into g
                               select new totalbudgettotalexpense
                               {//SELECT sum([FunderContribution]) FROM[dbo].[ProgramCostEstimation] where[ProgramID] = 35 and[CostEstimationYear] IN('2018-2019', '2019-2020')
                                   FunderContribution = Convert.ToDecimal(_programCostEstimationRepository.GetAll()
                                                            .Where(xx => xx.ProgramID == g.Key.raa
                                                            && yearRangesPCE.Any(yr => yr == xx.CostEstimationYear)
                                                            // && xx.ManagerID == input 
                                                            //&& xx.ExpensesYear == finyear               //this Query is Changed by Sonali
                                                            //&& xx.Status != ProgramFunds.Status1.Reject
                                                            ).Sum(aa => aa.FunderContribution)),
                                   CommunityContribution = Convert.ToDecimal(_programCostEstimationRepository.GetAll()
                                                            .Where(xx => xx.ProgramID == g.Key.raa
                                                            && yearRangesPCE.Any(yr => yr == xx.CostEstimationYear)
                                                            //&& xx.ManagerID == input                       //this Query is Changed by Sonali
                                                            ).Sum(aa => aa.CommunityContribution)),
                                   Othercontribution = Convert.ToDecimal(_programCostEstimationRepository.GetAll().Where(xx => xx.ProgramID == g.Key.raa
                                                                        && yearRangesPCE.Any(yr => yr == xx.CostEstimationYear)
                                                                            //&& xx.ManagerID == input
                                                                            ).Sum(aa => aa.OtherContribution)),

                                   ExpenseAmount = Convert.ToDecimal(_ProgramExpenseRepository.GetAll().Where(xx => xx.ProgramID == g.Key.raa //&& xx.ManagerID == input 
                                                                     && xx.Status != ProgramFunds.Status1.Reject
                                                                     && yearRangesPER.Any(xy => xy == xx.ExpensesYear)
                                                                    //&& xx.CostEstimationYear == finyear
                                                                    ).Sum(aa => aa.Amount)),
                                   Communitycontributionreceived = Convert.ToDecimal(_RequestAAplasRepository.GetAll().
                                                                    Where(xx => xx.ProgramID == g.Key.raa && xx.TotalUnits != 0 //&& xx.ManagerID == input
                                                                    && yearRangesRA.Any(xy => xy == xx.CostEstimationYear)
                                                                    ).Sum(aa => aa.CommunityContributionRecived)),
                                   Govcontributionreceived = Convert.ToDecimal(_RequestAAplasRepository.GetAll().Where(xx => xx.ProgramID == g.Key.raa
                                                                          && xx.TotalUnits != 0// && xx.ManagerID == input
                                                                          && yearRangesRA.Any(xy => xy == xx.CostEstimationYear)
                                                                   ).Sum(aa => aa.GovernmentContributionRecived)),
                                   TotalBalanceamount = Convert.ToDecimal(_RequestAAplasRepository.GetAll().Where(xx => xx.ProgramID == g.Key.raa).Sum(aa => aa.FunderContribution))
                                                      - Convert.ToDecimal(_ProgramExpenseRepository.GetAll().Where(xx => xx.ProgramID == g.Key.raa).Sum(aa => aa.Amount)),
                                   TotalBalanceCommunity = Convert.ToDecimal(_RequestAAplasRepository.GetAll().Where(xx => xx.ProgramID == g.Key.raa).Sum(aa => aa.CommunityContribution))
                                                         - Convert.ToDecimal(_RequestAAplasRepository.GetAll().Where(xx => xx.ProgramID == g.Key.raa).Sum(aa => aa.CommunityContributionRecived)),
                                   ProgramName = g.Key.pName,
                                   PID = g.Key.programId
                               }).ToList();
                allChartData.totalbudgettotalexpenses = result1;

                return allChartData;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<List<clustorwisetotalbudgettotalexpense>> Clustorwisetotalbudgettotalexpenses(int ProgramId)
        {
            try
            {
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var clustorwisetotalbudgettotalexpense = new List<clustorwisetotalbudgettotalexpense>();

                //this Querries is Changed by Sonali

                var finyear = "";
                DateTime dt = Convert.ToDateTime(DateTime.Now.ToString());
                int m = dt.Month;
                int y = dt.Year;
                if (m > 3)
                {
                    finyear = y.ToString() + "-" + Convert.ToString((y + 1));
                    //get last  two digits(eg: 10 from 2010);
                }
                else
                {
                    finyear = Convert.ToString((y - 1)) + "-" + y.ToString();
                }


                var allYearPCE = _programCostEstimationRepository.GetAll().Where(x => x.CostEstimationYear != null).OrderBy(xx => xx.Id).Select(xx => xx.CostEstimationYear).Distinct().ToList();
                var allYearPER = _ProgramExpenseRepository.GetAll().Where(x => x.ExpensesYear != null).OrderBy(xx => xx.Id).Select(xx => xx.ExpensesYear).Distinct().ToList();
                var allYearRA = _RequestAAplasRepository.GetAll().Where(x => x.CostEstimationYear != null).OrderBy(xx => xx.Id).Select(xx => xx.CostEstimationYear).Distinct().ToList();

                var yearRangesPCE = new List<string>();
                var yearRangesPER = new List<string>();
                var yearRangesRA = new List<string>();
                string NYear = finyear.Substring(5, 4);
                foreach (var i in allYearPCE)
                {
                    if (i.Length > 8)
                    {
                        if (Convert.ToInt32(i.Substring(0, 4)) < Convert.ToInt32(NYear))
                        {
                            yearRangesPCE.Add(i);
                        }
                    }
                }
                foreach (var i in allYearPER)
                {
                    if (i.Length > 8)
                    {
                        if (Convert.ToInt32(i.Substring(0, 4)) < Convert.ToInt32(NYear))
                        {
                            yearRangesPER.Add(i);
                        }
                    }
                }
                foreach (var i in allYearRA)
                {
                    if (i.Length > 8)
                    {
                        if (Convert.ToInt32(i.Substring(0, 4)) < Convert.ToInt32(NYear))
                        {
                            yearRangesRA.Add(i);
                        }
                    }
                }

                var expense = _ProgramExpenseRepository.GetAll();

                var result = (from ra in _RequestAAplasRepository.GetAll().Include(t => t.Village).Where(aa => aa.ProgramID == ProgramId && aa.ManagerID == input)
                              join l in _LocationRepository.GetAll() on ra.LocationID equals l.Id
                              join v in _VillageRepository.GetAll() on ra.VillageID equals v.Id into rtemp
                              from v in rtemp.DefaultIfEmpty()
                              group ra by new
                              {
                                  raa = ra.ProgramID,
                                  ClustorName = l.Name,
                                  VilageName = v == null ? "" : v.Name
                              } into g
                              select new clustorwisetotalbudgettotalexpense
                              {
                                  FunderContribution = Convert.ToDecimal(g.Where(xx => xx.TotalUnits != 0 && yearRangesRA.Any(yr => yr == xx.CostEstimationYear)).Sum(aa => aa.FunderContribution)),
                                  CommunityContribution = Convert.ToDecimal(g.Where(xx => xx.TotalUnits != 0 && yearRangesRA.Any(yr => yr == xx.CostEstimationYear)).Sum(aa => aa.CommunityContribution)),
                                  Othercontribution = Convert.ToDecimal(g.Where(xx => xx.TotalUnits != 0 && yearRangesRA.Any(yr => yr == xx.CostEstimationYear)).Sum(aa => aa.OtherContribution)),
                                  ExpenseAmount = Convert.ToDecimal((from y1 in expense.Where(xx => xx.ProgramID == ProgramId && xx.ManagerID == input && xx.Status != ProgramFunds.Status1.Reject && yearRangesPER.Any(xy => xy == xx.ExpensesYear))
                                                                     join y2 in g
                                                                     on y1.PrgActionAreaActivityMappingID equals y2.PrgActionAreaActivityMappingID
                                                                     select y1).Sum(aa => aa.Amount)),
                                  Communitycontributionreceived = Convert.ToDecimal(g.Where(xx => xx.TotalUnits != 0 && yearRangesRA.Any(xy => xy == xx.CostEstimationYear))
                                                             .Sum(aa => aa.CommunityContributionRecived)),
                                  Govcontributionreceived = Convert.ToDecimal(g.Where(xx => xx.ProgramID == ProgramId
                                                                         && xx.TotalUnits != 0
                                                                         && xx.ManagerID == input
                                                                         && yearRangesRA.Any(xy => xy == xx.CostEstimationYear))
                                                            .Sum(aa => aa.GovernmentContributionRecived)),
                                  TotalBalanceamount = Convert.ToDecimal(g.Sum(aa => aa.FunderContribution))
                                                       - Convert.ToDecimal((from y1 in expense.Where(xx => xx.ProgramID == ProgramId && xx.ManagerID == input && xx.Status != ProgramFunds.Status1.Reject && yearRangesPER.Any(xy => xy == xx.ExpensesYear))
                                                                            join y2 in g
                                                                            on y1.PrgActionAreaActivityMappingID equals y2.PrgActionAreaActivityMappingID
                                                                            select y1).Sum(aa => aa.Amount)),
                                  TotalBalanceCommunity = Convert.ToDecimal(g.Sum(aa => aa.CommunityContribution))
                                                          - Convert.ToDecimal(g.Where(xx => xx.ProgramID == ProgramId).Sum(aa => aa.CommunityContributionRecived)),
                                  ClustorName = g.Key.ClustorName,
                                  VilageName = g.Key.VilageName
                              }


                    ).ToList();




                return result;

                //  return clustorwisetotalbudgettotalexpense;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        public async Task<List<LatestActivityAssignReport>> LatestActivityAssignReport()
        {
            try
            {
                string value;
                var allChartData = new AllChartDto();
                int input = Convert.ToInt32(GetCurrentUser().Id);
                var connectionString = _appConfiguration["ConnectionStrings:Default"];
                var latestActivityAssignReports = new List<LatestActivityAssignReport>();
                var summryAllProjectss = new List<SummryAllProjects>();


                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    await ConnectionConnect(conn);
                    using (var cmdThree = new SqlCommand("", conn))
                    {
                        //ProjectWiseCostEstimationYear
                        var dataTable = new DataTable();
                        cmdThree.CommandTimeout = 0;
                        cmdThree.CommandText = "[dbo].[RRCInchargeDashborad]";
                        cmdThree.CommandType = CommandType.StoredProcedure;
                        cmdThree.Parameters.Add(new SqlParameter("@RRCInchargeId", input));

                        cmdThree.Parameters.Add(new SqlParameter("@type", 13));

                        SqlDataReader dr = cmdThree.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();

                            if (value != "")
                            {
                                foreach (DataRow row in dataTable.Rows)
                                {
                                    latestActivityAssignReports.Add(new LatestActivityAssignReport
                                    {
                                        ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),
                                        ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),
                                        SubActivityName = (row["SubactivityName"] == DBNull.Value) ? "" : Convert.ToString(row["SubactivityName"]),
                                        AssignTo = (row["AssignedTo"] == DBNull.Value) ? "" : Convert.ToString(row["AssignedTo"]),

                                        ProjectStatus = (row["ProjectStatus"] == DBNull.Value) ? "" : Convert.ToString(row["ProjectStatus"]),

                                    });
                                }

                            }
                        }
                        conn.Close();
                    }
                }
                //allChartData.actionAReaRRCdtos = new List<ActionAReaRRCdto>();
                //allChartData.programManagerImplemenationPlanCheckListWiseBudgets = programManagerImplemenationPlanCheckListWiseBudgets;
                //var budget = YearWiseRRCActionAReabudget(prgramName, projectYear);
                //allChartData.actionAReaRRCdtos = budget.Result;



                return latestActivityAssignReports;
            }
            catch (Exception ex)
            {

                throw;
            }
        }



    }
}