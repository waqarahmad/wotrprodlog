﻿using Abp.Domain.Entities.Auditing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.ProgramFunds;

namespace WOTR.PM.ProgramExpenses.Dto
{
   public  class ExpenseDto :FullAuditedEntity
    {
        public virtual int TenantId { get; set; }
        public int ProgramID { get; set; }
        public string ExpensesYear { get; set; }
        public int? PrgActionAreaActivityMappingID { get; set; }
        public int QuarterId { get; set; }
        public string ExpenseTitle { get; set; }
        public int ExpensesTypeID { get; set; }
        public string ExpensesTypeName { get; set; }
        public DateTime ExpenseDate { get; set; }
        public string Remark { get; set; }
        public int Unit { get; set; }
        public decimal Amount { get; set; }
        public string Image { get; set; }
        public int? UnitOfMeasuresID { get; set; }
        public string UnitOfMeasuresIDName { get; set; }
        public string subActivityName { get; set; }
        public int subActivityId { get; set; }
        public int? MapSubActivityIteamsToImplementionPlanID { get; set; }
        public int? ProgramQuqterUnitMappingID { get; set; }
        public int ImplementationPlanCheckListID { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public Status1 status { get; set; }
        public int ManagerID { get; set; }
        public string ManagerName { get; set; }
        public List< expensesimage> Expensesimage { get; set; }
        public string ActivityName { get; set; }
        public string ProgramName { get; set; }
        public string LocationName { get; set; }
        public string ExDate { get; set; }

        public int Count { get; set; }


    }

    public   class ExpensesTypeDto : FullAuditedEntity
    {
        public virtual int TenantId { get; set; }
        public string TypeName { get; set; }
        public int ProgramId { get; set; }
        
    }

    public class expensesimage : FullAuditedEntity
    {
        public string image { get; set; }
    }
    //public class ProgramQuqterUnitMapping
    //{
    //    public string ExpenseTitle { get; set; }
    //    public ExpenseType ExpenseType { get; set; }
    //    public DateTime ExpenseDate { get; set; }
    //    public string Remark { get; set; }
    //    public int Unit { get; set; }
    //    public decimal Amount { get; set; }
    //    public string Image { get; set; }
    //    public int? UnitOfMeasuresID { get; set; }
    //    public int? MapSubActivityIteamsToImplementionPlanID { get; set; }
    //}
}
