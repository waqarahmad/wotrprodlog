﻿using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.Dto;

namespace WOTR.PM.ProgramExpenses.Dto
{
    public class GetExpensesPerProgramFilterDto: PagedAndSortedInputDto
    {
        public string Filter { get; set; }

        public int programId { get; set; }

        public string Year { get; set; }

    }
}
