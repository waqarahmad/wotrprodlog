﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.ProgramExpenses.Dto
{
  public   class ProgramExpenseImageDto : FullAuditedEntity
    {
        public virtual int TenantId { get; set; }

       
        public int ProgramExpenseID { get; set; }
       

        public string image { get; set; }
       
    }
}
