﻿using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.ProgramExpenses.Dto;
using Microsoft.WindowsAzure.Storage.Blob;

namespace WOTR.PM.ProgramExpenses
{
    public interface IUploadExpensesFileAppservice
    {
        CloudBlobContainer GetCloudBlobContainer(string containerName);
        void UploadImage(UploadFileDto input, string containerName);
        List<string> UploadFiles(List<UploadFileDto> input, string containerName);
    }
}
