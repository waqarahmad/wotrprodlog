﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ProgramExpenses.Dto;

namespace WOTR.PM.ProgramExpenses
{
   public  interface IProgramExpenseAppService:  IApplicationService
   {
       Task<List<string>> CreateORUpdate(ExpenseDto Input);
        List<ExpenseDto> GetExpenseAsPerProgramID(int programId);
        List<string> getAllEnumLists();
        List<ExpensesTypeDto> GetAllExpensesType();
        void DeleteExpense(EntityDto<int> Input);

    }
}
