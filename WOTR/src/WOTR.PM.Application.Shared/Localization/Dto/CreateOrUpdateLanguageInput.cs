﻿using System.ComponentModel.DataAnnotations;

namespace WOTR.PM.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}