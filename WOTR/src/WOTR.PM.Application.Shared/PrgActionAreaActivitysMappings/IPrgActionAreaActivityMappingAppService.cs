﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Users.Dto;
using WOTR.PM.PrgActionAreaActivitysMappings.Dto;

namespace WOTR.PM.PrgActionAreaActivitysMappings
{
   public interface IPrgActionAreaActivityMappingAppService :IApplicationService 
    {
        //List<PrgActionAreaActivityMappingRepositoryDto> GetAllprogramActionAreaActivity(int ProjectId);

        Task<List<string>> CreateOrUpdatePrgActivityMapping(List<PrgActionAreaActivityMappingRepositoryDto> Input);
        Task<List<string>> CreateOrUpdateActivityActionAreaJSTree(prgActionAreaJSTreeDto Input);
        List<prgActionAreaJSTreeDto> GetAllJsTree(int? ProjectId);
        Task<UserRoleDto> GetUserRoleNameForEdit(NullableIdDto<long> input);
        List<prgActionAreaJSTreeDto> GetAllProgrameComponentWithActivityJsTree(string Input, int user_id);
        
    }
}
