﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.PrgActionAreaActivitysMappings.Dto
{
   public  class prgActionAreaJSTreeDto : FullAuditedEntity
    {
        public virtual int TenantId { get; set; }
        public string Name { get; set; }
        public int PrgComponentActivityId { get; set; }
        public int PranteId { get; set; }
        public int ProgramID { get; set; }
        public int? ComponentID { get; set; }
        public string ComponentName { get; set; }
        public int? ActivityID { get; set; }     
        public int? ActionAreaID { get; set; }
        public int? SubActionAreaID { get; set; }
    }
}
