﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.Authorization.Users.Dto;


namespace WOTR.PM.PrgVillageCluster.Dto
{
    public class PreprogramdetailsDto : FullAuditedEntity
    {
        public List<prgVillageClusterListDto> RRc { get; set; }
        public List<prgVillageClusterListDto> VillageClusterList { get; set; }
        public List<prgVillageClusterVillages> Villages { get; set; }
        public List<PrgVillageClusterUsers> Users { get; set; }
        public List<prgManagersListDtos> RRCC { get; set; }
        public List<prgManagersListDtosrrc> RRCInchargelist { get; set; }
        public List<Precostestimationoverall> components { get; set; }
        public List<precostestimationoverallActivity> Activity { get; set; }
        public List<programInformationListDto> programdetails { get; set; }


    }

    public class programInformationListDto : FullAuditedEntity
    {
        public static List<programInformationListDto> programdetails;

        //...Program Table
        public string ProgramName { get; set; }

        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public int? DonorID { get; set; }
        public int TenantId { get; set; }

        public long ProgramSanctionedYear { get; set; }
        public DateTime ProgramStartDate { get; set; }
        public DateTime PrgramEndDate { get; set; }


        //...programmanagaerMapping.
        public List<int> ManagerID { get; set; }
        public int ProgramManagerId { get; set; }      //for single entry
        ////.program Region coverage
        public List<int?> LocationID { get; set; }

        //public int? ProgramID { get; set; }


        public DateTime ProgramSubmitDate { get; set; } //..program submition date .
        public DateTime ProgramApprovaltDate { get; set; } //..program Approval date .
        public decimal? GantnAmountSanctionRupess { get; set; } //..Donor sanction Amount .


        public string DonorName { get; set; }
        public string ManagerName { get; set; }
        public string ProgramBadgeColor { get; set; }
        public string ProgramBadgeImage { get; set; }
        public DateTime? ExecutionTime { get; set; }
        public string ServiceName { get; set; }
    }
    public class prgVillageClusterListDto : FullAuditedEntity
    { 
    public string Name { get; set; } //....Location Name
    public string StateName { get; set; }
    public string DistrictName { get; set; }
    public string TalukaName { get; set; }
    public string VillageName { get; set; }
    public string ComponentName { get; set; }

    public string RRcName { get; set; }

    public int? ProgramID { get; set; }  //....Foriegn key is created in database table with the name "ProgramID"  

    public int? RRCID { get; set; }  //....Foriegn key is created in database table with the name "LocationId"   

    public int? StateID { get; set; }  //....Foriegn key is created in database table with the name "ProgramID"    

    public int? DistrictID { get; set; }  //....Foriegn key is created in database table with the name "ProgramID" 

   /* public int? TalukaID { get; set; } */ //....Foriegn key is created in database table with the name "ProgramID"      
    public List<int?> TalukaID { get; set; }
   public List<int?> villageID { get; set; }
    public int? LocationId { get; set; }

    public List<long> UsreId { get; set; }
}

    public class prgVillageClusterVillages : FullAuditedEntity
    {
        public string villageName { get; set; }
        public int? villageID { get; set; }
    }

    public class precostestimationoverall : FullAuditedEntity
    {
        public string ComponentName { get; set; }
        public int? ComponenetId { get; set; }
    }

    public class precostestimationoverallActivity : FullAuditedEntity
    {
        public string ActivityName { get; set; }
        public int? ActivityId { get; set; }
        public decimal TotalUnitCost { get; set; }
        public long TotalUnit { get; set; }
        public decimal UnitCost { get; set; }
        public decimal CommunityConstribution { get; set; }
        public decimal FunderConstribution { get; set; }
        public decimal OtherConstribution { get; set; }
        public long AllTotal { get; set; }

    }

    public class PrgVillageClusterUsers : FullAuditedEntity
    {
        public string UserName { get; set; }
        public long UsreID { get; set; }
    }

    public class prgManagersListDtos : FullAuditedEntity
    {
        public List<UserListDto> RRcIncharge { get; set; }

        public List<UserListDto> ProjectManagers { get; set; }

        public string username { get; set; }
        public int? userid { get; set; }
    }
    public class prgManagersListDtosrrc : FullAuditedEntity
    {
        public List<UserListDto> RRcIncharge { get; set; }

        public List<UserListDto> ProjectManagers { get; set; }

        public string username { get; set; }
        public int? userid { get; set; }
    }

    public class precostestimationoverallregionsummuery : FullAuditedEntity
    {
        public string ActivityName { get; set; }
        public int? ActivityId { get; set; }
        public decimal TotalUnitCost { get; set; }
        public long TotalUnit { get; set; }
        public decimal UnitCost { get; set; }
        public decimal CommunityConstribution { get; set; }
        public decimal FunderConstribution { get; set; }
        public decimal OtherConstribution { get; set; }
        public long AllTotal { get; set; }
        public string ActionAreaName { get; set; }
        public string SubactionAreaName { get; set; }
        public string programName { get; set; }
        public string loctionname { get; set; }
        public decimal TotalAll { get; set; }
        public string RegionName { get; set; }
        public int? locationId { get; set; }
        public decimal? GrantTotal { get; set; }
    }

}

