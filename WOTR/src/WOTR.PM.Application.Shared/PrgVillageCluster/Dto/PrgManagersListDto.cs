﻿using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.Authorization.Users.Dto;

namespace WOTR.PM.PrgVillageCluster.Dto
{
    public class PrgManagersListDto
    {
        public List<UserListDto> RRcIncharge { get; set; }

        public List<UserListDto> ProjectManagers { get; set; }
    }
}
