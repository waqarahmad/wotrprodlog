﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.Authorization.Users.Dto;

namespace WOTR.PM.PrgVillageCluster.Dto
{
    public class PrgVillageClusterListDto: FullAuditedEntity
    {
        public string Name { get; set; } //....Location Name
        public string StateName { get; set; }
        public string DistrictName { get; set; }
        public string TalukaName { get; set; }
        public string VillageName { get; set; }
        public string ComponentName { get; set; }

        public string RRcName { get; set; }

        public int? ProgramID { get; set; }  //....Foriegn key is created in database table with the name "ProgramID"  
       
        public int? RRCID { get; set; }  //....Foriegn key is created in database table with the name "LocationId"   
        
        public int? StateID { get; set; }  //....Foriegn key is created in database table with the name "ProgramID"    
       
        public int? DistrictID { get; set; }  //....Foriegn key is created in database table with the name "ProgramID" 

        /*public int? TalukaID { get; set; }*/  //....Foriegn key is created in database table with the name "ProgramID"      
        public List<int?> TalukaID { get; set; }
        public List<int?> villageID { get; set; }
        public int? LocationId { get; set; }

        public List<ClusterUser> PrgClusterUser { get; set; }
        public List<PrgVillageClusterListDto> RRc { get; set; }
        public List<PrgVillageClusterListDto> VillageClusterList { get; set; }
        public List<PrgVillageClusterVillages> Villages { get; set; }
        public List<PrgVillageClusterTaluka> Talukas { get; set; }
        public List<prgVillageClusterUsers> Users { get; set; }
        public List<PrgManagersListDtos> RRCC { get; set; }
        public List<PrgManagersListDtosrrc> RRCInchargelist { get; set; }
        public List<Precostestimationoverall> components { get; set; }
        public List<PrecostestimationoverallActivity> Activity { get; set; }
    }

    public class ClusterUser
    {
        public long UserId { get; set; }
        public long UserRoleId { get; set; }
    }

    public class PrgVillageClusterVillages:FullAuditedEntity
    {
        public string Name { get; set; }
    }
    public class PrgVillageClusterTaluka : FullAuditedEntity
    {
        public string TalukaName { get; set; }
    }

    public class Precostestimationoverall : FullAuditedEntity
    {
        public string ComponentName { get; set; }
        public int? ComponenetId { get; set; }
    }

    public class PrecostestimationoverallActivity : FullAuditedEntity
    {
        public string ActivityName { get; set; }
        public int? ActivityId { get; set; }
        public decimal TotalUnitCost { get; set; }
        public long TotalUnit { get; set; }
        public decimal UnitCost { get; set; }
        public decimal CommunityConstribution { get; set; }
        public decimal FunderConstribution { get; set; }
        public decimal OtherConstribution { get; set; }
        public long AllTotal { get; set; }
        public string ActivityLevel { get; set; }


    }

    public class prgVillageClusterUsers:FullAuditedEntity
    {
        public string UserName { get; set; }
        public long UsreID { get; set; }
    }

    public class PrgManagersListDtos : FullAuditedEntity
    {
        public List<UserListDto> RRcIncharge { get; set; }

        public List<UserListDto> ProjectManagers { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public long? UsreRoleId { get; set; }
    }
    public class PrgManagersListDtosrrc : FullAuditedEntity
    {
        public List<UserListDto> RRcIncharge { get; set; }

        public List<UserListDto> ProjectManagers { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public long? UsreRoleId { get; set; }
    }


}
