﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.PrgVillageCluster.Dto;

namespace WOTR.PM.PrgVillageCluster
{
    public interface IPrgVillageClusterAppService: IApplicationService
    {
        Task CreatOrUpdateVillageCluster1(PrgVillageClusterListDto Input);
        List<PrgManagersListDto> GetAllProgrameRRcInchargeAndProjectManager(int RRcId);
        Task<List<PrgVillageClusterListDto>> GetAllVillageCluster(int programeId,string input);
        List<PrgVillageClusterListDto> GetAllVillageClusterList(int ProgrameId);
        void Deleteprogramclustor(EntityDto<int> Input);
    }
}
