﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using WOTR.PM.Dashboards.Dto;
using WOTR.PM.NewImpactIndicator.IIForms.Dto;

namespace WOTR.PM.Utils
{
    public static class FillDashboardDatatableDto
    {
        public static void FillStates(AdminDashboardData adminDashboardData)
        {

            try
            {
                adminDashboardData.getAllState = new List<dualListDto>();
                foreach (DataRow row in adminDashboardData.dataTable.Rows)
                {
                    adminDashboardData.getAllState.Add(new dualListDto
                    {
                        id = (row["StateId"] == DBNull.Value) ? 00 : Convert.ToInt32(row["StateId"]),
                        name = (row["StateName"] == DBNull.Value) ? "" : Convert.ToString(row["StateName"])
                    });
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        public static void FillBudgetProjectDetailsByState(AdminDashboardData adminDashboardData)
        {
            try
            {
                adminDashboardData.budgetProjectMembersDetailsByStateDto = new BudgetProjectMembersByStateDto();
                foreach (DataRow row in adminDashboardData.dataTable.Rows)
                {
                    adminDashboardData.budgetProjectMembersDetailsByStateDto = new BudgetProjectMembersByStateDto
                    {
                        OverallTotalProject = (row["OverallTotalProject"] == DBNull.Value) ? "" : Convert.ToString(row["OverallTotalProject"]),
                        TotalProject = (row["TotalProject"] == DBNull.Value) ? "" : Convert.ToString(row["TotalProject"]),
                        TotalMembers = (row["TeamMember"] == DBNull.Value) ? "" : Convert.ToString(row["TeamMember"]),
                    };
                }

               

            }
            catch (Exception)
            {

                throw;
            }
        }
        public static void FillMemberDetailsByState(AdminDashboardData adminDashboardData)
        {
            try
            {
                adminDashboardData.userDetailsDto = new List<UserDetailsDto>();
                foreach (DataRow row in adminDashboardData.dataTable.Rows)
                {
                    adminDashboardData.userDetailsDto.Add( new UserDetailsDto
                    {
                        StaffName = (row["StaffName"] == DBNull.Value) ? "" : Convert.ToString(row["StaffName"]),
                        MobileNo = (row["PhoneNumber"] == DBNull.Value) ? "" : Convert.ToString(row["PhoneNumber"]),
                        EmailAddress = (row["EmailAddress"] == DBNull.Value) ? "" : Convert.ToString(row["EmailAddress"]),
                        Designation = (row["designation"] == DBNull.Value) ? "" : Convert.ToString(row["designation"]),
                        
                    });
                }



            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void FillBudgetAndExpenseByState(AdminDashboardData adminDashboardData)
        {
            try
            {
                adminDashboardData.budgetAndExpenseByStateDto = new List<BudgetAndExpenseByStateDto>();
                foreach (DataRow row in adminDashboardData.dataTable.Rows)
                {
                    adminDashboardData.SumOfTotalBudget = (row["SumOfTotalBudget"] == DBNull.Value) ? "0.00" : Convert.ToString(row["SumOfTotalBudget"]);
                    adminDashboardData.SumOfTotalExpense = (row["SumOfTotalExpense"] == DBNull.Value) ? "0.00" : Convert.ToString(row["SumOfTotalExpense"]);
                    adminDashboardData.budgetAndExpenseByStateDto.Add(new BudgetAndExpenseByStateDto
                    {
                        projectName = (row["Name"] == DBNull.Value) ? "" : Convert.ToString(row["Name"]),
                        TotalBudget = (row["Budget"] == DBNull.Value) ? "0.00" : Convert.ToString(row["Budget"]),
                        TotalExpense = (row["programExpense"] == DBNull.Value) ? "0.00" : Convert.ToString(row["programExpense"]),
                        StateId = (row["StateID"] == DBNull.Value) ? 00 : Convert.ToInt32(row["StateID"])
                    });
                }



            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void FillDemandAndRemainingByState(AdminDashboardData adminDashboardData)
        {
            try
            {
                adminDashboardData.projectDemandAndRemainingByStateDto = new List<ProjectDemandAndRemainingByStateDto>();
                foreach (DataRow row in adminDashboardData.dataTable.Rows)
                {
                    adminDashboardData.projectDemandAndRemainingByStateDto.Add(new ProjectDemandAndRemainingByStateDto
                    {
                        projectName = (row["Name"] == DBNull.Value) ? "" : Convert.ToString(row["Name"]),
                        Demand = (row["programFund"] == DBNull.Value) ? "0.00" : Convert.ToString(row["programFund"]),
                        DemandRemaining = (row["remaining"] == DBNull.Value) ? "0.00" : Convert.ToString(row["remaining"])
                    });
                }



            }
            catch (Exception)
            {

                throw;
            }
        }
        public static void FillDemandSummaryByState(AdminDashboardData adminDashboardData)
        {
            try
            {
                adminDashboardData.demandSummaryByStateDto = new List<DemandSummaryByStateDto>();
                adminDashboardData.TotalDemand = 0;
                foreach (DataRow row in adminDashboardData.dataTable.Rows)
                {
                    adminDashboardData.TotalDemand += (row["Demand"] == DBNull.Value) ? 0.00 : Convert.ToDouble(row["Demand"]);
                    adminDashboardData.demandSummaryByStateDto.Add(new DemandSummaryByStateDto
                    {
                        stateName = (row["StateName"] == DBNull.Value) ? "" : Convert.ToString(row["StateName"]),
                        Demand = (row["Demand"] == DBNull.Value) ? "0.00" : Convert.ToString(row["Demand"]),
                        StateId = (row["Id"] == DBNull.Value) ? 00 : Convert.ToInt32(row["Id"])
                        //,TotalDemand = (row["TotalDemand"] == DBNull.Value) ? "0.00" : Convert.ToString(row["TotalDemand"])
                    });
                }



            }
            catch (Exception)
            {

                throw;
            }
        }
        public static void FillAmountSanctionByState(AdminDashboardData adminDashboardData)
        {
            try
            {
                adminDashboardData.amountSanctionByStateDto = new List<AmountSanctionByStateDto>();
                foreach (DataRow row in adminDashboardData.dataTable.Rows)
                {
                    adminDashboardData.amountSanctionByStateDto.Add(new AmountSanctionByStateDto
                    {
                        Amount = (row["AmountSanction"] == DBNull.Value) ? "" : Convert.ToString(row["AmountSanction"]),
                        SanctionDate = (row["Date"] == DBNull.Value) ? DateTime.Now : Convert.ToDateTime(row["Date"])

                    });
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void FillContributionsByState(AdminDashboardData adminDashboardData)
        {
            try
            {
                adminDashboardData.contributionsByStateDto = new List<ContributionsByStateDto>();
                foreach (DataRow row in adminDashboardData.dataTable.Rows)
                {
                    adminDashboardData.contributionsByStateDto.Add(new ContributionsByStateDto
                    {
                        StateName = (row["StateName"] == DBNull.Value) ? "" : Convert.ToString(row["StateName"]),
                        FunderContribution = (row["FunderContribution"] == DBNull.Value) ? "" : Convert.ToString(row["FunderContribution"]),
                        CommunityContribution = (row["CommunityContribution"] == DBNull.Value) ? "" : Convert.ToString(row["CommunityContribution"]),
                        OtherContribution = (row["OtherContribution"] == DBNull.Value) ? "" : Convert.ToString(row["OtherContribution"]),
                        Budget = (row["Budget"] == DBNull.Value) ? "" : Convert.ToString(row["Budget"]),
                        StateId = (row["StateID"] == DBNull.Value) ? 00 : Convert.ToInt32(row["StateID"])
                    });
                }



            }
            catch (Exception)
            {

                throw;
            }
        }
        public static void FillActionSubActionByState(AdminDashboardData adminDashboardData)
        {
            try
            {
                adminDashboardData.actionSubActionByStateDto = new List<ActionSubActionByStateDto>();
                foreach (DataRow row in adminDashboardData.dataTable.Rows)
                {
                    adminDashboardData.actionSubActionByStateDto.Add(new ActionSubActionByStateDto
                    {
                        ActionName = (row["ActionName"] == DBNull.Value) ? "" : Convert.ToString(row["ActionName"]),
                        Subaction = (row["Subaction"] == DBNull.Value) ? "" : Convert.ToString(row["Subaction"]),
                        FunderContribution = (row["FunderContribution"] == DBNull.Value) ? "" : Convert.ToString(row["FunderContribution"]),
                        CommunityContribution = (row["CommunityContribution"] == DBNull.Value) ? "" : Convert.ToString(row["CommunityContribution"]),
                        OtherContribution = (row["OtherContribution"] == DBNull.Value) ? "" : Convert.ToString(row["OtherContribution"]),
                        ActionAreaId = (row["ActionAreaId"] == DBNull.Value) ? 00 : Convert.ToInt16(row["ActionAreaId"]),
                        SubactionAreaId= (row["subActionId"] == DBNull.Value) ? 00 : Convert.ToInt16(row["subActionId"]),
                        Value = 0,

                    });
                }



            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void FillActionByState(AdminDashboardData adminDashboardData)
        {
            try
            {
                adminDashboardData.actionByStateDto = new List<ActionByStateDto>();
                foreach (DataRow row in adminDashboardData.dataTable.Rows)
                {
                    adminDashboardData.actionByStateDto.Add(new ActionByStateDto
                    {
                        id = (row["ActionAreaId"] == DBNull.Value) ? "" : Convert.ToString(row["ActionAreaId"]),
                        ActionName = (row["ActionName"] == DBNull.Value) ? "" : Convert.ToString(row["ActionName"]),
                        Contribution = (row["TotalContribution"] == DBNull.Value) ? "" : Convert.ToString(row["TotalContribution"]),
                    });
                }



            }
            catch (Exception)
            {

                throw;
            }
        }
        public static void FillRRCAndProgramWiseExpense(AdminDashboardData adminDashboardData)
        {
            try
            {
                adminDashboardData.rRCAndProgramWiseExpenseDto = new List<RRCAndProgramWiseExpenseDto>();
                foreach (DataRow row in adminDashboardData.dataTable.Rows)
                {
                    adminDashboardData.rRCAndProgramWiseExpenseDto.Add(new RRCAndProgramWiseExpenseDto
                    {
                        LocationName = (row["LocationName"] == DBNull.Value) ? "" : Convert.ToString(row["LocationName"]),
                        LocationId = (row["LocationId"] == DBNull.Value) ? 00 : Convert.ToInt32(row["LocationId"]),
                        ActionAreaName = (row["ActionAreaName"] == DBNull.Value) ? "" : Convert.ToString(row["ActionAreaName"]),
                        ActionAreaId = (row["ActionAreaId"] == DBNull.Value) ? 00 : Convert.ToInt32(row["ActionAreaId"]),
                        ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),
                        ProgramId = (row["ProgramId"] == DBNull.Value) ? 00 : Convert.ToInt32(row["ProgramId"]),
                        Amount = (row["Amount"] == DBNull.Value) ? "" : Convert.ToString(row["Amount"]),
                    });
                }



            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
