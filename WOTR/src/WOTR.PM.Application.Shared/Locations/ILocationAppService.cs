﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.Locations.Dto;

namespace WOTR.PM.Locations
{
    public interface ILocationAppService : IApplicationService
    {
        List<string> CreateLocation(LocationListDto Input);
        List<LocationListDto> GetAllLocations();
        void DeleteLocation(EntityDto<int> Input);
        List<LocationListDto> GetAllParentLocations(LocationType input,int StateId);
        List<LocationListDto> GetAllLocationWithUsers(string Input, int user_id);
         List<LocationListDto> GetRespeactedLocation(List<int> pranetLocation, LocationType type);
        List<LocationListDto> GetAllRRcList();
    }
}
