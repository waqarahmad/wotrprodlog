﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Locations.Dto
{
    public class LocationListDto : FullAuditedEntity
    {
        public string UserLocationId { get; set; }
        public long? UserId { get; set; }

        public long? CreatorUserID { get; set; }
        public int? parentId { get; set; }

        public int TenantId { get; set; }
        public string Name { get; set; } //....Location Name
        public int ParentLocation { get; set; }//.....ParentLocation        
        public LocationType LocationType { get; set; }//.....ParentLocation        
        /* public string State { get; set; } *///....Location State
        // public int Id { get; set; }


            //modified Yo
        public int? StateID { get; set; }
        public int? DistrictId { get; set; }
        public int? TalukaId { get; set; }
        public int? VillageId { get; set; }
    }
}
