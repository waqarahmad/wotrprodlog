﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Feedback.Dto
{
    public class WorkItemListDto
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string CreatedDate { get; set; }

        public string ReproDescription { get; set; }
        public string Description { get; set; }
    }

    public class InputFeedbackForm
    {
        public string filter { get; set; }

        public List<IdNameDropdownDto> PriorityIds { get; set; }

        public int SortNo { get; set; }
    }

    public class IdNameDropdownDto {
        public int Id { get; set; }
        public string ItemName { get; set; }

    
    }


}
