﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Feedback.Dto
{
    public class FeedbackDto
    {
        public string Title { get; set; }
        public string FeedbackType { get; set; }
        public string Description { get; set; }
    }
}
