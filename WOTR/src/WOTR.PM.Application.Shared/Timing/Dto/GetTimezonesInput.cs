﻿using Abp.Configuration;

namespace WOTR.PM.Timing.Dto
{
    public class GetTimezonesInput
    {
        public SettingScopes DefaultTimezoneScope { get; set; }
    }
}
