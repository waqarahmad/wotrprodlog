﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Components;
using WOTR.PM.Programs.Dto;

namespace WOTR.PM.Programs
{
 public interface IProgramCompnentsAppService : IApplicationService
 {
        Task<List<string>> CreateOrUpdateProgramCompnents(List<ProgramCompnentsListDto> Input);
        List<ProgramCompnentsListDto> GetAllProgramCompnent();
        List<GetComponentActivityMappingsstDto> GetComponentActivityMapping(List<int> Id);
        List<String> AddComponetToProgramComponetMapping(ProgramComponentMappingListDto Input);
         List<GetAllComponentandActivityMappingDtoList> GetAllComponentandActvity(int projectId);
        List<GetComponentActivityMappingListDto> GetComponentandActivityForProgram(int ProjectId);
        List<ProgrameComponentListDto> GetComponentCostEstimation(int programmeId);
        void deleteProgramComponent(int ComponentId, int programId);
        void AddActivityToProgrameComponentActivityMapping(GetAllComponentandActivityMappingDtoList Input);
    }
}
