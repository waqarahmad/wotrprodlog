﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.PrgVillageCluster.Dto;
using WOTR.PM.Programs.Dto;

namespace WOTR.PM.Programs
{
    public interface IProgarmAppService: IApplicationService
    {
        Task<List<long>> CreateOrUpdateProgram(ProgramInformationListDto Input);
        Task<List<ProgramInformationListDto>> GetAllProgram();
        List<ProgramInformationListDto> GetProgramDetailsbyID(int ProgramId);
        Task<List<Donorwisedto>> GetAllProgramsDetailsDonorWise(string Year);
        List<ProgDto> GetAllProgramsWithUserId(int userId);
        Task DeleteProgram(int input);
        Task<PagedResultDto<ProgDtoList>> GetAllProgramsWithFilter(GetAllProgramsInput input);
        ProgramInformationListDto GetProgramDetailsByProgramID(int input);
        Task<List<PrgVillageClusterListDto>> GetProgramDetailsbyvillageclustor(int ProgramId);
    }
}
