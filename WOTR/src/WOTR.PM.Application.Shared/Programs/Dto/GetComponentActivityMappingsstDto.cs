﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Programs.Dto
{
  public  class GetComponentActivityMappingsstDto : FullAuditedEntity
    {
         public string Name { get; set; } //...Activity Name 
        public string Description { get; set; }//....Activity Description
        public string Createdby { get; set; }
        public long code { get; set; }
        public int? ComponentId { get; set; }
        public int? ActivityId { get; set; }
    }
}
