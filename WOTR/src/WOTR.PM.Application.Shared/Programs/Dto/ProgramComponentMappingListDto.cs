﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Programs.Dto
{
   public class ProgramComponentMappingListDto : FullAuditedEntity
    {
        public int ProgramID { get; set; }
        public List<int> ComponentID { get; set; }
    }
}
