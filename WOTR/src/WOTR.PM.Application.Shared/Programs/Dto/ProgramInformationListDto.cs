﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Programs.Dto
{
    public class ProgramInformationListDto :FullAuditedEntity
    {
        //...Program Table
        public string ProgramName { get; set; }

        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public int? DonorID { get; set; }
        public int? RRCID { get; set; }

        public int TenantId { get; set; }

        public long ProgramSanctionedYear { get; set; }
        public DateTime ProgramStartDate { get; set; }
        public DateTime PrgramEndDate { get; set; }


        //...programmanagaerMapping.
        public List<int> ManagerID { get; set; }
        public int ProgramManagerId { get; set; }      //for single entry
        ////.program Region coverage
        public List< int?> LocationID { get; set; }

        //public int? ProgramID { get; set; }


        public DateTime ProgramSubmitDate { get; set; } //..program submition date .
        public DateTime ProgramApprovaltDate { get; set; } //..program Approval date .
        public decimal? GantnAmountSanctionRupess { get; set; } //..Donor sanction Amount .

        public string TotalCommunityCost { get; set; }
        public string TotalFunderCost { get; set; }
        public string TotalotherCost { get; set; }
        public decimal? AllTotal { get; set; }
        public long AllTotalDonortotalcost { get; set; }


        public string DonorName { get; set; }
        public string ManagerName { get; set;}
        public string ProgramBadgeColor { get; set; }
        public string ProgramBadgeImage { get; set; }
        public DateTime? ExecutionTime { get; set; }
        public string ServiceName { get; set; }
        public string LocationName { get; set; }
        public string ProjectCostGrant { get; set; }
        public string ProjectCostCBO { get; set; }
        public string LocalContribution { get; set; }
        public string GovtContribution { get; set; }
        public string GrandTotal { get; set; }
        public string Units { get; set; }
        public string ActionAreaName { get; set; }
        public string SubactionAreaName { get; set; }

        public string ActionAreaId { get; set; }
        public int? PROGID { get; set; }
        //archita
        public string VillageName { get; set; }
        public string ClusterName { get; set; }
        public string TotalUnits { get; set; }
        public string CostperUnit { get; set; }
        public string RegionName { get; set; }

        public int? ActivityId { get; set; }
        public string ActivityName { get; set; }

        public long? CurrentProgramManagerID { get; set; }

        //archita
    }
    public class Donorwisedto
    {
        public string DonorName{ get; set; }
        public string allTotalDonortotalcost { get; set; }
    }
    public class programnamedto
    {
        public string programname { get; set; }
        public string date { get; set; }
        public int? donorId { get; set; }
        public decimal? GrantAmount { get; set; }
        public DateTime ProgramStartDate { get; set; }
        public DateTime PrgramEndDate { get; set; }


    }
    public class year
    {
        public string date { get; set; }

    }
    public class RegionWiseDetailsList
    {
        public string ProgramName { get; set; }
        public string CompanyName { get; set; }
        public string ActionAreaName { get; set; }

        public string SubactionAreaName { get; set; }
        public string ActivityName { get; set; }

        public decimal ProjectCostGrant { get; set; }
        public decimal ProjectCostCBO { get; set; }
        public decimal LocalContribution { get; set; }
        public decimal GovtContribution { get; set; }
        public decimal GrandTotal { get; set; }
    }
    public class Actionareadto
    {
        public string ActionAreaName { get; set; }
        public List<subactionareadto> subactionareadtos { get; set; }


    }
    public class subactionareadto
    {
        public string SubactionAreaName { get; set; }
        public List<Activitiydto> activitiydtos { get; set; }

    }
    public class Activitiydto
    {
        public string ActivityName { get; set; }

        public decimal ProjectCostGrant { get; set; }
        public decimal ProjectCostCBO { get; set; }
        public decimal LocalContribution { get; set; }
        public decimal GovtContribution { get; set; }
        public decimal GrandTotal { get; set; }
    }
    public class clustorwise
    {
        public string programName { get; set; }
        public string ActivityName { get; set; }
        public string ClustorName { get; set; }
        public int? villageId { get; set; }
        public string VillageName { get; set; }
        public string Activitylevel { get; set; }
        public decimal? TotalUnits { get; set; }
        public decimal? TotalUnitCost { get; set; }
        public decimal? UnitCost { get; set; }
        public decimal? CommunityContribution { get; set; }
        public decimal? FunderContribution { get; set; }

        public decimal? OtherContribution { get; set; }

        public decimal? Total { get; set; }



    }
}
