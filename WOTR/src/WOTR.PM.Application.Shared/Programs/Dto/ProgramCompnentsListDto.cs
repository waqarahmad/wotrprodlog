﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Programs.Dto
{
  public   class ProgramCompnentsListDto: FullAuditedEntity
    {
        public int ProgramID { get; set; }
        public int ComponentID { get; set; }
        public string componentName { get; set; }
        public List<int> ComponentActivityID { get; set; }

        //public List<int?> ImpactIndicatorID { get; set; }
        //public List<int?> ImpactIndicatorctivitiesID { get; set; }
    }
}
