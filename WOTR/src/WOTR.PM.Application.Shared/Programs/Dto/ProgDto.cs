﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.Authorization.Users.Dto;

namespace WOTR.PM.Programs.Dto
{
    public class ProgDto : FullAuditedEntityDto
    {
        public string Name { get; set; }

        public string ManagerName { get; set; }

        public List<UserListDto> Users { get; set; }
    }
}
