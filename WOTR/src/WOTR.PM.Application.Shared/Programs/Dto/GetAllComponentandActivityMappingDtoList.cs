﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Programs.Dto
{
   public  class GetAllComponentandActivityMappingDtoList : FullAuditedEntity
    {
        public int ProgramID { get; set; }
        public int ComponentID { get; set; }
        public string ComponentName { get; set; }

        public int ActivityID { get; set; }
        public string ActivityName { get; set; }
        public string Descri { get; set; }

        public string CreateBy { get; set; }
        public DateTime CreateTime { get; set; }

    }
}
