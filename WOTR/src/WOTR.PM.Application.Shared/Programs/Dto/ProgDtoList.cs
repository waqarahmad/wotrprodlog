﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.Dto;

namespace WOTR.PM.Programs.Dto
{
    public class GetAllProgramsInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Name";
            }
        }
    }

    public class ProgDtoList : FullAuditedEntityDto
    {
        public string Name { get; set; }
    }
}
