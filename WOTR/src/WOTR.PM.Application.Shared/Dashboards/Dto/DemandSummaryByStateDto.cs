﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Dashboards.Dto
{
    public class DemandSummaryByStateDto
    {
        public string stateName { get; set; }

        public string Demand { get; set; }

        public int StateId { get; set; }        
        
    }
}
