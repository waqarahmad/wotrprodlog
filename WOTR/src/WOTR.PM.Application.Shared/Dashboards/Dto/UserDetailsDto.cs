﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Dashboards.Dto
{
    public class UserDetailsDto
    {
        public string StaffName { get; set; }

        public string MobileNo { get; set; }

        public string EmailAddress { get; set; }

        public string Designation { get; set; }

    }
}
