﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Dashboards.Dto
{
    public class BudgetAndExpenseByStateDto
    {

        public int StateId { get; set; }
        public string projectName { get; set; }

        public string TotalBudget { get; set; }

        public string TotalExpense { get; set; }    

    }
}
