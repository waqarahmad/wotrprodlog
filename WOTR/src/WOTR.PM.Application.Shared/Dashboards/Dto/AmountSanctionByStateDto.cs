﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Dashboards.Dto
{
    public class AmountSanctionByStateDto
    {
        public string Amount { get; set; }

        public DateTime? SanctionDate { get; set; }

    }
}
