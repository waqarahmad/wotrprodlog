﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Dashboards.Dto
{
    public class ContributionsByStateDto
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
        public string FunderContribution { get; set; }
        public string CommunityContribution { get; set; }
        public string OtherContribution { get; set; }

        public string Budget { get; set; }
    }
}
