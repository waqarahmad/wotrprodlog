﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Dashboards.Dto
{
    public class ActionByStateDto
    {
        public string id { get; set; }

        public string ActionName { get; set; }

        public string Contribution { get; set; }
    }
}
