﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Dashboards.Dto
{
    public class RRCAndProgramWiseExpenseDto
    {

        public string LocationName { get; set; }
        public int LocationId { get; set; }
        public string ActionAreaName { get; set; }

        public int ActionAreaId { get; set; }
        public string ProgramName { get; set; }
        public int ProgramId { get; set; }
        public string Amount { get; set; }
    }
}
