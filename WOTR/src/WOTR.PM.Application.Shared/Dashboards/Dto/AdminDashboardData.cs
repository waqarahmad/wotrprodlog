﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using WOTR.PM.NewImpactIndicator.IIForms.Dto;

namespace WOTR.PM.Dashboards.Dto
{
    public class AdminDashboardData
    {
        public DataTable dataTable { get; set; }

        public DataSet dataSet { get; set; }
        public List<dualListDto> getAllState { get; set; }

        public BudgetProjectMembersByStateDto budgetProjectMembersDetailsByStateDto { get; set; }

        public List<UserDetailsDto> userDetailsDto { get; set; }



        public string SumOfTotalBudget { get; set; }
        public string SumOfTotalExpense { get; set; }
        public List<BudgetAndExpenseByStateDto> budgetAndExpenseByStateDto { get; set; }



        public List<ProjectDemandAndRemainingByStateDto> projectDemandAndRemainingByStateDto { get; set; }


        public double TotalDemand { get; set; }
        public List<DemandSummaryByStateDto> demandSummaryByStateDto { get; set; }
        public List<AmountSanctionByStateDto> amountSanctionByStateDto { get; set; }
        public List<ContributionsByStateDto> contributionsByStateDto { get; set; }
        public List<ActionSubActionByStateDto> actionSubActionByStateDto { get; set; }
        public List<ActionByStateDto> actionByStateDto { get; set; }
        public List<RRCAndProgramWiseExpenseDto> rRCAndProgramWiseExpenseDto { get; set; }



        

    }
}
