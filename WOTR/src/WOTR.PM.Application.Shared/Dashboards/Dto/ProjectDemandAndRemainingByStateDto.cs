﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Dashboards.Dto
{
    public class ProjectDemandAndRemainingByStateDto
    {
        public string projectName { get; set; }

        public string Demand { get; set; }

        public string DemandRemaining { get; set; }
    }
}
