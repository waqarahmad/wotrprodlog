﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Dashboards.Dto
{
    public class BudgetProjectMembersByStateDto
    {
        public string OverallTotalProject { get; set; }

        public string TotalProject { get; set; }

        public string TotalMembers { get; set; }
    }


}
