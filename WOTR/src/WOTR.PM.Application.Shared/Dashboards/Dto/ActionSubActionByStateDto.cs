﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Dashboards.Dto
{
    public class ActionSubActionByStateDto
    {
        public string ActionName { get; set; }

        public int ActionAreaId { get; set; }

        public double Value { get; set; }
        public int SubactionAreaId { get; set; }
        public string Subaction { get; set; }
        public string FunderContribution { get; set; }
        public string CommunityContribution { get; set; }
        public string OtherContribution { get; set; }

    }
}
