﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.FreqOfOccurences.Dto
{
    public class GetAllFreqOfOccurrence : FullAuditedEntity
    {
        public string FreqOfOccurrence { get; set; }
        public int TenantId { get; set; }
    }
}
