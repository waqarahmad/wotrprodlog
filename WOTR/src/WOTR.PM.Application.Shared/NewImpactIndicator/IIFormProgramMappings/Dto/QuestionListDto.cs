﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.IIFormProgramMappings.Dto
{
    public class QuestionListDto : FullAuditedEntity
    {
        public string Question { get; set; }

        public string Category { get; set; }

        public string SubCategory { get; set; }

    }
}
