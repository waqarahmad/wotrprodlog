﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.IIFormProgramMappings.Dto
{
    public class GetAllProgramAndFormMapping : FullAuditedEntity
    {
        public int? impactIndicatorFormId { get; set; }
        public int? programId { get; set; }
        public string programName { get; set; }
        public string FormName { get; set; }
        public string Status { get; set; }

    }
}
