﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.NewImpactIndicator.Sources.Dto;

namespace WOTR.PM.NewImpactIndicator.Sources
{
    public interface ISourceAppService: IApplicationService
    {
        List<GetAllSource> GetAllSource(string filter);

        Task Delete(EntityDto input);
    }
}
