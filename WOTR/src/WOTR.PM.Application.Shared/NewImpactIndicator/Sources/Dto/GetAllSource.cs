﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.Sources.Dto
{
    public class GetAllSource : FullAuditedEntity
    {

        public string SourceName { get; set; }
        public int TenantId { get; set; }
    }
}
