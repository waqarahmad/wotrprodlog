﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.IIResponses.Dto
{
    public class GetAllResponseReportDto : FullAuditedEntity
    {
         public string FreqOfOccurrence { get; set; }
         public List<GetAllAnswersReportDto> answerDetails { get; set; }
    }

    public class GetAllAnswersReportDto : FullAuditedEntity
    {
        public string Village { get; set; }
        public string Category { get; set; }
        public string Subcategory { get; set; }
        public string Question { get; set; }
        public string Crop { get; set; }
        public string Answer { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime DateOfCollection { get; set; }

    }


}
