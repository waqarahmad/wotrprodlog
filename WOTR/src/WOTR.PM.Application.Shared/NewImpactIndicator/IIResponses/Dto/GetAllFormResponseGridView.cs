﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.IIResponses.Dto
{
    public class GetAllFormResponseGridView
    {
        public long IIFormResponseId { get; set; }

        public int ProgramId { get; set; }

        public string Name { get; set; }

        public int villageId { get; set; }
        public string Village { get; set; } //....Location Name

        public long DataFillPersonId { get; set; }

        public string UserName { get; set; }


    }
}
