﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.IIResponses.Dto
{
    public class ProjectReportInputDto
    {
        public int ProgramId { get; set; }

        public List<int> VillageIds { get; set; }

        public int FormId { get; set;  }
    }
}
