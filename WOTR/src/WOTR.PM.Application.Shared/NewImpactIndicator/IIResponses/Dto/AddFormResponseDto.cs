﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.IIResponses.Dto
{
    public class AddFormResponseDto : FullAuditedEntity, IMustHaveTenant
    {
        public int ProgramId { get; set; }

        public int villageId { get; set; }

        public long DataFillPersonId { get; set; }

        public int impactIndicatorFormId { get; set; }

        public int TenantId { get; set; }

        public List<FrequencyOccurenceDto> FrequencyOccurenceList { get; set; }

    }

}
