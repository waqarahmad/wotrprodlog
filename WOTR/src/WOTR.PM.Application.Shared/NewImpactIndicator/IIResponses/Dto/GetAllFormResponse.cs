﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.IIResponses.Dto
{
    public class GetAllFormResponse : FullAuditedEntity
    {
        public int ProgramId { get; set; }
        public string Name { get; set; }

        public int villageId { get; set; }
        public string village { get; set; }

        public long DataFillPersonId { get; set; }
        public string FullName { get; set; }



    }
}
