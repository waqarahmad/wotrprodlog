﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.IIResponses.Dto
{
    public class GetAllFormResponseDetails : FullAuditedEntity
    {
        public long IIFormResponseId { get; set; }

        public List<GetAllFrequencyOfOccurrence> FrequencyOfOccurance { get; set; }
    }

    public class GetAllFrequencyOfOccurrence : FullAuditedEntity
    {
        public int FreqOfOccurrenceId { get; set; }

        public string FreqOfOccurrence { get; set; }

        public string DateOfEntryStr { get; set; }

        public string DateOfCollectionStr { get; set; }

        public DateTime DateOfCollection { get; set; }

        public DateTime DateOfEntry { get; set; }

        public List<GetAllCategoryFromForm> CategoryList { get; set; }
    }

    public class GetAllCategoryFromForm : FullAuditedEntity
    {
        public int CategoryId { get; set; }
        public string Category { get; set; }

        public List<GetAllSubCategoryFromForm> SubCategoryList { get; set; }
    }
    public class GetAllSubCategoryFromForm : FullAuditedEntity
    {
        public int SubCategoryId { get; set; }

        public string SubCategory { get; set; }        

        public List<GetQuestionType> QuestionTypesList { get; set; }
    }

    public class GetQuestionType
    {
        public int Type { get; set; }

        public DateTime DateOfEntry { get; set; }

        public DateTime DateOfCollection { get; set; }

        public int SourceId { get; set; }

        public int? CropId { get; set; }
        public List<GetAllQuestionsFromForm> QuestionList { get; set; }
    }



    public class GetAllQuestionsFromForm : FullAuditedEntity<long>
    {
        public int IIQuestionaryId { get; set; }

        public long IIFormResponseId { get; set; }

        public int SourceId { get; set; }
        public string Question { get; set; }

        public string QuestionType { get; set; }        

        public string Answer { get; set; }        

    }
}
