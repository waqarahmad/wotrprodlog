﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.IIResponses.Dto
{

    public class FrequencyOccurenceDto
    {
        public int FrequencyOfOccurrenceId { get; set; }

        public string FrequencyOfOccurrenceName { get; set; }

        public DateTime EntryDate { get; set; }

        public DateTime CollectedDate { get; set; }

        public List<GetAllCategoryFromForm> CategoryList { get; set; }

        //public List<AddFormResponseDetailsDto> AddFormResponseDetailsDtoList { get; set; }
    }
    public class AddFormResponseDetailsDto
    {
        public int Id { get; set; }
        public DateTime DateOfEntry { get; set; }

        public DateTime DateOfCollection { get; set; }

        public int FrequencyOfOccurrenceId { get; set; }

        public int IIQuestionaryId { get; set; }

        public string Answer { get; set; }

        public int SourceId { get; set; }
    }
}
