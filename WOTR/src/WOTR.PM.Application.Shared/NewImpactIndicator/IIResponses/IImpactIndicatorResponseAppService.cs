﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Users.Dto;
using WOTR.PM.Dto;
using WOTR.PM.NewImpactIndicator.IIResponses.Dto;

namespace WOTR.PM.NewImpactIndicator.IIResponses
{
    public interface IImpactIndicatorResponseAppService: IApplicationService
    {
        Task<AddFormResponseDto> GetResponseById(EntityDto input);

        Task<PagedResultDto<GetAllFormResponseGridView>> GetResponseForGrid(PagedSortedAndFilteredInputDto input);

        Task<PagedResultDto<UserListDto>> GetProgramUsers(int? programId);

        Task<string> DeleteFormAnswer(long input);
    }
}
