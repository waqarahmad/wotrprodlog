﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.SubCategory.Dto
{
    public class GetAllSubCategory : Entity<int>
    {
        public string SubCategory { get; set; }

        public string Category { get; set; }

        public int ImpactIndicatorCategoryId { get; set; }
    }
}
