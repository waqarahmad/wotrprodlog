﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.NewImpactIndicator.SubCategory.Dto;

namespace WOTR.PM.NewImpactIndicator.SubCategory
{
    public interface ISubCategoryAppService : IApplicationService
    {
        List<GetAllSubCategory> GetAllImapactIndiactorSubCategory(string filter);
    }
}
