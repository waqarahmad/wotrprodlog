﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.NewImpactIndicator;

namespace WOTR.PM.NewImpactIndicator.Questionaries.Dto
{
    public class GetAllQuestionary : FullAuditedEntity
    {
        public string Question { get; set; }

        public string Description { get; set; }

        public string QuestionType { get; set; }

        public string QuestionStatus { get; set; }

        public int frequencyOfOccurrenceId { get; set; }

        public int impactIndicatorCategoryId { get; set; }

        public int impactIndicatorSubCategoryId { get; set; }

        public string impactIndicatorCategoryName { get; set; }

        public string impactIndicatorSubCategoryName { get; set; }
        public string frequencyOfOccurrence { get; set; }
    }
}
