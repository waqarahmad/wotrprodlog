﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.Questionaries.Dto
{
    public class GetAllQuestionByIdsInputDto
    {
        public int CategoryId { get; set; }
        public int SubCategoryId { get; set; }
    }
}
