﻿using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.Dto;

namespace WOTR.PM.NewImpactIndicator.Questionaries.Dto
{
    public class GetAllQuestionaryInput : PagedAndSortedInputDto
    {
        public string filter { get; set; }
    }
}
