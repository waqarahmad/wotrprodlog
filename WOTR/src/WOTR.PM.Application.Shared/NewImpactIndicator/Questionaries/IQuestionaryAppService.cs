﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.NewImpactIndicator.IIForms.Dto;
using WOTR.PM.NewImpactIndicator.Questionaries.Dto;

namespace WOTR.PM.NewImpactIndicator.Questionaries
{
    public interface IQuestionaryAppService : IApplicationService
    {

        List<GetAllQuestionary> GetAllQuestionary(string input);


        string CreateOrEdit(AddEditQuestionaryDto input);

        Task Delete(EntityDto input);

        List<dualListDto> FetchAllQuetionByCateAndSubCat(GetAllQuestionByIdsInputDto input);

    }
}
