﻿using Abp.Domain.Entities.Auditing;

namespace WOTR.PM.NewImpactIndicator.IIForms.Dto
{
    public class GetAllImpactIndicatorIForms : FullAuditedEntity
    {
        public string FormName { get; set; }

        public string FormStatus { get; set; }
    }
}
