﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.IIForms.Dto
{
    public class AddEditImpactIndicatorForm : FullAuditedEntity
    {
        public string FormName { get; set; }

        public string FormStatus { get; set; }

        public List<string> QuestionList { get; set; }


    }
}
