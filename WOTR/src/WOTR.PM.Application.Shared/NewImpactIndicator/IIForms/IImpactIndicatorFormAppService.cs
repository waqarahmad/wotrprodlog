﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.NewImpactIndicator.IIForms.Dto;

namespace WOTR.PM.NewImpactIndicator.IIForms
{
    public interface IImpactIndicatorFormAppService : IApplicationService
    {
        List<GetAllImpactIndicatorIForms> GetAllForm(string input);

         Task<string> createEditImapactIndiactorForm(AddEditImpactIndicatorForm Input);

    }
}
