﻿using System;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.NewImpactIndicator.Crops.Dto;
using WOTR.PM.NewImpactIndicator.Crops;

namespace WOTR.PM.NewImpactIndicator.Crops
{
    public interface ICropAppService : IApplicationService
    {
        List<GetAllCrops> GetAllCrop(string filter);

      //  string CreatOrUpdateCrop(Crop Input);
    }
}
