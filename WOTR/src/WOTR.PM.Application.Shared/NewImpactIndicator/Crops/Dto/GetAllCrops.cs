﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.Dto;

namespace WOTR.PM.NewImpactIndicator.Crops.Dto
{
    public class GetAllCrops :  FullAuditedEntity
    {

        public string Name { get; set; }
        public int TenantId { get; set; }
    }
}
