﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.NewImpactIndicator.Category.Dto;

namespace WOTR.PM.NewImpactIndicator.Category
{
    public interface ICategoryAppService : IApplicationService
    {
        List<GetAllCategory> GetAllImapactIndiactorCategory(string filter);

       // string CreateEditImapactIndiactorCategory(ImpactIndicatorCategory input);
    }
}
