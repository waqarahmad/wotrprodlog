﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.NewImpactIndicator.Category.Dto
{
    public class GetAllCategory : Entity<int>
    {
        public string Category { get; set; }
    }
}
