﻿using WOTR.PM.Dto;

namespace WOTR.PM.Common.Dto
{
    public class FindUsersInput : PagedAndFilteredInputDto
    {
        public int? TenantId { get; set; }
    }
}