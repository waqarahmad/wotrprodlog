﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.LoginAndroid.Dto;

namespace WOTR.PM.LoginAndroid
{
    public interface IPushNotifiactionAppService
    {
        string SendPushNotification(string userId, string message, string title);
        //  string CreateOrUpadteAppInfoDetails(AppInfo Input);
        Task<string> CreateOrUpadteAppDeviceListsDetails(AppDeviceListsDto Input);
    }
}
