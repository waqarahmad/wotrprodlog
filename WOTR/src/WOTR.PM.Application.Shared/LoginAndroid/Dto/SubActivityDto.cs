﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.LoginAndroid.Dto
{
    public class SubActivityDto : FullAuditedEntity
    {
        public string SubActivityName { get; set; }
        public int prgActionAreaID { get; set; }
        public int? ActivityId { get; set; }
    }

}
