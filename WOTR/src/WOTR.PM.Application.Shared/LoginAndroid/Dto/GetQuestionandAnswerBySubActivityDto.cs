﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using WOTR.PM.PrgImplementationPlans;

namespace WOTR.PM.LoginAndroid.Dto
{
    public  class GetQuestionandAnswerBySubActivityDto
    {
        public string QuestionaryName { get; set; }
        public int QuestionaryId { get; set; }
        public string InputType { get; set; }
       
        public string answer { get; set; }
       
        public int QuestionarymappngId { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public prgImplementationPlanQuestionariesMappingStatus Status { get; set; }

        public List<Url1> url { get; set; }


    }
    
       
       


    public class Url1
    {

        public string url { get; set; }
        public int urlTableId { get; set; }
    }
   
}
