﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.LoginAndroid.Dto
{
    public class SubAcitvityListDto : FullAuditedEntity
    {
        public string SubActivityname { get; set; }
        // public int Id { get; set; }
        public string Unitofmeasure { get; set; }
        public decimal? unit { get; set; }
        public DateTime startDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public int ManagerId { get; set; }
        public string ManagerName  { get;set;}
        public int? ProgramID { get; set; }
        public string  CostEstimationYear { get; set; }
        public int? QuarterID { get; set; }
        public int? PrgActionAreaActivityMappingID { get; set; }



    }
}
