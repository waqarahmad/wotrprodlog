﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.PrgImplementationPlans;

namespace WOTR.PM.LoginAndroid.Dto
{
    public class TaskDto : FullAuditedEntity
    {
        public DateTime startDate { get; set; }
        public DateTime EndDate{get;set;}
        public string Description { get; set; }
        public string SubActivityName { get; set; }
        public SubActivityStatus status { get; set; }
    }
}
