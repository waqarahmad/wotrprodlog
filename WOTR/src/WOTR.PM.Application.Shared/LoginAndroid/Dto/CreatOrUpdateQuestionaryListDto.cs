﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.LoginAndroid.Dto
{
  public  class CreatOrUpdateQuestionaryListDto: FullAuditedEntity
    {
        public int? PrgImplementationsPlansID { get; set; }
        public int ProgramId { get; set; }
        public int TenantId { get; set; }
        public List<Questionary> Questionary { get; set; }
        public List<Url> url { get; set; }

    }
    public class Questionary
    {
        public string QuestionaryName { get; set; }
        public int QuestionaryId { get; set; }
        public string answer { get; set; }
        public int QuestionarymappngId { get; set; }
        public string InputType { get; set; }

    }
    public class Url
    {
       
        public string url { get; set; }
        public int urlTableId { get; set; }
        public int? questionryId { get; set; }
    }
}
