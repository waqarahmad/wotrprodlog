﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.LoginAndroid.Dto
{
   public  class RegisterOutputDto
    {
        public string AppId { get; set; }
        public string SenderId { get; set; }
    }
}
