﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.LoginAndroid.Dto
{
  public   class GetExpenseInput
    {
        public virtual int TenantId { get; set; }
        public int ProgramID { get; set; }
        public int? PrgActionAreaActivityMappingID { get; set; }
        public int QuarterId { get; set; }
        public int? ProgramQuqterUnitMappingID { get; set; }
        public int ImplementationPlanCheckListID { get; set; }
        public long CreatorUserId { get; set; }
    }
}
