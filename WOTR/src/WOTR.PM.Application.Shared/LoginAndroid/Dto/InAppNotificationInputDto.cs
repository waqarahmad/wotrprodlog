﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.LoginAndroid.Dto
{
  public  class InAppNotificationInputDto
    {
        public string applicationId { get; set; }
        public string senderId { get; set; }
        public string deviceId { get; set; }
        public string messages { get; set; }
        public string title { get; set; }
        public int userId { get; set; }
       
    }
}
