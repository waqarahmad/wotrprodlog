﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.LoginAndroid.Dto
{
   public class AppDeviceListsDto
    {
        public int TenantId { get; set; }
      //  public int Id { get; set; }
        
        public string DeviceToken { get; set; }
       
        public string DeviceUniqueId { get; set; }

        public int UserId { get; set; }

        public long ? CreatorUserId { get; set; }



    }
}
