﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.LoginAndroid.Dto
{
   public  class ProjectDetilsDto :FullAuditedEntity
    {
        


        //...Program Table
        public string ProgramName { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public int? DonorID { get; set; }
        public string prgimage { get; set; }

        public long ProgramSanctionedYear { get; set; }
        public DateTime ProgramStartDate { get; set; }
        public DateTime PrgramEndDate { get; set; }


        //...programmanagaerMapping.
        public List<int> ManagerID { get; set; }
        ////.program Region coverage
        public List<int?> LocationID { get; set; }

        //public int? ProgramID { get; set; }


        public DateTime ProgramSubmitDate { get; set; } //..program submition date .
        public DateTime ProgramApprovaltDate { get; set; } //..program Approval date .
        public decimal? GantnAmountSanctionRupess { get; set; } //..Donor sanction Amount .


        public string DonorName { get; set; }
        public string ManagerName { get; set; }

    }
}
