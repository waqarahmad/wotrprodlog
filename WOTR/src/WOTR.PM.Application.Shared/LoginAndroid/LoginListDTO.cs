﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.LoginAndroid
{
    public class LoginListDTO : FullAuditedEntity
    {
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public string EmailAddress { get; set; }
        public Boolean isActive { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public string PhoneNumber { get; set; }
        public int? TenantId { get; set; }
        public string UserName { get; set; }
        public string Address { get; set; }
        public int UserRole { get; set; }

    }
}
