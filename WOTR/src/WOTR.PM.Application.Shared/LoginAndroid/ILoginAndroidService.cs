﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Components;
using WOTR.PM.LoginAndroid.Dto;
using WOTR.PM.PrgImplementationPlans.Dto;
using WOTR.PM.ProgramExpenses.Dto;
using WOTR.PM.UnitOfMeasures.Dtos;

namespace WOTR.PM.LoginAndroid
{
    public interface ILoginAndroidService : IApplicationService
    {

        List<ProjectDetilsDto> GetProjectDetailsForAndroid(int userId);
        List<TaskDto> GetAllTask(int userID);
        String UpdateStatusOfSubActivityForAndroid(int ImplementationPlanCheckListID, int status);
        List<ExpensesTypeDto> GetAllExpensesTypeforAndroid(int TenantId);
        List<loginUserDetailsDto> GetLoginInfoforAndroid(string mobilenumber, string name, int TenantId);
        List<UnitofMeasureDto> GetAllUnitOfMeasureforAndroid(int TenantId);

        Task<List<SubActivityForAndri>> GetSubActivityForAndri(int userId, int TenantId, int programId,
            int PrgActionAreaActivityMappingID, string role);

        List<SubActivityDto> GetActivityByprogramIdForAndroidUser(int programId, int userId);
        string CreateORUpdateExpensesForAndroid(ExpenseDto Input);

        List<ExpenseDto> GetExpenseDetails(GetExpenseInput Input);
        string UpdateSubActivityUnits(int ImplementationPlanCheckListID, decimal AchieveUnit);
        List<ImplementationListDto> GetManagerprojectforAndroid(long? userId);
    }
}
