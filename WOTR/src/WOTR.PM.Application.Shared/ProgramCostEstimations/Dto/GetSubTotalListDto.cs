﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.ProgramCostEstimations.Dto
{
   public  class GetSubTotalListDto : FullAuditedEntity
    {
        public int ProgramID { get; set; }
        public int? ComponentID { get; set; }

        public decimal? SubTotalTotalunits { get; set; }
        public decimal? SubTotalUnitCost { get; set; }
        public decimal? SubTotalofTotalCost { get; set; }
        public decimal? SubTotalofCommunityContribution { get; set; }
        public decimal? SubTotalofFunderContribution { get; set; }
        public string CostEstimationYear { get; set; }
    }
}
