﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.ProgramCostEstimations.Dto
{
   public  class ProgramCostEstimationListDto : FullAuditedEntity
    {

        //public List<int?> ComponentID { get; set; }
        //public List<int?> ActivityID { get; set; }


        public  int TenantId { get; set; }
        public int ProgramID { get; set; }
        public int? ComponentID { get; set; }
        public int ActivityID { get; set; }
        public int? UnitOfMeasuresID { get; set; }
        public long TotalUnits { get; set; }
        public decimal UnitCost { get; set; }
        public decimal TotalUnitCost { get; set; }
        public decimal CommunityContribution { get; set; }
        public decimal FunderContribution { get; set; }
        public decimal OtherContribution { get; set; }

       
    }
}
