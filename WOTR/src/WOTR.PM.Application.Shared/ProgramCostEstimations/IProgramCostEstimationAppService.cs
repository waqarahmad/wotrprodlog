﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.Components;
using WOTR.PM.ProgramCostEstimations.Dto;

namespace WOTR.PM.ProgramCostEstimations
{
   public  interface IProgramCostEstimationAppService : IApplicationService
    {
        List<string> CreateOrUpdateProgramCostEstimation(List<ProgrameComponentListDto> Input);
        void DeleteActivityCostDetails(EntityDto<int> Input);
        //List<string> CreateOrUpdateSubTotal(GetComponentActivityMappingListDto Input);
       // List<string> CreateOrUpdateTotal(GetComponentActivityMappingListDto Input);
    }
}
