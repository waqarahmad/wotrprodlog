﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.ImpactIndicators.Dto
{
   public  class ImpactIndicatorsListDto: FullAuditedEntity
    {
        public string Name { get; set; }
    }
}
