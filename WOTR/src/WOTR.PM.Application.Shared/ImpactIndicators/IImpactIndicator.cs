﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ImpactIndicators.Dto;

namespace WOTR.PM.ImpactIndicators
{
   public  interface IImpactIndicator : IApplicationService
    {
        List<string> creatImpacatIndicator(ImpactIndicatorsListDto Input);
        List<ImpactIndicatorsListDto> GetAll();
        Task Delete(EntityDto<int> input); 
    }
}
