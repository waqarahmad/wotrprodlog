﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ImpactIndicators.Dto;

namespace WOTR.PM.ImpactIndicators
{
   public interface IImpactIndicatorActivity : IApplicationService
    {
       
        List<string> creatImpacatIndicatorActivity(ImpactIndicatorActivityListDto Input);
        Task Delete(EntityDto<int> input);
        List<ImpactIndicatorActivityListDto> GetAll();
    }
}
