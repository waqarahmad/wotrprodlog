using System.Collections.Generic;
using Abp.Application.Services.Dto;
using WOTR.PM.Editions.Dto;

namespace WOTR.PM.MultiTenancy.Dto
{
    public class GetTenantFeaturesEditOutput
    {
        public List<NameValueDto> FeatureValues { get; set; }

        public List<FlatFeatureDto> Features { get; set; }
    }
}