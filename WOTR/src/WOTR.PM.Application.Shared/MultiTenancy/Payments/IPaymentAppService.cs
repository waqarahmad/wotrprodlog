﻿using System.Threading.Tasks;
using Abp.Application.Services;
using WOTR.PM.MultiTenancy.Dto;
using WOTR.PM.MultiTenancy.Payments.Dto;
using Abp.Application.Services.Dto;

namespace WOTR.PM.MultiTenancy.Payments
{
    public interface IPaymentAppService : IApplicationService
    {
        Task<PaymentInfoDto> GetPaymentInfo(PaymentInfoInput input);

        Task<CreatePaymentResponse> CreatePayment(CreatePaymentDto input);

        Task<ExecutePaymentResponse> ExecutePayment(ExecutePaymentDto input);

        Task<PagedResultDto<SubscriptionPaymentListDto>> GetPaymentHistory(GetPaymentHistoryInput input);
    }
}
