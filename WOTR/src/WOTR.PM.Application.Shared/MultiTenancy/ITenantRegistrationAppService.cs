using System.Threading.Tasks;
using Abp.Application.Services;
using WOTR.PM.Editions.Dto;
using WOTR.PM.MultiTenancy.Dto;

namespace WOTR.PM.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}