﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ProgramRegionSpendings.Dto;

namespace WOTR.PM.ProgramRegionSpendings
{
    public interface IProgramRegionSpendingAppServices : IApplicationService
    {
        Task<List<ProgramRegionSpendingListDto>> GetProgramRegionSpendingDeatails(int ProgramID);
        Task<List<string>> CreateOrUpdateProgramRegionSpending(List<ProgramRegionSpendingListDto> Input);
        Task Delete(EntityDto Input);
    }
}
