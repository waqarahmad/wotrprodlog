﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.Donors.Dto;

namespace WOTR.PM.Donors
{
   public interface IDonerAppService :IApplicationService
    {
        List<string> CreateOrUpdateDoner(DonorListDto Input);
        List<DonorListDto> GetAllDonor();
        List<DonorListDto> GetAllDonorsearch(string Input);

        
        List<CountryListDto> GetAllCountry();
    }
}
