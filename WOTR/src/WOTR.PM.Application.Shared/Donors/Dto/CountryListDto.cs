﻿
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WOTR.PM.Donors.Dto
{
    
    public class CountryListDto :FullAuditedEntity
    {
        public int id { get; set; }
        public string CountryName { get; set; }
    }
}
