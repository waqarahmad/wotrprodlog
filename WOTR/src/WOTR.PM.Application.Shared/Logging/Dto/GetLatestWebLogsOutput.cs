﻿using System.Collections.Generic;

namespace WOTR.PM.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatestWebLogLines { get; set; }
    }
}
