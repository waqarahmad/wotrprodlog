﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Logging.Dto
{
   public  class ProjectDetailsForAndroidUserDto :FullAuditedEntity
    {
        public string ProjectName { get; set; }
        public int ProjectId { get; set; }
    }
}
