﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Logging.Dto
{
  public   class GetAllProgramDetailsForAndroidUser:FullAuditedEntity
    {
        public int ProgramId { get; set; }
        public  string ProgramName { get; set; }

       
    }
}
