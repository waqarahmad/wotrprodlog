﻿using Abp.Application.Services;
using WOTR.PM.Dto;
using WOTR.PM.Logging.Dto;

namespace WOTR.PM.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
