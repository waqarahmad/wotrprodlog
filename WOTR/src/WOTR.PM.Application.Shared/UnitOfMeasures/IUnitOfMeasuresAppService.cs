using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using WOTR.PM.UnitOfMeasures.Dtos;
using System.Collections.Generic;

namespace WOTR.PM.UnitOfMeasures
{
    public interface IUnitOfMeasuresAppService : IApplicationService 
    {
        Task<PagedResultDto<UnitofMeasureDto>> GetAll(GetAllUnitofMeasuresInput input);

		Task<CreateOrEditUnitofMeasureDto> GetUnitofMeasureForEdit(EntityDto<int> input);

		Task CreateOrEdit(CreateOrEditUnitofMeasureDto input);

		string Delete(EntityDto<int> Input);

        List<UnitofMeasureDto> GetAllUnitOfMeasure();
    }
}