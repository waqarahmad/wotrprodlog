using System;
using Abp.Application.Services.Dto;

namespace WOTR.PM.UnitOfMeasures.Dtos
{
    public class UnitofMeasureDto : EntityDto
    {
        public string Name { get; set; }
    }
}