using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace WOTR.PM.UnitOfMeasures.Dtos
{
    public class CreateOrEditUnitofMeasureDto : FullAuditedEntityDto<int?>
    {
        [Required]
        public string Name { get; set; }
    }
}