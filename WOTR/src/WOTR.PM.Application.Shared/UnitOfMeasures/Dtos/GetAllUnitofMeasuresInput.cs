using Abp.Application.Services.Dto;

namespace WOTR.PM.UnitOfMeasures.Dtos
{
    public class GetAllUnitofMeasuresInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}