﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.Activities.Dto;

namespace WOTR.PM.Activities
{
    public interface IActivityAppService : IApplicationService
    {
        List<string> CreateActivity(ActivitiyListDto Input);
        List<ActivitiyListDto> GetAllActivities(string input);
        string DeleteActivity(EntityDto<int> Input);
    }
}
