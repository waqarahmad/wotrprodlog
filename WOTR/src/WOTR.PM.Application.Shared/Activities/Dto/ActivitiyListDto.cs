﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Activities.Dto
{
    public class ActivitiyListDto : FullAuditedEntity
    {
        public virtual int TenantId { get; set; }
        public string Name { get; set; } //...Activity Name 
        public string Description { get; set; }//....Activity Description
        public string FirstName { get; set; }
        public int? UnitOfMeasuresId { get; set; }
    }
}
