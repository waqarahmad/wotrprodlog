using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using WOTR.PM.Auditing.Dto;
using WOTR.PM.Dto;

namespace WOTR.PM.Auditing
{
    public interface IAuditLogAppService : IApplicationService
    {
        Task<PagedResultDto<AuditLogListDto>> GetAuditLogs(GetAuditLogsInput input);

        Task<FileDto> GetAuditLogsToExcel(GetAuditLogsInput input);
    }
}