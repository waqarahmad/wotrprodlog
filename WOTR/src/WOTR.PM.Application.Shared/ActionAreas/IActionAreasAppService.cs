﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.ActionAreas.Dto;

namespace WOTR.PM.ActionAreas
{
    public interface IActionAreasAppService : IApplicationService
    {
        List<string> CreatOrUpdateActionArea(ActionAreasListDto Input);
        List<ActionAreasListDto> GetAllActionAreaRecord();
        List<ActionAreasListDto> Getsearch(string input);
        string DeleteActoinArea(EntityDto<int> Input);
      //  List<ActionAreasListDto> GetActionAreaBySearch(string Input);

    }
}
