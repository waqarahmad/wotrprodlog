﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.ActionAreas.Dto
{
    public class SubActionAreaListDto : FullAuditedEntity
    {
        public int TenantId { get; set; }
        public string Name { get; set; } //...Name of SubAction Area
        public string Code { get; set; }//....SubAction area code which is user defined
        public int? ActionAreaId { get; set; }//..Foreign Key Action Area
        public string Filter { get; set; }
        public string FullName { get; set; }
    }
}
