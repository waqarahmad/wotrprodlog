﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.ActionAreas.Dto
{
    public class ActionAreasListDto : FullAuditedEntity
    {
        public virtual int TenantId { get; set; }
        public string Name { get; set; }

        public string Code { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Filter { get; set; }

    }
   
}
