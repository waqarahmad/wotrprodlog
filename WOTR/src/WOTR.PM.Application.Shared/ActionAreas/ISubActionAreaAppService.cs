﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.ActionAreas.Dto;

namespace WOTR.PM.ActionAreas
{
    public interface ISubActionAreaAppService : IApplicationService
    {
        List<string> CreatOrUpdateSubActionArea(SubActionAreaListDto Input);
        List<SubActionAreaListDto> GetAllSubActionAreaRecord();
        List<SubActionAreaListDto> Getsearch(string input);
        List<SubActionAreaListDto> GetSubactionaginsActionarea(int actionAreaId);
        string DeleteSubactionArea(EntityDto<int> Input);
    }
}
