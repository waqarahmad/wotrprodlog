﻿using Abp.Application.Services;
using System.Threading.Tasks;
using WOTR.PM.PrgRequestAAPlas.Dto;
using WOTR.PM.Tenants.Dashboard.Dto;

namespace WOTR.PM.Tenants.Dashboard
{
    public interface ITenantDashboardAppService : IApplicationService
    {
        GetMemberActivityOutput GetMemberActivity();

        GetDashboardDataOutput GetDashboardData(GetDashboardDataInput input);

        GetSalesSummaryOutput GetSalesSummary(GetSalesSummaryInput input);

        GetWorldMapOutput GetWorldMap(GetWorldMapInput input);

        GetGeneralStatsOutput GetGeneralStats(GetGeneralStatsInput input);

        Task<AllChartDto> RRCInchargebudgetOne();
    }
}
