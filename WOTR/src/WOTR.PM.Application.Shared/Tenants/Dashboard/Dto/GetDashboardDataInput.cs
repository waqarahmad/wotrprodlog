namespace WOTR.PM.Tenants.Dashboard.Dto
{
    public class GetDashboardDataInput
    {
        public SalesSummaryDatePeriod SalesSummaryDatePeriod { get; set; }
    }
}