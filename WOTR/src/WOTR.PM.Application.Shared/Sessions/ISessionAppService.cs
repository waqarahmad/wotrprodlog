﻿using System.Threading.Tasks;
using Abp.Application.Services;
using WOTR.PM.Sessions.Dto;

namespace WOTR.PM.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken();
    }
}
