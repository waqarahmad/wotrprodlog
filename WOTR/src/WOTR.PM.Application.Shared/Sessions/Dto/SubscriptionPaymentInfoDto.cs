﻿namespace WOTR.PM.Sessions.Dto
{
    public class SubscriptionPaymentInfoDto
    {
        public decimal Amount { get; set; }
    }
}