﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Components
{
    public interface IComponentActivityMappingAppService : IApplicationService
    {
        List<string> CreateComponentActivityMapping(ComponentActivityMapListDto Input);
        List<ComponentActivityMapListDto> GetAllComponentActivityMapping();
        void DeleteComponentActivityMapping(EntityDto<int> Input);
        List<GetComponentActivityMappingListDto> GetAllComponentandActivitymappingforcomponenttable();
    }
}
