﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Components
{
    public class ComponentListDto : FullAuditedEntity
    {
        public int TenantId { get; set; }
        public string Name { get; set; } //...Name of Component
        public long Code { get; set; }//....Component Code

    }
}
