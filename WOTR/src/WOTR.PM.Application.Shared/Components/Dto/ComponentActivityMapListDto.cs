﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Components
{
    public class ComponentActivityMapListDto : FullAuditedEntity
    {
        public int TenantId { get; set; }
        public int ComponentID { get; set; } //...Component ID
        public int ActivityID { get; set; }//....Activity ID
        public List<int> ActID { get; set; }
        public string componentName { get;set;}
        public int ComponentCode { get; set; }

    }
}
