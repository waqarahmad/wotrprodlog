﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Components
{
    public interface IComponentAppService : IApplicationService
    {
        List<string> CreateComponent(GetComponentActivityMappingListDto Input);
        List<ComponentListDto> GetAllComponents( );
        string DeleteComponent(EntityDto<int> Input);
    }
}
