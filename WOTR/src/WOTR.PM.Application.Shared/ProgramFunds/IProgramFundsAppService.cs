﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.PrgImplementationPlans.Dto;
using WOTR.PM.ProgramFunds.Dto;

namespace WOTR.PM.ProgramFunds
{
    public interface IProgramFundsAppService: IApplicationService
    {
        List<ProgramFundsListDto> GetProjectFunds(int ProgramId);
        Task<List<string>> CreateProjectFund(List<ListActionSubactionListImplemenTationDto1> Input);
        List<ProgramFundsListDto> GetProjectFundsForProgramManager();
        List<ProgramFundsListDto> GetProjectFundsForAccountant();
        List<string> UpdateProjectFundStatus(int id, Status1 prgstatus);
        List<ProgramFundsListDto> GetProjectFundDetailForResult(string input, int ProgramID);
        Task<List<ProgramFundsListDto>> GetProjectFundsForAdmin();
        Task<List<string>> UpdateProjectFundStatus1(int id, Status1 prgstatus);
    }
}
