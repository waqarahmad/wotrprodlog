using Abp.Application.Services.Dto;

namespace WOTR.PM.ChekLists.Dtos
{
    public class GetAllChekListsInput : PagedAndSortedResultRequestDto
    {
		public string Filter { get; set; }
    }
}