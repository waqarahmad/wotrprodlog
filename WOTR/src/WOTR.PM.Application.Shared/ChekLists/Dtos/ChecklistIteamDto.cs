﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.ChekLists.Dtos
{
   public  class ChecklistIteamDto : FullAuditedEntity
    {
        public string Name { get; set; }
        public int? ChecklistID { get; set; }
        public bool? IsChecked { get; set; }
        public int TenantId { get; set; }
    }
}
