using System;
using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;

namespace WOTR.PM.ChekLists.Dtos
{
    public class CreateOrEditChekListDto : FullAuditedEntityDto<int?>
    {
        [Required]
        public string ChecklistName { get; set; }
        public string Description { get; set; }

        public int? Rating { get; set; }
        public virtual int TenantId { get; set; }
        public int ActivityID { get; set; }
        public string ActivityName { get; set; }

        public List<cheklistandSublistDto> iteam { get; set; }
    }

    public class cheklistandSublistDto : FullAuditedEntity
    {
        public string CheklisiteamtName { get; set; }
        public int? checklistID_subiteamTable { get; set; }
        public List<QuestListDto> Ques { get; set; }
    }

    public class SubActivityQuesMapDto: FullAuditedEntity
    {
        public int? SubActivityId { get; set; }
        public List<int?> QuestionaryIds { get; set; }
    }

    public class QuestListDto : FullAuditedEntity
    {
        public string Name { get; set; }
        public string InputType { get; set; }
        public int questionmappingid { get; set; }
    }
}