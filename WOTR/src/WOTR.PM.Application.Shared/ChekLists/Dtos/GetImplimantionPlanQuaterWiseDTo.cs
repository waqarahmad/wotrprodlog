﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.ChekLists.Dtos
{
  public   class GetImplimantionPlanQuaterWiseDTo
    {
       public int programId { get; set; }
        public int locationID { get; set; }
        public int prgActionAreaActivityMappingID { get; set; }
        public string year { get; set; }
        public  int QuqterID { get; set; }
        
    }
}
