using System;
using Abp.Application.Services.Dto;

namespace WOTR.PM.ChekLists.Dtos
{
    public class ChekListDto : EntityDto
    {
        public string checklistName { get; set; }

        public int Id { get; set; }
    }
}