using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using WOTR.PM.ChekLists.Dtos;
using System.Collections.Generic;

namespace WOTR.PM.ChekLists
{
    public interface IChekListsAppService : IApplicationService
    {
        Task<List<CreateOrEditChekListDto>> GetAll(string input);

        Task<CreateOrEditChekListDto> GetChekListForEdit(EntityDto<int> input);

        Task CreateOrEdit(CreateOrEditChekListDto input);

        string Delete(EntityDto<int> input);
        void CreateOrUpdateQuestionary(QuestListDto input);
        void AddSubactQuesMap(SubActivityQuesMapDto input);
        List<cheklistandSublistDto> GetAllSubActivityWithQuestions();
        void CreateOreditCheklistandsubiteam(List<cheklistandSublistDto> input, int n);

    }
}