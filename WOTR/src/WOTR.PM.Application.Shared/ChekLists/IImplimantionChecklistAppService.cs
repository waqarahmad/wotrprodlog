﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ChekLists.Dtos;
using WOTR.PM.PrgImplementationPlans.Dto;

namespace WOTR.PM.ChekLists
{
   public  interface IImplimantionChecklistAppService : IApplicationService
   {
        Task<List<string>> CreateOrEditImplementionCheckList(ImplimentaionChecklistDto input);
        Task<List<ImplementationListDto>> GetImplementionPlan(int programId);
        List<ImplimentaionChecklistDto> GetimplimentaionPlanQuaterWise(GetImplimantionPlanQuaterWiseDTo input);
        Task MapSubActivityToImplimentionPlan(SubActivityandIteams input);
        List<cheklistandSublistDto1> GetMapsubActivityForImplentionPlan(int PrgActionAreaActivityMappingID);
        List<ListActionSubactionListImplemenTationDto> GetQuarterWisePlan(int ProgramId, int QuarterId, int PrgActionMapId, int? villageId);
        List<subUnitActivityQuarterWise> GetSubActivityUnitWise(int unitID);
        decimal? GetRemaingBalance(int PrgActionAreaActivityMapping, int Program);
        int RemainingSubActivitiesMap(int? prgActionMapId, int? unitId);
        List<QuestionaryListDto> GetQuestionaryBySubActivity(int? subActivityId);
        //Task<List<ImplementationListDto>> GetImplementionPlanForReport(int programId);
   }
}
