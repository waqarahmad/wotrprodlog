﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Authorization.Users.Dto;
using WOTR.PM.PrgImplementationPlans.Dto;
using WOTR.PM.PrgRequestAAPlas.Dto;
using WOTR.PM.Programs.Dto;

namespace WOTR.PM.PrgRequestAAPlas
{
    public interface IPrgRequestAAPlasAppService : IApplicationService
    {
        Task<List<RequestAAPlasListDto>> GetRequestApplaDeatils(int ProgramId);
        Task<ProgDto> CreatOrUpdateRequestAppl(ProgrameCostEstimationYearRequestApplDto Input);
        Task<List<RequestAAPlasListDto>> GetReQuestApplaForProjectManager(int ProgramId);
        Task CreatePrgUnitMapping(ListActionSubactionListImplemenTationDto input);
       // Task<List<ProgramSummary>> GetProgramSummary(EntityDto input);
        Task<List<RequestAAPlasListDto>> GetRequestApplaDeatilsForActionArea(int ProgramId);
        //Task<List<ProgramSummary>> GetProgramExpenseSummary(EntityDto input);
         
    }
}
