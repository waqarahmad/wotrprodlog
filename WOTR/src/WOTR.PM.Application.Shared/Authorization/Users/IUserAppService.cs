﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using WOTR.PM.Authorization.Roles.Dto;
using WOTR.PM.Authorization.Users.Dto;
using WOTR.PM.Dto;

namespace WOTR.PM.Authorization.Users
{
    public interface IUserAppService : IApplicationService
    {
        Task<PagedResultDto<UserListDto>> GetUsers(GetUsersInput input);

        Task<FileDto> GetUsersToExcel();

        Task<GetUserForEditOutput> GetUserForEdit(NullableIdDto<long> input);

        Task<GetUserPermissionsForEditOutput> GetUserPermissionsForEdit(EntityDto<long> input);

        Task ResetUserSpecificPermissions(EntityDto<long> input);

        Task UpdateUserPermissions(UpdateUserPermissionsInput input);

        Task CreateOrUpdateUser(CreateOrUpdateUserInput input);

        Task DeleteUser(EntityDto<long> input);

        Task UnlockUser(EntityDto<long> input);

         List<GetAllManagerDto> GetAllManager();

        bool ConfirmUserRole(int user_id);

        List<RoleListDto> GetUserRoles(string userName);

        Task userRoleById(int id, string userNameOrEmailAddress);
    }
}