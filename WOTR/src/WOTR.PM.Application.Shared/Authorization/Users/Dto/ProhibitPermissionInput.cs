﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Authorization.Users.Dto
{
    public class ProhibitPermissionInput
    {
        public long UserId { get; set; }
        public string Permission { get; set; }
    }
}
