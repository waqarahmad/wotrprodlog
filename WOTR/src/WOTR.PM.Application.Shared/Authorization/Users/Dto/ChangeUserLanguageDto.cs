﻿using System.ComponentModel.DataAnnotations;

namespace WOTR.PM.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
