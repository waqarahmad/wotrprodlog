﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Authorization.Users.Dto
{
    public class GetAllManagerDto
    {
        public string Name { get; set; }
        public long ManagerId { get; set; }
        public string ManaggerLocation { get; set; }
    }
}
