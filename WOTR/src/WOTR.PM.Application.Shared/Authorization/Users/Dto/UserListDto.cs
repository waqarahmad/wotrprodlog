﻿using System;
using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace WOTR.PM.Authorization.Users.Dto
{
    public class UserListDto : EntityDto<long>, IPassivable, IHasCreationTime
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public Guid? ProfilePictureId { get; set; }

        public bool IsEmailConfirmed { get; set; }

        public List<UserListRoleDto> Roles { get; set; }

        public DateTime? LastLoginTime { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreationTime { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Category CategoryId { get; set; }

        public int? LocationId { get; set; }

        public int? ManagerId { get; set; }

        public string Address { get; set; }

        public string RoleName { get; set; }

        public int? ActionId { get; set; }

        public string ActionName { get; set; }

        public long? UserRoleId { get; set; }
    }
}