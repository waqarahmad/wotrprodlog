﻿using System.Collections.Generic;
using WOTR.PM.Authorization.Permissions.Dto;

namespace WOTR.PM.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}