﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.Authorization.Users.Dto
{
    public class GetUserDetailsForMailDto
    {
        public string Name { get; set; }

        public string EmailId { get; set; }
    }
}
