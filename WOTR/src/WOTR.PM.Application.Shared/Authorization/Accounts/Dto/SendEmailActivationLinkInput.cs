﻿using System.ComponentModel.DataAnnotations;

namespace WOTR.PM.Authorization.Accounts.Dto
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}