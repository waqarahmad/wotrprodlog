﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using WOTR.PM.Authorization.Permissions.Dto;

namespace WOTR.PM.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
