﻿using System.Threading.Tasks;
using Abp.Application.Services;
using WOTR.PM.Configuration.Tenants.Dto;

namespace WOTR.PM.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
