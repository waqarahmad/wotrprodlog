﻿using Abp.Notifications;
using WOTR.PM.Dto;

namespace WOTR.PM.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}