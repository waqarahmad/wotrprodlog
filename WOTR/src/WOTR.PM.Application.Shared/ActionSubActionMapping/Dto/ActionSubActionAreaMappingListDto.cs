﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace WOTR.PM.ActionSubActionMapping.Dto
{
    public class ActionSubActionAreaMappingListDto: FullAuditedEntity
    {
       
        public virtual int TenantId { get; set; }

        public int? ActionAreaID { get; set; }         

        public int? SubActionAreaID { get; set; }       
      
        public List<int> ActivityID { get; set; }

        public string FirstName { get; set; }

        public string ActionAreaName { get; set; }

        public string SubActionAreaName { get; set; }

        public string ActivityName { get; set; }
        
    }
}
