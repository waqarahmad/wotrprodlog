﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using WOTR.PM.ActionSubActionMapping.Dto;

namespace WOTR.PM.ActionSubActionMapping
{
    public interface IActionSubActionMappingAppService: IApplicationService
    {
        List<string> CreatOrUpdateActionSubActionMappingArea(ActionSubActionAreaMappingListDto Input);
        void DeleteActionSubactionMapping(EntityDto<int> Input);
    }
}
