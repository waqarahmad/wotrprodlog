using Microsoft.AspNetCore.Mvc;
using WOTR.PM.Web.Controllers;

namespace WOTR.PM.Web.Public.Controllers
{
    public class HomeController : PMControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}