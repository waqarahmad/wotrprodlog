﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace WOTR.PM.Web.Public.Views
{
    public abstract class PMViewComponent : AbpViewComponent
    {
        protected PMViewComponent()
        {
            LocalizationSourceName = PMConsts.LocalizationSourceName;
        }
    }
}