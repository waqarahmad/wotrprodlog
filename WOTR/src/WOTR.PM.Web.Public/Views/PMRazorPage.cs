﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace WOTR.PM.Web.Public.Views
{
    public abstract class PMRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected PMRazorPage()
        {
            LocalizationSourceName = PMConsts.LocalizationSourceName;
        }
    }
}
