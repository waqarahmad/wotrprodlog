﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace WOTR.PM
{
    [DependsOn(typeof(PMXamarinSharedModule))]
    public class PMXamarinAndroidModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PMXamarinAndroidModule).GetAssembly());
        }
    }
}