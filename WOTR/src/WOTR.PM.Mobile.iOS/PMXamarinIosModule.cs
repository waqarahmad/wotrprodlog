﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace WOTR.PM
{
    [DependsOn(typeof(PMXamarinSharedModule))]
    public class PMXamarinIosModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PMXamarinIosModule).GetAssembly());
        }
    }
}