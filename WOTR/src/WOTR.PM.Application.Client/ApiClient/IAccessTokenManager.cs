﻿using System.Threading.Tasks;
using WOTR.PM.ApiClient.Models;

namespace WOTR.PM.ApiClient
{
    public interface IAccessTokenManager
    {
        Task<string> GetAccessTokenAsync();
         
        Task<AbpAuthenticateResultModel> LoginAsync();

        void Logout();

        bool IsUserLoggedIn { get; }
    }
}