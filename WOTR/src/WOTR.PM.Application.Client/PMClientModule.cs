﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace WOTR.PM
{
    public class PMClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PMClientModule).GetAssembly());
        }
    }
}
