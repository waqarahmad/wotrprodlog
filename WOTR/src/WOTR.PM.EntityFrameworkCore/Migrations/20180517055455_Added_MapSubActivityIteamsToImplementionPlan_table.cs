﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Added_MapSubActivityIteamsToImplementionPlan_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MapSubActivityIteamsToImplementionPlan",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChecklistID = table.Column<int>(nullable: true),
                    ChecklistItemID = table.Column<int>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    PrgActionAreaActivityMappingID = table.Column<int>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MapSubActivityIteamsToImplementionPlan", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MapSubActivityIteamsToImplementionPlan_Checklist_ChecklistID",
                        column: x => x.ChecklistID,
                        principalTable: "Checklist",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MapSubActivityIteamsToImplementionPlan_ChecklistItem_ChecklistItemID",
                        column: x => x.ChecklistItemID,
                        principalTable: "ChecklistItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MapSubActivityIteamsToImplementionPlan_PrgActionAreaActivityMapping_PrgActionAreaActivityMappingID",
                        column: x => x.PrgActionAreaActivityMappingID,
                        principalTable: "PrgActionAreaActivityMapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MapSubActivityIteamsToImplementionPlan_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MapSubActivityIteamsToImplementionPlan_ChecklistID",
                table: "MapSubActivityIteamsToImplementionPlan",
                column: "ChecklistID");

            migrationBuilder.CreateIndex(
                name: "IX_MapSubActivityIteamsToImplementionPlan_ChecklistItemID",
                table: "MapSubActivityIteamsToImplementionPlan",
                column: "ChecklistItemID");

            migrationBuilder.CreateIndex(
                name: "IX_MapSubActivityIteamsToImplementionPlan_PrgActionAreaActivityMappingID",
                table: "MapSubActivityIteamsToImplementionPlan",
                column: "PrgActionAreaActivityMappingID");

            migrationBuilder.CreateIndex(
                name: "IX_MapSubActivityIteamsToImplementionPlan_ProgramID",
                table: "MapSubActivityIteamsToImplementionPlan",
                column: "ProgramID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MapSubActivityIteamsToImplementionPlan");
        }
    }
}
