﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_managerId_colum_programExpenses_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ManagerID",
                table: "ProgramExpense",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_ProgramExpense_ManagerID",
                table: "ProgramExpense",
                column: "ManagerID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramExpense_AbpUsers_ManagerID",
                table: "ProgramExpense",
                column: "ManagerID",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProgramExpense_AbpUsers_ManagerID",
                table: "ProgramExpense");

            migrationBuilder.DropIndex(
                name: "IX_ProgramExpense_ManagerID",
                table: "ProgramExpense");

            migrationBuilder.DropColumn(
                name: "ManagerID",
                table: "ProgramExpense");
        }
    }
}
