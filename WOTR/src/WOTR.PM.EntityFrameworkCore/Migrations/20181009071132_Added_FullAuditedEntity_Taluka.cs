﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Added_FullAuditedEntity_Taluka : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "Taluka",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "Taluka",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "Taluka",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "Taluka",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Taluka",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Taluka",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "Taluka",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Taluka",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "Taluka");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "Taluka");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "Taluka");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "Taluka");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Taluka");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Taluka");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "Taluka");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Taluka");
        }
    }
}
