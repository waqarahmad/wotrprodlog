﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Alter_column_UserId_ForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_VillageCluster_UserId",
                table: "VillageCluster",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_VillageCluster_AbpUsers_UserId",
                table: "VillageCluster",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VillageCluster_AbpUsers_UserId",
                table: "VillageCluster");

            migrationBuilder.DropIndex(
                name: "IX_VillageCluster_UserId",
                table: "VillageCluster");
        }
    }
}
