﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Modify_ImpactIndicatorSubcatory_To_ImpactIndicatorSubcatoryId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IIQuestionary_IISubCategory_ImpactIndicatorSubCategory",
                table: "IIQuestionary");

            migrationBuilder.RenameColumn(
                name: "ImpactIndicatorSubCategory",
                table: "IIQuestionary",
                newName: "ImpactIndicatorSubCategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_IIQuestionary_ImpactIndicatorSubCategory",
                table: "IIQuestionary",
                newName: "IX_IIQuestionary_ImpactIndicatorSubCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_IIQuestionary_IISubCategory_ImpactIndicatorSubCategoryId",
                table: "IIQuestionary",
                column: "ImpactIndicatorSubCategoryId",
                principalTable: "IISubCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IIQuestionary_IISubCategory_ImpactIndicatorSubCategoryId",
                table: "IIQuestionary");

            migrationBuilder.RenameColumn(
                name: "ImpactIndicatorSubCategoryId",
                table: "IIQuestionary",
                newName: "ImpactIndicatorSubCategory");

            migrationBuilder.RenameIndex(
                name: "IX_IIQuestionary_ImpactIndicatorSubCategoryId",
                table: "IIQuestionary",
                newName: "IX_IIQuestionary_ImpactIndicatorSubCategory");

            migrationBuilder.AddForeignKey(
                name: "FK_IIQuestionary_IISubCategory_ImpactIndicatorSubCategory",
                table: "IIQuestionary",
                column: "ImpactIndicatorSubCategory",
                principalTable: "IISubCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
