﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class aaded_prgImplementationPlanQuestionariesUrlMapping_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "prgImplementationPlanQuestionariesUrlMapping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    PrgImplementationsPlansID = table.Column<int>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_prgImplementationPlanQuestionariesUrlMapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_prgImplementationPlanQuestionariesUrlMapping_PrgImplementationsPlans_PrgImplementationsPlansID",
                        column: x => x.PrgImplementationsPlansID,
                        principalTable: "PrgImplementationsPlans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_prgImplementationPlanQuestionariesUrlMapping_PrgImplementationsPlansID",
                table: "prgImplementationPlanQuestionariesUrlMapping",
                column: "PrgImplementationsPlansID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "prgImplementationPlanQuestionariesUrlMapping");
        }
    }
}
