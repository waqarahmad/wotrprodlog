﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Remove_ProgramcomponentForeignKey_ProgramComponentActivityMapping_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProgramComponentsActivitesMapping_ProgramComponentsMapping_ProgramComponentsMappingID",
                table: "ProgramComponentsActivitesMapping");

            migrationBuilder.DropIndex(
                name: "IX_ProgramComponentsActivitesMapping_ProgramComponentsMappingID",
                table: "ProgramComponentsActivitesMapping");

            migrationBuilder.DropColumn(
                name: "ProgramComponentsMappingID",
                table: "ProgramComponentsActivitesMapping");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProgramComponentsMappingID",
                table: "ProgramComponentsActivitesMapping",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProgramComponentsActivitesMapping_ProgramComponentsMappingID",
                table: "ProgramComponentsActivitesMapping",
                column: "ProgramComponentsMappingID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramComponentsActivitesMapping_ProgramComponentsMapping_ProgramComponentsMappingID",
                table: "ProgramComponentsActivitesMapping",
                column: "ProgramComponentsMappingID",
                principalTable: "ProgramComponentsMapping",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
