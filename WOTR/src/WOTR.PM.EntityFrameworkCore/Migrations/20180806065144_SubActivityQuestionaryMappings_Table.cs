﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class SubActivityQuestionaryMappings_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SubActivityQuestionaryMapping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityID = table.Column<int>(nullable: true),
                    ChecklistItemID = table.Column<int>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    QuestionarieID = table.Column<int>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubActivityQuestionaryMapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubActivityQuestionaryMapping_Activity_ActivityID",
                        column: x => x.ActivityID,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubActivityQuestionaryMapping_ChecklistItem_ChecklistItemID",
                        column: x => x.ChecklistItemID,
                        principalTable: "ChecklistItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubActivityQuestionaryMapping_Questionarie_QuestionarieID",
                        column: x => x.QuestionarieID,
                        principalTable: "Questionarie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubActivityQuestionaryMapping_ActivityID",
                table: "SubActivityQuestionaryMapping",
                column: "ActivityID");

            migrationBuilder.CreateIndex(
                name: "IX_SubActivityQuestionaryMapping_ChecklistItemID",
                table: "SubActivityQuestionaryMapping",
                column: "ChecklistItemID");

            migrationBuilder.CreateIndex(
                name: "IX_SubActivityQuestionaryMapping_QuestionarieID",
                table: "SubActivityQuestionaryMapping",
                column: "QuestionarieID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubActivityQuestionaryMapping");
        }
    }
}
