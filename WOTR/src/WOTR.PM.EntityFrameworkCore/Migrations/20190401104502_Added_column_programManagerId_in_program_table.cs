﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Added_column_programManagerId_in_program_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ProgramManagerID",
                table: "Program",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Program_ProgramManagerID",
                table: "Program",
                column: "ProgramManagerID");

            migrationBuilder.AddForeignKey(
                name: "FK_Program_AbpUsers_ProgramManagerID",
                table: "Program",
                column: "ProgramManagerID",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Program_AbpUsers_ProgramManagerID",
                table: "Program");

            migrationBuilder.DropIndex(
                name: "IX_Program_ProgramManagerID",
                table: "Program");

            migrationBuilder.DropColumn(
                name: "ProgramManagerID",
                table: "Program");
        }
    }
}
