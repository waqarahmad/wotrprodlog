﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class add_column_senderID_and_ApplicationID_inAppDevicelist_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApplicationId",
                table: "AppDeviceLists",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SenderId",
                table: "AppDeviceLists",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApplicationId",
                table: "AppDeviceLists");

            migrationBuilder.DropColumn(
                name: "SenderId",
                table: "AppDeviceLists");
        }
    }
}
