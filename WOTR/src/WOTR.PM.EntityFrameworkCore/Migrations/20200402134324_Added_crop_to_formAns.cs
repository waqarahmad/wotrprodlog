﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Added_crop_to_formAns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CropId",
                table: "IIFormAnswers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "QuestionType",
                table: "IIFormAnswers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_IIFormAnswers_CropId",
                table: "IIFormAnswers",
                column: "CropId");

            migrationBuilder.AddForeignKey(
                name: "FK_IIFormAnswers_Crop_CropId",
                table: "IIFormAnswers",
                column: "CropId",
                principalTable: "Crop",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IIFormAnswers_Crop_CropId",
                table: "IIFormAnswers");

            migrationBuilder.DropIndex(
                name: "IX_IIFormAnswers_CropId",
                table: "IIFormAnswers");

            migrationBuilder.DropColumn(
                name: "CropId",
                table: "IIFormAnswers");

            migrationBuilder.DropColumn(
                name: "QuestionType",
                table: "IIFormAnswers");
        }
    }
}
