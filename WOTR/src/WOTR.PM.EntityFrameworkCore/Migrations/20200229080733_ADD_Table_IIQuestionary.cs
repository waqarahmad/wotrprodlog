﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class ADD_Table_IIQuestionary : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IIQuestionary",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    ImpactIndicatorCategoryId = table.Column<int>(nullable: true),
                    ImpactIndicatorSubCategory = table.Column<int>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    Question = table.Column<string>(nullable: true),
                    QuestionStatus = table.Column<int>(nullable: false),
                    QuestionType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IIQuestionary", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IIQuestionary_IICategory_ImpactIndicatorCategoryId",
                        column: x => x.ImpactIndicatorCategoryId,
                        principalTable: "IICategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_IIQuestionary_IISubCategory_ImpactIndicatorSubCategory",
                        column: x => x.ImpactIndicatorSubCategory,
                        principalTable: "IISubCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IIQuestionary_ImpactIndicatorCategoryId",
                table: "IIQuestionary",
                column: "ImpactIndicatorCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_IIQuestionary_ImpactIndicatorSubCategory",
                table: "IIQuestionary",
                column: "ImpactIndicatorSubCategory");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IIQuestionary");
        }
    }
}
