﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_ProgramRegionSpending_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProgramRegionSpending",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityID = table.Column<int>(nullable: true),
                    CommunityContribution = table.Column<decimal>(nullable: true),
                    ComponentID = table.Column<int>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Grant = table.Column<decimal>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    LocationID = table.Column<int>(nullable: true),
                    OtherContribution = table.Column<decimal>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    TotalUnitCost = table.Column<decimal>(nullable: true),
                    TotalUnits = table.Column<long>(nullable: true),
                    UnitCost = table.Column<decimal>(nullable: true),
                    UnitOfMeasuresID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramRegionSpending", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProgramRegionSpending_Activity_ActivityID",
                        column: x => x.ActivityID,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramRegionSpending_Component_ComponentID",
                        column: x => x.ComponentID,
                        principalTable: "Component",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramRegionSpending_Location_LocationID",
                        column: x => x.LocationID,
                        principalTable: "Location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramRegionSpending_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProgramRegionSpending_UnitOfMeasure_UnitOfMeasuresID",
                        column: x => x.UnitOfMeasuresID,
                        principalTable: "UnitOfMeasure",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProgramRegionSpending_ActivityID",
                table: "ProgramRegionSpending",
                column: "ActivityID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramRegionSpending_ComponentID",
                table: "ProgramRegionSpending",
                column: "ComponentID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramRegionSpending_LocationID",
                table: "ProgramRegionSpending",
                column: "LocationID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramRegionSpending_ProgramID",
                table: "ProgramRegionSpending",
                column: "ProgramID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramRegionSpending_UnitOfMeasuresID",
                table: "ProgramRegionSpending",
                column: "UnitOfMeasuresID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProgramRegionSpending");
        }
    }
}
