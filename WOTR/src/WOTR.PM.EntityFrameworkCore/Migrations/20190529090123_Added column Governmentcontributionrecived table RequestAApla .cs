﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class AddedcolumnGovernmentcontributionrecivedtableRequestAApla : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProgramRegionSpending_Program_ProgramID",
                table: "ProgramRegionSpending");

            migrationBuilder.AddColumn<decimal>(
                name: "GovernmentContributionRecived",
                table: "RequestAAplas",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ProgramID",
                table: "ProgramRegionSpending",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramRegionSpending_Program_ProgramID",
                table: "ProgramRegionSpending",
                column: "ProgramID",
                principalTable: "Program",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProgramRegionSpending_Program_ProgramID",
                table: "ProgramRegionSpending");

            migrationBuilder.DropColumn(
                name: "GovernmentContributionRecived",
                table: "RequestAAplas");

            migrationBuilder.AlterColumn<int>(
                name: "ProgramID",
                table: "ProgramRegionSpending",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramRegionSpending_Program_ProgramID",
                table: "ProgramRegionSpending",
                column: "ProgramID",
                principalTable: "Program",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
