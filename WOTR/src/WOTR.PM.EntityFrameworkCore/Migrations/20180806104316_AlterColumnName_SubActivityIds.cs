﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class AlterColumnName_SubActivityIds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubActivityQuestionaryMapping_ChecklistItem_ChecklistItemID",
                table: "SubActivityQuestionaryMapping");

            migrationBuilder.RenameColumn(
                name: "ChecklistItemID",
                table: "SubActivityQuestionaryMapping",
                newName: "SubActivityId");

            migrationBuilder.RenameIndex(
                name: "IX_SubActivityQuestionaryMapping_ChecklistItemID",
                table: "SubActivityQuestionaryMapping",
                newName: "IX_SubActivityQuestionaryMapping_SubActivityId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubActivityQuestionaryMapping_SubActivity_SubActivityId",
                table: "SubActivityQuestionaryMapping",
                column: "SubActivityId",
                principalTable: "SubActivity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubActivityQuestionaryMapping_SubActivity_SubActivityId",
                table: "SubActivityQuestionaryMapping");

            migrationBuilder.RenameColumn(
                name: "SubActivityId",
                table: "SubActivityQuestionaryMapping",
                newName: "ChecklistItemID");

            migrationBuilder.RenameIndex(
                name: "IX_SubActivityQuestionaryMapping_SubActivityId",
                table: "SubActivityQuestionaryMapping",
                newName: "IX_SubActivityQuestionaryMapping_ChecklistItemID");

            migrationBuilder.AddForeignKey(
                name: "FK_SubActivityQuestionaryMapping_ChecklistItem_ChecklistItemID",
                table: "SubActivityQuestionaryMapping",
                column: "ChecklistItemID",
                principalTable: "ChecklistItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
