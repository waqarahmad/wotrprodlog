﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Added_column_locationId_projectFund : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LocationID",
                table: "ProgramFunds",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProgramFunds_LocationID",
                table: "ProgramFunds",
                column: "LocationID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramFunds_Location_LocationID",
                table: "ProgramFunds",
                column: "LocationID",
                principalTable: "Location",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProgramFunds_Location_LocationID",
                table: "ProgramFunds");

            migrationBuilder.DropIndex(
                name: "IX_ProgramFunds_LocationID",
                table: "ProgramFunds");

            migrationBuilder.DropColumn(
                name: "LocationID",
                table: "ProgramFunds");
        }
    }
}
