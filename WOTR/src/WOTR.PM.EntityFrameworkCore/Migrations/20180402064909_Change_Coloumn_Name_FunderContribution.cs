﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Change_Coloumn_Name_FunderContribution : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Grant",
                table: "ProgramRegionSpending",
                newName: "FunderContribution");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FunderContribution",
                table: "ProgramRegionSpending",
                newName: "Grant");
        }
    }
}
