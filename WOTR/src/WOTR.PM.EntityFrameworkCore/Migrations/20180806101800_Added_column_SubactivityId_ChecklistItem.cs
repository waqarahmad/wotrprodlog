﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Added_column_SubactivityId_ChecklistItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubActivityId",
                table: "ChecklistItem",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ChecklistItem_SubActivityId",
                table: "ChecklistItem",
                column: "SubActivityId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChecklistItem_SubActivity_SubActivityId",
                table: "ChecklistItem",
                column: "SubActivityId",
                principalTable: "SubActivity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChecklistItem_SubActivity_SubActivityId",
                table: "ChecklistItem");

            migrationBuilder.DropIndex(
                name: "IX_ChecklistItem_SubActivityId",
                table: "ChecklistItem");

            migrationBuilder.DropColumn(
                name: "SubActivityId",
                table: "ChecklistItem");
        }
    }
}
