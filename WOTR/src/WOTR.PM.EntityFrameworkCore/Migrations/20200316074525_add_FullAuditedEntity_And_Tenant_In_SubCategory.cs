﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class add_FullAuditedEntity_And_Tenant_In_SubCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "IISubCategory",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "IISubCategory",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "IISubCategory",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "IISubCategory",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "IISubCategory",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "IISubCategory",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "IISubCategory",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "IISubCategory",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "IISubCategory");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "IISubCategory");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "IISubCategory");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "IISubCategory");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "IISubCategory");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "IISubCategory");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "IISubCategory");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "IISubCategory");
        }
    }
}
