﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_RequestAAplas_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RequestAAplas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChecklistID = table.Column<int>(nullable: true),
                    CommunityContribution = table.Column<decimal>(nullable: true),
                    CostEstimationYear = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    FunderContribution = table.Column<decimal>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    LocationID = table.Column<int>(nullable: true),
                    ManagerID = table.Column<long>(nullable: false),
                    OtherContribution = table.Column<decimal>(nullable: true),
                    PrgActionAreaActivityMappingID = table.Column<int>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    Quarter1FinancialRs = table.Column<decimal>(nullable: false),
                    Quarter1PhysicalUnits = table.Column<int>(nullable: true),
                    Quarter2FinancialRs = table.Column<decimal>(nullable: false),
                    Quarter2PhysicalUnits = table.Column<int>(nullable: true),
                    Quarter3FinancialRs = table.Column<decimal>(nullable: false),
                    Quarter3PhysicalUnits = table.Column<int>(nullable: true),
                    Quarter4FinancialRs = table.Column<decimal>(nullable: false),
                    Quarter4PhysicalUnits = table.Column<int>(nullable: true),
                    RequestStatus = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    TotalUnitCost = table.Column<decimal>(nullable: true),
                    TotalUnits = table.Column<long>(nullable: true),
                    UnitCost = table.Column<decimal>(nullable: true),
                    UnitOfMeasuresID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestAAplas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RequestAAplas_Checklist_ChecklistID",
                        column: x => x.ChecklistID,
                        principalTable: "Checklist",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RequestAAplas_Location_LocationID",
                        column: x => x.LocationID,
                        principalTable: "Location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RequestAAplas_AbpUsers_ManagerID",
                        column: x => x.ManagerID,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RequestAAplas_PrgActionAreaActivityMapping_PrgActionAreaActivityMappingID",
                        column: x => x.PrgActionAreaActivityMappingID,
                        principalTable: "PrgActionAreaActivityMapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RequestAAplas_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RequestAAplas_UnitOfMeasure_UnitOfMeasuresID",
                        column: x => x.UnitOfMeasuresID,
                        principalTable: "UnitOfMeasure",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RequestAAplas_ChecklistID",
                table: "RequestAAplas",
                column: "ChecklistID");

            migrationBuilder.CreateIndex(
                name: "IX_RequestAAplas_LocationID",
                table: "RequestAAplas",
                column: "LocationID");

            migrationBuilder.CreateIndex(
                name: "IX_RequestAAplas_ManagerID",
                table: "RequestAAplas",
                column: "ManagerID");

            migrationBuilder.CreateIndex(
                name: "IX_RequestAAplas_PrgActionAreaActivityMappingID",
                table: "RequestAAplas",
                column: "PrgActionAreaActivityMappingID");

            migrationBuilder.CreateIndex(
                name: "IX_RequestAAplas_ProgramID",
                table: "RequestAAplas",
                column: "ProgramID");

            migrationBuilder.CreateIndex(
                name: "IX_RequestAAplas_UnitOfMeasuresID",
                table: "RequestAAplas",
                column: "UnitOfMeasuresID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RequestAAplas");
        }
    }
}
