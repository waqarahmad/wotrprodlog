﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_Migration_ImplementationPlanChecklist_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ImplementationPlanCheckList",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssingUserId = table.Column<int>(nullable: true),
                    ChecklistItemID = table.Column<int>(nullable: true),
                    CheklistEndDate = table.Column<DateTime>(nullable: false),
                    CheklistStartDate = table.Column<DateTime>(nullable: false),
                    CostEstimationYear = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    LocationId = table.Column<int>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    UnitOfMeasuresID = table.Column<int>(nullable: true),
                    Units = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImplementationPlanCheckList", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ImplementationPlanCheckList_ChecklistItem_ChecklistItemID",
                        column: x => x.ChecklistItemID,
                        principalTable: "ChecklistItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ImplementationPlanCheckList_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ImplementationPlanCheckList_UnitOfMeasure_UnitOfMeasuresID",
                        column: x => x.UnitOfMeasuresID,
                        principalTable: "UnitOfMeasure",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ImplementationPlanCheckList_ChecklistItemID",
                table: "ImplementationPlanCheckList",
                column: "ChecklistItemID");

            migrationBuilder.CreateIndex(
                name: "IX_ImplementationPlanCheckList_ProgramID",
                table: "ImplementationPlanCheckList",
                column: "ProgramID");

            migrationBuilder.CreateIndex(
                name: "IX_ImplementationPlanCheckList_UnitOfMeasuresID",
                table: "ImplementationPlanCheckList",
                column: "UnitOfMeasuresID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ImplementationPlanCheckList");
        }
    }
}
