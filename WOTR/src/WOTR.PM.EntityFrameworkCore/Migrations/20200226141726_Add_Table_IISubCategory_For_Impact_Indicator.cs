﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Add_Table_IISubCategory_For_Impact_Indicator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IISubCategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ImpactIndicatorCategoryId = table.Column<int>(nullable: true),
                    SubCategory = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IISubCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IISubCategory_IICategory_ImpactIndicatorCategoryId",
                        column: x => x.ImpactIndicatorCategoryId,
                        principalTable: "IICategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IISubCategory_ImpactIndicatorCategoryId",
                table: "IISubCategory",
                column: "ImpactIndicatorCategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IISubCategory");
        }
    }
}
