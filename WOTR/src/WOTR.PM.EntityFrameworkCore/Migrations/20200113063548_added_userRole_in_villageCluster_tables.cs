﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_userRole_in_villageCluster_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "UserRoleId",
                table: "VillageCluster",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VillageCluster_UserRoleId",
                table: "VillageCluster",
                column: "UserRoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_VillageCluster_AbpUserRoles_UserRoleId",
                table: "VillageCluster",
                column: "UserRoleId",
                principalTable: "AbpUserRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VillageCluster_AbpUserRoles_UserRoleId",
                table: "VillageCluster");

            migrationBuilder.DropIndex(
                name: "IX_VillageCluster_UserRoleId",
                table: "VillageCluster");

            migrationBuilder.DropColumn(
                name: "UserRoleId",
                table: "VillageCluster");
        }
    }
}
