﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Alter_ActionAreaId_Coloumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ActionAreaId",
                table: "SubActionArea",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.CreateIndex(
                name: "IX_SubActionArea_ActionAreaId",
                table: "SubActionArea",
                column: "ActionAreaId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubActionArea_ActionArea_ActionAreaId",
                table: "SubActionArea",
                column: "ActionAreaId",
                principalTable: "ActionArea",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubActionArea_ActionArea_ActionAreaId",
                table: "SubActionArea");

            migrationBuilder.DropIndex(
                name: "IX_SubActionArea_ActionAreaId",
                table: "SubActionArea");

            migrationBuilder.AlterColumn<string>(
                name: "ActionAreaId",
                table: "SubActionArea",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
