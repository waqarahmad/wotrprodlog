﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class aaded_implementationPlanCheklistId_column_prmQmappingTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ImplementationPlanCheckListID",
                table: "prgImplementationPlanQuestionariesMapping",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_prgImplementationPlanQuestionariesMapping_ImplementationPlanCheckListID",
                table: "prgImplementationPlanQuestionariesMapping",
                column: "ImplementationPlanCheckListID");

            migrationBuilder.AddForeignKey(
                name: "FK_prgImplementationPlanQuestionariesMapping_ImplementationPlanCheckList_ImplementationPlanCheckListID",
                table: "prgImplementationPlanQuestionariesMapping",
                column: "ImplementationPlanCheckListID",
                principalTable: "ImplementationPlanCheckList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_prgImplementationPlanQuestionariesMapping_ImplementationPlanCheckList_ImplementationPlanCheckListID",
                table: "prgImplementationPlanQuestionariesMapping");

            migrationBuilder.DropIndex(
                name: "IX_prgImplementationPlanQuestionariesMapping_ImplementationPlanCheckListID",
                table: "prgImplementationPlanQuestionariesMapping");

            migrationBuilder.DropColumn(
                name: "ImplementationPlanCheckListID",
                table: "prgImplementationPlanQuestionariesMapping");
        }
    }
}
