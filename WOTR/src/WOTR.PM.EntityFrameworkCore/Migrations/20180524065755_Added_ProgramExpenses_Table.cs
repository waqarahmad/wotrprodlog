﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Added_ProgramExpenses_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProgramExpense",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ExpenseDate = table.Column<DateTime>(nullable: false),
                    ExpenseTitle = table.Column<string>(nullable: true),
                    ExpenseType = table.Column<int>(nullable: false),
                    ExpensesYear = table.Column<string>(nullable: true),
                    Image = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    MapSubActivityIteamsToImplementionPlanID = table.Column<int>(nullable: true),
                    PrgActionAreaActivityMappingID = table.Column<int>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    ProgramQuqterUnitMappingID = table.Column<int>(nullable: true),
                    QuarterId = table.Column<int>(nullable: false),
                    Remark = table.Column<string>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    Unit = table.Column<int>(nullable: false),
                    UnitOfMeasuresID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramExpense", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProgramExpense_MapSubActivityIteamsToImplementionPlan_MapSubActivityIteamsToImplementionPlanID",
                        column: x => x.MapSubActivityIteamsToImplementionPlanID,
                        principalTable: "MapSubActivityIteamsToImplementionPlan",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramExpense_PrgActionAreaActivityMapping_PrgActionAreaActivityMappingID",
                        column: x => x.PrgActionAreaActivityMappingID,
                        principalTable: "PrgActionAreaActivityMapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramExpense_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProgramExpense_ProgramQuqterUnitMapping_ProgramQuqterUnitMappingID",
                        column: x => x.ProgramQuqterUnitMappingID,
                        principalTable: "ProgramQuqterUnitMapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramExpense_UnitOfMeasure_UnitOfMeasuresID",
                        column: x => x.UnitOfMeasuresID,
                        principalTable: "UnitOfMeasure",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProgramExpense_MapSubActivityIteamsToImplementionPlanID",
                table: "ProgramExpense",
                column: "MapSubActivityIteamsToImplementionPlanID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramExpense_PrgActionAreaActivityMappingID",
                table: "ProgramExpense",
                column: "PrgActionAreaActivityMappingID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramExpense_ProgramID",
                table: "ProgramExpense",
                column: "ProgramID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramExpense_ProgramQuqterUnitMappingID",
                table: "ProgramExpense",
                column: "ProgramQuqterUnitMappingID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramExpense_UnitOfMeasuresID",
                table: "ProgramExpense",
                column: "UnitOfMeasuresID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProgramExpense");
        }
    }
}
