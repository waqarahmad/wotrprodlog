﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class ADD_Column_ImpactIndicatorFormId_In_IIFormResponses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "impactIndicatorFormId",
                table: "IIFormResponses",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_IIFormResponses_impactIndicatorFormId",
                table: "IIFormResponses",
                column: "impactIndicatorFormId");

            migrationBuilder.AddForeignKey(
                name: "FK_IIFormResponses_IIForm_impactIndicatorFormId",
                table: "IIFormResponses",
                column: "impactIndicatorFormId",
                principalTable: "IIForm",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IIFormResponses_IIForm_impactIndicatorFormId",
                table: "IIFormResponses");

            migrationBuilder.DropIndex(
                name: "IX_IIFormResponses_impactIndicatorFormId",
                table: "IIFormResponses");

            migrationBuilder.DropColumn(
                name: "impactIndicatorFormId",
                table: "IIFormResponses");
        }
    }
}
