﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Added_ProgramFunds_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProgramFunds",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Amount = table.Column<decimal>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    PrgActionAreaActivityMappingID = table.Column<int>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    Unit = table.Column<int>(nullable: false),
                    UnitOfMeasuresID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramFunds", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProgramFunds_PrgActionAreaActivityMapping_PrgActionAreaActivityMappingID",
                        column: x => x.PrgActionAreaActivityMappingID,
                        principalTable: "PrgActionAreaActivityMapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramFunds_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProgramFunds_UnitOfMeasure_UnitOfMeasuresID",
                        column: x => x.UnitOfMeasuresID,
                        principalTable: "UnitOfMeasure",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProgramFunds_PrgActionAreaActivityMappingID",
                table: "ProgramFunds",
                column: "PrgActionAreaActivityMappingID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramFunds_ProgramID",
                table: "ProgramFunds",
                column: "ProgramID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramFunds_UnitOfMeasuresID",
                table: "ProgramFunds",
                column: "UnitOfMeasuresID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProgramFunds");
        }
    }
}
