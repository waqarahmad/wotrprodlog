﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_migration_for_ImplementionplanChecklist_coloumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LocationId",
                table: "ImplementationPlanCheckList",
                newName: "ProgramLocationId");

            migrationBuilder.AddColumn<int>(
                name: "ChecklistID",
                table: "ImplementationPlanCheckList",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IteamLocationId",
                table: "ImplementationPlanCheckList",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PrgActionAreaActivityMappingID",
                table: "ImplementationPlanCheckList",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ImplementationPlanCheckList_ChecklistID",
                table: "ImplementationPlanCheckList",
                column: "ChecklistID");

            migrationBuilder.CreateIndex(
                name: "IX_ImplementationPlanCheckList_PrgActionAreaActivityMappingID",
                table: "ImplementationPlanCheckList",
                column: "PrgActionAreaActivityMappingID");

            migrationBuilder.AddForeignKey(
                name: "FK_ImplementationPlanCheckList_Checklist_ChecklistID",
                table: "ImplementationPlanCheckList",
                column: "ChecklistID",
                principalTable: "Checklist",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ImplementationPlanCheckList_PrgActionAreaActivityMapping_PrgActionAreaActivityMappingID",
                table: "ImplementationPlanCheckList",
                column: "PrgActionAreaActivityMappingID",
                principalTable: "PrgActionAreaActivityMapping",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ImplementationPlanCheckList_Checklist_ChecklistID",
                table: "ImplementationPlanCheckList");

            migrationBuilder.DropForeignKey(
                name: "FK_ImplementationPlanCheckList_PrgActionAreaActivityMapping_PrgActionAreaActivityMappingID",
                table: "ImplementationPlanCheckList");

            migrationBuilder.DropIndex(
                name: "IX_ImplementationPlanCheckList_ChecklistID",
                table: "ImplementationPlanCheckList");

            migrationBuilder.DropIndex(
                name: "IX_ImplementationPlanCheckList_PrgActionAreaActivityMappingID",
                table: "ImplementationPlanCheckList");

            migrationBuilder.DropColumn(
                name: "ChecklistID",
                table: "ImplementationPlanCheckList");

            migrationBuilder.DropColumn(
                name: "IteamLocationId",
                table: "ImplementationPlanCheckList");

            migrationBuilder.DropColumn(
                name: "PrgActionAreaActivityMappingID",
                table: "ImplementationPlanCheckList");

            migrationBuilder.RenameColumn(
                name: "ProgramLocationId",
                table: "ImplementationPlanCheckList",
                newName: "LocationId");
        }
    }
}
