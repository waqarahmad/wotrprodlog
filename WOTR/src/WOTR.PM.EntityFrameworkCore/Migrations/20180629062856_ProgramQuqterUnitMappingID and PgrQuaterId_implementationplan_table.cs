﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class ProgramQuqterUnitMappingIDandPgrQuaterId_implementationplan_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "QuarterID",
                table: "ImplementationPlanCheckList",
                newName: "ProgramQuqterUnitMappingID");

            migrationBuilder.AddColumn<int>(
                name: "PrgQuarter",
                table: "ImplementationPlanCheckList",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_ImplementationPlanCheckList_ProgramQuqterUnitMappingID",
                table: "ImplementationPlanCheckList",
                column: "ProgramQuqterUnitMappingID");

            migrationBuilder.AddForeignKey(
                name: "FK_ImplementationPlanCheckList_ProgramQuqterUnitMapping_ProgramQuqterUnitMappingID",
                table: "ImplementationPlanCheckList",
                column: "ProgramQuqterUnitMappingID",
                principalTable: "ProgramQuqterUnitMapping",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ImplementationPlanCheckList_ProgramQuqterUnitMapping_ProgramQuqterUnitMappingID",
                table: "ImplementationPlanCheckList");

            migrationBuilder.DropIndex(
                name: "IX_ImplementationPlanCheckList_ProgramQuqterUnitMappingID",
                table: "ImplementationPlanCheckList");

            migrationBuilder.DropColumn(
                name: "PrgQuarter",
                table: "ImplementationPlanCheckList");

            migrationBuilder.RenameColumn(
                name: "ProgramQuqterUnitMappingID",
                table: "ImplementationPlanCheckList",
                newName: "QuarterID");
        }
    }
}
