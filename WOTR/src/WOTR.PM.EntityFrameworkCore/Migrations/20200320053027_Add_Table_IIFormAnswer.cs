﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Add_Table_IIFormAnswer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IIFormAnswers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Answer = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DateOfCollection = table.Column<DateTime>(nullable: false),
                    DateOfEntry = table.Column<DateTime>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    FrequencyOfoccurrenceId = table.Column<int>(nullable: false),
                    IIFormResponseId = table.Column<long>(nullable: false),
                    IIQuestionaryId = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    SourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IIFormAnswers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IIFormAnswers_FrequencyOfoccurrences_FrequencyOfoccurrenceId",
                        column: x => x.FrequencyOfoccurrenceId,
                        principalTable: "FrequencyOfoccurrences",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IIFormAnswers_IIFormResponses_IIFormResponseId",
                        column: x => x.IIFormResponseId,
                        principalTable: "IIFormResponses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IIFormAnswers_IIQuestionary_IIQuestionaryId",
                        column: x => x.IIQuestionaryId,
                        principalTable: "IIQuestionary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IIFormAnswers_Sources_SourceId",
                        column: x => x.SourceId,
                        principalTable: "Sources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IIFormAnswers_FrequencyOfoccurrenceId",
                table: "IIFormAnswers",
                column: "FrequencyOfoccurrenceId");

            migrationBuilder.CreateIndex(
                name: "IX_IIFormAnswers_IIFormResponseId",
                table: "IIFormAnswers",
                column: "IIFormResponseId");

            migrationBuilder.CreateIndex(
                name: "IX_IIFormAnswers_IIQuestionaryId",
                table: "IIFormAnswers",
                column: "IIQuestionaryId");

            migrationBuilder.CreateIndex(
                name: "IX_IIFormAnswers_SourceId",
                table: "IIFormAnswers",
                column: "SourceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IIFormAnswers");
        }
    }
}
