﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class AddedmigrationforProgramComponentsAndImpactIndicatorMapping : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProgramComponentsAndImpactIndicatorMapping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ComponentActivityID = table.Column<int>(nullable: true),
                    ComponentID = table.Column<int>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ImpactIndicatorID = table.Column<int>(nullable: true),
                    ImpactIndicatorctivitiesID = table.Column<int>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramComponentsAndImpactIndicatorMapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProgramComponentsAndImpactIndicatorMapping_Activity_ComponentActivityID",
                        column: x => x.ComponentActivityID,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramComponentsAndImpactIndicatorMapping_Component_ComponentID",
                        column: x => x.ComponentID,
                        principalTable: "Component",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProgramComponentsAndImpactIndicatorMapping_ImpactIndicator_ImpactIndicatorID",
                        column: x => x.ImpactIndicatorID,
                        principalTable: "ImpactIndicator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramComponentsAndImpactIndicatorMapping_ImpactIndicatorActivity_ImpactIndicatorctivitiesID",
                        column: x => x.ImpactIndicatorctivitiesID,
                        principalTable: "ImpactIndicatorActivity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramComponentsAndImpactIndicatorMapping_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProgramComponentsAndImpactIndicatorMapping_ComponentActivityID",
                table: "ProgramComponentsAndImpactIndicatorMapping",
                column: "ComponentActivityID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramComponentsAndImpactIndicatorMapping_ComponentID",
                table: "ProgramComponentsAndImpactIndicatorMapping",
                column: "ComponentID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramComponentsAndImpactIndicatorMapping_ImpactIndicatorID",
                table: "ProgramComponentsAndImpactIndicatorMapping",
                column: "ImpactIndicatorID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramComponentsAndImpactIndicatorMapping_ImpactIndicatorctivitiesID",
                table: "ProgramComponentsAndImpactIndicatorMapping",
                column: "ImpactIndicatorctivitiesID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramComponentsAndImpactIndicatorMapping_ProgramID",
                table: "ProgramComponentsAndImpactIndicatorMapping",
                column: "ProgramID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProgramComponentsAndImpactIndicatorMapping");
        }
    }
}
