﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Added_FullAuditedEntity_District : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "District",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "District",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "District",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "District",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "District",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "District",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "District",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "District",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "District");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "District");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "District");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "District");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "District");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "District");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "District");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "District");
        }
    }
}
