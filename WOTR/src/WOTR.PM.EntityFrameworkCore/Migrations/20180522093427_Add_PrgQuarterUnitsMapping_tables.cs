﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Add_PrgQuarterUnitsMapping_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "VillageID",
                table: "RequestAAplas",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ProgramQuqterUnitMapping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    NosOfUnit = table.Column<int>(nullable: false),
                    PrgActionAreaActivityMappingID = table.Column<int>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    ProgramQuaterYear = table.Column<string>(nullable: true),
                    QuarterId = table.Column<int>(nullable: false),
                    Rs = table.Column<decimal>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramQuqterUnitMapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProgramQuqterUnitMapping_PrgActionAreaActivityMapping_PrgActionAreaActivityMappingID",
                        column: x => x.PrgActionAreaActivityMappingID,
                        principalTable: "PrgActionAreaActivityMapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramQuqterUnitMapping_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RequestAAplas_VillageID",
                table: "RequestAAplas",
                column: "VillageID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramQuqterUnitMapping_PrgActionAreaActivityMappingID",
                table: "ProgramQuqterUnitMapping",
                column: "PrgActionAreaActivityMappingID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramQuqterUnitMapping_ProgramID",
                table: "ProgramQuqterUnitMapping",
                column: "ProgramID");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestAAplas_Village_VillageID",
                table: "RequestAAplas",
                column: "VillageID",
                principalTable: "Village",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RequestAAplas_Village_VillageID",
                table: "RequestAAplas");

            migrationBuilder.DropTable(
                name: "ProgramQuqterUnitMapping");

            migrationBuilder.DropIndex(
                name: "IX_RequestAAplas_VillageID",
                table: "RequestAAplas");

            migrationBuilder.DropColumn(
                name: "VillageID",
                table: "RequestAAplas");
        }
    }
}
