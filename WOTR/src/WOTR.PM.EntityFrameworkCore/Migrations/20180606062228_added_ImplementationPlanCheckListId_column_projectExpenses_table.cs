﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_ImplementationPlanCheckListId_column_projectExpenses_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ImplementationPlanCheckListID",
                table: "ProgramExpense",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProgramExpense_ImplementationPlanCheckListID",
                table: "ProgramExpense",
                column: "ImplementationPlanCheckListID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramExpense_ImplementationPlanCheckList_ImplementationPlanCheckListID",
                table: "ProgramExpense",
                column: "ImplementationPlanCheckListID",
                principalTable: "ImplementationPlanCheckList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProgramExpense_ImplementationPlanCheckList_ImplementationPlanCheckListID",
                table: "ProgramExpense");

            migrationBuilder.DropIndex(
                name: "IX_ProgramExpense_ImplementationPlanCheckListID",
                table: "ProgramExpense");

            migrationBuilder.DropColumn(
                name: "ImplementationPlanCheckListID",
                table: "ProgramExpense");
        }
    }
}
