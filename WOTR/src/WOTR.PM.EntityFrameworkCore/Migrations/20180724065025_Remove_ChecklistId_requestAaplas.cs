﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Remove_ChecklistId_requestAaplas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RequestAAplas_Checklist_ChecklistID",
                table: "RequestAAplas");

            migrationBuilder.DropIndex(
                name: "IX_RequestAAplas_ChecklistID",
                table: "RequestAAplas");

            migrationBuilder.DropColumn(
                name: "ChecklistID",
                table: "RequestAAplas");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ChecklistID",
                table: "RequestAAplas",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_RequestAAplas_ChecklistID",
                table: "RequestAAplas",
                column: "ChecklistID");

            migrationBuilder.AddForeignKey(
                name: "FK_RequestAAplas_Checklist_ChecklistID",
                table: "RequestAAplas",
                column: "ChecklistID",
                principalTable: "Checklist",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
