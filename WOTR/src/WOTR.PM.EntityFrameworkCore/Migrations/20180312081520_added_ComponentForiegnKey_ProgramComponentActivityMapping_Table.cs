﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_ComponentForiegnKey_ProgramComponentActivityMapping_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ComponentID",
                table: "ProgramComponentsActivitesMapping",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProgramComponentsActivitesMapping_ComponentID",
                table: "ProgramComponentsActivitesMapping",
                column: "ComponentID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramComponentsActivitesMapping_Component_ComponentID",
                table: "ProgramComponentsActivitesMapping",
                column: "ComponentID",
                principalTable: "Component",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProgramComponentsActivitesMapping_Component_ComponentID",
                table: "ProgramComponentsActivitesMapping");

            migrationBuilder.DropIndex(
                name: "IX_ProgramComponentsActivitesMapping_ComponentID",
                table: "ProgramComponentsActivitesMapping");

            migrationBuilder.DropColumn(
                name: "ComponentID",
                table: "ProgramComponentsActivitesMapping");
        }
    }
}
