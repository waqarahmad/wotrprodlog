﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Add_column_villageId_ProgramQuarterUNitMapping : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "VillageID",
                table: "ProgramQuqterUnitMapping",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProgramQuqterUnitMapping_VillageID",
                table: "ProgramQuqterUnitMapping",
                column: "VillageID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramQuqterUnitMapping_Village_VillageID",
                table: "ProgramQuqterUnitMapping",
                column: "VillageID",
                principalTable: "Village",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProgramQuqterUnitMapping_Village_VillageID",
                table: "ProgramQuqterUnitMapping");

            migrationBuilder.DropIndex(
                name: "IX_ProgramQuqterUnitMapping_VillageID",
                table: "ProgramQuqterUnitMapping");

            migrationBuilder.DropColumn(
                name: "VillageID",
                table: "ProgramQuqterUnitMapping");
        }
    }
}
