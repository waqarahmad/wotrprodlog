﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Add_New_Col_FrequnencyOfOccurance_IIQuestionary : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FrequencyOfOccurance",
                table: "IIQuestionary",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FrequencyOfOccurance",
                table: "IIQuestionary");
        }
    }
}
