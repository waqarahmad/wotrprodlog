﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class MigrationAdded_VillageIdToLocationTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "VillageId",
                table: "Location",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Location_VillageId",
                table: "Location",
                column: "VillageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Village_VillageId",
                table: "Location",
                column: "VillageId",
                principalTable: "Village",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_Village_VillageId",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_VillageId",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "VillageId",
                table: "Location");
        }
    }
}
