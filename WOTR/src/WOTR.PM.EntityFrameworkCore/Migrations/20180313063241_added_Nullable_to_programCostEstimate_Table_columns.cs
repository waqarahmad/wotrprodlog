﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_Nullable_to_programCostEstimate_Table_columns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "UnitCost",
                table: "ProgramCostEstimation",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<long>(
                name: "TotalUnits",
                table: "ProgramCostEstimation",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalUnitCost",
                table: "ProgramCostEstimation",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "OtherContribution",
                table: "ProgramCostEstimation",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "FunderContribution",
                table: "ProgramCostEstimation",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "CommunityContribution",
                table: "ProgramCostEstimation",
                nullable: true,
                oldClrType: typeof(decimal));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "UnitCost",
                table: "ProgramCostEstimation",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "TotalUnits",
                table: "ProgramCostEstimation",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalUnitCost",
                table: "ProgramCostEstimation",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "OtherContribution",
                table: "ProgramCostEstimation",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "FunderContribution",
                table: "ProgramCostEstimation",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CommunityContribution",
                table: "ProgramCostEstimation",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);
        }
    }
}
