﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_columns_to_villagecluster_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VillageCluster",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    DistrictID = table.Column<int>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: false),
                    ProgramID = table.Column<int>(nullable: true),
                    RRCID = table.Column<int>(nullable: true),
                    StateID = table.Column<int>(nullable: true),
                    TalukaID = table.Column<int>(nullable: true),
                    TenantId = table.Column<int>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    VillageID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VillageCluster", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VillageCluster_District_DistrictID",
                        column: x => x.DistrictID,
                        principalTable: "District",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VillageCluster_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VillageCluster_Location_RRCID",
                        column: x => x.RRCID,
                        principalTable: "Location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VillageCluster_States_StateID",
                        column: x => x.StateID,
                        principalTable: "States",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VillageCluster_Taluka_TalukaID",
                        column: x => x.TalukaID,
                        principalTable: "Taluka",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VillageCluster_Village_VillageID",
                        column: x => x.VillageID,
                        principalTable: "Village",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VillageCluster_DistrictID",
                table: "VillageCluster",
                column: "DistrictID");

            migrationBuilder.CreateIndex(
                name: "IX_VillageCluster_ProgramID",
                table: "VillageCluster",
                column: "ProgramID");

            migrationBuilder.CreateIndex(
                name: "IX_VillageCluster_RRCID",
                table: "VillageCluster",
                column: "RRCID");

            migrationBuilder.CreateIndex(
                name: "IX_VillageCluster_StateID",
                table: "VillageCluster",
                column: "StateID");

            migrationBuilder.CreateIndex(
                name: "IX_VillageCluster_TalukaID",
                table: "VillageCluster",
                column: "TalukaID");

            migrationBuilder.CreateIndex(
                name: "IX_VillageCluster_VillageID",
                table: "VillageCluster",
                column: "VillageID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VillageCluster");
        }
    }
}
