﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_ExpensesTypesId_column_Programexpenses_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ExpenseType",
                table: "ProgramExpense",
                newName: "ExpensesTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramExpense_ExpensesTypeID",
                table: "ProgramExpense",
                column: "ExpensesTypeID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramExpense_ExpensesType_ExpensesTypeID",
                table: "ProgramExpense",
                column: "ExpensesTypeID",
                principalTable: "ExpensesType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProgramExpense_ExpensesType_ExpensesTypeID",
                table: "ProgramExpense");

            migrationBuilder.DropIndex(
                name: "IX_ProgramExpense_ExpensesTypeID",
                table: "ProgramExpense");

            migrationBuilder.RenameColumn(
                name: "ExpensesTypeID",
                table: "ProgramExpense",
                newName: "ExpenseType");
        }
    }
}
