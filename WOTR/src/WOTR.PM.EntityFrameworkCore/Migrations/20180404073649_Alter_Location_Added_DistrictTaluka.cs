﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Alter_Location_Added_DistrictTaluka : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "State",
                table: "Location");

            migrationBuilder.AddColumn<int>(
                name: "DistrictId",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StateID",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TalukaId",
                table: "Location",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Location_DistrictId",
                table: "Location",
                column: "DistrictId");

            migrationBuilder.CreateIndex(
                name: "IX_Location_StateID",
                table: "Location",
                column: "StateID");

            migrationBuilder.CreateIndex(
                name: "IX_Location_TalukaId",
                table: "Location",
                column: "TalukaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Location_District_DistrictId",
                table: "Location",
                column: "DistrictId",
                principalTable: "District",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Location_States_StateID",
                table: "Location",
                column: "StateID",
                principalTable: "States",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Taluka_TalukaId",
                table: "Location",
                column: "TalukaId",
                principalTable: "Taluka",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_District_DistrictId",
                table: "Location");

            migrationBuilder.DropForeignKey(
                name: "FK_Location_States_StateID",
                table: "Location");

            migrationBuilder.DropForeignKey(
                name: "FK_Location_Taluka_TalukaId",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_DistrictId",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_StateID",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_TalukaId",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "DistrictId",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "StateID",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "TalukaId",
                table: "Location");

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "Location",
                nullable: false,
                defaultValue: "");
        }
    }
}
