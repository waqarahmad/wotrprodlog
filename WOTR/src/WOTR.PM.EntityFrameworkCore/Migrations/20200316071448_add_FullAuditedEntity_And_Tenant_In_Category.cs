﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class add_FullAuditedEntity_And_Tenant_In_Category : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreationTime",
                table: "IICategory",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<long>(
                name: "CreatorUserId",
                table: "IICategory",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DeleterUserId",
                table: "IICategory",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletionTime",
                table: "IICategory",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "IICategory",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "IICategory",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "LastModifierUserId",
                table: "IICategory",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "IICategory",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreationTime",
                table: "IICategory");

            migrationBuilder.DropColumn(
                name: "CreatorUserId",
                table: "IICategory");

            migrationBuilder.DropColumn(
                name: "DeleterUserId",
                table: "IICategory");

            migrationBuilder.DropColumn(
                name: "DeletionTime",
                table: "IICategory");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "IICategory");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "IICategory");

            migrationBuilder.DropColumn(
                name: "LastModifierUserId",
                table: "IICategory");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "IICategory");
        }
    }
}
