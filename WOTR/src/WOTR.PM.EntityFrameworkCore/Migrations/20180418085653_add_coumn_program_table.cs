﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class add_coumn_program_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "GantnAmountSanctionRupess",
                table: "Program",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ProgramApprovaltDate",
                table: "Program",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "ProgramSubmitDate",
                table: "Program",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GantnAmountSanctionRupess",
                table: "Program");

            migrationBuilder.DropColumn(
                name: "ProgramApprovaltDate",
                table: "Program");

            migrationBuilder.DropColumn(
                name: "ProgramSubmitDate",
                table: "Program");
        }
    }
}
