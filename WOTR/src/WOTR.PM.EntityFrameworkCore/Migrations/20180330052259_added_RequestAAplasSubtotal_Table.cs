﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_RequestAAplasSubtotal_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RequestAAplasSubtotal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CostEstimationYear = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    LocationID = table.Column<int>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    SubTotalTotalunits = table.Column<decimal>(nullable: true),
                    SubTotalUnitCost = table.Column<decimal>(nullable: true),
                    SubTotalofCommunityContribution = table.Column<decimal>(nullable: true),
                    SubTotalofFunderContribution = table.Column<decimal>(nullable: true),
                    SubTotalofOtherContribution = table.Column<decimal>(nullable: true),
                    SubTotalofTotalCost = table.Column<decimal>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RequestAAplasSubtotal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RequestAAplasSubtotal_Location_LocationID",
                        column: x => x.LocationID,
                        principalTable: "Location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RequestAAplasSubtotal_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RequestAAplasSubtotal_LocationID",
                table: "RequestAAplasSubtotal",
                column: "LocationID");

            migrationBuilder.CreateIndex(
                name: "IX_RequestAAplasSubtotal_ProgramID",
                table: "RequestAAplasSubtotal",
                column: "ProgramID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RequestAAplasSubtotal");
        }
    }
}
