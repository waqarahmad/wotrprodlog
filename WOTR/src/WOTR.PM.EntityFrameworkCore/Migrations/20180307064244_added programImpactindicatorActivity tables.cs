﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class addedprogramImpactindicatorActivitytables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProgramImpactIndicatorActivityMapping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    ImpactIndicatorctivitiesID = table.Column<int>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    ProgramImpactIndicatorMappingID = table.Column<int>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramImpactIndicatorActivityMapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProgramImpactIndicatorActivityMapping_ImpactIndicatorActivity_ImpactIndicatorctivitiesID",
                        column: x => x.ImpactIndicatorctivitiesID,
                        principalTable: "ImpactIndicatorActivity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramImpactIndicatorActivityMapping_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProgramImpactIndicatorActivityMapping_ProgramImpactIndicatorMapping_ProgramImpactIndicatorMappingID",
                        column: x => x.ProgramImpactIndicatorMappingID,
                        principalTable: "ProgramImpactIndicatorMapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProgramImpactIndicatorActivityMapping_ImpactIndicatorctivitiesID",
                table: "ProgramImpactIndicatorActivityMapping",
                column: "ImpactIndicatorctivitiesID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramImpactIndicatorActivityMapping_ProgramID",
                table: "ProgramImpactIndicatorActivityMapping",
                column: "ProgramID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramImpactIndicatorActivityMapping_ProgramImpactIndicatorMappingID",
                table: "ProgramImpactIndicatorActivityMapping",
                column: "ProgramImpactIndicatorMappingID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProgramImpactIndicatorActivityMapping");
        }
    }
}
