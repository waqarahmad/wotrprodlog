﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Added_Column_ProgramManagerId_projectManagerId_ProjectFunds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ProgramManagerID",
                table: "ProgramFunds",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ProjectManagerID",
                table: "ProgramFunds",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProgramFunds_ProgramManagerID",
                table: "ProgramFunds",
                column: "ProgramManagerID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramFunds_ProjectManagerID",
                table: "ProgramFunds",
                column: "ProjectManagerID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramFunds_AbpUsers_ProgramManagerID",
                table: "ProgramFunds",
                column: "ProgramManagerID",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramFunds_AbpUsers_ProjectManagerID",
                table: "ProgramFunds",
                column: "ProjectManagerID",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProgramFunds_AbpUsers_ProgramManagerID",
                table: "ProgramFunds");

            migrationBuilder.DropForeignKey(
                name: "FK_ProgramFunds_AbpUsers_ProjectManagerID",
                table: "ProgramFunds");

            migrationBuilder.DropIndex(
                name: "IX_ProgramFunds_ProgramManagerID",
                table: "ProgramFunds");

            migrationBuilder.DropIndex(
                name: "IX_ProgramFunds_ProjectManagerID",
                table: "ProgramFunds");

            migrationBuilder.DropColumn(
                name: "ProgramManagerID",
                table: "ProgramFunds");

            migrationBuilder.DropColumn(
                name: "ProjectManagerID",
                table: "ProgramFunds");
        }
    }
}
