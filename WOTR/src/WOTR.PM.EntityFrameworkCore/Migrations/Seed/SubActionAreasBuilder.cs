﻿using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.EntityFrameworkCore;

namespace WOTR.PM.Migrations.Seed
{
    public class SubActionAreasBuilder
    {
        private readonly PMDbContext _context;
        public IAbpSession AbpSession { get; set; }
        private readonly int _TentantId;

        public SubActionAreasBuilder(PMDbContext context, int TentantId)
        {
            _context = context;
            _TentantId = TentantId;
        }
        public void create()
        {

            var ActionAreaDictionary = _context.ActionArea.ToDictionary(s => s.Code, s => s.Id);

            List<SubActionArea> subactionareaList = new List<SubActionArea>();

            
            

            subactionareaList.Add(new SubActionArea() { Code = "101", Name = "Community Mobilisation, Participatory planning and capacity Building", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "102", Name = "Gender and Women Development", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "103", Name = "Soil and water conservation measures", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "104", Name = "Sustainable Agri practices", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "105", Name = "Agro Metrology", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "106", Name = "Water Budgeting", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "107", Name = "Water Resource developmentand enhancement of water efficiency", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "108", Name = "Livestock", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "109", Name = "DRR", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "110", Name = "Biodiversity", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "111", Name = "Renewal Energy/Alternate energy", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "112", Name = "Health and environment sanitation", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "113", Name = "Livelihoods", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "114", Name = "Drinking Water", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "115", Name = "Innovative Actions", ActionAreaId = ActionAreaDictionary["1"] });
            subactionareaList.Add(new SubActionArea() { Code = "201", Name = "Organising demand based training programmes for external agencies and institutions", ActionAreaId = ActionAreaDictionary["2"] });
            subactionareaList.Add(new SubActionArea() { Code = "202", Name = "Consutancy support", ActionAreaId = ActionAreaDictionary["2"] });
            subactionareaList.Add(new SubActionArea() { Code = "301", Name = "Capacity Building of Trainers and resource persons of WOTR", ActionAreaId = ActionAreaDictionary["3"] });
            subactionareaList.Add(new SubActionArea() { Code = "302", Name = "Development & Preperation of Training kit", ActionAreaId = ActionAreaDictionary["3"] });
            subactionareaList.Add(new SubActionArea() { Code = "303", Name = "Coordination Meetings, Retreats, Picnics, Exposures &Trainings for staff", ActionAreaId = ActionAreaDictionary["3"] });
            subactionareaList.Add(new SubActionArea() { Code = "304", Name = "Planning meeting/Kaizen promotion activities/Monthly staff meeting", ActionAreaId = ActionAreaDictionary["3"] });
            subactionareaList.Add(new SubActionArea() { Code = "401", Name = "Baseline studies, DPR for inhouse project", ActionAreaId = ActionAreaDictionary["4"] });
            subactionareaList.Add(new SubActionArea() { Code = "402", Name = "Evaluation or impact studies", ActionAreaId = ActionAreaDictionary["4"] });
            subactionareaList.Add(new SubActionArea() { Code = "403", Name = "Tools (methodologies)", ActionAreaId = ActionAreaDictionary["4"] });
            subactionareaList.Add(new SubActionArea() { Code = "404", Name = "Mannuals", ActionAreaId = ActionAreaDictionary["4"] });
            subactionareaList.Add(new SubActionArea() { Code = "405", Name = "Research Study Reports", ActionAreaId = ActionAreaDictionary["4"] });
            subactionareaList.Add(new SubActionArea() { Code = "406", Name = "Research Papers - Technical", ActionAreaId = ActionAreaDictionary["4"] });
            subactionareaList.Add(new SubActionArea() { Code = "407", Name = "Research Papers - Non-Technical", ActionAreaId = ActionAreaDictionary["4"] });
            subactionareaList.Add(new SubActionArea() { Code = "408", Name = "Stake holder workshops/ Community Mobilization (CB and Awareness)e.g. Kalapathak, wallpapers, etc.", ActionAreaId = ActionAreaDictionary["4"] });
            subactionareaList.Add(new SubActionArea() { Code = "409", Name = "Dissemination Workshops(village/state/dist/national)", ActionAreaId = ActionAreaDictionary["4"] });
            subactionareaList.Add(new SubActionArea() { Code = "410", Name = "WOTR Publications", ActionAreaId = ActionAreaDictionary["4"] });
            subactionareaList.Add(new SubActionArea() { Code = "501", Name = "Publications/Print communications", ActionAreaId = ActionAreaDictionary["5"] });
            subactionareaList.Add(new SubActionArea() { Code = "502", Name = "Audio-Visual communications - films & animations", ActionAreaId = ActionAreaDictionary["5"] });
            subactionareaList.Add(new SubActionArea() { Code = "503", Name = "Web-based communications - micro-websites/social media etc.", ActionAreaId = ActionAreaDictionary["5"] });
            subactionareaList.Add(new SubActionArea() { Code = "504", Name = "Media Outreach", ActionAreaId = ActionAreaDictionary["5"] });
            subactionareaList.Add(new SubActionArea() { Code = "505", Name = "Participation in and holding Festivals, exhibitions, fairs", ActionAreaId = ActionAreaDictionary["5"] });
            subactionareaList.Add(new SubActionArea() { Code = "506", Name = "Trainings in Communication Techniques", ActionAreaId = ActionAreaDictionary["5"] });
            subactionareaList.Add(new SubActionArea() { Code = "507", Name = "Project and Process Documentation", ActionAreaId = ActionAreaDictionary["5"] });
            subactionareaList.Add(new SubActionArea() { Code = "601", Name = "Participation in National and international workshops and gatherings", ActionAreaId = ActionAreaDictionary["6"] });
            subactionareaList.Add(new SubActionArea() { Code = "602", Name = "Knowledge Management/ Dissemination/ Awareness", ActionAreaId = ActionAreaDictionary["6"] });
            subactionareaList.Add(new SubActionArea() { Code = "603", Name = "Linkage building and providing facilities and support to academic institutions to conduct joint courses in NRM", ActionAreaId = ActionAreaDictionary["6"] });
            subactionareaList.Add(new SubActionArea() { Code = "604", Name = "Summer placement for students from India and Abroad", ActionAreaId = ActionAreaDictionary["6"] });
            subactionareaList.Add(new SubActionArea() { Code = "605", Name = "National level workshops for Policy Makers", ActionAreaId = ActionAreaDictionary["6"] });
            subactionareaList.Add(new SubActionArea() { Code = "606", Name = "Organising sponsored training programmes for NGOs, public and private institutions in the field of CCA, NRM, livelihood promotion and self help promotion. ECO", ActionAreaId = ActionAreaDictionary["6"] });
            subactionareaList.Add(new SubActionArea() { Code = "701", Name = "Software development and IT suport to WOTR and Sister Organisations", ActionAreaId = ActionAreaDictionary["7"] });
            subactionareaList.Add(new SubActionArea() { Code = "702", Name = "Software development for external agencies", ActionAreaId = ActionAreaDictionary["7"] });
            subactionareaList.Add(new SubActionArea() { Code = "703", Name = "GIS & RS Support to Research Wing", ActionAreaId = ActionAreaDictionary["7"] });
            subactionareaList.Add(new SubActionArea() { Code = "704", Name = "Geoinformatics based database system development (Internal and external)", ActionAreaId = ActionAreaDictionary["7"] });
            subactionareaList.Add(new SubActionArea() { Code = "705", Name = "Service provider especially in IT to NGOs, CBOs, etc.", ActionAreaId = ActionAreaDictionary["7"] });
            subactionareaList.Add(new SubActionArea() { Code = "706", Name = "Trainings IT & GIS", ActionAreaId = ActionAreaDictionary["7"] });
            subactionareaList.Add(new SubActionArea() { Code = "801", Name = "Travel expenses", ActionAreaId = ActionAreaDictionary["8"] });
            subactionareaList.Add(new SubActionArea() { Code = "802", Name = "Office running expenses", ActionAreaId = ActionAreaDictionary["8"] });
            subactionareaList.Add(new SubActionArea() { Code = "803", Name = "Capital expenses", ActionAreaId = ActionAreaDictionary["8"] });
            subactionareaList.Add(new SubActionArea() { Code = "804", Name = "Personell expenses", ActionAreaId = ActionAreaDictionary["8"] });

          



            foreach (var dist in subactionareaList)
            {
                var _districtName = dist.Name.Trim();
              
                var districtObject = _context.SubActionArea.IgnoreQueryFilters().FirstOrDefault(d => d.Name == _districtName);

                if (districtObject == null)
                {
                    _context.SubActionArea.Add(new SubActionArea { ActionAreaId = dist.ActionAreaId, Name = _districtName, Code = dist.Code ,TenantId=_TentantId,CreatorUserId=2});
                }
            }




        }

    }
}
