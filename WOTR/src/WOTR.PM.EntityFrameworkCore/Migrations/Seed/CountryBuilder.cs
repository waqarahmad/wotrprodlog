﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.EntityFrameworkCore;

namespace WOTR.PM.Migrations.Seed
{
    public class CountryBuilder
    {
        private readonly PMDbContext _context;
        public CountryBuilder(PMDbContext contex)
        {
            _context = contex;
        }
        public void create()
        {

            List<string> countryNames = new List<string>() { "USA", "Spain", "France", "India" };
            foreach (string countryName in countryNames)
            {
                var ctry = _context.Countrys.FirstOrDefault(p => p.CountryName == countryName);
                if (ctry == null)

                    _context.Countrys.Add(
             new Countrys.Country
             {
                 CountryName = countryName
             });
            }

        }
    }
}
