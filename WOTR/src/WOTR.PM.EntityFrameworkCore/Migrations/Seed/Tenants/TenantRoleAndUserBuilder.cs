﻿using System.Linq;
using Abp;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using Abp.Notifications;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WOTR.PM.Authorization;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Authorization.Users;
using WOTR.PM.EntityFrameworkCore;
using WOTR.PM.Notifications;

namespace WOTR.PM.Migrations.Seed.Tenants
{
    public class TenantRoleAndUserBuilder
    {
        private readonly PMDbContext _context;
        private readonly int _tenantId;

        public TenantRoleAndUserBuilder(PMDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            CreateRolesAndUsers();
        }

        private void CreateRolesAndUsers()
        {
            //Admin role

            var adminRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Admin);
            if (adminRole == null)
            {
                adminRole = _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Admin, StaticRoleNames.Tenants.Admin) { IsStatic = true }).Entity;
                _context.SaveChanges();

                //Grant all permissions to admin role
                var permissions = PermissionFinder
                    .GetAllPermissions(new AppAuthorizationProvider(false))
                    .Where(p => p.MultiTenancySides.HasFlag(MultiTenancySides.Tenant))
                    .ToList();

                foreach (var permission in permissions)
                {
                    _context.Permissions.Add(
                        new RolePermissionSetting
                        {
                            TenantId = _tenantId,
                            Name = permission.Name,
                            IsGranted = true,
                            RoleId = adminRole.Id
                        });
                }

                _context.SaveChanges();
            }
            //User role

            var userRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.User);
            if (userRole == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.User, StaticRoleNames.Tenants.User) { IsStatic = true, IsDefault = false });
                _context.SaveChanges();
            }

            //User role

            var ManagingTrustee = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.ManagingTrustee);
            if (ManagingTrustee == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.ManagingTrustee, StaticRoleNames.Tenants.ManagingTrustee) { IsStatic = true, IsDefault = false });
                _context.SaveChanges();
            }

            //User role

            var ExecutiveDirective = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.ExecutiveDirective);
            if (ExecutiveDirective == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.ExecutiveDirective, StaticRoleNames.Tenants.ExecutiveDirective) { IsStatic = true, IsDefault = false });
                _context.SaveChanges();
            }

            //User role

            var ProgramCoordinator = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.ProgramCoordinator);
            if (ProgramCoordinator == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.ProgramCoordinator, StaticRoleNames.Tenants.ProgramCoordinator) { IsStatic = true, IsDefault = false });
                _context.SaveChanges();
            }

            var ProgramManager = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.ProgramManager);
            if (ProgramManager == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.ProgramManager, StaticRoleNames.Tenants.ProgramManager) { IsStatic = true, IsDefault = false });
                _context.SaveChanges();
            }

            var CommunicationTeam = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.CommunicationTeam);
            if (CommunicationTeam == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.CommunicationTeam, StaticRoleNames.Tenants.CommunicationTeam) { IsStatic = true, IsDefault = false });
                _context.SaveChanges();
            }

            var RRcIncharge = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.RRcIncharge);
            if (RRcIncharge == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.RRcIncharge, StaticRoleNames.Tenants.RRcIncharge) { IsStatic = true, IsDefault = false });
                _context.SaveChanges();
            }


            var ProjectManager = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.ProjectManager);
            if (ProjectManager == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.ProjectManager, StaticRoleNames.Tenants.ProjectManager) { IsStatic = true, IsDefault = false });
                _context.SaveChanges();
            }

            var FieldStaffMember = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.FieldStaffMember);
            if (FieldStaffMember == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.FieldStaffMember, StaticRoleNames.Tenants.FieldStaffMember) { IsStatic = true, IsDefault = false });
                _context.SaveChanges();
            }

            var ProgramManagerFinanceOrAdmin = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.ProgramManagerFinanceOrAdmin);
            if (ProgramManagerFinanceOrAdmin == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.ProgramManagerFinanceOrAdmin, StaticRoleNames.Tenants.ProgramManagerFinanceOrAdmin) { IsStatic = true, IsDefault = false });
                _context.SaveChanges();
            }

            //...Added Employee Manager Role
            var employeeManagerRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.ActionAreaManager);
            if (employeeManagerRole == null)
            {
                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.ActionAreaManager, StaticRoleNames.Tenants.ActionAreaManager) { IsStatic = true, IsDefault = false });
                _context.SaveChanges();
            }

            //admin user

            var adminUser = _context.Users.IgnoreQueryFilters().FirstOrDefault(u => u.TenantId == _tenantId && u.UserName == AbpUserBase.AdminUserName);
            if (adminUser == null)
            {
                adminUser = User.CreateTenantAdminUser(_tenantId, "admin@defaulttenant.com");
                adminUser.Password = new PasswordHasher<User>(new OptionsWrapper<PasswordHasherOptions>(new PasswordHasherOptions())).HashPassword(adminUser, "123qwe");
                adminUser.IsEmailConfirmed = true;
                adminUser.ShouldChangePasswordOnNextLogin = true;
                adminUser.IsActive = true;

                _context.Users.Add(adminUser);
                _context.SaveChanges();

                //Assign Admin role to admin user
                _context.UserRoles.Add(new UserRole(_tenantId, adminUser.Id, adminRole.Id));
                _context.SaveChanges();

                //User account of admin user
                if (_tenantId == 1)
                {
                    _context.UserAccounts.Add(new UserAccount
                    {
                        TenantId = _tenantId,
                        UserId = adminUser.Id,
                        UserName = AbpUserBase.AdminUserName,
                        EmailAddress = adminUser.EmailAddress
                    });
                    _context.SaveChanges();
                }

                //Notification subscription
                _context.NotificationSubscriptions.Add(new NotificationSubscriptionInfo(SequentialGuidGenerator.Instance.Create(), _tenantId, adminUser.Id, AppNotificationNames.NewUserRegistered));
                _context.SaveChanges();
            }
        }
    }
}
