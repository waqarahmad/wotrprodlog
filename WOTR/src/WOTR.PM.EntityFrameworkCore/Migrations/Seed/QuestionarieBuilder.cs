﻿using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.EntityFrameworkCore;
using WOTR.PM.PrgImplementationsPlans;

namespace WOTR.PM.Migrations.Seed
{
    public class QuestionarieBuilder
    {
        private readonly PMDbContext _context;
        public IAbpSession AbpSession { get; set; }
        private readonly int _TentantId;

        public QuestionarieBuilder(PMDbContext context, int TentantId)
        {
            _context = context;
            _TentantId = TentantId;
        }
        public void create()
        {

            //try
            //{

            //    var QuestionarieDictionary = _context.Questionarie.ToDictionary(u => u.Name, u => u.Id);
            //}
            //catch(Exception e)
            //{
            //    throw new Exception(e.Message);
            //}
            List<Questionarie> QuestionarieName = new List<Questionarie>();


            QuestionarieName.Add(new Questionarie() { Name = "decision(yes/no)",  InputType = "RadioBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Agriculture Land covered, ha", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Plant Species Planted,Number", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Upload photo of VDC resolution", InputType = "CheckBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Upload photo of VDC resolution- Max 1", InputType = "CheckBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Upload sample photos", InputType = "CheckBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Upload photo of muster sanction note", InputType = "CheckBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Upload photos of drains- Max 2", InputType = "CheckBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Upload photos- Max 2", InputType = "CheckBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Upload photos of work in progress- Max 2", InputType = "CheckBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Upload photo of Engineer/ VDC- Max 2", InputType = "CheckBox" });

            QuestionarieName.Add(new Questionarie() { Name = "Upload photo of the site - Max 2", InputType = "CheckBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Upload photo of muster, bills, vouchers", InputType = "CheckBox" });
            QuestionarieName.Add(new Questionarie() { Name = "upload_photo", InputType = "CheckBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Name of the Village section planned", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Plantation Area, ha", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Total Number of Plants planted", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Farmer selected for Impact (Y/N)", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Name of Location / Benefitting farmers", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Plot selected for Impact (Y/N)", InputType = "Radio" });
            QuestionarieName.Add(new Questionarie() { Name = "Irrigation Facility available (Y/N)", InputType = "Radio" });
            QuestionarieName.Add(new Questionarie() { Name = "Names of Benefitting farmers", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Cement Nala Bund,Rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Cement Nala Bund , Completed", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Earthen Nala Bund ,Rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Earthen Nala Bund , Completed", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Gabion Structure,Rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Gabion Structure,Numbers Completed", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Type of Structure", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Earthen Gully Plugs,Rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Earthen Gully Plugs, Completed", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Loose Boulder structures,Completed", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Stone Gully plugs Numbers Completed", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Loose Boulder structures,Rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Stone Gully plugs Numbers,Rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Material Payment, Rs.", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Treatment Name", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Photograph of field exercise", InputType = "ChekBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Farm Bund + RFB + SB + CB + GB_well selected for Impact study", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "CCT + SCT_well selected for Impact study", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "WAT + deep CCT_well selected for Impact study", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Farm Bund + RFB + SB + CB + GB_Famer selected for Impact study", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "CCT + SCT_Famer selected for Impact study", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "WAT + deep CCT_Famer selected for Impact study", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Farm Bund + RFB + SB + CB + GB_budget,rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "CCT + SCT_budget,rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "WAT + deep CCT_budget,rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Farm Bund + RFB + SB + CB + GB_Area_covered,ha", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "CCT + SCT_Area_covered,ha", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "WAT + deep CCT_Area covered,ha", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Storage Potential created TCM", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Length of Water Spread Area,Meter", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Top width of Structure,Meter", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Bottom width of Structure,Meter", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Height of Structure,Meter", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Length of Structure,Meter", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Benefitting farmers Master", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Nearest Gat number", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Location Details", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "supervisor days", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "machinedays", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "labourdays", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Contribution/Shramadan",  InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name =  "Machine Payment, Rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Supervision Payment,Rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Miscalleneous Payment,Rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Labour Payment,Rs", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Names of Families selected for Impact", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Number of Machines working on site", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Number of Labours working on site", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Treatment Master", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Forest Land Covered,ha", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Number of Women present for PNP", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Number of VDC members present for PNP", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Number of farmers present for PNP", InputType = "InputBox" });
            QuestionarieName.Add(new Questionarie() { Name = "Community/GP Land covered, ha", InputType = "InputBox" });

            foreach (var QName in QuestionarieName)
            {

                var _districtName = QName.Name.Trim();

                var districtObject = _context.Questionarie.FirstOrDefault(d => d.Name == _districtName);

                if (districtObject == null)
                {
                    _context.Questionarie.Add
                        (new Questionarie
                        {
                            Name = _districtName,
                            CreatorUserId = 2,
                            InputType=QName.InputType,
                            TenantId=1
                        });
                }
            }

        }
    }
    }
