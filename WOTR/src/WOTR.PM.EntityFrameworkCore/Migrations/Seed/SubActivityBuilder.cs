﻿using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Checklists;
using WOTR.PM.ChekLists;
using WOTR.PM.EntityFrameworkCore;

namespace WOTR.PM.Migrations.Seed
{
    public class SubActivityBuilder
    {
        private readonly PMDbContext _context;
        public IAbpSession AbpSession { get; set; }
        private readonly int _TentantId;

        public SubActivityBuilder(PMDbContext context, int TentantId)
        {
            _context = context;
            _TentantId = TentantId;
        }
        public void create()
        {
            try { 
            var ActivityDictionary = _context.Activity.ToDictionary(s => s.Name, s => s.Id);
            List<Checklist> CheckListList = new List<Checklist>();

            CheckListList.Add(new Checklist() { ChecklistName = "Area Treatment", ActivityID = ActivityDictionary["Area Treatment"] });
            CheckListList.Add(new Checklist() { ChecklistName = "Minor DLT Structures", ActivityID = ActivityDictionary["Minor DLT Structures"] });
            CheckListList.Add(new Checklist() { ChecklistName = "Major DLT Structures", ActivityID = ActivityDictionary["Major DLT Structures"] });
            CheckListList.Add(new Checklist() { ChecklistName = "Horticulture - AH", ActivityID = ActivityDictionary["Horticulture - AH"] });
            CheckListList.Add(new Checklist() { ChecklistName = "Horticulture - DH", ActivityID = ActivityDictionary["Horticulture - DH"] });
            CheckListList.Add(new Checklist() { ChecklistName = "Afforestation - Plantation", ActivityID = ActivityDictionary["Afforestation - Plantation"] });
            //CheckListList.Add(new Checklist() { ChecklistName = "Area Treatment", ActivityID = ActivityDictionary["Area Treatment"] });

            foreach (var Check in CheckListList)
            {
                var _CheckListName = Check.ChecklistName.Trim();

                var ChecklistObj = _context.ChekLists.FirstOrDefault(d => d.ChecklistName == _CheckListName);

                if (ChecklistObj == null)
                {
                    _context.ChekLists.Add(new Checklist { ChecklistName = Check.ChecklistName, ActivityID = Check.ActivityID, TenantId = _TentantId, CreatorUserId = 2 });
                }
            }

            _context.SaveChanges();

            List<SubActivityList> SubActivityList = new List<SubActivityList>();

            SubActivityList.Add(new SubActivityList() { Name = "Net planning" });
            SubActivityList.Add(new SubActivityList() { Name = "Submission of MDB file" });
            SubActivityList.Add(new SubActivityList() { Name = "PNP data entry" });
            SubActivityList.Add(new SubActivityList() { Name = "Prepare Map" });
            SubActivityList.Add(new SubActivityList() { Name = "Prepare weekly work execution plan (Labor/machine reuirement estimation)" });
            SubActivityList.Add(new SubActivityList() { Name = "Labor/machine procurement" });
            SubActivityList.Add(new SubActivityList() { Name = "Layout of work" });
            SubActivityList.Add(new SubActivityList() { Name = "Work execution" });
            SubActivityList.Add(new SubActivityList() { Name = "Quality Monitoring" });
            SubActivityList.Add(new SubActivityList() { Name = "Measurements verification" });
            SubActivityList.Add(new SubActivityList() { Name = "Data entry- muster" });
            SubActivityList.Add(new SubActivityList() { Name = "VDC approval to muster" });
            SubActivityList.Add(new SubActivityList() { Name = "Muster payment" });
            SubActivityList.Add(new SubActivityList() { Name = "Muster completion" });
            SubActivityList.Add(new SubActivityList() { Name = "Area identification" });
            SubActivityList.Add(new SubActivityList() { Name = "Labor/material procurement" });
            SubActivityList.Add(new SubActivityList() { Name = "Site selection" });
            SubActivityList.Add(new SubActivityList() { Name = "Farmer's consent/ VDC approval" });
            SubActivityList.Add(new SubActivityList() { Name = "Survey" });
            SubActivityList.Add(new SubActivityList() { Name = "Prepare and submit design & estimate" });
            SubActivityList.Add(new SubActivityList() { Name = "Prepare and submit list of benefiting farmers & wells" });
            SubActivityList.Add(new SubActivityList() { Name = "Layout & Foundation" });
            SubActivityList.Add(new SubActivityList() { Name = "Measurements" });
            SubActivityList.Add(new SubActivityList() { Name = "Muster/material payment" });
            SubActivityList.Add(new SubActivityList() { Name = "Measurement verification" });
            SubActivityList.Add(new SubActivityList() { Name = "Work completion" });
            SubActivityList.Add(new SubActivityList() { Name = "Selection of farmers, area, species and numbers" });
            SubActivityList.Add(new SubActivityList() { Name = "Pits digging" });
            SubActivityList.Add(new SubActivityList() { Name = "Procurement of plants from nursery" });
            SubActivityList.Add(new SubActivityList() { Name = "Plantation" });
            SubActivityList.Add(new SubActivityList() { Name = "Aftercare plan preperation with farmers" });
            SubActivityList.Add(new SubActivityList() { Name = "VDC/JFM/GP resolution" });
            SubActivityList.Add(new SubActivityList() { Name = "Selection of species and numbers" });
            SubActivityList.Add(new SubActivityList() { Name = "Aftercare plan preperation with VDC/JFMC/GP" });

            foreach (var SubAct in SubActivityList)
            {
                var _SubItemName = SubAct.Name.Trim();
                var SubItemObj = _context.SubActivity.FirstOrDefault(d => d.Name == _SubItemName);

                if (SubItemObj == null)
                {
                    _context.SubActivity.Add(new SubActivityList { Name = SubAct.Name, TenantId = _TentantId, CreatorUserId = 2 });
                }
            }
            _context.SaveChanges();
            var CheckListDictionary = _context.ChekLists.ToDictionary(c => c.ChecklistName, c => c.Id);
            var SubActivityListDictionary = _context.SubActivity.ToDictionary(c => c.Name, c => c.Id);

            List<ChecklistItem> CheckListItemList = new List<ChecklistItem>();

            CheckListItemList.Add(new ChecklistItem() { Name = "Net planning", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["Net planning"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Submission of MDB file", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["Submission of MDB file"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "PNP data entry", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["PNP data entry"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Prepare Map", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["Prepare Map"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Prepare weekly work execution plan (Labor/machine reuirement estimation)", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["Prepare weekly work execution plan (Labor/machine reuirement estimation)"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Labor/machine procurement", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["Labor/machine procurement"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Layout of work", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["Layout of work"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Work execution", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["Work execution"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Quality monitoring", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["Quality Monitoring"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Measurements verification", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["Measurements verification"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Data entry- muster ", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["Data entry- muster"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "VDC approval to muster", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["VDC approval to muster"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Muster payment", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["Muster payment"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Muster completion", ChecklistID = CheckListDictionary["Area Treatment"], SubActivityId = SubActivityListDictionary["Muster completion"] });

            CheckListItemList.Add(new ChecklistItem() { Name = "Area identification", ChecklistID = CheckListDictionary["Minor DLT Structures"], SubActivityId = SubActivityListDictionary["Area identification"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Layout of work", ChecklistID = CheckListDictionary["Minor DLT Structures"], SubActivityId = SubActivityListDictionary["Layout of work"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Labor/material procurement", ChecklistID = CheckListDictionary["Minor DLT Structures"], SubActivityId = SubActivityListDictionary["Labor/material procurement"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Work execution", ChecklistID = CheckListDictionary["Minor DLT Structures"], SubActivityId = SubActivityListDictionary["Work execution"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Quality Monitoring", ChecklistID = CheckListDictionary["Minor DLT Structures"], SubActivityId = SubActivityListDictionary["Quality Monitoring"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Measurement verification", ChecklistID = CheckListDictionary["Minor DLT Structures"], SubActivityId = SubActivityListDictionary["Measurement verification"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Data entry- muster", ChecklistID = CheckListDictionary["Minor DLT Structures"], SubActivityId = SubActivityListDictionary["Data entry- muster"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "VDC approval to muster", ChecklistID = CheckListDictionary["Minor DLT Structures"], SubActivityId = SubActivityListDictionary["VDC approval to muster"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Muster Payment", ChecklistID = CheckListDictionary["Minor DLT Structures"], SubActivityId = SubActivityListDictionary["Muster payment"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Muster completion", ChecklistID = CheckListDictionary["Minor DLT Structures"], SubActivityId = SubActivityListDictionary["Muster completion"] });

            CheckListItemList.Add(new ChecklistItem() { Name = "Site selection", ChecklistID = CheckListDictionary["Major DLT Structures"], SubActivityId = SubActivityListDictionary["Site selection"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Farmer's consent/ VDC approval", ChecklistID = CheckListDictionary["Major DLT Structures"], SubActivityId = SubActivityListDictionary["Farmer's consent/ VDC approval"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Survey", ChecklistID = CheckListDictionary["Major DLT Structures"], SubActivityId = SubActivityListDictionary["Survey"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Prepare and submit design & estimate", ChecklistID = CheckListDictionary["Major DLT Structures"], SubActivityId = SubActivityListDictionary["Prepare and submit design & estimate"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Prepare and submit list of benefiting farmers & wells", ChecklistID = CheckListDictionary["Major DLT Structures"], SubActivityId = SubActivityListDictionary["Prepare and submit list of benefiting farmers & wells"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Labor/material procurement", ChecklistID = CheckListDictionary["Major DLT Structures"], SubActivityId = SubActivityListDictionary["Labor/material procurement"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Layout & Foundation", ChecklistID = CheckListDictionary["Major DLT Structures"], SubActivityId = SubActivityListDictionary["Layout & Foundation"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Work execution", ChecklistID = CheckListDictionary["Major DLT Structures"], SubActivityId = SubActivityListDictionary["Work execution"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Quality monitoring", ChecklistID = CheckListDictionary["Major DLT Structures"], SubActivityId = SubActivityListDictionary["Quality Monitoring"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Measurements", ChecklistID = CheckListDictionary["Major DLT Structures"], SubActivityId = SubActivityListDictionary["Measurements"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Muster/material payment", ChecklistID = CheckListDictionary["Major DLT Structures"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Work completion", ChecklistID = CheckListDictionary["Major DLT Structures"], SubActivityId = SubActivityListDictionary["Work completion"] });

            CheckListItemList.Add(new ChecklistItem() { Name = "Selection of farmers, area, species and numbers", ChecklistID = CheckListDictionary["Horticulture - AH"], SubActivityId = SubActivityListDictionary["Selection of farmers, area, species and numbers"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Pits digging", ChecklistID = CheckListDictionary["Horticulture - AH"], SubActivityId = SubActivityListDictionary["Pits digging"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Procurement of plants from nursery", ChecklistID = CheckListDictionary["Horticulture - AH"], SubActivityId = SubActivityListDictionary["Procurement of plants from nursery"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Plantation", ChecklistID = CheckListDictionary["Horticulture - AH"], SubActivityId = SubActivityListDictionary["Plantation"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Muster/material payment", ChecklistID = CheckListDictionary["Horticulture - AH"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Aftercare plan preperation with farmers", ChecklistID = CheckListDictionary["Horticulture - AH"], SubActivityId = SubActivityListDictionary["Aftercare plan preperation with farmers"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Work completion", ChecklistID = CheckListDictionary["Horticulture - AH"], SubActivityId = SubActivityListDictionary["Work completion"] });

            CheckListItemList.Add(new ChecklistItem() { Name = "Selection of farmers, area, species and numbers", ChecklistID = CheckListDictionary["Horticulture - DH"], SubActivityId = SubActivityListDictionary["Selection of farmers, area, species and numbers"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Pits digging", ChecklistID = CheckListDictionary["Horticulture - DH"], SubActivityId = SubActivityListDictionary["Pits digging"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Procurement of plants from nursery", ChecklistID = CheckListDictionary["Horticulture - DH"], SubActivityId = SubActivityListDictionary["Procurement of plants from nursery"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Plantation", ChecklistID = CheckListDictionary["Horticulture - DH"], SubActivityId = SubActivityListDictionary["Plantation"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Muster/material payment", ChecklistID = CheckListDictionary["Horticulture - DH"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Aftercare plan preperation with farmers", ChecklistID = CheckListDictionary["Horticulture - DH"], SubActivityId = SubActivityListDictionary["Aftercare plan preperation with farmers"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Work completion", ChecklistID = CheckListDictionary["Horticulture - DH"], SubActivityId = SubActivityListDictionary["Work completion"] });

            CheckListItemList.Add(new ChecklistItem() { Name = "Area identification", ChecklistID = CheckListDictionary["Afforestation - Plantation"], SubActivityId = SubActivityListDictionary["Area identification"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "VDC/JFM/GP resolution", ChecklistID = CheckListDictionary["Afforestation - Plantation"], SubActivityId = SubActivityListDictionary["VDC/JFM/GP resolution"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Selection of species and numbers", ChecklistID = CheckListDictionary["Afforestation - Plantation"], SubActivityId = SubActivityListDictionary["Selection of species and numbers"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Procurement of plants from nursery", ChecklistID = CheckListDictionary["Afforestation - Plantation"], SubActivityId = SubActivityListDictionary["Procurement of plants from nursery"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Plantation", ChecklistID = CheckListDictionary["Afforestation - Plantation"], SubActivityId = SubActivityListDictionary["Plantation"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Muster/material payment", ChecklistID = CheckListDictionary["Afforestation - Plantation"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Aftercare plan preperation with VDC/JFMC/GP", ChecklistID = CheckListDictionary["Afforestation - Plantation"], SubActivityId = SubActivityListDictionary["Aftercare plan preperation with VDC/JFMC/GP"] });
            CheckListItemList.Add(new ChecklistItem() { Name = "Work completion", ChecklistID = CheckListDictionary["Afforestation - Plantation"], SubActivityId = SubActivityListDictionary["Work completion"] });

            foreach (var CheckListIte in CheckListItemList)
            {
                var _CheckListItemName = CheckListIte.Name.Trim();
                long? checkListId = CheckListIte.ChecklistID;
                var CheckListItemObj = _context.ChecklistItem.FirstOrDefault(d => d.Name == _CheckListItemName && d.ChecklistID == checkListId);

                if (CheckListItemObj == null)
                {
                    _context.ChecklistItem.Add(new ChecklistItem { Name = CheckListIte.Name, ChecklistID = CheckListIte.ChecklistID, SubActivityId = CheckListIte.SubActivityId, TenantId = _TentantId, CreatorUserId = 2 });
                }
            }
            _context.SaveChanges();

            var QuestionaryDictionary = _context.Questionarie.ToDictionary(c => c.Name, c => c.Id);
            var SubActivityDictionary = _context.SubActivity.ToDictionary(c => c.Name, c => c.Id);
            List<SubActivityQuestionaryMapping> SubActivityQuestionaryMappingList = new List<SubActivityQuestionaryMapping>();

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Name of the Village section planned"], SubActivityId = SubActivityListDictionary["Net planning"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Number of farmers present for PNP"], SubActivityId = SubActivityListDictionary["Net planning"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Number of VDC members present for PNP"], SubActivityId = SubActivityListDictionary["Net planning"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Number of Women present for PNP"], SubActivityId = SubActivityListDictionary["Net planning"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Photograph of field exercise"], SubActivityId = SubActivityListDictionary["Net planning"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["decision(yes/no)"], SubActivityId = SubActivityListDictionary["Submission of MDB file"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["decision(yes/no)"], SubActivityId = SubActivityListDictionary["PNP data entry"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Agriculture Land covered, ha"], SubActivityId = SubActivityListDictionary["Prepare Map"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Community/GP Land covered, ha"], SubActivityId = SubActivityListDictionary["Prepare Map"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Forest Land Covered,ha"], SubActivityId = SubActivityListDictionary["Prepare Map"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["WAT + deep CCT_Area covered,ha"], SubActivityId = SubActivityListDictionary["Prepare Map"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["CCT + SCT_Area_covered,ha"], SubActivityId = SubActivityListDictionary["Prepare Map"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Farm Bund + RFB + SB + CB + GB_Area_covered,ha"], SubActivityId = SubActivityListDictionary["Prepare Map"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["WAT + deep CCT_budget,rs"], SubActivityId = SubActivityListDictionary["Prepare Map"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["CCT + SCT_budget,rs"], SubActivityId = SubActivityListDictionary["Prepare Map"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Farm Bund + RFB + SB + CB + GB_budget,rs"], SubActivityId = SubActivityListDictionary["Prepare Map"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["decision(yes/no)"], SubActivityId = SubActivityListDictionary["Prepare weekly work execution plan (Labor/machine reuirement estimation)"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["decision(yes/no)"], SubActivityId = SubActivityListDictionary["Labor/machine procurement"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["decision(yes/no)"], SubActivityId = SubActivityListDictionary["Layout of work"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Number of Labours working on site"], SubActivityId = SubActivityListDictionary["Work execution"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Number of Machines working on site"], SubActivityId = SubActivityListDictionary["Work execution"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Treatment Name"], SubActivityId = SubActivityListDictionary["Work execution"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Upload sample photos"], SubActivityId = SubActivityListDictionary["Quality Monitoring"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["decision(yes/no)"], SubActivityId = SubActivityListDictionary["Measurements verification"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["decision(yes/no)"], SubActivityId = SubActivityListDictionary["Data entry- muster"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Upload photo of VDC resolution"], SubActivityId = SubActivityListDictionary["VDC approval to muster"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Names of Families selected for Impact"], SubActivityId = SubActivityListDictionary["Muster payment"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Upload photo of muster sanction note"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Labour Payment,Rs"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Machine Payment, Rs"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Material Payment, Rs."], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Miscalleneous Payment,Rs"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Supervision Payment,Rs"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Contribution/Shramadan"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["labourdays"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["machinedays"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["supervisor days"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["WAT + deep CCT_Area covered,ha"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["CCT + SCT_Area_covered,ha"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Farm Bund + RFB + SB + CB + GB_Area_covered,ha"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["WAT + deep CCT_budget,rs"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["CCT + SCT_budget,rs"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Farm Bund + RFB + SB + CB + GB_budget,rs"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["WAT + deep CCT_Famer selected for Impact study"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["CCT + SCT_Famer selected for Impact study"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Farm Bund + RFB + SB + CB + GB_Famer selected for Impact study"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["WAT + deep CCT_well selected for Impact study"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["CCT + SCT_well selected for Impact study"], SubActivityId = SubActivityListDictionary["Muster completion"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Farm Bund + RFB + SB + CB + GB_well selected for Impact study"], SubActivityId = SubActivityListDictionary["Muster completion"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["decision(yes/no)"], SubActivityId = SubActivityListDictionary["Area identification"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Upload photo of the site - Max 2"], SubActivityId = SubActivityListDictionary["Site selection"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Upload photo of VDC resolution- Max 1"], SubActivityId = SubActivityListDictionary["Farmer's consent/ VDC approval"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["decision(yes/no)"], SubActivityId = SubActivityListDictionary["Prepare and submit design & estimate"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Location Details"], SubActivityId = SubActivityListDictionary["Prepare and submit list of benefiting farmers & wells"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Nearest Gat number"], SubActivityId = SubActivityListDictionary["Prepare and submit list of benefiting farmers & wells"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Names of Benefitting farmers"], SubActivityId = SubActivityListDictionary["Prepare and submit list of benefiting farmers & wells"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Type of Structure"], SubActivityId = SubActivityListDictionary["Prepare and submit list of benefiting farmers & wells"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Upload photos- Max 2"], SubActivityId = SubActivityListDictionary["Layout & Foundation"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Upload photo of muster, bills, vouchers"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Gabion Structure,Numbers Completed"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Gabion Structure,Rs"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Earthen Nala Bund ,Rs"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Earthen Nala Bund , Completed"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Cement Nala Bund,Rs"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Cement Nala Bund , Completed"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Labour Payment,Rs"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Machine Payment, Rs"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });

            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Material Payment, Rs."], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Miscalleneous Payment,Rs"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Supervision Payment,Rs"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Contribution/Shramadan"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            //SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Machine Payment, Rs"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            //SubActivityQuestionaryMappingList.Add(new SubActivityQuestionaryMapping() { QuestionarieID = QuestionaryDictionary["Machine Payment, Rs"], SubActivityId = SubActivityListDictionary["Muster/material payment"] });
            var subActivityQuestionaryMappings = _context.SubActivityQuestionaryMapping.IgnoreQueryFilters().ToList();

            foreach (var SubActMapList in SubActivityQuestionaryMappingList)
            {
                //var _CheckListItemName = SubActMapList.Name.Trim();
                long? SubActivtyId = SubActMapList.SubActivityId;
                long? QuestionarieID = SubActMapList.QuestionarieID;
                var SubActMapItemObj = subActivityQuestionaryMappings.FirstOrDefault(d => d.SubActivityId == SubActivtyId && d.QuestionarieID == QuestionarieID);

                if (SubActMapItemObj == null)
                {
                    _context.SubActivityQuestionaryMapping.Add(new SubActivityQuestionaryMapping { QuestionarieID = SubActMapList.QuestionarieID, SubActivityId = SubActMapList.SubActivityId, TenantId = _TentantId, CreatorUserId = 2 });
                }
            }


             }  
            catch(Exception ex)
            {
                throw ;
            }
        }
    }
}
