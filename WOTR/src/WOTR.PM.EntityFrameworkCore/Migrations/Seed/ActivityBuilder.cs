﻿using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.EntityFrameworkCore;

namespace WOTR.PM.Migrations.Seed
{
    public class ActivityBuilder
    {
        private readonly PMDbContext _context;
        public IAbpSession AbpSession { get; set; }
        private readonly int _TentantId;

        public ActivityBuilder(PMDbContext context, int TentantId)
        {
            _context = context;
            _TentantId = TentantId;
        }
        public void create()
        {

            var UnitOfmeasureDictionary = _context.UnitofMeasures.IgnoreQueryFilters().ToDictionary(u => u.Name, u => u.Id );


            List<Activity> ActivityList = new List<Activity>();
            
            ActivityList.Add(new Activity() { Name = "Awareness Programs" , UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Village Exposure", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "VDC details", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Horticulture - DH", UnitOfMeasuresId = UnitOfmeasureDictionary["Ha"] });
            ActivityList.Add(new Activity() { Name = "Horticulture - AH", UnitOfMeasuresId = UnitOfmeasureDictionary["Ha"] });
            ActivityList.Add(new Activity() { Name = "Major DLT Structures", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Minor DLT Structures", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Area Treatment", UnitOfMeasuresId = UnitOfmeasureDictionary["Ha"] });
            ActivityList.Add(new Activity() { Name = "Drudgery reduction Activity", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Mahila Melawa", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Wasundhara Sevika Training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Wasundhara sevika details", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Gender Training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "SMS Training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "SMS details", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "SHG Training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "SHG details", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Women Exposure", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Automated Weather Stations(AWS)", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Agro - advisory farmers", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Water Budgeting", UnitOfMeasuresId = UnitOfmeasureDictionary["Villages"] });
            ActivityList.Add(new Activity() { Name = "Water Budgeting - Display Board", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Farm Ponds", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Sunken Pond", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Well - Individual", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Well - Common", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Well Recharge", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Drip Irrigation", UnitOfMeasuresId = UnitOfmeasureDictionary["Ha"] });
            ActivityList.Add(new Activity() { Name = "Sprinkler irrigation", UnitOfMeasuresId = UnitOfmeasureDictionary["Ha"] });
            ActivityList.Add(new Activity() { Name = "Other micro irrigation systems", UnitOfMeasuresId = UnitOfmeasureDictionary["Ha"] });
            ActivityList.Add(new Activity() { Name = "Lift Irrigation system", UnitOfMeasuresId = UnitOfmeasureDictionary["Ha"] });
            ActivityList.Add(new Activity() { Name = "Fodder Demo", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Livestock camps", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Livestock owner training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Local breed conservation", UnitOfMeasuresId = UnitOfmeasureDictionary["breeds"] });
            ActivityList.Add(new Activity() { Name = "Water trough", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "DRR Activity", UnitOfMeasuresId = UnitOfmeasureDictionary["Villages"] });
            ActivityList.Add(new Activity() { Name = "DRR Mapping", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "DRR Committee Training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "DRR Proposal", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "DRR Committee", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Participatory biodiversity register(PBR)", UnitOfMeasuresId = UnitOfmeasureDictionary["Villages"] });
            ActivityList.Add(new Activity() { Name = "Biodiversity Management Comitee details", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Trainings to BMCs", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Organic Formulations", UnitOfMeasuresId = UnitOfmeasureDictionary["Farmers"] });
            ActivityList.Add(new Activity() { Name = "Soil Health Cards", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Soil testing", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Afforestation - Plantation", UnitOfMeasuresId = UnitOfmeasureDictionary["Ha"] });
            ActivityList.Add(new Activity() { Name = "Joint Forest Management Committee details", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Wasundhara sevak / Jalsevak / Para agronomist details", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Wasundhara Sevak / Jalsevak trainings", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "VDC Trainings", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Linkage Building Workshops", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "PRI Trainings", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "VDC Melava", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Monthly Review meeting", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Picnic", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Solar Home light systems", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Trainings for FPOs", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Farmer Producer Company details", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Farmer Exposure", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Farmer training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Farmer Field Schools", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Compost", UnitOfMeasuresId = UnitOfmeasureDictionary["Farmers"] });
            ActivityList.Add(new Activity() { Name = "Agri Demonstration",  UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "SCI Demonstrations", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Exposures Visits for FPOs", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
           
            ActivityList.Add(new Activity() { Name = "Cordination meeting", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Atmdarshan Training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Development and preperation of training kit", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "TOT for WOTR staff", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Other consultancy assignments", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Exposure of other institutions", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Exposure dialouge program", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Demand based training programs - International level", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Demand based training programs - National level", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "In house staff training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Sponsered training Programs", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Innovative activity", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Diagnostic camps", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Kitchen Garden Activities", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Best village competitions", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Toilets", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Livelihood Assessment Workshop", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Livelihood Training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Individual Livelihood Activity", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Group Livelihood Activity", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Drinking water projects / units", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "School Drinking water project", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Hot Water Chullah", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Smokleless Chullah", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Street Lights", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Health camps", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Anemia ditection camp", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Adolescent Trainings", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Food Demos", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Growth Monitoring", UnitOfMeasuresId = UnitOfmeasureDictionary["Children"] });
            ActivityList.Add(new Activity() { Name = "Health promoter details", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Health Promoter training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Vehicle Hiring", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Vehicle Insurance", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Annual Maintenance", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Books & Periodicals", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Equipment maintanance across WOTR", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Electricity", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Hardware Maintenance(IT etc.)",  UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Insurance(Other)", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Office expenses", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Office Running Expenses", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Pantry and Other", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Postage and Courier", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Printing and Stationary", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Public Relation", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "RRC Office rent", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Running Expenses", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Telephone", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Taxes", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "IT related capitals", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Vehicles", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Other capital items", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Regular Personnel  Expenses", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Other personell cost", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Retreat", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Wind mills", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Solar Water Pumps", UnitOfMeasuresId = UnitOfmeasureDictionary["Ha"] });
            ActivityList.Add(new Activity() { Name = "Community Biogas units", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Individual Biogas units", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Staff sponser International Training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Fuel & Toll", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Property Maintainance", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Personal Related Cost(Hiring charges of drivers)", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Property Tax", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Repair & Maintenance - Vehicle", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Taxes(RTO)", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Staff sponser for Training", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name = "Travel Allowance", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            ActivityList.Add(new Activity() { Name =  "Staff sponser for MDP", UnitOfMeasuresId = UnitOfmeasureDictionary["No"] });
            try
            {

                foreach (var item in ActivityList)
                {
                    var _dname = item.Name.Trim();
                    var dobject = _context.Activity.IgnoreQueryFilters().FirstOrDefault(d => d.Name == _dname);

                   
                    if (dobject == null)
                    {
                        _context.Activity.Add(
                            new Activity
                            {

                                Name = _dname,
                                TenantId = _TentantId,
                                Description = "Description",
                                CreatorUserId = 2,
                                UnitOfMeasuresId = item.UnitOfMeasuresId

                            });

                    }

                }

                _context.SaveChanges();


            }
            catch (Exception)
            {

                throw;
            }
        }

    }

}
