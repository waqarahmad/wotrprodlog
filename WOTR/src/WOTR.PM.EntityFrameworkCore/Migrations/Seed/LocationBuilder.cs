﻿using Abp.Runtime.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.EntityFrameworkCore;
using WOTR.PM.Locations;

namespace WOTR.PM.Migrations
{
    public class LocationBuilder
    {
        private readonly PMDbContext _context;
        public IAbpSession AbpSession { get; set; }
        private readonly int _tenantId;
        public LocationBuilder(PMDbContext contex, int tenantId)
        {
            _context = contex;
            _tenantId = tenantId;
        }

        public void Create()
        {

            try
            {
                int x = 0;
               
                List<string> locationList = new List<string>()
            { "Ahmednagar(HQ)",
                    "Aurangabad-RRC","Sangamner-RRC","Ahmednagar-Parner-RRC","Ahmednagar-Pathardi-RRC","Yeotmal-RRC","Pune-RRC","Ahmednagar-RRC"
                ,"A.P. Hydrabad-RRC","M.P. Jabalpur-RRC","Jharkhand-Ranchi-RRC","Rajasthan–Udaipur-RRC"};


                

                foreach (string locationName in locationList)
                {
                   
                    var locationObject = _context.Location.FirstOrDefault(x1 => x1.Name.ToUpper() == locationName.ToUpper());
                    var stateDictionary = _context.State.ToDictionary(s => s.StateName, s => s.Id);

                   
                    if (locationObject == null)
                    {
                        if (locationName == "Ahmednagar(HQ)")
                        {
                            _context.Location.Add(
                           new Location
                           {
                               Name = locationName,
                               ParentLocation = 0,
                               StateID = stateDictionary["Maharashtra"],
                                
                                TenantId = _tenantId,
                               LocationType = LocationType.HQ

                           });
                            _context.SaveChanges();
                        }
                        
                        if (locationName!= "Ahmednagar(HQ)")
                        {
                            x = _context.Location.Where(u => u.Name == "Ahmednagar(HQ)").SingleOrDefault().Id;
                        }
                       


                        if  ( locationName == "Aurangabad-RRC"||
                            locationName == "Sangamner-RRC" || locationName == "Ahmednagar-Parner-RRC" ||
                            locationName == "Ahmednagar-Pathardi-RRC" || locationName == "Yeotmal-RRC"
                            || locationName == "Pune-RRC" || locationName == "Ahmednagar-RRC")
                        {
                            _context.Location.Add(
                            new Location
                            {
                               
                                Name = locationName,
                                ParentLocation = 0,
                                StateID = stateDictionary["Maharashtra"],
                                
                                TenantId = _tenantId,
                                LocationType = LocationType.RRC

                            });
                        }

                        if (locationName== "A.P. Hydrabad-RRC")
                        {
                            _context.Location.Add(
                           new Location
                           {
                               Name = locationName,
                               ParentLocation = 0,
                               StateID = stateDictionary["Andhra Pradesh"],

                               TenantId = _tenantId,
                               LocationType = LocationType.RRC

                           });
                        }

                        if (locationName == "M.P. Jabalpur-RRC")
                        {
                            _context.Location.Add(
                           new Location
                           {
                               Name = locationName,
                               ParentLocation = 0,
                               StateID = stateDictionary["Madhya Pradesh"],

                               TenantId = _tenantId,
                               LocationType = LocationType.RRC

                           });
                        }

                        if (locationName == "Jharkhand-Ranchi-RRC")
                        {
                            _context.Location.Add(
                           new Location
                           {
                               Name = locationName,
                               ParentLocation = 0,
                               StateID = stateDictionary["Jharkhand"],

                               TenantId = _tenantId,
                               LocationType = LocationType.RRC

                           });
                        }

                        if (locationName == "Rajasthan–Udaipur-RRC")
                        {
                            _context.Location.Add(
                           new Location
                           {
                               Name = locationName,
                               ParentLocation = 0,
                               StateID = stateDictionary["Rajasthan"],

                               TenantId = _tenantId,
                               LocationType = LocationType.RRC

                           });
                        }
                        
                    }
                }

            }
            catch (Exception ex)
            {
            }

        }
    }

}

