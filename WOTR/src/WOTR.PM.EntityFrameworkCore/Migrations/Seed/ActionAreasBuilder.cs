﻿using Abp.Runtime.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ActionAreas;
using WOTR.PM.EntityFrameworkCore;

namespace WOTR.PM.Migrations.Seed
{
    /* ActionAreasBuilder Class
    Auther
    Anuj Adkine
    19/02/2018*/
    public class ActionAreasBuilder
    {
        private readonly PMDbContext _context;
        private readonly int _tenantId;
        public IAbpSession AbpSession { get; set; }
        public ActionAreasBuilder(PMDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void create()
        {
            List<string> ActionAreaList = new List<string>() {"Implementation of Projects( Watershed, Climate Change, Livelihood, Drinking Water, Sustainable Agricultuure, FPCs, etc) ",
                 "Training and Consultancy Services",
                "Upgrdation of Facilities and Institutional Development of WOTR",
                "Reaserch",
                "Dcumentation and Communication",
        "Networking, Advocacy and Linkage Building",
        "Application of IT to all projects",
        "Adminstration and Establishment"
            };
            int code = 1;

            try
            {
                foreach (string item in ActionAreaList)
                {
                    var type = _context.ActionArea.FirstOrDefault(t => t.Name.ToUpper() == item.ToUpper());
                    if (type == null)
                    {
                        _context.ActionArea.Add(
                            new ActionArea
                            {
                                Name = item,
                                TenantId = _tenantId,
                                Code = code.ToString(),
                                CreatorUserId = 2

                            });
                        code++;
                    }

                }

                _context.SaveChanges();

            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
