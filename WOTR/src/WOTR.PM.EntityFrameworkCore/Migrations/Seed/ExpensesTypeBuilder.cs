﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.EntityFrameworkCore;

namespace WOTR.PM.Migrations.Seed
{
  public   class ExpensesTypeBuilder
    {

        private readonly PMDbContext _context;

        public ExpensesTypeBuilder(PMDbContext contex)
        {
            _context = contex;
        }
        public void create()
        {




            List<string> TypeName = new List<string>()
            { " Labour Payment", "Machine Payment", "Material Payment",
                "Miscalleneous Payment", "Supervision Payment", " Stone Gully plugs",
                "Loose Boulder structures", "Earthen Gully Plugs" ,"Work","Travel","ADVANCE","Other"};
            foreach (string tyName in TypeName)
            {
                var ctry = _context.ExpensesType.FirstOrDefault(p => p.TypeName == tyName);
                if (ctry == null)

                    _context.ExpensesType.Add(
             new ProgramExpenses.ExpensesType
             {
                 TypeName = tyName,
                 TenantId=1
             });
            }

        }

    }
}
