﻿using System;
using System.Transactions;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.Uow;
using Abp.MultiTenancy;
using Microsoft.EntityFrameworkCore;
using WOTR.PM.EntityFrameworkCore;
using WOTR.PM.Migrations.Seed.Host;
using WOTR.PM.Migrations.Seed.Tenants;

namespace WOTR.PM.Migrations.Seed
{
    public static class SeedHelper
    {
        public static void SeedHostDb(IIocResolver iocResolver)
        {
            WithDbContext<PMDbContext>(iocResolver, SeedHostDb);
        }

        public static void SeedHostDb(PMDbContext context)
        {
            context.SuppressAutoSetTenantId = true;

            //Host seed
            new InitialHostDbBuilder(context).Create();

            //Default tenant seed (in host database).
            new DefaultTenantBuilder(context).Create();
            new TenantRoleAndUserBuilder(context, 1).Create();
            new StateBuilder(context,1).create();
            new LocationBuilder(context, 1).Create();
            new ActionAreasBuilder(context, 1).create();
            new SubActionAreasBuilder(context, 1).create();
            new UnitofMeasureBuilder(context, 1).create();
            new CountryBuilder(context).create();
            new ExpensesTypeBuilder(context).create();
            
            new ActivityBuilder(context, 1).create();
            new QuestionarieBuilder(context,1).create();
         //   new SubActivityBuilder(context, 1).create();  // this line is commented by sachin due to dublicate key
            //new DistrictBuilder(context).create();
        }

        private static void WithDbContext<TDbContext>(IIocResolver iocResolver, Action<TDbContext> contextAction)
            where TDbContext : DbContext
        {
            using (var uowManager = iocResolver.ResolveAsDisposable<IUnitOfWorkManager>())
            {
                using (var uow = uowManager.Object.Begin(TransactionScopeOption.Suppress))
                {
                    var context = uowManager.Object.Current.GetDbContext<TDbContext>(MultiTenancySides.Host);

                    contextAction(context);

                    uow.Complete();
                }
            }
        }
    }
}
