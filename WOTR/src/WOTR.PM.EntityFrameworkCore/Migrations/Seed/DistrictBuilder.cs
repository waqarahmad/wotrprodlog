﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.EntityFrameworkCore;
using WOTR.PM.States;

namespace WOTR.PM.Migrations.Seed
{
    class DistrictBuilder
    {
        private readonly PMDbContext _context;

        public DistrictBuilder(PMDbContext context)
        {
            _context = context;
        }

        public void create()
        {
            //var maharashtraID = _context.State.Where(x => x.StateName == "Maharashtra").FirstOrDefault();

            List<Tuple<int, string>> districts = new List<Tuple<int, string>>()
            {
                #region Maharashtra 36 
                
                new Tuple<int, string> ( 18, "Ahmednagar"),
                new Tuple<int, string> ( 18, "Akola"),
                new Tuple<int, string> ( 18, "Amaravati"),
                new Tuple<int, string> ( 18, "Aurangabad"),
                new Tuple<int, string> ( 18, "Beed"),
                new Tuple<int, string> ( 18, "Bhandara"),
                new Tuple<int, string> ( 18, "Buldhanax"),
                new Tuple<int, string> ( 18, "Chandrapur"),
                new Tuple<int, string> ( 18, "Dhule"),
                new Tuple<int, string> ( 18, "Gadchiroli"),
                new Tuple<int, string> ( 18, "Gondia"),
                new Tuple<int, string> ( 18, "Hingoli"),
                new Tuple<int, string> ( 18, "Jalgaon"),
                new Tuple<int, string> ( 18, "Jalnax"),
                new Tuple<int, string> ( 18, "Kolhapur"),
                new Tuple<int, string> ( 18, "Latur"),
                new Tuple<int, string> ( 18, "Mumbai City"),
                new Tuple<int, string> ( 18, "Mumbai Suburban"),
                new Tuple<int, string> ( 18, "Nagpur"),
                new Tuple<int, string> ( 18, "Nanded"),
                new Tuple<int, string> ( 18, "Nandurbar"),
                new Tuple<int, string> ( 18, "Nashik"),
                new Tuple<int, string> ( 18, "Osmanabad"),
                new Tuple<int, string> ( 18, "Palghar"),
                new Tuple<int, string> ( 18, "Parbhani"),
                new Tuple<int, string> ( 18, "Pune"),
                new Tuple<int, string> ( 18, "Raigad"),
                new Tuple<int, string> ( 18, "Ratnagiri"),
                new Tuple<int, string> ( 18, "Sangli"),
                new Tuple<int, string> ( 18, "Satara"),
                new Tuple<int, string> ( 18, "Sindhudurg"),
                new Tuple<int, string> ( 18, "Solapur"),
                new Tuple<int, string> ( 18, "Thane"),
                new Tuple<int, string> ( 18, "Wardha"),
                new Tuple<int, string> ( 18, "Washim"),
                new Tuple<int, string> ( 18, "Yavatmal"),
                #endregion

                #region Bihar 38
                new Tuple<int, string> ( 1, "Araria"),
                new Tuple<int, string> ( 1, "Arwal"),
                new Tuple<int, string> ( 1, "AuragabadB"),
                new Tuple<int, string> ( 1, "Banka"),
                new Tuple<int, string> ( 1, "Begusarai"),
                new Tuple<int, string> ( 1, "Bhagalpur"),
                new Tuple<int, string> ( 1, "Bhojpur"),
                new Tuple<int, string> ( 1, "Buxar"),
                new Tuple<int, string> ( 1, "Darbhanga"),
                new Tuple<int, string> ( 1, "East Champaran"),
                new Tuple<int, string> ( 1, "Gaya"),
                new Tuple<int, string> ( 1, "Gopalgani"),
                new Tuple<int, string> ( 1, "Jamui"),
                new Tuple<int, string> ( 1, "Jehanabad"),
                new Tuple<int, string> ( 1, "Kaimur"),
                new Tuple<int, string> ( 1, "Katihar"),
                new Tuple<int, string> ( 1, "Khagaria"),
                new Tuple<int, string> ( 1, "Kishanganj"),
                new Tuple<int, string> ( 1, "Lakhisarai"),
                new Tuple<int, string> ( 1, "Madhepura"),
                new Tuple<int, string> ( 1, "Madhubani"),
                new Tuple<int, string> ( 1, "Munger"),
                new Tuple<int, string> ( 1, "Muzaffarpur"),
                new Tuple<int, string> ( 1, "Nalanda"),
                new Tuple<int, string> ( 1, "Nawada"),
                new Tuple<int, string> ( 1, "Patna"),
                new Tuple<int, string> ( 1, "Purnia"),
                new Tuple<int, string> ( 1, "Rohtas"),
                new Tuple<int, string> ( 1, "Saharsa"),
                new Tuple<int, string> ( 1, "Samastipur"),
                new Tuple<int, string> ( 1, "Saran"),
                new Tuple<int, string> ( 1, "Sheikhpura"),
                new Tuple<int, string> ( 1, "Sheohar"),
                new Tuple<int, string> ( 1, "Sitamarhi"),
                new Tuple<int, string> ( 1, "Siwan"),
                new Tuple<int, string> ( 1, "Supaul"),
                new Tuple<int, string> ( 1, "Vaishali"),
                new Tuple<int, string> ( 1, "West Champaran"),
                #endregion

                #region Jharkhand 24
                new Tuple<int, string> ( 2, "Bokaro"),
                new Tuple<int, string> ( 2, "Chatra"),
                new Tuple<int, string> ( 2, "Deoghar"),
                new Tuple<int, string> ( 2, "Dhanbad"),
                new Tuple<int, string> ( 2, "Dumka"),
                new Tuple<int, string> ( 2, "East Singhbhum"),
                new Tuple<int, string> ( 2, "Garhwa"),
                new Tuple<int, string> ( 2, "Giridih"),
                new Tuple<int, string> ( 2, "Godda"),
                new Tuple<int, string> ( 2, "Gumla"),
                new Tuple<int, string> ( 2, "Hazaribagh"),
                new Tuple<int, string> ( 2, "Jamtara"),
                new Tuple<int, string> ( 2, "Khunti"),
                new Tuple<int, string> ( 2, "Koderma"),
                new Tuple<int, string> ( 2, "Latehar"),
                new Tuple<int, string> ( 2, "Lohardaga"),
                new Tuple<int, string> ( 2, "Pakur"),
                new Tuple<int, string> ( 2, "Palamu"),
                new Tuple<int, string> ( 2, "Ramgarh"),
                new Tuple<int, string> ( 2, "Ranchi"),
                new Tuple<int, string> ( 2, "Sahebganj"),
                new Tuple<int, string> ( 2, "Seraikela Kharsawan"),
                new Tuple<int, string> ( 2, "Simdega"),
                new Tuple<int, string> ( 2, "West Singhbhum"),
                #endregion

                #region UTTARAKHAND 13
                new Tuple<int, string> ( 3, "Almora"),
                new Tuple<int, string> ( 3, "Bageshwar"),
                new Tuple<int, string> ( 3, "Chamoli"),
                new Tuple<int, string> ( 3, "Champawat"),
                new Tuple<int, string> ( 3, "Dehradun"),
                new Tuple<int, string> ( 3, "Haridwar"),
                new Tuple<int, string> ( 3, "Nanital"),
                new Tuple<int, string> ( 3, "Pauri"),
                new Tuple<int, string> ( 3, "Pithoragarh"),
                new Tuple<int, string> ( 3, "Rudraprayag"),
                new Tuple<int, string> ( 3, "Tehri"),
                new Tuple<int, string> ( 3, "Udham Singh Nagar"),
                new Tuple<int, string> ( 3, "Uttarkashi"),

                #endregion

                #region CHATTISGARH 27
                new Tuple<int, string> ( 4, "Balod"),
                new Tuple<int, string> ( 4, "Baloda Bazar"),
                new Tuple<int, string> ( 4, "Balrampur"),
                new Tuple<int, string> ( 4, "Bastar"),
                new Tuple<int, string> ( 4, "Bemetara"),
                new Tuple<int, string> ( 4, "Bijapur"),
                new Tuple<int, string> ( 4, "Bilaspur"),
                new Tuple<int, string> ( 4, "Dantewada"),
                new Tuple<int, string> ( 4, "Dhamtari"),
                new Tuple<int, string> ( 4, "Durg"),
                new Tuple<int, string> ( 4, "Gariaband"),
                new Tuple<int, string> ( 4, "Janjgir Champsa"),
                new Tuple<int, string> ( 4, "Jashpur"),
                new Tuple<int, string> ( 4, "Kabirdham"),
                new Tuple<int, string> ( 4, "Kanker"),
                new Tuple<int, string> ( 4, "Kondagaon"),
                new Tuple<int, string> ( 4, "Korba"),
                new Tuple<int, string> ( 4, "Koriya"),
                new Tuple<int, string> ( 4, "Mahasamund"),
                new Tuple<int, string> ( 4, "Mungeli"),
                new Tuple<int, string> ( 4, "Narayanpur(Chattisgarh)"),
                new Tuple<int, string> ( 4, "Rajgarh"),
                new Tuple<int, string> ( 4, "Raipur"),
                new Tuple<int, string> ( 4, "Rajnandgaon"),
                new Tuple<int, string> ( 4, "Sukma"),
                new Tuple<int, string> ( 4, "Surajpur"),
                new Tuple<int, string> ( 4, "Surguja"),
                #endregion

                #region WEST BENGAL 23
                new Tuple<int, string> ( 5, "Alipurduar"),
                new Tuple<int, string> ( 5, "Bankura"),
                new Tuple<int, string> ( 5, "Birbhum"),
                new Tuple<int, string> ( 5, "Cooch Behar"),
                new Tuple<int, string> ( 5, "Dakshin Dinajpur"),
                new Tuple<int, string> ( 5, "Darjeeling"),
                new Tuple<int, string> ( 5, "Hooghly"),
                new Tuple<int, string> ( 5, "Howrah"),
                new Tuple<int, string> ( 5, "Jalpaiguri"),
                new Tuple<int, string> ( 5, "Jhargram"),
                new Tuple<int, string> ( 5, "Kalimpong"),
                new Tuple<int, string> ( 5, "Kolkata"),
                new Tuple<int, string> ( 5, "Malda"),
                new Tuple<int, string> ( 5, "Murshidabad"),
                new Tuple<int, string> ( 5, "Nadia"),
                new Tuple<int, string> ( 5, "North 24 Parganas"),
                new Tuple<int, string> ( 5, "Paschim Bardhaman"),
                new Tuple<int, string> ( 5, "Paschim Medinipur"),
                new Tuple<int, string> ( 5, "Purba Bardhaman"),
                new Tuple<int, string> ( 5, "Purba Medinipur"),
                new Tuple<int, string> ( 5, "Purulia"),
                new Tuple<int, string> ( 5, "South 24 Parganas"),
                new Tuple<int, string> ( 5, "Uttar Dinajpur"),
                #endregion

                #region UTTAR PRADESH 75
                new Tuple<int, string> ( 6, "Aligarh"),
                new Tuple<int, string> ( 6, "Allahabad"),
                new Tuple<int, string> ( 6, "Ambedkar Nagar"),
                new Tuple<int, string> ( 6, "Amethi"),
                new Tuple<int, string> ( 6, "Amroha"),
                new Tuple<int, string> ( 6, "Auraiya"),
                new Tuple<int, string> ( 6, "Azamgarh"),
                new Tuple<int, string> ( 6, "Baghpat"),
                new Tuple<int, string> ( 6, "Bahraich"),
                new Tuple<int, string> ( 6, "Ballia"),
                new Tuple<int, string> ( 6, "Balrampur"),
                new Tuple<int, string> ( 6, "Banda"),
                new Tuple<int, string> ( 6, "Barabanki"),
                new Tuple<int, string> ( 6, "Bareilly"),
                new Tuple<int, string> ( 6, "Basti"),
                new Tuple<int, string> ( 6, "Bhadohi"),
                new Tuple<int, string> ( 6, "Bijnor"),
                new Tuple<int, string> ( 6, "Budaun"),
                new Tuple<int, string> ( 6, "Bulandshahr"),
                new Tuple<int, string> ( 6, "Chandauli"),
                new Tuple<int, string> ( 6, "Chitrakoot"),
                new Tuple<int, string> ( 6, "Deoria"),
                new Tuple<int, string> ( 6, "Etah"),
                new Tuple<int, string> ( 6, "Etawah"),
                new Tuple<int, string> ( 6, "Faizabad"),
                new Tuple<int, string> ( 6, "Farrukhabad"),
                new Tuple<int, string> ( 6, "Fatehpur"),
                new Tuple<int, string> ( 6, "Firozabad"),
                new Tuple<int, string> ( 6, "Gautam Buddha Nagar"),
                new Tuple<int, string> ( 6, "Ghaziabad"),
                new Tuple<int, string> ( 6, "Ghazipur"),
                new Tuple<int, string> ( 6, "Gonda"),
                new Tuple<int, string> ( 6, "Gorakhpur"),
                new Tuple<int, string> ( 6, "Hamirpur"),
                new Tuple<int, string> ( 6, "Hapur"),
                new Tuple<int, string> ( 6, "Hardoi"),
                new Tuple<int, string> ( 6, "Hathras"),
                new Tuple<int, string> ( 6, "Jalaun"),
                new Tuple<int, string> ( 6, "Jaunpur"),
                new Tuple<int, string> ( 6, "Jhansi"),
                new Tuple<int, string> ( 6, "Kannauj"),
                new Tuple<int, string> ( 6, "Kanpur Dehat"),
                new Tuple<int, string> ( 6, "Kanpur Nagar"),
                new Tuple<int, string> ( 6, "Kasganj"),
                new Tuple<int, string> ( 6, "Kaushambi"),
                new Tuple<int, string> ( 6, "Kheri"),
                new Tuple<int, string> ( 6, "Kishinagar"),
                new Tuple<int, string> ( 6, "Lalitpur"),
                new Tuple<int, string> ( 6, "Lucknow"),
                new Tuple<int, string> ( 6, "Maharajganj"),
                new Tuple<int, string> ( 6, "Mahoba"),
                new Tuple<int, string> ( 6, "Mainpuri"),
                new Tuple<int, string> ( 6, "Mathura"),
                new Tuple<int, string> ( 6, "Mau"),
                new Tuple<int, string> ( 6, "Meerut"),
                new Tuple<int, string> ( 6, "Mirzapur"),
                new Tuple<int, string> ( 6, "Moradabad"),
                new Tuple<int, string> ( 6, "Muzaffarnagar"),
                new Tuple<int, string> ( 6, "Pilibhit"),
                new Tuple<int, string> ( 6, "Pratapgarh"),
                new Tuple<int, string> ( 6, "Raebareli"),
                new Tuple<int, string> ( 6, "Rampur"),
                new Tuple<int, string> ( 6, "Saharanpur"),
                new Tuple<int, string> ( 6, "Sambhal"),
                new Tuple<int, string> ( 6, "Sant Kabir Nagar"),
                new Tuple<int, string> ( 6, "Shahjahanpur"),
                new Tuple<int, string> ( 6, "Shamli"),
                new Tuple<int, string> ( 6, "Shravasti"),
                new Tuple<int, string> ( 6, "SidharthNagar"),
                new Tuple<int, string> ( 6, "Sitapur"),
                new Tuple<int, string> ( 6, "Sonbhadra"),
                new Tuple<int, string> ( 6, "Sultanpur"),
                new Tuple<int, string> ( 6, "Unnao"),
                new Tuple<int, string> ( 6, "Varanasi"),
                #endregion
            };


            foreach(var distList in districts)
            {

                var dist1 = _context.District.FirstOrDefault(x => x.DistrictName == distList.Item2);

                if (dist1 == null)
                {
                    _context.District.Add(
                        new District
                        {
                            StateId = distList.Item1,
                            DistrictName = distList.Item2
                        });
                }  
            }




            //var stateList = _context.State.ToList();
            //if(stateList != null)
            //{
            //    foreach(var st in stateList)
            //    {

            //    }
            //}
        }


    }

    
}
