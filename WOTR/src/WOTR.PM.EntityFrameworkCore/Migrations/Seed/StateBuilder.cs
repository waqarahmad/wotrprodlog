﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.EntityFrameworkCore;
using WOTR.PM.Locations;
using WOTR.PM.States;

namespace WOTR.PM.Migrations.Seed
{
    public class StateBuilder
    {
        private readonly PMDbContext _context;
        private readonly int _tenantId;
        public StateBuilder(PMDbContext contex, int TenantId)
        {
            _context = contex;
            _tenantId = TenantId;
        }

        public void create()
        {

            try
            {
                #region STATE SEED
                List<string> stateNames = new List<string>()
                    { "Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Goa","Gujarat","Haryana","Himachal Pradesh",
                    "Jammu & Kashmir","Karnataka","Kerala","Madhya Pradesh",
                    "Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland",
                    "Orissa","Punjab","Rajasthan","Sikkim","Tamil Nadu","Tripura",
                    "Uttar Pradesh","West Bengal","Chhattisgarh","Uttarakhand","Jharkhand","Telangana"};

                var states = _context.State.ToList();
                foreach (string stateName in stateNames)
                {
                    var d1 = states.FirstOrDefault(x => x.StateName == stateName);

                    if (d1 == null)
                    {
                        _context.State.Add(
                            new State
                            {
                                StateName = stateName,
                                // TenantId = 1

                            });

                    }

                }

                // --------------- R_Modifications starts --------------------------------
                _context.SaveChanges();

                #endregion

                #region DISTRICT SEED
                var stateDictionary = _context.State.ToDictionary(s => s.StateName, s => s.Id);

                List<District> districtList = new List<District>();

                #region Maharashtra(36)
                districtList.Add(new District() { DistrictName = "Ahmednagar", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Akola", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Amravati", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Aurangabad(Maharashtra)", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Beed", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Bhandara", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Buldhana", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Chandrapur", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Dhule", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Gadchiroli", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Gondiya", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Hingoli", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Jalgaon", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Jalna", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Kolhapur", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Latur", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Mumbai City", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Mumbai Suburban", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Nagpur", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Nanded", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Nandurbar", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Nashik", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Osmanabad", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Palghar", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Parbhani", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Pune", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Raigad", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Ratnagiri", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Sangli", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Satara", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Sindhudurg", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Solapur", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Thane", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Wardha", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Washim", StateId = stateDictionary["Maharashtra"] });
                districtList.Add(new District() { DistrictName = "Yavatmal", StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Andhra Pradesh(13)
                districtList.Add(new District() { DistrictName = "Anantapur", StateId = stateDictionary["Andhra Pradesh"] });
                districtList.Add(new District() { DistrictName = "Chittoor", StateId = stateDictionary["Andhra Pradesh"] });
                districtList.Add(new District() { DistrictName = "East Godavari", StateId = stateDictionary["Andhra Pradesh"] });
                districtList.Add(new District() { DistrictName = "Guntur", StateId = stateDictionary["Andhra Pradesh"] });
                districtList.Add(new District() { DistrictName = "Kadapa", StateId = stateDictionary["Andhra Pradesh"] });
                districtList.Add(new District() { DistrictName = "Krishna", StateId = stateDictionary["Andhra Pradesh"] });
                districtList.Add(new District() { DistrictName = "Kurnool", StateId = stateDictionary["Andhra Pradesh"] });
                districtList.Add(new District() { DistrictName = "Nellore", StateId = stateDictionary["Andhra Pradesh"] });
                districtList.Add(new District() { DistrictName = "Prakasam", StateId = stateDictionary["Andhra Pradesh"] });
                districtList.Add(new District() { DistrictName = "Srikakulam", StateId = stateDictionary["Andhra Pradesh"] });
                districtList.Add(new District() { DistrictName = "Visakhapatnam", StateId = stateDictionary["Andhra Pradesh"] });
                districtList.Add(new District() { DistrictName = "Vizianagaram", StateId = stateDictionary["Andhra Pradesh"] });
                districtList.Add(new District() { DistrictName = "West Godavari", StateId = stateDictionary["Andhra Pradesh"] });
                #endregion

                #region Jharkhand(24)
                districtList.Add(new District() { DistrictName = "Bokaro", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Chatra", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Deoghar", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Dhanbad", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Dumka", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "East Singhbhum", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Garhwa", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Giridih", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Godda", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Gumla", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Hazaribagh", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Jamtara", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Khunti", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Koderma", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Latehar", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Lohardaga", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Pakur", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Palamu", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Ramgarh", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Ranchi", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Sahebganj", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Seraikela Kharsawan", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "Simdega", StateId = stateDictionary["Jharkhand"] });
                districtList.Add(new District() { DistrictName = "West Singhbhum", StateId = stateDictionary["Jharkhand"] });
                #endregion

                #region Rajasthan(33)
                districtList.Add(new District() { DistrictName = "Ajmer", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Alwar", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Banswara", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Baran", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Barmer", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Bharatpur", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Bhilwara", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Bikaner", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Bundi", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Chittorgarh", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Churu", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Dausa", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Dholpur", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Dungarpur", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Ganganagar", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Hanumangarh", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Jaipur", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Jaisalmer", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Jalore", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Jhalawar", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Jhunjhunu", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Jodhpur", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Karauli", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Kota", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Nagaur", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Pali", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Pratapgarh", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Rajsamand", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Sawai Madhopur", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Sikar", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Sirohi", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Tonk", StateId = stateDictionary["Rajasthan"] });
                districtList.Add(new District() { DistrictName = "Udaipur", StateId = stateDictionary["Rajasthan"] });
                #endregion

                #region Madhya Pradesh(51)
                districtList.Add(new District() { DistrictName = "Agar Malwa", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Alirajpur", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Anuppur", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Ashoknagar", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Balaghat", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Barwani", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Betul", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Bhind", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Bhopal", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Burhanpur", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Chhatarpur", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Chhindwara", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Damoh", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Datia", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Dewas", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Dhar", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Dindori", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Guna", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Gwalior", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Harda", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Hoshangabad", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Indore", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Jabalpur", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Jhabua", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Katni", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Khandwa", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Khargone", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Mandla", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Mandsaur", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Morena", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Narsinghpur", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Neemuch", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Panna", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Raisen", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Rajgarh", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Ratlam", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Rewa", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Sagar", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Satna", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Sehore", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Seoni", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Shahdol", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Shajapur", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Sheopur", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Shivpuri", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Sidhi", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Singrauli", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Tikamgarh", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Ujjain", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Umaria", StateId = stateDictionary["Madhya Pradesh"] });
                districtList.Add(new District() { DistrictName = "Vidisha", StateId = stateDictionary["Madhya Pradesh"] });
                #endregion



                #region Commented Districts

                #region Bihar(38)
                //districtList.Add(new District() { DistrictName = "Araria", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Arwal", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Auragabad(Bihar)", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Banka", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Begusarai", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Bhagalpur", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Bhojpur", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Buxar", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Darbhanga", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "East Champaran", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Gaya", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Gopalganj", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Jamui", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Jehanabad", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Kaimur", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Katihar", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Khagaria", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Kishanganj", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Lakhisarai", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Madhepura", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Madhubani", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Munger", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Muzaffarpur", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Nalanda", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Nawada", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Patna", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Purnia", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Rohtas", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Saharsa", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Samastipur", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Saran", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Sheikhpura", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Sheohar", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Sitamarhi", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Siwan", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Supaul", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "Vaishali", StateId = stateDictionary["Bihar"] });
                //districtList.Add(new District() { DistrictName = "West Champaran", StateId = stateDictionary["Bihar"] });
                #endregion

                #region Goa(2)
                //districtList.Add(new District() { DistrictName = "North Goa", StateId = stateDictionary["Goa"] });
                //districtList.Add(new District() { DistrictName = "South Goa", StateId = stateDictionary["Goa"] });
                #endregion

                #region Sikkim(4)
                //districtList.Add(new District() { DistrictName = "East Sikkim", StateId = stateDictionary["Sikkim"] });
                //districtList.Add(new District() { DistrictName = "North Sikkim", StateId = stateDictionary["Sikkim"] });
                //districtList.Add(new District() { DistrictName = "South Sikkim", StateId = stateDictionary["Sikkim"] });
                //districtList.Add(new District() { DistrictName = "West Sikkim", StateId = stateDictionary["Sikkim"] });
                #endregion

                #region Tripura(8)
                //districtList.Add(new District() { DistrictName = "Dhalai", StateId = stateDictionary["Tripura"] });
                //districtList.Add(new District() { DistrictName = "Gomati", StateId = stateDictionary["Tripura"] });
                //districtList.Add(new District() { DistrictName = "Khowai", StateId = stateDictionary["Tripura"] });
                //districtList.Add(new District() { DistrictName = "North Tripura", StateId = stateDictionary["Tripura"] });
                //districtList.Add(new District() { DistrictName = "Sepahijala", StateId = stateDictionary["Tripura"] });
                //districtList.Add(new District() { DistrictName = "South Tripura", StateId = stateDictionary["Tripura"] });
                //districtList.Add(new District() { DistrictName = "Unakoti", StateId = stateDictionary["Tripura"] });
                //districtList.Add(new District() { DistrictName = "West Tripura", StateId = stateDictionary["Tripura"] });
                #endregion

                #region Mizoram(8)
                //districtList.Add(new District() { DistrictName = "Aizawl", StateId = stateDictionary["Mizoram"] });
                //districtList.Add(new District() { DistrictName = "Champhai", StateId = stateDictionary["Mizoram"] });
                //districtList.Add(new District() { DistrictName = "Kolasib", StateId = stateDictionary["Mizoram"] });
                //districtList.Add(new District() { DistrictName = "Lawngtlai", StateId = stateDictionary["Mizoram"] });
                //districtList.Add(new District() { DistrictName = "Lunglei", StateId = stateDictionary["Mizoram"] });
                //districtList.Add(new District() { DistrictName = "Mamit", StateId = stateDictionary["Mizoram"] });
                //districtList.Add(new District() { DistrictName = "Saiha", StateId = stateDictionary["Mizoram"] });
                //districtList.Add(new District() { DistrictName = "Serchhip", StateId = stateDictionary["Mizoram"] });
                #endregion

                #region Nagaland(11)
                //districtList.Add(new District() { DistrictName = "Dimapur", StateId = stateDictionary["Nagaland"] });
                //districtList.Add(new District() { DistrictName = "Kiphire", StateId = stateDictionary["Nagaland"] });
                //districtList.Add(new District() { DistrictName = "Kohima", StateId = stateDictionary["Nagaland"] });
                //districtList.Add(new District() { DistrictName = "Longleng", StateId = stateDictionary["Nagaland"] });
                //districtList.Add(new District() { DistrictName = "Mokokchung", StateId = stateDictionary["Nagaland"] });
                //districtList.Add(new District() { DistrictName = "Mon", StateId = stateDictionary["Nagaland"] });
                //districtList.Add(new District() { DistrictName = "Peren", StateId = stateDictionary["Nagaland"] });
                //districtList.Add(new District() { DistrictName = "Phek", StateId = stateDictionary["Nagaland"] });
                //districtList.Add(new District() { DistrictName = "Tuensang", StateId = stateDictionary["Nagaland"] });
                //districtList.Add(new District() { DistrictName = "Wokha", StateId = stateDictionary["Nagaland"] });
                //districtList.Add(new District() { DistrictName = "Zunheboto", StateId = stateDictionary["Nagaland"] });
                #endregion

                #region Uttarakhand(13)
                //districtList.Add(new District() { DistrictName = "Almora", StateId = stateDictionary["Uttarakhand"] });
                //districtList.Add(new District() { DistrictName = "Bageshwar", StateId = stateDictionary["Uttarakhand"] });
                //districtList.Add(new District() { DistrictName = "Chamoli", StateId = stateDictionary["Uttarakhand"] });
                //districtList.Add(new District() { DistrictName = "Champawat", StateId = stateDictionary["Uttarakhand"] });
                //districtList.Add(new District() { DistrictName = "Dehradun", StateId = stateDictionary["Uttarakhand"] });
                //districtList.Add(new District() { DistrictName = "Haridwar", StateId = stateDictionary["Uttarakhand"] });
                //districtList.Add(new District() { DistrictName = "Nainital", StateId = stateDictionary["Uttarakhand"] });
                //districtList.Add(new District() { DistrictName = "Pauri", StateId = stateDictionary["Uttarakhand"] });
                //districtList.Add(new District() { DistrictName = "Pithoragarh", StateId = stateDictionary["Uttarakhand"] });
                //districtList.Add(new District() { DistrictName = "Rudraprayag", StateId = stateDictionary["Uttarakhand"] });
                //districtList.Add(new District() { DistrictName = "Tehri", StateId = stateDictionary["Uttarakhand"] });
                //districtList.Add(new District() { DistrictName = "Udham Singh Nagar", StateId = stateDictionary["Uttarakhand"] });
                //districtList.Add(new District() { DistrictName = "Uttarkashi", StateId = stateDictionary["Uttarakhand"] });
                #endregion

                #region Manipur(15)
                //districtList.Add(new District() { DistrictName = "Bishnupur", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Chandel", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Churachandpur", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Imphal East", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Imphal West", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Jiribam", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Kakching", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Kamjong", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Kangpokpi", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Noney", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Pherzawl", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Senapati", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Tamenglong", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Tengnoupal", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Thoubal", StateId = stateDictionary["Manipur"] });
                //districtList.Add(new District() { DistrictName = "Ukhrul", StateId = stateDictionary["Manipur"] });
                #endregion

                #region Meghalaya(11)
                //districtList.Add(new District() { DistrictName = "East Garo Hills", StateId = stateDictionary["Meghalaya"] });
                //districtList.Add(new District() { DistrictName = "East Jaintia Hills", StateId = stateDictionary["Meghalaya"] });
                //districtList.Add(new District() { DistrictName = "East Khasi Hills", StateId = stateDictionary["Meghalaya"] });
                //districtList.Add(new District() { DistrictName = "North Garo Hills", StateId = stateDictionary["Meghalaya"] });
                //districtList.Add(new District() { DistrictName = "Ri Bhoi", StateId = stateDictionary["Meghalaya"] });
                //districtList.Add(new District() { DistrictName = "South Garo Hills", StateId = stateDictionary["Meghalaya"] });
                //districtList.Add(new District() { DistrictName = "South West Garo Hills", StateId = stateDictionary["Meghalaya"] });
                //districtList.Add(new District() { DistrictName = "South West Khasi Hills", StateId = stateDictionary["Meghalaya"] });
                //districtList.Add(new District() { DistrictName = "West Garo Hills", StateId = stateDictionary["Meghalaya"] });
                //districtList.Add(new District() { DistrictName = "West Jaintia Hills", StateId = stateDictionary["Meghalaya"] });
                //districtList.Add(new District() { DistrictName = "West Khasi Hills", StateId = stateDictionary["Meghalaya"] });
                #endregion

                #region West Bengal(23)
                //districtList.Add(new District() { DistrictName = "Alipurduar", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Bankura", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Birbhum", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Cooch Behar", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Dakshin Dinajpur", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Darjeeling", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Hooghly", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Howrah", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Jalpaiguri", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Jhargram", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Kalimpong", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Kolkata", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Malda", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Murshidabad", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Nadia", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "North 24 Parganas", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Paschim Bardhaman", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Paschim Medinipur", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Purba Bardhaman", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Purba Medinipur", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Purulia", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "South 24 Parganas", StateId = stateDictionary["West Bengal"] });
                //districtList.Add(new District() { DistrictName = "Uttar Dinajpur", StateId = stateDictionary["West Bengal"] });
                #endregion

                #region Panjab(22)
                //districtList.Add(new District() { DistrictName = "Amritsar", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Barnala", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Bathinda", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Faridkot", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Fatehgarh Sahib", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Fazilka", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Firozpur", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Gurdaspur", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Hoshiarpur", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Jalandhar", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Kapurthala", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Ludhiana", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Mansa", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Moga", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Mohali", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Muktsar", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Pathankot", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Patiala", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Rupnagar", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Sangrur", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Shaheed Bhagat Singh Nagar", StateId = stateDictionary["Punjab"] });
                //districtList.Add(new District() { DistrictName = "Tarn Taran", StateId = stateDictionary["Punjab"] });
                #endregion

                #region Telangana(31)
                //districtList.Add(new District() { DistrictName = "Adilabad", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Bhadradri Kothagudem", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Hyderabad", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Jagtial", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Jangaon", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Jayashankar", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Jogulamba", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Kamareddy", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Karimnagar", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Khammam", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Komaram Bheem", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Mahabubabad", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Mahbubnagar", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Mancherial", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Medak", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Medchal", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Nagarkurnool", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Nalgonda", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Nirmal", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Nizamabad", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Peddapalli", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Rajanna Sircilla", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Ranga Reddy", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Sangareddy", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Siddipet", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Suryapet", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Vikarabad", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Wanaparthy", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Warangal Rural", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Warangal Urban", StateId = stateDictionary["Telangana"] });
                //districtList.Add(new District() { DistrictName = "Yadadri Bhuvanagiri", StateId = stateDictionary["Telangana"] });
                #endregion

                #region Tamilnadu(32)
                //districtList.Add(new District() { DistrictName = "Ariyalur", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Chennai", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Coimbatore", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Cuddalore", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Dharmapuri", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Dindigul", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Erode", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Kanchipuram", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Kanyakumari", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Karur", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Krishnagiri", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Madurai", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Nagapattinam", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Namakkal", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Nilgiris", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Perambalur", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Pudukkottai", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Ramanathapuram", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Salem", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Sivaganga", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Thanjavur", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Theni", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Thoothukudi", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Tiruchirappalli", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Tirunelveli", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Tiruppur", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Tiruvallur", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Tiruvannamalai", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Tiruvarur", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Vellore", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Viluppuram", StateId = stateDictionary["Tamil Nadu"] });
                //districtList.Add(new District() { DistrictName = "Virudhunagar", StateId = stateDictionary["Tamil Nadu"] });
                #endregion



                #region Uttar Pradesh(75)
                //districtList.Add(new District() { DistrictName = "Agra", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Aligarh", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Allahabad", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Ambedkar Nagar", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Amethi", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Amroha", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Auraiya", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Azamgarh", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Baghpat", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Bahraich", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Ballia", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Balrampur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Banda", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Barabanki", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Bareilly", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Basti", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Bhadohi", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Bijnor", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Budaun", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Bulandshahr", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Chandauli", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Chitrakoot", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Deoria", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Etah", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Etawah", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Faizabad", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Farrukhabad", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Fatehpur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Firozabad", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Gautam Buddha Nagar", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Ghaziabad", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Ghazipur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Gonda", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Gorakhpur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Hamirpur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Hapur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Hardoi", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Hathras", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Jalaun", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Jaunpur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Jhansi", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Kannauj", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Kanpur Dehat", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Kanpur Nagar", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Kasganj", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Kaushambi", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Kheri", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Kushinagar", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Lalitpur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Lucknow", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Maharajganj", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Mahoba", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Mainpuri", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Mathura", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Mau", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Meerut", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Mirzapur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Moradabad", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Muzaffarnagar", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Pilibhit", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Pratapgarh", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Raebareli", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Rampur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Saharanpur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Sambhal", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Sant Kabir Nagar", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Shahjahanpur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Shamli", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Shravasti", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Siddharthnagar", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Sitapur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Sonbhadra", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Sultanpur", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Unnao", StateId = stateDictionary["Uttar Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Varanasi", StateId = stateDictionary["Uttar Pradesh"] });
                #endregion

                #region Orissa(30)
                //districtList.Add(new District() { DistrictName = "Angul", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Balangir", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Balasore", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Bargarh", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Bhadrak", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Boudh", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Cuttack", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Debagarh", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Dhenkanal", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Gajapati", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Ganjam", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Jagatsinghpur", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Jajpur", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Jharsuguda", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Kalahandi", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Kandhamal", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Kendrapara", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Kendujhar", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Khordha", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Koraput", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Malkangiri", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Mayurbhanj", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Nabarangpur", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Nayagarh", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Nuapada", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Puri", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Rayagada", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Sambalpur", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Subarnapur", StateId = stateDictionary["Orissa"] });
                //districtList.Add(new District() { DistrictName = "Sundergarh", StateId = stateDictionary["Orissa"] });
                #endregion



                #region Himachal Pradesh(12)
                //districtList.Add(new District() { DistrictName = "Bilaspur", StateId = stateDictionary["Himachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Chamba", StateId = stateDictionary["Himachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Hamirpur(HP)", StateId = stateDictionary["Himachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Kangra", StateId = stateDictionary["Himachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Kinnaur", StateId = stateDictionary["Himachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Kullu", StateId = stateDictionary["Himachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Lahaul Spiti", StateId = stateDictionary["Himachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Mandi", StateId = stateDictionary["Himachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Shimla", StateId = stateDictionary["Himachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Sirmaur", StateId = stateDictionary["Himachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Solan", StateId = stateDictionary["Himachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Una", StateId = stateDictionary["Himachal Pradesh"] });
                #endregion

                #region Kerala(14)
                //districtList.Add(new District() { DistrictName = "Alappuzha", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Ernakulam", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Idukki", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Kannur", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Kasaragod", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Kollam", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Kottayam", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Kozhikode", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Malappuram", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Palakkad", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Pathanamthitta", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Thiruvananthapuram", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Thrissur", StateId = stateDictionary["Kerala"] });
                //districtList.Add(new District() { DistrictName = "Wayanad", StateId = stateDictionary["Kerala"] });
                #endregion

                #region Haryana(22)
                //districtList.Add(new District() { DistrictName = "Ambala", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Bhiwani", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Charkhi Dadri", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Faridabad", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Fatehabad", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Gurugram*", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Hisar", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Jhajjar", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Jind", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Kaithal", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Karnal", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Kurukshetra", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Mahendragarh", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Mewat", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Palwal", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Panchkula", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Panipat", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Rewari", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Rohtak", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Sirsa", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Sonipat", StateId = stateDictionary["Haryana"] });
                //districtList.Add(new District() { DistrictName = "Yamunanagar", StateId = stateDictionary["Haryana"] });
                #endregion

                #region Jammu & Kashmir(22)
                //districtList.Add(new District() { DistrictName = "Anantnag", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Bandipora", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Baramulla", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Budgam", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Doda", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Ganderbal", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Jammu", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Kargil", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Kathua", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Kishtwar", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Kulgam", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Kupwara", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Leh", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Poonch", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Pulwama", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Rajouri", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Ramban", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Reasi", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Samba", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Shopian", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Srinagar", StateId = stateDictionary["Jammu & Kashmir"] });
                //districtList.Add(new District() { DistrictName = "Udhampur", StateId = stateDictionary["Jammu & Kashmir"] });
                #endregion

                #region Arunachal Pradesh(21)
                //districtList.Add(new District() { DistrictName = "Anjaw", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Central Siang", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Changlang", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Dibang Valley", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "East Kameng", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "East Siang", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Kra Daadi", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Kurung Kumey", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Lohit", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Longding", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Lower Dibang Valley", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Lower Siang", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Lower Subansiri", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Namsai", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Papum Pare", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Tawang", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Tirap", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Upper Siang", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "Upper Subansiri", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "West Kameng", StateId = stateDictionary["Arunachal Pradesh"] });
                //districtList.Add(new District() { DistrictName = "West Siang", StateId = stateDictionary["Arunachal Pradesh"] });
                #endregion

                #region Karnataka(30)
                //districtList.Add(new District() { DistrictName = "Bagalkot", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Bangalore Rural", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Bangalore Urban", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Belgaum", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Bellary", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Bidar", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Bijapur", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Chamarajanagar", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Chikkaballapur", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Chikkamagaluru", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Chitradurga", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Dakshina Kannada", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Davanagere", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Dharwad", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Gadag", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Gulbarga", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Hassan", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Haveri", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Kodagu", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Kolar", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Koppal", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Mandya", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Mysore", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Raichur", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Ramanagara", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Shimoga", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Tumkur", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Udupi", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Uttara Kannada", StateId = stateDictionary["Karnataka"] });
                //districtList.Add(new District() { DistrictName = "Yadgir", StateId = stateDictionary["Karnataka"] });
                #endregion

                #region Assam(33)
                //districtList.Add(new District() { DistrictName = "Baksa", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Barpeta", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Biswanath", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Bongaigaon", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Cachar", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Charaideo", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Chirang", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Darrang", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Dhemaji", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Dhubri", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Dibrugarh", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Dima Hasao", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Goalpara", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Golaghat", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Hailakandi", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Hojai", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Jorhat", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Kamrup", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Kamrup Metropolitan", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Karbi Anglong", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Karimganj", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Kokrajhar", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Lakhimpur", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Majuli", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Morigaon", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Nagaon", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Nalbari", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Sivasagar", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Sonitpur", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "South Salmara-Mankachar", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Tinsukia", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "Udalguri", StateId = stateDictionary["Assam"] });
                //districtList.Add(new District() { DistrictName = "West Karbi Anglong", StateId = stateDictionary["Assam"] });
                #endregion

                #region Chhattisgarh(27)
                //districtList.Add(new District() { DistrictName = "Balod", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Baloda Bazar", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Balrampur", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Bastar", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Bemetara", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Bijapur(Chhattisgarh)", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Bilaspur(Chhattisgarh)", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Dantewada", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Dhamtari", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Durg", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Gariaband", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Janjgir Champa", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Jashpur", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Kabirdham", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Kanker", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Kondagaon", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Korba", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Koriya", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Mahasamund", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Mungeli", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Narayanpur", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Raigarh", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Raipur", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Rajnandgaon", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Sukma", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Surajpur", StateId = stateDictionary["Chhattisgarh"] });
                //districtList.Add(new District() { DistrictName = "Surguja", StateId = stateDictionary["Chhattisgarh"] });
                #endregion

                #region Gujarat(33)
                //districtList.Add(new District() { DistrictName = "Ahmedabad", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Amreli", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Anand", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Aravalli", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Banaskantha", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Bharuch", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Bhavnagar", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Botad", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Chhota Udaipur", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Dahod", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Dang", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Devbhoomi Dwarka", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Gandhinagar", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Gir Somnath", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Jamnagar", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Junagadh", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Kheda", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Kutch", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Mahisagar", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Mehsana", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Morbi", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Narmada", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Navsari", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Panchmahal", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Patan", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Porbandar", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Rajkot", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Sabarkantha", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Surat", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Surendranagar", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Tapi", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Vadodara", StateId = stateDictionary["Gujarat"] });
                //districtList.Add(new District() { DistrictName = "Valsad", StateId = stateDictionary["Gujarat"] });
                #endregion

                #endregion

                var districtsList = _context.District.IgnoreQueryFilters().ToList();

                foreach (var dist in districtList)
                {
                    var _districtName = dist.DistrictName.Trim();
                    //var districtObject = _context.District.IgnoreQueryFilters().FirstOrDefault(d => d.StateId == dist.StateId);
                    //var districtObject = _context.District.IgnoreQueryFilters().FirstOrDefault(d => d.DistrictName == dist.DistrictName);
                    var districtObject = districtsList.FirstOrDefault(d => d.DistrictName == _districtName);

                    if (districtObject == null)
                    {
                        //_context.District.Add(new District { StateId = dist.StateId, DistrictName = dist.DistrictName });
                        _context.District.Add(new District { StateId = dist.StateId, DistrictName = _districtName, CreatorUserId = 2, TenantId = _tenantId });
                    }
                }

                _context.SaveChanges();
                #endregion

                #region TALUKA SEED

                //var distList = _context.District.Select(d => new { Name = d.DistrictName, distID = d.Id });
                var districtDictionary = _context.District.ToDictionary(d => d.DistrictName, d => d.Id);

                List<Taluka> talukaList = new List<Taluka>();

                #region MAHARASTRA 

                #region PUNE(13)
                talukaList.Add(new Taluka() { TalukaName = "Ambegaon", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Indapur", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Khed", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Junnar", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Daund", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Purandar", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Baramati", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bhor", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mawal", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mulshi", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Velhe", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shirur", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Haveli", DistrictId = districtDictionary["Pune"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Solapur(11)
                talukaList.Add(new Taluka() { TalukaName = "Solapur North", DistrictId = districtDictionary["Solapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Malshiras", DistrictId = districtDictionary["Solapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Pandharpur", DistrictId = districtDictionary["Solapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Barshi", DistrictId = districtDictionary["Solapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Madha", DistrictId = districtDictionary["Solapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sangole", DistrictId = districtDictionary["Solapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Akkalkot", DistrictId = districtDictionary["Solapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mohol", DistrictId = districtDictionary["Solapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Solapur South", DistrictId = districtDictionary["Solapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Karmala", DistrictId = districtDictionary["Solapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mangalvedhe", DistrictId = districtDictionary["Solapur"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Thane(14)
                talukaList.Add(new Taluka() { TalukaName = "Kalyan", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Vasai", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bhivandi", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ambarnath", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Palghar", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ulhasnagar", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Dahanu", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shahapur", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Murbad", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Vada", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Talasari", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Jawhar", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Vikramgad", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mokhada", DistrictId = districtDictionary["Thane"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Nashik(14)
                talukaList.Add(new Taluka() { TalukaName = "Malegaon", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Niphad", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Baglan", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sinnar", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Dondori", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Nandgaon", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Yevla", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Igatpuri", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Chandvad", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kalwan", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Surgana", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Trimbakeshwar", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Deola", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Peint", DistrictId = districtDictionary["Nashik"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Aurangabad
                talukaList.Add(new Taluka() { TalukaName = "Aurangabad", DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sillod", DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Gangapur", DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Paithan", DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kannad", DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Vijapur", DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Phulambri", DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Khuldabad", DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Soegaon", DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Satara(10)
                talukaList.Add(new Taluka() { TalukaName = "Karad", DistrictId = districtDictionary["Satara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Phaltan", DistrictId = districtDictionary["Satara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Patan", DistrictId = districtDictionary["Satara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Khatav", DistrictId = districtDictionary["Satara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Koregaon", DistrictId = districtDictionary["Satara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Man", DistrictId = districtDictionary["Satara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Wai", DistrictId = districtDictionary["Satara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Khandala", DistrictId = districtDictionary["Satara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Jaoli", DistrictId = districtDictionary["Satara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mahabaleshwar", DistrictId = districtDictionary["Satara"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Amravati(13)
                talukaList.Add(new Taluka() { TalukaName = "Amravati", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Achalpur", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Warud", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Chandurbazar", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Dharni", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Morshi", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Daryapur", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Anjangaon Surji", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Dhamangaon Railway", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Nandgaon-Khandeshwar", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Chikhaldara", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bhatkuli", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Teosa", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Chandrapur Railway", DistrictId = districtDictionary["Amravati"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Nagpur(14)
                talukaList.Add(new Taluka() { TalukaName = "Nagpur Urban", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Nagpur Rural", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Hingna", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kamptee", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Savner", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Katol", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ramtek", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Umred", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Narkhed", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Parseoni", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mauda", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kuhi", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kalameshwar", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bhiwapur", DistrictId = districtDictionary["Nagpur"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Ahmednagar(13)
                talukaList.Add(new Taluka() { TalukaName = "Sangamner", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Nevasa", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Rahuri", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Rahta", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shrigonda", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kopargaon", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Akola", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shrirampur", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Parner", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Pathardi", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shevgaon", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Karjat", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Jamkhed", DistrictId = districtDictionary["Ahmednagar"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Jalgaon(14)
                talukaList.Add(new Taluka() { TalukaName = "Chalisgaon", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bhusawal", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Jamner", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Chopda", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Raver", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Pachora", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Amalner", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Yawal", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Parola", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Dharangaon", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Erandol", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Muktainagar", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bhadgaon", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bodvad", DistrictId = districtDictionary["Jalgaon"], StateId = stateDictionary["Maharashtra"] });

                #endregion

                #region Kolhapur(12)
                talukaList.Add(new Taluka() { TalukaName = "Karvir", DistrictId = districtDictionary["Kolhapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Hatkanangle", DistrictId = districtDictionary["Kolhapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shirol", DistrictId = districtDictionary["Kolhapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kagal", DistrictId = districtDictionary["Kolhapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Panhala", DistrictId = districtDictionary["Kolhapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Gadhinglaj", DistrictId = districtDictionary["Kolhapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Radhanagari", DistrictId = districtDictionary["Kolhapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Chandgad", DistrictId = districtDictionary["Kolhapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shahuwadi", DistrictId = districtDictionary["Kolhapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bhudargad", DistrictId = districtDictionary["Kolhapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ajra", DistrictId = districtDictionary["Kolhapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bavda", DistrictId = districtDictionary["Kolhapur"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Nanded(15)
                talukaList.Add(new Taluka() { TalukaName = "Mukhed", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Hadgaon", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kandhar", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kinwat", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Loha", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Deglur", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Naigaon(Khairgaon)", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Biloli", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bhokar", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mudkhed", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Himayatnagar", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ardhapur", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mahoor", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Umri", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Dharmabad", DistrictId = districtDictionary["Nanded"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Sangli(10)
                talukaList.Add(new Taluka() { TalukaName = "Miraj", DistrictId = districtDictionary["Sangli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Walwa", DistrictId = districtDictionary["Sangli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Jat", DistrictId = districtDictionary["Sangli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Tasgaon", DistrictId = districtDictionary["Sangli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Khanapur(Vita)", DistrictId = districtDictionary["Sangli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Palus", DistrictId = districtDictionary["Sangli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shirala", DistrictId = districtDictionary["Sangli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kavathemahankal", DistrictId = districtDictionary["Sangli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kadegaon", DistrictId = districtDictionary["Sangli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Atpadi", DistrictId = districtDictionary["Sangli"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Yavatmal(15)
                talukaList.Add(new Taluka() { TalukaName = "Pusad", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Umarkhed", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Wani", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Darwha", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mahagaon", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Arni", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kelapur", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Digras", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ghatanji", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ner", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ralegaon", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kalamb", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bahulgaon", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Zari-jamani", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Maregaon", DistrictId = districtDictionary["Yavatmal"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Raigad(15)
                talukaList.Add(new Taluka() { TalukaName = "Panvel", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Alibag", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Karjat(Raigad)", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Khalapur", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Pen", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mahad", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Roha", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Uran", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mangaon", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shrivardhan", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Murud", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sudhagad", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mhasla", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Poladpur", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Tala", DistrictId = districtDictionary["Raigad"], StateId = stateDictionary["Maharashtra"] });

                #endregion

                #region Buldhana(12)
                talukaList.Add(new Taluka() { TalukaName = "Khamgaon", DistrictId = districtDictionary["Buldhana"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Chikhli", DistrictId = districtDictionary["Buldhana"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mehkar", DistrictId = districtDictionary["Buldhana"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Malkapur", DistrictId = districtDictionary["Buldhana"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sindkhed Raja", DistrictId = districtDictionary["Buldhana"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Nandura", DistrictId = districtDictionary["Buldhana"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Motala", DistrictId = districtDictionary["Buldhana"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Jalgaon(jamod)", DistrictId = districtDictionary["Buldhana"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shegaon", DistrictId = districtDictionary["Buldhana"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Lonar", DistrictId = districtDictionary["Buldhana"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sangrampur", DistrictId = districtDictionary["Buldhana"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Deolgaon Raja", DistrictId = districtDictionary["Buldhana"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Beed(10)
                talukaList.Add(new Taluka() { TalukaName = "Georai", DistrictId = districtDictionary["Beed"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Parli", DistrictId = districtDictionary["Beed"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ambejogai", DistrictId = districtDictionary["Beed"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Manjlegaon", DistrictId = districtDictionary["Beed"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kaij", DistrictId = districtDictionary["Beed"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ashti", DistrictId = districtDictionary["Beed"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shirur(Kasar)", DistrictId = districtDictionary["Beed"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Patoda", DistrictId = districtDictionary["Beed"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Dharur", DistrictId = districtDictionary["Beed"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Wadwani", DistrictId = districtDictionary["Beed"], StateId = stateDictionary["Maharashtra"] });

                #endregion

                #region Latur(9)
                talukaList.Add(new Taluka() { TalukaName = "Nilanga", DistrictId = districtDictionary["Latur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Udgir", DistrictId = districtDictionary["Latur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ausa", DistrictId = districtDictionary["Latur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ahmadpur", DistrictId = districtDictionary["Latur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Chakur", DistrictId = districtDictionary["Latur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Renapur", DistrictId = districtDictionary["Latur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Deoni", DistrictId = districtDictionary["Latur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Jalkot", DistrictId = districtDictionary["Latur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shirur-Anantpal", DistrictId = districtDictionary["Latur"], StateId = stateDictionary["Maharashtra"] });

                #endregion

                #region Chandrapur(14)
                talukaList.Add(new Taluka() { TalukaName = "Warora", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Chimur", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Brahmapuri", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bhadravati", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Rajura", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ballarpur", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Nagbhir", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Korpana", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mul", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sindewahi", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sawali", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Gondpipri", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Jiwati", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Pombhurna", DistrictId = districtDictionary["Chandrapur"], StateId = stateDictionary["Maharashtra"] });

                #endregion

                #region Dhule(3)
                talukaList.Add(new Taluka() { TalukaName = "Sakri", DistrictId = districtDictionary["Dhule"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Shirpur", DistrictId = districtDictionary["Dhule"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sindkhede", DistrictId = districtDictionary["Dhule"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Jalna(7)
                talukaList.Add(new Taluka() { TalukaName = "Bhokardan", DistrictId = districtDictionary["Jalna"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ambad", DistrictId = districtDictionary["Jalna"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ghansawangi", DistrictId = districtDictionary["Jalna"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Partur", DistrictId = districtDictionary["Jalna"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mantha", DistrictId = districtDictionary["Jalna"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Jafferabad", DistrictId = districtDictionary["Jalna"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Badnapur", DistrictId = districtDictionary["Jalna"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Parbhani(8)
                talukaList.Add(new Taluka() { TalukaName = "Jintur", DistrictId = districtDictionary["Parbhani"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Gangakhed", DistrictId = districtDictionary["Parbhani"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Purna", DistrictId = districtDictionary["Parbhani"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sailu", DistrictId = districtDictionary["Parbhani"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Pathri", DistrictId = districtDictionary["Parbhani"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Manwath", DistrictId = districtDictionary["Parbhani"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Palam", DistrictId = districtDictionary["Parbhani"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sonpeth", DistrictId = districtDictionary["Parbhani"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Akola(6)
                talukaList.Add(new Taluka() { TalukaName = "Akot", DistrictId = districtDictionary["Akola"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Balapur", DistrictId = districtDictionary["Akola"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Murtijapur", DistrictId = districtDictionary["Akola"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Telhara", DistrictId = districtDictionary["Akola"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Barshitakli", DistrictId = districtDictionary["Akola"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Patur", DistrictId = districtDictionary["Akola"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Osmanabad(7)
                talukaList.Add(new Taluka() { TalukaName = "Tuljapur", DistrictId = districtDictionary["Osmanabad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Umarga", DistrictId = districtDictionary["Osmanabad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kalamb(Osmanabad)", DistrictId = districtDictionary["Osmanabad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Paranda", DistrictId = districtDictionary["Osmanabad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bhum", DistrictId = districtDictionary["Osmanabad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Lohara", DistrictId = districtDictionary["Osmanabad"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Washi", DistrictId = districtDictionary["Osmanabad"], StateId = stateDictionary["Maharashtra"] });

                #endregion

                #region Nandurbar(5)
                talukaList.Add(new Taluka() { TalukaName = "Shahade", DistrictId = districtDictionary["Nandurbar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Nawapur", DistrictId = districtDictionary["Nandurbar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Akkalkuwa", DistrictId = districtDictionary["Nandurbar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Akrani", DistrictId = districtDictionary["Nandurbar"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Talode", DistrictId = districtDictionary["Nandurbar"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Ratnagiri(8)
                talukaList.Add(new Taluka() { TalukaName = "Chiplun", DistrictId = districtDictionary["Ratnagiri"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sangameshwar", DistrictId = districtDictionary["Ratnagiri"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Khed(Ratnagiri)", DistrictId = districtDictionary["Ratnagiri"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Dapoli", DistrictId = districtDictionary["Ratnagiri"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Rajapur", DistrictId = districtDictionary["Ratnagiri"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Guhagar", DistrictId = districtDictionary["Ratnagiri"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Lanja", DistrictId = districtDictionary["Ratnagiri"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mandangad", DistrictId = districtDictionary["Ratnagiri"], StateId = stateDictionary["Maharashtra"] });

                #endregion

                #region Gondiya(7)
                talukaList.Add(new Taluka() { TalukaName = "Tirora", DistrictId = districtDictionary["Gondiya"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Arjuni Morgaon", DistrictId = districtDictionary["Gondiya"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Amgaon", DistrictId = districtDictionary["Gondiya"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Goregaon", DistrictId = districtDictionary["Gondiya"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sadak-Arjuni", DistrictId = districtDictionary["Gondiya"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Deori", DistrictId = districtDictionary["Gondiya"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Salekasa", DistrictId = districtDictionary["Gondiya"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Wardha(7)
                talukaList.Add(new Taluka() { TalukaName = "Hinganghat", DistrictId = districtDictionary["Wardha"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Deoli", DistrictId = districtDictionary["Wardha"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Arvi", DistrictId = districtDictionary["Wardha"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Seloo", DistrictId = districtDictionary["Wardha"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Samudrapur", DistrictId = districtDictionary["Wardha"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Karanja", DistrictId = districtDictionary["Wardha"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Ashti(Wardha)", DistrictId = districtDictionary["Wardha"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Bhandara(6)
                talukaList.Add(new Taluka() { TalukaName = "Tumsar", DistrictId = districtDictionary["Bhandara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Pauni", DistrictId = districtDictionary["Bhandara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mohadi", DistrictId = districtDictionary["Bhandara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sakoli", DistrictId = districtDictionary["Bhandara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Lakhani", DistrictId = districtDictionary["Bhandara"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Lakhandur", DistrictId = districtDictionary["Bhandara"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Washim(5)
                talukaList.Add(new Taluka() { TalukaName = "Karanja(Washim)", DistrictId = districtDictionary["Washim"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Risod", DistrictId = districtDictionary["Washim"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Malegaon(Washim)", DistrictId = districtDictionary["Washim"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mangrulpir", DistrictId = districtDictionary["Washim"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Manora", DistrictId = districtDictionary["Washim"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Hingoli(4)
                talukaList.Add(new Taluka() { TalukaName = "Basmath", DistrictId = districtDictionary["Hingoli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kalamnuri", DistrictId = districtDictionary["Hingoli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sengaon", DistrictId = districtDictionary["Hingoli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Aundha (Nagnath)", DistrictId = districtDictionary["Hingoli"], StateId = stateDictionary["Maharashtra"] });
                #endregion

                #region Gadchiroli(11)
                talukaList.Add(new Taluka() { TalukaName = "Chamorshi", DistrictId = districtDictionary["Gadchiroli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Aheri", DistrictId = districtDictionary["Gadchiroli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Armori", DistrictId = districtDictionary["Gadchiroli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kurkheda", DistrictId = districtDictionary["Gadchiroli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Desaiganj (Vadasa)", DistrictId = districtDictionary["Gadchiroli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Dhanora", DistrictId = districtDictionary["Gadchiroli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Etapalli", DistrictId = districtDictionary["Gadchiroli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sironcha", DistrictId = districtDictionary["Gadchiroli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Mulchera", DistrictId = districtDictionary["Gadchiroli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Korchi", DistrictId = districtDictionary["Gadchiroli"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Bhamragad", DistrictId = districtDictionary["Gadchiroli"], StateId = stateDictionary["Maharashtra"] });

                #endregion

                #region Sindhudurg(8)
                talukaList.Add(new Taluka() { TalukaName = "Kudal", DistrictId = districtDictionary["Sindhudurg"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Sawantwadi", DistrictId = districtDictionary["Sindhudurg"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Kankavli", DistrictId = districtDictionary["Sindhudurg"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Devgad", DistrictId = districtDictionary["Sindhudurg"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Malwan", DistrictId = districtDictionary["Sindhudurg"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Vengurla", DistrictId = districtDictionary["Sindhudurg"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Dodamarg", DistrictId = districtDictionary["Sindhudurg"], StateId = stateDictionary["Maharashtra"] });
                talukaList.Add(new Taluka() { TalukaName = "Vaibhavvadi", DistrictId = districtDictionary["Sindhudurg"], StateId = stateDictionary["Maharashtra"] });

                #endregion



                #endregion

                #region Andhra pradesh 
                #region AP-Kurnool
                talukaList.Add(new Taluka() { TalukaName = "Atmakur", DistrictId = districtDictionary["Kurnool"], StateId = stateDictionary["Andhra Pradesh"] });
                talukaList.Add(new Taluka() { TalukaName = "Jupadu Bungalow", DistrictId = districtDictionary["Kurnool"], StateId = stateDictionary["Andhra Pradesh"] });

                #endregion
                #endregion

                #region Jharkhand  

                #region Ranchi(18)
                talukaList.Add(new Taluka() { TalukaName = "Kanke", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Namkum", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Tamar I", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Mandar", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Silli", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Bero", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Angara", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Chanho", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Ormanjhi", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Burmu", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Bundu", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Khelari", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Sonahatu", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Ratu", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Nagri", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Lapung", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Rahe", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Itki", DistrictId = districtDictionary["Ranchi"], StateId = stateDictionary["Jharkhand"] });
                #endregion

                #region Dhanbad(8)
                talukaList.Add(new Taluka() { TalukaName = "Dhanbad-Cum-Kenduadih-Cum-Jagata", DistrictId = districtDictionary["Dhanbad"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Nirsa-Cum-Chirkunda", DistrictId = districtDictionary["Dhanbad"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Baghmara-Cum-Katras", DistrictId = districtDictionary["Dhanbad"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Gobindpur", DistrictId = districtDictionary["Dhanbad"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Topchanchi", DistrictId = districtDictionary["Dhanbad"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Baliapur", DistrictId = districtDictionary["Dhanbad"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Tundi", DistrictId = districtDictionary["Dhanbad"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Purbi Tundi", DistrictId = districtDictionary["Dhanbad"], StateId = stateDictionary["Jharkhand"] });

                #endregion

                #region Giridih(12)
                talukaList.Add(new Taluka() { TalukaName = "Jamua", DistrictId = districtDictionary["Giridih"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Dhanwar", DistrictId = districtDictionary["Giridih"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Dumri", DistrictId = districtDictionary["Giridih"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Deori(Giridih)", DistrictId = districtDictionary["Giridih"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Gande", DistrictId = districtDictionary["Giridih"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Birni", DistrictId = districtDictionary["Giridih"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Bagodar", DistrictId = districtDictionary["Giridih"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Bengabad", DistrictId = districtDictionary["Giridih"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Sariya", DistrictId = districtDictionary["Giridih"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Gawan", DistrictId = districtDictionary["Giridih"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Pirtanr", DistrictId = districtDictionary["Giridih"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Tisri", DistrictId = districtDictionary["Giridih"], StateId = stateDictionary["Jharkhand"] });

                #endregion

                #region East Singhbhum(11)
                talukaList.Add(new Taluka() { TalukaName = "Golmuri-Cum-Jugsalai", DistrictId = districtDictionary["East Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Potka", DistrictId = districtDictionary["East Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Baharagora", DistrictId = districtDictionary["East Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Ghatshila", DistrictId = districtDictionary["East Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Chakulia", DistrictId = districtDictionary["East Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Musabani", DistrictId = districtDictionary["East Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Patamda", DistrictId = districtDictionary["East Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Boram", DistrictId = districtDictionary["East Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Dumaria", DistrictId = districtDictionary["East Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Dhalbhumgarh", DistrictId = districtDictionary["East Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Gurbandha", DistrictId = districtDictionary["East Singhbhum"], StateId = stateDictionary["Jharkhand"] });

                #endregion

                #region Bokaro(9)
                talukaList.Add(new Taluka() { TalukaName = "Chas", DistrictId = districtDictionary["Bokaro"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Gumia", DistrictId = districtDictionary["Bokaro"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Chandankiyari", DistrictId = districtDictionary["Bokaro"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Bermo", DistrictId = districtDictionary["Bokaro"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Nawadih", DistrictId = districtDictionary["Bokaro"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Chadrapura", DistrictId = districtDictionary["Bokaro"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Peterwar", DistrictId = districtDictionary["Bokaro"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Jaridih", DistrictId = districtDictionary["Bokaro"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Kasmar", DistrictId = districtDictionary["Bokaro"], StateId = stateDictionary["Jharkhand"] });

                #endregion

                #region Palamu(20)
                talukaList.Add(new Taluka() { TalukaName = "Chainpur", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Medininagar(Daltonganj)", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Hussainabad", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Panki", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Chhatarpur", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Patan(Palamu)", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Bishrampur", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Nilambar-Pitambarpur(Lesliganj)", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Tarhasi", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Hariharganj", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Haidernagar", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Nawadiha Bazar - Nawadiha", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Pandu", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Satbarwa", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Nawa Bazar", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Mohammad Ganj", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Padwa", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Manatu", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Untari Road", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Pipra", DistrictId = districtDictionary["Palamu"], StateId = stateDictionary["Jharkhand"] });

                #endregion

                #region Hazaribagh(15)
                talukaList.Add(new Taluka() { TalukaName = "Chauparan", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Bishungarh", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Barkagaon", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Barhi", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Barkatha", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Ichak", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Katkamsandi", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Keredari", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Katamdag", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Dadi", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Padma", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Churchu", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Daru", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Chalkusa", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Tati jhariya", DistrictId = districtDictionary["Hazaribagh"], StateId = stateDictionary["Jharkhand"] });

                #endregion

                #region West Singhbhum(18)
                talukaList.Add(new Taluka() { TalukaName = "Chakradharpur", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Chaibasa", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Noamundi", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Jagannathpur", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Manoharpur", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Bandgaon", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Khuntpani", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Sonua", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Goilkera", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Majhgaon", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Manjhari", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Hat Gamharia", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Tantnagar", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Tonto", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Kumardungi", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Jhinkpani", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Anandpur", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });
                talukaList.Add(new Taluka() { TalukaName = "Gudri", DistrictId = districtDictionary["West Singhbhum"], StateId = stateDictionary["Jharkhand"] });

                #endregion

                #region AP-Khunti
                talukaList.Add(new Taluka() { TalukaName = "Murhu", DistrictId = districtDictionary["Khunti"], StateId = stateDictionary["Jharkhand"] });
                #endregion

                #endregion

                #region MP 
                talukaList.Add(new Taluka() { TalukaName = "Narayanganj", DistrictId = districtDictionary["Mandla"], StateId = stateDictionary["Madhya Pradesh"] });
                talukaList.Add(new Taluka() { TalukaName = "Niwas", DistrictId = districtDictionary["Mandla"], StateId = stateDictionary["Madhya Pradesh"] });
                #endregion

                #region Rajastan 
                #region Rajastan-Udaipur
                talukaList.Add(new Taluka() { TalukaName = "Gogunda", DistrictId = districtDictionary["Udaipur"], StateId = stateDictionary["Rajasthan"] });
                talukaList.Add(new Taluka() { TalukaName = "Jhadol", DistrictId = districtDictionary["Udaipur"], StateId = stateDictionary["Rajasthan"] });
                #endregion
                #region Rajastan-Pratapgarh
                talukaList.Add(new Taluka() { TalukaName = "Pratapgarh", DistrictId = districtDictionary["Pratapgarh"], StateId = stateDictionary["Rajasthan"] });
                #endregion

                #endregion

                #region Commented Talukas

                #region GOA

                //#region North Goa
                //talukaList.Add(new Taluka() { TalukaName = "Bardez", DistrictId = districtDictionary["North Goa"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bicholim", DistrictId = districtDictionary["North Goa"] });
                //talukaList.Add(new Taluka() { TalukaName = "Pernem", DistrictId = districtDictionary["North Goa"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sattari", DistrictId = districtDictionary["North Goa"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tiswadi", DistrictId = districtDictionary["North Goa"] });
                //#endregion

                //#region South Goa
                //talukaList.Add(new Taluka() { TalukaName = "Canacona", DistrictId = districtDictionary["South Goa"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mormugao", DistrictId = districtDictionary["South Goa"] });
                //talukaList.Add(new Taluka() { TalukaName = "Salcette", DistrictId = districtDictionary["South Goa"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sanguem", DistrictId = districtDictionary["South Goa"] });
                //talukaList.Add(new Taluka() { TalukaName = "Quepem", DistrictId = districtDictionary["South Goa"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dharbandora", DistrictId = districtDictionary["South Goa"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ponda", DistrictId = districtDictionary["South Goa"] });
                //#endregion

                #endregion

                #region Sikkim

                //#region East Sikkim(3)
                //talukaList.Add(new Taluka() { TalukaName = "Gangtok", DistrictId = districtDictionary["East Sikkim"] });
                //talukaList.Add(new Taluka() { TalukaName = "Pakyong", DistrictId = districtDictionary["East Sikkim"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rongli", DistrictId = districtDictionary["East Sikkim"] });
                //#endregion

                //#region North Sikkim(2)
                //talukaList.Add(new Taluka() { TalukaName = "Mangan", DistrictId = districtDictionary["North Sikkim"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chungthang", DistrictId = districtDictionary["North Sikkim"] });
                //#endregion

                //#region South Sikkim(2)
                //talukaList.Add(new Taluka() { TalukaName = "Namchi", DistrictId = districtDictionary["South Sikkim"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ravong", DistrictId = districtDictionary["South Sikkim"] });
                //#endregion

                //#region West Sikkim(2)
                //talukaList.Add(new Taluka() { TalukaName = "Gyalshing", DistrictId = districtDictionary["West Sikkim"] });
                //talukaList.Add(new Taluka() { TalukaName = "Soreng", DistrictId = districtDictionary["West Sikkim"] });
                //#endregion

                #endregion

                #region Tripura

                //#region Dhalai(5)
                //talukaList.Add(new Taluka() { TalukaName = "Salema", DistrictId = districtDictionary["Dhalai"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mahu", DistrictId = districtDictionary["Dhalai"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dumburnagar", DistrictId = districtDictionary["Dhalai"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ambassa", DistrictId = districtDictionary["Dhalai"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chhamanu", DistrictId = districtDictionary["Dhalai"] });
                //#endregion

                //#region West Tripura(16)
                //talukaList.Add(new Taluka() { TalukaName = "Bishalgarh", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mohanpur", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jirania", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Melaghar", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dukli", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Teliamura", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khowai", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kathalia", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Boxanagar", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kalyanpur", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jampuijala", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mandai", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tulshikhar", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hezamara", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Padmabil", DistrictId = districtDictionary["West Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mungiakumi", DistrictId = districtDictionary["West Tripura"] });

                //#endregion

                //#region South Tripura(11)
                //talukaList.Add(new Taluka() { TalukaName = "Matarbari", DistrictId = districtDictionary["South Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bokafa", DistrictId = districtDictionary["South Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rajnagar", DistrictId = districtDictionary["South Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Satchand", DistrictId = districtDictionary["South Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kakraban", DistrictId = districtDictionary["South Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Amarpur", DistrictId = districtDictionary["South Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hrishyamukh", DistrictId = districtDictionary["South Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rupaichhari", DistrictId = districtDictionary["South Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Karbuk", DistrictId = districtDictionary["South Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Killa", DistrictId = districtDictionary["South Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ompi", DistrictId = districtDictionary["South Tripura"] });

                //#endregion

                //#region North Tripura(8)
                //talukaList.Add(new Taluka() { TalukaName = "Kadamtala", DistrictId = districtDictionary["North Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Panisagar", DistrictId = districtDictionary["North Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gournagar", DistrictId = districtDictionary["North Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dadsa", DistrictId = districtDictionary["North Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kumarghat", DistrictId = districtDictionary["North Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Pencharthal", DistrictId = districtDictionary["North Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Damchhara", DistrictId = districtDictionary["North Tripura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jampuii hills", DistrictId = districtDictionary["North Tripura"] });

                //#endregion


                #endregion

                #region Mizoram

                //#region Aizawl(5)
                //talukaList.Add(new Taluka() { TalukaName = "Tlangnuam", DistrictId = districtDictionary["Aizawl"] });
                //talukaList.Add(new Taluka() { TalukaName = "Thingsulthliah", DistrictId = districtDictionary["Aizawl"] });
                //talukaList.Add(new Taluka() { TalukaName = "Darlawn", DistrictId = districtDictionary["Aizawl"] });
                //talukaList.Add(new Taluka() { TalukaName = "Aibawk", DistrictId = districtDictionary["Aizawl"] });
                //talukaList.Add(new Taluka() { TalukaName = "Phullen", DistrictId = districtDictionary["Aizawl"] });

                //#endregion

                //#region Lunglei(3)
                //talukaList.Add(new Taluka() { TalukaName = "Lungsen", DistrictId = districtDictionary["Lunglei"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hnahthial", DistrictId = districtDictionary["Lunglei"] });
                //talukaList.Add(new Taluka() { TalukaName = "West Bunghmun", DistrictId = districtDictionary["Lunglei"] });

                //#endregion

                //#region Champhai(4)
                //talukaList.Add(new Taluka() { TalukaName = "Khawzawl", DistrictId = districtDictionary["Champhai"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khawbung", DistrictId = districtDictionary["Champhai"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ngopa", DistrictId = districtDictionary["Champhai"] });
                //talukaList.Add(new Taluka() { TalukaName = "East Lungdar", DistrictId = districtDictionary["Champhai"] });

                //#endregion

                //#region Lawngtlai(3)
                //talukaList.Add(new Taluka() { TalukaName = "Chawngte", DistrictId = districtDictionary["Lawngtlai"] });
                //talukaList.Add(new Taluka() { TalukaName = "S Bungtlang", DistrictId = districtDictionary["Lawngtlai"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sangau", DistrictId = districtDictionary["Lawngtlai"] });

                //#endregion

                //#region Mamit(3)
                //talukaList.Add(new Taluka() { TalukaName = "Zawlnuam", DistrictId = districtDictionary["Mamit"] });
                //talukaList.Add(new Taluka() { TalukaName = "West Phaileng", DistrictId = districtDictionary["Mamit"] });
                //talukaList.Add(new Taluka() { TalukaName = "Relek", DistrictId = districtDictionary["Mamit"] });

                //#endregion

                //#region Kolasib(3)
                //talukaList.Add(new Taluka() { TalukaName = "Bikhawthir", DistrictId = districtDictionary["Kolasib"] });
                //talukaList.Add(new Taluka() { TalukaName = "N Thingdawl", DistrictId = districtDictionary["Kolasib"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tlangnuam(Kolasib)", DistrictId = districtDictionary["Kolasib"] });

                //#endregion

                //#region Serchhip(2)
                //talukaList.Add(new Taluka() { TalukaName = "East Lungdar(Serchhip)", DistrictId = districtDictionary["Serchhip"] });
                //talukaList.Add(new Taluka() { TalukaName = "Thingsulthliah(Serchhip)", DistrictId = districtDictionary["Serchhip"] });

                //#endregion

                //#region Saiha
                //talukaList.Add(new Taluka() { TalukaName = "Tuipang", DistrictId = districtDictionary["Saiha"] });

                //#endregion

                #endregion

                #region Nagaland

                //#region Dimapur(8)
                //talukaList.Add(new Taluka() { TalukaName = "Dimapur Sadar", DistrictId = districtDictionary["Dimapur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chumukedima", DistrictId = districtDictionary["Dimapur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Medziphema", DistrictId = districtDictionary["Dimapur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dhansiripar", DistrictId = districtDictionary["Dimapur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kuhoboto", DistrictId = districtDictionary["Dimapur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Niuland", DistrictId = districtDictionary["Dimapur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Aquqhnaqua", DistrictId = districtDictionary["Dimapur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nihokhu", DistrictId = districtDictionary["Dimapur"] });

                //#endregion

                //#region Kohima(7)
                //talukaList.Add(new Taluka() { TalukaName = "Jakhama", DistrictId = districtDictionary["Kohima"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tseminyu", DistrictId = districtDictionary["Kohima"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chiephobozou", DistrictId = districtDictionary["Kohima"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sechu-Zubza", DistrictId = districtDictionary["Kohima"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kezocha", DistrictId = districtDictionary["Kohima"] });
                //talukaList.Add(new Taluka() { TalukaName = "Botsa", DistrictId = districtDictionary["Kohima"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tsogin", DistrictId = districtDictionary["Kohima"] });

                //#endregion

                //#region Mon(13)
                //talukaList.Add(new Taluka() { TalukaName = "Monyakshu", DistrictId = districtDictionary["Mon"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tizit", DistrictId = districtDictionary["Mon"] });
                //talukaList.Add(new Taluka() { TalukaName = "Longshen", DistrictId = districtDictionary["Mon"] });
                //talukaList.Add(new Taluka() { TalukaName = "Angjangyang", DistrictId = districtDictionary["Mon"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chen", DistrictId = districtDictionary["Mon"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tobu", DistrictId = districtDictionary["Mon"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mopong", DistrictId = districtDictionary["Mon"] });
                //talukaList.Add(new Taluka() { TalukaName = "Aboi", DistrictId = districtDictionary["Mon"] });
                //talukaList.Add(new Taluka() { TalukaName = "Wakching", DistrictId = districtDictionary["Mon"] });
                //talukaList.Add(new Taluka() { TalukaName = "Phomching", DistrictId = districtDictionary["Mon"] });
                //talukaList.Add(new Taluka() { TalukaName = "Naginimora", DistrictId = districtDictionary["Mon"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hunta", DistrictId = districtDictionary["Mon"] });
                //talukaList.Add(new Taluka() { TalukaName = "Shangnyu", DistrictId = districtDictionary["Mon"] });

                //#endregion

                //#region Tuensang(15)
                //talukaList.Add(new Taluka() { TalukaName = "Noklak", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Thonoknyu", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Longkhim", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Noksen", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Shamator", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chare", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Panso", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sotokur", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sangsangnyu", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nokhu", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ngoungchung", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chessore", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chingmei", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mangko", DistrictId = districtDictionary["Tuensang"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tsurungto", DistrictId = districtDictionary["Tuensang"] });

                //#endregion

                //#region Mokokchung(9)
                //talukaList.Add(new Taluka() { TalukaName = "Ongpangkong", DistrictId = districtDictionary["Mokokchung"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tuli", DistrictId = districtDictionary["Mokokchung"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chuchuyimlang", DistrictId = districtDictionary["Mokokchung"] });
                //talukaList.Add(new Taluka() { TalukaName = "Changtongya", DistrictId = districtDictionary["Mokokchung"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mangkolemba", DistrictId = districtDictionary["Mokokchung"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kubolong", DistrictId = districtDictionary["Mokokchung"] });
                //talukaList.Add(new Taluka() { TalukaName = "Alongkima", DistrictId = districtDictionary["Mokokchung"] });
                //talukaList.Add(new Taluka() { TalukaName = "Longchem", DistrictId = districtDictionary["Mokokchung"] });
                //talukaList.Add(new Taluka() { TalukaName = "Merangmen", DistrictId = districtDictionary["Mokokchung"] });

                //#endregion

                //#region Wokha(11)
                //talukaList.Add(new Taluka() { TalukaName = "Bhandari", DistrictId = districtDictionary["Wokha"] });
                //talukaList.Add(new Taluka() { TalukaName = "Aitepyong", DistrictId = districtDictionary["Wokha"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sungro", DistrictId = districtDictionary["Wokha"] });
                //talukaList.Add(new Taluka() { TalukaName = "Englan", DistrictId = districtDictionary["Wokha"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chukitong", DistrictId = districtDictionary["Wokha"] });
                //talukaList.Add(new Taluka() { TalukaName = "Wozhuro", DistrictId = districtDictionary["Wokha"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ralan", DistrictId = districtDictionary["Wokha"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sanis", DistrictId = districtDictionary["Wokha"] });
                //talukaList.Add(new Taluka() { TalukaName = "Lotsu", DistrictId = districtDictionary["Wokha"] });
                //talukaList.Add(new Taluka() { TalukaName = "Changpang", DistrictId = districtDictionary["Wokha"] });
                //talukaList.Add(new Taluka() { TalukaName = "Baghty", DistrictId = districtDictionary["Wokha"] });

                //#endregion

                //#region Phek(13)
                //talukaList.Add(new Taluka() { TalukaName = "Pfutsero", DistrictId = districtDictionary["Phek"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chetheba", DistrictId = districtDictionary["Phek"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chozuba", DistrictId = districtDictionary["Phek"] });
                //talukaList.Add(new Taluka() { TalukaName = "Meluri", DistrictId = districtDictionary["Phek"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sekruzu", DistrictId = districtDictionary["Phek"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chizami", DistrictId = districtDictionary["Phek"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sakraba", DistrictId = districtDictionary["Phek"] });
                //talukaList.Add(new Taluka() { TalukaName = "Razieba", DistrictId = districtDictionary["Phek"] });
                //talukaList.Add(new Taluka() { TalukaName = "Zuketsa", DistrictId = districtDictionary["Phek"] });
                //talukaList.Add(new Taluka() { TalukaName = "Phor", DistrictId = districtDictionary["Phek"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khuza", DistrictId = districtDictionary["Phek"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khezhakeno", DistrictId = districtDictionary["Phek"] });
                //talukaList.Add(new Taluka() { TalukaName = "Phokhungri", DistrictId = districtDictionary["Phek"] });

                //#endregion

                //#region Zunheboto(12)
                //talukaList.Add(new Taluka() { TalukaName = "Aghunato", DistrictId = districtDictionary["Zunheboto"] });
                //talukaList.Add(new Taluka() { TalukaName = "Satakha", DistrictId = districtDictionary["Zunheboto"] });
                //talukaList.Add(new Taluka() { TalukaName = "Suruhuto", DistrictId = districtDictionary["Zunheboto"] });
                //talukaList.Add(new Taluka() { TalukaName = "Pughoboto", DistrictId = districtDictionary["Zunheboto"] });
                //talukaList.Add(new Taluka() { TalukaName = "Atoizu", DistrictId = districtDictionary["Zunheboto"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ghathashi", DistrictId = districtDictionary["Zunheboto"] });
                //talukaList.Add(new Taluka() { TalukaName = "Asuto", DistrictId = districtDictionary["Zunheboto"] });
                //talukaList.Add(new Taluka() { TalukaName = "Akuluto", DistrictId = districtDictionary["Zunheboto"] });
                //talukaList.Add(new Taluka() { TalukaName = "Saptiqa", DistrictId = districtDictionary["Zunheboto"] });
                //talukaList.Add(new Taluka() { TalukaName = "Satoi", DistrictId = districtDictionary["Zunheboto"] });
                //talukaList.Add(new Taluka() { TalukaName = "V.K.", DistrictId = districtDictionary["Zunheboto"] });
                //talukaList.Add(new Taluka() { TalukaName = "Akuhaito", DistrictId = districtDictionary["Zunheboto"] });

                //#endregion

                //#region Peren(6)
                //talukaList.Add(new Taluka() { TalukaName = "Jalukie", DistrictId = districtDictionary["Peren"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tening", DistrictId = districtDictionary["Peren"] });
                //talukaList.Add(new Taluka() { TalukaName = "Athibung", DistrictId = districtDictionary["Peren"] });
                //talukaList.Add(new Taluka() { TalukaName = "Pedi (Ngwalwa)", DistrictId = districtDictionary["Peren"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nsong", DistrictId = districtDictionary["Peren"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kebai Khelma", DistrictId = districtDictionary["Peren"] });

                //#endregion

                //#region Kiphire(7)
                //talukaList.Add(new Taluka() { TalukaName = "Pungro", DistrictId = districtDictionary["Kiphire"] });
                //talukaList.Add(new Taluka() { TalukaName = "Seyochung", DistrictId = districtDictionary["Kiphire"] });
                //talukaList.Add(new Taluka() { TalukaName = "Amahator", DistrictId = districtDictionary["Kiphire"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kiusam", DistrictId = districtDictionary["Kiphire"] });
                //talukaList.Add(new Taluka() { TalukaName = "Longmatra", DistrictId = districtDictionary["Kiphire"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khongsa", DistrictId = districtDictionary["Kiphire"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sitimi", DistrictId = districtDictionary["Kiphire"] });

                //#endregion

                //#region Longleng(4)
                //talukaList.Add(new Taluka() { TalukaName = "Yongnyah", DistrictId = districtDictionary["Longleng"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sakshi", DistrictId = districtDictionary["Longleng"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tamlu", DistrictId = districtDictionary["Longleng"] });
                //talukaList.Add(new Taluka() { TalukaName = "Namsang", DistrictId = districtDictionary["Longleng"] });

                //#endregion



                #endregion

                #region Uttarakhand

                //#region Haridwar(3)
                //talukaList.Add(new Taluka() { TalukaName = "Roorkee", DistrictId = districtDictionary["Haridwar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Laksar", DistrictId = districtDictionary["Haridwar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hardwar", DistrictId = districtDictionary["Haridwar"] });

                //#endregion

                //#region Dehradun(5)
                //talukaList.Add(new Taluka() { TalukaName = "Vikasnagar", DistrictId = districtDictionary["Dehradun"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rishikesh", DistrictId = districtDictionary["Dehradun"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chakrata", DistrictId = districtDictionary["Dehradun"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kalsi", DistrictId = districtDictionary["Dehradun"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tyuni", DistrictId = districtDictionary["Dehradun"] });

                //#endregion

                //#region Udham Singh Nagar(7)
                //talukaList.Add(new Taluka() { TalukaName = "Kichha", DistrictId = districtDictionary["Udham Singh Nagar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kashipur", DistrictId = districtDictionary["Udham Singh Nagar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khatima", DistrictId = districtDictionary["Udham Singh Nagar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sitarganj", DistrictId = districtDictionary["Udham Singh Nagar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bajpur", DistrictId = districtDictionary["Udham Singh Nagar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gadarpur", DistrictId = districtDictionary["Udham Singh Nagar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jaspur", DistrictId = districtDictionary["Udham Singh Nagar"] });

                //#endregion

                //#region Nainital(7)
                //talukaList.Add(new Taluka() { TalukaName = "Haldwani", DistrictId = districtDictionary["Nainital"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ramnagar", DistrictId = districtDictionary["Nainital"] });
                //talukaList.Add(new Taluka() { TalukaName = "Lalkuan", DistrictId = districtDictionary["Nainital"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dhari", DistrictId = districtDictionary["Nainital"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kaladhungi", DistrictId = districtDictionary["Nainital"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kosya Kutauli", DistrictId = districtDictionary["Nainital"] });
                //talukaList.Add(new Taluka() { TalukaName = "Betalghat", DistrictId = districtDictionary["Nainital"] });

                //#endregion

                //#region Pauri(8)
                //talukaList.Add(new Taluka() { TalukaName = "Kotdwara", DistrictId = districtDictionary["Pauri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Thalisain", DistrictId = districtDictionary["Pauri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Lansdowne", DistrictId = districtDictionary["Pauri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Srinagar", DistrictId = districtDictionary["Pauri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Yamkeshwar", DistrictId = districtDictionary["Pauri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dhoomakot", DistrictId = districtDictionary["Pauri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chaubattakhal", DistrictId = districtDictionary["Pauri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Satpuli", DistrictId = districtDictionary["Pauri"] });

                //#endregion

                //#region Almora(8)
                //talukaList.Add(new Taluka() { TalukaName = "Ranikhet", DistrictId = districtDictionary["Almora"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bhikiyasain", DistrictId = districtDictionary["Almora"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bhanoli", DistrictId = districtDictionary["Almora"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sult", DistrictId = districtDictionary["Almora"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dwarahat", DistrictId = districtDictionary["Almora"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chaukhutiya", DistrictId = districtDictionary["Almora"] });
                //talukaList.Add(new Taluka() { TalukaName = "Someshwar", DistrictId = districtDictionary["Almora"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jainti", DistrictId = districtDictionary["Almora"] });

                //#endregion

                //#region Tehri(6)
                //talukaList.Add(new Taluka() { TalukaName = "Ghansali", DistrictId = districtDictionary["Tehri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Narendranagar", DistrictId = districtDictionary["Tehri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Devprayag", DistrictId = districtDictionary["Tehri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dhanaulti", DistrictId = districtDictionary["Tehri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Pratapnagar", DistrictId = districtDictionary["Tehri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jakhnidhar", DistrictId = districtDictionary["Tehri"] });

                //#endregion

                //#region Pithoragarh(5)
                //talukaList.Add(new Taluka() { TalukaName = "Didihat", DistrictId = districtDictionary["Pithoragarh"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gangolihat", DistrictId = districtDictionary["Pithoragarh"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dharchula", DistrictId = districtDictionary["Pithoragarh"] });
                //talukaList.Add(new Taluka() { TalukaName = "Berinag", DistrictId = districtDictionary["Pithoragarh"] });
                //talukaList.Add(new Taluka() { TalukaName = "Munsiari", DistrictId = districtDictionary["Pithoragarh"] });

                //#endregion

                //#region Chamoli(5)
                //talukaList.Add(new Taluka() { TalukaName = "Tharali", DistrictId = districtDictionary["Chamoli"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gairsain", DistrictId = districtDictionary["Chamoli"] });
                //talukaList.Add(new Taluka() { TalukaName = "Karnaprayag", DistrictId = districtDictionary["Chamoli"] });
                //talukaList.Add(new Taluka() { TalukaName = "Joshimath", DistrictId = districtDictionary["Chamoli"] });
                //talukaList.Add(new Taluka() { TalukaName = "Pokhari", DistrictId = districtDictionary["Chamoli"] });

                //#endregion

                //#region Uttarkashi(6)
                //talukaList.Add(new Taluka() { TalukaName = "Bhatwari", DistrictId = districtDictionary["Uttarkashi"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rajgarhi", DistrictId = districtDictionary["Uttarkashi"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dunda", DistrictId = districtDictionary["Uttarkashi"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chiniyalisaur", DistrictId = districtDictionary["Uttarkashi"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mori", DistrictId = districtDictionary["Uttarkashi"] });
                //talukaList.Add(new Taluka() { TalukaName = "Puraula", DistrictId = districtDictionary["Uttarkashi"] });

                //#endregion

                //#region Bageshwar(3)
                //talukaList.Add(new Taluka() { TalukaName = "Garud", DistrictId = districtDictionary["Bageshwar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kapkot", DistrictId = districtDictionary["Bageshwar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kunda", DistrictId = districtDictionary["Bageshwar"] });

                //#endregion

                //#region Champawat(3)
                //talukaList.Add(new Taluka() { TalukaName = "Lohaghat", DistrictId = districtDictionary["Champawat"] });
                //talukaList.Add(new Taluka() { TalukaName = "Poornagiri", DistrictId = districtDictionary["Champawat"] });
                //talukaList.Add(new Taluka() { TalukaName = "Pati", DistrictId = districtDictionary["Champawat"] });

                //#endregion

                //#region Rudraprayag(2)
                //talukaList.Add(new Taluka() { TalukaName = "Ukhimath", DistrictId = districtDictionary["Rudraprayag"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jakholi", DistrictId = districtDictionary["Rudraprayag"] });

                //#endregion


                #endregion

                #region Manipur

                //#region Imphal West(4)
                //talukaList.Add(new Taluka() { TalukaName = "Lamphelpat", DistrictId = districtDictionary["Imphal West"] });
                //talukaList.Add(new Taluka() { TalukaName = "Wangoi", DistrictId = districtDictionary["Imphal West"] });
                //talukaList.Add(new Taluka() { TalukaName = "Lamshang", DistrictId = districtDictionary["Imphal West"] });
                //talukaList.Add(new Taluka() { TalukaName = "Patsoi", DistrictId = districtDictionary["Imphal West"] });

                //#endregion

                //#region Senapati(6)
                //talukaList.Add(new Taluka() { TalukaName = "Mao-Maram", DistrictId = districtDictionary["Senapati"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sadar Hills West", DistrictId = districtDictionary["Senapati"] });
                //talukaList.Add(new Taluka() { TalukaName = "Purul", DistrictId = districtDictionary["Senapati"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sadar Hills East", DistrictId = districtDictionary["Senapati"] });
                //talukaList.Add(new Taluka() { TalukaName = "Saitu-Gamphazol", DistrictId = districtDictionary["Senapati"] });
                //talukaList.Add(new Taluka() { TalukaName = "Paomata", DistrictId = districtDictionary["Senapati"] });

                //#endregion

                //#region Imphal East(4)
                //talukaList.Add(new Taluka() { TalukaName = "Porompat", DistrictId = districtDictionary["Imphal East"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sawombung", DistrictId = districtDictionary["Imphal East"] });
                //talukaList.Add(new Taluka() { TalukaName = "Keirao Bitra", DistrictId = districtDictionary["Imphal East"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jiribam", DistrictId = districtDictionary["Imphal East"] });

                //#endregion

                //#region Thoubal(2)
                //talukaList.Add(new Taluka() { TalukaName = "Kakching", DistrictId = districtDictionary["Thoubal"] });
                //talukaList.Add(new Taluka() { TalukaName = "Lilong", DistrictId = districtDictionary["Thoubal"] });

                //#endregion

                //#region Churachandpur(4)
                //talukaList.Add(new Taluka() { TalukaName = "Churachandpur North", DistrictId = districtDictionary["Churachandpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tipaimukh", DistrictId = districtDictionary["Churachandpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Singngat", DistrictId = districtDictionary["Churachandpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Thanlon", DistrictId = districtDictionary["Churachandpur"] });

                //#endregion

                //#region Bishnupur(2)
                //talukaList.Add(new Taluka() { TalukaName = "Moirang", DistrictId = districtDictionary["Bishnupur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nambol", DistrictId = districtDictionary["Bishnupur"] });

                //#endregion

                //#region Ukhrul(5)
                //talukaList.Add(new Taluka() { TalukaName = "Ukhrul Central", DistrictId = districtDictionary["Ukhrul"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ukhrul North", DistrictId = districtDictionary["Ukhrul"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kamjong-Chassad", DistrictId = districtDictionary["Ukhrul"] });
                //talukaList.Add(new Taluka() { TalukaName = "Phungyar-Phaisat", DistrictId = districtDictionary["Ukhrul"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ukhrul South", DistrictId = districtDictionary["Ukhrul"] });

                //#endregion

                //#region Chandel(3)
                //talukaList.Add(new Taluka() { TalukaName = "Chakpikarong", DistrictId = districtDictionary["Chandel"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tengnoupal", DistrictId = districtDictionary["Chandel"] });
                //talukaList.Add(new Taluka() { TalukaName = "Machi", DistrictId = districtDictionary["Chandel"] });

                //#endregion

                //#region Tamenglong(3)
                //talukaList.Add(new Taluka() { TalukaName = "Nungba", DistrictId = districtDictionary["Tamenglong"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tamenglong North", DistrictId = districtDictionary["Tamenglong"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tamenglong West", DistrictId = districtDictionary["Tamenglong"] });

                //#endregion


                #endregion

                #region Meghalaya

                //#region East Khasi Hills(8)
                //talukaList.Add(new Taluka() { TalukaName = "Mylliem", DistrictId = districtDictionary["East Khasi Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mawphlang", DistrictId = districtDictionary["East Khasi Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mawryngkneng", DistrictId = districtDictionary["East Khasi Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Pynursla", DistrictId = districtDictionary["East Khasi Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mawsynram", DistrictId = districtDictionary["East Khasi Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Shella Bholaganj", DistrictId = districtDictionary["East Khasi Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mawkynrew", DistrictId = districtDictionary["East Khasi Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khatarshnong Laitkroh", DistrictId = districtDictionary["East Khasi Hills"] });
                //#endregion

                //#region West Garo Hills(8)
                //talukaList.Add(new Taluka() { TalukaName = "Selsella", DistrictId = districtDictionary["West Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rongram", DistrictId = districtDictionary["West Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Zikzak", DistrictId = districtDictionary["West Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Betasing", DistrictId = districtDictionary["West Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tikrikilla", DistrictId = districtDictionary["West Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dalu", DistrictId = districtDictionary["West Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dadenggiri", DistrictId = districtDictionary["West Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gambegre", DistrictId = districtDictionary["West Garo Hills"] });

                //#endregion

                //#region West Khasi Hills(6)
                //talukaList.Add(new Taluka() { TalukaName = "Mairang", DistrictId = districtDictionary["West Khasi Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nongstoin", DistrictId = districtDictionary["West Khasi Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mawshynrut", DistrictId = districtDictionary["West Khasi Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mawkyrwat", DistrictId = districtDictionary["West Khasi Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mawthadraishan", DistrictId = districtDictionary["West Khasi Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ranikor", DistrictId = districtDictionary["West Khasi Hills"] });

                //#endregion

                //#region East Garo Hills(5)
                //talukaList.Add(new Taluka() { TalukaName = "Resubelpara", DistrictId = districtDictionary["East Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Samanda", DistrictId = districtDictionary["East Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Songsak", DistrictId = districtDictionary["East Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dambo Rongjeng", DistrictId = districtDictionary["East Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kharkutta", DistrictId = districtDictionary["East Garo Hills"] });

                //#endregion

                //#region Ri Bhoi(3)
                //talukaList.Add(new Taluka() { TalukaName = "Umsning", DistrictId = districtDictionary["Ri Bhoi"] });
                //talukaList.Add(new Taluka() { TalukaName = "Umling", DistrictId = districtDictionary["Ri Bhoi"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jirang", DistrictId = districtDictionary["Ri Bhoi"] });

                //#endregion

                //#region South Garo Hills(4)
                //talukaList.Add(new Taluka() { TalukaName = "Baghmara", DistrictId = districtDictionary["South Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chokpot", DistrictId = districtDictionary["South Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gasuapara", DistrictId = districtDictionary["South Garo Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rongara", DistrictId = districtDictionary["South Garo Hills"] });

                //#endregion

                //#region East Jaintia Hills(5)
                //talukaList.Add(new Taluka() { TalukaName = "Thadlaskein", DistrictId = districtDictionary["East Jaintia Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Laskein", DistrictId = districtDictionary["East Jaintia Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khliehriat", DistrictId = districtDictionary["East Jaintia Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Amlarem", DistrictId = districtDictionary["East Jaintia Hills"] });
                //talukaList.Add(new Taluka() { TalukaName = "Saipung", DistrictId = districtDictionary["East Jaintia Hills"] });

                //#endregion

                #endregion

                #region West Bengal


                //#region North 24 Parganas(22)
                //talukaList.Add(new Taluka() { TalukaName = "Bongaon", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gaighata", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Deganga", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Barasat - I", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Baduria", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Swarupnagar", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bogda", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Basirhat - II", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Habra - I", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Barrackpur - II", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Haroa", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hasnabad", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Barasat - II", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Minakhan", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Amdanga", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rajarhat", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Barrackpur - I", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Habra - II", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hingalganj", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Basirhat - I", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sandeshkhali - I", DistrictId = districtDictionary["North 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sandeshkhali - II", DistrictId = districtDictionary["North 24 Parganas"] });

                //#endregion

                //#region South 24 Parganas(29)
                //talukaList.Add(new Taluka() { TalukaName = "Baruipur", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Basanti", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Patharpratima", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Magrahat - II", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Canning - I", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kulpi", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kakdwip", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Magrahat - I", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jaynagar - I", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Canning - II", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jaynagar - II", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Falta", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bhangar - I", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bhangar - II", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gosaba", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bishnupur - I", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kultali", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mathurapur - II", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sonarpur", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bishnupur - II", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mandirbazar", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sagar", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mathurapur - I", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Budge Budge - II", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Diamond Harbour - II", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Namkhana", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Thakurpukur Mahestola", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Diamond Harbour - I", DistrictId = districtDictionary["South 24 Parganas"] });
                //talukaList.Add(new Taluka() { TalukaName = "Budge Budge - I", DistrictId = districtDictionary["South 24 Parganas"] });

                //#endregion

                //#region Murshidabad(26)
                //talukaList.Add(new Taluka() { TalukaName = "Berhampore", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Domkal", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Lalgola", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Beldanga - I", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sagardighi", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Samsherganj", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Suti - II", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Farakka", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khargram", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rahunathganj - II", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hariharpara", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Burwan", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jalangi", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Beldanga - II", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Murshidabad Jiaganj", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nabagram", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nawda", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kandi", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bhagawangola - I", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Raghunathganj - I", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Raninagar - II", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Raninagar - I", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Suti - I", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bharatpur - II", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bharatpur - I", DistrictId = districtDictionary["Murshidabad"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bhagawangola - II", DistrictId = districtDictionary["Murshidabad"] });

                //#endregion

                //#region Paschim Medinipur(29)
                //talukaList.Add(new Taluka() { TalukaName = "Keshpur", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Narayangarh", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Debra", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sabang", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kharagpur - I", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Daspur - II", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Garbeta - I", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ghatal", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Daspur - I", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Pingla", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Midnapore", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Salbani", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kharagpur - II", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dantan - I", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jhargram", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Garbeta - III", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Binpur - II", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Binpur - I", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dantan - II", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Keshiary", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Garbeta - II", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nayagram", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chandrakona - I", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chandrakona - II", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sankrail", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jamboni", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mohanpur(PM)", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gopiballavpur - I", DistrictId = districtDictionary["Paschim Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gopiballavpur - II", DistrictId = districtDictionary["Paschim Medinipur"] });

                //#endregion

                //#region Hooghly(18)

                //talukaList.Add(new Taluka() { TalukaName = "Dhaniakhali", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Pandua", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Arambag", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Singur", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Polba - Dadpur", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Haripal", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khanakul - I", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chinsurah - Magra", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Balagarh", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jangipara", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khanakul - II", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chanditala - I", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tarakeswar", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Pursura", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Goghat - II", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chanditala - II", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Serampur Uttarpara", DistrictId = districtDictionary["Hooghly"] });
                //talukaList.Add(new Taluka() { TalukaName = "Goghat - I", DistrictId = districtDictionary["Hooghly"] });


                //#endregion

                //#region Nadia(17)
                //talukaList.Add(new Taluka() { TalukaName = "Chakdah", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nakashipara", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ranaghat - II", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kaliganj", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Krishnagar - I", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chapra", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hanskhali", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tehatta - I", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Santipur", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Haringhata", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Karimpur - II", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ranaghat - I", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Karimpur - I", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tehatta - II", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Krishnaganj", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Krishnagar - II", DistrictId = districtDictionary["Nadia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nabadwip", DistrictId = districtDictionary["Nadia"] });

                //#endregion

                //#region Purba Medinipur(25)
                //talukaList.Add(new Taluka() { TalukaName = "Kolaghat", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Panskura", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nanda Kumar", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bhagawanpur - I", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Moyna", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tamluk", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nandigram - I", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mahisadal", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sahid Matangini", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bhagawanpur - II", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chandipur", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Egra - II", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Deshopran", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Potashpur - II", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Potashpur - I", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Contai - I", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ramnagar - I", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Egra - I", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Contai - III", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ramnagar - II", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khejuri - II", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khejuri - I", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sutahata", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nandigram - II", DistrictId = districtDictionary["Purba Medinipur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Haldia", DistrictId = districtDictionary["Purba Medinipur"] });

                //#endregion

                //#region Howrah(14)
                //talukaList.Add(new Taluka() { TalukaName = "Domjur", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sankrail", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jagatballavpur", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Panchla", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Amta - I", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bagnan - I", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Uluberia - I", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bally Jagachha", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Amta - II", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Shyampur - I", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Shyampur - II", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Uluberia - II", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Udaynarayanpur", DistrictId = districtDictionary["Howrah"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bagnan - II", DistrictId = districtDictionary["Howrah"] });

                //#endregion

                //#region Malda(15)
                //talukaList.Add(new Taluka() { TalukaName = "Kaliachak - I", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kaliachak - III", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gazole", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ratua - I", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "English Bazar", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Manikchak", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Harischandrapur - II", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Habibpur", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kaliachak - II", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chanchal - II", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chanchal - I", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ratua -II", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Harischandrapur - I", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Malda (Old)", DistrictId = districtDictionary["Malda"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bamangola", DistrictId = districtDictionary["Malda"] });

                //#endregion

                //#region Jalpaiguri(12)
                //talukaList.Add(new Taluka() { TalukaName = "Dhupguri", DistrictId = districtDictionary["Jalpaiguri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rajganj", DistrictId = districtDictionary["Jalpaiguri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Maynaguri", DistrictId = districtDictionary["Jalpaiguri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mal", DistrictId = districtDictionary["Jalpaiguri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kalchini", DistrictId = districtDictionary["Jalpaiguri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Falakata", DistrictId = districtDictionary["Jalpaiguri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Alipurduar - II", DistrictId = districtDictionary["Jalpaiguri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Alipurduar - I", DistrictId = districtDictionary["Jalpaiguri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Madarihat", DistrictId = districtDictionary["Jalpaiguri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kumargram", DistrictId = districtDictionary["Jalpaiguri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nagrakata", DistrictId = districtDictionary["Jalpaiguri"] });
                //talukaList.Add(new Taluka() { TalukaName = "Matiali", DistrictId = districtDictionary["Jalpaiguri"] });

                //#endregion

                //#region Bankura(22)
                //talukaList.Add(new Taluka() { TalukaName = "Onda", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Barjora", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chhatna", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kotulpur", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Patrasayer", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gangajalghati", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Raipur", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Indus", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sonamukhi", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jaypur", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Vishnupur", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Indpur", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Taldangra", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Simlapal", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bankura - II", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Saltora", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Ranibundh", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khatra", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bankura - I", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sarenga", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mejhia", DistrictId = districtDictionary["Bankura"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hirbandh", DistrictId = districtDictionary["Bankura"] });

                //#endregion

                //#region Birbhum(19)
                //talukaList.Add(new Taluka() { TalukaName = "Murarai - II", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nanoor", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nalhati - I", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bolpur Sriniketan", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Labpur", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sainthia", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Murarai - I", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rampurhat - I", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rampurhat - II", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dubrajpur", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Illambazar", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mohammad Bazar", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mayureshwar - I", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Khoyrasol", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Nalhati - II", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mayureshwar - II", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Suri - I", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Suri - II", DistrictId = districtDictionary["Birbhum"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rajnagar(Birbhum)", DistrictId = districtDictionary["Birbhum"] });

                //#endregion

                //#region Uttar Dinajpur(9)
                //talukaList.Add(new Taluka() { TalukaName = "Raiganj", DistrictId = districtDictionary["Uttar Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Karandighi", DistrictId = districtDictionary["Uttar Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Goalpokhar - I", DistrictId = districtDictionary["Uttar Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Islampur", DistrictId = districtDictionary["Uttar Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Itahar", DistrictId = districtDictionary["Uttar Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Goalpokhar - II", DistrictId = districtDictionary["Uttar Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Chopra", DistrictId = districtDictionary["Uttar Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kaliaganj", DistrictId = districtDictionary["Uttar Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hemtabad", DistrictId = districtDictionary["Uttar Dinajpur"] });

                //#endregion

                //#region Purulia(20)
                //talukaList.Add(new Taluka() { TalukaName = "Para", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kashipur(Purulia)", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Barabazar", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Purulia I", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Purulia II", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Arsha", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Manbazar - I", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jhalda - II", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hura", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Balarampur", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jhalda I", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bagmundi", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jaipur", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Puncha", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Raghunathpur - I", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Raghunathpur - II", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Neturia", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Manbazar - II", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bundwan", DistrictId = districtDictionary["Purulia"] });
                //talukaList.Add(new Taluka() { TalukaName = "Santuri", DistrictId = districtDictionary["Purulia"] });

                //#endregion

                //#region Cooch Behar(13)
                //talukaList.Add(new Taluka() { TalukaName = "Cooch Behar I", DistrictId = districtDictionary["Cooch Behar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Cooch Behar II", DistrictId = districtDictionary["Cooch Behar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dinhata - I", DistrictId = districtDictionary["Cooch Behar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tufanganj - I", DistrictId = districtDictionary["Cooch Behar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Dinhata - II", DistrictId = districtDictionary["Cooch Behar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mathabhanga - II", DistrictId = districtDictionary["Cooch Behar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mathabhanga - I", DistrictId = districtDictionary["Cooch Behar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Tufanganj - II", DistrictId = districtDictionary["Cooch Behar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sitalkuchi", DistrictId = districtDictionary["Cooch Behar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bihar", DistrictId = districtDictionary["Cooch Behar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mekliganj", DistrictId = districtDictionary["Cooch Behar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Sitai", DistrictId = districtDictionary["Cooch Behar"] });
                //talukaList.Add(new Taluka() { TalukaName = "Haldibari", DistrictId = districtDictionary["Cooch Behar"] });

                //#endregion

                //#region Darjeeling(12)
                //talukaList.Add(new Taluka() { TalukaName = "Phansidewa", DistrictId = districtDictionary["Darjeeling"] });
                //talukaList.Add(new Taluka() { TalukaName = "Matigara", DistrictId = districtDictionary["Darjeeling"] });
                //talukaList.Add(new Taluka() { TalukaName = "Naxalbari", DistrictId = districtDictionary["Darjeeling"] });
                //talukaList.Add(new Taluka() { TalukaName = "Darjeeling Pulbazar", DistrictId = districtDictionary["Darjeeling"] });
                //talukaList.Add(new Taluka() { TalukaName = "Jorebunglow Sukiapokhri", DistrictId = districtDictionary["Darjeeling"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kharibari", DistrictId = districtDictionary["Darjeeling"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kurseong", DistrictId = districtDictionary["Darjeeling"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kalimpong - I", DistrictId = districtDictionary["Darjeeling"] });
                //talukaList.Add(new Taluka() { TalukaName = "Rangli Rangliot", DistrictId = districtDictionary["Darjeeling"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kalimpong - II", DistrictId = districtDictionary["Darjeeling"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gorubathan", DistrictId = districtDictionary["Darjeeling"] });
                //talukaList.Add(new Taluka() { TalukaName = "Mirik", DistrictId = districtDictionary["Darjeeling"] });
                //#endregion

                //#region Dakshin Dinajpur(8)
                //talukaList.Add(new Taluka() { TalukaName = "Tapan", DistrictId = districtDictionary["Dakshin Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Balurghat", DistrictId = districtDictionary["Dakshin Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Gangarampur", DistrictId = districtDictionary["Dakshin Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kushmundi", DistrictId = districtDictionary["Dakshin Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Kumarganj", DistrictId = districtDictionary["Dakshin Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Bansihari", DistrictId = districtDictionary["Dakshin Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Harirampur", DistrictId = districtDictionary["Dakshin Dinajpur"] });
                //talukaList.Add(new Taluka() { TalukaName = "Hilli", DistrictId = districtDictionary["Dakshin Dinajpur"] });

                //#endregion


                #endregion



                #endregion

                var talukasList = _context.Taluka.ToList();
                foreach (var ta in talukaList)
                {
                    var _TalukaName = ta.TalukaName.Trim();
                    /*var talukaObject = _context.Taluka.FirstOrDefault(t => t.DistrictId == ta.DistrictId)*/
                    ;
                    var talukaObject = talukasList.FirstOrDefault(t => t.TalukaName == _TalukaName);

                    if (talukaObject == null)
                    {
                        _context.Taluka.Add(new Taluka { DistrictId = ta.DistrictId, StateId = ta.StateId, CreatorUserId = 2, TenantId = _tenantId, TalukaName = _TalukaName });
                    }

                }

                // --------------- R_Modifications Ends --------------------------------
                _context.SaveChanges();
                #endregion

                #region VILLAGE SEED
                var talukaDictionary = _context.Taluka.ToDictionary(d => d.TalukaName, d => d.Id);

                List<Village> villagelist = new List<Village>();
                //assign name of villages to taluka 
                #region State-Maharashtra
                #region AHMEDNAGER
                #region AHMEDNAGER-AKOLE[55] Akola
                villagelist.Add(new Village() { Name = "Abhit khind", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ambhol", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Baravwadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bulewadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chinchavane", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Devgoan Pabhulwandi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Devgoan Akole", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dhamanvan", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dhupe", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Garwadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Godewadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ghoti", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Keli Kotul", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Keli Otur", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khadki Bk", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khadki Kh", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khetewadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kohane", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kothale", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Mahadevwadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Manik Ozar", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Maveshi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Morwadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nachanthav", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Paithan", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Palsunde", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Phopsandi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pimpaldari", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pimpri", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Purushwadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sakirwadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Satewadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Saverkute", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shelad", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shelvihir", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "shenit", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shilvandi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shirasgoan", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shiswad", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shivapur", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Somalwadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tale", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tambhol", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Thokalwadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Umbarvihir", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Umbrewadi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vihir", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Waghdari", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wanjulshet", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Warangushi", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Yesarthav", TalukaId = talukaDictionary["Akola"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });

                #endregion

                #region AHMEDNAGER-KARJAT[11]
                villagelist.Add(new Village() { Name = "Aalsunde", TalukaId = talukaDictionary["Karjat"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Benvadi", TalukaId = talukaDictionary["Karjat"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chakhalewadi", TalukaId = talukaDictionary["Karjat"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chande(kh)", TalukaId = talukaDictionary["Karjat"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chicholi Ramzan", TalukaId = talukaDictionary["Karjat"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dukhao", TalukaId = talukaDictionary["Karjat"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Durgaon", TalukaId = talukaDictionary["Karjat"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Mulewadi", TalukaId = talukaDictionary["Karjat"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Patharwadi", TalukaId = talukaDictionary["Karjat"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vadgaon Tanpuri", TalukaId = talukaDictionary["Karjat"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Valvad", TalukaId = talukaDictionary["Karjat"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                #endregion

                #region AHMEDNAGER-Pathardi[9]
                villagelist.Add(new Village() { Name = "Bhose", TalukaId = talukaDictionary["Pathardi"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dagadwadi", TalukaId = talukaDictionary["Pathardi"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Damalwadi", TalukaId = talukaDictionary["Pathardi"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dharwadi)", TalukaId = talukaDictionary["Pathardi"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dongarwadi", TalukaId = talukaDictionary["Pathardi"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gitewadi", TalukaId = talukaDictionary["Pathardi"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Joharwadi", TalukaId = talukaDictionary["Pathardi"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khandgoan", TalukaId = talukaDictionary["Pathardi"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tisgoan", TalukaId = talukaDictionary["Pathardi"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                #endregion

                #region AHMEDNAGER-Rahuri[5]
                villagelist.Add(new Village() { Name = "Khandewali", TalukaId = talukaDictionary["Rahuri"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kolyachiwadi", TalukaId = talukaDictionary["Rahuri"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kukadvedhe", TalukaId = talukaDictionary["Rahuri"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Laxmiwadi Khadamb)", TalukaId = talukaDictionary["Rahuri"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sase Gandhale Vasti", TalukaId = talukaDictionary["Rahuri"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                #endregion


                #region AHMEDNAGER-PARNER[18]
                villagelist.Add(new Village() { Name = "Babhulwade", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bhangadewadi", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "gadadwadi", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ganjewadi", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ganji Bhoyare", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gavadewadi", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Hanumanwadi", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Hivare Korda", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Jadhavwadi", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kalamkarwadi", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kaluchi Thakarwadi", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kodus", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kutewadi", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pimpri Jalsen", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ranmala", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sherikoldara", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sutarwadi", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Waghwasti", TalukaId = talukaDictionary["Parner"], DistrictId = districtDictionary["Ahmednagar"], StateID = stateDictionary["Maharashtra"] });
                #region
                //#endregion
                //#region AHMEDNAGER-SANGAMNER[55]
                //villagelist.Add(new Village() { Name = "Bhojdari", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Borban", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Darewadi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Dolasane", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Gondushi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Gunjalwadi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Jawale Baleshwar", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Jondhalwadi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "K.Thakarwadi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Karjule Pathar", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Karule", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Kasare", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Kaute Bk", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Kaute Kh", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Khandgedara", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Kumbharwadi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Mahalwadi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Malegoan Pathar", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Mendhavan", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Mhaswandi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Nimgoan Paga", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Pemrewadi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Pimpaldari", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Sangamner", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Sarole Pathar", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Satechiwadi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Sawarchol", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Sawargoan Ghule", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Shelkewadi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Shirasgoan", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Shivapur", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Temrewadi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Varwandi", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Wankute", TalukaId = talukaDictionary["Sangamner"] });
                //villagelist.Add(new Village() { Name = "Warudi Pathar", TalukaId = talukaDictionary["Sangamner"] });

                //#endregion
                //#region AHMEDNAGER-Shrigonda
                //villagelist.Add(new Village() { Name = "Bangarde", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Belwandikothar", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Borudewadi", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Chorachiwadi", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Dhangarwadi-Bhanga", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Ghotvi", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Gosaviwadi", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Holewadi", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Holewadi pargoan", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Kanasewadi", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Khandgoan", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Khetmaliswadi1", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Khetmaliswadi2", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Kolhewadi", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Mahandulwandi", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Mandavgoan Visabiga", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Pawarwadi", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Vadali", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Valghud", TalukaId = talukaDictionary["Shrigonda"] });
                //villagelist.Add(new Village() { Name = "Velu", TalukaId = talukaDictionary["Shrigonda"] });

                //#endregion
                #endregion
                #endregion

                #endregion

                #region AMRAVATI
                #region AMRAVATI-AMRAVATI
                villagelist.Add(new Village() { Name = "Kapustani", TalukaId = talukaDictionary["Amravati"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kektapur", TalukaId = talukaDictionary["Amravati"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                #endregion

                #region AMRAVATI-Bhatkuli
                villagelist.Add(new Village() { Name = "Ganori", TalukaId = talukaDictionary["Bhatkuli"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ibrahimpur", TalukaId = talukaDictionary["Bhatkuli"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khallar", TalukaId = talukaDictionary["Bhatkuli"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kharbi", TalukaId = talukaDictionary["Bhatkuli"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shipgoan", TalukaId = talukaDictionary["Bhatkuli"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                #endregion


                #region AMRAVATI-Dharni
                villagelist.Add(new Village() { Name = "Bairabad", TalukaId = talukaDictionary["Dharni"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Harda", TalukaId = talukaDictionary["Dharni"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kutanga", TalukaId = talukaDictionary["Dharni"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Padidam", TalukaId = talukaDictionary["Dharni"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                #endregion


                #region AMRAVATI-Nandgaon-Khandeshwar
                villagelist.Add(new Village() { Name = "Bellora", TalukaId = talukaDictionary["Nandgaon-Khandeshwar"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kajan", TalukaId = talukaDictionary["Nandgaon-Khandeshwar"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kohola", TalukaId = talukaDictionary["Nandgaon-Khandeshwar"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nadurabad", TalukaId = talukaDictionary["Nandgaon-Khandeshwar"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Rajna", TalukaId = talukaDictionary["Nandgaon-Khandeshwar"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sukali guruv ", TalukaId = talukaDictionary["Nandgaon-Khandeshwar"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sultanapur", TalukaId = talukaDictionary["Nandgaon-Khandeshwar"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Takli Gilba", TalukaId = talukaDictionary["Nandgaon-Khandeshwar"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Waghoda", TalukaId = talukaDictionary["Nandgaon-Khandeshwar"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                #endregion


                #region AMRAVATI-Warud
                villagelist.Add(new Village() { Name = "Bhemadi", TalukaId = talukaDictionary["Warud"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Daywadi", TalukaId = talukaDictionary["Warud"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Moti", TalukaId = talukaDictionary["Warud"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pimpalshenda", TalukaId = talukaDictionary["Warud"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Zatamzari", TalukaId = talukaDictionary["Warud"], DistrictId = districtDictionary["Amravati"], StateID = stateDictionary["Maharashtra"] });
                #endregion
                #endregion

                #region Aurangabad
                #region Aurangabad-Aurangabad
                villagelist.Add(new Village() { Name = "Ambelohol", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Babargoan", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bhalgoan", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bhendala", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bhoyagoan", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bolegoan", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Charatha", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dhavlapuri", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dighi", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Hadiyabad", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Hatmali", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kachner Tanda 1", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kachner Tana 2", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kachner Tanda 3", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kachner Tana 4", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kachner Tanda 5", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kachner Tana 6", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kodapur", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Malunja BK", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Malunja Kh", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Mendi", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Murumkheda", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Padampura", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Palasgoan", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pimpalkhunda", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sanjapur", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shelud", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shingi", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tembhapuri", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Toki", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Uttarwadi", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Zanardi", TalukaId = talukaDictionary["Aurangabad"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });

                #endregion
                #region Aurangabad-Paithan
                villagelist.Add(new Village() { Name = "Balanager", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bramhangoan", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dabhrul", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Daregoan Paitan", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Devgoan", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dongoan", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ektuni", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Georai Bashi", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gharegoan", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Hirapur", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Honobachiwadi", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "kapuswadi", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kherda", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Parundi", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shivgad Tanda", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sonwadi", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sundarwadi(Hamlate)", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tanda", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tekdi Tanda", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wanjar wadi(Apdulpur)", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wawa", TalukaId = talukaDictionary["Paithan"], DistrictId = districtDictionary["Aurangabad(Maharashtra)"], StateID = stateDictionary["Maharashtra"] });

                #endregion
                #endregion

                #region Beed
                #region Beed-AMBEJOGAI[55]
                villagelist.Add(new Village() { Name = "Anjanapur", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bharaj", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bhavthava", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chopanwad", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dhanora", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "dagadwadi", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dipewadgoan", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Estal", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Hivra", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Javalgoan", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Jogaiwadi", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kalvit Tanda", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kodari", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kopara", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Limgoan", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Makegoan", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Mamdapur", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Morali", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Murti", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nandgoan", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pathan Mandva", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pattiwadgoan", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pimpla Dhaiguda", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pokhri", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Rajewadi", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Rakshaswadi", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sakud", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Seluamba", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shirpatraiwadi", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Somnath Borgoan", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sugaon", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tahaborgoan", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Umrai", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Varapgaon", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Waghala", TalukaId = talukaDictionary["Ambejogai"], DistrictId = districtDictionary["Beed"], StateID = stateDictionary["Maharashtra"] });

                #endregion
                #endregion

                #region Dhule
                #region Dhule-Sakri[55]
                villagelist.Add(new Village() { Name = "Amali", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Basraval", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bhadgoan", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Charanmal", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chorwad", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dapur", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Daregoan Sakri", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Domkani", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dongarpada", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gartad", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kalamb", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khairkhunda", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khandbara", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kharadbari", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khatyal", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Maindane", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Manjari", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Mhasale", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Mogarpada", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Mohagoan", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nagpur - V", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Navagoan", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pimpalpada", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Rohod", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shendwad", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shirsole", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shiv", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sukhapur", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sutare", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ubhand", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Valwhe", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vardadi", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vardhane", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vitawe", TalukaId = talukaDictionary["Sakri"], DistrictId = districtDictionary["Dhule"], StateID = stateDictionary["Maharashtra"] });
                #endregion
                #endregion

                #region Jalna
                #region Jalna-Ambad[55]
                villagelist.Add(new Village() { Name = "Aava", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Badapur", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bakshyachi wadi", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bhiwandi Bodkha", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chikangoan", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chinchkhed", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Deshgavhan", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Domegoan", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gangaramwadi", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Hastpokhari", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Jamkhed", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Jogeshwar wadi", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kanadgoan", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Karjat", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khedgoan", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kingoan", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kingoan wadi", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Koli Shirasgoan", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kouchal wadi", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Lonar Bhayegoan", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Maher Bhayegoan", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Maliwadi", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Mardi", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Math Jalgoan", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nagonyachiwadi", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nandi", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nehalsingh wadi", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pagirwadi", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Parner Ambad", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pimparkhed", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Rohilagad", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shiradhon", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shirner", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Taka", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vithalwadi", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wadilsura", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Zodegoan", TalukaId = talukaDictionary["Ambad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });

                #endregion


                #region Jalna-Bhokardan[55]
                villagelist.Add(new Village() { Name = "Adgoan", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Anvapada", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Avhana", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Babulgoan", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Banegoan", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Baranjala Sable", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bellora", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bhivpur", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bhorkheda-Bkd", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bargoan Taru", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chandai Ekko", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chandai Tepli", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chandai Thombre", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chincholi", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dahigoan", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dehed", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Deulgoan Tad", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dhondkheda", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ekefal", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Garkheda Bkd", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gavhan Sangameshwar", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ghoshegoan", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Godri", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gosegoan", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Hisoda", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ibrahimpur Bkd", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Jainpur Kothara", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Janefal Gaikwad", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Janefal Dhabhadi", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Jaydevwadi", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Karlawadi", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kedarkheda", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khandala", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kshirsagar", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Koda", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kolegoan", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kosgoan", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kotha dabadi", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kotha Jahagir", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kotyha koli", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kothara jenpur", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kumbhari", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Lingewadi", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Malegoan", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Malkapur", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Malkheda", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Manapur", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Merkheda", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Mohlai", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Murtad palaskheda", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Muthad", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nanja", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nimbola", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Padmavti", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Palaskheda Thombre", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Palskheda Pimpale", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pandarpurwadi", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Parat Bk", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Parat Kh", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Perjapur", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pimapalgoan Kolte", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pimpalgoan Barab", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pralhadpur", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Rajala", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Relgoan", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Savangi Avghadrav", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Savkheda Bkd", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sirasgoan Mandap", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Soyagaon Devi", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Subhanpur", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sundarwadi Bkd", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tadkalas", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Thigalkheda", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vizora", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wadhod Tangada", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wadi ", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wadona", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Walsa Wadala", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Warud Bkd", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wadi Kh", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wadi Bk", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wadshed", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wal Savangi", TalukaId = talukaDictionary["Bhokardan"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });

                #endregion


                #region Jalna-Jafferabad[42]
                villagelist.Add(new Village() { Name = "Adha", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Aradkheda", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bharadkheda", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bharaj bk", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bhatodi", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bhorkheda gayki", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bhorkhedaj", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Borgaon(bk)", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Borgaon(math)", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chapner", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chinchkheda", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dhonkheda", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Garkheda jfd", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ghankheda", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gokulwadi", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gopi", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Janefal", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Javkheda theng", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khaparkheda", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khasgaon", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khota jahangir", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kolegaon", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kusali", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Mahsrul", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Mhasrul", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pasodi", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Rastal", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Savargaon(mhaske)", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Savarkheda jfd", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Savasani", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sawangi", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shindhi", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shipora", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sindhi", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Songiri", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sonkheda", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Takli", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tapovan(gondhan)", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Varkheda viro", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wadala", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Yeota", TalukaId = talukaDictionary["Jafferabad"], DistrictId = districtDictionary["Jalna"], StateID = stateDictionary["Maharashtra"] });


                #endregion
                #endregion

                #region Nandurbar
                #region Nandurbar-Navapur Remaining
                villagelist.Add(new Village() { Name = "Anjane", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bilda", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chikhali", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chorvihir", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gogegoan", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Jamada", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Charatha", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kadwan", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khatgoan", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Morad", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tarapur", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vadfali", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Wavdi", TalukaId = talukaDictionary["Nawapur"], DistrictId = districtDictionary["Nandurbar"], StateID = stateDictionary["Maharashtra"] });
                #endregion
                #endregion

                #region Nashik
                #region Nashik-Sinnar
                villagelist.Add(new Village() { Name = "Chandrapur", TalukaId = talukaDictionary["Sinnar"], DistrictId = districtDictionary["Nashik"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Jamagoan", TalukaId = talukaDictionary["Sinnar"], DistrictId = districtDictionary["Nashik"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khaprale", TalukaId = talukaDictionary["Sinnar"], DistrictId = districtDictionary["Nashik"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nandur", TalukaId = talukaDictionary["Sinnar"], DistrictId = districtDictionary["Nashik"], StateID = stateDictionary["Maharashtra"] });

                #endregion
                #endregion

                #region Pune
                #region Pune-Ambegoan
                villagelist.Add(new Village() { Name = "Awasari Kh", TalukaId = talukaDictionary["Ambegaon"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bharadi", TalukaId = talukaDictionary["Ambegaon"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khadki", TalukaId = talukaDictionary["Ambegaon"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khadki Ambegoan", TalukaId = talukaDictionary["Ambegaon"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nirgudsir", TalukaId = talukaDictionary["Ambegaon"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Paragoan Shingve", TalukaId = talukaDictionary["Ambegaon"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Thorandale", TalukaId = talukaDictionary["Ambegaon"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });

                #endregion


                #region Pune-Junnar
                villagelist.Add(new Village() { Name = "Bhorwadi", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bori Kh", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chas", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Chincholi Junnar", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dhalewadi", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dhamankhel", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dhangarwadi", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gadachiwadi", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Girwadi", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gunjalwadi", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Hivre", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kandali", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khanapur", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khilarwadi", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khodad", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kuran", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kusur", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Manjarwadi", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nagadwadi", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Narayangoan", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Nimdare", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Parunde", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pimpalgoan ", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Salwadi", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sawargoan", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shiroli Bk", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Shiroli Kh", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vadaj", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vadgoan", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vadgoan Sahani ", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Warulwadi", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Yedgoan", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Yenere", TalukaId = talukaDictionary["Junnar"], DistrictId = districtDictionary["Pune"], StateID = stateDictionary["Maharashtra"] });

                #endregion
                #endregion

                #region Raigad
                #region Raigad-Khalapur

                villagelist.Add(new Village() { Name = "Dhavli", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dhonvat", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Thakurwadi", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gorthan Kh", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khir Kandi", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kusavade", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Takavali", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tambati", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tambati-Thakarwadi", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tambatiwadi", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tambi", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vadval", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vanvathe", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Vasote", TalukaId = talukaDictionary["Khalapur"], DistrictId = districtDictionary["Raigad"], StateID = stateDictionary["Maharashtra"] });


                #endregion
                #endregion

                #region Wardha
                #region Wardha-Arvi
                villagelist.Add(new Village() { Name = "Ajangoan Jambuldar", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bodad pod", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Bothali Panzara", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Choramba", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Gaurkheda", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kinhala", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pachod", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Panzara", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Parsodi", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Saldara", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sukhali", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Talegoan", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Torada", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Tembri", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Umri", TalukaId = talukaDictionary["Arvi"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });

                #endregion


                #region Wardha-Deoli
                villagelist.Add(new Village() { Name = "Dhonapur", TalukaId = talukaDictionary["Deoli"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ekamba", TalukaId = talukaDictionary["Deoli"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Loni", TalukaId = talukaDictionary["Deoli"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pipri", TalukaId = talukaDictionary["Deoli"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                #endregion

                #region Wardha-Hinganghat
                villagelist.Add(new Village() { Name = "Dhivari", TalukaId = talukaDictionary["Hinganghat"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dhochi", TalukaId = talukaDictionary["Hinganghat"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Hadsti", TalukaId = talukaDictionary["Hinganghat"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Pipri", TalukaId = talukaDictionary["Hinganghat"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sasti", TalukaId = talukaDictionary["Hinganghat"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Savangi", TalukaId = talukaDictionary["Hinganghat"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                #endregion

                #region Wardha-Karanja
                villagelist.Add(new Village() { Name = "Ekarjun", TalukaId = talukaDictionary["Karanja"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Jassapur", TalukaId = talukaDictionary["Karanja"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Kinhala", TalukaId = talukaDictionary["Karanja"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Malegoan", TalukaId = talukaDictionary["Karanja"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                #endregion

                #region Wardha-Samudrapur
                villagelist.Add(new Village() { Name = "Daulatpur", TalukaId = talukaDictionary["Samudrapur"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Dhamangoan", TalukaId = talukaDictionary["Samudrapur"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Ganeshpur", TalukaId = talukaDictionary["Samudrapur"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Khursapar", TalukaId = talukaDictionary["Karanja"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });
                villagelist.Add(new Village() { Name = "Sukali", TalukaId = talukaDictionary["Karanja"], DistrictId = districtDictionary["Wardha"], StateID = stateDictionary["Maharashtra"] });


                #endregion
                #endregion

                #region Yavatmal
                #region Yavatmal-Ralegaon

                villagelist.Add(new Village() { Name = "Sonurli", TalukaId = talukaDictionary["Ralegaon"], DistrictId = districtDictionary["Yavatmal"], StateID = stateDictionary["Maharashtra"] });
                #endregion
                #endregion

                #endregion

                #region State-Andhra Pradesh
                #region Karnool
                #region Karnool-Atmakur
                villagelist.Add(new Village() { Name = "Indireshwaram", TalukaId = talukaDictionary["Atmakur"], DistrictId = districtDictionary["Kurnool"], StateID = stateDictionary["Andhra Pradesh"] });
                villagelist.Add(new Village() { Name = "Kottadlcheruvu", TalukaId = talukaDictionary["Atmakur"], DistrictId = districtDictionary["Kurnool"], StateID = stateDictionary["Andhra Pradesh"] });
                villagelist.Add(new Village() { Name = "Krishnapuram", TalukaId = talukaDictionary["Atmakur"], DistrictId = districtDictionary["Kurnool"], StateID = stateDictionary["Andhra Pradesh"] });
                villagelist.Add(new Village() { Name = "Sanjeevanagarthanda", TalukaId = talukaDictionary["Atmakur"], DistrictId = districtDictionary["Kurnool"], StateID = stateDictionary["Andhra Pradesh"] });
                villagelist.Add(new Village() { Name = "Siddapuram", TalukaId = talukaDictionary["Atmakur"], DistrictId = districtDictionary["Kurnool"], StateID = stateDictionary["Andhra Pradesh"] });
                villagelist.Add(new Village() { Name = "Vadlarampuram", TalukaId = talukaDictionary["Atmakur"], DistrictId = districtDictionary["Kurnool"], StateID = stateDictionary["Andhra Pradesh"] });
                villagelist.Add(new Village() { Name = "Kurkunda", TalukaId = talukaDictionary["Atmakur"], DistrictId = districtDictionary["Kurnool"], StateID = stateDictionary["Andhra Pradesh"] });
                #endregion
                #region Karnool-Jupadu Bungalow
                villagelist.Add(new Village() { Name = "Jupadu Bungalow", TalukaId = talukaDictionary["Jupadu Bungalow"], DistrictId = districtDictionary["Kurnool"], StateID = stateDictionary["Andhra Pradesh"] });
                villagelist.Add(new Village() { Name = "Mandlem", TalukaId = talukaDictionary["Jupadu Bungalow"], DistrictId = districtDictionary["Kurnool"], StateID = stateDictionary["Andhra Pradesh"] });
                villagelist.Add(new Village() { Name = "Thangadancha", TalukaId = talukaDictionary["Jupadu Bungalow"], DistrictId = districtDictionary["Kurnool"], StateID = stateDictionary["Andhra Pradesh"] });

                #endregion
                #endregion
                #endregion

                #region State-Jharkhand
                #region Khunti
                #region Khunti-Murhu
                villagelist.Add(new Village() { Name = "Balo", TalukaId = talukaDictionary["Murhu"], DistrictId = districtDictionary["Khunti"], StateID = stateDictionary["Jharkhand"] });
                villagelist.Add(new Village() { Name = "Bamni", TalukaId = talukaDictionary["Murhu"], DistrictId = districtDictionary["Khunti"], StateID = stateDictionary["Jharkhand"] });
                villagelist.Add(new Village() { Name = "Gangira", TalukaId = talukaDictionary["Murhu"], DistrictId = districtDictionary["Khunti"], StateID = stateDictionary["Jharkhand"] });
                villagelist.Add(new Village() { Name = "Ghaghari", TalukaId = talukaDictionary["Murhu"], DistrictId = districtDictionary["Khunti"], StateID = stateDictionary["Jharkhand"] });
                villagelist.Add(new Village() { Name = "Gurhami", TalukaId = talukaDictionary["Murhu"], DistrictId = districtDictionary["Khunti"], StateID = stateDictionary["Jharkhand"] });
                villagelist.Add(new Village() { Name = "Korakel", TalukaId = talukaDictionary["Murhu"], DistrictId = districtDictionary["Khunti"], StateID = stateDictionary["Jharkhand"] });
                villagelist.Add(new Village() { Name = "Morgoan", TalukaId = talukaDictionary["Murhu"], DistrictId = districtDictionary["Khunti"], StateID = stateDictionary["Jharkhand"] });
                villagelist.Add(new Village() { Name = "Oskeya", TalukaId = talukaDictionary["Murhu"], DistrictId = districtDictionary["Khunti"], StateID = stateDictionary["Jharkhand"] });
                villagelist.Add(new Village() { Name = "Toner", TalukaId = talukaDictionary["Murhu"], DistrictId = districtDictionary["Khunti"], StateID = stateDictionary["Jharkhand"] });

                #endregion
                #endregion
                #endregion

                #region State- Madhya Pradesh
                #region Mandle
                #region Mandle-Narayanganj
                villagelist.Add(new Village() { Name = "Bamhni", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Banar", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Barbaspur", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Barbati", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Bawal", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Bijigoan", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Chatra", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Chiri", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Dobha", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Dunagariya", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Gadhadevri mal", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Gadhadevri Raiyat", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Gujarsani", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Harratikur", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Khinha Ryt", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Kumha", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Mukas Kalan", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Partala", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Salhepani", TalukaId = talukaDictionary["Narayanganj"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });

                #endregion
                #region Mandle-Niwas
                villagelist.Add(new Village() { Name = "Bhawal", TalukaId = talukaDictionary["Niwas"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });
                villagelist.Add(new Village() { Name = "Mukas Kalan", TalukaId = talukaDictionary["Niwas"], DistrictId = districtDictionary["Mandla"], StateID = stateDictionary["Madhya Pradesh"] });

                #endregion
                #endregion
                #endregion

                #region State-Rajasthan
                #region udaipur 
                #region Udaipur-Gogunda
                villagelist.Add(new Village() { Name = "Badundiya", TalukaId = talukaDictionary["Gogunda"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Bagrunda(kheda)", TalukaId = talukaDictionary["Gogunda"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Baravli", TalukaId = talukaDictionary["Gogunda"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Hayla", TalukaId = talukaDictionary["Gogunda"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Modva", TalukaId = talukaDictionary["Gogunda"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Umerna", TalukaId = talukaDictionary["Gogunda"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Visma", TalukaId = talukaDictionary["Gogunda"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                #endregion
                #region Udaipur-Jhadol
                villagelist.Add(new Village() { Name = "Adkaliya", TalukaId = talukaDictionary["Jhadol"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Dimri", TalukaId = talukaDictionary["Jhadol"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Kanthariya", TalukaId = talukaDictionary["Jhadol"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Khardiya", TalukaId = talukaDictionary["Jhadol"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Lunawaton ka kheda", TalukaId = talukaDictionary["Jhadol"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Wadad", TalukaId = talukaDictionary["Jhadol"], DistrictId = districtDictionary["Udaipur"], StateID = stateDictionary["Rajasthan"] });
                #endregion
                #endregion
                #region Pratapgarh
                #region Pratapgarh-Pratapgarh
                villagelist.Add(new Village() { Name = "Amba Khera", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Amli Phain", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "bambori", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Billikhera", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Chamlawada", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Chapoton ka pathar", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Gumanpuara", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });

                villagelist.Add(new Village() { Name = "Jhantra", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Kesarpura", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Khoriya", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Madhura talab", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Naya Tapra", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Padampura", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Raghunathpura", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Saba Khera No 1", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Sagamagri", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Saripipli", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Sewra", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Teekhi Magri", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Ummedpura", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });
                villagelist.Add(new Village() { Name = "Untakheda", TalukaId = talukaDictionary["Pratapgarh"], DistrictId = districtDictionary["Pratapgarh"], StateID = stateDictionary["Rajasthan"] });

                #endregion
                #endregion
                #endregion

                #region Other State Rather Than Maharashtra

                #region State- Odisha
                //#region Gajpati
                //#region Gajpati-Gumma
                //villagelist.Add(new Village() { Name = "Almaida", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Amreising", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Ariada", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Babudi", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Baijhal", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Bebe", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Bhubani", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Brusava", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Gadebagarjang", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Gumma", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Jurbdi", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Kudinguda", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Olaida", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Padamsingi", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Papada", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Ruisingi", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Sailunda", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Sanamunisingi", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Sindring", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Talasing", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Tameigarjang", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Tarbaul", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Tempogiri", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Tuburuda", TalukaId = talukaDictionary["Gumma"] });
                //villagelist.Add(new Village() { Name = "Udasing", TalukaId = talukaDictionary["Gumma"] });

                //#endregion
                //#region Gajpati-Mohana
                //villagelist.Add(new Village() { Name = "Baghamari", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Bandhaguda", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Baunsuri", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Benipadar", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Bom", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Darupani", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Debakua", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Dhadi Ambo", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Gadring", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Galama", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Hachipanga", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Handima", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Jadaguda", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Jagannathpur", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Jaragonda", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Joda Ambo", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "K.Kalameri", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Kandhagani", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Kantasuru", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Kechpanka", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Kesariguda", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Kidasing", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Komangopadar", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Kontapadar", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Kunkorada", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Lobarsing", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Lompada", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Ludru", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Mohana I", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Mohana II", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Mohana III", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Pendrakhal", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Pidiary", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Pilipaju", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Podagalama", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Ramchandrapur", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Ratama", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Sagada", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Shymsundarpur", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Sikulipadar", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Tiama", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Topogiri", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Tuaguda", TalukaId = talukaDictionary["Mohana"] });
                //#endregion
                //#region Gajpati-Nuagada
                //villagelist.Add(new Village() { Name = "Adangar", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Beroi", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Gotha", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Guduru", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Kimbule", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Kinada", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Kinchdal", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Korada", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Lating", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Mandidi", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Naugada", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Ora", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Patipanga", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Phategudu", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Rajabul", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Salonkoi", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Sikabadi", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Tilikar", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Uama", TalukaId = talukaDictionary["Nuagada"] });
                //villagelist.Add(new Village() { Name = "Udayaguda", TalukaId = talukaDictionary["Nuagada"] });
                //#endregion
                //#region Gajpati-Rayagada
                //villagelist.Add(new Village() { Name = "Amarsing", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Arsiling", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "B.Antarsing", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Baunsgoan", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Belpadar", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Bengasahi", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Bolodaguma", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Bursunda", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Chanagoan", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Deula", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Dimirigoan", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "E.colony", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Gangabada", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Jakara", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Kalingo", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Kasibada", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Kerandi", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Khambagoan", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Kinchiling", TalukaId = talukaDictionary["Rayagada"] });
                //villagelist.Add(new Village() { Name = "Lakida", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Latigoan", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Manikapatna", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Oya", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Raikuguma", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Rayagada ", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "S.antarsing", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Sankaranji Shai", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Sankunda", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Subhandrapur", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Tumbo Goan", TalukaId = talukaDictionary["Mohana"] });
                //villagelist.Add(new Village() { Name = "Y.Angarsing", TalukaId = talukaDictionary["Mohana"] });
                //#endregion
                //#endregion
                #endregion

                #region State-Telangana
                //#region Mehbubnager
                //#region Mehbubnager-Damagidda
                //villagelist.Add(new Village() { Name = "Kamasanpally", TalukaId = talukaDictionary["Damagidda"] });
                //villagelist.Add(new Village() { Name = "Monopuram", TalukaId = talukaDictionary["Damagidda"] });
                //villagelist.Add(new Village() { Name = "Perapally", TalukaId = talukaDictionary["Damagidda"] });
                //villagelist.Add(new Village() { Name = "Vedegeri", TalukaId = talukaDictionary["Damagidda"] });
                //villagelist.Add(new Village() { Name = "Vattugundla", TalukaId = talukaDictionary["Damagidda"] });
                //#endregion
                //#region Mehbubnager-Kalvakurthy
                //villagelist.Add(new Village() { Name = "Chanradana", TalukaId = talukaDictionary["Kalvakurthy"] });
                //villagelist.Add(new Village() { Name = "Israipalli", TalukaId = talukaDictionary["Kalvakurthy"] });
                //villagelist.Add(new Village() { Name = "Jangi Reddy Palli", TalukaId = talukaDictionary["Kalvakurthy"] });
                //villagelist.Add(new Village() { Name = "Thangadancha", TalukaId = talukaDictionary["Kalvakurthy"] });
                //#endregion
                //#region Mehbubnager-Narayanpet
                //villagelist.Add(new Village() { Name = "Bapunpally", TalukaId = talukaDictionary["Narayanpet"] });
                //villagelist.Add(new Village() { Name = "Kamsanpally", TalukaId = talukaDictionary["Narayanpet"] });
                //villagelist.Add(new Village() { Name = "Perapalla Thanda", TalukaId = talukaDictionary["Narayanpet"] });
                //villagelist.Add(new Village() { Name = "Perapalli", TalukaId = talukaDictionary["Narayanpet"] });
                //#endregion
                //#region Mehbubnager-Tallakondapally
                //villagelist.Add(new Village() { Name = "Badunapur", TalukaId = talukaDictionary["Tallakondapally"] });
                //villagelist.Add(new Village() { Name = "Chandradana", TalukaId = talukaDictionary["Tallakondapally"] });
                //villagelist.Add(new Village() { Name = "Haryanaike thanda", TalukaId = talukaDictionary["Tallakondapally"] });
                //villagelist.Add(new Village() { Name = "Israipally", TalukaId = talukaDictionary["Tallakondapally"] });
                //villagelist.Add(new Village() { Name = "Jangareddypally", TalukaId = talukaDictionary["Tallakondapally"] });
                //villagelist.Add(new Village() { Name = "Rampur", TalukaId = talukaDictionary["Tallakondapally"] });
                //villagelist.Add(new Village() { Name = "Tallakondapally", TalukaId = talukaDictionary["Tallakondapally"] });
                //villagelist.Add(new Village() { Name = "Velajala", TalukaId = talukaDictionary["Tallakondapally"] });

                //#endregion
                //#region Mehbubnager-Veldanda
                //villagelist.Add(new Village() { Name = "Kuppagundla", TalukaId = talukaDictionary["Veldanda"] });
                //villagelist.Add(new Village() { Name = "Nagaraopally Thanda", TalukaId = talukaDictionary["Veldanda"] });
                //villagelist.Add(new Village() { Name = "Narayanapur Gate", TalukaId = talukaDictionary["Veldanda"] });
                //villagelist.Add(new Village() { Name = "Peddapur", TalukaId = talukaDictionary["Veldanda"] });
                //villagelist.Add(new Village() { Name = "Ragaipally", TalukaId = talukaDictionary["Veldanda"] });
                //villagelist.Add(new Village() { Name = "Veldande", TalukaId = talukaDictionary["Veldanda"] });

                //#endregion
                //#endregion

                #endregion
                #endregion

                var villages = _context.Village.ToList();
                foreach (var ta in villagelist)
                {
                    var _villageName = ta.Name.Trim();
                    /*var talukaObject = _context.Taluka.FirstOrDefault(t => t.DistrictId == ta.DistrictId)*/
                    ;
                    var VillageObject = villages.FirstOrDefault(t => t.Name == _villageName);

                    if (VillageObject == null)
                    {
                        _context.Village.Add(new Village { TalukaId = ta.TalukaId, StateID = ta.StateID, DistrictId = ta.DistrictId, CreatorUserId = 2, Name = _villageName, TenantId = _tenantId });
                    }

                }

                _context.SaveChanges();


                #endregion
            }
            catch (Exception ex)
            {
                Debug.WriteLine("MESSAGE:-------------- " + ex.Message);
            }




        }
    }
}
