﻿using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.EntityFrameworkCore;
using WOTR.PM.UnitOfMeasures;

namespace WOTR.PM.Migrations.Seed
{
   public class UnitofMeasureBuilder
    {
        private readonly PMDbContext _context;
        public IAbpSession AbpSession { get; set; }
        private readonly int _TentantId;

        public UnitofMeasureBuilder(PMDbContext context, int TentantId)
        {
            _context = context;
            _TentantId = TentantId;
        }
        public void create()
        {

            List<string> UnitofMeasureList = new List<string>() { "No", "Children", "Ha", "Farmers", "Villages", "breeds", "liter", "meter" };


            try
            {
                foreach (var item in UnitofMeasureList)
                {
                    var type = _context.UnitofMeasures.IgnoreQueryFilters().FirstOrDefault(t => t.Name == item.ToString());
                    if (type == null)
                    {
                        _context.UnitofMeasures.Add(
                            new UnitofMeasure
                            {

                                Name = item.ToString(),
                                TenantId = _TentantId,
                               
                                CreatorUserId = 2

                            });

                    }

                }
                _context.SaveChanges();



            }
            catch (Exception)
            {

                throw;
            }


        }

    }
}
