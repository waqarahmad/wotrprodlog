﻿using WOTR.PM.EntityFrameworkCore;

namespace WOTR.PM.Migrations.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly PMDbContext _context;

        public InitialHostDbBuilder(PMDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();

            _context.SaveChanges();
        }
    }
}
