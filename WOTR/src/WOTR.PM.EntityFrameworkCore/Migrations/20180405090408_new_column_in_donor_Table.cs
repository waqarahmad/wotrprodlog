﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class new_column_in_donor_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address1",
                table: "Donor");

            migrationBuilder.DropColumn(
                name: "Address1Pincode",
                table: "Donor");

            migrationBuilder.DropColumn(
                name: "Address2Pincode",
                table: "Donor");

            migrationBuilder.DropColumn(
                name: "CompanyAddressPincode",
                table: "Donor");

            migrationBuilder.DropColumn(
                name: "ContactNo1",
                table: "Donor");

            migrationBuilder.DropColumn(
                name: "ContactNo2",
                table: "Donor");

            migrationBuilder.DropColumn(
                name: "EmailAddress",
                table: "Donor");

            migrationBuilder.DropColumn(
                name: "Gender",
                table: "Donor");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Donor");

            migrationBuilder.RenameColumn(
                name: "CompanyAddressState",
                table: "Donor",
                newName: "ThridOtherEmail");

            migrationBuilder.RenameColumn(
                name: "CompanyAddressCity",
                table: "Donor",
                newName: "ThridName");

            migrationBuilder.RenameColumn(
                name: "Address2State",
                table: "Donor",
                newName: "ThridEmailAddress");

            migrationBuilder.RenameColumn(
                name: "Address2Country",
                table: "Donor",
                newName: "ThridContactNo");

            migrationBuilder.RenameColumn(
                name: "Address2City",
                table: "Donor",
                newName: "ThridAddress");

            migrationBuilder.RenameColumn(
                name: "Address2",
                table: "Donor",
                newName: "SecondOtherEmail");

            migrationBuilder.RenameColumn(
                name: "Address1State",
                table: "Donor",
                newName: "SecondName");

            migrationBuilder.RenameColumn(
                name: "Address1Country",
                table: "Donor",
                newName: "SecondEmailAddress");

            migrationBuilder.RenameColumn(
                name: "Address1City",
                table: "Donor",
                newName: "SecondContactNo");

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Donor",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "CompanyName",
                table: "Donor",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyEmailID",
                table: "Donor",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyContactNo",
                table: "Donor",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyAddressCountry",
                table: "Donor",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyAddress",
                table: "Donor",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstAddress",
                table: "Donor",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstContactNo",
                table: "Donor",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstEmailAddress",
                table: "Donor",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstOtherEmail",
                table: "Donor",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SecondAddress",
                table: "Donor",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstAddress",
                table: "Donor");

            migrationBuilder.DropColumn(
                name: "FirstContactNo",
                table: "Donor");

            migrationBuilder.DropColumn(
                name: "FirstEmailAddress",
                table: "Donor");

            migrationBuilder.DropColumn(
                name: "FirstOtherEmail",
                table: "Donor");

            migrationBuilder.DropColumn(
                name: "SecondAddress",
                table: "Donor");

            migrationBuilder.RenameColumn(
                name: "ThridOtherEmail",
                table: "Donor",
                newName: "CompanyAddressState");

            migrationBuilder.RenameColumn(
                name: "ThridName",
                table: "Donor",
                newName: "CompanyAddressCity");

            migrationBuilder.RenameColumn(
                name: "ThridEmailAddress",
                table: "Donor",
                newName: "Address2State");

            migrationBuilder.RenameColumn(
                name: "ThridContactNo",
                table: "Donor",
                newName: "Address2Country");

            migrationBuilder.RenameColumn(
                name: "ThridAddress",
                table: "Donor",
                newName: "Address2City");

            migrationBuilder.RenameColumn(
                name: "SecondOtherEmail",
                table: "Donor",
                newName: "Address2");

            migrationBuilder.RenameColumn(
                name: "SecondName",
                table: "Donor",
                newName: "Address1State");

            migrationBuilder.RenameColumn(
                name: "SecondEmailAddress",
                table: "Donor",
                newName: "Address1Country");

            migrationBuilder.RenameColumn(
                name: "SecondContactNo",
                table: "Donor",
                newName: "Address1City");

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Donor",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyName",
                table: "Donor",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "CompanyEmailID",
                table: "Donor",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "CompanyContactNo",
                table: "Donor",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "CompanyAddressCountry",
                table: "Donor",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "CompanyAddress",
                table: "Donor",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "Address1",
                table: "Donor",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "Address1Pincode",
                table: "Donor",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "Address2Pincode",
                table: "Donor",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "CompanyAddressPincode",
                table: "Donor",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "ContactNo1",
                table: "Donor",
                maxLength: 15,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactNo2",
                table: "Donor",
                maxLength: 15,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmailAddress",
                table: "Donor",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Gender",
                table: "Donor",
                maxLength: 1,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Donor",
                nullable: false,
                defaultValue: "");
        }
    }
}
