﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_PrgActionAreaActivityMapping_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PrgActionAreaActivityMapping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActionAreaID = table.Column<int>(nullable: true),
                    ActivityID = table.Column<int>(nullable: true),
                    ComponentID = table.Column<int>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    SubActionAreaID = table.Column<int>(nullable: true),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrgActionAreaActivityMapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrgActionAreaActivityMapping_ActionArea_ActionAreaID",
                        column: x => x.ActionAreaID,
                        principalTable: "ActionArea",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PrgActionAreaActivityMapping_Activity_ActivityID",
                        column: x => x.ActivityID,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PrgActionAreaActivityMapping_Component_ComponentID",
                        column: x => x.ComponentID,
                        principalTable: "Component",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PrgActionAreaActivityMapping_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrgActionAreaActivityMapping_SubActionArea_SubActionAreaID",
                        column: x => x.SubActionAreaID,
                        principalTable: "SubActionArea",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PrgActionAreaActivityMapping_ActionAreaID",
                table: "PrgActionAreaActivityMapping",
                column: "ActionAreaID");

            migrationBuilder.CreateIndex(
                name: "IX_PrgActionAreaActivityMapping_ActivityID",
                table: "PrgActionAreaActivityMapping",
                column: "ActivityID");

            migrationBuilder.CreateIndex(
                name: "IX_PrgActionAreaActivityMapping_ComponentID",
                table: "PrgActionAreaActivityMapping",
                column: "ComponentID");

            migrationBuilder.CreateIndex(
                name: "IX_PrgActionAreaActivityMapping_ProgramID",
                table: "PrgActionAreaActivityMapping",
                column: "ProgramID");

            migrationBuilder.CreateIndex(
                name: "IX_PrgActionAreaActivityMapping_SubActionAreaID",
                table: "PrgActionAreaActivityMapping",
                column: "SubActionAreaID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PrgActionAreaActivityMapping");
        }
    }
}
