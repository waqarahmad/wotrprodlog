﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Update_Table_IIQuestionary_Alter_FrequencyOfOccurance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FrequencyOfOccurance",
                table: "IIQuestionary");

            migrationBuilder.AddColumn<int>(
                name: "FrequencyOfOccurrenceId",
                table: "IIQuestionary",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_IIQuestionary_FrequencyOfOccurrenceId",
                table: "IIQuestionary",
                column: "FrequencyOfOccurrenceId");

            migrationBuilder.AddForeignKey(
                name: "FK_IIQuestionary_FrequencyOfoccurrences_FrequencyOfOccurrenceId",
                table: "IIQuestionary",
                column: "FrequencyOfOccurrenceId",
                principalTable: "FrequencyOfoccurrences",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_IIQuestionary_FrequencyOfoccurrences_FrequencyOfOccurrenceId",
                table: "IIQuestionary");

            migrationBuilder.DropIndex(
                name: "IX_IIQuestionary_FrequencyOfOccurrenceId",
                table: "IIQuestionary");

            migrationBuilder.DropColumn(
                name: "FrequencyOfOccurrenceId",
                table: "IIQuestionary");

            migrationBuilder.AddColumn<string>(
                name: "FrequencyOfOccurance",
                table: "IIQuestionary",
                nullable: true);
        }
    }
}
