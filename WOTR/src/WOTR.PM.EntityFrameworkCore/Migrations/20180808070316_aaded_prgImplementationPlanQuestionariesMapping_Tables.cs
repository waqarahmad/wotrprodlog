﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class aaded_prgImplementationPlanQuestionariesMapping_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PrgImplementationsPlans",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CheckListID = table.Column<int>(nullable: false),
                    CheckListItemID = table.Column<int>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    LocationID = table.Column<int>(nullable: false),
                    PrgActionAreaActivityMappingID = table.Column<int>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    UnitOfMeasuresID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrgImplementationsPlans", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrgImplementationsPlans_Checklist_CheckListID",
                        column: x => x.CheckListID,
                        principalTable: "Checklist",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrgImplementationsPlans_ChecklistItem_CheckListItemID",
                        column: x => x.CheckListItemID,
                        principalTable: "ChecklistItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PrgImplementationsPlans_Location_LocationID",
                        column: x => x.LocationID,
                        principalTable: "Location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrgImplementationsPlans_PrgActionAreaActivityMapping_PrgActionAreaActivityMappingID",
                        column: x => x.PrgActionAreaActivityMappingID,
                        principalTable: "PrgActionAreaActivityMapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PrgImplementationsPlans_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PrgImplementationsPlans_UnitOfMeasure_UnitOfMeasuresID",
                        column: x => x.UnitOfMeasuresID,
                        principalTable: "UnitOfMeasure",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "prgImplementationPlanQuestionariesMapping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Answer = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    PrgImplementationsPlansID = table.Column<int>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    QuestionarieID = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_prgImplementationPlanQuestionariesMapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_prgImplementationPlanQuestionariesMapping_PrgImplementationsPlans_PrgImplementationsPlansID",
                        column: x => x.PrgImplementationsPlansID,
                        principalTable: "PrgImplementationsPlans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_prgImplementationPlanQuestionariesMapping_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_prgImplementationPlanQuestionariesMapping_Questionarie_QuestionarieID",
                        column: x => x.QuestionarieID,
                        principalTable: "Questionarie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_prgImplementationPlanQuestionariesMapping_PrgImplementationsPlansID",
                table: "prgImplementationPlanQuestionariesMapping",
                column: "PrgImplementationsPlansID");

            migrationBuilder.CreateIndex(
                name: "IX_prgImplementationPlanQuestionariesMapping_ProgramID",
                table: "prgImplementationPlanQuestionariesMapping",
                column: "ProgramID");

            migrationBuilder.CreateIndex(
                name: "IX_prgImplementationPlanQuestionariesMapping_QuestionarieID",
                table: "prgImplementationPlanQuestionariesMapping",
                column: "QuestionarieID");

            migrationBuilder.CreateIndex(
                name: "IX_PrgImplementationsPlans_CheckListID",
                table: "PrgImplementationsPlans",
                column: "CheckListID");

            migrationBuilder.CreateIndex(
                name: "IX_PrgImplementationsPlans_CheckListItemID",
                table: "PrgImplementationsPlans",
                column: "CheckListItemID");

            migrationBuilder.CreateIndex(
                name: "IX_PrgImplementationsPlans_LocationID",
                table: "PrgImplementationsPlans",
                column: "LocationID");

            migrationBuilder.CreateIndex(
                name: "IX_PrgImplementationsPlans_PrgActionAreaActivityMappingID",
                table: "PrgImplementationsPlans",
                column: "PrgActionAreaActivityMappingID");

            migrationBuilder.CreateIndex(
                name: "IX_PrgImplementationsPlans_ProgramID",
                table: "PrgImplementationsPlans",
                column: "ProgramID");

            migrationBuilder.CreateIndex(
                name: "IX_PrgImplementationsPlans_UnitOfMeasuresID",
                table: "PrgImplementationsPlans",
                column: "UnitOfMeasuresID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "prgImplementationPlanQuestionariesMapping");

            migrationBuilder.DropTable(
                name: "PrgImplementationsPlans");
        }
    }
}
