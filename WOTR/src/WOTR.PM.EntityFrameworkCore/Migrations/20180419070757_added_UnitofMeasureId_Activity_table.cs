﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_UnitofMeasureId_Activity_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UnitOfMeasuresId",
                table: "Activity",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Activity_UnitOfMeasuresId",
                table: "Activity",
                column: "UnitOfMeasuresId");

            migrationBuilder.AddForeignKey(
                name: "FK_Activity_UnitOfMeasure_UnitOfMeasuresId",
                table: "Activity",
                column: "UnitOfMeasuresId",
                principalTable: "UnitOfMeasure",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Activity_UnitOfMeasure_UnitOfMeasuresId",
                table: "Activity");

            migrationBuilder.DropIndex(
                name: "IX_Activity_UnitOfMeasuresId",
                table: "Activity");

            migrationBuilder.DropColumn(
                name: "UnitOfMeasuresId",
                table: "Activity");
        }
    }
}
