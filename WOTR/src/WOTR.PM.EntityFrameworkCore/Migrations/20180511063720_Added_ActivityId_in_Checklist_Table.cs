﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Added_ActivityId_in_Checklist_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ActivityID",
                table: "Checklist",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Checklist_ActivityID",
                table: "Checklist",
                column: "ActivityID");

            migrationBuilder.AddForeignKey(
                name: "FK_Checklist_Activity_ActivityID",
                table: "Checklist",
                column: "ActivityID",
                principalTable: "Activity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Checklist_Activity_ActivityID",
                table: "Checklist");

            migrationBuilder.DropIndex(
                name: "IX_Checklist_ActivityID",
                table: "Checklist");

            migrationBuilder.DropColumn(
                name: "ActivityID",
                table: "Checklist");
        }
    }
}
