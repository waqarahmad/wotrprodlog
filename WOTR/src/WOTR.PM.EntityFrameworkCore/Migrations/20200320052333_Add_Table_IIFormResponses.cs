﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Add_Table_IIFormResponses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IIFormResponses",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DataFillPersonId = table.Column<long>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ProgramId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    villageId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IIFormResponses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IIFormResponses_AbpUsers_DataFillPersonId",
                        column: x => x.DataFillPersonId,
                        principalTable: "AbpUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IIFormResponses_Program_ProgramId",
                        column: x => x.ProgramId,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IIFormResponses_Village_villageId",
                        column: x => x.villageId,
                        principalTable: "Village",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IIFormResponses_DataFillPersonId",
                table: "IIFormResponses",
                column: "DataFillPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_IIFormResponses_ProgramId",
                table: "IIFormResponses",
                column: "ProgramId");

            migrationBuilder.CreateIndex(
                name: "IX_IIFormResponses_villageId",
                table: "IIFormResponses",
                column: "villageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IIFormResponses");
        }
    }
}
