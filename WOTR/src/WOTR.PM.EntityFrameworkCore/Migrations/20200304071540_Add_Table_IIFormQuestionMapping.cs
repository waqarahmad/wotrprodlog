﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class Add_Table_IIFormQuestionMapping : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IIFormQuestionMapping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    FormId = table.Column<int>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    QuestionId = table.Column<int>(nullable: true),
                    iIQuestionaryId = table.Column<int>(nullable: true),
                    impactIndicatorFormId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IIFormQuestionMapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IIFormQuestionMapping_IIQuestionary_iIQuestionaryId",
                        column: x => x.iIQuestionaryId,
                        principalTable: "IIQuestionary",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_IIFormQuestionMapping_IIForm_impactIndicatorFormId",
                        column: x => x.impactIndicatorFormId,
                        principalTable: "IIForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IIFormQuestionMapping_iIQuestionaryId",
                table: "IIFormQuestionMapping",
                column: "iIQuestionaryId");

            migrationBuilder.CreateIndex(
                name: "IX_IIFormQuestionMapping_impactIndicatorFormId",
                table: "IIFormQuestionMapping",
                column: "impactIndicatorFormId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IIFormQuestionMapping");
        }
    }
}
