﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class ADD_Table_IIFormProgramMapping : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IIFormProgramMapping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    impactIndicatorFormId = table.Column<int>(nullable: true),
                    programId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IIFormProgramMapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IIFormProgramMapping_IIForm_impactIndicatorFormId",
                        column: x => x.impactIndicatorFormId,
                        principalTable: "IIForm",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_IIFormProgramMapping_Program_programId",
                        column: x => x.programId,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IIFormProgramMapping_impactIndicatorFormId",
                table: "IIFormProgramMapping",
                column: "impactIndicatorFormId");

            migrationBuilder.CreateIndex(
                name: "IX_IIFormProgramMapping_programId",
                table: "IIFormProgramMapping",
                column: "programId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IIFormProgramMapping");
        }
    }
}
