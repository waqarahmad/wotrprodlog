﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class aaded_implementationPlanCheklistId_column_prmURLmappingTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ImplementationPlanCheckListID",
                table: "prgImplementationPlanQuestionariesUrlMapping",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_prgImplementationPlanQuestionariesUrlMapping_ImplementationPlanCheckListID",
                table: "prgImplementationPlanQuestionariesUrlMapping",
                column: "ImplementationPlanCheckListID");

            migrationBuilder.AddForeignKey(
                name: "FK_prgImplementationPlanQuestionariesUrlMapping_ImplementationPlanCheckList_ImplementationPlanCheckListID",
                table: "prgImplementationPlanQuestionariesUrlMapping",
                column: "ImplementationPlanCheckListID",
                principalTable: "ImplementationPlanCheckList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_prgImplementationPlanQuestionariesUrlMapping_ImplementationPlanCheckList_ImplementationPlanCheckListID",
                table: "prgImplementationPlanQuestionariesUrlMapping");

            migrationBuilder.DropIndex(
                name: "IX_prgImplementationPlanQuestionariesUrlMapping_ImplementationPlanCheckListID",
                table: "prgImplementationPlanQuestionariesUrlMapping");

            migrationBuilder.DropColumn(
                name: "ImplementationPlanCheckListID",
                table: "prgImplementationPlanQuestionariesUrlMapping");
        }
    }
}
