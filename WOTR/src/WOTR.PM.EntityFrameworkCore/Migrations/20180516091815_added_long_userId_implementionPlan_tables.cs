﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class added_long_userId_implementionPlan_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "AssingUserId",
                table: "ImplementationPlanCheckList",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "AssingUserId",
                table: "ImplementationPlanCheckList",
                nullable: true,
                oldClrType: typeof(long),
                oldNullable: true);
        }
    }
}
