﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class removeMapSubActivityIteamsToImplementionPlanID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProgramExpense_MapSubActivityIteamsToImplementionPlan_MapSubActivityIteamsToImplementionPlanID",
                table: "ProgramExpense");

            migrationBuilder.DropIndex(
                name: "IX_ProgramExpense_MapSubActivityIteamsToImplementionPlanID",
                table: "ProgramExpense");

            migrationBuilder.DropColumn(
                name: "MapSubActivityIteamsToImplementionPlanID",
                table: "ProgramExpense");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MapSubActivityIteamsToImplementionPlanID",
                table: "ProgramExpense",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProgramExpense_MapSubActivityIteamsToImplementionPlanID",
                table: "ProgramExpense",
                column: "MapSubActivityIteamsToImplementionPlanID");

            migrationBuilder.AddForeignKey(
                name: "FK_ProgramExpense_MapSubActivityIteamsToImplementionPlan_MapSubActivityIteamsToImplementionPlanID",
                table: "ProgramExpense",
                column: "MapSubActivityIteamsToImplementionPlanID",
                principalTable: "MapSubActivityIteamsToImplementionPlan",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
