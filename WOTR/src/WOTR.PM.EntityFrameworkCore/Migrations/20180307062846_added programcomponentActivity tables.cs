﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WOTR.PM.Migrations
{
    public partial class addedprogramcomponentActivitytables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProgramComponentsActivitesMapping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityID = table.Column<int>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    ProgramComponentsMappingID = table.Column<int>(nullable: true),
                    ProgramID = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgramComponentsActivitesMapping", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProgramComponentsActivitesMapping_Activity_ActivityID",
                        column: x => x.ActivityID,
                        principalTable: "Activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramComponentsActivitesMapping_ProgramComponentsMapping_ProgramComponentsMappingID",
                        column: x => x.ProgramComponentsMappingID,
                        principalTable: "ProgramComponentsMapping",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProgramComponentsActivitesMapping_Program_ProgramID",
                        column: x => x.ProgramID,
                        principalTable: "Program",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProgramComponentsActivitesMapping_ActivityID",
                table: "ProgramComponentsActivitesMapping",
                column: "ActivityID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramComponentsActivitesMapping_ProgramComponentsMappingID",
                table: "ProgramComponentsActivitesMapping",
                column: "ProgramComponentsMappingID");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramComponentsActivitesMapping_ProgramID",
                table: "ProgramComponentsActivitesMapping",
                column: "ProgramID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProgramComponentsActivitesMapping");
        }
    }
}
