﻿using Abp.EntityFrameworkCore;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Configuration;
using WOTR.PM.PrgActionAreaActivitysMappings;

namespace WOTR.PM.EntityFrameworkCore.Repositories
{
    public class PrgActionAreaActivityMappingRepository : PMRepositoryBase<PrgActionAreaActivityMapping>, IPrgActionAreaActivityMappingRepository
    {
        private IDbContextProvider<PMDbContext> _dbContextProvider;
        private readonly IConfigurationRoot _appConfiguration;
         

        public PrgActionAreaActivityMappingRepository(IDbContextProvider<PMDbContext> dbContextProvider, IHostingEnvironment env)
        : base(dbContextProvider)

        {
            _dbContextProvider = dbContextProvider;
            _appConfiguration = env.GetAppConfiguration();
        }

        public List<PrgActionAreaActivityMappingRepositoryDto> GetPrgActionAreaActivityMappingList(int? TenantId1, long? UserID, int programId)
        {

            var dataTable = new DataTable();

            List<PrgActionAreaActivityMappingRepositoryDto> PrgActionAreaActivityMappingRepositorysummary = new List<PrgActionAreaActivityMappingRepositoryDto>();
            List<ActivityActionandSubactionArea> ActivityActionandSubactionArea = new List<ActivityActionandSubactionArea>();
            //List<ProgrameActivityMappingYearDto> ProgrameActivityMappingYear = new List<ProgrameActivityMappingYearDto>();
            List<ActionSubActionList> ActionSubActionList = new List<ActionSubActionList>();

            string count = @"SELECT * FROM (select p.ProgramStartDate,p.PrgramEndDate , pc.TenantId, pc.ComponentID as'Component', cc.Code , cc.Name as 'ComponentName',pc.ProgramID,pcm.ActivityID, uu.UserName+' '+uu.Surname as 'CreatorUserName', pcm.CreationTime,ac.Name as 'action',ac.Id as 'ActionAreaId',sub.Name as 'sub',sub.Id as 'SubActionAreaId',sub.Code as 'SubactionCode',pac.Id as 'prgActionAreaActivityMapId',CONVERT(varchar(10), pac.ActionAreaID)+',' +CONVERT(varchar(10), pac.SubActionAreaID)+',' +CONVERT(varchar(10), pac.ActivityID) as 'prgActionAreaActivitymapactivitymapId'
                         from ProgramComponentsMapping pc join Program p on pc.ProgramID =p.Id and pc.ProgramID= '" + programId + "'"
                         + " left join ProgramComponentsActivitesMapping pcm on pc.ComponentID= pcm.ComponentID and pcm.ProgramID = '" + programId + "'  and pc.ProgramID= '" + programId + "' and pcm.IsDeleted ='false'"
                         + " left join Component cc on cc.Id= pc.ComponentID and pc.ProgramID= '" + programId + "'"
                         + " left join AbpUsers uu on uu.Id= pcm.CreatorUserId and pcm.ProgramID= '" + programId + "'"
                         + " left join ActionSAActivityMapping as asm on pcm.ActivityID=asm.ActivityID"
                         + " left join ActionArea as ac on asm.ActionAreaID=ac.Id"
                         + " left join PrgActionAreaActivityMapping pac on pac.ProgramID =pcm.ProgramID and pac.ActivityID =pcm.ActivityID"
                         + " left join SubActionArea  as sub on asm.SubActionAreaID=sub.Id where pc.IsDeleted ='false') as three"
                          + " LEFT JOIN (select a.Id as 'ActivityId', a.Name as 'ActivityName',a.Description from Activity a) as One ON three.ActivityID = One.ActivityId where TenantId= '" + TenantId1 + "'";


            //@"SELECT * FROM (select p.ProgramStartDate,p.PrgramEndDate , pc.TenantId, pc.ComponentID as'Component', cc.Code , cc.Name as 'ComponentName',pc.ProgramID,pcm.ActivityID, uu.UserName+' '+uu.Surname as 'CreatorUserName', pcm.CreationTime,ac.Name as 'action',sub.Name as 'sub',pcc.TotalUnits,pcc.UnitCost,pcc.TotalUnitCost,pcc.CostEstimationYear
            //         from ProgramComponentsMapping pc join Program p on pc.ProgramID =p.Id
            //         left join ProgramComponentsActivitesMapping pcm on pc.ComponentID= pcm.ComponentID and pcm.ProgramID ='" + programId + "'  and pc.ProgramID='" + programId + "' "
            //         + "  left join Component cc on cc.Id= pc.ComponentID and pc.ProgramID='" + programId + "' "
            //        + "  left join AbpUsers uu on uu.Id= pcm.CreatorUserId and pcm.ProgramID='" + programId + "' "
            //         + "  left join ActionSAActivityMapping as asm on pcm.ActivityID=asm.ActivityID "
            //        + " left join ActionArea as ac on asm.ActionAreaID=ac.Id "
            //          + " left join SubActionArea  as sub on asm.SubActionAreaID=sub.Id "
            //          + " left join ProgramCostEstimation pcc on pcc.ActivityID = pcm.ActivityID "
            //        + "  where pc.ProgramID= '" + programId + "' and pcc.ProgramID ='" + programId + "' ) as three "
            //  + " LEFT JOIN (select a.Id as 'ActivityId', a.Name as 'ActivityName',a.Description from Activity a) as One ON three.ActivityID = One.ActivityId where TenantId= '" + TenantId1 + "'  ";

            string value;
            float totalTime = new float();
            var connectionString = _appConfiguration["ConnectionStrings:Default"];
            //using (SqlConnection conn = new SqlConnection(GetDbContext().Database.GetDbConnection().ConnectionString))
              using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(count, conn))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                    {
                        conn.Open();
                    }
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
            }

            if (value == "")
            {
                totalTime = 0;

            }
            else
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    ActivityActionandSubactionArea.Add(new ActivityActionandSubactionArea
                    {
                        ActivityID = (row["ActivityId1"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ActivityId1"]),
                        ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),
                        ComponentID = (row["Component"] == DBNull.Value) ? 00 : Convert.ToInt32(row["Component"]),
                        ActionAreaName = (row["action"] == DBNull.Value) ? "" : Convert.ToString(row["action"]),
                        SubActionAreaName = (row["sub"] == DBNull.Value) ? "" : Convert.ToString(row["sub"]),
                        ActionAreaID = (row["ActionAreaId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ActionAreaId"]),
                        SubActionAreaID = (row["SubActionAreaId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["SubActionAreaId"]),
                        subActionAreaCode = (row["SubactionCode"] == DBNull.Value) ? "" : Convert.ToString(row["SubactionCode"]),
                        prgActionAreaID = (row["prgActionAreaActivityMapId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["prgActionAreaActivityMapId"]),
                        prgActionAreaAcivityMapId = (row["prgActionAreaActivitymapactivitymapId"] == DBNull.Value) ? "" : Convert.ToString(row["prgActionAreaActivitymapactivitymapId"]),
                    });



                    PrgActionAreaActivityMappingRepositorysummary.Add(new PrgActionAreaActivityMappingRepositoryDto
                    {
                        ComponentID = (row["Component"] == DBNull.Value) ? 00 : Convert.ToInt32(row["Component"]),
                        ComponentName = Convert.ToString(row["ComponentName"]),
                        ProgrameStartDate = (row["ProgramStartDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["ProgramStartDate"]),
                        ProgrameEndDate = (row["PrgramEndDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["PrgramEndDate"]),
                        ProgramID = (row["ProgramID"] == DBNull.Value) ? 00 : Convert.ToInt32(row["ProgramID"]),
                        Activity = ActivityActionandSubactionArea


                    });

                }



            }
            return PrgActionAreaActivityMappingRepositorysummary;
        }
    }
}
