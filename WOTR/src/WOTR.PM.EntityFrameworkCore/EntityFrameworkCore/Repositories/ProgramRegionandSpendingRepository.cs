﻿using Abp.EntityFrameworkCore;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.ProgramRegionSpendings;
using WOTR.PM.ProgramRegionSpendings.Dto;

namespace WOTR.PM.EntityFrameworkCore.Repositories
{
   public class ProgramRegionandSpendingRepository : PMRepositoryBase<ProgramRegionSpending>, IProgramRegionSpendingRespository
    {
        private IDbContextProvider<PMDbContext> _dbContextProvider;

        public ProgramRegionandSpendingRepository(IDbContextProvider<PMDbContext> dbContextProvider)
        : base(dbContextProvider)

        {
            _dbContextProvider = dbContextProvider;
        }


        public List<ProgramRegionSpendingListDto> GetComponentandActivityForRegionandSpending(int? TenantId, int programId)
        {
            var dataTable = new DataTable();

            List<ProgramRegionSpendingListDto> ProgramRegionSpendingsummary = new List<ProgramRegionSpendingListDto>();

            List<ProgrameCostEstimationYearRegionSpendingDto> ProgrameCostEstimationYearRegionSpending = new List<ProgrameCostEstimationYearRegionSpendingDto>();

            List<ListActivityforProgamRegionSpendingDto> ListActivityforProgamRegionSpending = new List<ListActivityforProgamRegionSpendingDto>();


            string count = @"SELECT * FROM (select pc.TenantId, pc.ComponentID as'Component', cc.Name as 'ComponentName',pc.ProgramID ,pcm.ActivityID, pce.FunderContribution, pce.OtherContribution, pce.TotalUnits, pce.TotalUnitCost, pce.UnitCost, pce.UnitOfMeasuresID, pce.CostEstimationYear, pce.CommunityContribution,pce.ActivityLevel,uu.Name as 'UnitOfMeasuresName',p.ProgramStartDate,p.PrgramEndDate   from ProgramComponentsMapping pc
left join ProgramComponentsActivitesMapping pcm on pc.ComponentID= pcm.ComponentID and pcm.ProgramID='" + programId + "'  and pc.ProgramID='" + programId + "' and pcm.IsDeleted='false'"
+ " left join Program as p on pcm.ProgramID=p.Id"
+ " left join Component cc on cc.Id= pc.ComponentID and pc.ProgramID= '" + programId + "'"
+ " left join ProgramCostEstimation as pce on pcm.ActivityID = pce.ActivityID and pcm.ComponentID =pce.ComponentID and pce.ProgramID='" + programId + "'"
+ " left join UnitOfMeasure uu on uu.Id= pce.UnitOfMeasuresID and pcm.ProgramID= '" + programId + "'  where pc.ProgramID= '" + programId + "' and pc.IsDeleted ='false' ) as three  "
+ " LEFT JOIN (select a.Id as 'ActivityId', a.Name as 'ActivityName',a.Description from Activity a) as One ON three.ActivityID = One.ActivityId where TenantId='" + TenantId + "' ";



            string value;
            float totalTime = new float();


            using (SqlConnection conn = new SqlConnection(GetDbContext().Database.GetDbConnection().ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(count, conn))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
            }

            if (value == "")
            {
                totalTime = 0;

            }
            else
            {
                foreach (DataRow row in dataTable.Rows)
                {

                    ListActivityforProgamRegionSpending.Add(new ListActivityforProgamRegionSpendingDto
                    {
                        ActivityID = (row["ActivityID"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["ActivityID"]),
                        ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),
                        CostEstimationYear=(row["CostEstimationYear"] == DBNull.Value)?"":Convert.ToString(row["CostEstimationYear"]),
                         UnitOfMeasuresID = (row["UnitOfMeasuresID"] == DBNull.Value) ? 0000 : Convert.ToInt16(row["UnitOfMeasuresID"]),
                        TotalUnits = (row["TotalUnits"] == DBNull.Value) ? 00000 : Convert.ToInt64 (row["TotalUnits"]),
                        UnitCost = (row["UnitCost"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["UnitCost"]),
                        TotalUnitCost = (row["TotalUnitCost"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["TotalUnitCost"]),
                        CommunityContribution = (row["CommunityContribution"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["CommunityContribution"]),
                        Grant = (row["FunderContribution"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["FunderContribution"]),
                        OtherContribution = (row["OtherContribution"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["OtherContribution"]),
                        ComponentID = (row["Component"] == DBNull.Value) ? 00000 : Convert.ToInt32(row["Component"]),
                        UnitOfMeasuresName=(row["UnitOfMeasuresName"] == DBNull.Value) ?"":Convert.ToString(row["UnitOfMeasuresName"]),
                        ActivityLevel = (ActivityLevel)Enum.Parse(typeof(ActivityLevel), (row["ActivityLevel"].ToString() == "") ? "1" : row["ActivityLevel"].ToString()),
                       
                    });

                    ProgrameCostEstimationYearRegionSpending.Add(new ProgrameCostEstimationYearRegionSpendingDto
                    {
                        Activity = ListActivityforProgamRegionSpending,
                    });

                    ProgramRegionSpendingsummary.Add(new ProgramRegionSpendingListDto
                    {
                        ComponentID = (row["Component"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["Component"]),
                        ComponentName = (row["ComponentName"] == DBNull.Value) ? "" : Convert.ToString(row["ComponentName"]),
                        ProgrameStartDate = (row["ProgramStartDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["ProgramStartDate"]),
                        ProgrameEndDate = (row["PrgramEndDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["PrgramEndDate"]),
                        ProgramID=(row["ProgramID"] == DBNull.Value)?0000:Convert.ToInt32(row["ProgramID"]),
                        CostEstimationYear = ProgrameCostEstimationYearRegionSpending,
                    });


                }
            }
            return ProgramRegionSpendingsummary;
        }
    }
}
