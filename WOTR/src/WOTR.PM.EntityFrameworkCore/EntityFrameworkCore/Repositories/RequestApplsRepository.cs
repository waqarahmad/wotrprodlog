﻿using Abp.EntityFrameworkCore;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.PrgRequestAAPlas;
using WOTR.PM.PrgRequestAAPlas.Dto;
using WOTR.PM.ProgramRegionSpendings;

namespace WOTR.PM.EntityFrameworkCore.Repositories
{
    public class RequestApplsRepository : PMRepositoryBase<RequestAAplas>, IRequestApplsRepository
    {
        private IDbContextProvider<PMDbContext> _dbContextProvider;

        public RequestApplsRepository(IDbContextProvider<PMDbContext> dbContextProvider)
        : base(dbContextProvider)

        {
            _dbContextProvider = dbContextProvider;
        }

        public List<RequestAAPlasListDto> GetRquestApplData(int? TenantId, int programId)
        {
            var dataTable = new DataTable();
            List<RequestAAPlasListDto> RequestApplsummary = new List<RequestAAPlasListDto>();

            List<ListActionSubactionListDto> ListActionSubactionListDtoForList = new List<ListActionSubactionListDto>();

            List<ProgrameCostEstimationYearRequestApplDto> ProgrameCostEstimationYearRequestApplDto = new List<ProgrameCostEstimationYearRequestApplDto>();

            string count = @"select p.ProgramStartDate,p.Name,p.PrgramEndDate,p.Id as ProgrameId,l.Name as LocationName,l.Id as locationId,ac.Name as ActionAreaName,ac.Id as ActionId,sa.Name as SubActionAreaName,act.Name as ActivityName,pa.Id as prActivityMappingId,pr.CostEstimationYear,pr.UnitOfMeasuresID,pr.UnitCost,pr.TotalUnits,pr.ActivityID,
                             pr.TotalUnitCost,pr.OtherContribution,pr.CommunityContribution,pr.FunderContribution
                             ,ra.Quarter1FinancialRs,ra.Quarter1PhysicalUnits,ra.RequestStatus,ra.Id as requestAaplaId,ra.VillageID,
							 ra.Quarter2FinancialRs,ra.ManagerID,ra.Quarter2PhysicalUnits,ra.Quarter3FinancialRs,ra.Quarter3PhysicalUnits,ra.Quarter4FinancialRs,ra.Quarter4PhysicalUnits,pce.ActivityLevel,u.Name as UnitMeasurName from PrgActionAreaActivityMapping  pa
                             join ProgramRegionSpending pr
                             on pa.ActivityID =pr.ActivityID and pa.ComponentID =pr.ComponentID"
                            + " join ProgramCostEstimation pce on pce.ActivityID = pr.ActivityID and pce.ComponentID = pr.ComponentID and pce.ProgramID = pr.ProgramID and pce.CostEstimationYear =pr.CostEstimationYear"
                            + " left join RequestAAplas ra  on ra.PrgActionAreaActivityMappingID = pa.Id and ra.CostEstimationYear = pr.CostEstimationYear and ra.LocationID =pr.LocationID"
                             + " join Location l on pr.LocationID =l.Id"
                             + " join ActionArea ac on pa.ActionAreaID =ac.Id"
                             + " join SubActionArea sa on pa.SubActionAreaID =sa.Id"
                             + " join Activity act on act.Id =pa.ActivityID"
                             + " join Program p on p.Id = pr.ProgramID"
                             + " join UnitOfMeasure u on u.Id =pr.UnitOfMeasuresID"
                             + " where l.IsDeleted='"+ 0 +"' and   pa.ProgramID ='" + programId + "' and pr.ProgramID ='" + programId + "'  and ra.VillageID is null";



            string value;
            float totalTime = new float();


            using (SqlConnection conn = new SqlConnection(GetDbContext().Database.GetDbConnection().ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(count, conn))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
            }

            if (value == "")
            {
                totalTime = 0;

            }
            else
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    ListActionSubactionListDtoForList.Add(new ListActionSubactionListDto
                    {
                        Id = (row["requestAaplaId"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["requestAaplaId"]),
                        VillageID = (row["VillageID"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["VillageID"]),
                        ActionAreaName = (row["ActionAreaName"] == DBNull.Value) ? "" : Convert.ToString(row["ActionAreaName"]),
                        ActionId = (row["ActionId"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["ActionId"]),
                        SubActionAreaName = (row["SubActionAreaName"] == DBNull.Value) ? "" : Convert.ToString(row["SubActionAreaName"]),
                        ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),
                        PrgActionAreaActivityMappingID = (row["prActivityMappingId"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["prActivityMappingId"]),
                        UnitOfMeasuresName = (row["UnitMeasurName"] == DBNull.Value) ? "" : Convert.ToString(row["UnitMeasurName"]),
                        UnitOfMeasuresID = (row["UnitOfMeasuresID"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["UnitOfMeasuresID"]),
                        TotalUnits = (row["TotalUnits"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["TotalUnits"]),
                        UnitCost = (row["UnitCost"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["UnitCost"]),
                        TotalUnitCost = (row["TotalUnitCost"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["TotalUnitCost"]),
                        CommunityContribution = (row["CommunityContribution"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["CommunityContribution"]),
                        FunderContribution = (row["FunderContribution"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["FunderContribution"]),
                        OtherContribution = (row["OtherContribution"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["OtherContribution"]),
                        LocationID = (row["locationId"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["locationId"]),
                        CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                        Quarter1FinancialRs = (row["Quarter1FinancialRs"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["Quarter1FinancialRs"]),
                        Quarter1PhysicalUnits = (row["Quarter1PhysicalUnits"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["Quarter1PhysicalUnits"]),
                        ActivityLevel = (ActivityLevel)Enum.Parse(typeof(ActivityLevel), (row["ActivityLevel"].ToString() == "") ? "1" : row["ActivityLevel"].ToString()),
                        Quarter2FinancialRs = (row["Quarter2FinancialRs"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["Quarter2FinancialRs"]),
                        Quarter2PhysicalUnits = (row["Quarter2PhysicalUnits"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["Quarter2PhysicalUnits"]),

                        Quarter3FinancialRs = (row["Quarter3FinancialRs"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["Quarter3FinancialRs"]),
                        Quarter3PhysicalUnits = (row["Quarter3PhysicalUnits"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["Quarter3PhysicalUnits"]),

                        Quarter4FinancialRs = (row["Quarter1FinancialRs"] == DBNull.Value) ? 00000 : Convert.ToInt64(row["Quarter4FinancialRs"]),
                        Quarter4PhysicalUnits = (row["Quarter1PhysicalUnits"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["Quarter4PhysicalUnits"]),
                    });

                    ProgrameCostEstimationYearRequestApplDto.Add(new ProgrameCostEstimationYearRequestApplDto
                    {
                        ActionSubactionCost = ListActionSubactionListDtoForList,
                    });

                    //if(row["RequestStatus"].ToString() == "")
                    //{
                    //     var x =RequestStatus.Approved;
                    //}
                    RequestApplsummary.Add(new RequestAAPlasListDto
                    {
                        Year = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                        ActivityLevel = (ActivityLevel)Enum.Parse(typeof(ActivityLevel), (row["ActivityLevel"].ToString() == "") ? "1" : row["ActivityLevel"].ToString()),
                        RequestStatus = (RequestStatus)Enum.Parse(typeof(RequestStatus), (row["RequestStatus"].ToString() == "") ? "1" : row["RequestStatus"].ToString()),
                        ManagerID = (row["ManagerID"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["ManagerID"]),
                        ProgramID = (row["ProgrameId"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["ProgrameId"]),
                        LocationID = (row["locationId"] == DBNull.Value) ? 0000 : Convert.ToInt32(row["locationId"]),
                        LocationName = (row["LocationName"] == DBNull.Value) ? "" : Convert.ToString(row["LocationName"]),
                        ProgrameStartDate = (row["ProgramStartDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["ProgramStartDate"]),
                        ProgrameEndDate = (row["PrgramEndDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["PrgramEndDate"]),
                        ProgramName = (row["Name"] == DBNull.Value) ? "" : Convert.ToString(row["Name"]),
                        CostEstimationYear = ProgrameCostEstimationYearRequestApplDto,

                    });

                }



            }
            return RequestApplsummary;
        }
    }
}

