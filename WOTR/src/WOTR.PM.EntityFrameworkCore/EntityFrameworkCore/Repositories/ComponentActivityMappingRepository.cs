﻿using Abp.EntityFrameworkCore;
using Abp.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WOTR.PM.Components;
using WOTR.PM.Configuration;
using WOTR.PM.Identity;
using WOTR.PM.PrgImplementationPlans;
using WOTR.PM.ProgramRegionSpendings;


namespace WOTR.PM.EntityFrameworkCore.Repositories
{
    public class ComponentActivityMappingRepository : PMRepositoryBase<ComponentActivityMapping>, IComponentActivityMappingRepository
    {
        private IDbContextProvider<PMDbContext> _dbContextProvider;
        private readonly IConfigurationRoot _appConfiguration;


        public ComponentActivityMappingRepository(IDbContextProvider<PMDbContext> dbContextProvider,
            IHostingEnvironment env)
        : base(dbContextProvider)

        {
            _dbContextProvider = dbContextProvider;
            _appConfiguration = env.GetAppConfiguration();
        }

        Demo AllProgramNamesList = new Demo();
        ComponentAndactivityMappingForList CA = new ComponentAndactivityMappingForList();
        public List<GetComponentActivityMappingListDto> GetComponentActivityMappingDetils(int? TenantId, long? userId)
        {
            var dataTable = new DataTable();
            List<GetComponentActivityMappingListDto> componentActivityMappingsummary = new List<GetComponentActivityMappingListDto>();

            string count = @"SELECT    One.TenantId, One.Id as 'ComponentTableId', One.[ C_Id] as 'C_Id' ,  One.Name as 'Component Name', One.Code as 'Component Code', One.ComponentCreationTime, One.ComponentID as 'Component Mapping Id',Activity.Name as 'Activity Name', Activity.Id as 'Activity Id', One.MappingCreationTime , One.CreatorUserId, One.FullName, Activity.Description
FROM ( SELECT Component.IsDeleted,Component.TenantId, Component.Id, Component.Name, Component.Code,ComponentActivityMapping.ComponentID,ComponentActivityMapping.Id as ' C_Id',ComponentActivityMapping.IsDeleted as 'ComponentActivityMapisdeleted', ComponentActivityMapping.ActivityID, Component.CreationTime as 'ComponentCreationTime', ComponentActivityMapping.CreationTime as 'MappingCreationTime', ComponentActivityMapping.CreatorUserId, AbpUsers.Name + ' ' + AbpUsers.Surname as 'FullName'
FROM Component LEFT JOIN ComponentActivityMapping ON Component.Id= ComponentActivityMapping.ComponentID and ComponentActivityMapping.IsDeleted ='false' LEFT JOIN AbpUsers ON ComponentActivityMapping.CreatorUserId= AbpUsers.Id) as One LEFT JOIN (select Activity.Name, Activity.Id, Activity.Description as 'Description' from Activity) as Activity ON One.ActivityID=Activity.Id  where one.TenantId='" + TenantId + "' and one.IsDeleted='false'";
            string value;
            float totalTime = new float();


            using (SqlConnection conn = new SqlConnection("Server=tcp:wotrstaging.database.windows.net,1433;Initial Catalog=wotrstaingdb;User id=wotrstagingadmin@wotrstaging.database.windows.net;Password=Welcome1$12345;Persist Security Info=true"))
            {

                using (SqlCommand cmd = new SqlCommand(count, conn))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
            }

            if (value == "")
            {
                totalTime = 0;

            }
            else
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    componentActivityMappingsummary.Add(new GetComponentActivityMappingListDto
                    {
                        Id = (row["C_Id"] == DBNull.Value) ? 000 : Convert.ToInt32(row["C_Id"]),
                        ComponetTableId = Convert.ToInt32(row["ComponentTableId"]),
                        componentName = Convert.ToString(row["Component Name"]),
                        ComponentCreatime = Convert.ToDateTime(row["ComponentCreationTime"]),
                        ComponentCode = (row["Component Code"] == DBNull.Value) ? "" : Convert.ToString(row["Component Code"]),
                        ComponentMappingID = (row["Component Mapping Id"] == DBNull.Value) ? "" : Convert.ToString(row["Component Mapping Id"]),



                        ActivityDescri = (row["Description"] == DBNull.Value) ? "" : Convert.ToString(row["Description"]),

                        ActivityName = (row["Activity Name"] == DBNull.Value) ? "" : Convert.ToString(row["Activity Name"]),

                        ActivityID = (row["Activity Id"] == DBNull.Value) ? 000 : Convert.ToInt32(row["Activity Id"]),

                        Description = (row["Description"] == DBNull.Value) ? "" : Convert.ToString(row["Description"]),

                        ComponentActivityMappingCreationTime = (row["MappingCreationTime"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["MappingCreationTime"]),

                        FullName = (row["FullName"] == DBNull.Value) ? "" : Convert.ToString(row["FullName"]),

                        //TenantId=Convert.ToInt32(row["TenantId"]),



                    });

                }



            }
            return componentActivityMappingsummary;
        }

        public List<GetComponentActivityMappingListDto> GetComponentandActivityForProgram(int? TenantId, long? UserID, int programId)
        {
            var dataTable = new DataTable();
            List<GetComponentActivityMappingListDto> ProgramComponentActivitysummary = new List<GetComponentActivityMappingListDto>();

            string count = @"SELECT * FROM (select pc.TenantId, pc.ComponentID as'Component',cc.Code , cc.Name as 'ComponentName',pc.ProgramID ,p.ProgramStartDate, p.PrgramEndDate,pcm.ActivityID, uu.UserName  as 'CreatorUserName', pcm.CreationTime  from ProgramComponentsMapping pc
left join ProgramComponentsActivitesMapping pcm on pc.ComponentID= pcm.ComponentID and pcm.ProgramID='" + programId + "'  and pc.ProgramID='" + programId + "' and pcm.IsDeleted ='false'"
+ " left join Program as p on pcm.ProgramID=p.Id"
 + " left join Component cc on cc.Id= pc.ComponentID and pc.ProgramID= '" + programId + "'"
 + " left join AbpUsers uu on uu.Id= pcm.CreatorUserId and pcm.ProgramID= '" + programId + "'  where pc.ProgramID= '" + programId + "' and pc.IsDeleted ='false') as three  "
 + " LEFT JOIN (select a.Id as 'ActivityId', a.Name as 'ActivityName',a.Description from Activity a) as One ON three.ActivityID = One.ActivityId where TenantId='" + TenantId + "'";

            //            @"SELECT * FROM (select pc.TenantId, pc.ComponentID as'Component', cc.Code , cc.Name as 'ComponentName',pc.ProgramID,pcm.ActivityID, uu.UserName+' '+uu.Surname as 'CreatorUserName', pcm.CreationTime  from ProgramComponentsMapping pc
            //left join ProgramComponentsActivitesMapping pcm on pc.ComponentID= pcm.ComponentID left join Component cc on cc.Id= pc.ComponentID left join AbpUsers uu on uu.Id= pcm.CreatorUserId) as three 
            //LEFT JOIN (select a.Id as 'ActivityId', a.Name as 'ActivityName',a.Description from Activity a) as One ON three.ActivityID = One.ActivityId where TenantId='1' ";

            string value;
            float totalTime = new float();


            using (SqlConnection conn = new SqlConnection("Server=tcp:wotrstaging.database.windows.net,1433;Initial Catalog=wotrstaingdb;User id=wotrstagingadmin@wotrstaging.database.windows.net;Password=Welcome1$12345;Persist Security Info=true"))
            {
                using (SqlCommand cmd = new SqlCommand(count, conn))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
            }

            if (value == "")
            {
                totalTime = 0;

            }
            else
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    ProgramComponentActivitysummary.Add(new GetComponentActivityMappingListDto
                    {

                        ComponetTableId = Convert.ToInt32(row["Component"]),
                        ComponentCode = Convert.ToString(row["Code"]),
                        componentName = Convert.ToString(row["ComponentName"]),
                        ActivityID = (row["ActivityID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ActivityID"]),
                        FullName = (row["CreatorUserName"] == DBNull.Value) ? "" : Convert.ToString(row["CreatorUserName"]),
                        ComponentActivityMappingCreationTime = (row["CreationTime"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["CreationTime"]),
                        ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),
                        ActivityDescri = (row["Description"] == DBNull.Value) ? "" : Convert.ToString(row["Description"]),
                        ComponentCreatime = (row["CreationTime"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["CreationTime"]),
                        //ProgrameStartDate = (row["ProgramStartDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["ProgramStartDate"]),
                        //ProgrameEndDate = (row["PrgramEndDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["PrgramEndDate"])
                    });

                }



            }
            return ProgramComponentActivitysummary;
        }

        public List<ProgrameComponentListDto> GetComponentandActivityForCostEstimation(int? TenantId, long? UserID, int programId)
        {
            var dataTable = new DataTable();
            var ProgramComponentActivitysummary = new List<ProgrameComponentListDto>();

            var ComponentAndactivityMappingForList = new List<ComponentAndactivityMappingForList>();

            var ProgrameCostEstimationYearDto = new List<ProgrameCostEstimationYearDto>();

            string count = @"select pcam.TenantId, pcam.ComponentID as 'Component', c.Code, c.Name as 'ComponentName', pcam.ProgramID, 
                             p.ProgramStartDate, p.PrgramEndDate, pcam.ActivityID ,oneTable.CostEstimationYear, uu.UserName as 'CreatorUserName', pcam.CreationTime,pctc1.Id as 'subTotalCostId',pctc1.SubTotalofTotalCost,pctc1.SubTotalTotalunits,pctc.Id as 'TotalCostId',pctc.TotalCost as 'OverallTotalCost', pctc.TotalUnits as 'OverallTotalUnit', 
                             oneTable.Id as 'ProgramCostEstimationId',oneTable.TotalUnits, oneTable.OtherContribution, oneTable.TotalUnitCost,oneTable.UnitCost,(oneTable.TotalUnits+oneTable.TotalUnitCost+oneTable.UnitCost)As 'Total',  oneTable.FunderContribution, oneTable.CommunityContribution,
                             oneTable.UnitOfMeasuresID,oneTable.ActivityLevel, a.Name as 'ActivityName',a.Description from ProgramComponentsActivitesMapping pcam
                             left join Activity a on a.Id =pcam.ActivityID left join Component c on c.Id = pcam.ComponentID left join Program p on p.Id = pcam.ProgramID
                             left join ProgramCETotalCost pctc on pctc.ProgramID = pcam.ProgramID left join AbpUsers uu on uu.Id= pcam.CreatorUserId 
                             left join (SELECT Id, ActivityID as 'aid', CommunityContribution, ComponentID, CreationTime, CreatorUserId,FunderContribution,OtherContribution, ProgramID, TenantId, TotalUnitCost, TotalUnits, UnitCost, UnitOfMeasuresID, CostEstimationYear, ActivityLevel FROM ProgramCostEstimation where ProgramCostEstimation.ProgramID ='" + programId + "' "
                              + " union"
                            + " SELECT Id, ActivityID as 'aid',CommunityContribution, ComponentID, CreationTime, CreatorUserId,FunderContribution,OtherContribution, ProgramID, TenantId, TotalUnitCost, TotalUnits, UnitCost, UnitOfMeasuresID, CostEstimationYear, ActivityLevel FROM ProgramCostEstimationOverall where ProgramCostEstimationOverall.ProgramID ='" + programId + "' ) "
                             + " oneTable on oneTable.ComponentID = pcam.ComponentID and oneTable.aid = pcam.ActivityID  "
                            + " left join programCESubTotalCost pctc1 on pctc1.ProgramID = pcam.ProgramID  and pctc1.ComponentID =pcam.ComponentID and pctc1.CostEstimationYear =oneTable.CostEstimationYear where pcam.ProgramID='" + programId + "' and pcam.IsDeleted ='false' ";


            string value;
            var totalTime = new float();


            using (SqlConnection conn = new SqlConnection(GetDbContext().Database.GetDbConnection().ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(count, conn))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
            }

            if (value == "")
            {
                totalTime = 0;

            }
            else
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    ComponentAndactivityMappingForList.Add(new ComponentAndactivityMappingForList
                    {
                        Id = (row["ProgramCostEstimationId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ProgramCostEstimationId"]),
                        FullName = (row["CreatorUserName"] == DBNull.Value) ? "" : Convert.ToString(row["CreatorUserName"]),
                        ComponetTableId = Convert.ToInt32(row["Component"]),
                        ActivityLevel = (ActivityLevel)Enum.Parse(typeof(ActivityLevel), (row["ActivityLevel"].ToString() == "") ? "1" : row["ActivityLevel"].ToString()),
                        ProgramCostEstimate_TotalUnits = (row["TotalUnits"] == DBNull.Value) ? 000 : Convert.ToDecimal(row["TotalUnits"]),
                        ProgramCostEstimate_TotalUnitCost = (row["TotalUnitCost"] == DBNull.Value) ? 000 : Convert.ToDecimal(row["TotalUnitCost"]),
                        ProgramCostEstimate_UnitCost = (row["UnitCost"] == DBNull.Value) ? 000 : Convert.ToDecimal(row["UnitCost"]),
                        ProgramCostEstimate_FunderContribution = (row["FunderContribution"] == DBNull.Value) ? 000 : Convert.ToInt64(row["FunderContribution"]),
                        ProgramCostEstimate_CommunityContribution = (row["CommunityContribution"] == DBNull.Value) ? 000 : Convert.ToInt64(row["CommunityContribution"]),
                        ProgramCostEstimate_OtherContribution = (row["OtherContribution"] == DBNull.Value) ? 000 : Convert.ToInt64(row["OtherContribution"]),
                        ProgramCostEstimate_UnitOfMeasuresID = (row["UnitOfMeasuresID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["UnitOfMeasuresID"]),
                        ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),
                        ActivityDescri = (row["Description"] == DBNull.Value) ? "" : Convert.ToString(row["Description"]),
                        ActivityID = (row["ActivityID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ActivityID"]),
                        ProgramCostEstimate_ProgramID = (row["ProgramID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ProgramID"]),
                        CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                        ProgramCostEstimate_Total = (row["Total"] == DBNull.Value) ? 000 : Convert.ToInt64(row["Total"]),
                    });

                    ProgrameCostEstimationYearDto.Add(new ProgrameCostEstimationYearDto
                    {
                        SubTotalofTotalCost = (row["SubTotalofTotalCost"] == DBNull.Value) ? 000 : Convert.ToInt64(row["SubTotalofTotalCost"]),
                        SubTotalTotalunits = (row["SubTotalTotalunits"] == DBNull.Value) ? 000 : Convert.ToInt64(row["SubTotalTotalunits"]),
                        Id = (row["subTotalCostId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["subTotalCostId"]),
                        CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                        ComponetTableId = Convert.ToInt32(row["Component"]),
                        Activity = ComponentAndactivityMappingForList,
                    });

                    ProgramComponentActivitysummary.Add(new ProgrameComponentListDto
                    {
                        Id = (row["TotalCostId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["TotalCostId"]),
                        ComponetTableId = Convert.ToInt32(row["Component"]),
                        ComponentCode = Convert.ToString(row["Code"]),
                        componentName = Convert.ToString(row["ComponentName"]),
                        ComponentCreatime = (row["CreationTime"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["CreationTime"]),
                        ProgrameId = (row["ProgramID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ProgramID"]),
                        ProgrameStartDate = (row["ProgramStartDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["ProgramStartDate"]),
                        ProgrameEndDate = (row["PrgramEndDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["PrgramEndDate"]),
                        CostEstimationYear = ProgrameCostEstimationYearDto,
                        TotalCost = (row["OverallTotalCost"] == DBNull.Value) ? 000 : Convert.ToInt64(row["OverallTotalCost"]),
                        TotalUnits = (row["OverallTotalUnit"] == DBNull.Value) ? 000 : Convert.ToInt64(row["OverallTotalUnit"]),
                    });

                }



            }
            return ProgramComponentActivitysummary;
        }
        public List<ProgrameComponentListDto> GetOverallRegionSpendingdetailsCostEstimation(int? TenantId, long? UserID, int programId, string year)
        {
            var dataTable = new DataTable();
            var ProgramComponentActivitysummary = new List<ProgrameComponentListDto>();

            var ComponentAndactivityMappingForList = new List<ComponentAndactivityMappingForList>();

            var ProgrameCostEstimationYearDto = new List<ProgrameCostEstimationYearDto>();

            string count = @"select pcam.TenantId, pcam.ComponentID as 'Component', c.Code, c.Name as 'ComponentName', pcam.ProgramID, 
                             p.ProgramStartDate, p.PrgramEndDate, pcam.ActivityID ,oneTable.CostEstimationYear, uu.UserName as 'CreatorUserName', pcam.CreationTime,pctc1.Id as 'subTotalCostId',pctc1.SubTotalofTotalCost,pctc1.SubTotalTotalunits,pctc.Id as 'TotalCostId',pctc.TotalCost as 'OverallTotalCost', pctc.TotalUnits as 'OverallTotalUnit', 
                             oneTable.Id as 'ProgramCostEstimationId',oneTable.TotalUnits, oneTable.OtherContribution, oneTable.TotalUnitCost,oneTable.UnitCost,(oneTable.TotalUnits+oneTable.TotalUnitCost+oneTable.UnitCost)As 'Total',  oneTable.FunderContribution, oneTable.CommunityContribution,
                             oneTable.UnitOfMeasuresID,oneTable.ActivityLevel, a.Name as 'ActivityName',a.Description from ProgramComponentsActivitesMapping pcam
                             left join Activity a on a.Id =pcam.ActivityID left join Component c on c.Id = pcam.ComponentID left join Program p on p.Id = pcam.ProgramID
                             left join ProgramCETotalCost pctc on pctc.ProgramID = pcam.ProgramID left join AbpUsers uu on uu.Id= pcam.CreatorUserId 
                             left join (SELECT Id, ActivityID as 'aid', CommunityContribution, ComponentID, CreationTime, CreatorUserId,FunderContribution,OtherContribution, ProgramID, TenantId, TotalUnitCost, TotalUnits, UnitCost, UnitOfMeasuresID, CostEstimationYear, ActivityLevel FROM ProgramCostEstimation where ProgramCostEstimation.ProgramID ='" + programId + "' "
                              + " union"
                            + " SELECT Id, ActivityID as 'aid',CommunityContribution, ComponentID, CreationTime, CreatorUserId,FunderContribution,OtherContribution, ProgramID, TenantId, TotalUnitCost, TotalUnits, UnitCost, UnitOfMeasuresID, CostEstimationYear, ActivityLevel FROM ProgramCostEstimationOverall where ProgramCostEstimationOverall.ProgramID ='" + programId + "' ) "
                             + " oneTable on oneTable.ComponentID = pcam.ComponentID and oneTable.aid = pcam.ActivityID  "
                            + " left join programCESubTotalCost pctc1 on pctc1.ProgramID = pcam.ProgramID  and pctc1.ComponentID =pcam.ComponentID and pctc1.CostEstimationYear =oneTable.CostEstimationYear where oneTable.CostEstimationYear='" + year + "' and pcam.ProgramID='" + programId + "' and pcam.IsDeleted ='false'  Order by c.Name,oneTable.ActivityLevel ";


            string value;
            var totalTime = new float();


            using (SqlConnection conn = new SqlConnection(GetDbContext().Database.GetDbConnection().ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(count, conn))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
            }

            if (value == "")
            {
                totalTime = 0;

            }
            else
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    ComponentAndactivityMappingForList.Add(new ComponentAndactivityMappingForList
                    {
                        Id = (row["ProgramCostEstimationId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ProgramCostEstimationId"]),
                        FullName = (row["CreatorUserName"] == DBNull.Value) ? "" : Convert.ToString(row["CreatorUserName"]),
                        ComponetTableId = Convert.ToInt32(row["Component"]),
                        ActivityLevel = (ActivityLevel)Enum.Parse(typeof(ActivityLevel), (row["ActivityLevel"].ToString() == "") ? "1" : row["ActivityLevel"].ToString()),
                        ProgramCostEstimate_TotalUnits = (row["TotalUnits"] == DBNull.Value) ? 000 : Convert.ToInt64(row["TotalUnits"]),
                        ProgramCostEstimate_TotalUnitCost = (row["TotalUnitCost"] == DBNull.Value) ? 000 : Convert.ToInt64(row["TotalUnitCost"]),
                        ProgramCostEstimate_UnitCost = (row["UnitCost"] == DBNull.Value) ? 000 : Convert.ToInt64(row["UnitCost"]),
                        ProgramCostEstimate_FunderContribution = (row["FunderContribution"] == DBNull.Value) ? 000 : Convert.ToInt64(row["FunderContribution"]),
                        ProgramCostEstimate_CommunityContribution = (row["CommunityContribution"] == DBNull.Value) ? 000 : Convert.ToInt64(row["CommunityContribution"]),
                        ProgramCostEstimate_OtherContribution = (row["OtherContribution"] == DBNull.Value) ? 000 : Convert.ToInt64(row["OtherContribution"]),
                        ProgramCostEstimate_UnitOfMeasuresID = (row["UnitOfMeasuresID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["UnitOfMeasuresID"]),
                        ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),
                        ActivityDescri = (row["Description"] == DBNull.Value) ? "" : Convert.ToString(row["Description"]),
                        ActivityID = (row["ActivityID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ActivityID"]),
                        ProgramCostEstimate_ProgramID = (row["ProgramID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ProgramID"]),
                        CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                        ProgramCostEstimate_Total = (row["Total"] == DBNull.Value) ? 000 : Convert.ToInt64(row["Total"]),
                    });

                    ProgrameCostEstimationYearDto.Add(new ProgrameCostEstimationYearDto
                    {
                        SubTotalofTotalCost = (row["SubTotalofTotalCost"] == DBNull.Value) ? 000 : Convert.ToInt64(row["SubTotalofTotalCost"]),
                        SubTotalTotalunits = (row["SubTotalTotalunits"] == DBNull.Value) ? 000 : Convert.ToInt64(row["SubTotalTotalunits"]),
                        Id = (row["subTotalCostId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["subTotalCostId"]),
                        CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                        ComponetTableId = Convert.ToInt32(row["Component"]),
                        Activity = ComponentAndactivityMappingForList,
                    });

                    ProgramComponentActivitysummary.Add(new ProgrameComponentListDto
                    {
                        Id = (row["TotalCostId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["TotalCostId"]),
                        ComponetTableId = Convert.ToInt32(row["Component"]),
                        ComponentCode = Convert.ToString(row["Code"]),
                        componentName = Convert.ToString(row["ComponentName"]),
                        ComponentCreatime = (row["CreationTime"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["CreationTime"]),
                        ProgrameId = (row["ProgramID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ProgramID"]),
                        ProgrameStartDate = (row["ProgramStartDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["ProgramStartDate"]),
                        ProgrameEndDate = (row["PrgramEndDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["PrgramEndDate"]),
                        CostEstimationYear = ProgrameCostEstimationYearDto,
                        TotalCost = (row["OverallTotalCost"] == DBNull.Value) ? 000 : Convert.ToInt64(row["OverallTotalCost"]),
                        TotalUnits = (row["OverallTotalUnit"] == DBNull.Value) ? 000 : Convert.ToInt64(row["OverallTotalUnit"]),
                    });

                }



            }
            return ProgramComponentActivitysummary;
        }


        public List<ProgrameComponentListDto> GetComponentandActivityForCostEstimationforallprograms(int? TenantId, long? UserID)
        {
            var dataTable = new DataTable();
            List<ProgrameComponentListDto> ProgramComponentActivitysummary = new List<ProgrameComponentListDto>();

            List<ComponentAndactivityMappingForList> ComponentAndactivityMappingForList = new List<ComponentAndactivityMappingForList>();

            List<ProgrameCostEstimationYearDto> ProgrameCostEstimationYearDto = new List<ProgrameCostEstimationYearDto>();

            string count = @"select pcam.TenantId, pcam.ComponentID as 'Component', c.Code, c.Name as 'ComponentName', pcam.ProgramID, 
                             p.ProgramStartDate, p.PrgramEndDate, pcam.ActivityID ,oneTable.CostEstimationYear, uu.UserName as 'CreatorUserName', pcam.CreationTime,pctc1.Id as 'subTotalCostId',pctc1.SubTotalofTotalCost,pctc1.SubTotalTotalunits,pctc.Id as 'TotalCostId',pctc.TotalCost as 'OverallTotalCost', pctc.TotalUnits as 'OverallTotalUnit', 
                             oneTable.Id as 'ProgramCostEstimationId',oneTable.TotalUnits, oneTable.OtherContribution, oneTable.TotalUnitCost,oneTable.UnitCost,oneTable.FunderContribution, oneTable.CommunityContribution,
                             oneTable.UnitOfMeasuresID,oneTable.ActivityLevel, a.Name as 'ActivityName',a.Description from ProgramComponentsActivitesMapping pcam
                             left join Activity a on a.Id =pcam.ActivityID left join Component c on c.Id = pcam.ComponentID left join Program p on p.Id = pcam.ProgramID
                             left join ProgramCETotalCost pctc on pctc.ProgramID = pcam.ProgramID left join AbpUsers uu on uu.Id= pcam.CreatorUserId 
                             left join (SELECT Id, ActivityID as 'aid', CommunityContribution, ComponentID, CreationTime, CreatorUserId,FunderContribution,OtherContribution, ProgramID, TenantId, TotalUnitCost, TotalUnits, UnitCost, UnitOfMeasuresID, CostEstimationYear, ActivityLevel FROM ProgramCostEstimation where ProgramCostEstimation.ProgramID =27 "
                              + " union"
                            + " SELECT Id, ActivityID as 'aid',CommunityContribution, ComponentID, CreationTime, CreatorUserId,FunderContribution,OtherContribution, ProgramID, TenantId, TotalUnitCost, TotalUnits, UnitCost, UnitOfMeasuresID, CostEstimationYear, ActivityLevel FROM ProgramCostEstimationOverall where ProgramCostEstimationOverall.ProgramID =27 ) "
                             + " oneTable on oneTable.ComponentID = pcam.ComponentID and oneTable.aid = pcam.ActivityID  "
                            + " left join programCESubTotalCost pctc1 on pctc1.ProgramID = pcam.ProgramID  and pctc1.ComponentID =pcam.ComponentID and pctc1.CostEstimationYear =oneTable.CostEstimationYear where  pcam.IsDeleted ='false' ";


            string value;
            float totalTime = new float();


            using (SqlConnection conn = new SqlConnection(GetDbContext().Database.GetDbConnection().ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(count, conn))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
            }

            if (value == "")
            {
                totalTime = 0;

            }
            else
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    ComponentAndactivityMappingForList.Add(new ComponentAndactivityMappingForList
                    {
                        Id = (row["ProgramCostEstimationId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ProgramCostEstimationId"]),
                        FullName = (row["CreatorUserName"] == DBNull.Value) ? "" : Convert.ToString(row["CreatorUserName"]),
                        ComponetTableId = Convert.ToInt32(row["Component"]),
                        ActivityLevel = (ActivityLevel)Enum.Parse(typeof(ActivityLevel), (row["ActivityLevel"].ToString() == "") ? "1" : row["ActivityLevel"].ToString()),
                        ProgramCostEstimate_TotalUnits = (row["TotalUnits"] == DBNull.Value) ? 000 : Convert.ToInt64(row["TotalUnits"]),
                        ProgramCostEstimate_TotalUnitCost = (row["TotalUnitCost"] == DBNull.Value) ? 000 : Convert.ToInt64(row["TotalUnitCost"]),
                        ProgramCostEstimate_UnitCost = (row["UnitCost"] == DBNull.Value) ? 000 : Convert.ToInt64(row["UnitCost"]),
                        ProgramCostEstimate_FunderContribution = (row["FunderContribution"] == DBNull.Value) ? 000 : Convert.ToInt64(row["FunderContribution"]),
                        ProgramCostEstimate_CommunityContribution = (row["CommunityContribution"] == DBNull.Value) ? 000 : Convert.ToInt64(row["CommunityContribution"]),
                        ProgramCostEstimate_OtherContribution = (row["OtherContribution"] == DBNull.Value) ? 000 : Convert.ToInt64(row["OtherContribution"]),
                        ProgramCostEstimate_UnitOfMeasuresID = (row["UnitOfMeasuresID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["UnitOfMeasuresID"]),
                        ActivityName = (row["ActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["ActivityName"]),
                        ActivityDescri = (row["Description"] == DBNull.Value) ? "" : Convert.ToString(row["Description"]),
                        ActivityID = (row["ActivityID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ActivityID"]),
                        ProgramCostEstimate_ProgramID = (row["ProgramID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ProgramID"]),
                        CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),


                    });

                    ProgrameCostEstimationYearDto.Add(new ProgrameCostEstimationYearDto
                    {
                        SubTotalofTotalCost = (row["SubTotalofTotalCost"] == DBNull.Value) ? 000 : Convert.ToInt64(row["SubTotalofTotalCost"]),
                        SubTotalTotalunits = (row["SubTotalTotalunits"] == DBNull.Value) ? 000 : Convert.ToInt64(row["SubTotalTotalunits"]),
                        Id = (row["subTotalCostId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["subTotalCostId"]),
                        CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                        ComponetTableId = Convert.ToInt32(row["Component"]),
                        Activity = ComponentAndactivityMappingForList,
                    });

                    ProgramComponentActivitysummary.Add(new ProgrameComponentListDto
                    {
                        Id = (row["TotalCostId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["TotalCostId"]),
                        ComponetTableId = Convert.ToInt32(row["Component"]),
                        ComponentCode = Convert.ToString(row["Code"]),
                        componentName = Convert.ToString(row["ComponentName"]),
                        ComponentCreatime = (row["CreationTime"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["CreationTime"]),
                        ProgrameId = (row["ProgramID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ProgramID"]),
                        ProgrameStartDate = (row["ProgramStartDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["ProgramStartDate"]),
                        ProgrameEndDate = (row["PrgramEndDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["PrgramEndDate"]),
                        CostEstimationYear = ProgrameCostEstimationYearDto,
                        TotalCost = (row["OverallTotalCost"] == DBNull.Value) ? 000 : Convert.ToInt64(row["OverallTotalCost"]),
                        TotalUnits = (row["OverallTotalUnit"] == DBNull.Value) ? 000 : Convert.ToInt64(row["OverallTotalUnit"]),
                    });

                }
            }
            return ProgramComponentActivitysummary;
        }

        public List<loginUserDetailsDto> GetLoginInfo(string mobilenumber, string name, int TenantId)
        {
            try
            {

                var dataTable = new DataTable();
                var loginUserDetailsDtosummary = new List<loginUserDetailsDto>();



                string count = @"select u.Name+ ' ' +u.Surname as fullName,u.id,u.EmailAddress,u.PhoneNumber,u.Address,u.LocationId,r.Name as rolName, l.Name as locationName,u.TenantId,u.ProfilePictureId,u.UserProfilePicBase64 from AbpUsers u 
                                join AbpUserRoles as ur on u.id=ur.userId 
                                join AbpRoles as r on r.id=ur.RoleId 
                                join Location as l  on u.LocationId = l.id
                                where (u.PhoneNumber='" + mobilenumber + "' and u.UserName='" + name + "' and u.TenantId='" + TenantId + "') ";


                string value;
                float totalTime = new float();

                using (SqlConnection conn = new SqlConnection("Server=tcp:wotrstaging.database.windows.net,1433;Initial Catalog=wotrstaingdb;User id=wotrstagingadmin@wotrstaging.database.windows.net;Password=Welcome1$12345;Persist Security Info=true"))
                {
                    using (SqlCommand cmd = new SqlCommand(count, conn))
                    {
                        if (conn.State == System.Data.ConnectionState.Closed)
                            conn.Open();
                        try
                        {
                            SqlDataReader dr = cmd.ExecuteReader();
                            dataTable.Load(dr);
                            if (dataTable.Rows.Count != 0)
                            {
                                value = dataTable.Rows[0][0].ToString();
                            }
                            else
                            {
                                return null;
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new UserFriendlyException("Error Db" + ex.Message);
                        }
                    }
                }

                if (value == "")
                {
                    totalTime = 0;

                }
                else
                {
                    foreach (DataRow row in dataTable.Rows)
                    {
                        loginUserDetailsDtosummary.Add(new loginUserDetailsDto
                        {
                            FullName = (row["fullName"] == DBNull.Value) ? "" : Convert.ToString(row["fullName"]),
                            PhoneNumber = (row["PhoneNumber"] == DBNull.Value) ? "" : Convert.ToString(row["PhoneNumber"]),
                            Address = (row["Address"] == DBNull.Value) ? "" : Convert.ToString(row["Address"]),
                            TenantId = (row["TenantId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["TenantId"]),
                            EmailAddress = (row["EmailAddress"] == DBNull.Value) ? "" : Convert.ToString(row["EmailAddress"]),
                            LocationName = (row["locationName"] == DBNull.Value) ? "" : Convert.ToString(row["locationName"]),
                            UserRole = (row["rolName"] == DBNull.Value) ? "" : Convert.ToString(row["rolName"]),
                            Id = (row["id"] == DBNull.Value) ? 000 : Convert.ToInt32(row["id"]),
                            ProfilePictureId = (row["ProfilePictureId"] == DBNull.Value) ? Guid.Empty : (Guid)row["ProfilePictureId"],
                            image = (row["UserProfilePicBase64"] == DBNull.Value) ? "" : Convert.ToString(row["UserProfilePicBase64"]),
                        });
                    }

                }
                return loginUserDetailsDtosummary;
            }
            catch (Exception ex2)
            {
                throw new UserFriendlyException("Error Db" + ex2.Message);
            }


        }

        public List<SubActivityForAndri> GetSubActivityForAndri(int userId, int TenantId, int programId, int PrgActionAreaActivityMappingID, string role)
        {
            var dataTable = new DataTable();
            var SubActivityForAndrisummary = new List<SubActivityForAndri>();
            string count;

            if (role == "RRcIncharge" || role == "ProjectManager" || role == "ProgramManager")
            {
                count = @"select c.Name as subActivityName,u.Name as uomName,i.ChecklistItemID,i.Units as unit,i.AchieveUnits,i.CheklistStartDate,i.CheklistEndDate,i.Description,i.id as impleId,i.status,i.CreatorUserId as ManagerId,i.ProgramID,i.CostEstimationYear,i.PrgQuarter,i.ProgramQuqterUnitMappingID,i.LastModificationTime,i.PrgActionAreaActivityMappingID ,a.Name+' '+a.Surname as AssingtoUser ,a.Name+' '+a.Surname as ManagerName from ImplementationPlanCheckList i 
                            join UnitOfMeasure u on i.UnitOfMeasuresID=u.id
                            join ChecklistItem c on i.ChecklistItemID=c.Id
                            join AbpUsers a on i.AssingUserId=a.Id
                            where(i.IsDeleted='false' and i.ProgramID=" + programId + " and i.TenantId=" + TenantId + "  and i.PrgActionAreaActivityMappingID=" + PrgActionAreaActivityMappingID + ")";

            }
            else
            {
                count = @"select c.Name as subActivityName,u.Name as uomName,i.ChecklistItemID,i.Units as unit,i.AchieveUnits,i.CheklistStartDate,i.CheklistEndDate,i.Description,i.id as impleId,i.status,i.CreatorUserId as ManagerId,i.ProgramID,i.CostEstimationYear,i.PrgQuarter,i.ProgramQuqterUnitMappingID,i.LastModificationTime,i.PrgActionAreaActivityMappingID,a.Name+' '+a.Surname as ManagerName ,a.Name+' '+a.Surname as AssingtoUser from ImplementationPlanCheckList i 
                            join UnitOfMeasure u on i.UnitOfMeasuresID=u.id
                            join ChecklistItem c on i.ChecklistItemID=c.Id
                            join AbpUsers a on i.CreatorUserId=a.Id
                            where( i.IsDeleted='false' and i.AssingUserId=" + userId + " and i.ProgramID=" + programId + " and i.TenantId=" + TenantId + "  and i.PrgActionAreaActivityMappingID=" + PrgActionAreaActivityMappingID + ")";

            }




            string value;
            float totalTime = new float();


            using (SqlConnection conn = new SqlConnection(GetDbContext().Database.GetDbConnection().ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(count, conn))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
            }

            if (value == "")
            {
                totalTime = 0;

            }
            else
            {
                try
                {
                    foreach (DataRow row in dataTable.Rows)
                    {
                        SubActivityForAndrisummary.Add(new SubActivityForAndri
                        {
                            SubActivityname = (row["subActivityName"] == DBNull.Value) ? "" : Convert.ToString(row["subActivityName"]),
                            Unitofmeasure = (row["uomName"] == DBNull.Value) ? "" : Convert.ToString(row["uomName"]),
                            Description = (row["Description"] == DBNull.Value) ? "" : Convert.ToString(row["Description"]),

                            startDate = (row["CheklistStartDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["CheklistStartDate"]),

                            EndDate = (row["CheklistEndDate"] == DBNull.Value) ? DateTime.Today : Convert.ToDateTime(row["CheklistEndDate"]),
                            LastModificationTime = (row["LastModificationTime"] == DBNull.Value) ? DateTime.Now : Convert.ToDateTime(row["LastModificationTime"]),
                            ManagerName = (row["ManagerName"] == DBNull.Value) ? "" : Convert.ToString(row["ManagerName"]),

                            status = (row["status"] == DBNull.Value) ? SubActivityStatus.NotStarted : (SubActivityStatus)Enum.Parse(typeof(SubActivityStatus), row["status"].ToString()),

                            Id = (row["impleId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["impleId"]),

                            ManagerId = (row["ManagerId"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ManagerId"]),

                            ProgramID = (row["ProgramID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ProgramID"]),
                            CostEstimationYear = (row["CostEstimationYear"] == DBNull.Value) ? "" : Convert.ToString(row["CostEstimationYear"]),
                            QuarterID = (row["PrgQuarter"] == DBNull.Value) ? 000 : Convert.ToInt32(row["PrgQuarter"]),
                            ProgramQuqterUnitMappingID = (row["ProgramQuqterUnitMappingID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ProgramQuqterUnitMappingID"]),
                            PrgActionAreaActivityMappingID = (row["PrgActionAreaActivityMappingID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["PrgActionAreaActivityMappingID"]),
                            unit = (row["unit"] == DBNull.Value) ? 000 : Convert.ToInt32(row["unit"]),
                            AchieveUnits = (row["AchieveUnits"] == DBNull.Value) ? 000 : Convert.ToDecimal(row["AchieveUnits"]),
                            AssingtoUser = (row["AssingtoUser"] == DBNull.Value) ? "" : Convert.ToString(row["AssingtoUser"]),

                            SubActivityId = (row["ChecklistItemID"] == DBNull.Value) ? 000 : Convert.ToInt32(row["ChecklistItemID"])
                        });


                    }
                }
                catch (Exception)
                {

                    throw;
                }


            }
            return SubActivityForAndrisummary;


        }

        public void UpdatesUnit()
        {

            using (SqlConnection conn = new SqlConnection(GetDbContext().Database.GetDbConnection().ConnectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                try
                {
                    GetDbContext().SaveChanges();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
        }



        public List<unitOFmeasuresDtoList> GetUnitofmeasures(int TenantId)
        {
            var dataTable = new DataTable();
            var UOMForAndrisummary = new List<unitOFmeasuresDtoList>();



            string count = @"select u.Name,u.Id from UnitOfMeasure u 
                            where( u.TenantId=" + TenantId + " )";



            string value;
            var totalTime = new float();


            using (SqlConnection conn = new SqlConnection(GetDbContext().Database.GetDbConnection().ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(count, conn))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        dataTable.Load(dr);
                        if (dataTable.Rows.Count != 0)
                        {
                            value = dataTable.Rows[0][0].ToString();
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
            }

            if (value == "")
            {
                totalTime = 0;

            }
            else
            {
                try
                {
                    foreach (DataRow row in dataTable.Rows)
                    {
                        UOMForAndrisummary.Add(new unitOFmeasuresDtoList
                        {
                            Name = (row["Name"] == DBNull.Value) ? "" : Convert.ToString(row["Name"]),



                            Id = (row["Id"] == DBNull.Value) ? 000 : Convert.ToInt32(row["Id"]),


                        });
                    }
                }
                catch (Exception)
                {

                    throw;
                }


            }
            return UOMForAndrisummary;
        }


        public Demo AAA(int? TenantId)
        {
            var dataTable = new DataTable();
            var result = new Demo();
            var ProgramList = new List<Demo>();
            var ComponentAndactivityMappingForList = new List<ComponentAndactivityMappingForList>();

            var count = @"select distinct p.Name as 'ProgramName'
							 from ProgramComponentsActivitesMapping pcam
                             left join Activity a on a.Id =pcam.ActivityID 
							 left join Component c on c.Id = pcam.ComponentID 
							 left join Program p on p.Id = pcam.ProgramID
                             left join ProgramCETotalCost pctc on pctc.ProgramID = pcam.ProgramID
							 left join AbpUsers uu on uu.Id= pcam.CreatorUserId 
							 left join ActionSAActivityMapping am on am.ActivityID=pcam.ActivityID
							 
                             left join (SELECT Id, ActivityID as 'aid', CommunityContribution, ComponentID, CreationTime, CreatorUserId,FunderContribution,OtherContribution, ProgramID, TenantId, TotalUnitCost, TotalUnits, UnitCost, UnitOfMeasuresID, CostEstimationYear, ActivityLevel FROM ProgramCostEstimation  union SELECT Id, ActivityID as 'aid',CommunityContribution, ComponentID, CreationTime, CreatorUserId,FunderContribution,OtherContribution, ProgramID, TenantId, TotalUnitCost, TotalUnits, UnitCost, UnitOfMeasuresID, CostEstimationYear, ActivityLevel FROM ProgramCostEstimationOverall  )  oneTable on oneTable.ComponentID = pcam.ComponentID and oneTable.aid = pcam.ActivityID 
							 left join(select Distinct Location.Name as 'Region',VillageCluster.ProgramID , VillageCluster.RRCID from Location join VillageCluster on Location.Id = VillageCluster.RRCID where  VillageCluster.RRCID IN(select VillageCluster.RRCID from VillageCluster join ProgramComponentsActivitesMapping on VillageCluster.ProgramID= ProgramComponentsActivitesMapping.ProgramID) )Regiontable on Regiontable.ProgramID = pcam.ProgramID   
							 left join(select  ActionArea.Name as 'ActionArea', ActionSAActivityMapping.ActivityID  from ActionArea join ActionSAActivityMapping on ActionArea.id=ActionSAActivityMapping.ActionAreaID)actiontable on actiontable.ActivityID = pcam.ActivityID 
							 left join(select SubActionArea.Name as 'SubAction', ActionSAActivityMapping.ActivityID  from SubActionArea join ActionSAActivityMapping on SubActionArea.id=ActionSAActivityMapping.SubActionAreaID)subactiontable on subactiontable.ActivityID = pcam.ActivityID 
							 left join programCESubTotalCost pctc1 on pctc1.ProgramID = pcam.ProgramID  and pctc1.ComponentID =pcam.ComponentID and pctc1.CostEstimationYear =oneTable.CostEstimationYear where  pcam.IsDeleted ='false' and pctc1.CostEstimationYear='OverAll'";



            string value;

            using (SqlConnection conn = new SqlConnection(GetDbContext().Database.GetDbConnection().ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(count, conn))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    try
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        dataTable.Load(dr);

                        foreach (DataRow row in dataTable.Rows)
                        {
                            ProgramList.Add(new Demo
                            {
                                ProgramName = (row["ProgramName"] == DBNull.Value) ? "" : Convert.ToString(row["ProgramName"]),
                            });
                            ComponentAndactivityMappingForList.Add(new ComponentAndactivityMappingForList
                            {
                                ProgramNameList = ProgramList
                            });
                        }


                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
            }
            return result;
        }
    }
}
