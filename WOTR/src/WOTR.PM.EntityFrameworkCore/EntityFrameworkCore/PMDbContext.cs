using WOTR.PM.ChekLists;

using WOTR.PM.UnitOfMeasures;
using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
//using System.ComponentModel;
using WOTR.PM.ActionAreas;
using WOTR.PM.ActionSubActionMapping;
using WOTR.PM.Authorization.Roles;
using WOTR.PM.Authorization.Users;
using WOTR.PM.Chat;
using WOTR.PM.Components;
using WOTR.PM.Donors;
using WOTR.PM.Editions;
using WOTR.PM.Friendships;
using WOTR.PM.Locations;
using WOTR.PM.MultiTenancy;
using WOTR.PM.MultiTenancy.Accounting;
using WOTR.PM.MultiTenancy.Payments;
using WOTR.PM.Programs;
using WOTR.PM.States;
using WOTR.PM.Storage;
using WOTR.PM.ImpactIndicators;
using WOTR.PM.ProgramCostEstimations;
using WOTR.PM.PrgActionAreaActivitysMappings;
using WOTR.PM.ProgramRegionSpendings;
using WOTR.PM.Checklists;
using WOTR.PM.PrgRequestAAPlas;
using WOTR.PM.Countrys;
using WOTR.PM.PrgQuarterUnits;
using WOTR.PM.ProgramExpenses;
using WOTR.PM.ProgramFunds;
using WOTR.PM.LoginAndroid;
using WOTR.PM.PrgImplementationsPlans;
using WOTR.PM.ProjectFund;
using WOTR.PM.NewImpactIndicator;

namespace WOTR.PM.EntityFrameworkCore
{
    public class PMDbContext : AbpZeroDbContext<Tenant, Role, User, PMDbContext>, IAbpPersistedGrantDbContext
    {
        public virtual DbSet<Checklist> ChekLists { get; set; }

        

       // public virtual DbSet<UnitofMeasure> UnitofMeasures { get; set; }

        /* Define an IDbSet for each entity of the application */

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<Friendship> Friendships { get; set; }

        public virtual DbSet<ChatMessage> ChatMessages { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

        public virtual DbSet<programCESubTotalCost> programCESubTotalCost { get; set; }

        public virtual DbSet<ProgramCETotalCost> ProgramCETotalCost { get; set; }

        public virtual DbSet<ProgramCostEstimationOverall> ProgramCostEstimationOverall { get; set; }
        


        //...Add DBSET for Wotr DatabaseTables
        public  DbSet<UnitofMeasure> UnitofMeasures { get; set; }
        public DbSet<Location> Location { get; set; }
        public DbSet<SubActionArea> SubActionArea { get; set; }
        public DbSet<ActionArea> ActionArea { get; set; }
        public DbSet<Activity> Activity { get; set; }
        public DbSet<Donor> Donor { get; set; }
        public DbSet<Component> Component { get; set; }
        public DbSet<ActionSAActivityMapping> ActionSAActivityMapping { get; set; }
        public DbSet<ComponentActivityMapping> ComponentActivityMapping { get; set; }
        public DbSet<State> State { get; set; }
        public DbSet<Program> Program { get; set; }
        public DbSet<ProgramRegionCoverage> ProgramRegionCoverage { get; set; }
        public DbSet<ProgramManagerMapping> ProgramManagerMapping { get; set; }
        public DbSet<ImpactIndicator> ImpactIndicator { get; set; }
        public DbSet<ImpactIndicatorActivity> ImpactIndicatorActivity { get; set; }
        public DbSet <ProgramComponentsMapping> ProgramComponentsMapping { get; set; }
        public DbSet<ProgramComponentsActivitesMapping> ProgramComponentsActivitesMapping { get; set; }
        public DbSet<ProgramImpactIndicatorMapping> ProgramImpactIndicatorMapping { get; set; }
        public DbSet<ProgramImpactIndicatorActivityMapping> ProgramImpactIndicatorActivityMapping { get; set; }
        public DbSet<ProgramCostEstimation> ProgramCostEstimation { get; set; }
        public DbSet<PrgActionAreaActivityMapping> PrgActionAreaActivityMapping { get; set; }
        public DbSet<ProgramRegionSpending> ProgramRegionSpending { get; set; }
        public DbSet<ChecklistItem> ChecklistItem { get; set; }
        public DbSet<ActivityCheckList> ActivityCheckList { get; set; }
        public DbSet<prgActionAreaJSTree> prgActionAreaJSTree { get; set; }
        public DbSet<RequestAAplas> RequestAAplas { get; set; }
        public DbSet<RequestAAplasSubtotal> RequestAAplasSubtotal { get; set; }
        public DbSet<ImplementationPlanCheckList> ImplementationPlanCheckList { get; set; }

        public DbSet<District> District { get; set; }
        
        public DbSet<Taluka> Taluka { get; set; }
        public DbSet<Country> Countrys { get; set; }
        public DbSet<Village> Village { get; set; }
        public DbSet<VillageCluster> VillageCluster { get; set; }
        public DbSet<MapSubActivityIteamsToImplementionPlan> MapSubActivityIteamsToImplementionPlan { get; set; }
        public DbSet<ProgramQuqterUnitMapping> ProgramQuqterUnitMapping { get; set; }
        public DbSet<ProgramExpense> ProgramExpense { get; set; }
        public DbSet<ProgramFund> ProgramFunds { get; set; }
        public DbSet<ProjectFunds> ProjectFund { get; set; }
        public DbSet<ExpensesType> ExpensesType { get; set; }
        public DbSet<ProgramExpenseImage> ProgramExpenseImage { get; set; }
        public DbSet<AppDeviceLists> AppDeviceLists { get; set; }
        public DbSet<AppInfo> AppInfo { get; set; }
        public DbSet<Questionarie> Questionarie { get; set; }
        public DbSet<SubActivityQuestionaryMapping> SubActivityQuestionaryMapping { get; set; }
        public DbSet<SubActivityList> SubActivity { get; set; }
        public DbSet<prgImplementationPlanQuestionariesMapping> prgImplementationPlanQuestionariesMapping { get; set; }
        public DbSet<prgImplementationPlanQuestionariesUrlMapping> prgImplementationPlanQuestionariesUrlMapping { get; set; }

        //Impact Indicator Tables

        public DbSet<Crop> Crop { get; set; }
        public DbSet<ImpactIndicatorCategory> ImpactIndicatorCategory { get; set; }
        public DbSet<ImpactIndicatorSubCategory> ImpactIndicatorSubCategory { get; set; }
        public DbSet<Source> Sources { get; set; }
        public DbSet<IIQuestionary> Questionary { get; set; }
        public DbSet<ImpactIndicatorForm> impactIndicatorForms { get; set; }
        public DbSet<IIFormQuestionMapping> iIFormQuestionMappings { get; set; }
        public DbSet<ProjectAndIIFormMapping> projectAndIIFormMappings { get; set; }
        public DbSet<FrequencyOfoccurrence> FrequencyOfoccurrences { get; set; }
        public DbSet<IIFormResponse> IIFormResponses { get; set; }
      
        public DbSet<IIFormAnswer> IIFormAnswers { get; set; }

        
        public PMDbContext(DbContextOptions<PMDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BinaryObject>(b =>
            {
                b.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { e.PaymentId, e.Gateway });
            });

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}
