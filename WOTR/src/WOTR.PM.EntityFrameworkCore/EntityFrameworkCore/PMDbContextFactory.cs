﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using WOTR.PM.Configuration;
using WOTR.PM.Web;

namespace WOTR.PM.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class PMDbContextFactory : IDesignTimeDbContextFactory<PMDbContext>
    {
        public PMDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PMDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);

            PMDbContextConfigurer.Configure(builder, configuration.GetConnectionString(PMConsts.ConnectionStringName));

            return new PMDbContext(builder.Options);
        }
    }
}