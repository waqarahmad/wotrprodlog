using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace WOTR.PM.EntityFrameworkCore
{
    public static class PMDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<PMDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<PMDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}