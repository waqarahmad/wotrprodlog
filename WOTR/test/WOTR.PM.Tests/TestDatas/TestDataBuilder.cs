﻿using WOTR.PM.EntityFrameworkCore;

namespace WOTR.PM.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly PMDbContext _context;
        private readonly int _tenantId;

        public TestDataBuilder(PMDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            new TestOrganizationUnitsBuilder(_context, _tenantId).Create();
            new TestSubscriptionPaymentBuilder(_context, _tenantId).Create();

            _context.SaveChanges();
        }
    }
}
