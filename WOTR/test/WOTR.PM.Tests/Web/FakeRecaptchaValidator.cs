﻿using System.Threading.Tasks;
using WOTR.PM.Security.Recaptcha;

namespace WOTR.PM.Tests.Web
{
    public class FakeRecaptchaValidator : IRecaptchaValidator
    {
        public Task ValidateAsync(string captchaResponse)
        {
            return Task.CompletedTask;
        }
    }
}
